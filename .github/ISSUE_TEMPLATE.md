- [ ] I've read the [guidelines for Contributing to Kontorol Projects](https://github.com/kontorol/platform-install-packages/blob/master/CONTRIBUTING.md)
- [ ] This is not a support request or question that should be posted on the [Kontorol forum](https://forum.kontorol.org) or submitted to the support team in the form of a ticket 


## Bug report
**Please provide a detailed description of the issue and the steps to reproduce:**


## Kontorol ENV
*Please fill out this section if you are hosting the Kontorol platform on your own servers [as opposed to using Kontorol SaaS]. In the event of SaaS, the below should be deleted.*


**Relevant errors from /opt/kontorol/log/kontorol_api_v3.log, /opt/kontorol/log/kontorol_apache_errors*log and /opt/kontorol/log/kontorol_prod.log:**

**Linux distribution name and version:**

**Are you using the RPM or deb packages?**
- [ ] RPM
- [ ] deb

**When using RPM, paste the output for:**
```
# rpm -qa "kontorol-*"
```

**For deb based systems:**
```
# dpkg -l "kontorol-*"
```

**Is this an all in one instance [single server] or a cluster?**
- [ ] All in one
- [ ] Cluster

**If applicable, please provide the MySQL version:**


**If you're having an issue with a specific media asset, please provide a link from which it can be downloaded/played:**


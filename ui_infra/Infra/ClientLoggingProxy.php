<?php
/**
 * Implements the IKontorolLogger interface used by the KontorolClient for logging purposes and proxies the message to the KontorolLog
 *  
 * @package UI-infra
 * @subpackage Client
 */
class Infra_ClientLoggingProxy implements Kontorol_Client_ILogger
{
	public function log($msg)
	{
		KontorolLog::debug($msg);
	}
}

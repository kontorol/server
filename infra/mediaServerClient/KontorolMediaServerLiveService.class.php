<?php

require_once(__DIR__ . '/KontorolMediaServerClient.class.php');
	
class KontorolMediaServerLiveService extends KontorolMediaServerClient
{
	function __construct($url)
	{
		parent::__construct($url);
	}
	
	
	/**
	 * 
	 * @param string $liveEntryId
	 * @return KontorolMediaServerSplitRecordingNowResponse
	 **/
	public function splitRecordingNow($liveEntryId)
	{
		$params = array();
		
		$params["liveEntryId"] = $this->parseParam($liveEntryId, 'xsd:string');

		return $this->doCall("splitRecordingNow", $params, 'KontorolMediaServerSplitRecordingNowResponse');
	}
	
}		
	

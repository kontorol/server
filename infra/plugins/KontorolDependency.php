<?php
/**
 * @package infra
 * @subpackage Plugins
 */
class KontorolDependency
{
	/**
	 * @var string
	 */
	protected $pluginName;
	
	/**
	 * @var KontorolVersion
	 */
	protected $minVersion;
	
	/**
	 * @param string $pluginName
	 * @param KontorolVersion $minVersion
	 */
	public function __construct($pluginName, KontorolVersion $minVersion = null)
	{
		$this->pluginName = $pluginName;
		$this->minVersion = $minVersion;
	}
	
	/**
	 * @return string plugin name
	 */
	public function getPluginName()
	{
		return $this->pluginName;
	}

	/**
	 * @return KontorolVersion minimum version
	 */
	public function getMinimumVersion()
	{
		return $this->minVersion;
	}
}

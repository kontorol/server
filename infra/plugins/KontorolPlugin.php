<?php
/**
 * @package infra
 * @subpackage Plugins
 */
abstract class KontorolPlugin implements IKontorolPlugin
{
	public function getInstance($interface)
	{
		if($this instanceof $interface)
			return $this;
			
		return null;
	}
}

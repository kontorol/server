<?php
/**
 * @package infra
 * @subpackage Plugins
 */
interface IKontorolBase
{
	/**
	 * Return an instance implementing the interface
	 * @param string $interface
	 * @return IKontorolBase
	 */
	public function getInstance($interface);
}

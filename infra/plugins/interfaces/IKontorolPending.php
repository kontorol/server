<?php
/**
 * Enable the plugin to define dependency on another plugin
 * @package infra
 * @subpackage Plugins
 */
interface IKontorolPending extends IKontorolBase
{
	/**
	 * Returns a Kontorol dependency object that defines the relationship between two plugins.
	 * 
	 * @return array<KontorolDependency> The Kontorol dependency object
	 */
	public static function dependsOn();
}

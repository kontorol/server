<?php
/**
 * Enable to plugin to add pages to external applications
 * @package infra
 * @subpackage Plugins
 */
interface IKontorolApplicationPages extends IKontorolBase
{
	/**
	 * @return array
	 */
	public static function getApplicationPages();	
}

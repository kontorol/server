<?php
/**
 * Enable to plugin to add pages to the admin console
 * @package infra
 * @subpackage Plugins
 */
interface IKontorolAdminConsolePages extends IKontorolApplicationPages
{
}

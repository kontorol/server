<?php
/**
 * Enable you to add database connections
 * @package infra
 * @subpackage Plugins
 */
interface IKontorolDatabaseConfig extends IKontorolBase
{
	/**
	 * @return array
	 */
	public static function getDatabaseConfig();	
}

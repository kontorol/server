<?php
/**
 * Enable plugin to execute KontorolRelatedFilter
 * @package infra
 * @subpackage Plugins
 */
interface IKontorolFilterExecutor extends IKontorolBase
{
	public static function canExecuteFilter(KontorolRelatedFilter $filter, $coreFilter);
	public static function executeFilter(KontorolRelatedFilter $filter, $coreFilter, KontorolFilterPager $pager);
}

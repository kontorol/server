<?php
/**
 * Enable to plugin to add translated keys
 * @package infra
 * @subpackage Plugins
 */
interface IKontorolApplicationTranslations extends IKontorolBase
{
	/**
	 * @return array
	 */
	public static function getTranslations($locale);	
}

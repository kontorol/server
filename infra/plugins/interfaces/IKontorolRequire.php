<?php
/**
 * Enable the plugin to define another plugin as a mandatory requirement for its load
 * @package infra
 * @subpackage Plugins
 */
interface IKontorolRequire extends IKontorolBase
{
	/**
	 * Returns string(s) of Kontorol Plugins which the plugin requires
	 * 
	 * @return array<String> The Kontorol dependency object
	 */
	public static function requires();
}

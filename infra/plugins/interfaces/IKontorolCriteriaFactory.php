<?php
/**
 * Enable the plugin to return extended KontorolCriteria object according to the searched object type
 * @package infra
 * @subpackage Plugins
 */
interface IKontorolCriteriaFactory extends IKontorolBase
{
	/**
	 * Creates a new KontorolCriteria for the given object name
	 * 
	 * @param string $objectType object type to create Criteria for.
	 * @return KontorolCriteria derived object
	 */
	public static function getKontorolCriteria($objectType);
}

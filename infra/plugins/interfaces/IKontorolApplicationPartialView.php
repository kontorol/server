<?php
/**
 * Enable the plugin to add phtml view to existing page
 * @package infra
 * @subpackage Plugins
 */
interface IKontorolApplicationPartialView extends IKontorolBase
{
	/**
	 * @return array<Kontorol_View_Helper_PartialViewPlugin>
	 */
	public static function getApplicationPartialViews($controller, $action);
}

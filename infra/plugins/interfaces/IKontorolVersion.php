<?php
/**
 * Enable you to give version to the plugin
 * The version might be importent for depencies between different plugins
 * @package infra
 * @subpackage Plugins
 */
interface IKontorolVersion extends IKontorolBase
{
	/**
	 * @return KontorolVersion
	 */
	public static function getVersion();
}

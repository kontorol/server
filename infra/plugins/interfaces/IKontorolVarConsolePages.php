<?php
/**
 * Enable to plugin to add pages to the var console
 * @package infra
 * @subpackage Plugins
 */
interface IKontorolVarConsolePages extends IKontorolApplicationPages
{
}

<?php
/**
 * Enable the plugin to handle bulk upload additional data
 * @package infra
 * @subpackage Plugins
 */
interface IKontorolImportHandler extends IKontorolBase
{
	/**
	 * This method makes an intermediate change to the imported file or its related data under certain conditions.
	 * @param KCurlHeaderResponse $curlInfo
	 * @param KontorolImportJobData $importData
	 * @param Object $params
	 */
	public static function handleImportContent($curlInfo, $importData, $params);	
}

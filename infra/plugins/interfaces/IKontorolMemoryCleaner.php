<?php
/**
 * Enable the plugin to clean unused memory, instances and pools
 * @package infra
 * @subpackage Plugins
 */
interface IKontorolMemoryCleaner extends IKontorolBase
{
	public static function cleanMemory();
}

<?php
/**
 * Enable the plugin to append configuration to existing server configuration
 * @package infra
 * @subpackage Plugins
 */
interface IKontorolConfigurator extends IKontorolBase
{
	/**
	 * Merge configuration data from the plugin
	 * 
	 * @param string $configName
	 * @return Iterator
	 */
	public static function getConfig($configName);
}

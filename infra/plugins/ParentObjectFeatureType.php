<?php
/**
 * @package infra
 * @subpackage Plugins
 */
class ParentObjectFeatureType implements IKontorolPluginEnum, ObjectFeatureType
{
    const PARENT = 'Parent';

    /* (non-PHPdoc)
     * @see IKontorolPluginEnum::getAdditionalValues()
     */
    public static function getAdditionalValues()
    {
        return array
        (
            'PARENT' => self::PARENT,
        );

    }

    /* (non-PHPdoc)
     * @see IKontorolPluginEnum::getAdditionalDescriptions()
     */
    public static function getAdditionalDescriptions() {
        return array();

    }
}

<?php
/**
 *  This class will help you validate if domains are allowing Kontorol.
 *  @package infra
 *  @subpackage general
 */

class kSpfMailerValidator
{
	const MAILER_KONTOROL_COM = 'mailer.kontorol.com';
	const SPF = 'v=spf1';
	const TXT = 'txt';

	/*
	 *  Get list of domains that are not allowing Kontorol
	 *  @param array $domains
	 *  @return array $domainsNotAllowed
	 */
	public static function getDomainsNotAllowed($domains)
	{
		$domainsNotAllowed = array();
		foreach ($domains as $domain)
		{
			$validationResult = self::validateDomain($domain);
			if($validationResult)
			{
				KontorolLog::debug("$domain allows Kontorol to send on behalf of it\n");
			}
			else
			{
				$domainsNotAllowed[$domain] = $domain;
				KontorolLog::debug("$domain is not allowing Kontorol\n");
			}
		}
		return $domainsNotAllowed;
	}

	/*
	 *  Check if specific domain is allowing Kontorol.
	 *  @param string $domain
	 *  @return boolean
	 */
	public static function validateDomain($domain)
	{
		$dnsRecords = dns_get_record($domain, DNS_TXT);
		if (!$dnsRecords)
		{
			return false;
		}
		foreach($dnsRecords as $record)
		{
			if((strpos($record[self::TXT], self::SPF) !== false)
				&& (strpos($record[self::TXT], self::MAILER_KONTOROL_COM) !== false))
			{
				return true;
			}
		}
		return false;
	}
}

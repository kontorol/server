<?php

class kExecWrapper
{
	public static function exec($command, &$output, &$return_var)
	{
		KontorolLog::log("Executing [$command]");

		$startTime = microtime(true);
		$res = exec($command, $output, $return_var);
		KontorolMonitorClient::monitorExec($command, $startTime);
		return $res;
	}

	public static function system($command, &$return_var)
	{
		KontorolLog::log("Executing [$command]");

		$startTime = microtime(true);
		$res = system($command, $return_var);
		KontorolMonitorClient::monitorExec($command, $startTime);
		return $res;
	}

	public static function shell_exec($command)
	{
		KontorolLog::log("Executing [$command]");

		$startTime = microtime(true);
		$res = shell_exec($command);
		KontorolMonitorClient::monitorExec($command, $startTime);
		return $res;
	}

	public static function passthru($command)
	{
		KontorolLog::log("Executing [$command]");

		$startTime = microtime(true);
		$res = passthru($command);
		KontorolMonitorClient::monitorExec($command, $startTime);
		return $res;
	}
}

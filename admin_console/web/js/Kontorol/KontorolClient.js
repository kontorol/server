function KontorolAccessControlOrderBy()
{
}
KontorolAccessControlOrderBy.prototype.CREATED_AT_ASC = "+createdAt";
KontorolAccessControlOrderBy.prototype.CREATED_AT_DESC = "-createdAt";

function KontorolAudioCodec()
{
}
KontorolAudioCodec.prototype.NONE = "";
KontorolAudioCodec.prototype.MP3 = "mp3";
KontorolAudioCodec.prototype.AAC = "aac";

function KontorolBaseEntryOrderBy()
{
}
KontorolBaseEntryOrderBy.prototype.NAME_ASC = "+name";
KontorolBaseEntryOrderBy.prototype.NAME_DESC = "-name";
KontorolBaseEntryOrderBy.prototype.MODERATION_COUNT_ASC = "+moderationCount";
KontorolBaseEntryOrderBy.prototype.MODERATION_COUNT_DESC = "-moderationCount";
KontorolBaseEntryOrderBy.prototype.CREATED_AT_ASC = "+createdAt";
KontorolBaseEntryOrderBy.prototype.CREATED_AT_DESC = "-createdAt";
KontorolBaseEntryOrderBy.prototype.RANK_ASC = "+rank";
KontorolBaseEntryOrderBy.prototype.RANK_DESC = "-rank";

function KontorolBaseJobOrderBy()
{
}
KontorolBaseJobOrderBy.prototype.CREATED_AT_ASC = "+createdAt";
KontorolBaseJobOrderBy.prototype.CREATED_AT_DESC = "-createdAt";
KontorolBaseJobOrderBy.prototype.EXECUTION_ATTEMPTS_ASC = "+executionAttempts";
KontorolBaseJobOrderBy.prototype.EXECUTION_ATTEMPTS_DESC = "-executionAttempts";

function KontorolBaseSyndicationFeedOrderBy()
{
}
KontorolBaseSyndicationFeedOrderBy.prototype.PLAYLIST_ID_ASC = "+playlistId";
KontorolBaseSyndicationFeedOrderBy.prototype.PLAYLIST_ID_DESC = "-playlistId";
KontorolBaseSyndicationFeedOrderBy.prototype.NAME_ASC = "+name";
KontorolBaseSyndicationFeedOrderBy.prototype.NAME_DESC = "-name";
KontorolBaseSyndicationFeedOrderBy.prototype.TYPE_ASC = "+type";
KontorolBaseSyndicationFeedOrderBy.prototype.TYPE_DESC = "-type";
KontorolBaseSyndicationFeedOrderBy.prototype.CREATED_AT_ASC = "+createdAt";
KontorolBaseSyndicationFeedOrderBy.prototype.CREATED_AT_DESC = "-createdAt";

function KontorolBatchJobErrorTypes()
{
}
KontorolBatchJobErrorTypes.prototype.APP = 0;
KontorolBatchJobErrorTypes.prototype.RUNTIME = 1;
KontorolBatchJobErrorTypes.prototype.HTTP = 2;
KontorolBatchJobErrorTypes.prototype.CURL = 3;

function KontorolBatchJobOrderBy()
{
}
KontorolBatchJobOrderBy.prototype.STATUS_ASC = "+status";
KontorolBatchJobOrderBy.prototype.STATUS_DESC = "-status";
KontorolBatchJobOrderBy.prototype.QUEUE_TIME_ASC = "+queueTime";
KontorolBatchJobOrderBy.prototype.QUEUE_TIME_DESC = "-queueTime";
KontorolBatchJobOrderBy.prototype.FINISH_TIME_ASC = "+finishTime";
KontorolBatchJobOrderBy.prototype.FINISH_TIME_DESC = "-finishTime";
KontorolBatchJobOrderBy.prototype.CREATED_AT_ASC = "+createdAt";
KontorolBatchJobOrderBy.prototype.CREATED_AT_DESC = "-createdAt";
KontorolBatchJobOrderBy.prototype.EXECUTION_ATTEMPTS_ASC = "+executionAttempts";
KontorolBatchJobOrderBy.prototype.EXECUTION_ATTEMPTS_DESC = "-executionAttempts";

function KontorolBatchJobStatus()
{
}
KontorolBatchJobStatus.prototype.PENDING = 0;
KontorolBatchJobStatus.prototype.QUEUED = 1;
KontorolBatchJobStatus.prototype.PROCESSING = 2;
KontorolBatchJobStatus.prototype.PROCESSED = 3;
KontorolBatchJobStatus.prototype.MOVEFILE = 4;
KontorolBatchJobStatus.prototype.FINISHED = 5;
KontorolBatchJobStatus.prototype.FAILED = 6;
KontorolBatchJobStatus.prototype.ABORTED = 7;
KontorolBatchJobStatus.prototype.ALMOST_DONE = 8;
KontorolBatchJobStatus.prototype.RETRY = 9;
KontorolBatchJobStatus.prototype.FATAL = 10;

function KontorolBatchJobType()
{
}
KontorolBatchJobType.prototype.CONVERT = 0;
KontorolBatchJobType.prototype.IMPORT = 1;
KontorolBatchJobType.prototype.DELETE = 2;
KontorolBatchJobType.prototype.FLATTEN = 3;
KontorolBatchJobType.prototype.BULKUPLOAD = 4;
KontorolBatchJobType.prototype.DVDCREATOR = 5;
KontorolBatchJobType.prototype.DOWNLOAD = 6;
KontorolBatchJobType.prototype.OOCONVERT = 7;
KontorolBatchJobType.prototype.CONVERT_PROFILE = 10;
KontorolBatchJobType.prototype.POSTCONVERT = 11;
KontorolBatchJobType.prototype.PULL = 12;
KontorolBatchJobType.prototype.REMOTE_CONVERT = 13;
KontorolBatchJobType.prototype.EXTRACT_MEDIA = 14;
KontorolBatchJobType.prototype.MAIL = 15;
KontorolBatchJobType.prototype.NOTIFICATION = 16;
KontorolBatchJobType.prototype.CLEANUP = 17;
KontorolBatchJobType.prototype.SCHEDULER_HELPER = 18;
KontorolBatchJobType.prototype.BULKDOWNLOAD = 19;
KontorolBatchJobType.prototype.PROJECT = 1000;

function KontorolBulkUploadCsvVersion()
{
}
KontorolBulkUploadCsvVersion.prototype.V1 = "1";
KontorolBulkUploadCsvVersion.prototype.V2 = "2";

function KontorolCategoryOrderBy()
{
}
KontorolCategoryOrderBy.prototype.DEPTH_ASC = "+depth";
KontorolCategoryOrderBy.prototype.DEPTH_DESC = "-depth";
KontorolCategoryOrderBy.prototype.FULL_NAME_ASC = "+fullName";
KontorolCategoryOrderBy.prototype.FULL_NAME_DESC = "-fullName";
KontorolCategoryOrderBy.prototype.CREATED_AT_ASC = "+createdAt";
KontorolCategoryOrderBy.prototype.CREATED_AT_DESC = "-createdAt";

function KontorolCommercialUseType()
{
}
KontorolCommercialUseType.prototype.COMMERCIAL_USE = "commercial_use";
KontorolCommercialUseType.prototype.NON_COMMERCIAL_USE = "non-commercial_use";

function KontorolContainerFormat()
{
}
KontorolContainerFormat.prototype.FLV = "flv";
KontorolContainerFormat.prototype.MP4 = "mp4";
KontorolContainerFormat.prototype.AVI = "avi";
KontorolContainerFormat.prototype.MOV = "mov";
KontorolContainerFormat.prototype._3GP = "3gp";

function KontorolConversionProfileOrderBy()
{
}
KontorolConversionProfileOrderBy.prototype.CREATED_AT_ASC = "+createdAt";
KontorolConversionProfileOrderBy.prototype.CREATED_AT_DESC = "-createdAt";

function KontorolCountryRestrictionType()
{
}
KontorolCountryRestrictionType.prototype.RESTRICT_COUNTRY_LIST = 0;
KontorolCountryRestrictionType.prototype.ALLOW_COUNTRY_LIST = 1;

function KontorolDataEntryOrderBy()
{
}
KontorolDataEntryOrderBy.prototype.NAME_ASC = "+name";
KontorolDataEntryOrderBy.prototype.NAME_DESC = "-name";
KontorolDataEntryOrderBy.prototype.MODERATION_COUNT_ASC = "+moderationCount";
KontorolDataEntryOrderBy.prototype.MODERATION_COUNT_DESC = "-moderationCount";
KontorolDataEntryOrderBy.prototype.CREATED_AT_ASC = "+createdAt";
KontorolDataEntryOrderBy.prototype.CREATED_AT_DESC = "-createdAt";
KontorolDataEntryOrderBy.prototype.RANK_ASC = "+rank";
KontorolDataEntryOrderBy.prototype.RANK_DESC = "-rank";

function KontorolDirectoryRestrictionType()
{
}
KontorolDirectoryRestrictionType.prototype.DONT_DISPLAY = 0;
KontorolDirectoryRestrictionType.prototype.DISPLAY_WITH_LINK = 1;

function KontorolDocumentEntryOrderBy()
{
}
KontorolDocumentEntryOrderBy.prototype.NAME_ASC = "+name";
KontorolDocumentEntryOrderBy.prototype.NAME_DESC = "-name";
KontorolDocumentEntryOrderBy.prototype.MODERATION_COUNT_ASC = "+moderationCount";
KontorolDocumentEntryOrderBy.prototype.MODERATION_COUNT_DESC = "-moderationCount";
KontorolDocumentEntryOrderBy.prototype.CREATED_AT_ASC = "+createdAt";
KontorolDocumentEntryOrderBy.prototype.CREATED_AT_DESC = "-createdAt";
KontorolDocumentEntryOrderBy.prototype.RANK_ASC = "+rank";
KontorolDocumentEntryOrderBy.prototype.RANK_DESC = "-rank";

function KontorolDocumentType()
{
}
KontorolDocumentType.prototype.DOCUMENT = 11;
KontorolDocumentType.prototype.SWF = 12;

function KontorolDurationType()
{
}
KontorolDurationType.prototype.NOT_AVAILABLE = "notavailable";
KontorolDurationType.prototype.SHORT = "short";
KontorolDurationType.prototype.MEDIUM = "medium";
KontorolDurationType.prototype.LONG = "long";

function KontorolEditorType()
{
}
KontorolEditorType.prototype.SIMPLE = 1;
KontorolEditorType.prototype.ADVANCED = 2;

function KontorolEntryModerationStatus()
{
}
KontorolEntryModerationStatus.prototype.PENDING_MODERATION = 1;
KontorolEntryModerationStatus.prototype.APPROVED = 2;
KontorolEntryModerationStatus.prototype.REJECTED = 3;
KontorolEntryModerationStatus.prototype.FLAGGED_FOR_REVIEW = 5;
KontorolEntryModerationStatus.prototype.AUTO_APPROVED = 6;

function KontorolEntryStatus()
{
}
KontorolEntryStatus.prototype.ERROR_IMPORTING = -2;
KontorolEntryStatus.prototype.ERROR_CONVERTING = -1;
KontorolEntryStatus.prototype.IMPORT = 0;
KontorolEntryStatus.prototype.PRECONVERT = 1;
KontorolEntryStatus.prototype.READY = 2;
KontorolEntryStatus.prototype.DELETED = 3;
KontorolEntryStatus.prototype.PENDING = 4;
KontorolEntryStatus.prototype.MODERATE = 5;
KontorolEntryStatus.prototype.BLOCKED = 6;

function KontorolEntryType()
{
}
KontorolEntryType.prototype.AUTOMATIC = -1;
KontorolEntryType.prototype.MEDIA_CLIP = 1;
KontorolEntryType.prototype.MIX = 2;
KontorolEntryType.prototype.PLAYLIST = 5;
KontorolEntryType.prototype.DATA = 6;
KontorolEntryType.prototype.DOCUMENT = 10;

function KontorolFlavorAssetStatus()
{
}
KontorolFlavorAssetStatus.prototype.ERROR = -1;
KontorolFlavorAssetStatus.prototype.QUEUED = 0;
KontorolFlavorAssetStatus.prototype.CONVERTING = 1;
KontorolFlavorAssetStatus.prototype.READY = 2;
KontorolFlavorAssetStatus.prototype.DELETED = 3;
KontorolFlavorAssetStatus.prototype.NOT_APPLICABLE = 4;

function KontorolFlavorParamsOrderBy()
{
}

function KontorolFlavorParamsOutputOrderBy()
{
}

function KontorolGender()
{
}
KontorolGender.prototype.UNKNOWN = 0;
KontorolGender.prototype.MALE = 1;
KontorolGender.prototype.FEMALE = 2;

function KontorolGoogleSyndicationFeedAdultValues()
{
}
KontorolGoogleSyndicationFeedAdultValues.prototype.YES = "Yes";
KontorolGoogleSyndicationFeedAdultValues.prototype.NO = "No";

function KontorolGoogleVideoSyndicationFeedOrderBy()
{
}
KontorolGoogleVideoSyndicationFeedOrderBy.prototype.PLAYLIST_ID_ASC = "+playlistId";
KontorolGoogleVideoSyndicationFeedOrderBy.prototype.PLAYLIST_ID_DESC = "-playlistId";
KontorolGoogleVideoSyndicationFeedOrderBy.prototype.NAME_ASC = "+name";
KontorolGoogleVideoSyndicationFeedOrderBy.prototype.NAME_DESC = "-name";
KontorolGoogleVideoSyndicationFeedOrderBy.prototype.TYPE_ASC = "+type";
KontorolGoogleVideoSyndicationFeedOrderBy.prototype.TYPE_DESC = "-type";
KontorolGoogleVideoSyndicationFeedOrderBy.prototype.CREATED_AT_ASC = "+createdAt";
KontorolGoogleVideoSyndicationFeedOrderBy.prototype.CREATED_AT_DESC = "-createdAt";

function KontorolITunesSyndicationFeedAdultValues()
{
}
KontorolITunesSyndicationFeedAdultValues.prototype.YES = "yes";
KontorolITunesSyndicationFeedAdultValues.prototype.NO = "no";
KontorolITunesSyndicationFeedAdultValues.prototype.CLEAN = "clean";

function KontorolITunesSyndicationFeedCategories()
{
}
KontorolITunesSyndicationFeedCategories.prototype.ARTS = "Arts";
KontorolITunesSyndicationFeedCategories.prototype.ARTS_DESIGN = "Arts/Design";
KontorolITunesSyndicationFeedCategories.prototype.ARTS_FASHION_BEAUTY = "Arts/Fashion &amp; Beauty";
KontorolITunesSyndicationFeedCategories.prototype.ARTS_FOOD = "Arts/Food";
KontorolITunesSyndicationFeedCategories.prototype.ARTS_LITERATURE = "Arts/Literature";
KontorolITunesSyndicationFeedCategories.prototype.ARTS_PERFORMING_ARTS = "Arts/Performing Arts";
KontorolITunesSyndicationFeedCategories.prototype.ARTS_VISUAL_ARTS = "Arts/Visual Arts";
KontorolITunesSyndicationFeedCategories.prototype.BUSINESS = "Business";
KontorolITunesSyndicationFeedCategories.prototype.BUSINESS_BUSINESS_NEWS = "Business/Business News";
KontorolITunesSyndicationFeedCategories.prototype.BUSINESS_CAREERS = "Business/Careers";
KontorolITunesSyndicationFeedCategories.prototype.BUSINESS_INVESTING = "Business/Investing";
KontorolITunesSyndicationFeedCategories.prototype.BUSINESS_MANAGEMENT_MARKETING = "Business/Management &amp; Marketing";
KontorolITunesSyndicationFeedCategories.prototype.BUSINESS_SHOPPING = "Business/Shopping";
KontorolITunesSyndicationFeedCategories.prototype.COMEDY = "Comedy";
KontorolITunesSyndicationFeedCategories.prototype.EDUCATION = "Education";
KontorolITunesSyndicationFeedCategories.prototype.EDUCATION_TECHNOLOGY = "Education/Education Technology";
KontorolITunesSyndicationFeedCategories.prototype.EDUCATION_HIGHER_EDUCATION = "Education/Higher Education";
KontorolITunesSyndicationFeedCategories.prototype.EDUCATION_K_12 = "Education/K-12";
KontorolITunesSyndicationFeedCategories.prototype.EDUCATION_LANGUAGE_COURSES = "Education/Language Courses";
KontorolITunesSyndicationFeedCategories.prototype.EDUCATION_TRAINING = "Education/Training";
KontorolITunesSyndicationFeedCategories.prototype.GAMES_HOBBIES = "Games &amp; Hobbies";
KontorolITunesSyndicationFeedCategories.prototype.GAMES_HOBBIES_AUTOMOTIVE = "Games &amp; Hobbies/Automotive";
KontorolITunesSyndicationFeedCategories.prototype.GAMES_HOBBIES_AVIATION = "Games &amp; Hobbies/Aviation";
KontorolITunesSyndicationFeedCategories.prototype.GAMES_HOBBIES_HOBBIES = "Games &amp; Hobbies/Hobbies";
KontorolITunesSyndicationFeedCategories.prototype.GAMES_HOBBIES_OTHER_GAMES = "Games &amp; Hobbies/Other Games";
KontorolITunesSyndicationFeedCategories.prototype.GAMES_HOBBIES_VIDEO_GAMES = "Games &amp; Hobbies/Video Games";
KontorolITunesSyndicationFeedCategories.prototype.GOVERNMENT_ORGANIZATIONS = "Government &amp; Organizations";
KontorolITunesSyndicationFeedCategories.prototype.GOVERNMENT_ORGANIZATIONS_LOCAL = "Government &amp; Organizations/Local";
KontorolITunesSyndicationFeedCategories.prototype.GOVERNMENT_ORGANIZATIONS_NATIONAL = "Government &amp; Organizations/National";
KontorolITunesSyndicationFeedCategories.prototype.GOVERNMENT_ORGANIZATIONS_NON_PROFIT = "Government &amp; Organizations/Non-Profit";
KontorolITunesSyndicationFeedCategories.prototype.GOVERNMENT_ORGANIZATIONS_REGIONAL = "Government &amp; Organizations/Regional";
KontorolITunesSyndicationFeedCategories.prototype.HEALTH = "Health";
KontorolITunesSyndicationFeedCategories.prototype.HEALTH_ALTERNATIVE_HEALTH = "Health/Alternative Health";
KontorolITunesSyndicationFeedCategories.prototype.HEALTH_FITNESS_NUTRITION = "Health/Fitness &amp; Nutrition";
KontorolITunesSyndicationFeedCategories.prototype.HEALTH_SELF_HELP = "Health/Self-Help";
KontorolITunesSyndicationFeedCategories.prototype.HEALTH_SEXUALITY = "Health/Sexuality";
KontorolITunesSyndicationFeedCategories.prototype.KIDS_FAMILY = "Kids &amp; Family";
KontorolITunesSyndicationFeedCategories.prototype.MUSIC = "Music";
KontorolITunesSyndicationFeedCategories.prototype.NEWS_POLITICS = "News &amp; Politics";
KontorolITunesSyndicationFeedCategories.prototype.RELIGION_SPIRITUALITY = "Religion &amp; Spirituality";
KontorolITunesSyndicationFeedCategories.prototype.RELIGION_SPIRITUALITY_BUDDHISM = "Religion &amp; Spirituality/Buddhism";
KontorolITunesSyndicationFeedCategories.prototype.RELIGION_SPIRITUALITY_CHRISTIANITY = "Religion &amp; Spirituality/Christianity";
KontorolITunesSyndicationFeedCategories.prototype.RELIGION_SPIRITUALITY_HINDUISM = "Religion &amp; Spirituality/Hinduism";
KontorolITunesSyndicationFeedCategories.prototype.RELIGION_SPIRITUALITY_ISLAM = "Religion &amp; Spirituality/Islam";
KontorolITunesSyndicationFeedCategories.prototype.RELIGION_SPIRITUALITY_JUDAISM = "Religion &amp; Spirituality/Judaism";
KontorolITunesSyndicationFeedCategories.prototype.RELIGION_SPIRITUALITY_OTHER = "Religion &amp; Spirituality/Other";
KontorolITunesSyndicationFeedCategories.prototype.RELIGION_SPIRITUALITY_SPIRITUALITY = "Religion &amp; Spirituality/Spirituality";
KontorolITunesSyndicationFeedCategories.prototype.SCIENCE_MEDICINE = "Science &amp; Medicine";
KontorolITunesSyndicationFeedCategories.prototype.SCIENCE_MEDICINE_MEDICINE = "Science &amp; Medicine/Medicine";
KontorolITunesSyndicationFeedCategories.prototype.SCIENCE_MEDICINE_NATURAL_SCIENCES = "Science &amp; Medicine/Natural Sciences";
KontorolITunesSyndicationFeedCategories.prototype.SCIENCE_MEDICINE_SOCIAL_SCIENCES = "Science &amp; Medicine/Social Sciences";
KontorolITunesSyndicationFeedCategories.prototype.SOCIETY_CULTURE = "Society &amp; Culture";
KontorolITunesSyndicationFeedCategories.prototype.SOCIETY_CULTURE_HISTORY = "Society &amp; Culture/History";
KontorolITunesSyndicationFeedCategories.prototype.SOCIETY_CULTURE_PERSONAL_JOURNALS = "Society &amp; Culture/Personal Journals";
KontorolITunesSyndicationFeedCategories.prototype.SOCIETY_CULTURE_PHILOSOPHY = "Society &amp; Culture/Philosophy";
KontorolITunesSyndicationFeedCategories.prototype.SOCIETY_CULTURE_PLACES_TRAVEL = "Society &amp; Culture/Places &amp; Travel";
KontorolITunesSyndicationFeedCategories.prototype.SPORTS_RECREATION = "Sports &amp; Recreation";
KontorolITunesSyndicationFeedCategories.prototype.SPORTS_RECREATION_AMATEUR = "Sports &amp; Recreation/Amateur";
KontorolITunesSyndicationFeedCategories.prototype.SPORTS_RECREATION_COLLEGE_HIGH_SCHOOL = "Sports &amp; Recreation/College &amp; High School";
KontorolITunesSyndicationFeedCategories.prototype.SPORTS_RECREATION_OUTDOOR = "Sports &amp; Recreation/Outdoor";
KontorolITunesSyndicationFeedCategories.prototype.SPORTS_RECREATION_PROFESSIONAL = "Sports &amp; Recreation/Professional";
KontorolITunesSyndicationFeedCategories.prototype.TECHNOLOGY = "Technology";
KontorolITunesSyndicationFeedCategories.prototype.TECHNOLOGY_GADGETS = "Technology/Gadgets";
KontorolITunesSyndicationFeedCategories.prototype.TECHNOLOGY_TECH_NEWS = "Technology/Tech News";
KontorolITunesSyndicationFeedCategories.prototype.TECHNOLOGY_PODCASTING = "Technology/Podcasting";
KontorolITunesSyndicationFeedCategories.prototype.TECHNOLOGY_SOFTWARE_HOW_TO = "Technology/Software How-To";
KontorolITunesSyndicationFeedCategories.prototype.TV_FILM = "TV &amp; Film";

function KontorolITunesSyndicationFeedOrderBy()
{
}
KontorolITunesSyndicationFeedOrderBy.prototype.PLAYLIST_ID_ASC = "+playlistId";
KontorolITunesSyndicationFeedOrderBy.prototype.PLAYLIST_ID_DESC = "-playlistId";
KontorolITunesSyndicationFeedOrderBy.prototype.NAME_ASC = "+name";
KontorolITunesSyndicationFeedOrderBy.prototype.NAME_DESC = "-name";
KontorolITunesSyndicationFeedOrderBy.prototype.TYPE_ASC = "+type";
KontorolITunesSyndicationFeedOrderBy.prototype.TYPE_DESC = "-type";
KontorolITunesSyndicationFeedOrderBy.prototype.CREATED_AT_ASC = "+createdAt";
KontorolITunesSyndicationFeedOrderBy.prototype.CREATED_AT_DESC = "-createdAt";

function KontorolLicenseType()
{
}
KontorolLicenseType.prototype.UNKNOWN = -1;
KontorolLicenseType.prototype.NONE = 0;
KontorolLicenseType.prototype.COPYRIGHTED = 1;
KontorolLicenseType.prototype.PUBLIC_DOMAIN = 2;
KontorolLicenseType.prototype.CREATIVECOMMONS_ATTRIBUTION = 3;
KontorolLicenseType.prototype.CREATIVECOMMONS_ATTRIBUTION_SHARE_ALIKE = 4;
KontorolLicenseType.prototype.CREATIVECOMMONS_ATTRIBUTION_NO_DERIVATIVES = 5;
KontorolLicenseType.prototype.CREATIVECOMMONS_ATTRIBUTION_NON_COMMERCIAL = 6;
KontorolLicenseType.prototype.CREATIVECOMMONS_ATTRIBUTION_NON_COMMERCIAL_SHARE_ALIKE = 7;
KontorolLicenseType.prototype.CREATIVECOMMONS_ATTRIBUTION_NON_COMMERCIAL_NO_DERIVATIVES = 8;
KontorolLicenseType.prototype.GFDL = 9;
KontorolLicenseType.prototype.GPL = 10;
KontorolLicenseType.prototype.AFFERO_GPL = 11;
KontorolLicenseType.prototype.LGPL = 12;
KontorolLicenseType.prototype.BSD = 13;
KontorolLicenseType.prototype.APACHE = 14;
KontorolLicenseType.prototype.MOZILLA = 15;

function KontorolMailJobOrderBy()
{
}
KontorolMailJobOrderBy.prototype.CREATED_AT_ASC = "+createdAt";
KontorolMailJobOrderBy.prototype.CREATED_AT_DESC = "-createdAt";
KontorolMailJobOrderBy.prototype.EXECUTION_ATTEMPTS_ASC = "+executionAttempts";
KontorolMailJobOrderBy.prototype.EXECUTION_ATTEMPTS_DESC = "-executionAttempts";

function KontorolMailJobStatus()
{
}
KontorolMailJobStatus.prototype.PENDING = 1;
KontorolMailJobStatus.prototype.SENT = 2;
KontorolMailJobStatus.prototype.ERROR = 3;
KontorolMailJobStatus.prototype.QUEUED = 4;

function KontorolMailType()
{
}
KontorolMailType.prototype.MAIL_TYPE_KONTOROL_NEWSLETTER = 10;
KontorolMailType.prototype.MAIL_TYPE_ADDED_TO_FAVORITES = 11;
KontorolMailType.prototype.MAIL_TYPE_ADDED_TO_CLIP_FAVORITES = 12;
KontorolMailType.prototype.MAIL_TYPE_NEW_COMMENT_IN_PROFILE = 13;
KontorolMailType.prototype.MAIL_TYPE_CLIP_ADDED_YOUR_KONTOROL = 20;
KontorolMailType.prototype.MAIL_TYPE_VIDEO_ADDED = 21;
KontorolMailType.prototype.MAIL_TYPE_ROUGHCUT_CREATED = 22;
KontorolMailType.prototype.MAIL_TYPE_ADDED_KONTOROL_TO_YOUR_FAVORITES = 23;
KontorolMailType.prototype.MAIL_TYPE_NEW_COMMENT_IN_KONTOROL = 24;
KontorolMailType.prototype.MAIL_TYPE_CLIP_ADDED = 30;
KontorolMailType.prototype.MAIL_TYPE_VIDEO_CREATED = 31;
KontorolMailType.prototype.MAIL_TYPE_ADDED_KONTOROL_TO_HIS_FAVORITES = 32;
KontorolMailType.prototype.MAIL_TYPE_NEW_COMMENT_IN_KONTOROL_YOU_CONTRIBUTED = 33;
KontorolMailType.prototype.MAIL_TYPE_CLIP_CONTRIBUTED = 40;
KontorolMailType.prototype.MAIL_TYPE_ROUGHCUT_CREATED_SUBSCRIBED = 41;
KontorolMailType.prototype.MAIL_TYPE_ADDED_KONTOROL_TO_HIS_FAVORITES_SUBSCRIBED = 42;
KontorolMailType.prototype.MAIL_TYPE_NEW_COMMENT_IN_KONTOROL_YOU_SUBSCRIBED = 43;
KontorolMailType.prototype.MAIL_TYPE_REGISTER_CONFIRM = 50;
KontorolMailType.prototype.MAIL_TYPE_PASSWORD_RESET = 51;
KontorolMailType.prototype.MAIL_TYPE_LOGIN_MAIL_RESET = 52;
KontorolMailType.prototype.MAIL_TYPE_REGISTER_CONFIRM_VIDEO_SERVICE = 54;
KontorolMailType.prototype.MAIL_TYPE_VIDEO_READY = 60;
KontorolMailType.prototype.MAIL_TYPE_VIDEO_IS_READY = 62;
KontorolMailType.prototype.MAIL_TYPE_BULK_DOWNLOAD_READY = 63;
KontorolMailType.prototype.MAIL_TYPE_NOTIFY_ERR = 70;
KontorolMailType.prototype.MAIL_TYPE_ACCOUNT_UPGRADE_CONFIRM = 80;
KontorolMailType.prototype.MAIL_TYPE_VIDEO_SERVICE_NOTICE = 81;
KontorolMailType.prototype.MAIL_TYPE_VIDEO_SERVICE_NOTICE_LIMIT_REACHED = 82;
KontorolMailType.prototype.MAIL_TYPE_VIDEO_SERVICE_NOTICE_ACCOUNT_LOCKED = 83;
KontorolMailType.prototype.MAIL_TYPE_VIDEO_SERVICE_NOTICE_ACCOUNT_DELETED = 84;
KontorolMailType.prototype.MAIL_TYPE_VIDEO_SERVICE_NOTICE_UPGRADE_OFFER = 85;
KontorolMailType.prototype.MAIL_TYPE_ACCOUNT_REACTIVE_CONFIRM = 86;
KontorolMailType.prototype.MAIL_TYPE_SYSTEM_USER_RESET_PASSWORD = 110;
KontorolMailType.prototype.MAIL_TYPE_SYSTEM_USER_RESET_PASSWORD_SUCCESS = 111;

function KontorolPlayableEntryOrderBy()
{
}
KontorolPlayableEntryOrderBy.prototype.PLAYS_ASC = "+plays";
KontorolPlayableEntryOrderBy.prototype.PLAYS_DESC = "-plays";
KontorolPlayableEntryOrderBy.prototype.VIEWS_ASC = "+views";
KontorolPlayableEntryOrderBy.prototype.VIEWS_DESC = "-views";
KontorolPlayableEntryOrderBy.prototype.DURATION_ASC = "+duration";
KontorolPlayableEntryOrderBy.prototype.DURATION_DESC = "-duration";
KontorolPlayableEntryOrderBy.prototype.NAME_ASC = "+name";
KontorolPlayableEntryOrderBy.prototype.NAME_DESC = "-name";
KontorolPlayableEntryOrderBy.prototype.MODERATION_COUNT_ASC = "+moderationCount";
KontorolPlayableEntryOrderBy.prototype.MODERATION_COUNT_DESC = "-moderationCount";
KontorolPlayableEntryOrderBy.prototype.CREATED_AT_ASC = "+createdAt";
KontorolPlayableEntryOrderBy.prototype.CREATED_AT_DESC = "-createdAt";
KontorolPlayableEntryOrderBy.prototype.RANK_ASC = "+rank";
KontorolPlayableEntryOrderBy.prototype.RANK_DESC = "-rank";

function KontorolMediaEntryOrderBy()
{
}
KontorolMediaEntryOrderBy.prototype.MEDIA_TYPE_ASC = "+mediaType";
KontorolMediaEntryOrderBy.prototype.MEDIA_TYPE_DESC = "-mediaType";
KontorolMediaEntryOrderBy.prototype.PLAYS_ASC = "+plays";
KontorolMediaEntryOrderBy.prototype.PLAYS_DESC = "-plays";
KontorolMediaEntryOrderBy.prototype.VIEWS_ASC = "+views";
KontorolMediaEntryOrderBy.prototype.VIEWS_DESC = "-views";
KontorolMediaEntryOrderBy.prototype.DURATION_ASC = "+duration";
KontorolMediaEntryOrderBy.prototype.DURATION_DESC = "-duration";
KontorolMediaEntryOrderBy.prototype.NAME_ASC = "+name";
KontorolMediaEntryOrderBy.prototype.NAME_DESC = "-name";
KontorolMediaEntryOrderBy.prototype.MODERATION_COUNT_ASC = "+moderationCount";
KontorolMediaEntryOrderBy.prototype.MODERATION_COUNT_DESC = "-moderationCount";
KontorolMediaEntryOrderBy.prototype.CREATED_AT_ASC = "+createdAt";
KontorolMediaEntryOrderBy.prototype.CREATED_AT_DESC = "-createdAt";
KontorolMediaEntryOrderBy.prototype.RANK_ASC = "+rank";
KontorolMediaEntryOrderBy.prototype.RANK_DESC = "-rank";

function KontorolMediaType()
{
}
KontorolMediaType.prototype.VIDEO = 1;
KontorolMediaType.prototype.IMAGE = 2;
KontorolMediaType.prototype.AUDIO = 5;

function KontorolMixEntryOrderBy()
{
}
KontorolMixEntryOrderBy.prototype.PLAYS_ASC = "+plays";
KontorolMixEntryOrderBy.prototype.PLAYS_DESC = "-plays";
KontorolMixEntryOrderBy.prototype.VIEWS_ASC = "+views";
KontorolMixEntryOrderBy.prototype.VIEWS_DESC = "-views";
KontorolMixEntryOrderBy.prototype.DURATION_ASC = "+duration";
KontorolMixEntryOrderBy.prototype.DURATION_DESC = "-duration";
KontorolMixEntryOrderBy.prototype.NAME_ASC = "+name";
KontorolMixEntryOrderBy.prototype.NAME_DESC = "-name";
KontorolMixEntryOrderBy.prototype.MODERATION_COUNT_ASC = "+moderationCount";
KontorolMixEntryOrderBy.prototype.MODERATION_COUNT_DESC = "-moderationCount";
KontorolMixEntryOrderBy.prototype.CREATED_AT_ASC = "+createdAt";
KontorolMixEntryOrderBy.prototype.CREATED_AT_DESC = "-createdAt";
KontorolMixEntryOrderBy.prototype.RANK_ASC = "+rank";
KontorolMixEntryOrderBy.prototype.RANK_DESC = "-rank";

function KontorolModerationFlagStatus()
{
}
KontorolModerationFlagStatus.prototype.PENDING = 1;
KontorolModerationFlagStatus.prototype.MODERATED = 2;

function KontorolModerationFlagType()
{
}
KontorolModerationFlagType.prototype.SEXUAL_CONTENT = 1;
KontorolModerationFlagType.prototype.VIOLENT_REPULSIVE = 2;
KontorolModerationFlagType.prototype.HARMFUL_DANGEROUS = 3;
KontorolModerationFlagType.prototype.SPAM_COMMERCIALS = 4;

function KontorolModerationObjectType()
{
}
KontorolModerationObjectType.prototype.ENTRY = 2;
KontorolModerationObjectType.prototype.USER = 3;

function KontorolNotificationObjectType()
{
}
KontorolNotificationObjectType.prototype.ENTRY = 1;
KontorolNotificationObjectType.prototype.KSHOW = 2;
KontorolNotificationObjectType.prototype.USER = 3;
KontorolNotificationObjectType.prototype.BATCH_JOB = 4;

function KontorolNotificationOrderBy()
{
}
KontorolNotificationOrderBy.prototype.CREATED_AT_ASC = "+createdAt";
KontorolNotificationOrderBy.prototype.CREATED_AT_DESC = "-createdAt";
KontorolNotificationOrderBy.prototype.EXECUTION_ATTEMPTS_ASC = "+executionAttempts";
KontorolNotificationOrderBy.prototype.EXECUTION_ATTEMPTS_DESC = "-executionAttempts";

function KontorolNotificationStatus()
{
}
KontorolNotificationStatus.prototype.PENDING = 1;
KontorolNotificationStatus.prototype.SENT = 2;
KontorolNotificationStatus.prototype.ERROR = 3;
KontorolNotificationStatus.prototype.SHOULD_RESEND = 4;
KontorolNotificationStatus.prototype.ERROR_RESENDING = 5;
KontorolNotificationStatus.prototype.SENT_SYNCH = 6;
KontorolNotificationStatus.prototype.QUEUED = 7;

function KontorolNotificationType()
{
}
KontorolNotificationType.prototype.ENTRY_ADD = 1;
KontorolNotificationType.prototype.ENTR_UPDATE_PERMISSIONS = 2;
KontorolNotificationType.prototype.ENTRY_DELETE = 3;
KontorolNotificationType.prototype.ENTRY_BLOCK = 4;
KontorolNotificationType.prototype.ENTRY_UPDATE = 5;
KontorolNotificationType.prototype.ENTRY_UPDATE_THUMBNAIL = 6;
KontorolNotificationType.prototype.ENTRY_UPDATE_MODERATION = 7;
KontorolNotificationType.prototype.USER_ADD = 21;
KontorolNotificationType.prototype.USER_BANNED = 26;

function KontorolNullableBoolean()
{
}
KontorolNullableBoolean.prototype.NULL_VALUE = -1;
KontorolNullableBoolean.prototype.FALSE_VALUE = 0;
KontorolNullableBoolean.prototype.TRUE_VALUE = 1;

function KontorolPartnerOrderBy()
{
}
KontorolPartnerOrderBy.prototype.ID_ASC = "+id";
KontorolPartnerOrderBy.prototype.ID_DESC = "-id";
KontorolPartnerOrderBy.prototype.NAME_ASC = "+name";
KontorolPartnerOrderBy.prototype.NAME_DESC = "-name";
KontorolPartnerOrderBy.prototype.WEBSITE_ASC = "+website";
KontorolPartnerOrderBy.prototype.WEBSITE_DESC = "-website";
KontorolPartnerOrderBy.prototype.CREATED_AT_ASC = "+createdAt";
KontorolPartnerOrderBy.prototype.CREATED_AT_DESC = "-createdAt";
KontorolPartnerOrderBy.prototype.ADMIN_NAME_ASC = "+adminName";
KontorolPartnerOrderBy.prototype.ADMIN_NAME_DESC = "-adminName";
KontorolPartnerOrderBy.prototype.ADMIN_EMAIL_ASC = "+adminEmail";
KontorolPartnerOrderBy.prototype.ADMIN_EMAIL_DESC = "-adminEmail";
KontorolPartnerOrderBy.prototype.STATUS_ASC = "+status";
KontorolPartnerOrderBy.prototype.STATUS_DESC = "-status";

function KontorolPartnerType()
{
}
KontorolPartnerType.prototype.KMC = 1;
KontorolPartnerType.prototype.WIKI = 100;
KontorolPartnerType.prototype.WORDPRESS = 101;
KontorolPartnerType.prototype.DRUPAL = 102;
KontorolPartnerType.prototype.DEKIWIKI = 103;
KontorolPartnerType.prototype.MOODLE = 104;
KontorolPartnerType.prototype.COMMUNITY_EDITION = 105;
KontorolPartnerType.prototype.JOOMLA = 106;

function KontorolPlaylistOrderBy()
{
}
KontorolPlaylistOrderBy.prototype.NAME_ASC = "+name";
KontorolPlaylistOrderBy.prototype.NAME_DESC = "-name";
KontorolPlaylistOrderBy.prototype.MODERATION_COUNT_ASC = "+moderationCount";
KontorolPlaylistOrderBy.prototype.MODERATION_COUNT_DESC = "-moderationCount";
KontorolPlaylistOrderBy.prototype.CREATED_AT_ASC = "+createdAt";
KontorolPlaylistOrderBy.prototype.CREATED_AT_DESC = "-createdAt";
KontorolPlaylistOrderBy.prototype.RANK_ASC = "+rank";
KontorolPlaylistOrderBy.prototype.RANK_DESC = "-rank";

function KontorolPlaylistType()
{
}
KontorolPlaylistType.prototype.DYNAMIC = 10;
KontorolPlaylistType.prototype.STATIC_LIST = 3;
KontorolPlaylistType.prototype.EXTERNAL = 101;

function KontorolReportType()
{
}
KontorolReportType.prototype.TOP_CONTENT = 1;
KontorolReportType.prototype.CONTENT_DROPOFF = 2;
KontorolReportType.prototype.CONTENT_INTERACTIONS = 3;
KontorolReportType.prototype.MAP_OVERLAY = 4;
KontorolReportType.prototype.TOP_CONTRIBUTORS = 5;
KontorolReportType.prototype.TOP_SYNDICATION = 6;
KontorolReportType.prototype.CONTENT_CONTRIBUTIONS = 7;
KontorolReportType.prototype.WIDGETS_STATS = 8;

function KontorolSearchProviderType()
{
}
KontorolSearchProviderType.prototype.FLICKR = 3;
KontorolSearchProviderType.prototype.YOUTUBE = 4;
KontorolSearchProviderType.prototype.MYSPACE = 7;
KontorolSearchProviderType.prototype.PHOTOBUCKET = 8;
KontorolSearchProviderType.prototype.JAMENDO = 9;
KontorolSearchProviderType.prototype.CCMIXTER = 10;
KontorolSearchProviderType.prototype.NYPL = 11;
KontorolSearchProviderType.prototype.CURRENT = 12;
KontorolSearchProviderType.prototype.MEDIA_COMMONS = 13;
KontorolSearchProviderType.prototype.KONTOROL = 20;
KontorolSearchProviderType.prototype.KONTOROL_USER_CLIPS = 21;
KontorolSearchProviderType.prototype.ARCHIVE_ORG = 22;
KontorolSearchProviderType.prototype.KONTOROL_PARTNER = 23;
KontorolSearchProviderType.prototype.METACAFE = 24;
KontorolSearchProviderType.prototype.SEARCH_PROXY = 28;

function KontorolSessionType()
{
}
KontorolSessionType.prototype.USER = 0;
KontorolSessionType.prototype.ADMIN = 2;

function KontorolSiteRestrictionType()
{
}
KontorolSiteRestrictionType.prototype.RESTRICT_SITE_LIST = 0;
KontorolSiteRestrictionType.prototype.ALLOW_SITE_LIST = 1;

function KontorolSourceType()
{
}
KontorolSourceType.prototype.FILE = 1;
KontorolSourceType.prototype.WEBCAM = 2;
KontorolSourceType.prototype.URL = 5;
KontorolSourceType.prototype.SEARCH_PROVIDER = 6;

function KontorolStatsEventType()
{
}
KontorolStatsEventType.prototype.WIDGET_LOADED = 1;
KontorolStatsEventType.prototype.MEDIA_LOADED = 2;
KontorolStatsEventType.prototype.PLAY = 3;
KontorolStatsEventType.prototype.PLAY_REACHED_25 = 4;
KontorolStatsEventType.prototype.PLAY_REACHED_50 = 5;
KontorolStatsEventType.prototype.PLAY_REACHED_75 = 6;
KontorolStatsEventType.prototype.PLAY_REACHED_100 = 7;
KontorolStatsEventType.prototype.OPEN_EDIT = 8;
KontorolStatsEventType.prototype.OPEN_VIRAL = 9;
KontorolStatsEventType.prototype.OPEN_DOWNLOAD = 10;
KontorolStatsEventType.prototype.OPEN_REPORT = 11;
KontorolStatsEventType.prototype.BUFFER_START = 12;
KontorolStatsEventType.prototype.BUFFER_END = 13;
KontorolStatsEventType.prototype.OPEN_FULL_SCREEN = 14;
KontorolStatsEventType.prototype.CLOSE_FULL_SCREEN = 15;
KontorolStatsEventType.prototype.REPLAY = 16;
KontorolStatsEventType.prototype.SEEK = 17;
KontorolStatsEventType.prototype.OPEN_UPLOAD = 18;
KontorolStatsEventType.prototype.SAVE_PUBLISH = 19;
KontorolStatsEventType.prototype.CLOSE_EDITOR = 20;
KontorolStatsEventType.prototype.PRE_BUMPER_PLAYED = 21;
KontorolStatsEventType.prototype.POST_BUMPER_PLAYED = 22;
KontorolStatsEventType.prototype.BUMPER_CLICKED = 23;
KontorolStatsEventType.prototype.FUTURE_USE_1 = 24;
KontorolStatsEventType.prototype.FUTURE_USE_2 = 25;
KontorolStatsEventType.prototype.FUTURE_USE_3 = 26;

function KontorolStatsKmcEventType()
{
}
KontorolStatsKmcEventType.prototype.CONTENT_PAGE_VIEW = 1001;
KontorolStatsKmcEventType.prototype.CONTENT_ADD_PLAYLIST = 1010;
KontorolStatsKmcEventType.prototype.CONTENT_EDIT_PLAYLIST = 1011;
KontorolStatsKmcEventType.prototype.CONTENT_DELETE_PLAYLIST = 1012;
KontorolStatsKmcEventType.prototype.CONTENT_DELETE_ITEM = 1058;
KontorolStatsKmcEventType.prototype.CONTENT_EDIT_ENTRY = 1013;
KontorolStatsKmcEventType.prototype.CONTENT_CHANGE_THUMBNAIL = 1014;
KontorolStatsKmcEventType.prototype.CONTENT_ADD_TAGS = 1015;
KontorolStatsKmcEventType.prototype.CONTENT_REMOVE_TAGS = 1016;
KontorolStatsKmcEventType.prototype.CONTENT_ADD_ADMIN_TAGS = 1017;
KontorolStatsKmcEventType.prototype.CONTENT_REMOVE_ADMIN_TAGS = 1018;
KontorolStatsKmcEventType.prototype.CONTENT_DOWNLOAD = 1019;
KontorolStatsKmcEventType.prototype.CONTENT_APPROVE_MODERATION = 1020;
KontorolStatsKmcEventType.prototype.CONTENT_REJECT_MODERATION = 1021;
KontorolStatsKmcEventType.prototype.CONTENT_BULK_UPLOAD = 1022;
KontorolStatsKmcEventType.prototype.CONTENT_ADMIN_KCW_UPLOAD = 1023;
KontorolStatsKmcEventType.prototype.CONTENT_CONTENT_GO_TO_PAGE = 1057;
KontorolStatsKmcEventType.prototype.ACCOUNT_CHANGE_PARTNER_INFO = 1030;
KontorolStatsKmcEventType.prototype.ACCOUNT_CHANGE_LOGIN_INFO = 1031;
KontorolStatsKmcEventType.prototype.ACCOUNT_CONTACT_US_USAGE = 1032;
KontorolStatsKmcEventType.prototype.ACCOUNT_UPDATE_SERVER_SETTINGS = 1033;
KontorolStatsKmcEventType.prototype.ACCOUNT_ACCOUNT_OVERVIEW = 1034;
KontorolStatsKmcEventType.prototype.ACCOUNT_ACCESS_CONTROL = 1035;
KontorolStatsKmcEventType.prototype.ACCOUNT_TRANSCODING_SETTINGS = 1036;
KontorolStatsKmcEventType.prototype.ACCOUNT_ACCOUNT_UPGRADE = 1037;
KontorolStatsKmcEventType.prototype.ACCOUNT_SAVE_SERVER_SETTINGS = 1038;
KontorolStatsKmcEventType.prototype.ACCOUNT_ACCESS_CONTROL_DELETE = 1039;
KontorolStatsKmcEventType.prototype.ACCOUNT_SAVE_TRANSCODING_SETTINGS = 1040;
KontorolStatsKmcEventType.prototype.LOGIN = 1041;
KontorolStatsKmcEventType.prototype.DASHBOARD_IMPORT_CONTENT = 1042;
KontorolStatsKmcEventType.prototype.DASHBOARD_UPDATE_CONTENT = 1043;
KontorolStatsKmcEventType.prototype.DASHBOARD_ACCOUNT_CONTACT_US = 1044;
KontorolStatsKmcEventType.prototype.DASHBOARD_VIEW_REPORTS = 1045;
KontorolStatsKmcEventType.prototype.DASHBOARD_EMBED_PLAYER = 1046;
KontorolStatsKmcEventType.prototype.DASHBOARD_EMBED_PLAYLIST = 1047;
KontorolStatsKmcEventType.prototype.DASHBOARD_CUSTOMIZE_PLAYERS = 1048;
KontorolStatsKmcEventType.prototype.APP_STUDIO_NEW_PLAYER_SINGLE_VIDEO = 1050;
KontorolStatsKmcEventType.prototype.APP_STUDIO_NEW_PLAYER_PLAYLIST = 1051;
KontorolStatsKmcEventType.prototype.APP_STUDIO_NEW_PLAYER_MULTI_TAB_PLAYLIST = 1052;
KontorolStatsKmcEventType.prototype.APP_STUDIO_EDIT_PLAYER_SINGLE_VIDEO = 1053;
KontorolStatsKmcEventType.prototype.APP_STUDIO_EDIT_PLAYER_PLAYLIST = 1054;
KontorolStatsKmcEventType.prototype.APP_STUDIO_EDIT_PLAYER_MULTI_TAB_PLAYLIST = 1055;
KontorolStatsKmcEventType.prototype.APP_STUDIO_DUPLICATE_PLAYER = 1056;
KontorolStatsKmcEventType.prototype.REPORTS_AND_ANALYTICS_BANDWIDTH_USAGE_TAB = 1070;
KontorolStatsKmcEventType.prototype.REPORTS_AND_ANALYTICS_CONTENT_REPORTS_TAB = 1071;
KontorolStatsKmcEventType.prototype.REPORTS_AND_ANALYTICS_USERS_AND_COMMUNITY_REPORTS_TAB = 1072;
KontorolStatsKmcEventType.prototype.REPORTS_AND_ANALYTICS_TOP_CONTRIBUTORS = 1073;
KontorolStatsKmcEventType.prototype.REPORTS_AND_ANALYTICS_MAP_OVERLAYS = 1074;
KontorolStatsKmcEventType.prototype.REPORTS_AND_ANALYTICS_TOP_SYNDICATIONS = 1075;
KontorolStatsKmcEventType.prototype.REPORTS_AND_ANALYTICS_TOP_CONTENT = 1076;
KontorolStatsKmcEventType.prototype.REPORTS_AND_ANALYTICS_CONTENT_DROPOFF = 1077;
KontorolStatsKmcEventType.prototype.REPORTS_AND_ANALYTICS_CONTENT_INTERACTIONS = 1078;
KontorolStatsKmcEventType.prototype.REPORTS_AND_ANALYTICS_CONTENT_CONTRIBUTIONS = 1079;
KontorolStatsKmcEventType.prototype.REPORTS_AND_ANALYTICS_VIDEO_DRILL_DOWN = 1080;
KontorolStatsKmcEventType.prototype.REPORTS_AND_ANALYTICS_CONTENT_DRILL_DOWN_INTERACTION = 1081;
KontorolStatsKmcEventType.prototype.REPORTS_AND_ANALYTICS_CONTENT_CONTRIBUTIONS_DRILLDOWN = 1082;
KontorolStatsKmcEventType.prototype.REPORTS_AND_ANALYTICS_VIDEO_DRILL_DOWN_DROPOFF = 1083;
KontorolStatsKmcEventType.prototype.REPORTS_AND_ANALYTICS_MAP_OVERLAYS_DRILLDOWN = 1084;
KontorolStatsKmcEventType.prototype.REPORTS_AND_ANALYTICS_TOP_SYNDICATIONS_DRILL_DOWN = 1085;
KontorolStatsKmcEventType.prototype.REPORTS_AND_ANALYTICS_BANDWIDTH_USAGE_VIEW_MONTHLY = 1086;
KontorolStatsKmcEventType.prototype.REPORTS_AND_ANALYTICS_BANDWIDTH_USAGE_VIEW_YEARLY = 1087;

function KontorolSyndicationFeedStatus()
{
}
KontorolSyndicationFeedStatus.prototype.DELETED = -1;
KontorolSyndicationFeedStatus.prototype.ACTIVE = 1;

function KontorolSyndicationFeedType()
{
}
KontorolSyndicationFeedType.prototype.GOOGLE_VIDEO = 1;
KontorolSyndicationFeedType.prototype.YAHOO = 2;
KontorolSyndicationFeedType.prototype.ITUNES = 3;
KontorolSyndicationFeedType.prototype.TUBE_MOGUL = 4;

function KontorolSystemUserOrderBy()
{
}
KontorolSystemUserOrderBy.prototype.ID_ASC = "+id";
KontorolSystemUserOrderBy.prototype.ID_DESC = "-id";
KontorolSystemUserOrderBy.prototype.STATUS_ASC = "+status";
KontorolSystemUserOrderBy.prototype.STATUS_DESC = "-status";

function KontorolSystemUserStatus()
{
}
KontorolSystemUserStatus.prototype.BLOCKED = 0;
KontorolSystemUserStatus.prototype.ACTIVE = 1;

function KontorolTubeMogulSyndicationFeedCategories()
{
}
KontorolTubeMogulSyndicationFeedCategories.prototype.ARTS_AND_ANIMATION = "Arts &amp; Animation";
KontorolTubeMogulSyndicationFeedCategories.prototype.COMEDY = "Comedy";
KontorolTubeMogulSyndicationFeedCategories.prototype.ENTERTAINMENT = "Entertainment";
KontorolTubeMogulSyndicationFeedCategories.prototype.MUSIC = "Music";
KontorolTubeMogulSyndicationFeedCategories.prototype.NEWS_AND_BLOGS = "News &amp; Blogs";
KontorolTubeMogulSyndicationFeedCategories.prototype.SCIENCE_AND_TECHNOLOGY = "Science &amp; Technology";
KontorolTubeMogulSyndicationFeedCategories.prototype.SPORTS = "Sports";
KontorolTubeMogulSyndicationFeedCategories.prototype.TRAVEL_AND_PLACES = "Travel &amp; Places";
KontorolTubeMogulSyndicationFeedCategories.prototype.VIDEO_GAMES = "Video Games";
KontorolTubeMogulSyndicationFeedCategories.prototype.ANIMALS_AND_PETS = "Animals &amp; Pets";
KontorolTubeMogulSyndicationFeedCategories.prototype.AUTOS = "Autos";
KontorolTubeMogulSyndicationFeedCategories.prototype.VLOGS_PEOPLE = "Vlogs &amp; People";
KontorolTubeMogulSyndicationFeedCategories.prototype.HOW_TO_INSTRUCTIONAL_DIY = "How To/Instructional/DIY";
KontorolTubeMogulSyndicationFeedCategories.prototype.COMMERCIALS_PROMOTIONAL = "Commercials/Promotional";
KontorolTubeMogulSyndicationFeedCategories.prototype.FAMILY_AND_KIDS = "Family &amp; Kids";

function KontorolTubeMogulSyndicationFeedOrderBy()
{
}
KontorolTubeMogulSyndicationFeedOrderBy.prototype.PLAYLIST_ID_ASC = "+playlistId";
KontorolTubeMogulSyndicationFeedOrderBy.prototype.PLAYLIST_ID_DESC = "-playlistId";
KontorolTubeMogulSyndicationFeedOrderBy.prototype.NAME_ASC = "+name";
KontorolTubeMogulSyndicationFeedOrderBy.prototype.NAME_DESC = "-name";
KontorolTubeMogulSyndicationFeedOrderBy.prototype.TYPE_ASC = "+type";
KontorolTubeMogulSyndicationFeedOrderBy.prototype.TYPE_DESC = "-type";
KontorolTubeMogulSyndicationFeedOrderBy.prototype.CREATED_AT_ASC = "+createdAt";
KontorolTubeMogulSyndicationFeedOrderBy.prototype.CREATED_AT_DESC = "-createdAt";

function KontorolUiConfCreationMode()
{
}
KontorolUiConfCreationMode.prototype.WIZARD = 2;
KontorolUiConfCreationMode.prototype.ADVANCED = 3;

function KontorolUiConfObjType()
{
}
KontorolUiConfObjType.prototype.PLAYER = 1;
KontorolUiConfObjType.prototype.CONTRIBUTION_WIZARD = 2;
KontorolUiConfObjType.prototype.SIMPLE_EDITOR = 3;
KontorolUiConfObjType.prototype.ADVANCED_EDITOR = 4;
KontorolUiConfObjType.prototype.PLAYLIST = 5;
KontorolUiConfObjType.prototype.APP_STUDIO = 6;

function KontorolUiConfOrderBy()
{
}
KontorolUiConfOrderBy.prototype.CREATED_AT_ASC = "+createdAt";
KontorolUiConfOrderBy.prototype.CREATED_AT_DESC = "-createdAt";

function KontorolUploadErrorCode()
{
}
KontorolUploadErrorCode.prototype.NO_ERROR = 0;
KontorolUploadErrorCode.prototype.GENERAL_ERROR = 1;
KontorolUploadErrorCode.prototype.PARTIAL_UPLOAD = 2;

function KontorolUserOrderBy()
{
}
KontorolUserOrderBy.prototype.CREATED_AT_ASC = "+createdAt";
KontorolUserOrderBy.prototype.CREATED_AT_DESC = "-createdAt";

function KontorolUserStatus()
{
}
KontorolUserStatus.prototype.BLOCKED = 0;
KontorolUserStatus.prototype.ACTIVE = 1;
KontorolUserStatus.prototype.DELETED = 2;

function KontorolVideoCodec()
{
}
KontorolVideoCodec.prototype.NONE = "";
KontorolVideoCodec.prototype.VP6 = "vp6";
KontorolVideoCodec.prototype.H263 = "h263";
KontorolVideoCodec.prototype.H264 = "h264";
KontorolVideoCodec.prototype.FLV = "flv";

function KontorolWidgetOrderBy()
{
}
KontorolWidgetOrderBy.prototype.CREATED_AT_ASC = "+createdAt";
KontorolWidgetOrderBy.prototype.CREATED_AT_DESC = "-createdAt";

function KontorolWidgetSecurityType()
{
}
KontorolWidgetSecurityType.prototype.NONE = 1;
KontorolWidgetSecurityType.prototype.TIMEHASH = 2;

function KontorolYahooSyndicationFeedAdultValues()
{
}
KontorolYahooSyndicationFeedAdultValues.prototype.ADULT = "adult";
KontorolYahooSyndicationFeedAdultValues.prototype.NON_ADULT = "nonadult";

function KontorolYahooSyndicationFeedCategories()
{
}
KontorolYahooSyndicationFeedCategories.prototype.ACTION = "Action";
KontorolYahooSyndicationFeedCategories.prototype.ART_AND_ANIMATION = "Art &amp; Animation";
KontorolYahooSyndicationFeedCategories.prototype.ENTERTAINMENT_AND_TV = "Entertainment &amp; TV";
KontorolYahooSyndicationFeedCategories.prototype.FOOD = "Food";
KontorolYahooSyndicationFeedCategories.prototype.GAMES = "Games";
KontorolYahooSyndicationFeedCategories.prototype.HOW_TO = "How-To";
KontorolYahooSyndicationFeedCategories.prototype.MUSIC = "Music";
KontorolYahooSyndicationFeedCategories.prototype.PEOPLE_AND_VLOGS = "People &amp; Vlogs";
KontorolYahooSyndicationFeedCategories.prototype.SCIENCE_AND_ENVIRONMENT = "Science &amp; Environment";
KontorolYahooSyndicationFeedCategories.prototype.TRANSPORTATION = "Transportation";
KontorolYahooSyndicationFeedCategories.prototype.ANIMALS = "Animals";
KontorolYahooSyndicationFeedCategories.prototype.COMMERCIALS = "Commercials";
KontorolYahooSyndicationFeedCategories.prototype.FAMILY = "Family";
KontorolYahooSyndicationFeedCategories.prototype.FUNNY_VIDEOS = "Funny Videos";
KontorolYahooSyndicationFeedCategories.prototype.HEALTH_AND_BEAUTY = "Health &amp; Beauty";
KontorolYahooSyndicationFeedCategories.prototype.MOVIES_AND_SHORTS = "Movies &amp; Shorts";
KontorolYahooSyndicationFeedCategories.prototype.NEWS_AND_POLITICS = "News &amp; Politics";
KontorolYahooSyndicationFeedCategories.prototype.PRODUCTS_AND_TECH = "Products &amp; Tech.";
KontorolYahooSyndicationFeedCategories.prototype.SPORTS = "Sports";
KontorolYahooSyndicationFeedCategories.prototype.TRAVEL = "Travel";

function KontorolYahooSyndicationFeedOrderBy()
{
}
KontorolYahooSyndicationFeedOrderBy.prototype.PLAYLIST_ID_ASC = "+playlistId";
KontorolYahooSyndicationFeedOrderBy.prototype.PLAYLIST_ID_DESC = "-playlistId";
KontorolYahooSyndicationFeedOrderBy.prototype.NAME_ASC = "+name";
KontorolYahooSyndicationFeedOrderBy.prototype.NAME_DESC = "-name";
KontorolYahooSyndicationFeedOrderBy.prototype.TYPE_ASC = "+type";
KontorolYahooSyndicationFeedOrderBy.prototype.TYPE_DESC = "-type";
KontorolYahooSyndicationFeedOrderBy.prototype.CREATED_AT_ASC = "+createdAt";
KontorolYahooSyndicationFeedOrderBy.prototype.CREATED_AT_DESC = "-createdAt";

function KontorolAccessControl()
{
}
KontorolAccessControl.prototype = new KontorolObjectBase();
/**
 * The id of the Access Control Profile
	 * 
 *
 * @var int
 * @readonly
 */
KontorolAccessControl.prototype.id = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolAccessControl.prototype.partnerId = null;

/**
 * The name of the Access Control Profile
	 * 
 *
 * @var string
 */
KontorolAccessControl.prototype.name = null;

/**
 * The description of the Access Control Profile
	 * 
 *
 * @var string
 */
KontorolAccessControl.prototype.description = null;

/**
 * Creation date as Unix timestamp (In seconds) 
	 * 
 *
 * @var int
 * @readonly
 */
KontorolAccessControl.prototype.createdAt = null;

/**
 * True if this Conversion Profile is the default
	 * 
 *
 * @var KontorolNullableBoolean
 */
KontorolAccessControl.prototype.isDefault = null;

/**
 * Array of Access Control Restrictions
	 * 
 *
 * @var KontorolRestrictionArray
 */
KontorolAccessControl.prototype.restrictions = null;


function KontorolFilter()
{
}
KontorolFilter.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var string
 */
KontorolFilter.prototype.orderBy = null;


function KontorolAccessControlFilter()
{
}
KontorolAccessControlFilter.prototype = new KontorolFilter();
/**
 * 
 *
 * @var int
 */
KontorolAccessControlFilter.prototype.idEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolAccessControlFilter.prototype.idIn = null;

/**
 * 
 *
 * @var int
 */
KontorolAccessControlFilter.prototype.createdAtGreaterThanOrEqual = null;

/**
 * 
 *
 * @var int
 */
KontorolAccessControlFilter.prototype.createdAtLessThanOrEqual = null;


function KontorolAccessControlListResponse()
{
}
KontorolAccessControlListResponse.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var KontorolAccessControlArray
 * @readonly
 */
KontorolAccessControlListResponse.prototype.objects = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolAccessControlListResponse.prototype.totalCount = null;


function KontorolAdminUser()
{
}
KontorolAdminUser.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var string
 * @readonly
 */
KontorolAdminUser.prototype.password = null;

/**
 * 
 *
 * @var string
 * @readonly
 */
KontorolAdminUser.prototype.email = null;

/**
 * 
 *
 * @var string
 */
KontorolAdminUser.prototype.screenName = null;


function KontorolBaseEntry()
{
}
KontorolBaseEntry.prototype = new KontorolObjectBase();
/**
 * Auto generated 10 characters alphanumeric string
	 * 
 *
 * @var string
 * @readonly
 */
KontorolBaseEntry.prototype.id = null;

/**
 * Entry name (Min 1 chars)
	 * 
 *
 * @var string
 */
KontorolBaseEntry.prototype.name = null;

/**
 * Entry description
	 * 
 *
 * @var string
 */
KontorolBaseEntry.prototype.description = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolBaseEntry.prototype.partnerId = null;

/**
 * The ID of the user who is the owner of this entry 
	 * 
 *
 * @var string
 */
KontorolBaseEntry.prototype.userId = null;

/**
 * Entry tags
	 * 
 *
 * @var string
 */
KontorolBaseEntry.prototype.tags = null;

/**
 * Entry admin tags can be updated only by administrators
	 * 
 *
 * @var string
 */
KontorolBaseEntry.prototype.adminTags = null;

/**
 * 
 *
 * @var string
 */
KontorolBaseEntry.prototype.categories = null;

/**
 * 
 *
 * @var KontorolEntryStatus
 * @readonly
 */
KontorolBaseEntry.prototype.status = null;

/**
 * Entry moderation status
	 * 
 *
 * @var KontorolEntryModerationStatus
 * @readonly
 */
KontorolBaseEntry.prototype.moderationStatus = null;

/**
 * Number of moderation requests waiting for this entry
	 * 
 *
 * @var int
 * @readonly
 */
KontorolBaseEntry.prototype.moderationCount = null;

/**
 * The type of the entry, this is auto filled by the derived entry object
	 * 
 *
 * @var KontorolEntryType
 * @readonly
 */
KontorolBaseEntry.prototype.type = null;

/**
 * Entry creation date as Unix timestamp (In seconds)
	 * 
 *
 * @var int
 * @readonly
 */
KontorolBaseEntry.prototype.createdAt = null;

/**
 * Calculated rank
	 * 
 *
 * @var float
 * @readonly
 */
KontorolBaseEntry.prototype.rank = null;

/**
 * The total (sum) of all votes
	 * 
 *
 * @var int
 * @readonly
 */
KontorolBaseEntry.prototype.totalRank = null;

/**
 * Number of votes
	 * 
 *
 * @var int
 * @readonly
 */
KontorolBaseEntry.prototype.votes = null;

/**
 * 
 *
 * @var int
 */
KontorolBaseEntry.prototype.groupId = null;

/**
 * Can be used to store various partner related data as a string 
	 * 
 *
 * @var string
 */
KontorolBaseEntry.prototype.partnerData = null;

/**
 * Download URL for the entry
	 * 
 *
 * @var string
 * @readonly
 */
KontorolBaseEntry.prototype.downloadUrl = null;

/**
 * Indexed search text for full text search
 *
 * @var string
 * @readonly
 */
KontorolBaseEntry.prototype.searchText = null;

/**
 * License type used for this entry
	 * 
 *
 * @var KontorolLicenseType
 */
KontorolBaseEntry.prototype.licenseType = null;

/**
 * Version of the entry data
 *
 * @var int
 * @readonly
 */
KontorolBaseEntry.prototype.version = null;

/**
 * Thumbnail URL
	 * 
 *
 * @var string
 * @readonly
 */
KontorolBaseEntry.prototype.thumbnailUrl = null;

/**
 * The Access Control ID assigned to this entry (null when not set, send -1 to remove)  
	 * 
 *
 * @var int
 */
KontorolBaseEntry.prototype.accessControlId = null;

/**
 * Entry scheduling start date (null when not set, send -1 to remove)
	 * 
 *
 * @var int
 */
KontorolBaseEntry.prototype.startDate = null;

/**
 * Entry scheduling end date (null when not set, send -1 to remove)
	 * 
 *
 * @var int
 */
KontorolBaseEntry.prototype.endDate = null;


function KontorolBaseEntryFilter()
{
}
KontorolBaseEntryFilter.prototype = new KontorolFilter();
/**
 * This filter should be in use for retrieving only a specific entry (identified by its entryId).
	 * @var string
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.idEqual = null;

/**
 * This filter should be in use for retrieving few specific entries (string should include comma separated list of entryId strings).
	 * @var string
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.idIn = null;

/**
 * This filter should be in use for retrieving specific entries while applying an SQL 'LIKE' pattern matching on entry names. It should include only one pattern for matching entry names against.
	 * @var string
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.nameLike = null;

/**
 * This filter should be in use for retrieving specific entries, while applying an SQL 'LIKE' pattern matching on entry names. It could include few (comma separated) patterns for matching entry names against, while applying an OR logic to retrieve entries that match at least one input pattern.
	 * @var string
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.nameMultiLikeOr = null;

/**
 * This filter should be in use for retrieving specific entries, while applying an SQL 'LIKE' pattern matching on entry names. It could include few (comma separated) patterns for matching entry names against, while applying an AND logic to retrieve entries that match all input patterns.
	 * @var string
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.nameMultiLikeAnd = null;

/**
 * This filter should be in use for retrieving entries with a specific name.
	 * @var string
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.nameEqual = null;

/**
 * This filter should be in use for retrieving only entries which were uploaded by/assigned to users of a specific Kontorol Partner (identified by Partner ID).
	 * @var int
 *
 * @var int
 */
KontorolBaseEntryFilter.prototype.partnerIdEqual = null;

/**
 * This filter should be in use for retrieving only entries within Kontorol network which were uploaded by/assigned to users of few Kontorol Partners  (string should include comma separated list of PartnerIDs)
	 * @var string
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.partnerIdIn = null;

/**
 * This filter parameter should be in use for retrieving only entries, uploaded by/assigned to a specific user (identified by user Id).
	 * @var string
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.userIdEqual = null;

/**
 * This filter should be in use for retrieving specific entries while applying an SQL 'LIKE' pattern matching on entry tags. It should include only one pattern for matching entry tags against.
	 * @var string
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.tagsLike = null;

/**
 * This filter should be in use for retrieving specific entries, while applying an SQL 'LIKE' pattern matching on tags.  It could include few (comma separated) patterns for matching entry tags against, while applying an OR logic to retrieve entries that match at least one input pattern.
	 * @var string
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.tagsMultiLikeOr = null;

/**
 * This filter should be in use for retrieving specific entries, while applying an SQL 'LIKE' pattern matching on tags.  It could include few (comma separated) patterns for matching entry tags against, while applying an AND logic to retrieve entries that match all input patterns.
	 * @var string
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.tagsMultiLikeAnd = null;

/**
 * This filter should be in use for retrieving specific entries while applying an SQL 'LIKE' pattern matching on entry tags, set by an ADMIN user. It should include only one pattern for matching entry tags against.
	 * @var string
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.adminTagsLike = null;

/**
 * This filter should be in use for retrieving specific entries, while applying an SQL 'LIKE' pattern matching on tags, set by an ADMIN user.  It could include few (comma separated) patterns for matching entry tags against, while applying an OR logic to retrieve entries that match at least one input pattern.
	 * @var string
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.adminTagsMultiLikeOr = null;

/**
 * This filter should be in use for retrieving specific entries, while applying an SQL 'LIKE' pattern matching on tags, set by an ADMIN user.  It could include few (comma separated) patterns for matching entry tags against, while applying an AND logic to retrieve entries that match all input patterns.
	 * @var string
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.adminTagsMultiLikeAnd = null;

/**
 * 
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.categoriesMatchAnd = null;

/**
 * 
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.categoriesMatchOr = null;

/**
 * This filter should be in use for retrieving only entries, at a specific {@link ?object=KontorolEntryStatus KontorolEntryStatus}.
	 * @var KontorolEntryStatus
 *
 * @var KontorolEntryStatus
 */
KontorolBaseEntryFilter.prototype.statusEqual = null;

/**
 * This filter should be in use for retrieving only entries, not at a specific {@link ?object=KontorolEntryStatus KontorolEntryStatus}.
	 * @var KontorolEntryStatus
 *
 * @var KontorolEntryStatus
 */
KontorolBaseEntryFilter.prototype.statusNotEqual = null;

/**
 * This filter should be in use for retrieving only entries, at few specific {@link ?object=KontorolEntryStatus KontorolEntryStatus} (comma separated).
	 * @var string
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.statusIn = null;

/**
 * This filter should be in use for retrieving only entries, not at few specific {@link ?object=KontorolEntryStatus KontorolEntryStatus} (comma separated).
	 * @var KontorolEntryStatus
 *
 * @var KontorolEntryStatus
 */
KontorolBaseEntryFilter.prototype.statusNotIn = null;

/**
 * 
 *
 * @var KontorolEntryModerationStatus
 */
KontorolBaseEntryFilter.prototype.moderationStatusEqual = null;

/**
 * 
 *
 * @var KontorolEntryModerationStatus
 */
KontorolBaseEntryFilter.prototype.moderationStatusNotEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.moderationStatusIn = null;

/**
 * 
 *
 * @var KontorolEntryModerationStatus
 */
KontorolBaseEntryFilter.prototype.moderationStatusNotIn = null;

/**
 * 
 *
 * @var KontorolEntryType
 */
KontorolBaseEntryFilter.prototype.typeEqual = null;

/**
 * This filter should be in use for retrieving entries of few {@link ?object=KontorolEntryType KontorolEntryType} (string should include a comma separated list of {@link ?object=KontorolEntryType KontorolEntryType} enumerated parameters).
	 * @var string
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.typeIn = null;

/**
 * This filter parameter should be in use for retrieving only entries which were created at Kontorol system after a specific time/date (standard timestamp format).
	 * @var int
 *
 * @var int
 */
KontorolBaseEntryFilter.prototype.createdAtGreaterThanOrEqual = null;

/**
 * This filter parameter should be in use for retrieving only entries which were created at Kontorol system before a specific time/date (standard timestamp format).
	 * @var int
 *
 * @var int
 */
KontorolBaseEntryFilter.prototype.createdAtLessThanOrEqual = null;

/**
 * 
 *
 * @var int
 */
KontorolBaseEntryFilter.prototype.groupIdEqual = null;

/**
 * This filter should be in use for retrieving specific entries while search match the input string within all of the following metadata attributes: name, description, tags, adminTags.
	 * @var string
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.searchTextMatchAnd = null;

/**
 * This filter should be in use for retrieving specific entries while search match the input string within at least one of the following metadata attributes: name, description, tags, adminTags.
	 * @var string
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.searchTextMatchOr = null;

/**
 * 
 *
 * @var int
 */
KontorolBaseEntryFilter.prototype.accessControlIdEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.accessControlIdIn = null;

/**
 * 
 *
 * @var int
 */
KontorolBaseEntryFilter.prototype.startDateGreaterThanOrEqual = null;

/**
 * 
 *
 * @var int
 */
KontorolBaseEntryFilter.prototype.startDateLessThanOrEqual = null;

/**
 * 
 *
 * @var int
 */
KontorolBaseEntryFilter.prototype.startDateGreaterThanOrEqualOrNull = null;

/**
 * 
 *
 * @var int
 */
KontorolBaseEntryFilter.prototype.startDateLessThanOrEqualOrNull = null;

/**
 * 
 *
 * @var int
 */
KontorolBaseEntryFilter.prototype.endDateGreaterThanOrEqual = null;

/**
 * 
 *
 * @var int
 */
KontorolBaseEntryFilter.prototype.endDateLessThanOrEqual = null;

/**
 * 
 *
 * @var int
 */
KontorolBaseEntryFilter.prototype.endDateGreaterThanOrEqualOrNull = null;

/**
 * 
 *
 * @var int
 */
KontorolBaseEntryFilter.prototype.endDateLessThanOrEqualOrNull = null;

/**
 * 
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.tagsNameMultiLikeOr = null;

/**
 * 
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.tagsAdminTagsMultiLikeOr = null;

/**
 * 
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.tagsAdminTagsNameMultiLikeOr = null;

/**
 * 
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.tagsNameMultiLikeAnd = null;

/**
 * 
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.tagsAdminTagsMultiLikeAnd = null;

/**
 * 
 *
 * @var string
 */
KontorolBaseEntryFilter.prototype.tagsAdminTagsNameMultiLikeAnd = null;


function KontorolBaseEntryListResponse()
{
}
KontorolBaseEntryListResponse.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var KontorolBaseEntryArray
 * @readonly
 */
KontorolBaseEntryListResponse.prototype.objects = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolBaseEntryListResponse.prototype.totalCount = null;


function KontorolBaseJob()
{
}
KontorolBaseJob.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolBaseJob.prototype.id = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolBaseJob.prototype.partnerId = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolBaseJob.prototype.createdAt = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolBaseJob.prototype.updatedAt = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolBaseJob.prototype.processorExpiration = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolBaseJob.prototype.executionAttempts = null;


function KontorolBaseJobFilter()
{
}
KontorolBaseJobFilter.prototype = new KontorolFilter();
/**
 * 
 *
 * @var int
 */
KontorolBaseJobFilter.prototype.idEqual = null;

/**
 * 
 *
 * @var int
 */
KontorolBaseJobFilter.prototype.idGreaterThanOrEqual = null;

/**
 * 
 *
 * @var int
 */
KontorolBaseJobFilter.prototype.partnerIdEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolBaseJobFilter.prototype.partnerIdIn = null;

/**
 * 
 *
 * @var int
 */
KontorolBaseJobFilter.prototype.createdAtGreaterThanOrEqual = null;

/**
 * 
 *
 * @var int
 */
KontorolBaseJobFilter.prototype.createdAtLessThanOrEqual = null;


function KontorolBaseRestriction()
{
}
KontorolBaseRestriction.prototype = new KontorolObjectBase();

function KontorolBaseSyndicationFeed()
{
}
KontorolBaseSyndicationFeed.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var string
 * @readonly
 */
KontorolBaseSyndicationFeed.prototype.id = null;

/**
 * 
 *
 * @var string
 * @readonly
 */
KontorolBaseSyndicationFeed.prototype.feedUrl = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolBaseSyndicationFeed.prototype.partnerId = null;

/**
 * link a playlist that will set what content the feed will include
	 * if empty, all content will be included in feed
	 * 
 *
 * @var string
 */
KontorolBaseSyndicationFeed.prototype.playlistId = null;

/**
 * feed name
	 * 
 *
 * @var string
 */
KontorolBaseSyndicationFeed.prototype.name = null;

/**
 * feed status
	 * 
 *
 * @var KontorolSyndicationFeedStatus
 * @readonly
 */
KontorolBaseSyndicationFeed.prototype.status = null;

/**
 * feed type
	 * 
 *
 * @var KontorolSyndicationFeedType
 * @readonly
 */
KontorolBaseSyndicationFeed.prototype.type = null;

/**
 * Base URL for each video, on the partners site
	 * This is required by all syndication types.
 *
 * @var string
 */
KontorolBaseSyndicationFeed.prototype.landingPage = null;

/**
 * Creation date as Unix timestamp (In seconds)
	 * 
 *
 * @var int
 * @readonly
 */
KontorolBaseSyndicationFeed.prototype.createdAt = null;

/**
 * allow_embed tells google OR yahoo weather to allow embedding the video on google OR yahoo video results
	 * or just to provide a link to the landing page.
	 * it is applied on the video-player_loc property in the XML (google)
	 * and addes media-player tag (yahoo)
 *
 * @var bool
 */
KontorolBaseSyndicationFeed.prototype.allowEmbed = null;

/**
 * Select a uiconf ID as player skin to include in the kwidget url
 *
 * @var int
 */
KontorolBaseSyndicationFeed.prototype.playerUiconfId = null;

/**
 * 
 *
 * @var int
 */
KontorolBaseSyndicationFeed.prototype.flavorParamId = null;

/**
 * 
 *
 * @var bool
 */
KontorolBaseSyndicationFeed.prototype.transcodeExistingContent = null;

/**
 * 
 *
 * @var bool
 */
KontorolBaseSyndicationFeed.prototype.addToDefaultConversionProfile = null;

/**
 * 
 *
 * @var string
 */
KontorolBaseSyndicationFeed.prototype.categories = null;


function KontorolBaseSyndicationFeedFilter()
{
}
KontorolBaseSyndicationFeedFilter.prototype = new KontorolFilter();

function KontorolBaseSyndicationFeedListResponse()
{
}
KontorolBaseSyndicationFeedListResponse.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var KontorolBaseSyndicationFeedArray
 * @readonly
 */
KontorolBaseSyndicationFeedListResponse.prototype.objects = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolBaseSyndicationFeedListResponse.prototype.totalCount = null;


function KontorolBatchJob()
{
}
KontorolBatchJob.prototype = new KontorolBaseJob();
/**
 * 
 *
 * @var string
 */
KontorolBatchJob.prototype.entryId = null;

/**
 * 
 *
 * @var KontorolBatchJobType
 * @readonly
 */
KontorolBatchJob.prototype.jobType = null;

/**
 * 
 *
 * @var int
 */
KontorolBatchJob.prototype.jobSubType = null;

/**
 * 
 *
 * @var KontorolJobData
 */
KontorolBatchJob.prototype.data = null;

/**
 * 
 *
 * @var KontorolBatchJobStatus
 */
KontorolBatchJob.prototype.status = null;

/**
 * 
 *
 * @var int
 */
KontorolBatchJob.prototype.abort = null;

/**
 * 
 *
 * @var int
 */
KontorolBatchJob.prototype.checkAgainTimeout = null;

/**
 * 
 *
 * @var int
 */
KontorolBatchJob.prototype.progress = null;

/**
 * 
 *
 * @var string
 */
KontorolBatchJob.prototype.message = null;

/**
 * 
 *
 * @var string
 */
KontorolBatchJob.prototype.description = null;

/**
 * 
 *
 * @var int
 */
KontorolBatchJob.prototype.updatesCount = null;

/**
 * 
 *
 * @var int
 */
KontorolBatchJob.prototype.priority = null;

/**
 * 
 *
 * @var int
 */
KontorolBatchJob.prototype.workGroupId = null;

/**
 * The id of the bulk upload job that initiated this job
 *
 * @var int
 */
KontorolBatchJob.prototype.bulkJobId = null;

/**
 * When one job creates another - the parent should set this parentJobId to be its own id.
 *
 * @var int
 */
KontorolBatchJob.prototype.parentJobId = null;

/**
 * The id of the root parent job
 *
 * @var int
 */
KontorolBatchJob.prototype.rootJobId = null;

/**
 * The time that the job was pulled from the queue
 *
 * @var int
 */
KontorolBatchJob.prototype.queueTime = null;

/**
 * The time that the job was finished or closed as failed
 *
 * @var int
 */
KontorolBatchJob.prototype.finishTime = null;

/**
 * 
 *
 * @var KontorolBatchJobErrorTypes
 */
KontorolBatchJob.prototype.errType = null;

/**
 * 
 *
 * @var int
 */
KontorolBatchJob.prototype.errNumber = null;

/**
 * 
 *
 * @var int
 */
KontorolBatchJob.prototype.fileSize = null;

/**
 * 
 *
 * @var bool
 */
KontorolBatchJob.prototype.lastWorkerRemote = null;


function KontorolBatchJobFilter()
{
}
KontorolBatchJobFilter.prototype = new KontorolBaseJobFilter();
/**
 * 
 *
 * @var string
 */
KontorolBatchJobFilter.prototype.entryIdEqual = null;

/**
 * 
 *
 * @var KontorolBatchJobType
 */
KontorolBatchJobFilter.prototype.jobTypeEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolBatchJobFilter.prototype.jobTypeIn = null;

/**
 * 
 *
 * @var int
 */
KontorolBatchJobFilter.prototype.jobSubTypeEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolBatchJobFilter.prototype.jobSubTypeIn = null;

/**
 * 
 *
 * @var KontorolBatchJobStatus
 */
KontorolBatchJobFilter.prototype.statusEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolBatchJobFilter.prototype.statusIn = null;

/**
 * 
 *
 * @var string
 */
KontorolBatchJobFilter.prototype.workGroupIdIn = null;

/**
 * 
 *
 * @var int
 */
KontorolBatchJobFilter.prototype.queueTimeGreaterThanOrEqual = null;

/**
 * 
 *
 * @var int
 */
KontorolBatchJobFilter.prototype.queueTimeLessThanOrEqual = null;

/**
 * 
 *
 * @var int
 */
KontorolBatchJobFilter.prototype.finishTimeGreaterThanOrEqual = null;

/**
 * 
 *
 * @var int
 */
KontorolBatchJobFilter.prototype.finishTimeLessThanOrEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolBatchJobFilter.prototype.errTypeIn = null;

/**
 * 
 *
 * @var int
 */
KontorolBatchJobFilter.prototype.fileSizeLessThan = null;

/**
 * 
 *
 * @var int
 */
KontorolBatchJobFilter.prototype.fileSizeGreaterThan = null;


function KontorolBatchJobListResponse()
{
}
KontorolBatchJobListResponse.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var KontorolBatchJobArray
 * @readonly
 */
KontorolBatchJobListResponse.prototype.objects = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolBatchJobListResponse.prototype.totalCount = null;


function KontorolBatchJobResponse()
{
}
KontorolBatchJobResponse.prototype = new KontorolObjectBase();
/**
 * The main batch job
	 * 
 *
 * @var KontorolBatchJob
 */
KontorolBatchJobResponse.prototype.batchJob = null;

/**
 * All batch jobs that reference the main job as root
	 * 
 *
 * @var KontorolBatchJobArray
 */
KontorolBatchJobResponse.prototype.childBatchJobs = null;


function KontorolJobData()
{
}
KontorolJobData.prototype = new KontorolObjectBase();

function KontorolBulkDownloadJobData()
{
}
KontorolBulkDownloadJobData.prototype = new KontorolJobData();
/**
 * Comma separated list of entry ids
	 * 
 *
 * @var string
 */
KontorolBulkDownloadJobData.prototype.entryIds = null;

/**
 * Flavor params id to use for conversion
	 * 
 *
 * @var int
 */
KontorolBulkDownloadJobData.prototype.flavorParamsId = null;

/**
 * The id of the requesting user
	 * 
 *
 * @var string
 */
KontorolBulkDownloadJobData.prototype.puserId = null;


function KontorolBulkUpload()
{
}
KontorolBulkUpload.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var int
 */
KontorolBulkUpload.prototype.id = null;

/**
 * 
 *
 * @var string
 */
KontorolBulkUpload.prototype.uploadedBy = null;

/**
 * 
 *
 * @var int
 */
KontorolBulkUpload.prototype.uploadedOn = null;

/**
 * 
 *
 * @var int
 */
KontorolBulkUpload.prototype.numOfEntries = null;

/**
 * 
 *
 * @var KontorolBatchJobStatus
 */
KontorolBulkUpload.prototype.status = null;

/**
 * 
 *
 * @var string
 */
KontorolBulkUpload.prototype.logFileUrl = null;

/**
 * 
 *
 * @var string
 */
KontorolBulkUpload.prototype.csvFileUrl = null;

/**
 * 
 *
 * @var KontorolBulkUploadResultArray
 */
KontorolBulkUpload.prototype.results = null;


function KontorolBulkUploadJobData()
{
}
KontorolBulkUploadJobData.prototype = new KontorolJobData();
/**
 * 
 *
 * @var int
 */
KontorolBulkUploadJobData.prototype.userId = null;

/**
 * The screen name of the user
	 * 
 *
 * @var string
 */
KontorolBulkUploadJobData.prototype.uploadedBy = null;

/**
 * Selected profile id for all bulk entries
	 * 
 *
 * @var int
 */
KontorolBulkUploadJobData.prototype.conversionProfileId = null;

/**
 * Created by the API
	 * 
 *
 * @var string
 */
KontorolBulkUploadJobData.prototype.csvFilePath = null;

/**
 * Created by the API
	 * 
 *
 * @var string
 */
KontorolBulkUploadJobData.prototype.resultsFileLocalPath = null;

/**
 * Created by the API
	 * 
 *
 * @var string
 */
KontorolBulkUploadJobData.prototype.resultsFileUrl = null;

/**
 * Number of created entries
	 * 
 *
 * @var int
 */
KontorolBulkUploadJobData.prototype.numOfEntries = null;

/**
 * The version of the csv file
	 * 
 *
 * @var KontorolBulkUploadCsvVersion
 */
KontorolBulkUploadJobData.prototype.csvVersion = null;


function KontorolBulkUploadListResponse()
{
}
KontorolBulkUploadListResponse.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var KontorolBulkUploads
 * @readonly
 */
KontorolBulkUploadListResponse.prototype.objects = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolBulkUploadListResponse.prototype.totalCount = null;


function KontorolBulkUploadResult()
{
}
KontorolBulkUploadResult.prototype = new KontorolObjectBase();
/**
 * The id of the result
	 * 
 *
 * @var int
 * @readonly
 */
KontorolBulkUploadResult.prototype.id = null;

/**
 * The id of the parent job
	 * 
 *
 * @var int
 */
KontorolBulkUploadResult.prototype.bulkUploadJobId = null;

/**
 * The index of the line in the CSV
	 * 
 *
 * @var int
 */
KontorolBulkUploadResult.prototype.lineIndex = null;

/**
 * 
 *
 * @var int
 */
KontorolBulkUploadResult.prototype.partnerId = null;

/**
 * 
 *
 * @var string
 */
KontorolBulkUploadResult.prototype.entryId = null;

/**
 * 
 *
 * @var int
 */
KontorolBulkUploadResult.prototype.entryStatus = null;

/**
 * The data as recieved in the csv
	 * 
 *
 * @var string
 */
KontorolBulkUploadResult.prototype.rowData = null;

/**
 * 
 *
 * @var string
 */
KontorolBulkUploadResult.prototype.title = null;

/**
 * 
 *
 * @var string
 */
KontorolBulkUploadResult.prototype.description = null;

/**
 * 
 *
 * @var string
 */
KontorolBulkUploadResult.prototype.tags = null;

/**
 * 
 *
 * @var string
 */
KontorolBulkUploadResult.prototype.url = null;

/**
 * 
 *
 * @var string
 */
KontorolBulkUploadResult.prototype.contentType = null;

/**
 * 
 *
 * @var int
 */
KontorolBulkUploadResult.prototype.conversionProfileId = null;

/**
 * 
 *
 * @var int
 */
KontorolBulkUploadResult.prototype.accessControlProfileId = null;

/**
 * 
 *
 * @var string
 */
KontorolBulkUploadResult.prototype.category = null;

/**
 * 
 *
 * @var int
 */
KontorolBulkUploadResult.prototype.scheduleStartDate = null;

/**
 * 
 *
 * @var int
 */
KontorolBulkUploadResult.prototype.scheduleEndDate = null;

/**
 * 
 *
 * @var string
 */
KontorolBulkUploadResult.prototype.thumbnailUrl = null;

/**
 * 
 *
 * @var bool
 */
KontorolBulkUploadResult.prototype.thumbnailSaved = null;

/**
 * 
 *
 * @var string
 */
KontorolBulkUploadResult.prototype.partnerData = null;

/**
 * 
 *
 * @var string
 */
KontorolBulkUploadResult.prototype.errorDescription = null;


function KontorolCEError()
{
}
KontorolCEError.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var string
 * @readonly
 */
KontorolCEError.prototype.id = null;

/**
 * 
 *
 * @var int
 */
KontorolCEError.prototype.partnerId = null;

/**
 * 
 *
 * @var string
 */
KontorolCEError.prototype.browser = null;

/**
 * 
 *
 * @var string
 */
KontorolCEError.prototype.serverIp = null;

/**
 * 
 *
 * @var string
 */
KontorolCEError.prototype.serverOs = null;

/**
 * 
 *
 * @var string
 */
KontorolCEError.prototype.phpVersion = null;

/**
 * 
 *
 * @var string
 */
KontorolCEError.prototype.ceAdminEmail = null;

/**
 * 
 *
 * @var string
 */
KontorolCEError.prototype.type = null;

/**
 * 
 *
 * @var string
 */
KontorolCEError.prototype.description = null;

/**
 * 
 *
 * @var string
 */
KontorolCEError.prototype.data = null;


function KontorolCategory()
{
}
KontorolCategory.prototype = new KontorolObjectBase();
/**
 * The id of the Category
	 * 
 *
 * @var int
 * @readonly
 */
KontorolCategory.prototype.id = null;

/**
 * 
 *
 * @var int
 */
KontorolCategory.prototype.parentId = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolCategory.prototype.depth = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolCategory.prototype.partnerId = null;

/**
 * The name of the Category. 
	 * The following characters are not allowed: '<', '>', ','
	 * 
 *
 * @var string
 */
KontorolCategory.prototype.name = null;

/**
 * The full name of the Category
	 * 
 *
 * @var string
 * @readonly
 */
KontorolCategory.prototype.fullName = null;

/**
 * Number of entries in this Category (including child categories)
	 * 
 *
 * @var int
 * @readonly
 */
KontorolCategory.prototype.entriesCount = null;

/**
 * Creation date as Unix timestamp (In seconds)
	 * 
 *
 * @var int
 * @readonly
 */
KontorolCategory.prototype.createdAt = null;


function KontorolCategoryFilter()
{
}
KontorolCategoryFilter.prototype = new KontorolFilter();
/**
 * 
 *
 * @var int
 */
KontorolCategoryFilter.prototype.idEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolCategoryFilter.prototype.idIn = null;

/**
 * 
 *
 * @var int
 */
KontorolCategoryFilter.prototype.parentIdEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolCategoryFilter.prototype.parentIdIn = null;

/**
 * 
 *
 * @var int
 */
KontorolCategoryFilter.prototype.depthEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolCategoryFilter.prototype.fullNameEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolCategoryFilter.prototype.fullNameStartsWith = null;


function KontorolCategoryListResponse()
{
}
KontorolCategoryListResponse.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var KontorolCategoryArray
 * @readonly
 */
KontorolCategoryListResponse.prototype.objects = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolCategoryListResponse.prototype.totalCount = null;


function KontorolClientNotification()
{
}
KontorolClientNotification.prototype = new KontorolObjectBase();
/**
 * The URL where the notification should be sent to 
 *
 * @var string
 */
KontorolClientNotification.prototype.url = null;

/**
 * The serialized notification data to send
 *
 * @var string
 */
KontorolClientNotification.prototype.data = null;


function KontorolConvartableJobData()
{
}
KontorolConvartableJobData.prototype = new KontorolJobData();
/**
 * 
 *
 * @var string
 */
KontorolConvartableJobData.prototype.srcFileSyncLocalPath = null;

/**
 * 
 *
 * @var string
 */
KontorolConvartableJobData.prototype.srcFileSyncRemoteUrl = null;

/**
 * 
 *
 * @var int
 */
KontorolConvartableJobData.prototype.flavorParamsOutputId = null;

/**
 * 
 *
 * @var KontorolFlavorParamsOutput
 */
KontorolConvartableJobData.prototype.flavorParamsOutput = null;

/**
 * 
 *
 * @var int
 */
KontorolConvartableJobData.prototype.mediaInfoId = null;


function KontorolConversionProfile()
{
}
KontorolConversionProfile.prototype = new KontorolObjectBase();
/**
 * The id of the Conversion Profile
	 * 
 *
 * @var int
 * @readonly
 */
KontorolConversionProfile.prototype.id = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolConversionProfile.prototype.partnerId = null;

/**
 * The name of the Conversion Profile
	 * 
 *
 * @var string
 */
KontorolConversionProfile.prototype.name = null;

/**
 * The description of the Conversion Profile
	 * 
 *
 * @var string
 */
KontorolConversionProfile.prototype.description = null;

/**
 * Creation date as Unix timestamp (In seconds) 
	 * 
 *
 * @var int
 * @readonly
 */
KontorolConversionProfile.prototype.createdAt = null;

/**
 * List of included flavor ids (comma separated)
	 * 
 *
 * @var string
 */
KontorolConversionProfile.prototype.flavorParamsIds = null;

/**
 * True if this Conversion Profile is the default
	 * 
 *
 * @var KontorolNullableBoolean
 */
KontorolConversionProfile.prototype.isDefault = null;

/**
 * Cropping dimensions
	 * 
 *
 * @var KontorolCropDimensions
 */
KontorolConversionProfile.prototype.cropDimensions = null;

/**
 * Clipping start position (in miliseconds)
	 * 
 *
 * @var int
 */
KontorolConversionProfile.prototype.clipStart = null;

/**
 * Clipping duration (in miliseconds)
	 * 
 *
 * @var int
 */
KontorolConversionProfile.prototype.clipDuration = null;


function KontorolConversionProfileFilter()
{
}
KontorolConversionProfileFilter.prototype = new KontorolFilter();
/**
 * 
 *
 * @var int
 */
KontorolConversionProfileFilter.prototype.idEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolConversionProfileFilter.prototype.idIn = null;


function KontorolConversionProfileListResponse()
{
}
KontorolConversionProfileListResponse.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var KontorolConversionProfileArray
 * @readonly
 */
KontorolConversionProfileListResponse.prototype.objects = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolConversionProfileListResponse.prototype.totalCount = null;


function KontorolConvertJobData()
{
}
KontorolConvertJobData.prototype = new KontorolConvartableJobData();
/**
 * 
 *
 * @var string
 */
KontorolConvertJobData.prototype.destFileSyncLocalPath = null;

/**
 * 
 *
 * @var string
 */
KontorolConvertJobData.prototype.destFileSyncRemoteUrl = null;

/**
 * 
 *
 * @var string
 */
KontorolConvertJobData.prototype.logFileSyncLocalPath = null;

/**
 * 
 *
 * @var string
 */
KontorolConvertJobData.prototype.flavorAssetId = null;

/**
 * 
 *
 * @var string
 */
KontorolConvertJobData.prototype.remoteMediaId = null;


function KontorolConvertProfileJobData()
{
}
KontorolConvertProfileJobData.prototype = new KontorolJobData();
/**
 * 
 *
 * @var string
 */
KontorolConvertProfileJobData.prototype.inputFileSyncLocalPath = null;

/**
 * The height of last created thumbnail, will be used to comapare if this thumbnail is the best we can have
	 * 
 *
 * @var int
 */
KontorolConvertProfileJobData.prototype.thumbHeight = null;

/**
 * The bit rate of last created thumbnail, will be used to comapare if this thumbnail is the best we can have
	 * 
 *
 * @var int
 */
KontorolConvertProfileJobData.prototype.thumbBitrate = null;


function KontorolCountryRestriction()
{
}
KontorolCountryRestriction.prototype = new KontorolBaseRestriction();
/**
 * Country restriction type (Allow or deny)
	 * 
 *
 * @var KontorolCountryRestrictionType
 */
KontorolCountryRestriction.prototype.countryRestrictionType = null;

/**
 * Comma separated list of country codes to allow to deny 
	 * 
 *
 * @var string
 */
KontorolCountryRestriction.prototype.countryList = null;


function KontorolCropDimensions()
{
}
KontorolCropDimensions.prototype = new KontorolObjectBase();
/**
 * Crop left point
	 * 
 *
 * @var int
 */
KontorolCropDimensions.prototype.left = null;

/**
 * Crop top point
	 * 
 *
 * @var int
 */
KontorolCropDimensions.prototype.top = null;

/**
 * Crop width
	 * 
 *
 * @var int
 */
KontorolCropDimensions.prototype.width = null;

/**
 * Crop height
	 * 
 *
 * @var int
 */
KontorolCropDimensions.prototype.height = null;


function KontorolDataEntry()
{
}
KontorolDataEntry.prototype = new KontorolBaseEntry();
/**
 * The data of the entry
 *
 * @var string
 */
KontorolDataEntry.prototype.dataContent = null;


function KontorolDataEntryFilter()
{
}
KontorolDataEntryFilter.prototype = new KontorolBaseEntryFilter();

function KontorolDataListResponse()
{
}
KontorolDataListResponse.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var KontorolDataEntryArray
 * @readonly
 */
KontorolDataListResponse.prototype.objects = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolDataListResponse.prototype.totalCount = null;


function KontorolDirectoryRestriction()
{
}
KontorolDirectoryRestriction.prototype = new KontorolBaseRestriction();
/**
 * Kontorol directory restriction type
	 * 
 *
 * @var KontorolDirectoryRestrictionType
 */
KontorolDirectoryRestriction.prototype.directoryRestrictionType = null;


function KontorolDocumentEntry()
{
}
KontorolDocumentEntry.prototype = new KontorolBaseEntry();
/**
 * The type of the document
 *
 * @var KontorolDocumentType
 * @insertonly
 */
KontorolDocumentEntry.prototype.documentType = null;


function KontorolDocumentEntryFilter()
{
}
KontorolDocumentEntryFilter.prototype = new KontorolBaseEntryFilter();
/**
 * 
 *
 * @var KontorolDocumentType
 */
KontorolDocumentEntryFilter.prototype.documentTypeEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolDocumentEntryFilter.prototype.documentTypeIn = null;


function KontorolEntryExtraDataParams()
{
}
KontorolEntryExtraDataParams.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var string
 */
KontorolEntryExtraDataParams.prototype.referrer = null;


function KontorolEntryExtraDataResult()
{
}
KontorolEntryExtraDataResult.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var bool
 */
KontorolEntryExtraDataResult.prototype.isSiteRestricted = null;

/**
 * 
 *
 * @var bool
 */
KontorolEntryExtraDataResult.prototype.isCountryRestricted = null;

/**
 * 
 *
 * @var bool
 */
KontorolEntryExtraDataResult.prototype.isSessionRestricted = null;

/**
 * 
 *
 * @var int
 */
KontorolEntryExtraDataResult.prototype.previewLength = null;

/**
 * 
 *
 * @var bool
 */
KontorolEntryExtraDataResult.prototype.isScheduledNow = null;

/**
 * 
 *
 * @var bool
 */
KontorolEntryExtraDataResult.prototype.isAdmin = null;


function KontorolExtractMediaJobData()
{
}
KontorolExtractMediaJobData.prototype = new KontorolConvartableJobData();
/**
 * 
 *
 * @var string
 */
KontorolExtractMediaJobData.prototype.flavorAssetId = null;


function KontorolFilterPager()
{
}
KontorolFilterPager.prototype = new KontorolObjectBase();
/**
 * The number of objects to retrieve. (Default is 30, maximum page size is 500).
	 * 
 *
 * @var int
 */
KontorolFilterPager.prototype.pageSize = null;

/**
 * The page number for which {pageSize} of objects should be retrieved (Default is 1).
	 * 
 *
 * @var int
 */
KontorolFilterPager.prototype.pageIndex = null;


function KontorolFlattenJobData()
{
}
KontorolFlattenJobData.prototype = new KontorolJobData();

function KontorolFlavorAsset()
{
}
KontorolFlavorAsset.prototype = new KontorolObjectBase();
/**
 * The ID of the Flavor Asset
	 * 
 *
 * @var string
 * @readonly
 */
KontorolFlavorAsset.prototype.id = null;

/**
 * The entry ID of the Flavor Asset
	 * 
 *
 * @var string
 * @readonly
 */
KontorolFlavorAsset.prototype.entryId = null;

/**
 * 
 *
 * @var string
 * @readonly
 */
KontorolFlavorAsset.prototype.partnerId = null;

/**
 * The status of the Flavor Asset
	 * 
 *
 * @var KontorolFlavorAssetStatus
 * @readonly
 */
KontorolFlavorAsset.prototype.status = null;

/**
 * The Flavor Params used to create this Flavor Asset
	 * 
 *
 * @var int
 * @readonly
 */
KontorolFlavorAsset.prototype.flavorParamsId = null;

/**
 * The version of the Flavor Asset
	 * 
 *
 * @var int
 * @readonly
 */
KontorolFlavorAsset.prototype.version = null;

/**
 * The width of the Flavor Asset 
	 * 
 *
 * @var int
 * @readonly
 */
KontorolFlavorAsset.prototype.width = null;

/**
 * The height of the Flavor Asset
	 * 
 *
 * @var int
 * @readonly
 */
KontorolFlavorAsset.prototype.height = null;

/**
 * The overall bitrate (in KBits) of the Flavor Asset 
	 * 
 *
 * @var int
 * @readonly
 */
KontorolFlavorAsset.prototype.bitrate = null;

/**
 * The frame rate (in FPS) of the Flavor Asset
	 * 
 *
 * @var int
 * @readonly
 */
KontorolFlavorAsset.prototype.frameRate = null;

/**
 * The size (in KBytes) of the Flavor Asset
	 * 
 *
 * @var int
 * @readonly
 */
KontorolFlavorAsset.prototype.size = null;

/**
 * True if this Flavor Asset is the original source
	 * 
 *
 * @var bool
 */
KontorolFlavorAsset.prototype.isOriginal = null;

/**
 * Tags used to identify the Flavor Asset in various scenarios
	 * 
 *
 * @var string
 */
KontorolFlavorAsset.prototype.tags = null;

/**
 * True if this Flavor Asset is playable in KDP
	 * 
 *
 * @var bool
 */
KontorolFlavorAsset.prototype.isWeb = null;

/**
 * The file extension
	 * 
 *
 * @var string
 */
KontorolFlavorAsset.prototype.fileExt = null;

/**
 * The container format
	 * 
 *
 * @var string
 */
KontorolFlavorAsset.prototype.containerFormat = null;

/**
 * The video codec
	 * 
 *
 * @var string
 */
KontorolFlavorAsset.prototype.videoCodecId = null;


function KontorolFlavorAssetWithParams()
{
}
KontorolFlavorAssetWithParams.prototype = new KontorolObjectBase();
/**
 * The Flavor Asset (Can be null when there are params without asset)
	 * 
 *
 * @var KontorolFlavorAsset
 */
KontorolFlavorAssetWithParams.prototype.flavorAsset = null;

/**
 * The Flavor Params
	 * 
 *
 * @var KontorolFlavorParams
 */
KontorolFlavorAssetWithParams.prototype.flavorParams = null;

/**
 * The entry id
	 * 
 *
 * @var string
 */
KontorolFlavorAssetWithParams.prototype.entryId = null;


function KontorolFlavorParams()
{
}
KontorolFlavorParams.prototype = new KontorolObjectBase();
/**
 * The id of the Flavor Params
	 * 
 *
 * @var int
 * @readonly
 */
KontorolFlavorParams.prototype.id = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolFlavorParams.prototype.partnerId = null;

/**
 * The name of the Flavor Params
	 * 
 *
 * @var string
 */
KontorolFlavorParams.prototype.name = null;

/**
 * The description of the Flavor Params
	 * 
 *
 * @var string
 */
KontorolFlavorParams.prototype.description = null;

/**
 * Creation date as Unix timestamp (In seconds)
	 * 
 *
 * @var int
 * @readonly
 */
KontorolFlavorParams.prototype.createdAt = null;

/**
 * True if those Flavor Params are part of system defaults
	 * 
 *
 * @var KontorolNullableBoolean
 * @readonly
 */
KontorolFlavorParams.prototype.isSystemDefault = null;

/**
 * The Flavor Params tags are used to identify the flavor for different usage (e.g. web, hd, mobile)
	 * 
 *
 * @var string
 */
KontorolFlavorParams.prototype.tags = null;

/**
 * The container format of the Flavor Params
	 * 
 *
 * @var KontorolContainerFormat
 */
KontorolFlavorParams.prototype.format = null;

/**
 * The video codec of the Flavor Params
	 * 
 *
 * @var KontorolVideoCodec
 */
KontorolFlavorParams.prototype.videoCodec = null;

/**
 * The video bitrate (in KBits) of the Flavor Params
	 * 
 *
 * @var int
 */
KontorolFlavorParams.prototype.videoBitrate = null;

/**
 * The audio codec of the Flavor Params
	 * 
 *
 * @var KontorolAudioCodec
 */
KontorolFlavorParams.prototype.audioCodec = null;

/**
 * The audio bitrate (in KBits) of the Flavor Params
	 * 
 *
 * @var int
 */
KontorolFlavorParams.prototype.audioBitrate = null;

/**
 * The number of audio channels for "downmixing"
	 * 
 *
 * @var int
 */
KontorolFlavorParams.prototype.audioChannels = null;

/**
 * The audio sample rate of the Flavor Params
	 * 
 *
 * @var int
 */
KontorolFlavorParams.prototype.audioSampleRate = null;

/**
 * The desired width of the Flavor Params
	 * 
 *
 * @var int
 */
KontorolFlavorParams.prototype.width = null;

/**
 * The desired height of the Flavor Params
	 * 
 *
 * @var int
 */
KontorolFlavorParams.prototype.height = null;

/**
 * The frame rate of the Flavor Params
	 * 
 *
 * @var int
 */
KontorolFlavorParams.prototype.frameRate = null;

/**
 * The gop size of the Flavor Params
	 * 
 *
 * @var int
 */
KontorolFlavorParams.prototype.gopSize = null;

/**
 * The list of conversion engines (comma separated)
	 * 
 *
 * @var string
 */
KontorolFlavorParams.prototype.conversionEngines = null;

/**
 * The list of conversion engines extra params (separated with "|")
	 * 
 *
 * @var string
 */
KontorolFlavorParams.prototype.conversionEnginesExtraParams = null;

/**
 * 
 *
 * @var bool
 */
KontorolFlavorParams.prototype.twoPass = null;


function KontorolFlavorParamsFilter()
{
}
KontorolFlavorParamsFilter.prototype = new KontorolFilter();
/**
 * 
 *
 * @var KontorolNullableBoolean
 */
KontorolFlavorParamsFilter.prototype.isSystemDefaultEqual = null;


function KontorolFlavorParamsListResponse()
{
}
KontorolFlavorParamsListResponse.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var KontorolFlavorParamsArray
 * @readonly
 */
KontorolFlavorParamsListResponse.prototype.objects = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolFlavorParamsListResponse.prototype.totalCount = null;


function KontorolFlavorParamsOutput()
{
}
KontorolFlavorParamsOutput.prototype = new KontorolFlavorParams();
/**
 * 
 *
 * @var int
 */
KontorolFlavorParamsOutput.prototype.flavorParamsId = null;

/**
 * 
 *
 * @var string
 */
KontorolFlavorParamsOutput.prototype.commandLinesStr = null;


function KontorolFlavorParamsOutputFilter()
{
}
KontorolFlavorParamsOutputFilter.prototype = new KontorolFlavorParamsFilter();

function KontorolGoogleVideoSyndicationFeed()
{
}
KontorolGoogleVideoSyndicationFeed.prototype = new KontorolBaseSyndicationFeed();
/**
 * 
 *
 * @var KontorolGoogleSyndicationFeedAdultValues
 */
KontorolGoogleVideoSyndicationFeed.prototype.adultContent = null;


function KontorolGoogleVideoSyndicationFeedFilter()
{
}
KontorolGoogleVideoSyndicationFeedFilter.prototype = new KontorolBaseSyndicationFeedFilter();

function KontorolITunesSyndicationFeed()
{
}
KontorolITunesSyndicationFeed.prototype = new KontorolBaseSyndicationFeed();
/**
 * feed description
	 * 
 *
 * @var string
 */
KontorolITunesSyndicationFeed.prototype.feedDescription = null;

/**
 * feed language
	 * 
 *
 * @var string
 */
KontorolITunesSyndicationFeed.prototype.language = null;

/**
 * feed landing page (i.e publisher website)
	 * 
 *
 * @var string
 */
KontorolITunesSyndicationFeed.prototype.feedLandingPage = null;

/**
 * author/publisher name
	 * 
 *
 * @var string
 */
KontorolITunesSyndicationFeed.prototype.ownerName = null;

/**
 * publisher email
	 * 
 *
 * @var string
 */
KontorolITunesSyndicationFeed.prototype.ownerEmail = null;

/**
 * podcast thumbnail
	 * 
 *
 * @var string
 */
KontorolITunesSyndicationFeed.prototype.feedImageUrl = null;

/**
 * 
 *
 * @var KontorolITunesSyndicationFeedCategories
 * @readonly
 */
KontorolITunesSyndicationFeed.prototype.category = null;

/**
 * 
 *
 * @var KontorolITunesSyndicationFeedAdultValues
 */
KontorolITunesSyndicationFeed.prototype.adultContent = null;

/**
 * 
 *
 * @var string
 */
KontorolITunesSyndicationFeed.prototype.feedAuthor = null;


function KontorolITunesSyndicationFeedFilter()
{
}
KontorolITunesSyndicationFeedFilter.prototype = new KontorolBaseSyndicationFeedFilter();

function KontorolImportJobData()
{
}
KontorolImportJobData.prototype = new KontorolJobData();
/**
 * 
 *
 * @var string
 */
KontorolImportJobData.prototype.srcFileUrl = null;

/**
 * 
 *
 * @var string
 */
KontorolImportJobData.prototype.destFileLocalPath = null;

/**
 * 
 *
 * @var string
 */
KontorolImportJobData.prototype.flavorAssetId = null;


function KontorolMailJob()
{
}
KontorolMailJob.prototype = new KontorolBaseJob();
/**
 * 
 *
 * @var KontorolMailType
 */
KontorolMailJob.prototype.mailType = null;

/**
 * 
 *
 * @var int
 */
KontorolMailJob.prototype.mailPriority = null;

/**
 * 
 *
 * @var KontorolMailJobStatus
 */
KontorolMailJob.prototype.status = null;

/**
 * 
 *
 * @var string
 */
KontorolMailJob.prototype.recipientName = null;

/**
 * 
 *
 * @var string
 */
KontorolMailJob.prototype.recipientEmail = null;

/**
 * kuserId  
 *
 * @var int
 */
KontorolMailJob.prototype.recipientId = null;

/**
 * 
 *
 * @var string
 */
KontorolMailJob.prototype.fromName = null;

/**
 * 
 *
 * @var string
 */
KontorolMailJob.prototype.fromEmail = null;

/**
 * 
 *
 * @var string
 */
KontorolMailJob.prototype.bodyParams = null;

/**
 * 
 *
 * @var string
 */
KontorolMailJob.prototype.subjectParams = null;

/**
 * 
 *
 * @var string
 */
KontorolMailJob.prototype.templatePath = null;

/**
 * 
 *
 * @var int
 */
KontorolMailJob.prototype.culture = null;

/**
 * 
 *
 * @var int
 */
KontorolMailJob.prototype.campaignId = null;

/**
 * 
 *
 * @var int
 */
KontorolMailJob.prototype.minSendDate = null;


function KontorolMailJobData()
{
}
KontorolMailJobData.prototype = new KontorolJobData();
/**
 * 
 *
 * @var KontorolMailType
 */
KontorolMailJobData.prototype.mailType = null;

/**
 * 
 *
 * @var int
 */
KontorolMailJobData.prototype.mailPriority = null;

/**
 * 
 *
 * @var KontorolMailJobStatus
 */
KontorolMailJobData.prototype.status = null;

/**
 * 
 *
 * @var string
 */
KontorolMailJobData.prototype.recipientName = null;

/**
 * 
 *
 * @var string
 */
KontorolMailJobData.prototype.recipientEmail = null;

/**
 * kuserId  
 *
 * @var int
 */
KontorolMailJobData.prototype.recipientId = null;

/**
 * 
 *
 * @var string
 */
KontorolMailJobData.prototype.fromName = null;

/**
 * 
 *
 * @var string
 */
KontorolMailJobData.prototype.fromEmail = null;

/**
 * 
 *
 * @var string
 */
KontorolMailJobData.prototype.bodyParams = null;

/**
 * 
 *
 * @var string
 */
KontorolMailJobData.prototype.subjectParams = null;

/**
 * 
 *
 * @var string
 */
KontorolMailJobData.prototype.templatePath = null;

/**
 * 
 *
 * @var int
 */
KontorolMailJobData.prototype.culture = null;

/**
 * 
 *
 * @var int
 */
KontorolMailJobData.prototype.campaignId = null;

/**
 * 
 *
 * @var int
 */
KontorolMailJobData.prototype.minSendDate = null;

/**
 * 
 *
 * @var bool
 */
KontorolMailJobData.prototype.isHtml = null;


function KontorolMailJobFilter()
{
}
KontorolMailJobFilter.prototype = new KontorolBaseJobFilter();

function KontorolPlayableEntry()
{
}
KontorolPlayableEntry.prototype = new KontorolBaseEntry();
/**
 * Number of plays
	 * 
 *
 * @var int
 * @readonly
 */
KontorolPlayableEntry.prototype.plays = null;

/**
 * Number of views
	 * 
 *
 * @var int
 * @readonly
 */
KontorolPlayableEntry.prototype.views = null;

/**
 * The width in pixels
	 * 
 *
 * @var int
 * @readonly
 */
KontorolPlayableEntry.prototype.width = null;

/**
 * The height in pixels
	 * 
 *
 * @var int
 * @readonly
 */
KontorolPlayableEntry.prototype.height = null;

/**
 * The duration in seconds
	 * 
 *
 * @var int
 * @readonly
 */
KontorolPlayableEntry.prototype.duration = null;

/**
 * The duration type (short for 0-4 mins, medium for 4-20 mins, long for 20+ mins)
	 * 
 *
 * @var KontorolDurationType
 * @readonly
 */
KontorolPlayableEntry.prototype.durationType = null;


function KontorolMediaEntry()
{
}
KontorolMediaEntry.prototype = new KontorolPlayableEntry();
/**
 * The media type of the entry
	 * 
 *
 * @var KontorolMediaType
 * @insertonly
 */
KontorolMediaEntry.prototype.mediaType = null;

/**
 * Override the default conversion quality  
	 * 
 *
 * @var string
 * @insertonly
 */
KontorolMediaEntry.prototype.conversionQuality = null;

/**
 * The source type of the entry 
 *
 * @var KontorolSourceType
 * @readonly
 */
KontorolMediaEntry.prototype.sourceType = null;

/**
 * The search provider type used to import this entry
 *
 * @var KontorolSearchProviderType
 * @readonly
 */
KontorolMediaEntry.prototype.searchProviderType = null;

/**
 * The ID of the media in the importing site
 *
 * @var string
 * @readonly
 */
KontorolMediaEntry.prototype.searchProviderId = null;

/**
 * The user name used for credits
 *
 * @var string
 */
KontorolMediaEntry.prototype.creditUserName = null;

/**
 * The URL for credits
 *
 * @var string
 */
KontorolMediaEntry.prototype.creditUrl = null;

/**
 * The media date extracted from EXIF data (For images) as Unix timestamp (In seconds)
 *
 * @var int
 * @readonly
 */
KontorolMediaEntry.prototype.mediaDate = null;

/**
 * The URL used for playback. This is not the download URL.
 *
 * @var string
 * @readonly
 */
KontorolMediaEntry.prototype.dataUrl = null;

/**
 * Comma separated flavor params ids that exists for this media entry
	 * 
 *
 * @var string
 * @readonly
 */
KontorolMediaEntry.prototype.flavorParamsIds = null;


function KontorolPlayableEntryFilter()
{
}
KontorolPlayableEntryFilter.prototype = new KontorolBaseEntryFilter();
/**
 * 
 *
 * @var int
 */
KontorolPlayableEntryFilter.prototype.durationLessThan = null;

/**
 * 
 *
 * @var int
 */
KontorolPlayableEntryFilter.prototype.durationGreaterThan = null;

/**
 * 
 *
 * @var int
 */
KontorolPlayableEntryFilter.prototype.durationLessThanOrEqual = null;

/**
 * 
 *
 * @var int
 */
KontorolPlayableEntryFilter.prototype.durationGreaterThanOrEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolPlayableEntryFilter.prototype.durationTypeMatchOr = null;


function KontorolMediaEntryFilter()
{
}
KontorolMediaEntryFilter.prototype = new KontorolPlayableEntryFilter();
/**
 * 
 *
 * @var KontorolMediaType
 */
KontorolMediaEntryFilter.prototype.mediaTypeEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolMediaEntryFilter.prototype.mediaTypeIn = null;

/**
 * 
 *
 * @var int
 */
KontorolMediaEntryFilter.prototype.mediaDateGreaterThanOrEqual = null;

/**
 * 
 *
 * @var int
 */
KontorolMediaEntryFilter.prototype.mediaDateLessThanOrEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolMediaEntryFilter.prototype.flavorParamsIdsMatchOr = null;

/**
 * 
 *
 * @var string
 */
KontorolMediaEntryFilter.prototype.flavorParamsIdsMatchAnd = null;


function KontorolMediaEntryFilterForPlaylist()
{
}
KontorolMediaEntryFilterForPlaylist.prototype = new KontorolMediaEntryFilter();
/**
 * 
 *
 * @var int
 */
KontorolMediaEntryFilterForPlaylist.prototype.limit = null;


function KontorolMediaListResponse()
{
}
KontorolMediaListResponse.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var KontorolMediaEntryArray
 * @readonly
 */
KontorolMediaListResponse.prototype.objects = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolMediaListResponse.prototype.totalCount = null;


function KontorolMixEntry()
{
}
KontorolMixEntry.prototype = new KontorolPlayableEntry();
/**
 * Indicates whether the user has submited a real thumbnail to the mix (Not the one that was generated automaticaly)
	 * 
 *
 * @var bool
 * @readonly
 */
KontorolMixEntry.prototype.hasRealThumbnail = null;

/**
 * The editor type used to edit the metadata
	 * 
 *
 * @var KontorolEditorType
 */
KontorolMixEntry.prototype.editorType = null;

/**
 * The xml data of the mix
 *
 * @var string
 */
KontorolMixEntry.prototype.dataContent = null;


function KontorolMixEntryFilter()
{
}
KontorolMixEntryFilter.prototype = new KontorolPlayableEntryFilter();

function KontorolMixListResponse()
{
}
KontorolMixListResponse.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var KontorolMixEntryArray
 * @readonly
 */
KontorolMixListResponse.prototype.objects = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolMixListResponse.prototype.totalCount = null;


function KontorolModerationFlag()
{
}
KontorolModerationFlag.prototype = new KontorolObjectBase();
/**
 * Moderation flag id
 *
 * @var int
 * @readonly
 */
KontorolModerationFlag.prototype.id = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolModerationFlag.prototype.partnerId = null;

/**
 * The user id that added the moderation flag
 *
 * @var string
 * @readonly
 */
KontorolModerationFlag.prototype.userId = null;

/**
 * The type of the moderation flag (entry or user)
 *
 * @var KontorolModerationObjectType
 * @readonly
 */
KontorolModerationFlag.prototype.moderationObjectType = null;

/**
 * If moderation flag is set for entry, this is the flagged entry id
 *
 * @var string
 */
KontorolModerationFlag.prototype.flaggedEntryId = null;

/**
 * If moderation flag is set for user, this is the flagged user id
 *
 * @var string
 */
KontorolModerationFlag.prototype.flaggedUserId = null;

/**
 * The moderation flag status
 *
 * @var KontorolModerationFlagStatus
 * @readonly
 */
KontorolModerationFlag.prototype.status = null;

/**
 * The comment that was added to the flag
 *
 * @var string
 */
KontorolModerationFlag.prototype.comments = null;

/**
 * 
 *
 * @var KontorolModerationFlagType
 */
KontorolModerationFlag.prototype.flagType = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolModerationFlag.prototype.createdAt = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolModerationFlag.prototype.updatedAt = null;


function KontorolModerationFlagListResponse()
{
}
KontorolModerationFlagListResponse.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var KontorolModerationFlagArray
 * @readonly
 */
KontorolModerationFlagListResponse.prototype.objects = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolModerationFlagListResponse.prototype.totalCount = null;


function KontorolNotification()
{
}
KontorolNotification.prototype = new KontorolBaseJob();
/**
 * 
 *
 * @var string
 */
KontorolNotification.prototype.puserId = null;

/**
 * 
 *
 * @var KontorolNotificationType
 */
KontorolNotification.prototype.type = null;

/**
 * 
 *
 * @var string
 */
KontorolNotification.prototype.objectId = null;

/**
 * 
 *
 * @var KontorolNotificationStatus
 */
KontorolNotification.prototype.status = null;

/**
 * 
 *
 * @var string
 */
KontorolNotification.prototype.notificationData = null;

/**
 * 
 *
 * @var int
 */
KontorolNotification.prototype.numberOfAttempts = null;

/**
 * 
 *
 * @var string
 */
KontorolNotification.prototype.notificationResult = null;

/**
 * 
 *
 * @var KontorolNotificationObjectType
 */
KontorolNotification.prototype.objType = null;

function KontorolNotificationJobData()
{
}
KontorolNotificationJobData.prototype = new KontorolJobData();
/**
 * 
 *
 * @var string
 */
KontorolNotificationJobData.prototype.userId = null;

/**
 * 
 *
 * @var KontorolNotificationType
 */
KontorolNotificationJobData.prototype.type = null;

/**
 * 
 *
 * @var string
 */
KontorolNotificationJobData.prototype.typeAsString = null;

/**
 * 
 *
 * @var string
 */
KontorolNotificationJobData.prototype.objectId = null;

/**
 * 
 *
 * @var KontorolNotificationStatus
 */
KontorolNotificationJobData.prototype.status = null;

/**
 * 
 *
 * @var string
 */
KontorolNotificationJobData.prototype.data = null;

/**
 * 
 *
 * @var int
 */
KontorolNotificationJobData.prototype.numberOfAttempts = null;

/**
 * 
 *
 * @var string
 */
KontorolNotificationJobData.prototype.notificationResult = null;

/**
 * 
 *
 * @var KontorolNotificationObjectType
 */
KontorolNotificationJobData.prototype.objType = null;


function KontorolPartner()
{
}
KontorolPartner.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolPartner.prototype.id = null;

/**
 * 
 *
 * @var string
 */
KontorolPartner.prototype.name = null;

/**
 * 
 *
 * @var string
 */
KontorolPartner.prototype.website = null;

/**
 * 
 *
 * @var string
 */
KontorolPartner.prototype.notificationUrl = null;

/**
 * 
 *
 * @var int
 */
KontorolPartner.prototype.appearInSearch = null;

/**
 * 
 *
 * @var string
 * @readonly
 */
KontorolPartner.prototype.createdAt = null;

/**
 * 
 *
 * @var string
 */
KontorolPartner.prototype.adminName = null;

/**
 * 
 *
 * @var string
 */
KontorolPartner.prototype.adminEmail = null;

/**
 * 
 *
 * @var string
 */
KontorolPartner.prototype.description = null;

/**
 * 
 *
 * @var KontorolCommercialUseType
 */
KontorolPartner.prototype.commercialUse = null;

/**
 * 
 *
 * @var string
 */
KontorolPartner.prototype.landingPage = null;

/**
 * 
 *
 * @var string
 */
KontorolPartner.prototype.userLandingPage = null;

/**
 * 
 *
 * @var string
 */
KontorolPartner.prototype.contentCategories = null;

/**
 * 
 *
 * @var KontorolPartnerType
 */
KontorolPartner.prototype.type = null;

/**
 * 
 *
 * @var string
 */
KontorolPartner.prototype.phone = null;

/**
 * 
 *
 * @var string
 */
KontorolPartner.prototype.describeYourself = null;

/**
 * 
 *
 * @var bool
 */
KontorolPartner.prototype.adultContent = null;

/**
 * 
 *
 * @var string
 */
KontorolPartner.prototype.defConversionProfileType = null;

/**
 * 
 *
 * @var int
 */
KontorolPartner.prototype.notify = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolPartner.prototype.status = null;

/**
 * 
 *
 * @var int
 */
KontorolPartner.prototype.allowQuickEdit = null;

/**
 * 
 *
 * @var int
 */
KontorolPartner.prototype.mergeEntryLists = null;

/**
 * 
 *
 * @var string
 */
KontorolPartner.prototype.notificationsConfig = null;

/**
 * 
 *
 * @var int
 */
KontorolPartner.prototype.maxUploadSize = null;

/**
 * readonly
 *
 * @var int
 */
KontorolPartner.prototype.partnerPackage = null;

/**
 * readonly
 *
 * @var string
 */
KontorolPartner.prototype.secret = null;

/**
 * readonly
 *
 * @var string
 */
KontorolPartner.prototype.adminSecret = null;

/**
 * 
 *
 * @var string
 * @readonly
 */
KontorolPartner.prototype.cmsPassword = null;

/**
 * readonly
 *
 * @var int
 */
KontorolPartner.prototype.allowMultiNotification = null;


function KontorolPartnerFilter()
{
}
KontorolPartnerFilter.prototype = new KontorolFilter();
/**
 * 
 *
 * @var string
 */
KontorolPartnerFilter.prototype.nameLike = null;

/**
 * 
 *
 * @var string
 */
KontorolPartnerFilter.prototype.nameMultiLikeOr = null;

/**
 * 
 *
 * @var string
 */
KontorolPartnerFilter.prototype.nameMultiLikeAnd = null;

/**
 * 
 *
 * @var string
 */
KontorolPartnerFilter.prototype.nameEqual = null;

/**
 * 
 *
 * @var int
 */
KontorolPartnerFilter.prototype.statusEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolPartnerFilter.prototype.statusIn = null;


function KontorolPartnerUsage()
{
}
KontorolPartnerUsage.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var float
 * @readonly
 */
KontorolPartnerUsage.prototype.hostingGB = null;

/**
 * 
 *
 * @var float
 * @readonly
 */
KontorolPartnerUsage.prototype.Percent = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolPartnerUsage.prototype.packageBW = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolPartnerUsage.prototype.usageGB = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolPartnerUsage.prototype.reachedLimitDate = null;

/**
 * 
 *
 * @var string
 * @readonly
 */
KontorolPartnerUsage.prototype.usageGraph = null;


function KontorolPlaylist()
{
}
KontorolPlaylist.prototype = new KontorolBaseEntry();
/**
 * Content of the playlist - 
	 * XML if the playlistType is dynamic 
	 * text if the playlistType is static 
	 * url if the playlistType is mRss 
 *
 * @var string
 */
KontorolPlaylist.prototype.playlistContent = null;

/**
 * 
 *
 * @var KontorolMediaEntryFilterForPlaylistArray
 */
KontorolPlaylist.prototype.filters = null;

/**
 * 
 *
 * @var int
 */
KontorolPlaylist.prototype.totalResults = null;

/**
 * Type of playlist  
 *
 * @var KontorolPlaylistType
 */
KontorolPlaylist.prototype.playlistType = null;

/**
 * Number of plays
 *
 * @var int
 * @readonly
 */
KontorolPlaylist.prototype.plays = null;

/**
 * Number of views
 *
 * @var int
 * @readonly
 */
KontorolPlaylist.prototype.views = null;

/**
 * The duration in seconds
 *
 * @var int
 * @readonly
 */
KontorolPlaylist.prototype.duration = null;


function KontorolPlaylistFilter()
{
}
KontorolPlaylistFilter.prototype = new KontorolBaseEntryFilter();

function KontorolPlaylistListResponse()
{
}
KontorolPlaylistListResponse.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var KontorolPlaylistArray
 * @readonly
 */
KontorolPlaylistListResponse.prototype.objects = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolPlaylistListResponse.prototype.totalCount = null;


function KontorolPostConvertJobData()
{
}
KontorolPostConvertJobData.prototype = new KontorolJobData();
/**
 * 
 *
 * @var string
 */
KontorolPostConvertJobData.prototype.srcFileSyncLocalPath = null;

/**
 * 
 *
 * @var string
 */
KontorolPostConvertJobData.prototype.flavorAssetId = null;

/**
 * Indicates if a thumbnail should be created
	 * 
 *
 * @var bool
 */
KontorolPostConvertJobData.prototype.createThumb = null;

/**
 * The path of the created thumbnail
	 * 
 *
 * @var string
 */
KontorolPostConvertJobData.prototype.thumbPath = null;

/**
 * The position of the thumbnail in the media file
	 * 
 *
 * @var int
 */
KontorolPostConvertJobData.prototype.thumbOffset = null;

/**
 * The height of the movie, will be used to comapare if this thumbnail is the best we can have
	 * 
 *
 * @var int
 */
KontorolPostConvertJobData.prototype.thumbHeight = null;

/**
 * The bit rate of the movie, will be used to comapare if this thumbnail is the best we can have
	 * 
 *
 * @var int
 */
KontorolPostConvertJobData.prototype.thumbBitrate = null;

/**
 * 
 *
 * @var int
 */
KontorolPostConvertJobData.prototype.flavorParamsOutputId = null;


function KontorolSessionRestriction()
{
}
KontorolSessionRestriction.prototype = new KontorolBaseRestriction();

function KontorolPreviewRestriction()
{
}
KontorolPreviewRestriction.prototype = new KontorolSessionRestriction();
/**
 * The preview restriction length 
	 * 
 *
 * @var int
 */
KontorolPreviewRestriction.prototype.previewLength = null;


function KontorolPullJobData()
{
}
KontorolPullJobData.prototype = new KontorolJobData();
/**
 * 
 *
 * @var string
 */
KontorolPullJobData.prototype.srcFileUrl = null;

/**
 * 
 *
 * @var string
 */
KontorolPullJobData.prototype.destFileLocalPath = null;


function KontorolRemoteConvertJobData()
{
}
KontorolRemoteConvertJobData.prototype = new KontorolConvartableJobData();
/**
 * 
 *
 * @var string
 */
KontorolRemoteConvertJobData.prototype.srcFileUrl = null;

/**
 * Should be set by the API
	 * 
 *
 * @var string
 */
KontorolRemoteConvertJobData.prototype.destFileUrl = null;


function KontorolReportGraph()
{
}
KontorolReportGraph.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var string
 */
KontorolReportGraph.prototype.id = null;

/**
 * 
 *
 * @var string
 */
KontorolReportGraph.prototype.data = null;


function KontorolReportInputFilter()
{
}
KontorolReportInputFilter.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var int
 */
KontorolReportInputFilter.prototype.fromDate = null;

/**
 * 
 *
 * @var int
 */
KontorolReportInputFilter.prototype.toDate = null;

/**
 * 
 *
 * @var string
 */
KontorolReportInputFilter.prototype.keywords = null;

/**
 * 
 *
 * @var bool
 */
KontorolReportInputFilter.prototype.searchInTags = null;

/**
 * 
 *
 * @var bool
 */
KontorolReportInputFilter.prototype.searchInAdminTags = null;

/**
 * 
 *
 * @var string
 */
KontorolReportInputFilter.prototype.categories = null;


function KontorolReportTable()
{
}
KontorolReportTable.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var string
 * @readonly
 */
KontorolReportTable.prototype.header = null;

/**
 * 
 *
 * @var string
 * @readonly
 */
KontorolReportTable.prototype.data = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolReportTable.prototype.totalCount = null;


function KontorolReportTotal()
{
}
KontorolReportTotal.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var string
 */
KontorolReportTotal.prototype.header = null;

/**
 * 
 *
 * @var string
 */
KontorolReportTotal.prototype.data = null;


function KontorolSearch()
{
}
KontorolSearch.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var string
 */
KontorolSearch.prototype.keyWords = null;

/**
 * 
 *
 * @var KontorolSearchProviderType
 */
KontorolSearch.prototype.searchSource = null;

/**
 * 
 *
 * @var KontorolMediaType
 */
KontorolSearch.prototype.mediaType = null;

/**
 * Use this field to pass dynamic data for searching
	 * For example - if you set this field to "mymovies_$partner_id"
	 * The $partner_id will be automatically replcaed with your real partner Id
	 * 
 *
 * @var string
 */
KontorolSearch.prototype.extraData = null;

/**
 * 
 *
 * @var string
 */
KontorolSearch.prototype.authData = null;


function KontorolSearchAuthData()
{
}
KontorolSearchAuthData.prototype = new KontorolObjectBase();
/**
 * The authentication data that further should be used for search
	 * 
 *
 * @var string
 */
KontorolSearchAuthData.prototype.authData = null;

/**
 * Login URL when user need to sign-in and authorize the search
 *
 * @var string
 */
KontorolSearchAuthData.prototype.loginUrl = null;

/**
 * Information when there was an error
 *
 * @var string
 */
KontorolSearchAuthData.prototype.message = null;


function KontorolSearchResult()
{
}
KontorolSearchResult.prototype = new KontorolSearch();
/**
 * 
 *
 * @var string
 */
KontorolSearchResult.prototype.id = null;

/**
 * 
 *
 * @var string
 */
KontorolSearchResult.prototype.title = null;

/**
 * 
 *
 * @var string
 */
KontorolSearchResult.prototype.thumbUrl = null;

/**
 * 
 *
 * @var string
 */
KontorolSearchResult.prototype.description = null;

/**
 * 
 *
 * @var string
 */
KontorolSearchResult.prototype.tags = null;

/**
 * 
 *
 * @var string
 */
KontorolSearchResult.prototype.url = null;

/**
 * 
 *
 * @var string
 */
KontorolSearchResult.prototype.sourceLink = null;

/**
 * 
 *
 * @var string
 */
KontorolSearchResult.prototype.credit = null;

/**
 * 
 *
 * @var KontorolLicenseType
 */
KontorolSearchResult.prototype.licenseType = null;

/**
 * 
 *
 * @var string
 */
KontorolSearchResult.prototype.flashPlaybackType = null;


function KontorolSearchResultResponse()
{
}
KontorolSearchResultResponse.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var KontorolSearchResultArray
 * @readonly
 */
KontorolSearchResultResponse.prototype.objects = null;

/**
 * 
 *
 * @var bool
 * @readonly
 */
KontorolSearchResultResponse.prototype.needMediaInfo = null;


function KontorolSiteRestriction()
{
}
KontorolSiteRestriction.prototype = new KontorolBaseRestriction();
/**
 * The site restriction type (allow or deny)
	 * 
 *
 * @var KontorolSiteRestrictionType
 */
KontorolSiteRestriction.prototype.siteRestrictionType = null;

/**
 * Comma separated list of sites (domains) to allow or deny
	 * 
 *
 * @var string
 */
KontorolSiteRestriction.prototype.siteList = null;


function KontorolStartWidgetSessionResponse()
{
}
KontorolStartWidgetSessionResponse.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolStartWidgetSessionResponse.prototype.partnerId = null;

/**
 * 
 *
 * @var string
 * @readonly
 */
KontorolStartWidgetSessionResponse.prototype.ks = null;

/**
 * 
 *
 * @var string
 * @readonly
 */
KontorolStartWidgetSessionResponse.prototype.userId = null;


function KontorolStatsEvent()
{
}
KontorolStatsEvent.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var string
 */
KontorolStatsEvent.prototype.clientVer = null;

/**
 * 
 *
 * @var KontorolStatsEventType
 */
KontorolStatsEvent.prototype.eventType = null;

/**
 * the client's timestamp of this event
	 * 
 *
 * @var float
 */
KontorolStatsEvent.prototype.eventTimestamp = null;

/**
 * a unique string generated by the client that will represent the client-side session: the primary component will pass it on to other components that sprout from it
 *
 * @var string
 */
KontorolStatsEvent.prototype.sessionId = null;

/**
 * 
 *
 * @var int
 */
KontorolStatsEvent.prototype.partnerId = null;

/**
 * 
 *
 * @var string
 */
KontorolStatsEvent.prototype.entryId = null;

/**
 * the UV cookie - creates in the operational system and should be passed on ofr every event 
 *
 * @var string
 */
KontorolStatsEvent.prototype.uniqueViewer = null;

/**
 * 
 *
 * @var string
 */
KontorolStatsEvent.prototype.widgetId = null;

/**
 * 
 *
 * @var int
 */
KontorolStatsEvent.prototype.uiconfId = null;

/**
 * the partner's user id 
 *
 * @var string
 */
KontorolStatsEvent.prototype.userId = null;

/**
 * the timestamp along the video when the event happend 
 *
 * @var int
 */
KontorolStatsEvent.prototype.currentPoint = null;

/**
 * the duration of the video in milliseconds - will make it much faster than quering the db for each entry 
 *
 * @var int
 */
KontorolStatsEvent.prototype.duration = null;

/**
 * will be retrieved from the request of the user 
 *
 * @var string
 * @readonly
 */
KontorolStatsEvent.prototype.userIp = null;

/**
 * the time in milliseconds the event took
 *
 * @var int
 */
KontorolStatsEvent.prototype.processDuration = null;

/**
 * the id of the GUI control - will be used in the future to better understand what the user clicked
 *
 * @var string
 */
KontorolStatsEvent.prototype.controlId = null;

/**
 * true if the user ever used seek in this session 
 *
 * @var bool
 */
KontorolStatsEvent.prototype.seek = null;

/**
 * timestamp of the new point on the timeline of the video after the user seeks 
 *
 * @var int
 */
KontorolStatsEvent.prototype.newPoint = null;

/**
 * the referrer of the client
 *
 * @var string
 */
KontorolStatsEvent.prototype.referrer = null;

/**
 * will indicate if the event is thrown for the first video in the session
 *
 * @var bool
 */
KontorolStatsEvent.prototype.isFirstInSession = null;


function KontorolStatsKmcEvent()
{
}
KontorolStatsKmcEvent.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var string
 */
KontorolStatsKmcEvent.prototype.clientVer = null;

/**
 * 
 *
 * @var string
 */
KontorolStatsKmcEvent.prototype.kmcEventActionPath = null;

/**
 * 
 *
 * @var KontorolStatsKmcEventType
 */
KontorolStatsKmcEvent.prototype.kmcEventType = null;

/**
 * the client's timestamp of this event
	 * 
 *
 * @var float
 */
KontorolStatsKmcEvent.prototype.eventTimestamp = null;

/**
 * a unique string generated by the client that will represent the client-side session: the primary component will pass it on to other components that sprout from it
 *
 * @var string
 */
KontorolStatsKmcEvent.prototype.sessionId = null;

/**
 * 
 *
 * @var int
 */
KontorolStatsKmcEvent.prototype.partnerId = null;

/**
 * 
 *
 * @var string
 */
KontorolStatsKmcEvent.prototype.entryId = null;

/**
 * 
 *
 * @var string
 */
KontorolStatsKmcEvent.prototype.widgetId = null;

/**
 * 
 *
 * @var int
 */
KontorolStatsKmcEvent.prototype.uiconfId = null;

/**
 * the partner's user id 
 *
 * @var string
 */
KontorolStatsKmcEvent.prototype.userId = null;

/**
 * will be retrieved from the request of the user 
 *
 * @var string
 * @readonly
 */
KontorolStatsKmcEvent.prototype.userIp = null;


function KontorolSyndicationFeedEntryCount()
{
}
KontorolSyndicationFeedEntryCount.prototype = new KontorolObjectBase();
/**
 * the total count of entries that should appear in the feed without flavor filtering
 *
 * @var int
 */
KontorolSyndicationFeedEntryCount.prototype.totalEntryCount = null;

/**
 * count of entries that will appear in the feed (including all relevant filters)
 *
 * @var int
 */
KontorolSyndicationFeedEntryCount.prototype.actualEntryCount = null;

/**
 * count of entries that requires transcoding in order to be included in feed
 *
 * @var int
 */
KontorolSyndicationFeedEntryCount.prototype.requireTranscodingCount = null;


function KontorolSystemUser()
{
}
KontorolSystemUser.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolSystemUser.prototype.id = null;

/**
 * 
 *
 * @var string
 */
KontorolSystemUser.prototype.email = null;

/**
 * 
 *
 * @var string
 */
KontorolSystemUser.prototype.firstName = null;

/**
 * 
 *
 * @var string
 */
KontorolSystemUser.prototype.lastName = null;

/**
 * 
 *
 * @var string
 */
KontorolSystemUser.prototype.password = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolSystemUser.prototype.createdBy = null;

/**
 * 
 *
 * @var KontorolSystemUserStatus
 */
KontorolSystemUser.prototype.status = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolSystemUser.prototype.statusUpdatedAt = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolSystemUser.prototype.createdAt = null;


function KontorolSystemUserFilter()
{
}
KontorolSystemUserFilter.prototype = new KontorolFilter();

function KontorolSystemUserListResponse()
{
}
KontorolSystemUserListResponse.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var KontorolSystemUserArray
 * @readonly
 */
KontorolSystemUserListResponse.prototype.objects = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolSystemUserListResponse.prototype.totalCount = null;


function KontorolTubeMogulSyndicationFeed()
{
}
KontorolTubeMogulSyndicationFeed.prototype = new KontorolBaseSyndicationFeed();
/**
 * 
 *
 * @var KontorolTubeMogulSyndicationFeedCategories
 * @readonly
 */
KontorolTubeMogulSyndicationFeed.prototype.category = null;


function KontorolTubeMogulSyndicationFeedFilter()
{
}
KontorolTubeMogulSyndicationFeedFilter.prototype = new KontorolBaseSyndicationFeedFilter();

function KontorolUiConf()
{
}
KontorolUiConf.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolUiConf.prototype.id = null;

/**
 * Name of the uiConf, this is not a primary key
 *
 * @var string
 */
KontorolUiConf.prototype.name = null;

/**
 * 
 *
 * @var string
 */
KontorolUiConf.prototype.description = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolUiConf.prototype.partnerId = null;

/**
 * 
 *
 * @var KontorolUiConfObjType
 */
KontorolUiConf.prototype.objType = null;

/**
 * 
 *
 * @var string
 * @readonly
 */
KontorolUiConf.prototype.objTypeAsString = null;

/**
 * 
 *
 * @var int
 */
KontorolUiConf.prototype.width = null;

/**
 * 
 *
 * @var int
 */
KontorolUiConf.prototype.height = null;

/**
 * 
 *
 * @var string
 */
KontorolUiConf.prototype.htmlParams = null;

/**
 * 
 *
 * @var string
 */
KontorolUiConf.prototype.swfUrl = null;

/**
 * 
 *
 * @var string
 * @readonly
 */
KontorolUiConf.prototype.confFilePath = null;

/**
 * 
 *
 * @var string
 */
KontorolUiConf.prototype.confFile = null;

/**
 * 
 *
 * @var string
 */
KontorolUiConf.prototype.confFileFeatures = null;

/**
 * 
 *
 * @var string
 */
KontorolUiConf.prototype.confVars = null;

/**
 * 
 *
 * @var bool
 */
KontorolUiConf.prototype.useCdn = null;

/**
 * 
 *
 * @var string
 */
KontorolUiConf.prototype.tags = null;

/**
 * 
 *
 * @var string
 */
KontorolUiConf.prototype.swfUrlVersion = null;

/**
 * Entry creation date as Unix timestamp (In seconds)
 *
 * @var int
 * @readonly
 */
KontorolUiConf.prototype.createdAt = null;

/**
 * Entry creation date as Unix timestamp (In seconds)
 *
 * @var int
 * @readonly
 */
KontorolUiConf.prototype.updatedAt = null;

/**
 * 
 *
 * @var KontorolUiConfCreationMode
 */
KontorolUiConf.prototype.creationMode = null;


function KontorolUiConfFilter()
{
}
KontorolUiConfFilter.prototype = new KontorolFilter();
/**
 * 
 *
 * @var int
 */
KontorolUiConfFilter.prototype.idEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolUiConfFilter.prototype.idIn = null;

/**
 * 
 *
 * @var string
 */
KontorolUiConfFilter.prototype.nameLike = null;

/**
 * 
 *
 * @var KontorolUiConfObjType
 */
KontorolUiConfFilter.prototype.objTypeEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolUiConfFilter.prototype.tagsMultiLikeOr = null;

/**
 * 
 *
 * @var string
 */
KontorolUiConfFilter.prototype.tagsMultiLikeAnd = null;

/**
 * 
 *
 * @var int
 */
KontorolUiConfFilter.prototype.createdAtGreaterThanOrEqual = null;

/**
 * 
 *
 * @var int
 */
KontorolUiConfFilter.prototype.createdAtLessThanOrEqual = null;

/**
 * 
 *
 * @var int
 */
KontorolUiConfFilter.prototype.updatedAtGreaterThanOrEqual = null;

/**
 * 
 *
 * @var int
 */
KontorolUiConfFilter.prototype.updatedAtLessThanOrEqual = null;


function KontorolUiConfListResponse()
{
}
KontorolUiConfListResponse.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var KontorolUiConfArray
 * @readonly
 */
KontorolUiConfListResponse.prototype.objects = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolUiConfListResponse.prototype.totalCount = null;


function KontorolUploadResponse()
{
}
KontorolUploadResponse.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var string
 */
KontorolUploadResponse.prototype.uploadTokenId = null;

/**
 * 
 *
 * @var int
 */
KontorolUploadResponse.prototype.fileSize = null;

/**
 * 
 *
 * @var KontorolUploadErrorCode
 */
KontorolUploadResponse.prototype.errorCode = null;

/**
 * 
 *
 * @var string
 */
KontorolUploadResponse.prototype.errorDescription = null;


function KontorolUser()
{
}
KontorolUser.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var string
 */
KontorolUser.prototype.id = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolUser.prototype.partnerId = null;

/**
 * 
 *
 * @var string
 */
KontorolUser.prototype.screenName = null;

/**
 * 
 *
 * @var string
 */
KontorolUser.prototype.fullName = null;

/**
 * 
 *
 * @var string
 */
KontorolUser.prototype.email = null;

/**
 * 
 *
 * @var int
 */
KontorolUser.prototype.dateOfBirth = null;

/**
 * 
 *
 * @var string
 */
KontorolUser.prototype.country = null;

/**
 * 
 *
 * @var string
 */
KontorolUser.prototype.state = null;

/**
 * 
 *
 * @var string
 */
KontorolUser.prototype.city = null;

/**
 * 
 *
 * @var string
 */
KontorolUser.prototype.zip = null;

/**
 * 
 *
 * @var string
 */
KontorolUser.prototype.thumbnailUrl = null;

/**
 * 
 *
 * @var string
 */
KontorolUser.prototype.description = null;

/**
 * 
 *
 * @var string
 */
KontorolUser.prototype.tags = null;

/**
 * Admin tags can be updated only by using an admin session
 *
 * @var string
 */
KontorolUser.prototype.adminTags = null;

/**
 * 
 *
 * @var KontorolGender
 */
KontorolUser.prototype.gender = null;

/**
 * 
 *
 * @var KontorolUserStatus
 */
KontorolUser.prototype.status = null;

/**
 * Creation date as Unix timestamp (In seconds)
 *
 * @var int
 * @readonly
 */
KontorolUser.prototype.createdAt = null;

/**
 * Last update date as Unix timestamp (In seconds)
 *
 * @var int
 * @readonly
 */
KontorolUser.prototype.updatedAt = null;

/**
 * Can be used to store various partner related data as a string 
 *
 * @var string
 */
KontorolUser.prototype.partnerData = null;

/**
 * 
 *
 * @var int
 */
KontorolUser.prototype.indexedPartnerDataInt = null;

/**
 * 
 *
 * @var string
 */
KontorolUser.prototype.indexedPartnerDataString = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolUser.prototype.storageSize = null;


function KontorolUserFilter()
{
}
KontorolUserFilter.prototype = new KontorolFilter();
/**
 * 
 *
 * @var string
 */
KontorolUserFilter.prototype.idEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolUserFilter.prototype.idIn = null;

/**
 * 
 *
 * @var int
 */
KontorolUserFilter.prototype.partnerIdEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolUserFilter.prototype.screenNameLike = null;

/**
 * 
 *
 * @var string
 */
KontorolUserFilter.prototype.screenNameStartsWith = null;

/**
 * 
 *
 * @var string
 */
KontorolUserFilter.prototype.emailLike = null;

/**
 * 
 *
 * @var string
 */
KontorolUserFilter.prototype.emailStartsWith = null;

/**
 * 
 *
 * @var string
 */
KontorolUserFilter.prototype.tagsMultiLikeOr = null;

/**
 * 
 *
 * @var string
 */
KontorolUserFilter.prototype.tagsMultiLikeAnd = null;

/**
 * 
 *
 * @var int
 */
KontorolUserFilter.prototype.createdAtGreaterThanOrEqual = null;

/**
 * 
 *
 * @var int
 */
KontorolUserFilter.prototype.createdAtLessThanOrEqual = null;


function KontorolUserListResponse()
{
}
KontorolUserListResponse.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var KontorolUserArray
 * @readonly
 */
KontorolUserListResponse.prototype.objects = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolUserListResponse.prototype.totalCount = null;


function KontorolWidget()
{
}
KontorolWidget.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var string
 * @readonly
 */
KontorolWidget.prototype.id = null;

/**
 * 
 *
 * @var string
 */
KontorolWidget.prototype.sourceWidgetId = null;

/**
 * 
 *
 * @var string
 * @readonly
 */
KontorolWidget.prototype.rootWidgetId = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolWidget.prototype.partnerId = null;

/**
 * 
 *
 * @var string
 */
KontorolWidget.prototype.entryId = null;

/**
 * 
 *
 * @var int
 */
KontorolWidget.prototype.uiConfId = null;

/**
 * 
 *
 * @var KontorolWidgetSecurityType
 */
KontorolWidget.prototype.securityType = null;

/**
 * 
 *
 * @var int
 */
KontorolWidget.prototype.securityPolicy = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolWidget.prototype.createdAt = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolWidget.prototype.updatedAt = null;

/**
 * Can be used to store various partner related data as a string 
 *
 * @var string
 */
KontorolWidget.prototype.partnerData = null;

/**
 * 
 *
 * @var string
 * @readonly
 */
KontorolWidget.prototype.widgetHTML = null;


function KontorolWidgetFilter()
{
}
KontorolWidgetFilter.prototype = new KontorolFilter();
/**
 * 
 *
 * @var string
 */
KontorolWidgetFilter.prototype.idEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolWidgetFilter.prototype.idIn = null;

/**
 * 
 *
 * @var string
 */
KontorolWidgetFilter.prototype.sourceWidgetIdEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolWidgetFilter.prototype.rootWidgetIdEqual = null;

/**
 * 
 *
 * @var int
 */
KontorolWidgetFilter.prototype.partnerIdEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolWidgetFilter.prototype.entryIdEqual = null;

/**
 * 
 *
 * @var int
 */
KontorolWidgetFilter.prototype.uiConfIdEqual = null;

/**
 * 
 *
 * @var int
 */
KontorolWidgetFilter.prototype.createdAtGreaterThanOrEqual = null;

/**
 * 
 *
 * @var int
 */
KontorolWidgetFilter.prototype.createdAtLessThanOrEqual = null;

/**
 * 
 *
 * @var int
 */
KontorolWidgetFilter.prototype.updatedAtGreaterThanOrEqual = null;

/**
 * 
 *
 * @var int
 */
KontorolWidgetFilter.prototype.updatedAtLessThanOrEqual = null;

/**
 * 
 *
 * @var string
 */
KontorolWidgetFilter.prototype.partnerDataLike = null;


function KontorolWidgetListResponse()
{
}
KontorolWidgetListResponse.prototype = new KontorolObjectBase();
/**
 * 
 *
 * @var KontorolWidgetArray
 * @readonly
 */
KontorolWidgetListResponse.prototype.objects = null;

/**
 * 
 *
 * @var int
 * @readonly
 */
KontorolWidgetListResponse.prototype.totalCount = null;


function KontorolYahooSyndicationFeed()
{
}
KontorolYahooSyndicationFeed.prototype = new KontorolBaseSyndicationFeed();
/**
 * 
 *
 * @var KontorolYahooSyndicationFeedCategories
 * @readonly
 */
KontorolYahooSyndicationFeed.prototype.category = null;

/**
 * 
 *
 * @var KontorolYahooSyndicationFeedAdultValues
 */
KontorolYahooSyndicationFeed.prototype.adultContent = null;

/**
 * feed description
	 * 
 *
 * @var string
 */
KontorolYahooSyndicationFeed.prototype.feedDescription = null;

/**
 * feed landing page (i.e publisher website)
	 * 
 *
 * @var string
 */
KontorolYahooSyndicationFeed.prototype.feedLandingPage = null;


function KontorolYahooSyndicationFeedFilter()
{
}
KontorolYahooSyndicationFeedFilter.prototype = new KontorolBaseSyndicationFeedFilter();


function KontorolAccessControlService(client)
{
	this.init(client);
}

KontorolAccessControlService.prototype = new KontorolServiceBase();

KontorolAccessControlService.prototype.add = function(callback, accessControl)
{

	kparams = new Object();
	this.client.addParam(kparams, "accessControl", accessControl.toParams());
	this.client.queueServiceActionCall("accesscontrol", "add", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolAccessControlService.prototype.get = function(callback, id)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.queueServiceActionCall("accesscontrol", "get", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolAccessControlService.prototype.update = function(callback, id, accessControl)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.addParam(kparams, "accessControl", accessControl.toParams());
	this.client.queueServiceActionCall("accesscontrol", "update", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolAccessControlService.prototype.delete = function(callback, id)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.queueServiceActionCall("accesscontrol", "delete", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolAccessControlService.prototype.listAction = function(callback, filter, pager)
{
	if(!filter)
		filter = null;
	if(!pager)
		pager = null;

	kparams = new Object();
	if (filter != null)
		this.client.addParam(kparams, "filter", filter.toParams());
	if (pager != null)
		this.client.addParam(kparams, "pager", pager.toParams());
	this.client.queueServiceActionCall("accesscontrol", "list", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolAdminconsoleService(client)
{
	this.init(client);
}

KontorolAdminconsoleService.prototype = new KontorolServiceBase();

KontorolAdminconsoleService.prototype.listBatchJobs = function(callback, filter, pager)
{
	if(!filter)
		filter = null;
	if(!pager)
		pager = null;

	kparams = new Object();
	if (filter != null)
		this.client.addParam(kparams, "filter", toParams(filter));
	if (pager != null)
		this.client.addParam(kparams, "pager", toParams(pager));
	this.client.queueServiceActionCall("adminconsole", "listBatchJobs", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolAdminUserService(client)
{
	this.init(client);
}

KontorolAdminUserService.prototype = new KontorolServiceBase();

KontorolAdminUserService.prototype.updatePassword = function(callback, email, password, newEmail, newPassword)
{
	if(!newEmail)
		newEmail = "";
	if(!newPassword)
		newPassword = "";

	kparams = new Object();
	this.client.addParam(kparams, "email", email);
	this.client.addParam(kparams, "password", password);
	this.client.addParam(kparams, "newEmail", newEmail);
	this.client.addParam(kparams, "newPassword", newPassword);
	this.client.queueServiceActionCall("adminuser", "updatePassword", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolAdminUserService.prototype.resetPassword = function(callback, email)
{

	kparams = new Object();
	this.client.addParam(kparams, "email", email);
	this.client.queueServiceActionCall("adminuser", "resetPassword", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolAdminUserService.prototype.login = function(callback, email, password)
{

	kparams = new Object();
	this.client.addParam(kparams, "email", email);
	this.client.addParam(kparams, "password", password);
	this.client.queueServiceActionCall("adminuser", "login", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolBaseEntryService(client)
{
	this.init(client);
}

KontorolBaseEntryService.prototype = new KontorolServiceBase();

KontorolBaseEntryService.prototype.addFromUploadedFile = function(callback, entry, uploadTokenId, type)
{
	if(!type)
		type = -1;

	kparams = new Object();
	this.client.addParam(kparams, "entry", entry.toParams());
	this.client.addParam(kparams, "uploadTokenId", uploadTokenId);
	this.client.addParam(kparams, "type", type);
	this.client.queueServiceActionCall("baseentry", "addFromUploadedFile", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolBaseEntryService.prototype.get = function(callback, entryId, version)
{
	if(!version)
		version = -1;

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.addParam(kparams, "version", version);
	this.client.queueServiceActionCall("baseentry", "get", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolBaseEntryService.prototype.update = function(callback, entryId, baseEntry)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.addParam(kparams, "baseEntry", baseEntry.toParams());
	this.client.queueServiceActionCall("baseentry", "update", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolBaseEntryService.prototype.getByIds = function(callback, entryIds)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryIds", entryIds);
	this.client.queueServiceActionCall("baseentry", "getByIds", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolBaseEntryService.prototype.delete = function(callback, entryId)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.queueServiceActionCall("baseentry", "delete", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolBaseEntryService.prototype.listAction = function(callback, filter, pager)
{
	if(!filter)
		filter = null;
	if(!pager)
		pager = null;

	kparams = new Object();
	if (filter != null)
		this.client.addParam(kparams, "filter", filter.toParams());
	if (pager != null)
		this.client.addParam(kparams, "pager", pager.toParams());
	this.client.queueServiceActionCall("baseentry", "list", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolBaseEntryService.prototype.count = function(callback, filter)
{
	if(!filter)
		filter = null;

	kparams = new Object();
	if (filter != null)
		this.client.addParam(kparams, "filter", filter.toParams());
	this.client.queueServiceActionCall("baseentry", "count", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolBaseEntryService.prototype.upload = function(callback, fileData)
{

	kparams = new Object();
	kfiles = new Object();
	this.client.addParam(kfiles, "fileData", fileData);
	this.client.queueServiceActionCall("baseentry", "upload", kparams, kfiles);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolBaseEntryService.prototype.updateThumbnailJpeg = function(callback, entryId, fileData)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	kfiles = new Object();
	this.client.addParam(kfiles, "fileData", fileData);
	this.client.queueServiceActionCall("baseentry", "updateThumbnailJpeg", kparams, kfiles);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolBaseEntryService.prototype.updateThumbnailFromUrl = function(callback, entryId, url)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.addParam(kparams, "url", url);
	this.client.queueServiceActionCall("baseentry", "updateThumbnailFromUrl", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolBaseEntryService.prototype.updateThumbnailFromSourceEntry = function(callback, entryId, sourceEntryId, timeOffset)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.addParam(kparams, "sourceEntryId", sourceEntryId);
	this.client.addParam(kparams, "timeOffset", timeOffset);
	this.client.queueServiceActionCall("baseentry", "updateThumbnailFromSourceEntry", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolBaseEntryService.prototype.flag = function(callback, moderationFlag)
{

	kparams = new Object();
	this.client.addParam(kparams, "moderationFlag", moderationFlag.toParams());
	this.client.queueServiceActionCall("baseentry", "flag", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolBaseEntryService.prototype.reject = function(callback, entryId)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.queueServiceActionCall("baseentry", "reject", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolBaseEntryService.prototype.approve = function(callback, entryId)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.queueServiceActionCall("baseentry", "approve", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolBaseEntryService.prototype.listFlags = function(callback, entryId, pager)
{
	if(!pager)
		pager = null;

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	if (pager != null)
		this.client.addParam(kparams, "pager", pager.toParams());
	this.client.queueServiceActionCall("baseentry", "listFlags", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolBaseEntryService.prototype.anonymousRank = function(callback, entryId, rank)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.addParam(kparams, "rank", rank);
	this.client.queueServiceActionCall("baseentry", "anonymousRank", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolBaseEntryService.prototype.getExtraData = function(callback, entryId, extraDataParams)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.addParam(kparams, "extraDataParams", extraDataParams.toParams());
	this.client.queueServiceActionCall("baseentry", "getExtraData", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolBulkUploadService(client)
{
	this.init(client);
}

KontorolBulkUploadService.prototype = new KontorolServiceBase();

KontorolBulkUploadService.prototype.add = function(callback, conversionProfileId, csvFileData)
{

	kparams = new Object();
	this.client.addParam(kparams, "conversionProfileId", conversionProfileId);
	kfiles = new Object();
	this.client.addParam(kfiles, "csvFileData", csvFileData);
	this.client.queueServiceActionCall("bulkupload", "add", kparams, kfiles);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolBulkUploadService.prototype.get = function(callback, id)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.queueServiceActionCall("bulkupload", "get", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolBulkUploadService.prototype.listAction = function(callback, pager)
{
	if(!pager)
		pager = null;

	kparams = new Object();
	if (pager != null)
		this.client.addParam(kparams, "pager", pager.toParams());
	this.client.queueServiceActionCall("bulkupload", "list", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolCategoryService(client)
{
	this.init(client);
}

KontorolCategoryService.prototype = new KontorolServiceBase();

KontorolCategoryService.prototype.add = function(callback, category)
{

	kparams = new Object();
	this.client.addParam(kparams, "category", category.toParams());
	this.client.queueServiceActionCall("category", "add", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolCategoryService.prototype.get = function(callback, id)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.queueServiceActionCall("category", "get", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolCategoryService.prototype.update = function(callback, id, category)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.addParam(kparams, "category", category.toParams());
	this.client.queueServiceActionCall("category", "update", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolCategoryService.prototype.delete = function(callback, id)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.queueServiceActionCall("category", "delete", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolCategoryService.prototype.listAction = function(callback, filter)
{
	if(!filter)
		filter = null;

	kparams = new Object();
	if (filter != null)
		this.client.addParam(kparams, "filter", filter.toParams());
	this.client.queueServiceActionCall("category", "list", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolConversionProfileService(client)
{
	this.init(client);
}

KontorolConversionProfileService.prototype = new KontorolServiceBase();

KontorolConversionProfileService.prototype.add = function(callback, conversionProfile)
{

	kparams = new Object();
	this.client.addParam(kparams, "conversionProfile", conversionProfile.toParams());
	this.client.queueServiceActionCall("conversionprofile", "add", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolConversionProfileService.prototype.get = function(callback, id)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.queueServiceActionCall("conversionprofile", "get", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolConversionProfileService.prototype.update = function(callback, id, conversionProfile)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.addParam(kparams, "conversionProfile", conversionProfile.toParams());
	this.client.queueServiceActionCall("conversionprofile", "update", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolConversionProfileService.prototype.delete = function(callback, id)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.queueServiceActionCall("conversionprofile", "delete", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolConversionProfileService.prototype.listAction = function(callback, filter, pager)
{
	if(!filter)
		filter = null;
	if(!pager)
		pager = null;

	kparams = new Object();
	if (filter != null)
		this.client.addParam(kparams, "filter", filter.toParams());
	if (pager != null)
		this.client.addParam(kparams, "pager", pager.toParams());
	this.client.queueServiceActionCall("conversionprofile", "list", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolDataService(client)
{
	this.init(client);
}

KontorolDataService.prototype = new KontorolServiceBase();

KontorolDataService.prototype.add = function(callback, dataEntry)
{

	kparams = new Object();
	this.client.addParam(kparams, "dataEntry", dataEntry.toParams());
	this.client.queueServiceActionCall("data", "add", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolDataService.prototype.get = function(callback, entryId, version)
{
	if(!version)
		version = -1;

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.addParam(kparams, "version", version);
	this.client.queueServiceActionCall("data", "get", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolDataService.prototype.update = function(callback, entryId, documentEntry)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.addParam(kparams, "documentEntry", documentEntry.toParams());
	this.client.queueServiceActionCall("data", "update", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolDataService.prototype.delete = function(callback, entryId)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.queueServiceActionCall("data", "delete", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolDataService.prototype.listAction = function(callback, filter, pager)
{
	if(!filter)
		filter = null;
	if(!pager)
		pager = null;

	kparams = new Object();
	if (filter != null)
		this.client.addParam(kparams, "filter", filter.toParams());
	if (pager != null)
		this.client.addParam(kparams, "pager", pager.toParams());
	this.client.queueServiceActionCall("data", "list", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolFlavorAssetService(client)
{
	this.init(client);
}

KontorolFlavorAssetService.prototype = new KontorolServiceBase();

KontorolFlavorAssetService.prototype.get = function(callback, id)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.queueServiceActionCall("flavorasset", "get", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolFlavorAssetService.prototype.getByEntryId = function(callback, entryId)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.queueServiceActionCall("flavorasset", "getByEntryId", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolFlavorAssetService.prototype.getWebPlayableByEntryId = function(callback, entryId)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.queueServiceActionCall("flavorasset", "getWebPlayableByEntryId", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolFlavorAssetService.prototype.convert = function(callback, entryId, flavorParamsId)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.addParam(kparams, "flavorParamsId", flavorParamsId);
	this.client.queueServiceActionCall("flavorasset", "convert", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolFlavorAssetService.prototype.reconvert = function(callback, id)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.queueServiceActionCall("flavorasset", "reconvert", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolFlavorAssetService.prototype.delete = function(callback, id)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.queueServiceActionCall("flavorasset", "delete", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolFlavorAssetService.prototype.getDownloadUrl = function(callback, id)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.queueServiceActionCall("flavorasset", "getDownloadUrl", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolFlavorAssetService.prototype.getFlavorAssetsWithParams = function(callback, entryId)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.queueServiceActionCall("flavorasset", "getFlavorAssetsWithParams", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolFlavorParamsService(client)
{
	this.init(client);
}

KontorolFlavorParamsService.prototype = new KontorolServiceBase();

KontorolFlavorParamsService.prototype.add = function(callback, flavorParams)
{

	kparams = new Object();
	this.client.addParam(kparams, "flavorParams", flavorParams.toParams());
	this.client.queueServiceActionCall("flavorparams", "add", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolFlavorParamsService.prototype.get = function(callback, id)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.queueServiceActionCall("flavorparams", "get", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolFlavorParamsService.prototype.update = function(callback, id, flavorParams)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.addParam(kparams, "flavorParams", flavorParams.toParams());
	this.client.queueServiceActionCall("flavorparams", "update", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolFlavorParamsService.prototype.delete = function(callback, id)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.queueServiceActionCall("flavorparams", "delete", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolFlavorParamsService.prototype.listAction = function(callback, filter, pager)
{
	if(!filter)
		filter = null;
	if(!pager)
		pager = null;

	kparams = new Object();
	if (filter != null)
		this.client.addParam(kparams, "filter", filter.toParams());
	if (pager != null)
		this.client.addParam(kparams, "pager", pager.toParams());
	this.client.queueServiceActionCall("flavorparams", "list", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolFlavorParamsService.prototype.getByConversionProfileId = function(callback, conversionProfileId)
{

	kparams = new Object();
	this.client.addParam(kparams, "conversionProfileId", conversionProfileId);
	this.client.queueServiceActionCall("flavorparams", "getByConversionProfileId", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolJobsService(client)
{
	this.init(client);
}

KontorolJobsService.prototype = new KontorolServiceBase();

KontorolJobsService.prototype.getImportStatus = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "getImportStatus", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.deleteImport = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "deleteImport", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.abortImport = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "abortImport", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.retryImport = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "retryImport", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.getBulkUploadStatus = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "getBulkUploadStatus", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.deleteBulkUpload = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "deleteBulkUpload", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.abortBulkUpload = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "abortBulkUpload", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.retryBulkUpload = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "retryBulkUpload", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.getConvertStatus = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "getConvertStatus", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.getConvertProfileStatus = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "getConvertProfileStatus", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.getRemoteConvertStatus = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "getRemoteConvertStatus", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.deleteConvert = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "deleteConvert", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.abortConvert = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "abortConvert", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.retryConvert = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "retryConvert", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.deleteRemoteConvert = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "deleteRemoteConvert", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.abortRemoteConvert = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "abortRemoteConvert", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.retryRemoteConvert = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "retryRemoteConvert", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.deleteConvertProfile = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "deleteConvertProfile", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.abortConvertProfile = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "abortConvertProfile", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.retryConvertProfile = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "retryConvertProfile", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.getPostConvertStatus = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "getPostConvertStatus", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.deletePostConvert = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "deletePostConvert", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.abortPostConvert = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "abortPostConvert", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.retryPostConvert = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "retryPostConvert", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.getPullStatus = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "getPullStatus", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.deletePull = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "deletePull", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.abortPull = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "abortPull", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.retryPull = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "retryPull", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.getExtractMediaStatus = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "getExtractMediaStatus", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.deleteExtractMedia = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "deleteExtractMedia", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.abortExtractMedia = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "abortExtractMedia", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.retryExtractMedia = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "retryExtractMedia", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.getNotificationStatus = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "getNotificationStatus", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.deleteNotification = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "deleteNotification", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.abortNotification = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "abortNotification", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.retryNotification = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "retryNotification", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.getMailStatus = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "getMailStatus", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.deleteMail = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "deleteMail", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.abortMail = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "abortMail", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.retryMail = function(callback, jobId)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.queueServiceActionCall("jobs", "retryMail", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.addMailJob = function(callback, mailJobData)
{

	kparams = new Object();
	this.client.addParam(kparams, "mailJobData", mailJobData.toParams());
	this.client.queueServiceActionCall("jobs", "addMailJob", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.addBatchJob = function(callback, batchJob)
{

	kparams = new Object();
	this.client.addParam(kparams, "batchJob", batchJob.toParams());
	this.client.queueServiceActionCall("jobs", "addBatchJob", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.getStatus = function(callback, jobId, jobType)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.addParam(kparams, "jobType", jobType);
	this.client.queueServiceActionCall("jobs", "getStatus", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.deleteJob = function(callback, jobId, jobType)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.addParam(kparams, "jobType", jobType);
	this.client.queueServiceActionCall("jobs", "deleteJob", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.abortJob = function(callback, jobId, jobType)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.addParam(kparams, "jobType", jobType);
	this.client.queueServiceActionCall("jobs", "abortJob", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.retryJob = function(callback, jobId, jobType)
{

	kparams = new Object();
	this.client.addParam(kparams, "jobId", jobId);
	this.client.addParam(kparams, "jobType", jobType);
	this.client.queueServiceActionCall("jobs", "retryJob", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolJobsService.prototype.listBatchJobs = function(callback, filter, pager)
{
	if(!filter)
		filter = null;
	if(!pager)
		pager = null;

	kparams = new Object();
	if (filter != null)
		this.client.addParam(kparams, "filter", toParams(filter));
	if (pager != null)
		this.client.addParam(kparams, "pager", toParams(pager));
	this.client.queueServiceActionCall("jobs", "listBatchJobs", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolMediaService(client)
{
	this.init(client);
}

KontorolMediaService.prototype = new KontorolServiceBase();

KontorolMediaService.prototype.addFromBulk = function(callback, mediaEntry, url, bulkUploadId)
{

	kparams = new Object();
	this.client.addParam(kparams, "mediaEntry", mediaEntry.toParams());
	this.client.addParam(kparams, "url", url);
	this.client.addParam(kparams, "bulkUploadId", bulkUploadId);
	this.client.queueServiceActionCall("media", "addFromBulk", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMediaService.prototype.addFromUrl = function(callback, mediaEntry, url)
{

	kparams = new Object();
	this.client.addParam(kparams, "mediaEntry", mediaEntry.toParams());
	this.client.addParam(kparams, "url", url);
	this.client.queueServiceActionCall("media", "addFromUrl", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMediaService.prototype.addFromSearchResult = function(callback, mediaEntry, searchResult)
{
	if(!mediaEntry)
		mediaEntry = null;
	if(!searchResult)
		searchResult = null;

	kparams = new Object();
	if (mediaEntry != null)
		this.client.addParam(kparams, "mediaEntry", mediaEntry.toParams());
	if (searchResult != null)
		this.client.addParam(kparams, "searchResult", searchResult.toParams());
	this.client.queueServiceActionCall("media", "addFromSearchResult", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMediaService.prototype.addFromUploadedFile = function(callback, mediaEntry, uploadTokenId)
{

	kparams = new Object();
	this.client.addParam(kparams, "mediaEntry", mediaEntry.toParams());
	this.client.addParam(kparams, "uploadTokenId", uploadTokenId);
	this.client.queueServiceActionCall("media", "addFromUploadedFile", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMediaService.prototype.addFromRecordedWebcam = function(callback, mediaEntry, webcamTokenId)
{

	kparams = new Object();
	this.client.addParam(kparams, "mediaEntry", mediaEntry.toParams());
	this.client.addParam(kparams, "webcamTokenId", webcamTokenId);
	this.client.queueServiceActionCall("media", "addFromRecordedWebcam", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMediaService.prototype.get = function(callback, entryId, version)
{
	if(!version)
		version = -1;

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.addParam(kparams, "version", version);
	this.client.queueServiceActionCall("media", "get", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMediaService.prototype.update = function(callback, entryId, mediaEntry)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.addParam(kparams, "mediaEntry", mediaEntry.toParams());
	this.client.queueServiceActionCall("media", "update", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMediaService.prototype.delete = function(callback, entryId)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.queueServiceActionCall("media", "delete", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMediaService.prototype.listAction = function(callback, filter, pager)
{
	if(!filter)
		filter = null;
	if(!pager)
		pager = null;

	kparams = new Object();
	if (filter != null)
		this.client.addParam(kparams, "filter", filter.toParams());
	if (pager != null)
		this.client.addParam(kparams, "pager", pager.toParams());
	this.client.queueServiceActionCall("media", "list", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMediaService.prototype.count = function(callback, filter)
{
	if(!filter)
		filter = null;

	kparams = new Object();
	if (filter != null)
		this.client.addParam(kparams, "filter", filter.toParams());
	this.client.queueServiceActionCall("media", "count", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMediaService.prototype.upload = function(callback, fileData)
{

	kparams = new Object();
	kfiles = new Object();
	this.client.addParam(kfiles, "fileData", fileData);
	this.client.queueServiceActionCall("media", "upload", kparams, kfiles);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMediaService.prototype.updateThumbnail = function(callback, entryId, timeOffset)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.addParam(kparams, "timeOffset", timeOffset);
	this.client.queueServiceActionCall("media", "updateThumbnail", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMediaService.prototype.updateThumbnailFromSourceEntry = function(callback, entryId, sourceEntryId, timeOffset)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.addParam(kparams, "sourceEntryId", sourceEntryId);
	this.client.addParam(kparams, "timeOffset", timeOffset);
	this.client.queueServiceActionCall("media", "updateThumbnailFromSourceEntry", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMediaService.prototype.updateThumbnailJpeg = function(callback, entryId, fileData)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	kfiles = new Object();
	this.client.addParam(kfiles, "fileData", fileData);
	this.client.queueServiceActionCall("media", "updateThumbnailJpeg", kparams, kfiles);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMediaService.prototype.updateThumbnailFromUrl = function(callback, entryId, url)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.addParam(kparams, "url", url);
	this.client.queueServiceActionCall("media", "updateThumbnailFromUrl", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMediaService.prototype.requestConversion = function(callback, entryId, fileFormat)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.addParam(kparams, "fileFormat", fileFormat);
	this.client.queueServiceActionCall("media", "requestConversion", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMediaService.prototype.flag = function(callback, moderationFlag)
{

	kparams = new Object();
	this.client.addParam(kparams, "moderationFlag", moderationFlag.toParams());
	this.client.queueServiceActionCall("media", "flag", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMediaService.prototype.reject = function(callback, entryId)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.queueServiceActionCall("media", "reject", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMediaService.prototype.approve = function(callback, entryId)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.queueServiceActionCall("media", "approve", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMediaService.prototype.listFlags = function(callback, entryId, pager)
{
	if(!pager)
		pager = null;

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	if (pager != null)
		this.client.addParam(kparams, "pager", pager.toParams());
	this.client.queueServiceActionCall("media", "listFlags", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMediaService.prototype.anonymousRank = function(callback, entryId, rank)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.addParam(kparams, "rank", rank);
	this.client.queueServiceActionCall("media", "anonymousRank", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolMixingService(client)
{
	this.init(client);
}

KontorolMixingService.prototype = new KontorolServiceBase();

KontorolMixingService.prototype.add = function(callback, mixEntry)
{

	kparams = new Object();
	this.client.addParam(kparams, "mixEntry", mixEntry.toParams());
	this.client.queueServiceActionCall("mixing", "add", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMixingService.prototype.get = function(callback, entryId, version)
{
	if(!version)
		version = -1;

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.addParam(kparams, "version", version);
	this.client.queueServiceActionCall("mixing", "get", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMixingService.prototype.update = function(callback, entryId, mixEntry)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.addParam(kparams, "mixEntry", mixEntry.toParams());
	this.client.queueServiceActionCall("mixing", "update", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMixingService.prototype.delete = function(callback, entryId)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.queueServiceActionCall("mixing", "delete", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMixingService.prototype.listAction = function(callback, filter, pager)
{
	if(!filter)
		filter = null;
	if(!pager)
		pager = null;

	kparams = new Object();
	if (filter != null)
		this.client.addParam(kparams, "filter", filter.toParams());
	if (pager != null)
		this.client.addParam(kparams, "pager", pager.toParams());
	this.client.queueServiceActionCall("mixing", "list", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMixingService.prototype.count = function(callback, filter)
{
	if(!filter)
		filter = null;

	kparams = new Object();
	if (filter != null)
		this.client.addParam(kparams, "filter", filter.toParams());
	this.client.queueServiceActionCall("mixing", "count", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMixingService.prototype.cloneAction = function(callback, entryId)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.queueServiceActionCall("mixing", "clone", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMixingService.prototype.appendMediaEntry = function(callback, mixEntryId, mediaEntryId)
{

	kparams = new Object();
	this.client.addParam(kparams, "mixEntryId", mixEntryId);
	this.client.addParam(kparams, "mediaEntryId", mediaEntryId);
	this.client.queueServiceActionCall("mixing", "appendMediaEntry", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMixingService.prototype.requestFlattening = function(callback, entryId, fileFormat, version)
{
	if(!version)
		version = -1;

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.addParam(kparams, "fileFormat", fileFormat);
	this.client.addParam(kparams, "version", version);
	this.client.queueServiceActionCall("mixing", "requestFlattening", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMixingService.prototype.getMixesByMediaId = function(callback, mediaEntryId)
{

	kparams = new Object();
	this.client.addParam(kparams, "mediaEntryId", mediaEntryId);
	this.client.queueServiceActionCall("mixing", "getMixesByMediaId", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMixingService.prototype.getReadyMediaEntries = function(callback, mixId, version)
{
	if(!version)
		version = -1;

	kparams = new Object();
	this.client.addParam(kparams, "mixId", mixId);
	this.client.addParam(kparams, "version", version);
	this.client.queueServiceActionCall("mixing", "getReadyMediaEntries", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolMixingService.prototype.anonymousRank = function(callback, entryId, rank)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.addParam(kparams, "rank", rank);
	this.client.queueServiceActionCall("mixing", "anonymousRank", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolNotificationService(client)
{
	this.init(client);
}

KontorolNotificationService.prototype = new KontorolServiceBase();

KontorolNotificationService.prototype.getClientNotification = function(callback, entryId, type)
{

	kparams = new Object();
	this.client.addParam(kparams, "entryId", entryId);
	this.client.addParam(kparams, "type", type);
	this.client.queueServiceActionCall("notification", "getClientNotification", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolPartnerService(client)
{
	this.init(client);
}

KontorolPartnerService.prototype = new KontorolServiceBase();

KontorolPartnerService.prototype.register = function(callback, partner, cmsPassword)
{
	if(!cmsPassword)
		cmsPassword = "";

	kparams = new Object();
	this.client.addParam(kparams, "partner", partner.toParams());
	this.client.addParam(kparams, "cmsPassword", cmsPassword);
	this.client.queueServiceActionCall("partner", "register", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolPartnerService.prototype.update = function(callback, partner, allowEmpty)
{
	if(!allowEmpty)
		allowEmpty = false;

	kparams = new Object();
	this.client.addParam(kparams, "partner", partner.toParams());
	this.client.addParam(kparams, "allowEmpty", allowEmpty);
	this.client.queueServiceActionCall("partner", "update", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolPartnerService.prototype.getSecrets = function(callback, partnerId, adminEmail, cmsPassword)
{

	kparams = new Object();
	this.client.addParam(kparams, "partnerId", partnerId);
	this.client.addParam(kparams, "adminEmail", adminEmail);
	this.client.addParam(kparams, "cmsPassword", cmsPassword);
	this.client.queueServiceActionCall("partner", "getSecrets", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolPartnerService.prototype.getInfo = function(callback)
{

	kparams = new Object();
	this.client.queueServiceActionCall("partner", "getInfo", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolPartnerService.prototype.get = function(callback, partnerId)
{

	kparams = new Object();
	this.client.addParam(kparams, "partnerId", partnerId);
	this.client.queueServiceActionCall("partner", "get", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolPartnerService.prototype.getUsage = function(callback, year, month, resolution)
{
	if(!year)
		year = "";
	if(!month)
		month = 1;
	if(!resolution)
		resolution = "days";

	kparams = new Object();
	this.client.addParam(kparams, "year", year);
	this.client.addParam(kparams, "month", month);
	this.client.addParam(kparams, "resolution", resolution);
	this.client.queueServiceActionCall("partner", "getUsage", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolPlaylistService(client)
{
	this.init(client);
}

KontorolPlaylistService.prototype = new KontorolServiceBase();

KontorolPlaylistService.prototype.add = function(callback, playlist, updateStats)
{
	if(!updateStats)
		updateStats = false;

	kparams = new Object();
	this.client.addParam(kparams, "playlist", playlist.toParams());
	this.client.addParam(kparams, "updateStats", updateStats);
	this.client.queueServiceActionCall("playlist", "add", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolPlaylistService.prototype.get = function(callback, id, version)
{
	if(!version)
		version = -1;

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.addParam(kparams, "version", version);
	this.client.queueServiceActionCall("playlist", "get", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolPlaylistService.prototype.update = function(callback, id, playlist, updateStats)
{
	if(!updateStats)
		updateStats = false;

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.addParam(kparams, "playlist", playlist.toParams());
	this.client.addParam(kparams, "updateStats", updateStats);
	this.client.queueServiceActionCall("playlist", "update", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolPlaylistService.prototype.delete = function(callback, id)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.queueServiceActionCall("playlist", "delete", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolPlaylistService.prototype.listAction = function(callback, filter, pager)
{
	if(!filter)
		filter = null;
	if(!pager)
		pager = null;

	kparams = new Object();
	if (filter != null)
		this.client.addParam(kparams, "filter", filter.toParams());
	if (pager != null)
		this.client.addParam(kparams, "pager", pager.toParams());
	this.client.queueServiceActionCall("playlist", "list", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolPlaylistService.prototype.execute = function(callback, id, detailed)
{
	if(!detailed)
		detailed = false;

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.addParam(kparams, "detailed", detailed);
	this.client.queueServiceActionCall("playlist", "execute", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolPlaylistService.prototype.executeFromContent = function(callback, playlistType, playlistContent, detailed)
{
	if(!detailed)
		detailed = false;

	kparams = new Object();
	this.client.addParam(kparams, "playlistType", playlistType);
	this.client.addParam(kparams, "playlistContent", playlistContent);
	this.client.addParam(kparams, "detailed", detailed);
	this.client.queueServiceActionCall("playlist", "executeFromContent", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolPlaylistService.prototype.executeFromFilters = function(callback, filters, totalResults, detailed)
{
	if(!detailed)
		detailed = false;

	kparams = new Object();
	for(var index in filters)
	{
		var obj = filters[index];
		this.client.addParam(kparams, "filters:" + index, obj.toParams());
	}
	this.client.addParam(kparams, "totalResults", totalResults);
	this.client.addParam(kparams, "detailed", detailed);
	this.client.queueServiceActionCall("playlist", "executeFromFilters", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolPlaylistService.prototype.getStatsFromContent = function(callback, playlistType, playlistContent)
{

	kparams = new Object();
	this.client.addParam(kparams, "playlistType", playlistType);
	this.client.addParam(kparams, "playlistContent", playlistContent);
	this.client.queueServiceActionCall("playlist", "getStatsFromContent", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolReportService(client)
{
	this.init(client);
}

KontorolReportService.prototype = new KontorolServiceBase();

KontorolReportService.prototype.getGraphs = function(callback, reportType, reportInputFilter, dimension, objectIds)
{
	if(!dimension)
		dimension = null;
	if(!objectIds)
		objectIds = null;

	kparams = new Object();
	this.client.addParam(kparams, "reportType", reportType);
	this.client.addParam(kparams, "reportInputFilter", reportInputFilter.toParams());
	this.client.addParam(kparams, "dimension", dimension);
	this.client.addParam(kparams, "objectIds", objectIds);
	this.client.queueServiceActionCall("report", "getGraphs", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolReportService.prototype.getTotal = function(callback, reportType, reportInputFilter, objectIds)
{
	if(!objectIds)
		objectIds = null;

	kparams = new Object();
	this.client.addParam(kparams, "reportType", reportType);
	this.client.addParam(kparams, "reportInputFilter", reportInputFilter.toParams());
	this.client.addParam(kparams, "objectIds", objectIds);
	this.client.queueServiceActionCall("report", "getTotal", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolReportService.prototype.getTable = function(callback, reportType, reportInputFilter, pager, order, objectIds)
{
	if(!order)
		order = null;
	if(!objectIds)
		objectIds = null;

	kparams = new Object();
	this.client.addParam(kparams, "reportType", reportType);
	this.client.addParam(kparams, "reportInputFilter", reportInputFilter.toParams());
	this.client.addParam(kparams, "pager", pager.toParams());
	this.client.addParam(kparams, "order", order);
	this.client.addParam(kparams, "objectIds", objectIds);
	this.client.queueServiceActionCall("report", "getTable", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolReportService.prototype.getUrlForReportAsCsv = function(callback, reportTitle, reportText, headers, reportType, reportInputFilter, dimension, pager, order, objectIds)
{
	if(!dimension)
		dimension = null;
	if(!pager)
		pager = null;
	if(!order)
		order = null;
	if(!objectIds)
		objectIds = null;

	kparams = new Object();
	this.client.addParam(kparams, "reportTitle", reportTitle);
	this.client.addParam(kparams, "reportText", reportText);
	this.client.addParam(kparams, "headers", headers);
	this.client.addParam(kparams, "reportType", reportType);
	this.client.addParam(kparams, "reportInputFilter", reportInputFilter.toParams());
	this.client.addParam(kparams, "dimension", dimension);
	if (pager != null)
		this.client.addParam(kparams, "pager", pager.toParams());
	this.client.addParam(kparams, "order", order);
	this.client.addParam(kparams, "objectIds", objectIds);
	this.client.queueServiceActionCall("report", "getUrlForReportAsCsv", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolSearchService(client)
{
	this.init(client);
}

KontorolSearchService.prototype = new KontorolServiceBase();

KontorolSearchService.prototype.search = function(callback, search, pager)
{
	if(!pager)
		pager = null;

	kparams = new Object();
	this.client.addParam(kparams, "search", search.toParams());
	if (pager != null)
		this.client.addParam(kparams, "pager", pager.toParams());
	this.client.queueServiceActionCall("search", "search", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolSearchService.prototype.getMediaInfo = function(callback, searchResult)
{

	kparams = new Object();
	this.client.addParam(kparams, "searchResult", searchResult.toParams());
	this.client.queueServiceActionCall("search", "getMediaInfo", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolSearchService.prototype.searchUrl = function(callback, mediaType, url)
{

	kparams = new Object();
	this.client.addParam(kparams, "mediaType", mediaType);
	this.client.addParam(kparams, "url", url);
	this.client.queueServiceActionCall("search", "searchUrl", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolSearchService.prototype.externalLogin = function(callback, searchSource, userName, password)
{

	kparams = new Object();
	this.client.addParam(kparams, "searchSource", searchSource);
	this.client.addParam(kparams, "userName", userName);
	this.client.addParam(kparams, "password", password);
	this.client.queueServiceActionCall("search", "externalLogin", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolSessionService(client)
{
	this.init(client);
}

KontorolSessionService.prototype = new KontorolServiceBase();

KontorolSessionService.prototype.start = function(callback, secret, userId, type, partnerId, expiry, privileges)
{
	if(!userId)
		userId = "";
	if(!type)
		type = 0;
	if(!partnerId)
		partnerId = -1;
	if(!expiry)
		expiry = 86400;
	if(!privileges)
		privileges = null;

	kparams = new Object();
	this.client.addParam(kparams, "secret", secret);
	this.client.addParam(kparams, "userId", userId);
	this.client.addParam(kparams, "type", type);
	this.client.addParam(kparams, "partnerId", partnerId);
	this.client.addParam(kparams, "expiry", expiry);
	this.client.addParam(kparams, "privileges", privileges);
	this.client.queueServiceActionCall("session", "start", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolSessionService.prototype.startWidgetSession = function(callback, widgetId, expiry)
{
	if(!expiry)
		expiry = 86400;

	kparams = new Object();
	this.client.addParam(kparams, "widgetId", widgetId);
	this.client.addParam(kparams, "expiry", expiry);
	this.client.queueServiceActionCall("session", "startWidgetSession", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolStatsService(client)
{
	this.init(client);
}

KontorolStatsService.prototype = new KontorolServiceBase();

KontorolStatsService.prototype.collect = function(callback, event)
{

	kparams = new Object();
	this.client.addParam(kparams, "event", event.toParams());
	this.client.queueServiceActionCall("stats", "collect", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolStatsService.prototype.kmcCollect = function(callback, kmcEvent)
{

	kparams = new Object();
	this.client.addParam(kparams, "kmcEvent", kmcEvent.toParams());
	this.client.queueServiceActionCall("stats", "kmcCollect", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolStatsService.prototype.reportKceError = function(callback, kontorolCEError)
{

	kparams = new Object();
	this.client.addParam(kparams, "kontorolCEError", kontorolCEError.toParams());
	this.client.queueServiceActionCall("stats", "reportKceError", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolSyndicationFeedService(client)
{
	this.init(client);
}

KontorolSyndicationFeedService.prototype = new KontorolServiceBase();

KontorolSyndicationFeedService.prototype.add = function(callback, syndicationFeed)
{

	kparams = new Object();
	this.client.addParam(kparams, "syndicationFeed", syndicationFeed.toParams());
	this.client.queueServiceActionCall("syndicationfeed", "add", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolSyndicationFeedService.prototype.get = function(callback, id)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.queueServiceActionCall("syndicationfeed", "get", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolSyndicationFeedService.prototype.update = function(callback, id, syndicationFeed)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.addParam(kparams, "syndicationFeed", syndicationFeed.toParams());
	this.client.queueServiceActionCall("syndicationfeed", "update", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolSyndicationFeedService.prototype.delete = function(callback, id)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.queueServiceActionCall("syndicationfeed", "delete", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolSyndicationFeedService.prototype.listAction = function(callback, filter, pager)
{
	if(!filter)
		filter = null;
	if(!pager)
		pager = null;

	kparams = new Object();
	if (filter != null)
		this.client.addParam(kparams, "filter", filter.toParams());
	if (pager != null)
		this.client.addParam(kparams, "pager", pager.toParams());
	this.client.queueServiceActionCall("syndicationfeed", "list", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolSyndicationFeedService.prototype.getEntryCount = function(callback, feedId)
{

	kparams = new Object();
	this.client.addParam(kparams, "feedId", feedId);
	this.client.queueServiceActionCall("syndicationfeed", "getEntryCount", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolSyndicationFeedService.prototype.requestConversion = function(callback, feedId)
{

	kparams = new Object();
	this.client.addParam(kparams, "feedId", feedId);
	this.client.queueServiceActionCall("syndicationfeed", "requestConversion", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolSystemService(client)
{
	this.init(client);
}

KontorolSystemService.prototype = new KontorolServiceBase();

KontorolSystemService.prototype.ping = function(callback)
{

	kparams = new Object();
	this.client.queueServiceActionCall("system", "ping", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolUiConfService(client)
{
	this.init(client);
}

KontorolUiConfService.prototype = new KontorolServiceBase();

KontorolUiConfService.prototype.add = function(callback, uiConf)
{

	kparams = new Object();
	this.client.addParam(kparams, "uiConf", uiConf.toParams());
	this.client.queueServiceActionCall("uiconf", "add", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolUiConfService.prototype.update = function(callback, id, uiConf)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.addParam(kparams, "uiConf", uiConf.toParams());
	this.client.queueServiceActionCall("uiconf", "update", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolUiConfService.prototype.get = function(callback, id)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.queueServiceActionCall("uiconf", "get", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolUiConfService.prototype.delete = function(callback, id)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.queueServiceActionCall("uiconf", "delete", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolUiConfService.prototype.cloneAction = function(callback, id)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.queueServiceActionCall("uiconf", "clone", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolUiConfService.prototype.listTemplates = function(callback, filter, pager)
{
	if(!filter)
		filter = null;
	if(!pager)
		pager = null;

	kparams = new Object();
	if (filter != null)
		this.client.addParam(kparams, "filter", filter.toParams());
	if (pager != null)
		this.client.addParam(kparams, "pager", pager.toParams());
	this.client.queueServiceActionCall("uiconf", "listTemplates", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolUiConfService.prototype.listAction = function(callback, filter, pager)
{
	if(!filter)
		filter = null;
	if(!pager)
		pager = null;

	kparams = new Object();
	if (filter != null)
		this.client.addParam(kparams, "filter", filter.toParams());
	if (pager != null)
		this.client.addParam(kparams, "pager", pager.toParams());
	this.client.queueServiceActionCall("uiconf", "list", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolUploadService(client)
{
	this.init(client);
}

KontorolUploadService.prototype = new KontorolServiceBase();

KontorolUploadService.prototype.getUploadTokenId = function(callback)
{

	kparams = new Object();
	this.client.queueServiceActionCall("upload", "getUploadTokenId", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolUploadService.prototype.uploadByTokenId = function(callback, fileData, uploadTokenId)
{

	kparams = new Object();
	kfiles = new Object();
	this.client.addParam(kfiles, "fileData", fileData);
	this.client.addParam(kparams, "uploadTokenId", uploadTokenId);
	this.client.queueServiceActionCall("upload", "uploadByTokenId", kparams, kfiles);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolUploadService.prototype.getUploadedFileStatusByTokenId = function(callback, uploadTokenId)
{

	kparams = new Object();
	this.client.addParam(kparams, "uploadTokenId", uploadTokenId);
	this.client.queueServiceActionCall("upload", "getUploadedFileStatusByTokenId", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolUploadService.prototype.upload = function(callback, fileData)
{

	kparams = new Object();
	kfiles = new Object();
	this.client.addParam(kfiles, "fileData", fileData);
	this.client.queueServiceActionCall("upload", "upload", kparams, kfiles);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolUserService(client)
{
	this.init(client);
}

KontorolUserService.prototype = new KontorolServiceBase();

KontorolUserService.prototype.add = function(callback, user)
{

	kparams = new Object();
	this.client.addParam(kparams, "user", user.toParams());
	this.client.queueServiceActionCall("user", "add", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolUserService.prototype.update = function(callback, userId, user)
{

	kparams = new Object();
	this.client.addParam(kparams, "userId", userId);
	this.client.addParam(kparams, "user", user.toParams());
	this.client.queueServiceActionCall("user", "update", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolUserService.prototype.get = function(callback, userId)
{

	kparams = new Object();
	this.client.addParam(kparams, "userId", userId);
	this.client.queueServiceActionCall("user", "get", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolUserService.prototype.delete = function(callback, userId)
{

	kparams = new Object();
	this.client.addParam(kparams, "userId", userId);
	this.client.queueServiceActionCall("user", "delete", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolUserService.prototype.listAction = function(callback, filter, pager)
{
	if(!filter)
		filter = null;
	if(!pager)
		pager = null;

	kparams = new Object();
	if (filter != null)
		this.client.addParam(kparams, "filter", filter.toParams());
	if (pager != null)
		this.client.addParam(kparams, "pager", pager.toParams());
	this.client.queueServiceActionCall("user", "list", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolUserService.prototype.notifyBan = function(callback, userId)
{

	kparams = new Object();
	this.client.addParam(kparams, "userId", userId);
	this.client.queueServiceActionCall("user", "notifyBan", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolWidgetService(client)
{
	this.init(client);
}

KontorolWidgetService.prototype = new KontorolServiceBase();

KontorolWidgetService.prototype.add = function(callback, widget)
{

	kparams = new Object();
	this.client.addParam(kparams, "widget", widget.toParams());
	this.client.queueServiceActionCall("widget", "add", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolWidgetService.prototype.update = function(callback, id, widget)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.addParam(kparams, "widget", widget.toParams());
	this.client.queueServiceActionCall("widget", "update", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolWidgetService.prototype.get = function(callback, id)
{

	kparams = new Object();
	this.client.addParam(kparams, "id", id);
	this.client.queueServiceActionCall("widget", "get", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolWidgetService.prototype.cloneAction = function(callback, widget)
{

	kparams = new Object();
	this.client.addParam(kparams, "widget", widget.toParams());
	this.client.queueServiceActionCall("widget", "clone", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolWidgetService.prototype.listAction = function(callback, filter, pager)
{
	if(!filter)
		filter = null;
	if(!pager)
		pager = null;

	kparams = new Object();
	if (filter != null)
		this.client.addParam(kparams, "filter", filter.toParams());
	if (pager != null)
		this.client.addParam(kparams, "pager", pager.toParams());
	this.client.queueServiceActionCall("widget", "list", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolXInternalService(client)
{
	this.init(client);
}

KontorolXInternalService.prototype = new KontorolServiceBase();

KontorolXInternalService.prototype.xAddBulkDownload = function(callback, entryIds, flavorParamsId)
{
	if(!flavorParamsId)
		flavorParamsId = "";

	kparams = new Object();
	this.client.addParam(kparams, "entryIds", entryIds);
	this.client.addParam(kparams, "flavorParamsId", flavorParamsId);
	this.client.queueServiceActionCall("xinternal", "xAddBulkDownload", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolSystemUserService(client)
{
	this.init(client);
}

KontorolSystemUserService.prototype = new KontorolServiceBase();

KontorolSystemUserService.prototype.verifyPassword = function(callback, email, password)
{

	kparams = new Object();
	this.client.addParam(kparams, "email", email);
	this.client.addParam(kparams, "password", password);
	this.client.queueServiceActionCall("systemuser", "verifyPassword", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolSystemUserService.prototype.generateNewPassword = function(callback)
{

	kparams = new Object();
	this.client.queueServiceActionCall("systemuser", "generateNewPassword", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolSystemUserService.prototype.setNewPassword = function(callback, userId, password)
{

	kparams = new Object();
	this.client.addParam(kparams, "userId", userId);
	this.client.addParam(kparams, "password", password);
	this.client.queueServiceActionCall("systemuser", "setNewPassword", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolSystemUserService.prototype.add = function(callback, systemUser)
{

	kparams = new Object();
	this.client.addParam(kparams, "systemUser", systemUser.toParams());
	this.client.queueServiceActionCall("systemuser", "add", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolSystemUserService.prototype.get = function(callback, userId)
{

	kparams = new Object();
	this.client.addParam(kparams, "userId", userId);
	this.client.queueServiceActionCall("systemuser", "get", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolSystemUserService.prototype.getByEmail = function(callback, email)
{

	kparams = new Object();
	this.client.addParam(kparams, "email", email);
	this.client.queueServiceActionCall("systemuser", "getByEmail", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolSystemUserService.prototype.update = function(callback, userId, systemUser)
{

	kparams = new Object();
	this.client.addParam(kparams, "userId", userId);
	this.client.addParam(kparams, "systemUser", systemUser.toParams());
	this.client.queueServiceActionCall("systemuser", "update", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolSystemUserService.prototype.delete = function(callback, userId)
{

	kparams = new Object();
	this.client.addParam(kparams, "userId", userId);
	this.client.queueServiceActionCall("systemuser", "delete", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

KontorolSystemUserService.prototype.listAction = function(callback, filter, pager)
{
	if(!filter)
		filter = null;
	if(!pager)
		pager = null;

	kparams = new Object();
	if (filter != null)
		this.client.addParam(kparams, "filter", filter.toParams());
	if (pager != null)
		this.client.addParam(kparams, "pager", pager.toParams());
	this.client.queueServiceActionCall("systemuser", "list", kparams);
	if (!this.client.isMultiRequest())
		this.client.doQueue(callback);
};

function KontorolClient(config)
{
	this.init(config);
}

KontorolClient.prototype = new KontorolClientBase()
/**
 * Add & Manage Access Controls
 *
 * @var KontorolAccessControlService
 */
KontorolClient.prototype.accessControl = null;

/**
 * admin console service lets you manage cross partner reports, activity, status and config. 
	 * 
 *
 * @var KontorolAdminconsoleService
 */
KontorolClient.prototype.adminconsole = null;

/**
 * Manage details for the administrative user
 *
 * @var KontorolAdminUserService
 */
KontorolClient.prototype.adminUser = null;

/**
 * Base Entry Service
 *
 * @var KontorolBaseEntryService
 */
KontorolClient.prototype.baseEntry = null;

/**
 * Bulk upload service is used to upload & manage bulk uploads using CSV files
 *
 * @var KontorolBulkUploadService
 */
KontorolClient.prototype.bulkUpload = null;

/**
 * Add & Manage Categories
 *
 * @var KontorolCategoryService
 */
KontorolClient.prototype.category = null;

/**
 * Add & Manage Conversion Profiles
 *
 * @var KontorolConversionProfileService
 */
KontorolClient.prototype.conversionProfile = null;

/**
 * Data service lets you manage data content (textual content)
 *
 * @var KontorolDataService
 */
KontorolClient.prototype.data = null;

/**
 * Retrieve information and invoke actions on Flavor Asset
 *
 * @var KontorolFlavorAssetService
 */
KontorolClient.prototype.flavorAsset = null;

/**
 * Add & Manage Flavor Params
 *
 * @var KontorolFlavorParamsService
 */
KontorolClient.prototype.flavorParams = null;

/**
 * batch service lets you handle different batch process from remote machines.
	 * As oppesed to other ojects in the system, locking mechanism is critical in this case.
	 * For this reason the GetExclusiveXX, UpdateExclusiveXX and FreeExclusiveXX actions are important for the system's intergity.
	 * In general - updating batch object should be done only using the UpdateExclusiveXX which in turn can be called only after 
	 * acuiring a batch objet properly (using  GetExclusiveXX).
	 * If an object was aquired and should be returned to the pool in it's initial state - use the FreeExclusiveXX action 
	 * 
 *
 * @var KontorolJobsService
 */
KontorolClient.prototype.jobs = null;

/**
 * Media service lets you upload and manage media files (images / videos & audio)
 *
 * @var KontorolMediaService
 */
KontorolClient.prototype.media = null;

/**
 * A Mix is an XML unique format invented by Kontorol, it allows the user to create a mix of videos and images, in and out points, transitions, text overlays, soundtrack, effects and much more...
	 * Mixing service lets you create a new mix, manage its metadata and make basic manipulations.   
 *
 * @var KontorolMixingService
 */
KontorolClient.prototype.mixing = null;

/**
 * Notification Service
 *
 * @var KontorolNotificationService
 */
KontorolClient.prototype.notification = null;

/**
 * partner service allows you to change/manage your partner personal details and settings as well
 *
 * @var KontorolPartnerService
 */
KontorolClient.prototype.partner = null;

/**
 * Playlist service lets you create,manage and play your playlists
	 * Playlists could be static (containing a fixed list of entries) or dynamic (baseed on a filter)
 *
 * @var KontorolPlaylistService
 */
KontorolClient.prototype.playlist = null;

/**
 * api for getting reports data by the report type and some inputFilter
 *
 * @var KontorolReportService
 */
KontorolClient.prototype.report = null;

/**
 * Search service allows you to search for media in various media providers
	 * This service is being used mostly by the CW component
 *
 * @var KontorolSearchService
 */
KontorolClient.prototype.search = null;

/**
 * Session service
 *
 * @var KontorolSessionService
 */
KontorolClient.prototype.session = null;

/**
 * Stats Service
 *
 * @var KontorolStatsService
 */
KontorolClient.prototype.stats = null;

/**
 * Add & Manage Syndication Feeds
 *
 * @var KontorolSyndicationFeedService
 */
KontorolClient.prototype.syndicationFeed = null;

/**
 * System service is used for internal system helpers & to retrieve system level information
 *
 * @var KontorolSystemService
 */
KontorolClient.prototype.system = null;

/**
 * UiConf service lets you create and manage your UIConfs for the various flash components
	 * This service is used by the KMC-ApplicationStudio
 *
 * @var KontorolUiConfService
 */
KontorolClient.prototype.uiConf = null;

/**
 * Upload service is used to upload files and get the token that can be later used as a reference to the uploaded file
	 * 
 *
 * @var KontorolUploadService
 */
KontorolClient.prototype.upload = null;

/**
 * Manage partner users on Kontorol's side
	 * The userId in kontorol is the unique Id in the partner's system, and the [partnerId,Id] couple are unique key in kontorol's DB
 *
 * @var KontorolUserService
 */
KontorolClient.prototype.user = null;

/**
 * widget service for full widget management
 *
 * @var KontorolWidgetService
 */
KontorolClient.prototype.widget = null;

/**
 * Internal Service is used for actions that are used internally in Kontorol applications and might be changed in the future without any notice.
 *
 * @var KontorolXInternalService
 */
KontorolClient.prototype.xInternal = null;

/**
 * System user service
 *
 * @var KontorolSystemUserService
 */
KontorolClient.prototype.systemUser = null;


KontorolClient.prototype.init = function(config)
{
	KontorolClientBase.prototype.init.apply(this, arguments);
	this.accessControl = new KontorolAccessControlService(this);
	this.adminconsole = new KontorolAdminconsoleService(this);
	this.adminUser = new KontorolAdminUserService(this);
	this.baseEntry = new KontorolBaseEntryService(this);
	this.bulkUpload = new KontorolBulkUploadService(this);
	this.category = new KontorolCategoryService(this);
	this.conversionProfile = new KontorolConversionProfileService(this);
	this.data = new KontorolDataService(this);
	this.flavorAsset = new KontorolFlavorAssetService(this);
	this.flavorParams = new KontorolFlavorParamsService(this);
	this.jobs = new KontorolJobsService(this);
	this.media = new KontorolMediaService(this);
	this.mixing = new KontorolMixingService(this);
	this.notification = new KontorolNotificationService(this);
	this.partner = new KontorolPartnerService(this);
	this.playlist = new KontorolPlaylistService(this);
	this.report = new KontorolReportService(this);
	this.search = new KontorolSearchService(this);
	this.session = new KontorolSessionService(this);
	this.stats = new KontorolStatsService(this);
	this.syndicationFeed = new KontorolSyndicationFeedService(this);
	this.system = new KontorolSystemService(this);
	this.uiConf = new KontorolUiConfService(this);
	this.upload = new KontorolUploadService(this);
	this.user = new KontorolUserService(this);
	this.widget = new KontorolWidgetService(this);
	this.xInternal = new KontorolXInternalService(this);
	this.systemUser = new KontorolSystemUserService(this);
}

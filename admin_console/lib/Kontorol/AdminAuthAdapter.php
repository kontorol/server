<?php
/**
 * @package Admin
 * @subpackage Authentication
 */
class Kontorol_AdminAuthAdapter extends Infra_AuthAdapter
{
	/* (non-PHPdoc)
	 * @see Infra_AuthAdapter::getUserIdentity()
	 */
	protected function getUserIdentity(Kontorol_Client_Type_User $user=null, $ks=null, $partnerId=null)
	{
		return new Kontorol_AdminUserIdentity($user, $ks, $this->timezoneOffset, $partnerId);
	}
}

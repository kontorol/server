<?php
/**
 * @package Admin
 * @subpackage views
 */
class Kontorol_View_Helper_JobTypeTranslate extends Zend_View_Helper_Abstract
{
	public function jobTypeTranslate($jobType, $jobSubType = null)
	{
		if($jobType == Kontorol_Client_Enum_BatchJobType::CONVERT && $jobSubType)
			return $this->view->enumTranslate('Kontorol_Client_Enum_ConversionEngineType', $jobSubType);
			
		return $this->view->enumTranslate('Kontorol_Client_Enum_BatchJobType', $jobType);
	}
}

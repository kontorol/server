# Kontorol Backend Core project
Kontorol is the world's first Open Source Online Video Platform, transforming the way people work,
learn, and entertain using online video.
The Kontorol platform empowers media applications with advanced video management, publishing,
and monetization tools that increase their reach and monetization and simplify their video operations.
Kontorol improves productivity and interaction among millions of employees by providing enterprises
powerful online video tools for boosting internal knowledge sharing, training, and collaboration,
and for more effective marketing. Kontorol offers next generation learning for millions of students and
teachers by providing educational institutions disruptive online video solutions for improved teaching,
learning, and increased engagement across campuses and beyond.
For more information visit: http://corp.kontorol.com and http://www.kontorol.org.

## Commercial Editions and Paid Support

The Open Source Kontorol Platform is provided under the [AGPLv3 license](http://www.gnu.org/licenses/agpl-3.0.html) and with no
commercial support or liability.  

[Kontorol Inc.](http://corp.kontorol.com) also provides commercial solutions and services including pro-active platform monitoring,
applications, SLA, 24/7 support and professional services. If you're looking for a commercially supported video platform  with
integrations to commercial encoders, streaming servers, eCDN, DRM and more - Start a [Free Trial of the Kontorol.com Hosted
Platform](http://corp.kontorol.com/free-trial) or learn more about [Kontorol' Commercial OnPrem
Edition™](http://corp.kontorol.com/Deployment-Options/Kontorol-On-Prem-Edition). For existing RPM based users, Kontorol offers
commercial upgrade options.

## How you can help
Thank you for running Kontorol and pushing the limits of online video! By joining the Kontorol community and contributing you will help grow the platform, and keeping it open, stable and accessible to all. 

You can contribute meaningfully to the project from day one:    

+ By contributing sanity tests that verify overall platform stability - [Join the Kontorol CI Project](https://github.com/kontorol/platform-continuous-integration).
+ By testing the platform components, including various deployment settings and environments, and network settings, [reporting issues and feature requests](https://github.com/kontorol/platform-install-packages/issues) and [pushing pull-requests](https://help.github.com/articles/creating-a-pull-request) that patch bugs.
+ By creating [documentation](https://github.com/kontorol/platform-install-packages/tree/master/doc), demos and examples.
+ By translating Kontorol and the documentation.

To make a contribution, follow the [See our CONTRIBUTERS doc](https://github.com/kontorol/platform-install-packages/blob/master/CONTRIBUTING.md)

Contact the authors or email community@kontorol.org if you have other ideas.

## Documentation and Guides

* [**Frequently Asked Questions**](https://github.com/kontorol/platform-install-packages/blob/master/doc/kontorol-packages-faq.md)
* [API Client libraries introduction](http://knowledge.kontorol.com/introduction-kontorol-client-libraries)
* [API Client libraries download](http://www.kontorol.com/api_v3/testme/client-libs.php)
* [Single-server All-In-One RedHat based Linux distros Installation (including FC, RHEL and CentOS)](https://github.com/kontorol/platform-install-packages/blob/master/doc/install-kontorol-redhat-based.md).
* [Single-server All-In-One deb based Linux distros Installation (Debian, Ubuntu)](https://github.com/kontorol/platform-install-packages/blob/master/doc/install-kontorol-deb-based.md).
* [Deploying Kontorol Clusters (RPM)](https://github.com/kontorol/platform-install-packages/blob/master/doc/rpm-cluster-deployment-instructions.md)
* [Deploying Kontorol Clusters (deb)](https://github.com/kontorol/platform-install-packages/blob/master/doc/deb-cluster-deployment-instructions.md)
* [Deploying Kontorol using Opscode Chef (RPM)](https://github.com/kontorol/platform-install-packages/blob/master/doc/rpm-chef-cluster-deployment.md)
* [Setting up Kontorol platform monitoring](https://github.com/kontorol/platform-install-packages/blob/master/doc/platform-monitors.md)
* [Required Open Ports to run Kontorol](https://github.com/kontorol/platform-install-packages/blob/master/doc/kontorol-required-ports.md)
* [Deploy Local Repository for Offline Install](https://github.com/kontorol/platform-install-packages/blob/master/doc/deploy-local-rpm-repo-offline-install.md)



## License and Copyright Information
All code in this project is released under the [AGPLv3 license](http://www.gnu.org/licenses/agpl-3.0.html) unless a different license for a particular library is specified in the applicable library path. 

Copyright © Kontorol Inc. All rights reserved.


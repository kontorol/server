<?php
/**
 * @package Scheduler
 */
class KontorolBatchException extends KontorolException
{
	public function __construct($message, $code, $arguments = null)
	{
		parent::__construct($message, $code, $arguments);
	}
}

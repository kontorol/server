<?php
/**
 * 
 * @package Scheduler
 */

chdir(__DIR__);
define('KONTOROL_ROOT_PATH', realpath(__DIR__ . '/../'));
require_once(KONTOROL_ROOT_PATH . '/alpha/config/kConf.php');

define("KONTOROL_BATCH_PATH", KONTOROL_ROOT_PATH . "/batch");

// Autoloader - override the autoloader defaults
require_once(KONTOROL_ROOT_PATH . "/infra/KAutoloader.php");
KAutoloader::setClassPath(array(
	KAutoloader::buildPath(KONTOROL_ROOT_PATH, "infra", "*"),
	KAutoloader::buildPath(KONTOROL_ROOT_PATH, "vendor", "*"),
	KAutoloader::buildPath(KONTOROL_ROOT_PATH, "plugins", "*"),
	KAutoloader::buildPath(KONTOROL_BATCH_PATH, "*"),
));

KAutoloader::addClassPath(KAutoloader::buildPath(KONTOROL_ROOT_PATH, "plugins", "*", "batch", "*"));

KAutoloader::setIncludePath(array(
	KAutoloader::buildPath(KONTOROL_ROOT_PATH, "vendor", "ZendFramework", "library"),
));
KAutoloader::setClassMapFilePath(kEnvironment::get("cache_root_path") . '/batch/classMap.cache');
KAutoloader::addExcludePath(KAutoloader::buildPath(KONTOROL_ROOT_PATH, "vendor", "aws", "*")); // Do not load AWS files
KAutoloader::addExcludePath(KAutoloader::buildPath(KONTOROL_ROOT_PATH, "vendor", "HTMLPurifier", "*")); // Do not load HTMLPurifier files
KAutoloader::register();

// Logger
$loggerConfigPath = KONTOROL_ROOT_PATH . "/configurations/logger.ini";

try // we don't want to fail when logger is not configured right
{
	$config = new Zend_Config_Ini($loggerConfigPath);
	KontorolLog::initLog($config->batch);
	KontorolLog::setContext("BATCH");
}
catch(Zend_Config_Exception $ex)
{
}

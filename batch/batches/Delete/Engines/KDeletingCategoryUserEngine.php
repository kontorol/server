<?php
/**
 * @package Scheduler
 * @subpackage Delete
 */
class KDeletingCategoryUserEngine extends KDeletingEngine
{
	/* (non-PHPdoc)
	 * @see KDeletingEngine::delete()
	 */
	protected function delete(KontorolFilter $filter)
	{
		return $this->deleteCategoryUsers($filter);
	}
	
	/**
	 * @param KontorolCategoryUserFilter $filter The filter should return the list of category users that need to be deleted
	 * @return int the number of deleted category users
	 */
	protected function deleteCategoryUsers(KontorolCategoryUserFilter $filter)
	{
		$filter->orderBy = KontorolCategoryUserOrderBy::CREATED_AT_ASC;
		
		$categoryUsersList = KBatchBase::$kClient->categoryUser->listAction($filter, $this->pager);
		if(!$categoryUsersList->objects || !count($categoryUsersList->objects))
			return 0;
			
		KBatchBase::$kClient->startMultiRequest();
		foreach($categoryUsersList->objects as $categoryUser)
		{
			/* @var $categoryUser KontorolCategoryUser */
			KBatchBase::$kClient->categoryUser->delete($categoryUser->categoryId, $categoryUser->userId);
		}
		$results = KBatchBase::$kClient->doMultiRequest();
		foreach($results as $index => $result)
			if(is_array($result) && isset($result['code']))
				unset($results[$index]);
				
		return count($results);
	}
}

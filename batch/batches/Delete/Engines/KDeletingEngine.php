<?php
/**
 * @package Scheduler
 * @subpackage Delete
 */
abstract class KDeletingEngine
{
	/**
	 * @var KontorolClient
	 */
	protected $client;
	
	/**
	 * @var KontorolFilterPager
	 */
	protected $pager;
	
	/**
	 * The partner that owns the objects
	 * @var int
	 */
	private $partnerId;
	
	/**
	 * The batch system partner id
	 * @var int
	 */
	private $batchPartnerId;
	
	/**
	 * @param int $objectType of enum KontorolDeleteObjectType
	 * @return KDeletingEngine
	 */
	public static function getInstance($objectType)
	{
		switch($objectType)
		{
			case KontorolDeleteObjectType::CATEGORY_ENTRY:
				return new KDeletingCategoryEntryEngine();
				
			case KontorolDeleteObjectType::CATEGORY_USER:
				return new KDeletingCategoryUserEngine();

			case KontorolDeleteObjectType::GROUP_USER:
				return new KDeletingGroupUserEngine();

			case KontorolDeleteObjectType::CATEGORY_ENTRY_AGGREGATION:
 				return new KDeletingAggregationChannelEngine();
				
			case KontorolDeleteObjectType::USER_ENTRY :
 				return new KDeletingUserEntryEngine();
			
			default:
				return KontorolPluginManager::loadObject('KDeletingEngine', $objectType);
		}
	}
	
	/**
	 * @param int $partnerId
	 * @param KontorolDeleteJobData $jobData
  	 * @param KontorolClient $client
  	 */
	public function configure($partnerId, $jobData)
	{
		$this->partnerId = $partnerId;
		$this->batchPartnerId = KBatchBase::$taskConfig->getPartnerId();

		$this->pager = new KontorolFilterPager();
		$this->pager->pageSize = 100;
		
		if(KBatchBase::$taskConfig->params && KBatchBase::$taskConfig->params->pageSize)
			$this->pager->pageSize = KBatchBase::$taskConfig->params->pageSize;
	}

	
	/**
	 * @param KontorolFilter $filter The filter should return the list of objects that need to be reindexed
	 * @param bool $shouldUpdate Indicates that the object columns and attributes values should be recalculated before reindexed
	 * @return int the number of indexed objects
	 */
	public function run(KontorolFilter $filter)
	{
		KBatchBase::impersonate($this->partnerId);
		$ret = $this->delete($filter);
		KBatchBase::unimpersonate();
		
		return $ret;
	}
	
	/**
	 * @param KontorolFilter $filter The filter should return the list of objects that need to be deleted
	 * @return int the number of deleted objects
	 */
	abstract protected function delete(KontorolFilter $filter);
}

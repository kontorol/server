<?php
/**
 * @package Scheduler
 * @subpackage Delete
 */
class KDeletingUserEntryEngine extends KDeletingEngine
{
	/* (non-PHPdoc)
	 * @see KDeletingEngine::delete()
	 */
	protected function delete(KontorolFilter $filter)
	{
		return $this->deleteUserEntries($filter);
	}
	
	/**
	 * @param KontorolUserEntryFilter $filter The filter should return the list of user entries that need to be deleted
	 * @return int the number of deleted category entries
	 */
	protected function deleteUserEntries(KontorolUserEntryFilter $filter)
	{
		$filter->orderBy = KontorolUserEntryOrderBy::CREATED_AT_ASC;
		
		$userEntryList = KBatchBase::$kClient->userEntry->listAction($filter, $this->pager);
		if(!$userEntryList->objects || !count($userEntryList->objects))
			return 0;
			
		KBatchBase::$kClient->startMultiRequest();
		foreach($userEntryList->objects as $userEntry)
		{
			/* @var $categoryEntry KontorolUserEntry */
			KBatchBase::$kClient->userEntry->delete($userEntry->id);
		}
		$results = KBatchBase::$kClient->doMultiRequest();
		foreach($results as $index => $result)
			if(is_array($result) && isset($result['code']))
				unset($results[$index]);

		return count($results);
	}
}

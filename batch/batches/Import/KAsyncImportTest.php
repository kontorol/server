<?php
/**
 * @package Scheduler
 * @subpackage Debug
 */
chdir(dirname( __FILE__ ) . "/../../");
require_once(__DIR__ . "/../../bootstrap.php");

/**
 * @package Scheduler
 * @subpackage Debug
 */
class KAsyncImportTest extends PHPUnit_Framework_TestCase
{
	const JOB_NAME = 'KAsyncImport';
	
	public function setUp() 
	{
		parent::setUp();
	}
	
	public function tearDown() 
	{
		parent::tearDown();
	}
	
	public function testGoodUrl()
	{
		$this->doTest('http://kondev.kontorol.com/content/zbale/9spkxiz8m4_100007.mp4', KontorolBatchJobStatus::FINISHED);
	}
	
//	public function testSpecialCharsUrl()
//	{
//		$this->doTest('http://kondev.kontorol.com/content/zbale/trailer_480 ()p.mov', KontorolBatchJobStatus::FINISHED);
//	}
//	
//	public function testSpacedUrl()
//	{
//		$this->doTest(' http://kondev.kontorol.com/content/zbale/9spkxiz8m4_100007.mp4', KontorolBatchJobStatus::FINISHED);
//	}
//	
//	public function testMissingFileUrl()
//	{
//		$this->doTest('http://localhost/api_v3/sample/xxx.avi', KontorolBatchJobStatus::FAILED);
//	}
//	
//	public function testInvalidServerUrl()
//	{
//		$this->doTest('http://xxx', KontorolBatchJobStatus::FAILED);
//	}
//	
//	public function testInvalidUrl()
//	{
//		$this->doTest('xxx', KontorolBatchJobStatus::FAILED);
//	}
//	
//	public function testEmptyUrl()
//	{
//		$this->doTest('', KontorolBatchJobStatus::FAILED);
//	}
	
	public function doTest($value, $expectedStatus)
	{
		$iniFile = "batch_config.ini";
		$schedulerConfig = new KSchedulerConfig($iniFile);
	
		$taskConfigs = $schedulerConfig->getTaskConfigList();
		$config = null;
		foreach($taskConfigs as $taskConfig)
		{
			if($taskConfig->name == self::JOB_NAME)
				$config = $taskConfig;
		}
		$this->assertNotNull($config);
		
		$jobs = $this->prepareJobs($value);
		
		$config->setTaskIndex(1);
		$instance = new $config->type($config);
		$instance->setUnitTest(true);
		$jobs = $instance->run($jobs); 
		$instance->done();
		
		foreach($jobs as $job)
			$this->assertEquals($expectedStatus, $job->status);
	}
	
	private function prepareJobs($value)
	{
		$data = new KontorolImportJobData();
		$data->srcFileUrl = $value;
		
		$job = new KontorolBatchJob();
		$job->id = 1;
		$job->status = KontorolBatchJobStatus::PENDING;
		$job->data = $data;
		
		return array($job);
	}
}

?>

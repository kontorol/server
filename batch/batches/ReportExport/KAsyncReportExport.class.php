<?php
/**
 * @package Scheduler
 * @subpackage ReportExport
 */
class KAsyncReportExport extends KJobHandlerWorker
{

	public static function getType()
	{
		return KontorolBatchJobType::REPORT_EXPORT;
	}

	/**
	 * @param KontorolBatchJob $job
	 * @return KontorolBatchJob
	 */
	protected function exec(KontorolBatchJob $job)
	{
		$this->updateJob($job, 'Creating CSV Export', KontorolBatchJobStatus::PROCESSING);
		$job = $this->createCsv($job, $job->data);
		return $job;
	}

	protected function createCsv(KontorolBatchJob $job, KontorolReportExportJobData $data)
	{
		$partnerId = $job->partnerId;

		$outputDir = self::$taskConfig->params->localTempPath . DIRECTORY_SEPARATOR . $partnerId;
		KBatchBase::createDir($outputDir);

		$reportFiles = array();

		$reportItems = $data->reportItems;
		foreach ($reportItems as $reportItem)
		{
			$engine = ReportExportFactory::getEngine($reportItem, $outputDir);
			if (!$engine)
			{
				return $this->closeJob($job, null, null, 'Report export engine not found', KontorolBatchJobStatus::FAILED, $data);
			}

			try
			{
				KBatchBase::impersonate($job->partnerId);
				$reportFile = $engine->createReport();
				$reportEmailName = $engine->getEmailFileName();
				KBatchBase::unimpersonate();
				$exportFile = new KontorolReportExportFile();
				$exportFile->fileId = $reportFile;
				$exportFile->fileName = $reportEmailName;
				$reportFiles[] = $exportFile;
				$this->setFilePermissions($reportFile);
			}
			catch (Exception $e)
			{
				KBatchBase::unimpersonate();
				return $this->closeJob($job, null, null, 'Cannot create report', KontorolBatchJobStatus::RETRY, $data);
			}
		}

		$this->moveFiles($reportFiles, $job, $data, $partnerId);
		return $job;
	}

	protected function moveFiles($tmpFiles, KontorolBatchJob $job, KontorolReportExportJobData $data, $partnerId)
	{
		KBatchBase::createDir(self::$taskConfig->params->sharedTempPath. DIRECTORY_SEPARATOR . $partnerId);
		$outFiles = array();
		foreach ($tmpFiles as $tmpFile)
		{
			$res = $this->moveFile($tmpFile->fileId, $partnerId);
			if (!$res)
			{
				return $this->closeJob($job, KontorolBatchJobErrorTypes::APP, KontorolBatchJobAppErrors::NFS_FILE_DOESNT_EXIST, 'Failed to move report file', KontorolBatchJobStatus::RETRY);
			}
			$exportFile = new KontorolReportExportFile();
			$exportFile->fileId = $res;
			$exportFile->fileName = $tmpFile->fileName;

			$outFiles[] = $exportFile;
		}

		$data->files = $outFiles;
		return $this->closeJob($job, null, null, 'CSV files created successfully', KontorolBatchJobStatus::FINISHED, $data);
	}

	protected function moveFile($filePath, $partnerId)
	{
		$fileName =  basename($filePath);
		$sharedLocation = self::$taskConfig->params->sharedTempPath . DIRECTORY_SEPARATOR . $partnerId . DIRECTORY_SEPARATOR . $partnerId . "_" . $fileName;

		$fileSize = kFile::fileSize($filePath);
		rename($filePath, $sharedLocation);

		$this->setFilePermissions($sharedLocation);
		if (!$this->checkFileExists($sharedLocation, $fileSize))
		{
			return false;
		}
		return $sharedLocation;
	}

}

<?php
/**
 * @package Scheduler
 * @subpackage ReportExport
 */
class ReportExportFactory
{
	public static function getEngine($reportItem, $outputPath)
	{
		switch ($reportItem->action)
		{
			case KontorolReportExportItemType::TABLE:
				return new kReportExportTableEngine($reportItem, $outputPath);
			case KontorolReportExportItemType::GRAPH:
				return new kReportExportGraphEngine($reportItem, $outputPath);
			case KontorolReportExportItemType::TOTAL:
				return new kReportExportTotalEngine($reportItem, $outputPath);
			default:
				return null;
		}
	}

}

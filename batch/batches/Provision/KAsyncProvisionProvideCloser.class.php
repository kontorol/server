<?php
/**
 * Closes the process of provisioning a new stream.
 *
 * 
 * @package Scheduler
 * @subpackage Provision
 */
class KAsyncProvisionProvideCloser extends KJobCloserWorker
{
	/* (non-PHPdoc)
	 * @see KJobHandlerWorker::exec()
	 */
	protected function exec(KontorolBatchJob $job) {
		$this->closeProvisionProvide($job);
		
	}

	public static function getType()
	{
		return KontorolBatchJobType::PROVISION_PROVIDE;
	}

	protected function closeProvisionProvide (KontorolBatchJob $job)
	{
		if(($job->queueTime + self::$taskConfig->params->maxTimeBeforeFail) < time())
			return new KProvisionEngineResult(KontorolBatchJobStatus::CLOSER_TIMEOUT, "Timed out");
			
		$engine = KProvisionEngine::getInstance( $job->jobSubType, $job->data);
		if ( $engine == null )
		{
			$err = "Cannot find provision engine [{$job->jobSubType}] for job id [{$job->id}]";
			return $this->closeJob($job, KontorolBatchJobErrorTypes::APP, KontorolBatchJobAppErrors::ENGINE_NOT_FOUND, $err, KontorolBatchJobStatus::FAILED);
		}
		
		KontorolLog::info( "Using engine: " . $engine->getName() );
	
		$results = $engine->checkProvisionedStream($job, $job->data);

		if($results->status == KontorolBatchJobStatus::FINISHED)
			return $this->closeJob($job, null, null, null, KontorolBatchJobStatus::FINISHED, $results->data);
		
		return $this->closeJob($job, null, null, $results->errMessage, KontorolBatchJobStatus::ALMOST_DONE, $results->data);
		
	}
	
}

<?php
/**
 * Will provision new live stream.
 *
 * 
 * @package Scheduler
 * @subpackage Provision
 */
class KAsyncProvisionProvide extends KJobHandlerWorker
{
	/* (non-PHPdoc)
	 * @see KBatchBase::getType()
	 */
	public static function getType()
	{
		return KontorolBatchJobType::PROVISION_PROVIDE;
	}
	
	/* (non-PHPdoc)
	 * @see KJobHandlerWorker::exec()
	 */
	protected function exec(KontorolBatchJob $job)
	{
		return $this->provision($job, $job->data);
	}
	
	/* (non-PHPdoc)
	 * @see KJobHandlerWorker::getMaxJobsEachRun()
	 */
	protected function getMaxJobsEachRun()
	{
		return 1;
	}
	
	protected function provision(KontorolBatchJob $job, KontorolProvisionJobData $data)
	{
		$job = $this->updateJob($job, null, KontorolBatchJobStatus::QUEUED);
		
		$engine = KProvisionEngine::getInstance( $job->jobSubType , $data);
		
		if ( $engine == null )
		{
			$err = "Cannot find provision engine [{$job->jobSubType}] for job id [{$job->id}]";
			return $this->closeJob($job, KontorolBatchJobErrorTypes::APP, KontorolBatchJobAppErrors::ENGINE_NOT_FOUND, $err, KontorolBatchJobStatus::FAILED);
		}
		
		KontorolLog::info( "Using engine: " . $engine->getName() );
	
		$results = $engine->provide($job, $data);

		if($results->status == KontorolBatchJobStatus::FINISHED)
			return $this->closeJob($job, null, null, null, KontorolBatchJobStatus::ALMOST_DONE, $results->data);
			
		return $this->closeJob($job, KontorolBatchJobErrorTypes::APP, null, $results->errMessage, $results->status, $results->data);
	}
}

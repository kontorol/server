<?php
/**
 * base class for the real ProvisionEngine in the system - currently only akamai 
 * 
 * @package Scheduler
 * @subpackage Provision
 * @abstract
 */
abstract class KProvisionEngine
{
	
	/**
	 * Will return the proper engine depending on the type (KontorolSourceType)
	 *
	 * @param int $provider
	 * @param KontorolProvisionJobData $data
	 * @return KProvisionEngine
	 */
	public static function getInstance ( $provider , KontorolProvisionJobData $data = null)
	{
		$engine =  null;
		
		switch ($provider )
		{
			case KontorolSourceType::AKAMAI_LIVE:
				$engine = new KProvisionEngineAkamai($data);
				break;
			case KontorolSourceType::AKAMAI_UNIVERSAL_LIVE:
				$engine = new KProvisionEngineUniversalAkamai($data);
				break;
			default:
				$engine = KontorolPluginManager::loadObject('KProvisionEngine', $provider);
		}
		
		return $engine;
	}

	
	/**
	 * @return string
	 */
	abstract public function getName();
	
	/**
	 * @param KontorolBatchJob $job
	 * @param KontorolProvisionJobData $data
	 * @return KProvisionEngineResult
	 */
	abstract public function provide( KontorolBatchJob $job, KontorolProvisionJobData $data );
	
	/**
	 * @param KontorolBatchJob $job
	 * @param KontorolProvisionJobData $data
	 * @return KProvisionEngineResult
	 */
	abstract public function delete( KontorolBatchJob $job, KontorolProvisionJobData $data );
	
	/**
	 * @param KontorolBatchJob $job
	 * @param KontorolProvisionJobData $data
	 * @return KProvisionEngineResult
	 */
	abstract public function checkProvisionedStream ( KontorolBatchJob $job, KontorolProvisionJobData $data ) ;
}


/**
 * @package Scheduler
 * @subpackage Conversion
 *
 */
class KProvisionEngineResult
{
	/**
	 * @var int
	 */
	public $status;
	
	/**
	 * @var string
	 */
	public $errMessage;
	
	/**
	 * @var KontorolProvisionJobData
	 */
	public $data;
	
	/**
	 * @param int $status
	 * @param string $errMessage
	 * @param KontorolProvisionJobData $data
	 */
	public function __construct( $status , $errMessage, KontorolProvisionJobData $data = null )
	{
		$this->status = $status;
		$this->errMessage = $errMessage;
		$this->data = $data;
	}
}


<?php
/**
 * Will provision new live stram.
 *
 * 
 * @package Scheduler
 * @subpackage Provision
 */
class KAsyncProvisionDelete extends KJobHandlerWorker
{
	/* (non-PHPdoc)
	 * @see KBatchBase::getType()
	 */
	public static function getType()
	{
		return KontorolBatchJobType::PROVISION_DELETE;
	}
	
	/* (non-PHPdoc)
	 * @see KJobHandlerWorker::exec()
	 */
	protected function exec(KontorolBatchJob $job)
	{
		return $this->provision($job, $job->data);
	}
	
	/* (non-PHPdoc)
	 * @see KJobHandlerWorker::getMaxJobsEachRun()
	 */
	protected function getMaxJobsEachRun()
	{
		return 1;
	}
	
	protected function provision(KontorolBatchJob $job, KontorolProvisionJobData $data)
	{
		$job = $this->updateJob($job, null, KontorolBatchJobStatus::QUEUED);
	
		$engine = KProvisionEngine::getInstance( $job->jobSubType , $data);
		
		if ( $engine == null )
		{
			$err = "Cannot find provision engine [{$job->jobSubType}] for job id [{$job->id}]";
			return $this->closeJob($job, KontorolBatchJobErrorTypes::APP, KontorolBatchJobAppErrors::ENGINE_NOT_FOUND, $err, KontorolBatchJobStatus::FAILED);
		}
		
		KontorolLog::info( "Using engine: " . $engine->getName() );
	
		$results = $engine->delete($job, $data);

		if($results->status == KontorolBatchJobStatus::FINISHED)
			return $this->closeJob($job, null, null, null, KontorolBatchJobStatus::FINISHED, $results->data);
			
		return $this->closeJob($job, KontorolBatchJobErrorTypes::APP, null, $results->errMessage, $results->status, $results->data);
	}
}

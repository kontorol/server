<?php
/**
 * This worker deletes physical files from disk
 *
 * @package Scheduler
 * @subpackage Delete
 */
class KAsyncDeleteFile extends KJobHandlerWorker
{
	public static function getType()
	{
		return KontorolBatchJobType::DELETE_FILE;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see KJobHandlerWorker::exec()
	 */
	protected function exec(KontorolBatchJob $job)
	{
		$this->updateJob($job, "File deletion started", KontorolBatchJobStatus::PROCESSING);
		$jobData = $job->data;
		
		/* @var $jobData KontorolDeleteFileJobData */
		$result = unlink($jobData->localFileSyncPath);
		
		if (!$result)
			return $this->closeJob($job, KontorolBatchJobErrorTypes::RUNTIME, null, "Failed to delete file from disk", KontorolBatchJobStatus::FAILED);
		
		return $this->closeJob($job, null, null, 'File deleted successfully', KontorolBatchJobStatus::FINISHED);
		
	}


}

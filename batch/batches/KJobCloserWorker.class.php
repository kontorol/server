<?php
/**
 * Base class for all job closer workers.
 * 
 * @package Scheduler
 */
abstract class KJobCloserWorker extends KJobHandlerWorker
{
	public function run($jobs = null)
	{
		if(KBatchBase::$taskConfig->isInitOnly())
			return $this->init();
		
		if(is_null($jobs))
			$jobs = KBatchBase::$kClient->batch->getExclusiveAlmostDone($this->getExclusiveLockKey(), KBatchBase::$taskConfig->maximumExecutionTime, $this->getMaxJobsEachRun(), $this->getFilter(), static::getType());
		
		KontorolLog::info(count($jobs) . " jobs to close");
		
		if(! count($jobs) > 0)
		{
			KontorolLog::info("Queue size: 0 sent to scheduler");
			$this->saveSchedulerQueue(static::getType(), 0);
			return null;
		}
		
		foreach($jobs as &$job)
		{
			try
			{
				self::setCurrentJob($job);
				$job = $this->exec($job);
			}
			catch(KontorolException $kex)
			{
				KBatchBase::unimpersonate();
				$job = $this->closeJob($job, KontorolBatchJobErrorTypes::KONTOROL_API, $kex->getCode(), "Error: " . $kex->getMessage(), KontorolBatchJobStatus::FAILED);
			}
			catch(KontorolClientException $kcex)
			{
				KBatchBase::unimpersonate();
				$job = $this->closeJob($job, KontorolBatchJobErrorTypes::KONTOROL_CLIENT, $kcex->getCode(), "Error: " . $kcex->getMessage(), KontorolBatchJobStatus::RETRY);
			}
			catch(Exception $ex)
			{
				KBatchBase::unimpersonate();
				$job = $this->closeJob($job, KontorolBatchJobErrorTypes::RUNTIME, $ex->getCode(), "Error: " . $ex->getMessage(), KontorolBatchJobStatus::FAILED);
			}
			self::unsetCurrentJob();
		}
			
		return $jobs;
	}
	
	/**
	* @param string $jobType
	* @return KontorolWorkerQueueFilter
	*/
	protected function getQueueFilter($jobType)
	{
		$workerQueueFilter = $this->getBaseQueueFilter($jobType);
		$workerQueueFilter->filter->statusEqual = KontorolBatchJobStatus::ALMOST_DONE;
		
		return $workerQueueFilter;
	}
}

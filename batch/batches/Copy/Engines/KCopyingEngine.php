<?php
/**
 * @package Scheduler
 * @subpackage Copy
 */
abstract class KCopyingEngine
{
	/**
	 * @var KontorolClient
	 */
	protected $client;
	
	/**
	 * @var KontorolFilterPager
	 */
	protected $pager;
	
	/**
	 * @var int
	 */
	private $lastCopyId;
	
	/**
 	 * @var int
 	 */
 	private $lastCreatedAt;
	
	/**
	 * The partner that owns the objects
	 * @var int
	 */
	private $partnerId;
	
	/**
	 * The batch system partner id
	 * @var int
	 */
	private $batchPartnerId;
	
	/**
	 * @param int $objectType of enum KontorolCopyObjectType
	 * @return KCopyingEngine
	 */
	public static function getInstance($objectType)
	{
		switch($objectType)
		{
			case KontorolCopyObjectType::CATEGORY_USER:
				return new KCopyingCategoryUserEngine();
				
			case KontorolCopyObjectType::CATEGORY_ENTRY:
 				return new KCopyingCategoryEntryEngine();
				
			default:
				return KontorolPluginManager::loadObject('KCopyingEngine', $objectType);
		}
	}
	
	/**
	 * @param int $partnerId
	 * @param KontorolClient $client
	 * @param KSchedularTaskConfig $taskConfig
	 */
	public function configure($partnerId)
	{
		$this->partnerId = $partnerId;
		$this->batchPartnerId = KBatchBase::$taskConfig->getPartnerId();

		$this->pager = new KontorolFilterPager();
		$this->pager->pageSize = 100;
		
		if(KBatchBase::$taskConfig->params->pageSize)
			$this->pager->pageSize = KBatchBase::$taskConfig->params->pageSize;
	}
	
	
	/**
	 * @param KontorolFilter $filter The filter should return the list of objects that need to be copied
	 * @param KontorolObjectBase $templateObject Template object to overwrite attributes on the copied object
	 * @return int the number of copied objects
	 */
	public function run(KontorolFilter $filter, KontorolObjectBase $templateObject)
	{
		KBatchBase::impersonate($this->partnerId);
		$ret = $this->copy($filter, $templateObject);
		KBatchBase::unimpersonate();
		
		return $ret;
	}
	
	/**
	 * @param KontorolFilter $filter The filter should return the list of objects that need to be copied
	 * @param KontorolObjectBase $templateObject Template object to overwrite attributes on the copied object
	 * @return int the number of copied objects
	 */
	abstract protected function copy(KontorolFilter $filter, KontorolObjectBase $templateObject);
	
	/**
	 * Creates a new object instance, based on source object and copied attribute from the template object
	 * @param KontorolObjectBase $sourceObject
	 * @param KontorolObjectBase $templateObject
	 * @return KontorolObjectBase
	 */
	abstract protected function getNewObject(KontorolObjectBase $sourceObject, KontorolObjectBase $templateObject);
	
	/**
	 * @return int $lastCopyId
	 */
	public function getLastCopyId()
	{
		return $this->lastCopyId;
	}

	/**
	 * @param int $lastCopyId
	 */
	protected function setLastCopyId($lastCopyId)
	{
		$this->lastCopyId = $lastCopyId;
	}
}

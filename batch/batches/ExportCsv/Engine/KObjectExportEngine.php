<?php

/**
 * @package Scheduler
 * @subpackage ExportCsv
 */
abstract class KObjectExportEngine
{
	/**
	 * @param int $objectType of enum KontorolExportObjectType
	 * @return KObjectExportEngine
	 */
	public static function getInstance($objectType)
	{
		switch($objectType)
		{
			case KontorolExportObjectType::USER:
				return new KUserExportEngine();
			
			
			default:
				return KontorolPluginManager::loadObject('KObjectExportEngine', $objectType);
		}
	}
	
	abstract public function fillCsv (&$csvFile, &$data);
	
	/**
	 * Generate the first csv row containing the fields
	 */
	abstract protected function addHeaderRowToCsv($csvFile, $additionalFields);
}

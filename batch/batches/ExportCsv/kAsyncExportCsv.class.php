<?php
/**
 * @package Scheduler
 * @subpackage Copy
 */

/**
 * Will create csv of objects and mail it
 *
 * @package Scheduler
 * @subpackage Export-Csv
 */
class KAsyncExportCsv extends KJobHandlerWorker
{

	private $apiError = null;

	/* (non-PHPdoc)
	 * @see KBatchBase::getType()
	 */
	public static function getType()
	{
		return KontorolBatchJobType::EXPORT_CSV;
	}
	/**
	 * (non-PHPdoc)
	 * @see KBatchBase::getJobType()
	 */
	protected function getJobType()
	{
		return KontorolBatchJobType::EXPORT_CSV;
	}

	/* (non-PHPdoc)
	 * @see KJobHandlerWorker::exec()
	 */
	protected function exec(KontorolBatchJob $job)
	{
		return $this->generateCsvForExport($job, $job->data);
	}

	/**
	 * Generate csv contains users info which will be later sent by mail
	 */
	private function generateCsvForExport(KontorolBatchJob $job, KontorolExportCsvJobData $data)
	{
		$this->updateJob($job, "Start generating csv for export", KontorolBatchJobStatus::PROCESSING);
		self::impersonate($job->partnerId);

		// Create local path for csv generation
		$directory = self::$taskConfig->params->localTempPath . DIRECTORY_SEPARATOR . $job->partnerId;
		KBatchBase::createDir($directory);
		$filePath = $directory . DIRECTORY_SEPARATOR . 'export_' .$job->partnerId.'_'.$job->id . '.csv';
		$data->outputPath = $filePath;
		KontorolLog::info("Temp file path: [$filePath]");

		//fill the csv with users data
		$csvFile = fopen($filePath,"w");
		
		$engine = KObjectExportEngine::getInstance($job->jobSubType);
		$engine->fillCsv($csvFile, $data);
		
		fclose($csvFile);
		$this->setFilePermissions($filePath);
		self::unimpersonate();

		if($this->apiError)
		{
			$e = $this->apiError;
			return $this->closeJob($job, KontorolBatchJobErrorTypes::KONTOROL_API, $e->getCode(), $e->getMessage(), KontorolBatchJobStatus::RETRY);
		}

		// Copy the report to shared location.
		$this->moveFile($job, $data, $job->partnerId);
		return $job;
	}


	/**
	 * the function move the file to the shared location
	 */
	protected function moveFile(KontorolBatchJob $job, KontorolExportCsvJobData $data, $partnerId)
	{
		$directory = isset($data->sharedOutputPath) ? $data->sharedOutputPath : null;
		if(!$directory)
		{
			$directory = self::$taskConfig->params->sharedTempPath . DIRECTORY_SEPARATOR . $partnerId . DIRECTORY_SEPARATOR;
			KBatchBase::createDir($directory);
		}
		$fileName = basename($data->outputPath);
		$sharedLocation = $directory . $fileName;

		$fileSize = kFile::fileSize($data->outputPath);
		kFile::moveFile($data->outputPath, $sharedLocation);
		$data->outputPath = $sharedLocation;

		$this->setFilePermissions($sharedLocation);
		if(!$this->checkFileExists($sharedLocation, $fileSize))
		{
			return $this->closeJob($job, KontorolBatchJobErrorTypes::APP, KontorolBatchJobAppErrors::OUTPUT_FILE_DOESNT_EXIST, 'Failed to move csv file', KontorolBatchJobStatus::RETRY);
		}
		return $this->closeJob($job, null, null, 'CSV created successfully', KontorolBatchJobStatus::FINISHED, $data);
	}

}


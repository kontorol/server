<?php

class LiveReportFactory {
	
	public function getExporter($type, KontorolLiveReportExportJobData $jobData) {
		
		$exporter = null;
		switch ($type) {
			case KontorolLiveReportExportType::PARTNER_TOTAL_ALL :
				$exporter = new PartnerTotalAllExporter($jobData);
				break;
			case KontorolLiveReportExportType::PARTNER_TOTAL_LIVE :
				$exporter = new PartnerTotalLiveExporter($jobData);
				break;
			case KontorolLiveReportExportType::ENTRY_TIME_LINE_ALL :
				$exporter = new EntryTimeLineAllExporter($jobData);
				break;
			case KontorolLiveReportExportType::ENTRY_TIME_LINE_LIVE :
				$exporter = new EntryTimeLineLiveExporter ($jobData);
				break;
			case KontorolLiveReportExportType::LOCATION_ALL :
				$exporter = new LocationAllExporter($jobData);
				break;
			case KontorolLiveReportExportType::LOCATION_LIVE :
				$exporter = new LocationLiveExporter($jobData);
				break;
			case KontorolLiveReportExportType::SYNDICATION_ALL :
				$exporter = new SyndicationAllExporter($jobData);
				break;
			case KontorolLiveReportExportType::SYNDICATION_LIVE :
				$exporter = new SyndicationLiveExporter($jobData);
				break;
			default:
				throw new KOperationEngineException("Unknown Exporter type : " . $type);
		}
		
		$exporter->init($jobData);
		
		return $exporter;
	}
}

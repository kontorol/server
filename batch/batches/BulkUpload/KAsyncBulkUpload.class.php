<?php
/**
 * @package Scheduler
 * @subpackage Bulk-Upload
 */

setlocale ( LC_ALL, 'en_US.UTF-8' );

/**
 * Will initiate a single bulk upload.
 * The state machine of the job is as follows:
 * get the csv, parse it and validate it
 * creates the entries
 *
 * @package Scheduler
 * @subpackage Bulk-Upload
 */
class KAsyncBulkUpload extends KJobHandlerWorker 
{
	/* (non-PHPdoc)
	 * @see KBatchBase::getType()
	 */
	public static function getType()
	{
		return KontorolBatchJobType::BULKUPLOAD;
	}

	/* (non-PHPdoc)
	 * @see KJobHandlerWorker::exec()
	 */
	protected function exec(KontorolBatchJob $job)
	{
		ini_set('auto_detect_line_endings', true);
		try
		{
			$job = $this->startBulkUpload($job);
		}
		catch (KontorolBulkUploadAbortedException $abortedException)
		{
			self::unimpersonate();
			$job = $this->closeJob($job, null, null, null, KontorolBatchJobStatus::ABORTED);
		}
		catch(KontorolBatchException $kbex)
		{
			self::unimpersonate();
			$job = $this->closeJob($job, KontorolBatchJobErrorTypes::APP, $kbex->getCode(), "Error: " . $kbex->getMessage(), KontorolBatchJobStatus::FAILED);
		}
		catch(KontorolException $kex)
		{
			self::unimpersonate();
			$job = $this->closeJob($job, KontorolBatchJobErrorTypes::KONTOROL_API, $kex->getCode(), "Error: " . $kex->getMessage(), KontorolBatchJobStatus::FAILED);
		}
		catch(KontorolClientException $kcex)
		{
			self::unimpersonate();
			$job = $this->closeJob($job, KontorolBatchJobErrorTypes::KONTOROL_CLIENT, $kcex->getCode(), "Error: " . $kcex->getMessage(), KontorolBatchJobStatus::RETRY);
		}
		catch(Exception $ex)
		{
			self::unimpersonate();
			$job = $this->closeJob($job, KontorolBatchJobErrorTypes::RUNTIME, $ex->getCode(), "Error: " . $ex->getMessage(), KontorolBatchJobStatus::FAILED);
		}
		ini_set('auto_detect_line_endings', false);

		return $job;
	}


	/* (non-PHPdoc)
	 * @see KJobHandlerWorker::getMaxJobsEachRun()
	 */
	protected function getMaxJobsEachRun()
	{
		return 1;
	}

	/**
	 * Starts the bulk upload
	 * @param KontorolBatchJob $job
	 * @return KontorolBatchJob
	 * @throws KontorolBatchException
	 * @throws KontorolException
	 */
	private function startBulkUpload(KontorolBatchJob $job)
	{
		KontorolLog::info( "Start bulk upload ($job->id)" );
		
		//Gets the right Engine instance 
		$engine = KBulkUploadEngine::getEngine($job->jobSubType, $job);
		if (is_null ( $engine )) {
			throw new KontorolException ( "Unable to find bulk upload engine", KontorolBatchJobAppErrors::ENGINE_NOT_FOUND );
		}
		$job = $this->updateJob($job, 'Parsing file [' . $engine->getName() . ']', KontorolBatchJobStatus::QUEUED, $engine->getData());
		$this->appendPrivilegesToKs($job->data->privileges);
		$engine->setJob($job);
		$engine->setData($job->data);
		$engine->handleBulkUpload();
		
		$job = $engine->getJob();
		$data = $engine->getData();

		$countObjects = $this->countCreatedObjects($job->id, $job->data->bulkUploadObjectType);
		$countHandledObjects = $countObjects[0];
		$countErrorObjects = $countObjects[1];

		if(!$countHandledObjects && !$engine->shouldRetry() && $countErrorObjects)
			throw new KontorolBatchException("None of the uploaded items were processed succsessfuly", KontorolBatchJobAppErrors::BULK_NO_ENTRIES_HANDLED, $engine->getData());
		
		if($engine->shouldRetry())
		{
			self::$kClient->batch->resetJobExecutionAttempts($job->id, $this->getExclusiveLockKey(), $job->jobType);
			return $this->closeJob($job, null, null, "Retrying: ".$countHandledObjects." ".$engine->getObjectTypeTitle()." objects were handled until now", KontorolBatchJobStatus::RETRY);
		}

		//check if all items were done already
		if(!self::$kClient->batch->updateBulkUploadResults($job->id) && !$countErrorObjects)
		{
			return $this->closeJob($job, null, null, 'Finished successfully', KontorolBatchJobStatus::FINISHED);
		}
			
		return $this->closeJob($job, null, null, 'Waiting for objects closure', KontorolBatchJobStatus::ALMOST_DONE, $data);
	}
	
	/**
	 * Return the count of created entries
	 * @param int $jobId
	 * @return int
	 */
	protected function countCreatedObjects($jobId, $bulkuploadObjectType) 
	{
		$createdCount = 0;
		$errorCount = 0;
		
		$counters = self::$kClient->batch->countBulkUploadEntries($jobId, $bulkuploadObjectType);
		foreach($counters as $counter)
		{
			/** @var KontorolKeyValue $counter */
			if ($counter->key == 'created')
				$createdCount = $counter->value;
			if ($counter->key == 'error')
				$errorCount = $counter->value;
		}
		
		return array($createdCount, $errorCount);
	}
	
}

<?php
/**
 * @package Scheduler
 * @subpackage Bulk-Download
 */

/**
 * Will close almost done bulk downloads.
 * The state machine of the job is as follows:
 * 	 	get almost done bulk downloads 
 * 		check converts statuses
 * 		update the bulk status
 *
 * @package Scheduler
 * @subpackage Bulk-Download
 */
class KAsyncBulkDownloadCloser extends KJobCloserWorker
{
	/* (non-PHPdoc)
	 * @see KBatchBase::getType()
	 */
	public static function getType()
	{
		return KontorolBatchJobType::BULKDOWNLOAD;
	}
	
	/* (non-PHPdoc)
	 * @see KJobHandlerWorker::exec()
	 */
	protected function exec(KontorolBatchJob $job)
	{
		return $this->fetchStatus($job);
	}

	private function fetchStatus(KontorolBatchJob $job)
	{
		if(($job->queueTime + KBatchBase::$taskConfig->params->maxTimeBeforeFail) < time())
			return $this->closeJob($job, KontorolBatchJobErrorTypes::APP, KontorolBatchJobAppErrors::CLOSER_TIMEOUT, 'Timed out', KontorolBatchJobStatus::FAILED);
		
		return $this->closeJob($job, null, null, null, KontorolBatchJobStatus::ALMOST_DONE);
	}
}

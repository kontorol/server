<?php
/**
 * @package Scheduler
 * @subpackage Conversion
 */

/**
 * Will close a clip concat jobs that wasn't finished in the configured max time
 * @package Scheduler
 * @subpackage ClipConcat
 */
class KClipConcatCloser extends KJobCloserWorker
{
	/* (non-PHPdoc)
	 * @see KBatchBase::getType()
	 */
	public static function getType()
	{
		return KontorolBatchJobType::CLIP_CONCAT;
	}

	/* (non-PHPdoc)
	 * @see KJobHandlerWorker::exec()
	 */
	protected function exec(KontorolBatchJob $job)
	{
		return $this->checkTimeout($job);
	}

	private function checkTimeout(KontorolBatchJob $job)
	{
		if($job->queueTime && ($job->queueTime + self::$taskConfig->params->maxTimeBeforeFail) < time())
		{
			return $this->closeJob($job, KontorolBatchJobErrorTypes::APP, KontorolBatchJobAppErrors::CLOSER_TIMEOUT, 'Timed out', KontorolBatchJobStatus::FAILED);
		}

		return $this->closeJob($job, null, null, null, KontorolBatchJobStatus::ALMOST_DONE);
	}
}

<?php
/**
 * Base class for all job handler workers.
 *
 * @package Scheduler
 */
abstract class KJobHandlerWorker extends KBatchBase
{
	/**
	 * The job object that currently handled
	 * @var KontorolBatchJob
	 */
	private static $currentJob;

	/**
	 * @param KontorolBatchJob $job
	 * @return KontorolBatchJob
	 */
	abstract protected function exec(KontorolBatchJob $job);

	/**
	 * Returns the job object that currently handled
	 * @return KontorolBatchJob
	 */
	public static function getCurrentJob()
	{
		return self::$currentJob;
	}

	/**
	 * @param KontorolBatchJob $currentJob
	 */
	protected static function setCurrentJob(KontorolBatchJob $currentJob)
	{
		KontorolLog::debug("Start job[$currentJob->id] type[$currentJob->jobType] sub-type[$currentJob->jobSubType] object[$currentJob->jobObjectType] object-id[$currentJob->jobObjectId] partner-id[$currentJob->partnerId] dc[$currentJob->dc] parent-id[$currentJob->parentJobId] root-id[$currentJob->rootJobId]");
		self::$currentJob = $currentJob;

		self::$kClient->setClientTag(self::$clientTag . " partnerId: " . $currentJob->partnerId);
	}

	protected static function unsetCurrentJob()
	{
		$currentJob = self::getCurrentJob();
		KontorolLog::debug("End job[$currentJob->id]");
		self::$currentJob = null;

		self::$kClient->setClientTag(self::$clientTag);
	}

	protected function init()
	{
		$this->saveQueueFilter(static::getType());
	}

	protected function getMaxJobsEachRun()
	{
		if(!KBatchBase::$taskConfig->maxJobsEachRun)
			return 1;

		return KBatchBase::$taskConfig->maxJobsEachRun;
	}

	protected function getJobs()
	{
		$maxJobToPull = KBatchBase::$taskConfig->maxJobToPullToCache;
		return KBatchBase::$kClient->batch->getExclusiveJobs($this->getExclusiveLockKey(), KBatchBase::$taskConfig->maximumExecutionTime,
				$this->getMaxJobsEachRun(), $this->getFilter(), static::getType(), $maxJobToPull);
	}

	public function run($jobs = null)
	{
		if(KBatchBase::$taskConfig->isInitOnly())
			return $this->init();

		if(is_null($jobs))
		{
			try
			{
				$jobs = $this->getJobs();
			}
			catch (Exception $e)
			{
				KontorolLog::err($e->getMessage());
				return null;
			}
		}

		KontorolLog::info(count($jobs) . " jobs to handle");

		if(! count($jobs) > 0)
		{
			KontorolLog::info("Queue size: 0 sent to scheduler");
			$this->saveSchedulerQueue(static::getType(), 0);
			return null;
		}

		foreach($jobs as &$job)
		{
			try
			{
				self::setCurrentJob($job);
				//$this->validateFileAccess($job);
				$job = $this->exec($job);
				self::unimpersonate();
			}
			catch(KontorolException $kex)
			{
				self::unimpersonate();
				$this->closeJobOnError($job,KontorolBatchJobErrorTypes::KONTOROL_API, $kex, KontorolBatchJobStatus::FAILED);
			}
			catch(kApplicativeException $kaex)
			{
				self::unimpersonate();
				$this->closeJobOnError($job,KontorolBatchJobErrorTypes::APP, $kaex, KontorolBatchJobStatus::FAILED);
			}
			catch(kTemporaryException $ktex)
			{
				self::unimpersonate();
				if($ktex->getResetJobExecutionAttempts())
					KBatchBase::$kClient->batch->resetJobExecutionAttempts($job->id, $this->getExclusiveLockKey(), $job->jobType);

				$this->closeJobOnError($job,KontorolBatchJobErrorTypes::RUNTIME, $ktex, KontorolBatchJobStatus::RETRY, $ktex->getData());
			}
			catch(KontorolClientException $kcex)
			{
				self::unimpersonate();
				$this->closeJobOnError($job,KontorolBatchJobErrorTypes::KONTOROL_CLIENT, $kcex, KontorolBatchJobStatus::RETRY);
			}
			catch(Exception $ex)
			{
				self::unimpersonate();
				$this->closeJobOnError($job,KontorolBatchJobErrorTypes::RUNTIME, $ex, KontorolBatchJobStatus::FAILED);
			}
			self::unsetCurrentJob();
		}

		return $jobs;
	}

	protected function closeJobOnError($job, $error, $ex, $status, $data = null)
	{
		try
		{
			self::unimpersonate();
			$job = $this->closeJob($job, $error, $ex->getCode(), "Error: " . $ex->getMessage(), $status, $data);
		}
		catch(Exception $ex)
		{
			KontorolLog::err("Failed to close job after expirencing an error.");
			KontorolLog::err($ex->getMessage());
		}
	}

	/**
	 * @param int $jobId
	 * @param KontorolBatchJob $job
	 * @return KontorolBatchJob
	 */
	protected function updateExclusiveJob($jobId, KontorolBatchJob $job)
	{
		return KBatchBase::$kClient->batch->updateExclusiveJob($jobId, $this->getExclusiveLockKey(), $job);
	}

	/**
	 * @param KontorolBatchJob $job
	 * @return KontorolBatchJob
	 */
	protected function freeExclusiveJob(KontorolBatchJob $job)
	{
		$resetExecutionAttempts = false;
		if ($job->status == KontorolBatchJobStatus::ALMOST_DONE)
			$resetExecutionAttempts = true;

		$response = KBatchBase::$kClient->batch->freeExclusiveJob($job->id, $this->getExclusiveLockKey(), static::getType(), $resetExecutionAttempts);

		if(is_numeric($response->queueSize)) {
			KontorolLog::info("Queue size: $response->queueSize sent to scheduler");
			$this->saveSchedulerQueue(static::getType(), $response->queueSize);
		}

		return $response->job;
	}

	/**
	 * @return KontorolBatchJobFilter
	 */
	protected function getFilter()
	{
		$filter = new KontorolBatchJobFilter();
		if(KBatchBase::$taskConfig->filter)
			$filter = KBatchBase::$taskConfig->filter;

		if (KBatchBase::$taskConfig->minCreatedAtMinutes && is_numeric(KBatchBase::$taskConfig->minCreatedAtMinutes))
		{
			$minCreatedAt = time() - (KBatchBase::$taskConfig->minCreatedAtMinutes * 60);
			$filter->createdAtLessThanOrEqual = $minCreatedAt;
		}

		return $filter;
	}

	/**
	 * @return KontorolExclusiveLockKey
	 */
	protected function getExclusiveLockKey()
	{
		$lockKey = new KontorolExclusiveLockKey();
		$lockKey->schedulerId = $this->getSchedulerId();
		$lockKey->workerId = $this->getId();
		$lockKey->batchIndex = $this->getIndex();

		return $lockKey;
	}

	/**
	 * @param KontorolBatchJob $job
	 */
	protected function onFree(KontorolBatchJob $job)
	{
		$this->onJobEvent($job, KBatchEvent::EVENT_JOB_FREE);
	}

	/**
	 * @param KontorolBatchJob $job
	 */
	protected function onUpdate(KontorolBatchJob $job)
	{
		$this->onJobEvent($job, KBatchEvent::EVENT_JOB_UPDATE);
	}

	/**
	 * @param KontorolBatchJob $job
	 * @param int $event_id
	 */
	protected function onJobEvent(KontorolBatchJob $job, $event_id)
	{
		$event = new KBatchEvent();

		$event->partner_id = $job->partnerId;
		$event->entry_id = $job->entryId;
		$event->bulk_upload_id = $job->bulkJobId;
		$event->batch_parant_id = $job->parentJobId;
		$event->batch_root_id = $job->rootJobId;
		$event->batch_status = $job->status;

		$this->onEvent($event_id, $event);
	}

	/**
	 * @param string $jobType
	 * @return KontorolWorkerQueueFilter
	 */
	protected function getBaseQueueFilter($jobType)
	{
		$filter = $this->getFilter();
		$filter->jobTypeEqual = $jobType;

		$workerQueueFilter = new KontorolWorkerQueueFilter();
		$workerQueueFilter->schedulerId = $this->getSchedulerId();
		$workerQueueFilter->workerId = $this->getId();
		$workerQueueFilter->filter = $filter;
		$workerQueueFilter->jobType = $jobType;

		return $workerQueueFilter;
	}

	/**
	 * @param string $jobType
	 * @param boolean $isCloser
	 * @return KontorolWorkerQueueFilter
	 */
	protected function getQueueFilter($jobType)
	{
		$workerQueueFilter = $this->getBaseQueueFilter($jobType);
		//$workerQueueFilter->filter->statusIn = KontorolBatchJobStatus::PENDING . ',' . KontorolBatchJobStatus::RETRY;

		return $workerQueueFilter;
	}

	/**
	 * @param int $jobType
	 */
	protected function saveQueueFilter($jobType)
	{
		$filter = $this->getQueueFilter($jobType);

		$type = KBatchBase::$taskConfig->name;
		$file = "$type.flt";

		KScheduleHelperManager::saveFilter($file, $filter);
	}

	/**
	 * @param int $jobType
	 * @param int $size
	 */
	protected function saveSchedulerQueue($jobType, $size = null)
	{
		if(is_null($size))
		{
			$workerQueueFilter = $this->getQueueFilter($jobType);
			$size = KBatchBase::$kClient->batch->getQueueSize($workerQueueFilter);
		}

		$queueStatus = new KontorolBatchQueuesStatus();
		$queueStatus->workerId = $this->getId();
		$queueStatus->jobType = $jobType;
		$queueStatus->size = $size;

		$this->saveSchedulerCommands(array($queueStatus));
	}

	/**
	 * @return KontorolBatchJob
	 */
	protected function newEmptyJob()
	{
		return new KontorolBatchJob();
	}

	/**
	 * @param KontorolBatchJob $job
	 * @param string $msg
	 * @param int $status
	 * @param unknown_type $data
	 * @param boolean $remote
	 * @return KontorolBatchJob
	 */
	protected function updateJob(KontorolBatchJob $job, $msg, $status, KontorolJobData $data = null)
	{
		$updateJob = $this->newEmptyJob();

		if(! is_null($msg))
		{
			$updateJob->message = $msg;
			$updateJob->description = $job->description . "\n$msg";
		}

		$updateJob->status = $status;
		$updateJob->data = $data;

		KontorolLog::info("job[$job->id] status: [$status] msg : [$msg]");
		if($this->isUnitTest)
			return $job;

		$job = $this->updateExclusiveJob($job->id, $updateJob);
		if($job instanceof KontorolBatchJob)
			$this->onUpdate($job);

		return $job;
	}

	/**
	 * @param KontorolBatchJob $job
	 * @param int $errType
	 * @param int $errNumber
	 * @param string $msg
	 * @param int $status
	 * @param KontorolJobData $data
	 * @return KontorolBatchJob
	 */
	protected function closeJob(KontorolBatchJob $job, $errType, $errNumber, $msg, $status, $data = null)
	{
		if(! is_null($errType))
			KontorolLog::err($msg);

		$updateJob = $this->newEmptyJob();

		if(! is_null($msg))
		{
			$updateJob->message = $msg;
			$updateJob->description = $job->description . "\n$msg";
		}

		$updateJob->status = $status;
		$updateJob->errType = $errType;
		$updateJob->errNumber = $errNumber;
		$updateJob->data = $data;

		KontorolLog::info("job[$job->id] status: [$status] msg : [$msg]");
		if($this->isUnitTest)
		{
			$job->status = $updateJob->status;
			$job->message = $updateJob->message;
			$job->description = $updateJob->description;
			$job->errType = $updateJob->errType;
			$job->errNumber = $updateJob->errNumber;
			return $job;
		}

		$job = $this->updateExclusiveJob($job->id, $updateJob);
		if($job instanceof KontorolBatchJob)
			$this->onUpdate($job);

		KontorolLog::info("Free job[$job->id]");
		$job = $this->freeExclusiveJob($job);
		if($job instanceof KontorolBatchJob)
			$this->onFree($job);

		return $job;
	}
}

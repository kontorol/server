<?php
/**
 * @package Scheduler
 * @subpackage Debug
 */
chdir(dirname( __FILE__ ) . "/../../");
require_once(__DIR__ . "/../../bootstrap.php");

/**
 * @package Scheduler
 * @subpackage Debug
 */
class KAsyncIndexTest extends PHPUnit_Framework_TestCase
{
	const JOB_NAME = 'KAsyncIndex';
	
	public function testMediaEntryFilter()
	{
		$filter = new KontorolMediaEntryFilter();
		// TODO define the filter
		
		$this->doTestEntry($filter, KontorolBatchJobStatus::FINISHED);
	}

	public function testDocumentEntryFilter()
	{
		$filter = new KontorolDocumentEntryFilter();
		// TODO define the filter
		
		$this->doTestEntry($filter, KontorolBatchJobStatus::FINISHED);
	}
	
	public function doTestEntry(KontorolBaseEntryFilter $filter, $expectedStatus)
	{
		$this->doTest(KontorolIndexObjectType::ENTRY, $filter, $expectedStatus);
	}
	
	public function doTest($objectType, KontorolFilter $filter, $expectedStatus)
	{
		$iniFile = "batch_config.ini";
		$schedulerConfig = new KSchedulerConfig($iniFile);
	
		$taskConfigs = $schedulerConfig->getTaskConfigList();
		$config = null;
		foreach($taskConfigs as $taskConfig)
		{
			if($taskConfig->name == self::JOB_NAME)
				$config = $taskConfig;
		}
		$this->assertNotNull($config);
		
		$jobs = $this->prepareJobs($objectType, $filter);
		
		$config->setTaskIndex(1);
		$instance = new $config->type($config);
		$instance->setUnitTest(true);
		$jobs = $instance->run($jobs); 
		$instance->done();
		
		foreach($jobs as $job)
			$this->assertEquals($expectedStatus, $job->status);
	}
	
	private function prepareJobs($objectType, KontorolFilter $filter)
	{
		$data = new KontorolIndexJobData();
		$data->filter = $filter;
		
		$job = new KontorolBatchJob();
		$job->id = 1;
		$job->jobSubType = $objectType;
		$job->status = KontorolBatchJobStatus::PENDING;
		$job->data = $data;
		
		return array($job);
	}
}

?>

<?php
/**
 * @package Scheduler
 * @subpackage Conversion
 */

/**
 * Will close almost done conversions that sent to remote systems and store the files in the file system.
 * The state machine of the job is as follows:
 * 	 	get almost done conversions 
 * 		check the convert status
 * 		download the converted file
 * 		save recovery file in case of crash
 * 		move the file to the archive
 *
 * @package Scheduler
 * @subpackage Conversion
 */
class KAsyncConvertProfileCloser extends KJobCloserWorker
{
	/* (non-PHPdoc)
	 * @see KBatchBase::getType()
	 */
	public static function getType()
	{
		return KontorolBatchJobType::CONVERT_PROFILE;
	}
	
	/* (non-PHPdoc)
	 * @see KJobHandlerWorker::exec()
	 */
	protected function exec(KontorolBatchJob $job)
	{
		return $this->checkTimeout($job);
	}

	private function checkTimeout(KontorolBatchJob $job)
	{
		
		if($job->queueTime && ($job->queueTime + self::$taskConfig->params->maxTimeBeforeFail) < time())
			return $this->closeJob($job, KontorolBatchJobErrorTypes::APP, KontorolBatchJobAppErrors::CLOSER_TIMEOUT, 'Timed out', KontorolBatchJobStatus::FAILED);
		else if ($this->checkConvertDone($job))
		{
			return $this->closeJob($job, null, null, null, KontorolBatchJobStatus::FINISHED);
		}
			
		return $this->closeJob($job, null, null, null, KontorolBatchJobStatus::ALMOST_DONE);
	}
	
	private function checkConvertDone(KontorolBatchJob $job)
	{
		/**
		 * @var KontorolConvertProfileJobData $data
		 */
		return self::$kClient->batch->checkEntryIsDone($job->id);
	}
}

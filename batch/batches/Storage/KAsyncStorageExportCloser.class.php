<?php
class KAsyncStorageExportCloser extends KJobCloserWorker
{
	/* (non-PHPdoc)
	 * @see KJobHandlerWorker::exec()
	 */
	protected function exec(KontorolBatchJob $job) {
		$this->closeStorageExport($job);
		
	}

	public static function getType()
	{
		return KontorolBatchJobType::STORAGE_EXPORT;
	}
	
	protected function closeStorageExport (KontorolBatchJob $job)
	{
		$storageExportEngine = KExportEngine::getInstance($job->jobSubType, $job->partnerId, $job->data);
		
		$closeResult = $storageExportEngine->verifyExportedResource();
		$this->closeJob($job, null, null, null, $closeResult ? KontorolBatchJobStatus::FINISHED : KontorolBatchJobStatus::ALMOST_DONE);
	}
}

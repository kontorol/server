<?php
/**
 * 
 */
abstract class KExportEngine
{
	/**
	 * @var KontorolStorageJobData
	 */
	protected $data;
	
	/**
	 * @param KontorolStorageJobData $data
	 */
	public function __construct(KontorolStorageJobData $data)
	{
		$this->data = $data;
	}
	
	/**
	 * @return bool
	 */
	abstract function export ();
	
	
	/**
	 * @return bool
	 */
	abstract function verifyExportedResource ();
    
    /**
     * @return bool
     */
    abstract function delete();
	
	/**
	 * @param int $protocol
	 * @param KontorolStorageExportJobData $data
	 * @return KExportEngine
	 */
	public static function getInstance ($protocol, $partnerId, KontorolStorageJobData $data)
	{
		switch ($protocol)
		{
			case KontorolStorageProfileProtocol::FTP:
			case KontorolStorageProfileProtocol::KONTOROL_DC:
			case KontorolStorageProfileProtocol::S3:
			case KontorolStorageProfileProtocol::SCP:
			case KontorolStorageProfileProtocol::SFTP:
			case KontorolStorageProfileProtocol::LOCAL:
				return new KFileTransferExportEngine($data, $protocol);
			default:
				return KontorolPluginManager::loadObject('KExportEngine', $protocol, array($data, $partnerId));
		}
	}

	public function setExportDataFields($storageProfile, $fileSync)
	{
	}
}

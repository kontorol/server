<?php
/**
 * @package Scheduler
 * @subpackage Extract-Media
 */

/**
 * Will extract the media info of a single file 
 *
 * @package Scheduler
 * @subpackage Extract-Media
 */
class KAsyncExtractMedia extends KJobHandlerWorker
{
	/* (non-PHPdoc)
	 * @see KBatchBase::getType()
	 */
	public static function getType()
	{
		return KontorolBatchJobType::EXTRACT_MEDIA;
	}
	
	/* (non-PHPdoc)
	 * @see KJobHandlerWorker::exec()
	 */
	protected function exec(KontorolBatchJob $job)
	{
		return $this->extract($job, $job->data);
	}
	
	/* (non-PHPdoc)
	 * @see KJobHandlerWorker::getMaxJobsEachRun()
	 */
	protected function getMaxJobsEachRun()
	{
		return 1;
	}
	
	/**
	 * Will take a single KontorolBatchJob and extract the media info for the given file
	 */
	private function extract(KontorolBatchJob $job, KontorolExtractMediaJobData $data)
	{
		$srcFileSyncDescriptor = reset($data->srcFileSyncs);
		$mediaFile = null;
		if($srcFileSyncDescriptor)
			$mediaFile = trim($srcFileSyncDescriptor->fileSyncLocalPath);
		
		if(!$this->pollingFileExists($mediaFile))
			return $this->closeJob($job, KontorolBatchJobErrorTypes::APP, KontorolBatchJobAppErrors::NFS_FILE_DOESNT_EXIST, "Source file $mediaFile does not exist", KontorolBatchJobStatus::RETRY);
		
		if(!kFile::isFile($mediaFile))
			return $this->closeJob($job, KontorolBatchJobErrorTypes::APP, KontorolBatchJobAppErrors::NFS_FILE_DOESNT_EXIST, "Source file $mediaFile is not a file", KontorolBatchJobStatus::FAILED);
		
		$this->updateJob($job, "Extracting file media info on $mediaFile", KontorolBatchJobStatus::QUEUED);
		
		$localFileSize = kFile::fileSize($mediaFile);
		$remoteFileSize = isset($srcFileSyncDescriptor->fileSyncRemoteUrl) ? kFile::fileSize($srcFileSyncDescriptor->fileSyncRemoteUrl) : 0;
		if($srcFileSyncDescriptor->fileSyncRemoteUrl && $localFileSize != $remoteFileSize)
		{
			KontorolLog::debug("Moving file from [$mediaFile] to [{$srcFileSyncDescriptor->fileSyncRemoteUrl}]");
			kFile::moveFile($mediaFile, $srcFileSyncDescriptor->fileSyncRemoteUrl, true, true);
		}
		else
		{
			KontorolLog::debug("File will not be synced to shared storage, either fileSyncRemoteUrl not provided or file already exists and size match [$localFileSize] [$remoteFileSize]");
		}

		$mediaInfo = $this->extractMediaInfo($job, $mediaFile);
		
		if(is_null($mediaInfo))
		{
			return $this->closeJob($job, KontorolBatchJobErrorTypes::APP, KontorolBatchJobAppErrors::EXTRACT_MEDIA_FAILED, "Failed to extract media info: $mediaFile", KontorolBatchJobStatus::RETRY);
		}
		
		if($data->calculateComplexity)
			$this->calculateMediaFileComplexity($mediaInfo, $mediaFile);
		
		if($data->detectGOP>0) {
			$this->detectMediaFileGOP($mediaInfo, $mediaFile, $data->detectGOP);
		}

		$duration = $mediaInfo->containerDuration;
		if(!$duration)
			$duration = $mediaInfo->videoDuration;
		if(!$duration)
			$duration = $mediaInfo->audioDuration;
		
		if($data->extractId3Tags)
			$this->extractId3Tags($mediaFile, $data, $duration);
		
		KontorolLog::debug("flavorAssetId [$data->flavorAssetId]");
		$mediaInfo->flavorAssetId = $data->flavorAssetId;
		$mediaInfo = $this->getClient()->batch->addMediaInfo($mediaInfo);
		$data->mediaInfoId = $mediaInfo->id;
		
		$this->updateJob($job, "Saving media info id $mediaInfo->id", KontorolBatchJobStatus::PROCESSED, $data);
		$this->closeJob($job, null, null, null, KontorolBatchJobStatus::FINISHED);
		
		return $job;
	}
	
	/**
	 * extractMediaInfo extract the file info using mediainfo and parse the returned data
	 *  
	 * @param string $mediaFile file full path
	 * @return KontorolMediaInfo or null for failure
	 */
	private function extractMediaInfo($job, $mediaFile)
	{
		$mediaInfo = null;
		try
		{
			$mediaFile = kFile::realPath($mediaFile);
			$engine = KBaseMediaParser::getParser($job->jobSubType, $mediaFile, self::$taskConfig, $job);
			if($engine)
			{
				$mediaInfo = $engine->getMediaInfo();
			}
			else
			{
				$err = "No media info parser engine found for job sub type [$job->jobSubType]";
				return $this->closeJob($job, KontorolBatchJobErrorTypes::APP, KontorolBatchJobAppErrors::ENGINE_NOT_FOUND, $err, KontorolBatchJobStatus::FAILED);
			}
		}
		catch(Exception $ex)
		{
			KontorolLog::err($ex->getMessage());
			$mediaInfo = null;
		}
		
		return $mediaInfo;
	}
	
	/*
	 * Calculate media file 'complexity'
	 */
	private function calculateMediaFileComplexity(&$mediaInfo, $mediaFile)
	{
		$complexityValue = null;
		
		if(isset(self::$taskConfig->params->localTempPath) && file_exists(self::$taskConfig->params->localTempPath))
		{
			$ffmpegBin = isset(self::$taskConfig->params->ffmpegCmd)? self::$taskConfig->params->ffmpegCmd: null;
			$ffprobeBin = isset(self::$taskConfig->params->ffprobeCmd)? self::$taskConfig->params->ffprobeCmd: null;
			$mediaInfoBin = isset(self::$taskConfig->params->mediaInfoCmd)? self::$taskConfig->params->mediaInfoCmd: null;
			$calcComplexity = new KMediaFileComplexity($ffmpegBin, $ffprobeBin, $mediaInfoBin);
			
			$baseOutputName = tempnam(self::$taskConfig->params->localTempPath, "/complexitySampled_".pathinfo($mediaFile, PATHINFO_FILENAME));
			$stat = $calcComplexity->EvaluateSampled($mediaFile, $mediaInfo, $baseOutputName.".mp4");
			if(isset($stat->complexityValue))
			{
				KontorolLog::log("Complexity: value($stat->complexityValue)");
				if(isset($stat->y))
					KontorolLog::log("Complexity: y($stat->y)");
				
				$complexityValue = $stat->complexityValue;
			}
			$scanTmpfiles = glob($baseOutputName.'*');
                        foreach($scanTmpfiles as $tmpFilename) {
                                KontorolLog::log("deleting tmp file: $tmpFilename");
                                unlink($tmpFilename);
                        }
		}
		
		if($complexityValue)
			$mediaInfo->complexityValue = $complexityValue;
	}
	
	private function extractId3Tags($filePath, KontorolExtractMediaJobData $data, $duration)
	{
		try
		{
			$kontorolId3TagParser = new KSyncPointsMediaInfoParser($filePath);
			$syncPointArray = $kontorolId3TagParser->getStreamSyncPointData();
			
			$outputFileName = pathinfo($filePath, PATHINFO_FILENAME) . ".data";
			$localTempSyncPointsFilePath = self::$taskConfig->params->localTempPath . DIRECTORY_SEPARATOR . $outputFileName;
			$sharedTempSyncPointFilePath = self::$taskConfig->params->sharedTempPath . DIRECTORY_SEPARATOR . $outputFileName;

			$retries = 3;
			$interval = (self::$taskConfig->fileSystemCommandInterval ? self::$taskConfig->fileSystemCommandInterval : self::DEFAULT_SLEEP_INTERVAL);
			while ($retries-- > 0)
			{
				if (kFile::setFileContent($localTempSyncPointsFilePath, serialize($syncPointArray)) &&
					$this->moveDataFile($data, $localTempSyncPointsFilePath, $sharedTempSyncPointFilePath))
						return true;
				KontorolLog::log("Failed on moving syncPointArray to server, waiting $interval seconds");
				sleep($interval);
			}
			throw new kTemporaryException("Failed on moving syncPointArray to server. temp path: {$localTempSyncPointsFilePath}");
		}
		catch(kTemporaryException $ktex)
		{
			$this->unimpersonate();
			throw $ktex;
		}
		catch(Exception $ex) 
		{
			$this->unimpersonate();
			KontorolLog::warning("Failed to extract id3tags data or duration data with error: " . print_r($ex, true));
		}
		
	}
	
	private function moveDataFile(KontorolExtractMediaJobData $data, $localTempSyncPointsFilePath, $sharedTempSyncPointFilePath)
	{
		KontorolLog::debug("moving file from [$localTempSyncPointsFilePath] to [$sharedTempSyncPointFilePath]");
		$fileSize = kFile::fileSize($localTempSyncPointsFilePath);
		
		$res = kFile::moveFile($localTempSyncPointsFilePath, $sharedTempSyncPointFilePath, true);
		if (!$res)
			return false;
		clearstatcache();
		
		$this->setFilePermissions($sharedTempSyncPointFilePath);
		if(!$this->checkFileExists($sharedTempSyncPointFilePath, $fileSize))
		{
			KontorolLog::warning("Failed to move file to [$sharedTempSyncPointFilePath]");
			return false;
		}
		else
			$data->destDataFilePath = $sharedTempSyncPointFilePath;
		return true;
	}

	/*
	 *
	 */
	 private function detectMediaFileGOP($mediaInfo, $mediaFile, $interval)
	 {
		KontorolLog::log("Detection interval($interval)");
		list($minGOP,$maxGOP,$detectedGOP) = KFFMpegMediaParser::detectGOP((isset(self::$taskConfig->params->ffprobeCmd)? self::$taskConfig->params->ffprobeCmd: null), $mediaFile, 0, $interval);
		KontorolLog::log("Detected - minGOP($minGOP),maxGOP($maxGOP),detectedGOP($detectedGOP)");
		if(isset($maxGOP)){
			$mediaInfo->maxGOP = $maxGOP;
		}
	 }
}


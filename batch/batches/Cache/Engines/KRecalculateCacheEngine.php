<?php
/**
 * @package Scheduler
 * @subpackage RecalculateCache
 */
abstract class KRecalculateCacheEngine
{
	/**
	 * @param int $objectType of enum KontorolRecalculateCacheType
	 * @return KRecalculateCacheEngine
	 */
	public static function getInstance($objectType)
	{
		switch($objectType)
		{
			case KontorolRecalculateCacheType::RESPONSE_PROFILE:
				return new KRecalculateResponseProfileCacheEngine();
				
			default:
				return KontorolPluginManager::loadObject('KRecalculateCacheEngine', $objectType);
		}
	}
	
	/**
	 * @param KontorolRecalculateCacheJobData $data
	 * @return int cached objects count
	 */
	abstract public function recalculate(KontorolRecalculateCacheJobData $data);
}

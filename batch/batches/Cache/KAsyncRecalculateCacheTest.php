<?php
/**
 * @package Scheduler
 * @subpackage Debug
 */
chdir(dirname( __FILE__ ) . "/../../");
require_once(__DIR__ . "/../../bootstrap.php");

/**
 * @package Scheduler
 * @subpackage Debug
 */
class KAsyncRecalculateCacheTest extends PHPUnit_Framework_TestCase
{
	const JOB_NAME = 'KAsyncRecalculateCache';
	
	public function testMediaEntryFilter()
	{
		$filter = new KontorolMediaEntryFilter();
		// TODO define the filter
		
		$this->doTestCategoryUser($filter, KontorolBatchJobStatus::FINISHED);
	}

	public function doTestCategoryUser(KontorolBaseEntryFilter $filter, $expectedStatus)
	{
		$this->doTest(KontorolCopyObjectType::CATEGORY_USER, $filter, $expectedStatus);
	}
	
	public function doTest($objectType, KontorolFilter $filter, $expectedStatus)
	{
		$iniFile = "batch_config.ini";
		$schedulerConfig = new KSchedulerConfig($iniFile);
	
		$taskConfigs = $schedulerConfig->getTaskConfigList();
		$config = null;
		foreach($taskConfigs as $taskConfig)
		{
			if($taskConfig->name == self::JOB_NAME)
				$config = $taskConfig;
		}
		$this->assertNotNull($config);
		
		$jobs = $this->prepareJobs($objectType, $filter);
		
		$config->setTaskIndex(1);
		$instance = new $config->type($config);
		$instance->setUnitTest(true);
		$jobs = $instance->run($jobs); 
		$instance->done();
		
		foreach($jobs as $job)
			$this->assertEquals($expectedStatus, $job->status);
	}
	
	private function prepareJobs($objectType, KontorolFilter $filter)
	{
		$data = new KontorolCopyJobData();
		$data->filter = $filter;
		
		$job = new KontorolBatchJob();
		$job->id = 1;
		$job->jobSubType = $objectType;
		$job->status = KontorolBatchJobStatus::PENDING;
		$job->data = $data;
		
		return array($job);
	}
}

<?php
/**
 * @package plugins.confMaps
 * @subpackage api.objects
 */
class KontorolConfMapsArray extends KontorolTypedArray
{
	public function __construct()
	{
		parent::__construct("KontorolConfMaps");
	}
	public function insert(KontorolConfMaps $map)
	{
		$this->array[] = $map;
	}
}

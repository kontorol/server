<?php
/**
 * @package plugins.confMaps
 * @subpackage api.objects
 */

class KontorolConfMapsListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolConfMapsArray
	 * @readonly
	 */
	public $objects;
}

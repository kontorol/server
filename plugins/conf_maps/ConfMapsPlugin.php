<?php
/**
 * Sending beacons on various objects
 * @package plugins.confMaps
 */
class ConfMapsPlugin extends KontorolPlugin implements IKontorolServices, IKontorolPermissions, IKontorolAdminConsolePages
{
	const PLUGIN_NAME = 'confMaps';

	public static function getPluginName()
	{
		return self::PLUGIN_NAME;
	}

	/* (non-PHPdoc)
	 * @see IKontorolPermissions::isAllowedPartner()
	 */
	public static function isAllowedPartner($partnerId)
	{
		return ($partnerId == Partner::ADMIN_CONSOLE_PARTNER_ID);
	}

	public static function getServicesMap ()
	{
		$map = array(
			'confMaps' => 'ConfMapsService',
		);
		return $map;
	}

	/*
	 * @see IKontorolAdminConsolePages::getApplicationPages()
	 */
	public static function getApplicationPages()
	{
		$pages = array();
		$pages[] = new ConfigurationMapListAction();
		$pages[] = new ConfigurationMapConfigureAction();
		$pages[] = new ConfigurationMapDiffAction();
		$pages[] = new AuditTrailListAction();
		return $pages;
	}
}

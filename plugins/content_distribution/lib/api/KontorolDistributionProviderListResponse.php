<?php
/**
 * @package plugins.contentDistribution
 * @subpackage api.objects
 */
class KontorolDistributionProviderListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolDistributionProviderArray
	 * @readonly
	 */
	public $objects;
}

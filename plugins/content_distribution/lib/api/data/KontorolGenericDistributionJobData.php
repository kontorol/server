<?php
/**
 * @package plugins.contentDistribution
 * @subpackage api.objects
 */
class KontorolGenericDistributionJobProviderData extends KontorolDistributionJobProviderData
{
	private static $actionAttributes = array(
		KontorolDistributionAction::SUBMIT => 'submitAction',
		KontorolDistributionAction::UPDATE => 'updateAction',
		KontorolDistributionAction::DELETE => 'deleteAction',
		KontorolDistributionAction::FETCH_REPORT => 'fetchReportAction',
	);
	
	/**
	 * @var string
	 */
	public $xml;
	
	/**
	 * @var string
	 */
	public $resultParseData;
	
	/**
	 * @var KontorolGenericDistributionProviderParser
	 */
	public $resultParserType;
	
	public function __construct(KontorolDistributionJobData $distributionJobData = null)
	{
		if(!$distributionJobData)
			return;
			
		$action = KontorolDistributionAction::SUBMIT;
		if($distributionJobData instanceof KontorolDistributionDeleteJobData)
			$action = KontorolDistributionAction::DELETE;
		if($distributionJobData instanceof KontorolDistributionUpdateJobData)
			$action = KontorolDistributionAction::UPDATE;
		if($distributionJobData instanceof KontorolDistributionFetchReportJobData)
			$action = KontorolDistributionAction::FETCH_REPORT;
			
		if(!($distributionJobData->distributionProfile instanceof KontorolGenericDistributionProfile))
		{
			KontorolLog::err("Distribution profile is not generic");
			return;
		}
		
		$this->loadProperties($distributionJobData, $distributionJobData->distributionProfile, $action);
	}
	
	public function loadProperties(KontorolDistributionJobData $distributionJobData, KontorolGenericDistributionProfile $distributionProfile, $action)
	{
		$actionName = self::$actionAttributes[$action];
		
		$genericProviderAction = GenericDistributionProviderActionPeer::retrieveByProviderAndAction($distributionProfile->genericProviderId, $action);
		if(!$genericProviderAction)
		{
			KontorolLog::err("Generic provider [{$distributionProfile->genericProviderId}] action [$actionName] not found");
			return;
		}
		
		if(!$distributionJobData->entryDistribution)
		{
			KontorolLog::err("Entry Distribution object not provided");
			return;
		}
		
		if(!$distributionProfile->$actionName->protocol)
			$distributionProfile->$actionName->protocol = $genericProviderAction->getProtocol();
		if(!$distributionProfile->$actionName->serverUrl)
			$distributionProfile->$actionName->serverUrl = $genericProviderAction->getServerAddress();
		if(!$distributionProfile->$actionName->serverPath)
			$distributionProfile->$actionName->serverPath = $genericProviderAction->getRemotePath();
		if(!$distributionProfile->$actionName->username)
			$distributionProfile->$actionName->username = $genericProviderAction->getRemoteUsername();
		if(!$distributionProfile->$actionName->password)
			$distributionProfile->$actionName->password = $genericProviderAction->getRemotePassword();
		if(!$distributionProfile->$actionName->ftpPassiveMode)
			$distributionProfile->$actionName->ftpPassiveMode = $genericProviderAction->getFtpPassiveMode();
		if(!$distributionProfile->$actionName->httpFieldName)
			$distributionProfile->$actionName->httpFieldName = $genericProviderAction->getHttpFieldName();
		if(!$distributionProfile->$actionName->httpFileName)
			$distributionProfile->$actionName->httpFileName = $genericProviderAction->getHttpFileName();
	
		$entry = entryPeer::retrieveByPKNoFilter($distributionJobData->entryDistribution->entryId);
		if(!$entry)
		{
			KontorolLog::err("Entry [" . $distributionJobData->entryDistribution->entryId . "] not found");
			return;
		}
			
		$mrss = kMrssManager::getEntryMrss($entry);
		if(!$mrss)
		{
			KontorolLog::err("MRSS not returned for entry [" . $entry->getId() . "]");
			return;
		}
			
		$xml = new KDOMDocument();
		if(!$xml->loadXML($mrss))
		{
			KontorolLog::err("MRSS not is not valid XML:\n$mrss\n");
			return;
		}
		
		$key = $genericProviderAction->getSyncKey(GenericDistributionProviderAction::FILE_SYNC_DISTRIBUTION_PROVIDER_ACTION_MRSS_TRANSFORMER);
		if(kFileSyncUtils::fileSync_exists($key))
		{
			$xslPath = kFileSyncUtils::getLocalFilePathForKey($key);
			if($xslPath)
			{
				$xsl = new KDOMDocument();
				$xsl->load($xslPath);
			
				// set variables in the xsl
				$varNodes = $xsl->getElementsByTagName('variable');
				foreach($varNodes as $varNode)
				{
					$nameAttr = $varNode->attributes->getNamedItem('name');
					if(!$nameAttr)
						continue;
						
					$name = $nameAttr->value;
					if($name && $distributionJobData->$name)
					{
						$varNode->textContent = $distributionJobData->$name;
						$varNode->appendChild($xsl->createTextNode($distributionJobData->$name));
					}
				}
				
				$proc = new XSLTProcessor;
				$proc->registerPHPFunctions(kXml::getXslEnabledPhpFunctions());
				$proc->importStyleSheet($xsl);
				
				$xml = $proc->transformToDoc($xml);
				if(!$xml)
				{
					KontorolLog::err("Transform returned false");
					return;
				}
			}
		}
	
		$key = $genericProviderAction->getSyncKey(GenericDistributionProviderAction::FILE_SYNC_DISTRIBUTION_PROVIDER_ACTION_MRSS_VALIDATOR);
		
		list ($fileSync , $local) = kFileSyncUtils::getReadyFileSyncForKey( $key , true , false  );
		if($fileSync)
		{
			/* @var $fileSync FileSync */
			$xsdPath = $fileSync->getFullPath();
			if($xsdPath && !$xml->schemaValidate($xsdPath, $fileSync->getEncryptionKey(), $fileSync->getIv()))
			{
				KontorolLog::err("Inavlid XML:\n" . $xml->saveXML());
				KontorolLog::err("Schema [$xsdPath]:\n" . $fileSync->decrypt());
				return;
			}
		}
		
		$this->xml = $xml->saveXML();
		
		$key = $genericProviderAction->getSyncKey(GenericDistributionProviderAction::FILE_SYNC_DISTRIBUTION_PROVIDER_ACTION_RESULTS_TRANSFORMER);
		if(kFileSyncUtils::fileSync_exists($key))
			$this->resultParseData = kFileSyncUtils::file_get_contents($key, true, false);
			
		$this->resultParserType = $genericProviderAction->getResultsParser();
	}
		
	private static $map_between_objects = array
	(
		"xml" ,
		"resultParseData" ,
		"resultParserType" ,
	);

	public function getMapBetweenObjects ( )
	{
		return array_merge ( parent::getMapBetweenObjects() , self::$map_between_objects );
	}
}

<?php
/**
 * @package plugins.contentDistribution
 * @subpackage api.objects
 */
class KontorolDistributionThumbDimensionsArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolDistributionThumbDimensionsArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
    		$nObj = new KontorolDistributionThumbDimensions();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolDistributionThumbDimensions");
	}
}

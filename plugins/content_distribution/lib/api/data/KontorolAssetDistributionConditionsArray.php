<?php

/**
 * Array of asset distribution conditions
 *
 * @package plugins.contentDistribution
 * @subpackage api.objects
 */
class KontorolAssetDistributionConditionsArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolAssetDistributionConditionsArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
			switch(get_class($obj))
			{
				case 'kAssetDistributionPropertyCondition':
					$nObj = new KontorolAssetDistributionPropertyCondition();
					break;
			}

			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}

		return $newArr;
	}
	
	public function __construct()
	{
		parent::__construct("KontorolAssetDistributionCondition");
	}
}

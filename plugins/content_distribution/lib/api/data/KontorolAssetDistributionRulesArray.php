<?php

/**
 * Array of asset distribution rules
 *
 * @package plugins.contentDistribution
 * @subpackage api.objects
 */
class KontorolAssetDistributionRulesArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolAssetDistributionRulesArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
			$nObj = new KontorolAssetDistributionRule();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}

		return $newArr;
	}

	public function __construct()
	{
		parent::__construct("KontorolAssetDistributionRule");
	}
}

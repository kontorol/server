<?php
/**
 * @package plugins.contentDistribution
 * @subpackage api.objects
 */
class KontorolDistributionValidationErrorArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolDistributionValidationErrorArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
			$nObj = null;
			switch($obj->getErrorType())
			{
				case DistributionErrorType::MISSING_FLAVOR:
    				$nObj = new KontorolDistributionValidationErrorMissingFlavor();
    				break;
    			
				case DistributionErrorType::MISSING_THUMBNAIL:
    				$nObj = new KontorolDistributionValidationErrorMissingThumbnail();
    				break;
    			
				case DistributionErrorType::MISSING_METADATA:
    				$nObj = new KontorolDistributionValidationErrorMissingMetadata();
    				break;

				case DistributionErrorType::MISSING_ASSET:
					$nObj = new KontorolDistributionValidationErrorMissingAsset();
					break;
    			
				case DistributionErrorType::INVALID_DATA:
					if($obj->getMetadataProfileId())
    					$nObj = new KontorolDistributionValidationErrorInvalidMetadata();
    				else
    					$nObj = new KontorolDistributionValidationErrorInvalidData();
    				break;

    				case DistributionErrorType::CONDITION_NOT_MET:
    					$nObj = new KontorolDistributionValidationErrorConditionNotMet();
    					break;

				default:
					break;
			}
			
			if(!$nObj)
				continue;
				
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolDistributionValidationError");
	}
}

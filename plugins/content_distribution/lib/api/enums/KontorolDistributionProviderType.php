<?php
/**
 * @package plugins.contentDistribution
 * @subpackage api.enum
 */
class KontorolDistributionProviderType extends KontorolDynamicEnum implements DistributionProviderType
{
	public static function getEnumClass()
	{
		return 'DistributionProviderType';
	}
}

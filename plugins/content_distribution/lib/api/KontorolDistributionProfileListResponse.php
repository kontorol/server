<?php
/**
 * @package plugins.contentDistribution
 * @subpackage api.objects
 */
class KontorolDistributionProfileListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolDistributionProfileArray
	 * @readonly
	 */
	public $objects;
}

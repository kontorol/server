<?php
/**
 * @package plugins.contentDistribution
 * @subpackage api.objects
 */
class KontorolGenericDistributionProviderActionArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolGenericDistributionProviderActionArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
    		$nObj = new KontorolGenericDistributionProviderAction();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolGenericDistributionProviderAction");
	}
}

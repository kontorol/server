<?php
/**
 * @package plugins.contentDistribution
 * @subpackage api.filters
 */
class KontorolDistributionProviderFilter extends KontorolDistributionProviderBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		throw new Exception("Distribution providers can't be filtered");
	}
}

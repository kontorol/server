<?php
/**
 * @package plugins.contentDistribution
 * @subpackage api.filters
 */
class KontorolEntryDistributionFilter extends KontorolEntryDistributionBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new EntryDistributionFilter();
	}
	
	/* (non-PHPdoc)
	 * @see KontorolRelatedFilter::getListResponse()
	 */
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$c = new Criteria();
		$entryDistributionFilter = $this->toObject();
		
		$entryDistributionFilter->attachToCriteria($c);
		$count = EntryDistributionPeer::doCount($c);
		
		$pager->attachToCriteria ( $c );
		$list = EntryDistributionPeer::doSelect($c);
		
		$response = new KontorolEntryDistributionListResponse();
		$response->objects = KontorolEntryDistributionArray::fromDbArray($list, $responseProfile);
		$response->totalCount = $count;
	
		return $response;
	}
}

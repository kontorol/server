<?php
/**
 * @package plugins.contentDistribution
 * @subpackage api.filters
 */
class KontorolGenericDistributionProviderActionFilter extends KontorolGenericDistributionProviderActionBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new GenericDistributionProviderActionFilter();
	}
}

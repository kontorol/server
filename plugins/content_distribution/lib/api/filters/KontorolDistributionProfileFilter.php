<?php
/**
 * @package plugins.contentDistribution
 * @subpackage api.filters
 */
class KontorolDistributionProfileFilter extends KontorolDistributionProfileBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new DistributionProfileFilter();
	}
}

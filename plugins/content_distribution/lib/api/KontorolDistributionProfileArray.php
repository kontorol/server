<?php
/**
 * @package plugins.contentDistribution
 * @subpackage api.objects
 */
class KontorolDistributionProfileArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolDistributionProfileArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
    		$nObj = KontorolDistributionProfileFactory::createKontorolDistributionProfile($obj->getProviderType());
    		if(!$nObj)
    		{
    			KontorolLog::err("Distribution Profile Factory could not find matching profile type for provider [" . $obj->getProviderType() . "]");
    			continue;
    		}
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolDistributionProfile");
	}
}

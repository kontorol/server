<?php
/**
 * @package plugins.contentDistribution
 * @subpackage api.objects
 */
class KontorolEntryDistributionArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolEntryDistributionArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
    		$nObj = new KontorolEntryDistribution();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolEntryDistribution");
	}
}

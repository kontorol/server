<?php
/**
 * @package plugins.contentDistribution
 * @subpackage api.objects
 */
class KontorolGenericDistributionProviderActionListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolGenericDistributionProviderActionArray
	 * @readonly
	 */
	public $objects;
}

<?php
/**
 * @package plugins.contentDistribution
 * @subpackage api.objects
 */
class KontorolGenericDistributionProviderListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolGenericDistributionProviderArray
	 * @readonly
	 */
	public $objects;
}

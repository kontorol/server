<?php
/**
 * @package plugins.contentDistribution
 * @subpackage api
 */
class KontorolDistributionProfileFactory
{	
	/**
	 * @param int $providerType
	 * @return KontorolDistributionProfile
	 */
	public static function createKontorolDistributionProfile($providerType)
	{
		if($providerType == KontorolDistributionProviderType::GENERIC)
			return new KontorolGenericDistributionProfile();
			
		if($providerType == KontorolDistributionProviderType::SYNDICATION)
			return new KontorolSyndicationDistributionProfile();
			
		$distributionProfile = KontorolPluginManager::loadObject('KontorolDistributionProfile', $providerType);
		if($distributionProfile)
			return $distributionProfile;
		
		return null;
	}
}

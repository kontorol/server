<?php
/**
 * @package plugins.contentDistribution
 * @subpackage api.objects
 */
class KontorolDistributionProviderArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolDistributionProviderArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
    		$nObj = new KontorolGenericDistributionProvider();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolDistributionProvider");
	}
}

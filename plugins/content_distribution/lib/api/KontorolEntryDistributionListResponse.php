<?php
/**
 * @package plugins.contentDistribution
 * @subpackage api.objects
 */
class KontorolEntryDistributionListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolEntryDistributionArray
	 * @readonly
	 */
	public $objects;
}

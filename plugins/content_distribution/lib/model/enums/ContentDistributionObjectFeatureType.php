<?php
/**
 * @package plugins.contentDistribution
 * @subpackage model.enum
 */
class ContentDistributionObjectFeatureType implements IKontorolPluginEnum, ObjectFeatureType
{
	const CONTENT_DISTRIBUTION = 'ContentDistribution';
	
	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalValues()
	 */
	public static function getAdditionalValues() 
	{
		return array
		(
			'CONTENT_DISTRIBUTION' => self::CONTENT_DISTRIBUTION,
		);
		
	}

	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalDescriptions()
	 */
	public static function getAdditionalDescriptions() {
		return array();
		
	}
}

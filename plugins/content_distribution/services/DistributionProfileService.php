<?php
/**
 * Distribution Profile service
 *
 * @service distributionProfile
 * @package plugins.contentDistribution
 * @subpackage api.services
 */
class DistributionProfileService extends KontorolBaseService
{
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);

		$this->applyPartnerFilterForClass('DistributionProfile');
		
		if(!ContentDistributionPlugin::isAllowedPartner($this->getPartnerId()))
			throw new KontorolAPIException(KontorolErrors::FEATURE_FORBIDDEN, ContentDistributionPlugin::PLUGIN_NAME);
	}
	
	/**
	 * Add new Distribution Profile
	 * 
	 * @action add
	 * @param KontorolDistributionProfile $distributionProfile
	 * @return KontorolDistributionProfile
	 * @throws ContentDistributionErrors::DISTRIBUTION_PROVIDER_NOT_FOUND
	 */
	function addAction(KontorolDistributionProfile $distributionProfile)
	{
		$distributionProfile->validatePropertyMinLength("name", 1);
		$distributionProfile->validatePropertyNotNull("providerType");
					
		if(is_null($distributionProfile->status))
			$distributionProfile->status = KontorolDistributionProfileStatus::DISABLED;
		
		$providerType = kPluginableEnumsManager::apiToCore('DistributionProviderType', $distributionProfile->providerType);
		$dbDistributionProfile = DistributionProfilePeer::createDistributionProfile($providerType);
		if(!$dbDistributionProfile)
			throw new KontorolAPIException(ContentDistributionErrors::DISTRIBUTION_PROVIDER_NOT_FOUND, $distributionProfile->providerType);
			
		$distributionProfile->toInsertableObject($dbDistributionProfile);
		$dbDistributionProfile->setPartnerId($this->impersonatedPartnerId);
		$dbDistributionProfile->save();
		
		$distributionProfile = KontorolDistributionProfileFactory::createKontorolDistributionProfile($dbDistributionProfile->getProviderType());
		$distributionProfile->fromObject($dbDistributionProfile, $this->getResponseProfile());
		return $distributionProfile;
	}
	
	/**
	 * Get Distribution Profile by id
	 * 
	 * @action get
	 * @param int $id
	 * @return KontorolDistributionProfile
	 * @throws ContentDistributionErrors::DISTRIBUTION_PROFILE_NOT_FOUND
	 */
	function getAction($id)
	{
		$dbDistributionProfile = DistributionProfilePeer::retrieveByPK($id);
		if (!$dbDistributionProfile)
			throw new KontorolAPIException(ContentDistributionErrors::DISTRIBUTION_PROFILE_NOT_FOUND, $id);
			
		$distributionProfile = KontorolDistributionProfileFactory::createKontorolDistributionProfile($dbDistributionProfile->getProviderType());
		$distributionProfile->fromObject($dbDistributionProfile, $this->getResponseProfile());
		return $distributionProfile;
	}
	
	/**
	 * Update Distribution Profile by id
	 * 
	 * @action update
	 * @param int $id
	 * @param KontorolDistributionProfile $distributionProfile
	 * @return KontorolDistributionProfile
	 * @throws ContentDistributionErrors::DISTRIBUTION_PROFILE_NOT_FOUND
	 */
	function updateAction($id, KontorolDistributionProfile $distributionProfile)
	{
		$dbDistributionProfile = DistributionProfilePeer::retrieveByPK($id);
		if (!$dbDistributionProfile)
			throw new KontorolAPIException(ContentDistributionErrors::DISTRIBUTION_PROFILE_NOT_FOUND, $id);
		
		if ($distributionProfile->name !== null)
			$distributionProfile->validatePropertyMinLength("name", 1);
			
		$distributionProfile->toUpdatableObject($dbDistributionProfile);
		$dbDistributionProfile->save();
		
		$distributionProfile = KontorolDistributionProfileFactory::createKontorolDistributionProfile($dbDistributionProfile->getProviderType());
		$distributionProfile->fromObject($dbDistributionProfile, $this->getResponseProfile());
		return $distributionProfile;
	}
	
	/**
	 * Update Distribution Profile status by id
	 * 
	 * @action updateStatus
	 * @param int $id
	 * @param KontorolDistributionProfileStatus $status
	 * @return KontorolDistributionProfile
	 * @throws ContentDistributionErrors::DISTRIBUTION_PROFILE_NOT_FOUND
	 */
	function updateStatusAction($id, $status)
	{
		$dbDistributionProfile = DistributionProfilePeer::retrieveByPK($id);
		if (!$dbDistributionProfile)
			throw new KontorolAPIException(ContentDistributionErrors::DISTRIBUTION_PROFILE_NOT_FOUND, $id);
		
		$dbDistributionProfile->setStatus($status);
		$dbDistributionProfile->save();
		
		$distributionProfile = KontorolDistributionProfileFactory::createKontorolDistributionProfile($dbDistributionProfile->getProviderType());
		$distributionProfile->fromObject($dbDistributionProfile, $this->getResponseProfile());
		return $distributionProfile;
	}
	
	/**
	 * Delete Distribution Profile by id
	 * 
	 * @action delete
	 * @param int $id
	 * @throws ContentDistributionErrors::DISTRIBUTION_PROFILE_NOT_FOUND
	 */
	function deleteAction($id)
	{
		$dbDistributionProfile = DistributionProfilePeer::retrieveByPK($id);
		if (!$dbDistributionProfile)
			throw new KontorolAPIException(ContentDistributionErrors::DISTRIBUTION_PROFILE_NOT_FOUND, $id);

		$dbDistributionProfile->setStatus(DistributionProfileStatus::DELETED);
		$dbDistributionProfile->save();
	}
	
	
	/**
	 * List all distribution providers
	 * 
	 * @action list
	 * @param KontorolDistributionProfileFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return KontorolDistributionProfileListResponse
	 */
	function listAction(KontorolDistributionProfileFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
			$filter = new KontorolDistributionProfileFilter();
			
		if (!$pager)
		    $pager = new KontorolFilterPager();
        
		 //Change the pageSize to support clients who hae had all their dist. profiles listed in Eagle
		$pager->pageSize = 100;
		
		$c = new Criteria();
		$distributionProfileFilter = new DistributionProfileFilter();
		$filter->toObject($distributionProfileFilter);
		
		$distributionProfileFilter->attachToCriteria($c);
		$count = DistributionProfilePeer::doCount($c);
		
		$pager->attachToCriteria($c);
		$list = DistributionProfilePeer::doSelect($c);
		
		$response = new KontorolDistributionProfileListResponse();
		$response->objects = KontorolDistributionProfileArray::fromDbArray($list, $this->getResponseProfile());
		$response->totalCount = $count;
	
		return $response;
	}	
	
	/**
	 * @action listByPartner
	 * @param KontorolPartnerFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return KontorolDistributionProfileListResponse
	 */
	public function listByPartnerAction(KontorolPartnerFilter $filter = null, KontorolFilterPager $pager = null)
	{
		$c = new Criteria();
		
		if (!is_null($filter))
		{
			
			$partnerFilter = new partnerFilter();
			$filter->toObject($partnerFilter);
			$partnerFilter->set('_gt_id', 0);
			
			$partnerCriteria = new Criteria();
			$partnerFilter->attachToCriteria($partnerCriteria);
			$partnerCriteria->setLimit(1000);
			$partnerCriteria->clearSelectColumns();
			$partnerCriteria->addSelectColumn(PartnerPeer::ID);
			$stmt = PartnerPeer::doSelectStmt($partnerCriteria);
			
			if($stmt->rowCount() < 1000) // otherwise, it's probably all partners
			{
				$partnerIds = $stmt->fetchAll(PDO::FETCH_COLUMN);
				$c->add(DistributionProfilePeer::PARTNER_ID, $partnerIds, Criteria::IN);
			}
		}
			
		if (is_null($pager))
			$pager = new KontorolFilterPager();
			
		$c->addDescendingOrderByColumn(DistributionProfilePeer::CREATED_AT);
		
		$totalCount = DistributionProfilePeer::doCount($c);
		$pager->attachToCriteria($c);
		$list = DistributionProfilePeer::doSelect($c);
		$newList = KontorolDistributionProfileArray::fromDbArray($list, $this->getResponseProfile());
		
		$response = new KontorolDistributionProfileListResponse();
		$response->totalCount = $totalCount;
		$response->objects = $newList;
		return $response;
	}
}

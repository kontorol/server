<?php
/**
 * Distribution Provider service
 *
 * @service distributionProvider
 * @package plugins.contentDistribution
 * @subpackage api.services
 */
class DistributionProviderService extends KontorolBaseService
{
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		$this->applyPartnerFilterForClass('GenericDistributionProvider');
		
		if(!ContentDistributionPlugin::isAllowedPartner($this->getPartnerId()))
			throw new KontorolAPIException(KontorolErrors::FEATURE_FORBIDDEN, ContentDistributionPlugin::PLUGIN_NAME);
	}
	
	
	/**
	 * List all distribution providers
	 * 
	 * @action list
	 * @param KontorolDistributionProviderFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return KontorolDistributionProviderListResponse
	 */
	function listAction(KontorolDistributionProviderFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
			$filter = new KontorolDistributionProviderFilter();
			
		$c = new Criteria();
		if($filter instanceof KontorolGenericDistributionProviderFilter)
		{
			$genericDistributionProviderFilter = new GenericDistributionProviderFilter();
			$filter->toObject($genericDistributionProviderFilter);
			
			$genericDistributionProviderFilter->attachToCriteria($c);
		}
		$count = GenericDistributionProviderPeer::doCount($c);
		
		if (! $pager)
			$pager = new KontorolFilterPager ();
		$pager->attachToCriteria($c);
		$list = GenericDistributionProviderPeer::doSelect($c);
		
		$response = new KontorolDistributionProviderListResponse();
		$response->objects = KontorolDistributionProviderArray::fromDbArray($list, $this->getResponseProfile());
		$response->totalCount = $count;
	
		$syndicationProvider = new KontorolSyndicationDistributionProvider();
		$syndicationProvider->fromObject(SyndicationDistributionProvider::get());
		$response->objects[] = $syndicationProvider;
		$response->totalCount++;
		
		$pluginInstances = KontorolPluginManager::getPluginInstances('IKontorolContentDistributionProvider');
		foreach($pluginInstances as $pluginInstance)
		{
			$provider = $pluginInstance->getKontorolProvider();
			if($provider)
			{
				$response->objects[] = $provider;
				$response->totalCount++;
			}
		}
		
		return $response;
	}	
}

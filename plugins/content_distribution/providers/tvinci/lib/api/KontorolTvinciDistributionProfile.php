<?php
/**
 * @package plugins.tvinciDistribution
 * @subpackage api.objects
 */
class KontorolTvinciDistributionProfile extends KontorolConfigurableDistributionProfile
{
	/**
	 * @var string
	 */
	public $ingestUrl;
	
	/**
	 * @var string
	 */
	public $username;

	/**
	 * @var string
	 */
	public $password;

	/**
	 * Tags array for Tvinci distribution
	 * @var KontorolTvinciDistributionTagArray
	 */
	public $tags;

	/**
	 * @var string
	 */
	public $xsltFile;

	/**
	 * @var string
	 */
	public $innerType;

	/**
	 * @var KontorolTvinciAssetsType
	 */
	public $assetsType;

	/*
	 * mapping between the field on this object (on the left) and the setter/getter on the object (on the right)
	 */
	private static $map_between_objects = array 
	(
		'ingestUrl',
		'username',
		'password',
		'tags',
		'xsltFile',
		'innerType',
		'assetsType',
	 );
		 
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}

	/**
	 * @param TvinciDistributionProfile $srcObj
	 * @param KontorolDetachedResponseProfile $responseProfile
	 */
	protected function doFromObject($srcObj, KontorolDetachedResponseProfile $responseProfile = null)
	{
		parent::doFromObject($srcObj, $responseProfile);
		$this->tags = KontorolTvinciDistributionTagArray::fromDbArray($srcObj->getTags());
	}
}

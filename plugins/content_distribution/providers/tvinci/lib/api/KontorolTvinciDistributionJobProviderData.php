<?php
/**
 * @package plugins.tvinciDistribution
 * @subpackage api.objects
 */
class KontorolTvinciDistributionJobProviderData extends KontorolConfigurableDistributionJobProviderData
{
	/**
	 * @var string
	 */
	public $xml;

	public function __construct(KontorolDistributionJobData $distributionJobData = null)
	{
		parent::__construct($distributionJobData);

		if( (!$distributionJobData) ||
			(!($distributionJobData->distributionProfile instanceof KontorolTvinciDistributionProfile)) ||
			(! $distributionJobData->entryDistribution) )
			return;

		$entry = null;
		if ( $distributionJobData->entryDistribution->entryId )
		{
			$entry = entryPeer::retrieveByPK($distributionJobData->entryDistribution->entryId);
		}

		if ( ! $entry ) {
			KontorolLog::err("Can't find entry with id: {$distributionJobData->entryDistribution->entryId}");
			return;
		}

		$feedHelper = new TvinciDistributionFeedHelper($distributionJobData->distributionProfile, $entry);

		if ($distributionJobData instanceof KontorolDistributionSubmitJobData)
		{
			$this->xml = $feedHelper->buildSubmitFeed();
		}
		elseif ($distributionJobData instanceof KontorolDistributionUpdateJobData)
		{
			$this->xml = $feedHelper->buildUpdateFeed();
		}
		elseif ($distributionJobData instanceof KontorolDistributionDeleteJobData)
		{
			$this->xml = $feedHelper->buildDeleteFeed();
		}
	}

	private static $map_between_objects = array
	(
		'xml',
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}
}

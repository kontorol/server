<?php
/**
 * @package plugins.tvinciDistribution
 * @subpackage api.objects
 */
class KontorolTvinciDistributionTagArray extends KontorolTypedArray
{
	public function __construct()
	{
		parent::__construct("KontorolTvinciDistributionTag");
	}
	
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolTvinciDistributionTagArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
			$nObj = new KontorolTvinciDistributionTag();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}

		return $newArr;
	}
}

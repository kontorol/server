<?php
/**
 * @package plugins.facebookDistribution
 * @subpackage api.enum
 */
class KontorolFacebookDistributionField extends KontorolStringEnum implements FacebookDistributionField
{
	// see FacebookDistributionField interface
}

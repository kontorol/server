<?php
/**
 * @package plugins.facebookDistribution
 * @subpackage api.objects
 *
 */
class KontorolFacebookCaptionDistributionInfo extends KontorolObject{

	/**
	 * @var string
	 */
	public $language; 
	
	/**
	 * @var string
	 */
	public $label; 
	
	/**
	 * @var string
	 */
	public $filePath;
	
	/**
	 * @var string
	 */
	public $remoteId;
	
	/**
	 * @var string
	 */
	public $version;
	
	/**
	 * @var string
	 */
	public $assetId;
		
}

<?php
/**
 * @package plugins.freewheelGenericDistribution
 * @subpackage api.enum
 */
class KontorolFreewheelGenericDistributionField extends KontorolStringEnum implements FreewheelGenericDistributionField
{
	// see FreewheelGenericDistributionField interface
}

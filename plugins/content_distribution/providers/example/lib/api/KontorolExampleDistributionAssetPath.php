<?php
/**
 * @package plugins.exampleDistribution
 * @subpackage api.objects
 */
class KontorolExampleDistributionAssetPath extends KontorolDistributionJobProviderData
{
	/**
	 * @var string
	 */
	public $path;
}

<?php
/**
 * @package plugins.doubleClickDistribution
 * @subpackage api
 */
class DoubleClickDistributionErrors extends KontorolErrors
{
	const INVALID_FEED_URL = "INVALID_FEED_URL;;Invalid feed URL";
}

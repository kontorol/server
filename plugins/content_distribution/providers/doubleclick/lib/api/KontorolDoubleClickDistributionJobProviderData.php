<?php
/**
* @package plugins.doubleClickDistribution
 * @subpackage api.objects
 */
class KontorolDoubleClickDistributionJobProviderData extends KontorolDistributionJobProviderData
{
	public function __construct(KontorolDistributionJobData $distributionJobData = null)
	{
	}

	private static $map_between_objects = array();

	public function getMapBetweenObjects()
	{
		return array_merge ( parent::getMapBetweenObjects() , self::$map_between_objects );
	}	
}

<?php
/**
 * @package plugins.avnDistribution
 * @subpackage api
 */
class AvnDistributionErrors extends KontorolErrors
{
	const INVALID_FEED_URL = "INVALID_FEED_URL;;Invalid feed URL";
}

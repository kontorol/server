<?php
/**
 * @package plugins.uverseClickToOrderDistribution
 * @subpackage api.enum
 */
class KontorolUverseClickToOrderDistributionField extends KontorolStringEnum implements UverseClickToOrderDistributionField
{
	// see UverseClicktoOrderDistributionField interface
}

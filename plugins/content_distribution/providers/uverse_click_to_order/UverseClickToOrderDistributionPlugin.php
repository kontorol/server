<?php
/**
 * @package plugins.uverseClickToOrderDistribution
 */
class UverseClickToOrderDistributionPlugin extends KontorolPlugin implements IKontorolPermissions, IKontorolEnumerator, IKontorolPending, IKontorolObjectLoader, IKontorolContentDistributionProvider, IKontorolEventConsumers, IKontorolServices
{
	const PLUGIN_NAME = 'uverseClickToOrderDistribution';
	const UVERSE_CLICK_TO_ORDER_EVENT_CONSUMER = "kUverseClickToOrderEventConsumer";
	const CONTENT_DSTRIBUTION_VERSION_MAJOR = 1;
	const CONTENT_DSTRIBUTION_VERSION_MINOR = 0;
	const CONTENT_DSTRIBUTION_VERSION_BUILD = 0;
	
	public static function getPluginName()
	{
		return self::PLUGIN_NAME;
	}
	
	public static function dependsOn()
	{
		$contentDistributionVersion = new KontorolVersion(
			self::CONTENT_DSTRIBUTION_VERSION_MAJOR,
			self::CONTENT_DSTRIBUTION_VERSION_MINOR,
			self::CONTENT_DSTRIBUTION_VERSION_BUILD);
			
		$dependency = new KontorolDependency(ContentDistributionPlugin::getPluginName(), $contentDistributionVersion);
		return array($dependency);
	}
	
	public static function isAllowedPartner($partnerId)
	{
		if($partnerId == Partner::ADMIN_CONSOLE_PARTNER_ID)
			return true;
			
		$partner = PartnerPeer::retrieveByPK($partnerId);
		return $partner->getPluginEnabled(ContentDistributionPlugin::getPluginName());
	}
	
	/**
	 * @return array<string> list of enum classes names that extend the base enum name
	 */
	public static function getEnums($baseEnumName = null)
	{
		if(is_null($baseEnumName))
			return array('UverseClickToOrderDistributionProviderType');
	
		if($baseEnumName == 'DistributionProviderType')
			return array('UverseClickToOrderDistributionProviderType');
			
		return array();
	}
	
	/**
	 * @param string $baseClass
	 * @param string $enumValue
	 * @param array $constructorArgs
	 * @return object
	 */
	
	public static function loadObject($baseClass, $enumValue, array $constructorArgs = null)
	{			
		$objectClass = self::getObjectClass($baseClass, $enumValue);
		
		if (is_null($objectClass)) {
			return null;
		}
		
		if (!is_null($constructorArgs))
		{
			$reflect = new ReflectionClass($objectClass);
			return $reflect->newInstanceArgs($constructorArgs);
		}
		else
		{
			return new $objectClass();
		}
	}
		
	/**
	 * @param string $baseClass
	 * @param string $enumValue
	 * @return string
	 */
	public static function getObjectClass($baseClass, $enumValue)
	{
		// client side apps like batch and admin console
		if (class_exists('KontorolClient') && $enumValue == KontorolDistributionProviderType::UVERSE_CLICK_TO_ORDER)
		{
			if($baseClass == 'KontorolDistributionProfile')
				return 'KontorolUverseClickToOrderDistributionProfile';
		}
		
		if (class_exists('Kontorol_Client_Client') && $enumValue == Kontorol_Client_ContentDistribution_Enum_DistributionProviderType::UVERSE_CLICK_TO_ORDER)
		{
			if($baseClass == 'Form_ProviderProfileConfiguration')
				return 'Form_UverseClickToOrderProfileConfiguration';
				
			if($baseClass == 'Kontorol_Client_ContentDistribution_Type_DistributionProfile')
				return 'Kontorol_Client_UverseClickToOrderDistribution_Type_UverseClickToOrderDistributionProfile';
		}
		
		if($baseClass == 'KontorolDistributionProfile' && $enumValue == self::getDistributionProviderTypeCoreValue(UverseClickToOrderDistributionProviderType::UVERSE_CLICK_TO_ORDER))
			return 'KontorolUverseClickToOrderDistributionProfile';
			
		if($baseClass == 'DistributionProfile' && $enumValue == self::getDistributionProviderTypeCoreValue(UverseClickToOrderDistributionProviderType::UVERSE_CLICK_TO_ORDER))
			return 'UverseClickToOrderDistributionProfile';
			
		return null;
	}
	
	/**
	 * Return a distribution provider instance
	 * 
	 * @return IDistributionProvider
	 */
	public static function getProvider()
	{
		return UverseClickToOrderDistributionProvider::get();
	}
	
	/**
	 * Return an API distribution provider instance
	 * 
	 * @return KontorolDistributionProvider
	 */
	public static function getKontorolProvider()
	{
		$distributionProvider = new KontorolUverseClickToOrderDistributionProvider();
		$distributionProvider->fromObject(self::getProvider());
		return $distributionProvider;
	}
	
	/**
	 * Append provider specific nodes and attributes to the MRSS
	 * 
	 * @param EntryDistribution $entryDistribution
	 * @param SimpleXMLElement $mrss
	 */
	public static function contributeMRSS(EntryDistribution $entryDistribution, SimpleXMLElement $mrss)
	{
		 // append UverseClickToOrder specific report statistics
	    $distributionProfile = DistributionProfilePeer::retrieveByPK($entryDistribution->getDistributionProfileId());
		$mrss->addChild('background_image_wide', $distributionProfile->getBackgroundImageWide());
		$mrss->addChild('background_image_standard', $distributionProfile->getBackgroundImageStandard());
	}

	/**
	 * @return int id of dynamic enum in the DB.
	 */
	public static function getDistributionProviderTypeCoreValue($valueName)
	{
		$value = self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
		return kPluginableEnumsManager::apiToCore('DistributionProviderType', $value);
	}
	
	/**
	 * @return string external API value of dynamic enum.
	 */
	public static function getApiValue($valueName)
	{
		return self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolEventConsumers::getEventConsumers()
	 */
	public static function getEventConsumers()
	{
		return array(
			self::UVERSE_CLICK_TO_ORDER_EVENT_CONSUMER,
		);
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolServices::getServicesMap()
	 */
	public static function getServicesMap()
	{
		return array(
			'uverseClickToOrder' => 'UverseClickToOrderService',
		);
	}
}

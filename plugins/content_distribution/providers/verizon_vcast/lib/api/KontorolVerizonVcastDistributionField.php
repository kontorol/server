<?php
/**
 * @package plugins.verizonVcastDistribution
 * @subpackage api.enum
 */
class KontorolVerizonVcastDistributionField extends KontorolStringEnum implements VerizonVcastDistributionField
{
	// see VerizonVcastDistributionField interface
}

<?php
/**
 * @package plugins.uverseDistribution
 * @subpackage lib
 */
class UverseDistributionEngine extends DistributionEngine implements 
	IDistributionEngineUpdate,
	IDistributionEngineSubmit,
	IDistributionEngineDelete
{
	/* (non-PHPdoc)
	 * @see IDistributionEngineSubmit::submit()
	 */
	public function submit(KontorolDistributionSubmitJobData $data)
	{
		if(!$data->distributionProfile || !($data->distributionProfile instanceof KontorolUverseDistributionProfile))
			KontorolLog::err("Distribution profile must be of type KontorolUverseDistributionProfile");
	
		if(!$data->providerData || !($data->providerData instanceof KontorolUverseDistributionJobProviderData))
			KontorolLog::err("Provider data must be of type KontorolUverseDistributionJobProviderData");
		
		$this->sendFile($data, $data->distributionProfile, $data->providerData);
		
		return true;
	}

	/* (non-PHPdoc)
	 * @see IDistributionEngineDelete::delete()
	 */
	public function delete(KontorolDistributionDeleteJobData $data)
	{
		if(!$data->distributionProfile || !($data->distributionProfile instanceof KontorolUverseDistributionProfile))
			KontorolLog::err("Distribution profile must be of type KontorolUverseDistributionProfile");
	
		if(!$data->providerData || !($data->providerData instanceof KontorolUverseDistributionJobProviderData))
			KontorolLog::err("Provider data must be of type KontorolUverseDistributionJobProviderData");
		
		$this->handleDelete($data, $data->distributionProfile, $data->providerData);
		
		return true;
	}

	/* (non-PHPdoc)
	 * @see IDistributionEngineUpdate::update()
	 */
	public function update(KontorolDistributionUpdateJobData $data)
	{
		if(!$data->distributionProfile || !($data->distributionProfile instanceof KontorolUverseDistributionProfile))
			KontorolLog::err("Distribution profile must be of type KontorolUverseDistributionProfile");
	
		if(!$data->providerData || !($data->providerData instanceof KontorolUverseDistributionJobProviderData))
			KontorolLog::err("Provider data must be of type KontorolUverseDistributionJobProviderData");
		
		$this->sendFile($data, $data->distributionProfile, $data->providerData);
		
		return true;
	}
	
	/**
	 * @param KontorolDistributionJobData $data
	 * @param KontorolUverseDistributionProfile $distributionProfile
	 * @param KontorolUverseDistributionJobProviderData $providerData
	 */
	protected function sendFile(KontorolDistributionJobData $data, KontorolUverseDistributionProfile $distributionProfile, KontorolUverseDistributionJobProviderData $providerData)
	{
		$ftpManager = $this->getFTPManager($distributionProfile);
		
		$providerData->remoteAssetFileName = $this->getRemoteFileName($distributionProfile, $providerData);
		$providerData->remoteAssetUrl = $this->getRemoteUrl($distributionProfile, $providerData);
		if ($ftpManager->fileExists($providerData->remoteAssetFileName))
			KontorolLog::err('The file ['.$providerData->remoteAssetFileName.'] already exists at the FTP');
		else
			$ftpManager->putFile($providerData->remoteAssetFileName, $providerData->localAssetFilePath);
	}
	
	/**
	 * @param KontorolDistributionJobData $data
	 * @param KontorolUverseDistributionProfile $distributionProfile
	 * @param KontorolUverseDistributionJobProviderData $providerData
	 */
	protected function handleDelete(KontorolDistributionJobData $data, KontorolUverseDistributionProfile $distributionProfile, KontorolUverseDistributionJobProviderData $providerData)
	{
		$ftpManager = $this->getFTPManager($distributionProfile);
		$ftpManager->delFile($providerData->remoteAssetFileName);
	}
	
	/**
	 * 
	 * @param KontorolUverseDistributionProfile $distributionProfile
	 * @return ftpMgr
	 */
	protected function getFTPManager(KontorolUverseDistributionProfile $distributionProfile)
	{
		$host = $distributionProfile->ftpHost;
		$login = $distributionProfile->ftpLogin;
		$password = $distributionProfile->ftpPassword;
		$engineOptions = isset(KBatchBase::$taskConfig->engineOptions) ? KBatchBase::$taskConfig->engineOptions->toArray() : array();
		$ftpManager = kFileTransferMgr::getInstance(kFileTransferMgrType::FTP, $engineOptions);
		$ftpManager->login($host, $login, $password);
		return $ftpManager;
	}
	
	/**
	 * @param KontorolUverseDistributionProfile $distributionProfile
	 * @param KontorolUverseDistributionJobProviderData $providerData
	 * @return string
	 */
	protected function getRemoteFileName(KontorolUverseDistributionProfile $distributionProfile, KontorolUverseDistributionJobProviderData $providerData)
	{
		return pathinfo($providerData->localAssetFilePath, PATHINFO_BASENAME);
	}
	
	/**
	 * @param KontorolUverseDistributionProfile $distributionProfile
	 * @param KontorolUverseDistributionJobProviderData $providerData
	 * @return string
	 */
	protected function getRemoteUrl(KontorolUverseDistributionProfile $distributionProfile, KontorolUverseDistributionJobProviderData $providerData)
	{
		$remoteFileName = $this->getRemoteFileName($distributionProfile, $providerData);
		return 'ftp://'.$distributionProfile->ftpHost.'/'.$remoteFileName;
	}
}

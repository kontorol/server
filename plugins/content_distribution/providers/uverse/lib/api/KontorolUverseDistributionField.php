<?php
/**
 * @package plugins.uverseDistribution
 * @subpackage api.enum
 */
class KontorolUverseDistributionField extends KontorolStringEnum implements UverseDistributionField
{
	// see UverseDistributionField interface
}

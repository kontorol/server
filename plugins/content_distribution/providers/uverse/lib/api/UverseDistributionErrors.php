<?php
/**
 * @package plugins.uverseDistribution
 * @subpackage api.objects
 */
class UverseDistributionErrors extends KontorolErrors
{
	const INVALID_FEED_URL = "INVALID_FEED_URL;;Invalid feed URL";
}

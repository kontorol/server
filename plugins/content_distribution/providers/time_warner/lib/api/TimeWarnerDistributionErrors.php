<?php
/**
 * @package plugins.timeWarnerDistribution
 * @subpackage api
 */
class TimeWarnerDistributionErrors extends KontorolErrors
{
	const INVALID_FEED_URL = "INVALID_FEED_URL;;Invalid feed URL";
}

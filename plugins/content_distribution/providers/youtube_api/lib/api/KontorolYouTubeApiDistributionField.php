<?php
/**
 * @package plugins.youtubeApiDistribution
 * @subpackage api.enum
 */
class KontorolYouTubeApiDistributionField extends KontorolStringEnum implements YouTubeApiDistributionField
{
	// see YouTubeApiDistributionField interface
}

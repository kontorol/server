<?php
/**
 * @package plugins.youtubeApiDistribution
 * @subpackage api.objects
 *
 */
class KontorolYouTubeApiCaptionDistributionInfo extends KontorolObject{

	/**
	 * @var string
	 */
	public $language; 
	
	/**
	 * @var string
	 */
	public $label; 
	
	/**
	 * @var string
	 */
	public $filePath;

	/**
	 * @var string
	 */
	public $remoteId;
	
	/**
	 * @var KontorolYouTubeApiDistributionCaptionAction
	 */
	public $action;	
	
	/**
	 * @var string
	 */
	public $version;
	
	/**
	 * @var string
	 */
	public $assetId;
		
}

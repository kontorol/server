<?php
/**
 * @package plugins.youtubeApiDistribution
 * @subpackage api.objects
 */
class KontorolYouTubeApiCaptionDistributionInfoArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolYouTubeApiCaptionDistributionInfoArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
			$nObj = new KontorolYouTubeApiCaptionDistributionInfo();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolYouTubeApiCaptionDistributionInfo");
	}
}

<?php
/**
 * @package plugins.ideticDistribution
 * @subpackage api.enum
 */
class KontorolIdeticDistributionField extends KontorolStringEnum implements IdeticDistributionField
{
	// see IdeticDistributionField interface
}

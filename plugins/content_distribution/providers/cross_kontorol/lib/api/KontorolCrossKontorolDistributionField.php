<?php
/**
 * @package plugins.crossKontorolDistribution
 * @subpackage api.enum
 */
class KontorolCrossKontorolDistributionField extends KontorolStringEnum implements CrossKontorolDistributionField
{
	// see CrossKontorolDistributionField interface
}

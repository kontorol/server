<?php
/**
 * @package plugins.crossKontorolDistribution
 * @subpackage lib
 */
class CrossKontorolDistributionProviderType implements IKontorolPluginEnum, DistributionProviderType
{
	const CROSS_KONTOROL = 'CROSS_KONTOROL';
	
	public static function getAdditionalValues()
	{
		return array(
			'CROSS_KONTOROL' => self::CROSS_KONTOROL,
		);
	}
	
	/**
	 * @return array
	 */
	public static function getAdditionalDescriptions()
	{
		return array();
	}
}

<?php
/**
 * @package plugins.crossKontorolDistribution
 * @subpackage lib.batch
 */
class CrossKontorolEntryObjectsContainer
{
    /**
     * @var KontorolBaseEntry
     */
    public $entry;
        
    /**
     * @var array<KontorolMetadata>
     */
    public $metadataObjects;
    
    /**
     * @var array<KontorolFlavorAsset>
     */
    public $flavorAssets;
    
    /**
     * @var array<KontorolContentResource>
     */
    public $flavorAssetsContent;
    
    /**
     * @var array<KontorolThumbAsset>
     */
    public $thumbAssets;
    
    /**
     * @var array<KontorolContentResource>
     */
    public $thumbAssetsContent;

    /**
     * @var array<KontorolTimedThumbAsset>
     */
    public $timedThumbAssets;

    /**
     * @var array<KontorolCaptionAsset>
     */
    public $captionAssets;
    
    /**
     * @var array<KontorolContentResource>
     */
    public $captionAssetsContent;
    
    /**
     * @var array<KontorolCuePoint>
     */
    public $cuePoints;

    /**
     * @var array<KontorolThumbCuePoint>
     */
    public $thumbCuePoints;

    /**
     * Initialize all member variables
     */
    public function __construct()
    {
        $this->entry = null;
        $this->metadataObjects = array();
        $this->flavorAssets = array();
        $this->flavorAssetsContent = array();
        $this->thumbAssets = array();
        $this->timedThumbAssets = array();		
        $this->thumbAssetsContent = array();
        $this->captionAssets = array();
        $this->captionAssetsContent = array();
        $this->cuePoints = array();
        $this->thumbCuePoints = array();
    }
}

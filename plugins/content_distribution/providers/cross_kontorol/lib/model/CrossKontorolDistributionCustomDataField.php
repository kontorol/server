<?php
/**
 * @package plugins.crossKontorolDistribution
 * @subpackage lib
 */

class CrossKontorolDistributionCustomDataField
{
    const DISTRIBUTED_FLAVOR_ASSETS = 'CrossKontorolDistribution.distributedFlavorAssets';
    const DISTRIBUTED_THUMB_ASSETS = 'CrossKontorolDistribution.distributedThumbAssets';
    const DISTRIBUTED_METADATA = 'CrossKontorolDistribution.distributedMetadata';
    const DISTRIBUTED_CAPTION_ASSETS = 'CrossKontorolDistribution.distributedCaptionAssets';
    const DISTRIBUTED_CUE_POINTS = 'CrossKontorolDistribution.distributedCuePoints';
    const DISTRIBUTED_THUMB_CUE_POINTS = 'CrossKontorolDistribution.distributedThumbCuePoints';
    const DISTRIBUTED_TIMED_THUMB_ASSETS = 'CrossKontorolDistribution.distributedTimedThumbAssets';
}

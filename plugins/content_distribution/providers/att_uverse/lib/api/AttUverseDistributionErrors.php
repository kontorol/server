<?php
/**
 * @package plugins.attUverseDistribution
 * @subpackage api
 */
class AttUverseDistributionErrors extends KontorolErrors
{
	const INVALID_FEED_URL = "INVALID_FEED_URL;;Invalid feed URL";
}

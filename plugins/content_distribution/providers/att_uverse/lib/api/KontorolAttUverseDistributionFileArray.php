<?php
/**
 * @package plugins.attUverseDistribution
 * @subpackage api.objects
 */
class KontorolAttUverseDistributionFileArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolAttUverseDistributionFileArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
			$nObj = new KontorolAttUverseDistributionFile();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}

		return $newArr;
	}

	public function __construct()
	{
		parent::__construct("KontorolAttUverseDistributionFile");
	}
}

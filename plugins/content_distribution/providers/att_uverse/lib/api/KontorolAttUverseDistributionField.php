<?php
/**
 * @package plugins.attUverseDistribution
 * @subpackage api.enum
 */
class KontorolAttUverseDistributionField extends KontorolStringEnum implements AttUverseDistributionField
{
	// see AttUverseDistributionField interface
}

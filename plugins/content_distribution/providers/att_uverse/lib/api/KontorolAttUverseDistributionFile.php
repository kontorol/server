<?php
/**
 * @package plugins.attUverseDistribution
 * @subpackage api.objects
 */
class KontorolAttUverseDistributionFile extends KontorolObject
{
	
	/**
	 * @var string
	 */
	public $remoteFilename;
	
	/**
	 * @var string
	 */
	public $localFilePath;
	
	/**
	 * @var KontorolAssetType
	 */
	public $assetType;
	
	/**
	 * @var string
	 */
	public $assetId;

}

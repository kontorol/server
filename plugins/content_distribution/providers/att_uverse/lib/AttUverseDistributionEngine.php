<?php
/**
 * @package plugins.attUverseDistribution
 * @subpackage lib
 */
class AttUverseDistributionEngine extends DistributionEngine implements 
	IDistributionEngineUpdate,
	IDistributionEngineSubmit	
{
	
	const FEED_TEMPLATE = 'feed_template.xml';

	/* (non-PHPdoc)
	 * @see IDistributionEngineSubmit::submit()
	 */
	public function submit(KontorolDistributionSubmitJobData $data)
	{
		if(!$data->distributionProfile || !($data->distributionProfile instanceof KontorolAttUverseDistributionProfile))
			KontorolLog::err("Distribution profile must be of type KontorolAttUverseDistributionProfile");
	
		if(!$data->providerData || !($data->providerData instanceof KontorolAttUverseDistributionJobProviderData))
			KontorolLog::err("Provider data must be of type KontorolAttUverseDistributionJobProviderData");
		
		$this->handleSubmit($data, $data->distributionProfile, $data->providerData);
		
		return true;
	}

	/* (non-PHPdoc)
	 * @see IDistributionEngineUpdate::update()
	 */
	public function update(KontorolDistributionUpdateJobData $data)
	{
		if(!$data->distributionProfile || !($data->distributionProfile instanceof KontorolAttUverseDistributionProfile))
			KontorolLog::err("Distribution profile must be of type KontorolAttUverseDistributionProfile");
	
		if(!$data->providerData || !($data->providerData instanceof KontorolUverseDistributionJobProviderData))
			KontorolLog::err("Provider data must be of type KontorolUverseDistributionJobProviderData");
		
		$this->handleSubmit($data, $data->distributionProfile, $data->providerData);
		
		return true;
	}
	
	/**
	 * @param KontorolDistributionJobData $data
	 * @param KontorolAttUverseDistributionProfile $distributionProfile
	 * @param KontorolAttUverseDistributionJobProviderData $providerData
	 */
	protected function handleSubmit(KontorolDistributionJobData $data, KontorolAttUverseDistributionProfile $distributionProfile, KontorolAttUverseDistributionJobProviderData $providerData)
	{
		/* @var $entryDistribution EntryDistribution */
		$entryDistribution = $data->entryDistribution;	

		$remoteId = $entryDistribution->entryId;
		$data->remoteId = $remoteId;
							
		$ftpManager = $this->getFTPManager($distributionProfile);
		if(!$ftpManager)
			throw new Exception("FTP manager not loaded");		
			
		//upload video to FTP
		$remoteAssetFileUrls = array();
		$remoteThumbnailFileUrls = array();
		$remoteCaptionFileUrls = array();
		/* @var $file KontorolAttUverseDistributionFile */
		foreach ($providerData->filesForDistribution as $file){
			$ftpPath = $distributionProfile->ftpPath;
			$destFilePath = $ftpPath ?  $ftpPath.DIRECTORY_SEPARATOR.$file->remoteFilename: $file->remoteFilename;	
			$this->uploadAssetsFiles($ftpManager, $destFilePath, $file->localFilePath);
			if ($file->assetType == KontorolAssetType::FLAVOR)
				$remoteAssetFileUrls[$file->assetId] = 'ftp://'.$distributionProfile->ftpHost.'/'.$destFilePath;
			if ( $file->assetType == KontorolAssetType::THUMBNAIL)
				$remoteThumbnailFileUrls[$file->assetId] = 'ftp://'.$distributionProfile->ftpHost.'/'.$destFilePath;
			if ( ($file->assetType == KontorolAssetType::ATTACHMENT) ||($file->assetType == KontorolAssetType::CAPTION))
				$remoteCaptionFileUrls[$file->assetId] = 'ftp://'.$distributionProfile->ftpHost.'/'.$destFilePath;
		}
		
		//save flavor assets on provider data to use in the service				
		$providerData->remoteAssetFileUrls = serialize($remoteAssetFileUrls);
		//save thumnail assets on provider data to use in the service
		$providerData->remoteThumbnailFileUrls = serialize($remoteThumbnailFileUrls);
		//save caption assets on provider data to use in the service
		$providerData->remoteCaptionFileUrls = serialize($remoteCaptionFileUrls);
		

	}	
	
	/**
	 * 
	 * @param KontorolAttUverseDistributionProfile $distributionProfile
	 * @return ftpMgr
	 */
	protected function getFTPManager(KontorolAttUverseDistributionProfile $distributionProfile)
	{
		$host = $distributionProfile->ftpHost;
		$login = $distributionProfile->ftpUsername;
		$password = $distributionProfile->ftpPassword;
		$engineOptions = isset(KBatchBase::$taskConfig->engineOptions) ? KBatchBase::$taskConfig->engineOptions->toArray() : array();
		$ftpManager = kFileTransferMgr::getInstance(kFileTransferMgrType::FTP, $engineOptions);
		$ftpManager->login($host, $login, $password);
		return $ftpManager;
	}
	
	
	protected function uploadAssetsFiles($ftpManager, $destFileName, $filePath)
	{									
		if ($ftpManager->fileExists($destFileName))
		{
			KontorolLog::err('The file ['.$destFileName.'] already exists at the FTP');
		}
		else	
		{					
			$ftpManager->putFile($destFileName, $filePath, true);
		}
	}
	

}

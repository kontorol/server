<?php
/**
 * @package plugins.youTubeDistribution
 * @subpackage api.enum
 */
class KontorolYouTubeDistributionField extends KontorolStringEnum implements YouTubeDistributionField
{
	// see YouTubeDistributionField interface
}

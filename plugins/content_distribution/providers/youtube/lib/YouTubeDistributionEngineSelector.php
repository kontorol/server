<?php
/**
 * @package plugins.youTubeDistribution
 * @subpackage lib
 */
class YouTubeDistributionEngineSelector extends DistributionEngine implements
	IDistributionEngineUpdate,
	IDistributionEngineSubmit,
	IDistributionEngineReport,
	IDistributionEngineDelete,
	IDistributionEngineCloseUpdate,
	IDistributionEngineCloseSubmit,
	IDistributionEngineCloseDelete
{
	/* (non-PHPdoc)
	 * @see IDistributionEngineSubmit::submit()
	 */
	public function submit(KontorolDistributionSubmitJobData $data)
	{
		$engine = $this->getEngineByProfile($data);
		return $engine->submit($data);
	}

	/* (non-PHPdoc)
	 * @see IDistributionEngineCloseSubmit::closeSubmit()
	 */
	public function closeSubmit(KontorolDistributionSubmitJobData $data)
	{
		$engine = $this->getEngineByProfile($data);
		return $engine->closeSubmit($data);
	}

	/* (non-PHPdoc)
	 * @see IDistributionEngineDelete::delete()
	 */
	public function delete(KontorolDistributionDeleteJobData $data)
	{
		$engine = $this->getEngineByProfile($data);
		return $engine->delete($data);
	}

	/* (non-PHPdoc)
	 * @see IDistributionEngineCloseDelete::closeDelete()
	 */
	public function closeDelete(KontorolDistributionDeleteJobData $data)
	{
		$engine = $this->getEngineByProfile($data);
		return $engine->closeDelete($data);
	}

	/* (non-PHPdoc)
	 * @see IDistributionEngineUpdate::update()
	 */
	public function update(KontorolDistributionUpdateJobData $data)
	{
		$engine = $this->getEngineByProfile($data);
		return $engine->update($data);
	}

	/* (non-PHPdoc)
	 * @see IDistributionEngineCloseUpdate::closeUpdate()
	 */
	public function closeUpdate(KontorolDistributionUpdateJobData $data)
	{
		$engine = $this->getEngineByProfile($data);
		return $engine->closeUpdate($data);
	}

	/* (non-PHPdoc)
	 * @see IDistributionEngineReport::fetchReport()
	 */
	public function fetchReport(KontorolDistributionFetchReportJobData $data)
	{
		$engine = $this->getEngineByProfile($data);
		return $engine->fetchReport($data);
	}

	protected function getEngineByProfile(KontorolDistributionJobData $data)
	{
		if (!$data->distributionProfile instanceof KontorolYouTubeDistributionProfile)
			throw new Exception('Distribution profile is not of type KontorolYouTubeDistributionProfile for entry distribution #'.$data->entryDistributionId);

		switch ( $data->distributionProfile->feedSpecVersion )
		{
			case KontorolYouTubeDistributionFeedSpecVersion::VERSION_1:
			{
				$engine = new YouTubeDistributionLegacyEngine();
				break;
			}
			case KontorolYouTubeDistributionFeedSpecVersion::VERSION_2:
			{
				$engine = new YouTubeDistributionRightsFeedEngine();
				break;
			}
			case KontorolYouTubeDistributionFeedSpecVersion::VERSION_3:
			{
				$engine = new YouTubeDistributionCsvEngine();
				break;
			}
			default:
				throw new Exception('Distribution profile feedSpecVersion does not match existing versions');
		}

		if (KBatchBase::$taskConfig)
			$engine->configure();
		$engine->setClient();

		return $engine;
	}
}

<?php
/**
 * @package plugins.yahooDistribution
 * @subpackage api.enum
 */
class KontorolYahooDistributionProcessFeedActionStatus extends KontorolEnum implements YahooDistributionProcessFeedStatus
{
	// see YahooDistributionProcessFeedStatus interface
}

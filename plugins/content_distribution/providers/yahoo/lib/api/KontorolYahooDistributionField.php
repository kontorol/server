<?php
/**
 * @package plugins.yahooDistribution
 * @subpackage api.enum
 */
class KontorolYahooDistributionField extends KontorolStringEnum implements YahooDistributionField
{
	// see YahooDistributionField interface
}

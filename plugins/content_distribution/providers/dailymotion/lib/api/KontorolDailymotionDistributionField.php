<?php
/**
 * @package plugins.dailymotionDistribution
 * @subpackage api.enum
 */
class KontorolDailymotionDistributionField extends KontorolStringEnum implements DailymotionDistributionField
{
	// see DailymotionDistributionField interface
}

<?php
/**
 * @package plugins.dailymotionDistribution
 * @subpackage api.enum
 */
class KontorolDailymotionGeoBlockingMapping extends KontorolEnum
{
	CONST DISABLED = 0;
	CONST ACCESS_CONTROL = 1;
	CONST METADATA = 2;
}

<?php
/**
 * @package plugins.dailymotionDistribution
 * @subpackage lib
 */
class KontorolDailymotionDistributionCaptionFormat extends KontorolEnum
{
	const SRT = 1;
	const STL = 2;
	const TT = 3;

}

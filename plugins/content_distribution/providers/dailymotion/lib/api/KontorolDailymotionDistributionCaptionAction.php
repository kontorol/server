<?php
/**
 * @package plugins.dailymotionDistribution
 * @subpackage lib
 */
class KontorolDailymotionDistributionCaptionAction extends KontorolEnum
{
	const UPDATE_ACTION = 1;
	const SUBMIT_ACTION = 2;
	const DELETE_ACTION = 3;

}

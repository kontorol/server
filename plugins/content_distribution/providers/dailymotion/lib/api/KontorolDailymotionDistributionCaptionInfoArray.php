<?php
/**
 * @package plugins.dailymotionDistribution
 * @subpackage api.objects
 */
class KontorolDailymotionDistributionCaptionInfoArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolDailymotionDistributionCaptionInfoArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
			$nObj = new KontorolDailymotionDistributionCaptionInfo();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolDailymotionDistributionCaptionInfo");
	}
}

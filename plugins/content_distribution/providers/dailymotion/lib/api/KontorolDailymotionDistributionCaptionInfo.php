<?php
/**
 * @package plugins.dailymotionDistribution
 * @subpackage api.objects
 *
 */
class KontorolDailymotionDistributionCaptionInfo extends KontorolObject{

	/**
	 * @var string
	 */
	public $language; 
	
	/**
	 * @var string
	 */
	public $filePath;
	
	/**
	 * @var string
	 */
	public $remoteId;
	
	/**
	 * @var KontorolDailymotionDistributionCaptionAction
	 */
	public $action;	
	
	/**
	 * @var string
	 */
	public $version;
	
	/**
	 * @var string
	 */
	public $assetId;
	
	/**
	 * @var KontorolDailymotionDistributionCaptionFormat
	 */
	public $format;
		
}

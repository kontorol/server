<?php
/**
 * @package plugins.metroPcsDistribution
 * @subpackage api.enum
 */
class KontorolMetroPcsDistributionField extends KontorolStringEnum implements MetroPcsDistributionField
{
	// see MetroPcsDistributionField interface
}

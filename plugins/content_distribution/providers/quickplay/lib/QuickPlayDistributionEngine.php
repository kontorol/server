<?php
/**
 * @package plugins.quickPlayDistribution
 * @subpackage lib
 */
class QuickPlayDistributionEngine extends DistributionEngine implements 
	IDistributionEngineSubmit,
	IDistributionEngineUpdate
{
	/* (non-PHPdoc)
	 * @see IDistributionEngineSubmit::submit()
	 */
	public function submit(KontorolDistributionSubmitJobData $data)
	{
		$this->validateJobDataObjectTypes($data);
		
		$this->handleSubmit($data, $data->distributionProfile, $data->providerData);
		
		return true;
	}

	/**
	 * (non-PHPdoc)
	 * @see IDistributionEngineUpdate::update()
	 */
	public function update(KontorolDistributionUpdateJobData $data)
	{
		$this->validateJobDataObjectTypes($data);
		
		$this->handleSubmit($data, $data->distributionProfile, $data->providerData);
		
		return true;
	}
	
	/**
	 * @param KontorolDistributionJobData $data
	 * @throws Exception
	 */
	protected function validateJobDataObjectTypes(KontorolDistributionJobData $data)
	{
		if(!$data->distributionProfile || !($data->distributionProfile instanceof KontorolQuickPlayDistributionProfile))
			throw new Exception("Distribution profile must be of type KontorolQuickPlayDistributionProfile");
	
		if(!$data->providerData || !($data->providerData instanceof KontorolQuickPlayDistributionJobProviderData))
			throw new Exception("Provider data must be of type KontorolQuickPlayDistributionJobProviderData");
	}
	
	/**
	 * @param string $path
	 * @param KontorolDistributionJobData $data
	 * @param KontorolVerizonDistributionProfile $distributionProfile
	 * @param KontorolVerizonDistributionJobProviderData $providerData
	 */
	public function handleSubmit(KontorolDistributionJobData $data, KontorolQuickPlayDistributionProfile $distributionProfile, KontorolQuickPlayDistributionJobProviderData $providerData)
	{
		$fileName = $data->entryDistribution->entryId . '_' . date('Y-m-d_H-i-s') . '.xml';
		KontorolLog::info('Sending file '. $fileName);
		
		$sftpManager = $this->getSFTPManager($distributionProfile);
		
		// upload the thumbnails
		foreach($providerData->thumbnailFilePaths as $thumbnailFilePath)
		{
			/* @var $thumbnailFilePath KontorolString */
			if (!kFile::checkFileExists($thumbnailFilePath->value))
				throw new KontorolDistributionException('Thumbnail file path ['.$thumbnailFilePath.'] not found, assuming it wasn\'t synced and the job will retry');

			$thumbnailUploadPath = '/'.$distributionProfile->sftpBasePath.'/'.pathinfo($thumbnailFilePath->value, PATHINFO_BASENAME);
			if ($sftpManager->fileExists($thumbnailUploadPath))
				KontorolLog::info('File "'.$thumbnailUploadPath.'" already exists, skip it');
			else
				$sftpManager->putFile($thumbnailUploadPath, $thumbnailFilePath->value);
		}
		
		// upload the video files
		foreach($providerData->videoFilePaths as $videoFilePath)
		{
			/* @var $videoFilePath KontorolString */
			if (!kFile::checkFileExists($videoFilePath->value))
				throw new KontorolDistributionException('Video file path ['.$videoFilePath.'] not found, assuming it wasn\'t synced and the job will retry');

			$videoUploadPath = '/'.$distributionProfile->sftpBasePath.'/'.pathinfo($videoFilePath->value, PATHINFO_BASENAME);
			if ($sftpManager->fileExists($videoUploadPath))
				KontorolLog::info('File "'.$videoUploadPath.'" already exists, skip it');
			else
				$sftpManager->putFile($videoUploadPath, $videoFilePath->value);
		}

		$tmpfile = tempnam(sys_get_temp_dir(), time());
		file_put_contents($tmpfile, $providerData->xml);
		// upload the metadata file
		$res = $sftpManager->putFile('/'.$distributionProfile->sftpBasePath.'/'.$fileName, $tmpfile);
		unlink($tmpfile);
				
		if ($res === false)
			throw new Exception('Failed to upload metadata file to sftp');
			
		$data->remoteId = $fileName;
		$data->sentData = $providerData->xml;
	}
	
	/**
	 * 
	 * @param KontorolQuickPlayDistributionProfile $distributionProfile
	 * @return sftpMgr
	 */
	protected function getSFTPManager(KontorolQuickPlayDistributionProfile $distributionProfile)
	{
		$host = $distributionProfile->sftpHost;
		$login = $distributionProfile->sftpLogin;
		$pass = $distributionProfile->sftpPass;
		$engineOptions = isset(KBatchBase::$taskConfig->engineOptions) ? KBatchBase::$taskConfig->engineOptions->toArray() : array();
		$sftpManager = kFileTransferMgr::getInstance(kFileTransferMgrType::SFTP, $engineOptions);
		$sftpManager->login($host, $login, $pass);
		return $sftpManager;
	}
}

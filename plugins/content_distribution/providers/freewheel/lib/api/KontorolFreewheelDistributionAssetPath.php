<?php
/**
 * @package plugins.freewheelDistribution
 * @subpackage api.objects
 */
class KontorolFreewheelDistributionAssetPath extends KontorolDistributionJobProviderData
{
	/**
	 * @var string
	 */
	public $path;
}

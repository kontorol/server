<?php
/**
 * @package plugins.contentDistribution 
 * @subpackage Scheduler.Distribute
 */
interface IDistributionEngineDisable extends IDistributionEngineUpdate
{
	/**
	 * disables the package.
	 * @param KontorolDistributionDisableJobData $data
	 * @return bool true if finished, false if will be finished asynchronously
	 */
	public function disable(KontorolDistributionDisableJobData $data);
}

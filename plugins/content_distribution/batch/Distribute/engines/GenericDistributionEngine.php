<?php
/**
 * @package plugins.contentDistribution 
 * @subpackage Scheduler.Distribute
 */
class GenericDistributionEngine extends DistributionEngine implements 
	IDistributionEngineUpdate,
	IDistributionEngineSubmit,
	IDistributionEngineReport,
	IDistributionEngineDelete,
	IDistributionEngineCloseUpdate,
	IDistributionEngineCloseSubmit,
	IDistributionEngineCloseReport,
	IDistributionEngineCloseDelete
{
	protected $tempXmlPath;
	
	/* (non-PHPdoc)
	 * @see DistributionEngine::configure()
	 */
	public function configure()
	{
		parent::configure();
		
		if(KBatchBase::$taskConfig->params->tempXmlPath)
		{
			$this->tempXmlPath = KBatchBase::$taskConfig->params->tempXmlPath;
			if(!is_dir($this->tempXmlPath))
				kFile::fullMkfileDir($this->tempXmlPath, 0777, true);
		}
		else
		{
			KontorolLog::err("params.tempXmlPath configuration not supplied");
			$this->tempXmlPath = sys_get_temp_dir();
		}
	}

	/* (non-PHPdoc)
	 * @see IDistributionEngineSubmit::submit()
	 */
	public function submit(KontorolDistributionSubmitJobData $data)
	{
		if(!$data->distributionProfile || !($data->distributionProfile instanceof KontorolGenericDistributionProfile))
			KontorolLog::err("Distribution profile must be of type KontorolGenericDistributionProfile");
	
		if(!$data->providerData || !($data->providerData instanceof KontorolGenericDistributionJobProviderData))
			KontorolLog::err("Provider data must be of type KontorolGenericDistributionJobProviderData");
		
		return $this->handleAction($data, $data->distributionProfile, $data->distributionProfile->submitAction, $data->providerData);
	}

	/**
	 * @param KontorolDistributionJobData $data
	 * @param KontorolGenericDistributionProfile $distributionProfile
	 * @param KontorolGenericDistributionJobProviderData $providerData
	 * @throws Exception
	 * @throws kFileTransferMgrException
	 * @return boolean true if finished, false if will be finished asynchronously
	 */
	protected function handleAction(KontorolDistributionJobData $data, KontorolGenericDistributionProfile $distributionProfile, KontorolGenericDistributionProfileAction $distributionProfileAction, KontorolGenericDistributionJobProviderData $providerData)
	{
		if(!$providerData->xml)
			throw new Exception("XML data not supplied");
		
		$fileName = uniqid() . '.xml';
		$srcFile = $this->tempXmlPath . '/' . $fileName;
		$destFile = $distributionProfileAction->serverPath;
			
		if($distributionProfileAction->protocol != KontorolDistributionProtocol::HTTP && $distributionProfileAction->protocol != KontorolDistributionProtocol::HTTPS)
			$destFile .= '/' . $fileName;
			
		$destFile = str_replace('{REMOTE_ID}', $data->remoteId, $destFile);
		
		kFile::filePutContents($srcFile, $providerData->xml);
		KontorolLog::log("XML written to file [$srcFile]");
		
		$engineOptions = isset(KBatchBase::$taskConfig->engineOptions) ? KBatchBase::$taskConfig->engineOptions->toArray() : array();
		$engineOptions['passiveMode'] = $distributionProfileAction->ftpPassiveMode;
		$engineOptions['fieldName'] = $distributionProfileAction->httpFieldName;
		$engineOptions['fileName'] = $distributionProfileAction->httpFileName;
		$fileTransferMgr = kFileTransferMgr::getInstance($distributionProfileAction->protocol, $engineOptions);
		if(!$fileTransferMgr)
			throw new Exception("File transfer manager type [$distributionProfileAction->protocol] not supported");
			
		$fileTransferMgr->login($distributionProfileAction->serverUrl, $distributionProfileAction->username, $distributionProfileAction->password);
		$fileTransferMgr->putFile($destFile, $srcFile, true);
		$results = $fileTransferMgr->getResults();
		
		if($results && is_string($results))
		{
			$data->results = $results;
			$parsedValues = $this->parseResults($results, $providerData->resultParserType, $providerData->resultParseData);
			if(count($parsedValues))
				list($data->remoteId) = $parsedValues;
		}
		$data->sentData = $providerData->xml;
		
		return true;
	}

	/**
	 * @param string $results
	 * @param KontorolGenericDistributionProviderParser $resultParserType
	 * @param string $resultParseData
	 * @return array of parsed values
	 */
	protected function parseResults($results, $resultParserType, $resultParseData)
	{
		switch($resultParserType)
		{
			case KontorolGenericDistributionProviderParser::XSL;
				$xml = new DOMDocument();
				if(!$xml->loadXML($results))
					return false;
		
				$xsl = new DOMDocument();
				$xsl->loadXML($resultParseData);
				
				$proc = new XSLTProcessor;
				$proc->registerPHPFunctions(kXml::getXslEnabledPhpFunctions());
				$proc->importStyleSheet($xsl);
				
				$data = $proc->transformToDoc($xml);
				if(!$data)
					return false;
					
				return explode(',', $data);
				
			case KontorolGenericDistributionProviderParser::XPATH;
				$xml = new DOMDocument();
				if(!$xml->loadXML($results))
					return false;
		
				$xpath = new DOMXPath($xml);
				$elements = $xpath->query($resultParseData);
				if(is_null($elements))
					return false;
					
				$matches = array();
				foreach ($elements as $element)
					$matches[] = $element->textContent;
					
				return $matches;;
				
			case KontorolGenericDistributionProviderParser::REGEX;
				$matches = array();
				if(!preg_match("/$resultParseData/", $results, $matches))
					return false;
					
				return array_shift($matches);
				
			default;
				return false;
		}
	}

	/* (non-PHPdoc)
	 * @see IDistributionEngineCloseSubmit::closeSubmit()
	 */
	public function closeSubmit(KontorolDistributionSubmitJobData $data)
	{
		// not supported
		return false;
	}

	/* (non-PHPdoc)
	 * @see IDistributionEngineDelete::delete()
	 */
	public function delete(KontorolDistributionDeleteJobData $data)
	{
		if(!$data->distributionProfile || !($data->distributionProfile instanceof KontorolGenericDistributionProfile))
			KontorolLog::err("Distribution profile must be of type KontorolGenericDistributionProfile");
	
		if(!$data->providerData || !($data->providerData instanceof KontorolGenericDistributionJobProviderData))
			KontorolLog::err("Provider data must be of type KontorolGenericDistributionJobProviderData");
		
		return $this->handleAction($data, $data->distributionProfile, $data->distributionProfile->deleteAction, $data->providerData);
	}

	/* (non-PHPdoc)
	 * @see IDistributionEngineCloseDelete::closeDelete()
	 */
	public function closeDelete(KontorolDistributionDeleteJobData $data)
	{
		// not supported
		return false;
	}

	/* (non-PHPdoc)
	 * @see IDistributionEngineCloseReport::closeReport()
	 */
	public function closeReport(KontorolDistributionFetchReportJobData $data)
	{
		// not supported
		return false;
	}

	/* (non-PHPdoc)
	 * @see IDistributionEngineCloseUpdate::closeUpdate()
	 */
	public function closeUpdate(KontorolDistributionUpdateJobData $data)
	{
		// not supported
		return false;
	}

	/* (non-PHPdoc)
	 * @see IDistributionEngineReport::fetchReport()
	 */
	public function fetchReport(KontorolDistributionFetchReportJobData $data)
	{
		if(!$data->distributionProfile || !($data->distributionProfile instanceof KontorolGenericDistributionProfile))
			KontorolLog::err("Distribution profile must be of type KontorolGenericDistributionProfile");
	
		if(!$data->providerData || !($data->providerData instanceof KontorolGenericDistributionJobProviderData))
			KontorolLog::err("Provider data must be of type KontorolGenericDistributionJobProviderData");
		
		return $this->handleFetchReport($data, $data->distributionProfile, $data->distributionProfile->report, $data->providerData);
	}


	/**
	 * @param KontorolDistributionJobData $data
	 * @param KontorolGenericDistributionProfile $distributionProfile
	 * @param KontorolGenericDistributionJobProviderData $providerData
	 * @throws Exception
	 * @throws kFileTransferMgrException
	 * @return boolean true if finished, false if will be finished asynchronously
	 */
	protected function handleFetchReport(KontorolDistributionFetchReportJobData $data, KontorolGenericDistributionProfile $distributionProfile, KontorolGenericDistributionProfileAction $distributionProfileAction, KontorolGenericDistributionJobProviderData $providerData)
	{
		$srcFile = str_replace('{REMOTE_ID}', $data->remoteId, $distributionProfileAction->serverPath);
		
		KontorolLog::log("Fetch report from url [$srcFile]");
		$results = kFile::getFileContent($srcFile);
	
		if($results && is_string($results))
		{
			$data->results = $results;
			$parsedValues = $this->parseResults($results, $providerData->resultParserType, $providerData->resultParseData);
			if(count($parsedValues))
				list($data->plays, $data->views) = $parsedValues;
		}
		
		return true;
	}

	/* (non-PHPdoc)
	 * @see IDistributionEngineUpdate::update()
	 */
	public function update(KontorolDistributionUpdateJobData $data)
	{
		if(!$data->distributionProfile || !($data->distributionProfile instanceof KontorolGenericDistributionProfile))
			KontorolLog::err("Distribution profile must be of type KontorolGenericDistributionProfile");
	
		if(!$data->providerData || !($data->providerData instanceof KontorolGenericDistributionJobProviderData))
			KontorolLog::err("Provider data must be of type KontorolGenericDistributionJobProviderData");
		
		return $this->handleAction($data, $data->distributionProfile, $data->distributionProfile->updateAction, $data->providerData);
	}

}

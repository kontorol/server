<?php
/**
 * @package plugins.contentDistribution 
 * @subpackage Scheduler.Distribute
 */
abstract class DistributionEngine implements IDistributionEngine
{	
	/**
	 * @var int
	 */
	protected $partnerId;
	
	/**
	 * @var string
	 */
	protected $tempDirectory = null;
	
	/**
	 * @param string $interface
	 * @param KontorolDistributionProviderType $providerType
	 * @param KontorolDistributionJobData $data
	 * @return DistributionEngine
	 */
	public static function getEngine($interface, $providerType, KontorolDistributionJobData $data)
	{
		$engine = null;
		if($providerType == KontorolDistributionProviderType::GENERIC)
		{
			$engine = new GenericDistributionEngine();
		}
		else
		{
			$engine = KontorolPluginManager::loadObject($interface, $providerType);
		}
		
		if($engine)
		{
			$engine->setClient();
			$engine->configure($data);
		}
		
		return $engine;
	}
	
	/* (non-PHPdoc)
	 * @see IDistributionEngine::setClient()
	 */
	public function setClient()
	{
		$this->partnerId = KBatchBase::$kClient->getPartnerId();
	}
	
	/* (non-PHPdoc)
	 * @see IDistributionEngine::setClient()
	 */
	public function configure()
	{
		$this->tempDirectory = isset(KBatchBase::$taskConfig->params->tempDirectoryPath) ? KBatchBase::$taskConfig->params->tempDirectoryPath : sys_get_temp_dir();
		if (!is_dir($this->tempDirectory)) 
			kFile::fullMkfileDir($this->tempDirectory, 0700, true);
	}

	/**
	 * @param string $entryId
	 * @return KontorolMediaEntry
	 */
	protected function getEntry($partnerId, $entryId)
	{
		KBatchBase::impersonate($partnerId);
		$entry = KBatchBase::$kClient->baseEntry->get($entryId);
		KBatchBase::unimpersonate();
		
		return $entry;
	}

	/**
	 * @param string $flavorAssetIds comma seperated
	 * @return array<KontorolFlavorAsset>
	 */
	protected function getFlavorAssets($partnerId, $flavorAssetIds)
	{
		$filter = new KontorolAssetFilter();
		$filter->idIn = $flavorAssetIds;
		
		try
		{
			KBatchBase::impersonate($entryDistribution->partnerId);
			$flavorAssetsList = KBatchBase::$kClient->flavorAsset->listAction($filter);
			KBatchBase::unimpersonate();
		}
		catch (Exception $e)
		{
			KBatchBase::unimpersonate();
			throw $e;
		}
		
		return $flavorAssetsList->objects;
	}

	/**
	 * @param string $thumbAssetIds comma seperated
	 * @return array<KontorolThumbAsset>
	 */
	protected function getThumbAssets($partnerId, $thumbAssetIds, $entryId)
	{
		$filter = new KontorolAssetFilter();
		$filter->idIn = $thumbAssetIds;
		$filter->entryIdEqual = $entryId;
		
		try
		{
			KBatchBase::impersonate($partnerId);
			$thumbAssetsList = KBatchBase::$kClient->thumbAsset->listAction($filter);
			KBatchBase::unimpersonate();
		}
		catch (Exception $e)
		{
			KBatchBase::unimpersonate();
			throw $e;
		}
		
		return $thumbAssetsList->objects;
	}

	/**
	 * @param string $assetId
	 * @return string url
	 */
	protected function getAssetUrl($assetId)
	{
		$contentDistributionPlugin = KontorolContentDistributionClientPlugin::get(KBatchBase::$kClient);
		return $contentDistributionPlugin->contentDistributionBatch->getAssetUrl($assetId);
	
//		$domain = $this->kontorolClient->getConfig()->serviceUrl;
//		return "$domain/api_v3/service/thumbAsset/action/serve/thumbAssetId/$thumbAssetId";
	}

	/**
	 * @param array<KontorolMetadata> $metadataObjects
	 * @param string $field
	 * @return array|string
	 */
	protected function findMetadataValue(array $metadataObjects, $field, $asArray = false)
	{
		$results = array();
		foreach($metadataObjects as $metadata)
		{
			$xml = new DOMDocument();
			$xml->loadXML($metadata->xml);
			$nodes = $xml->getElementsByTagName($field);
			foreach($nodes as $node)
				$results[] = $node->textContent;
		}
		
		if(!$asArray)
		{
			if(!count($results))
				return null;
				
			if(count($results) == 1)
				return reset($results);
		}
			
		return $results;
	}

	/**
	 * @param string $objectId
	 * @param KontorolMetadataObjectType $objectType
	 * @return array<KontorolMetadata>
	 */
	protected function getMetadataObjects($partnerId, $objectId, $objectType = KontorolMetadataObjectType::ENTRY, $metadataProfileId = null)
	{
		if(!class_exists('KontorolMetadata'))
			return null;
			
		KBatchBase::impersonate($partnerId);
		
		$metadataFilter = new KontorolMetadataFilter();
		$metadataFilter->objectIdEqual = $objectId;
		$metadataFilter->metadataObjectTypeEqual = $objectType;
		$metadataFilter->orderBy = KontorolMetadataOrderBy::CREATED_AT_DESC;
		
		if($metadataProfileId)
			$metadataFilter->metadataProfileIdEqual = $metadataProfileId;
		
		$metadataPager = new KontorolFilterPager();
		$metadataPager->pageSize = 1;
		$metadataListResponse = KBatchBase::$kClient->metadata->listAction($metadataFilter, $metadataPager);
		
		KBatchBase::unimpersonate();
		
		if(!$metadataListResponse->totalCount)
			throw new Exception("No metadata objects found");

		return $metadataListResponse->objects;
	}

	protected function getCaptionContent($captionAssetId)
	{
		KontorolLog::info("Retrieve caption assets content for captionAssetId: [$captionAssetId]");
		try
		{
			$captionClientPlugin = KontorolCaptionClientPlugin::get(KBatchBase::$kClient);
			$captionAssetContentUrl= $captionClientPlugin->captionAsset->serve($captionAssetId);
			return KCurlWrapper::getContent($captionAssetContentUrl);
		}
		catch(Exception $e)
		{
			KontorolLog::info("Can't serve caption asset id [$captionAssetId] " . $e->getMessage());
		}
	}

	protected function getAssetFile($assetId, $directory, $fileName = null)
	{
		KontorolLog::info("Retrieve asset content for assetId: [$assetId]");
		try
		{
			$filePath = $directory;
			if ($fileName)
				 $filePath .= '/'.$fileName;
			else
				$filePath .= '/asset_'.$assetId;

			$assetContentUrl = $this->getAssetUrl($assetId);
			$res = KCurlWrapper::getDataFromFile($assetContentUrl, $filePath, null, true);
			if ($res)
				return $filePath;
		}
		catch(Exception $e)
		{
			KontorolLog::info("Can't serve asset id [$assetId] " . $e->getMessage());
		}
		return null;
	}
}

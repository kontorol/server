<?php
/**
 * @package plugins.contentDistribution 
 * @subpackage Scheduler.Distribute
 */
interface IDistributionEngine
{
	/**
	 * @param KSchedularTaskConfig $taskConfig
	 */
	public function configure();
	
	/**
	 * @param KontorolClient $kontorolClient
	 */
	public function setClient();
}

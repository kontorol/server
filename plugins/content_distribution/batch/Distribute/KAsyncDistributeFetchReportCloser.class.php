<?php
/**
 * Distributes kontorol entries to remote destination
 *
 * @package plugins.contentDistribution 
 * @subpackage Scheduler.Distribute
 */
class KAsyncDistributeFetchReportCloser extends KAsyncDistributeCloser
{
	/* (non-PHPdoc)
	 * @see KBatchBase::getType()
	 */
	public static function getType()
	{
		return KontorolBatchJobType::DISTRIBUTION_FETCH_REPORT;
	}
	
	/* (non-PHPdoc)
	 * @see KAsyncDistribute::getDistributionEngine()
	 */
	protected function getDistributionEngine($providerType, KontorolDistributionJobData $data)
	{
		return DistributionEngine::getEngine('IDistributionEngineCloseReport', $providerType, $data);
	}
	
	/* (non-PHPdoc)
	 * @see KAsyncDistribute::execute()
	 */
	protected function execute(KontorolDistributionJobData $data)
	{
		return $this->engine->closeReport($data);
	}
}

<?php
/**
 * Distributes kontorol entries to remote destination
 *
 * @package plugins.contentDistribution 
 * @subpackage Scheduler.Distribute
 */
class KAsyncDistributeUpdate extends KAsyncDistribute
{
	/* (non-PHPdoc)
	 * @see KBatchBase::getType()
	 */
	public static function getType()
	{
		return KontorolBatchJobType::DISTRIBUTION_UPDATE;
	}
	
	/* (non-PHPdoc)
	 * @see KAsyncDistribute::getDistributionEngine()
	 */
	protected function getDistributionEngine($providerType, KontorolDistributionJobData $data)
	{
		return DistributionEngine::getEngine('IDistributionEngineUpdate', $providerType, $data);
	}
	
	/* (non-PHPdoc)
	 * @see KAsyncDistribute::execute()
	 */
	protected function execute(KontorolDistributionJobData $data)
	{
		return $this->engine->update($data);
	}
}

<?php
/**
 * Distributes kontorol entries to remote destination
 *
 * @package plugins.contentDistribution 
 * @subpackage Scheduler.Distribute
 */
abstract class KAsyncDistribute extends KJobHandlerWorker
{
	/**
	 * @var IDistributionEngine
	 */
	protected $engine;
	
	/* (non-PHPdoc)
	 * @see KJobHandlerWorker::exec()
	 */
	protected function exec(KontorolBatchJob $job)
	{
		return $this->distribute($job, $job->data);;
	}
	
	/**
	 * @return DistributionEngine
	 */
	abstract protected function getDistributionEngine($providerType, KontorolDistributionJobData $data);
	
	/**
	 * Throw detailed exceptions for any failure 
	 * @return bool true if job is closed, false for almost done
	 */
	abstract protected function execute(KontorolDistributionJobData $data);
	
	protected function distribute(KontorolBatchJob $job, KontorolDistributionJobData $data)
	{
		try
		{
			$this->engine = $this->getDistributionEngine($job->jobSubType, $data);
			if (!$this->engine)
			{
				KontorolLog::err('Cannot create DistributeEngine of type ['.$job->jobSubType.']');
				$this->closeJob($job, KontorolBatchJobErrorTypes::APP, null, 'Error: Cannot create DistributeEngine of type ['.$job->jobSubType.']', KontorolBatchJobStatus::FAILED);
				return $job;
			}
			$job = $this->updateJob($job, "Engine found [" . get_class($this->engine) . "]", KontorolBatchJobStatus::QUEUED);
						
			$closed = $this->execute($data);
			if($closed)
				return $this->closeJob($job, null, null, null, KontorolBatchJobStatus::FINISHED, $data);
			 			
			return $this->closeJob($job, null, null, null, KontorolBatchJobStatus::ALMOST_DONE, $data);
		}
		catch(KontorolDistributionException $ex)
		{
			KontorolLog::err($ex);
			$job = $this->closeJob($job, KontorolBatchJobErrorTypes::APP, $ex->getCode(), "Error: " . $ex->getMessage(), KontorolBatchJobStatus::RETRY, $job->data);
		}
		catch(Exception $ex)
		{
			KontorolLog::err($ex);
			$job = $this->closeJob($job, KontorolBatchJobErrorTypes::RUNTIME, $ex->getCode(), "Error: " . $ex->getMessage(), KontorolBatchJobStatus::FAILED, $job->data);
		}
		return $job;
	}
}

<?php
/**
 * Distributes kontorol entries to remote destination
 *
 * @package plugins.contentDistribution 
 * @subpackage Scheduler.Distribute
 */
class KAsyncDistributeDisable extends KAsyncDistribute
{
	/* (non-PHPdoc)
	 * @see KBatchBase::getType()
	 */
	public static function getType()
	{
		return KontorolBatchJobType::DISTRIBUTION_DISABLE;
	}
	
	/* (non-PHPdoc)
	 * @see KAsyncDistribute::getDistributionEngine()
	 */
	protected function getDistributionEngine($providerType, KontorolDistributionJobData $data)
	{
		return DistributionEngine::getEngine('IDistributionEngineDisable', $providerType, $data);
	}
	
	/* (non-PHPdoc)
	 * @see KAsyncDistribute::execute()
	 */
	protected function execute(KontorolDistributionJobData $data)
	{
		return $this->engine->disable($data);
	}
}

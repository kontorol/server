<?php

/**
 * @package plugins.scheduledTaskContentDistribution
 * @subpackage lib.objectTaskEngine
 */
class KObjectTaskDistributeEngine extends KObjectTaskEntryEngineBase
{

	/**
	 * @param KontorolBaseEntry $object
	 */
	function processObject($object)
	{
		/** @var KontorolDistributeObjectTask $objectTask */
		$objectTask = $this->getObjectTask();
		if (is_null($objectTask))
			return;

		$entryId = $object->id;
		$distributionProfileId = $objectTask->distributionProfileId;
		if (!$distributionProfileId)
			throw new Exception('Distribution profile id was not configured');

		KontorolLog::info("Trying to distribute entry $entryId with profile $distributionProfileId");

		$client = $this->getClient();
		$contentDistributionPlugin = KontorolContentDistributionClientPlugin::get($client);
		$distributionProfile = $contentDistributionPlugin->distributionProfile->get($distributionProfileId);

		if ($distributionProfile->submitEnabled == KontorolDistributionProfileActionStatus::DISABLED)
			throw new Exception("Submit action for distribution profile $distributionProfileId id disabled");

		$entryDistribution = $this->getEntryDistribution($entryId, $distributionProfileId);
		if ($entryDistribution && $entryDistribution->status == KontorolEntryDistributionStatus::REMOVED)
		{
			KontorolLog::info("Entry distribution is in status REMOVED, deleting it completely");
			$contentDistributionPlugin->entryDistribution->delete($entryDistribution->id);
			$entryDistribution = null;
		}

		if ($entryDistribution)
		{
			KontorolLog::info("Entry distribution already exists with id $entryDistribution->id");
		}
		else
		{
			$entryDistribution = new KontorolEntryDistribution();
			$entryDistribution->distributionProfileId = $distributionProfileId;
			$entryDistribution->entryId = $entryId;
			$entryDistribution = $contentDistributionPlugin->entryDistribution->add($entryDistribution);
		}

		$shouldSubmit = false;
		switch($entryDistribution->status)
		{
			case KontorolEntryDistributionStatus::PENDING:
				$shouldSubmit = true;
				break;
			case KontorolEntryDistributionStatus::QUEUED:
				KontorolLog::info('Entry distribution is already queued');
				break;
			case KontorolEntryDistributionStatus::READY:
				KontorolLog::info('Entry distribution was already submitted');
				break;
			case KontorolEntryDistributionStatus::SUBMITTING:
				KontorolLog::info('Entry distribution is currently being submitted');
				break;
			case KontorolEntryDistributionStatus::UPDATING:
				KontorolLog::info('Entry distribution is currently being updated, so it was submitted already');
				break;
			case KontorolEntryDistributionStatus::DELETING:
				// throwing exception, the task will retry on next execution
				throw new Exception('Entry distribution is currently being deleted and cannot be handled at this stage');
				break;
			case KontorolEntryDistributionStatus::ERROR_SUBMITTING:
			case KontorolEntryDistributionStatus::ERROR_UPDATING:
			case KontorolEntryDistributionStatus::ERROR_DELETING:
				KontorolLog::info('Entry distribution is in error state, trying to resubmit');
				$shouldSubmit = true;
				break;
			case KontorolEntryDistributionStatus::IMPORT_SUBMITTING:
			case KontorolEntryDistributionStatus::IMPORT_UPDATING:
				KontorolLog::info('Entry distribution is waiting for an import job to be finished, do nothing, it will be submitted/updated automatically');
				break;
			default:
				throw new Exception("Entry distribution status $entryDistribution->status is invalid");
		}

		if ($shouldSubmit)
		{
			$contentDistributionPlugin->entryDistribution->submitAdd($entryDistribution->id, true);
		}
	}

	protected function getEntryDistribution($entryId, $distributionProfileId)
	{
		$distributionPlugin = KontorolContentDistributionClientPlugin::get($this->getClient());
		$entryDistributionFilter = new KontorolEntryDistributionFilter();
		$entryDistributionFilter->entryIdEqual = $entryId;
		$entryDistributionFilter->distributionProfileIdEqual = $distributionProfileId;
		$result = $distributionPlugin->entryDistribution->listAction($entryDistributionFilter);
		if (count($result->objects))
			return $result->objects[0];
		else
			return null;
	}
}

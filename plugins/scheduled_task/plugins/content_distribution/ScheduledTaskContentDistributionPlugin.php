<?php
/**
 * Extension plugin for scheduled task plugin to add support for distributing content
 *
 * @package plugins.scheduledTaskEventNotification
 */
class ScheduledTaskContentDistributionPlugin extends KontorolPlugin implements IKontorolPending, IKontorolEnumerator, IKontorolObjectLoader
{
	const PLUGIN_NAME = 'scheduledTaskContentDistribution';
	
	const SCHEDULED_TASK_PLUGIN_NAME = 'scheduledTask';
	
	const CONTENT_DISTRIBUTION_PLUGIN_NAME = 'contentDistribution';
	const CONTENT_DISTRIBUTION_PLUGIN_VERSION_MAJOR = 1;
	const CONTENT_DISTRIBUTION_PLUGIN_VERSION_MINOR = 0;
	const CONTENT_DISTRIBUTION_PLUGIN_VERSION_BUILD = 0;
	
	/* (non-PHPdoc)
	 * @see IKontorolPlugin::getPluginName()
	 */
	public static function getPluginName()
	{
		return self::PLUGIN_NAME;
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolPending::dependsOn()
	 */
	public static function dependsOn()
	{
		$eventNotificationVersion = new KontorolVersion(self::CONTENT_DISTRIBUTION_PLUGIN_VERSION_MAJOR, self::CONTENT_DISTRIBUTION_PLUGIN_VERSION_MINOR, self::CONTENT_DISTRIBUTION_PLUGIN_VERSION_BUILD);
		
		$scheduledTaskDependency = new KontorolDependency(self::SCHEDULED_TASK_PLUGIN_NAME);
		$eventNotificationDependency = new KontorolDependency(self::CONTENT_DISTRIBUTION_PLUGIN_NAME, $eventNotificationVersion);
		
		return array($scheduledTaskDependency, $eventNotificationDependency);
	}
			
	/* (non-PHPdoc)
	 * @see IKontorolEnumerator::getEnums()
	 */
	public static function getEnums($baseEnumName = null)
	{
		if(is_null($baseEnumName))
			return array('DistributeObjectTaskType');
	
		if($baseEnumName == 'ObjectTaskType')
			return array('DistributeObjectTaskType');
			
		return array();
	}

	/* (non-PHPdoc)
	 * @see IKontorolObjectLoader::loadObject()
	 */
	public static function loadObject($baseClass, $enumValue, array $constructorArgs = null)
	{
		if (class_exists('Kontorol_Client_Client'))
			return null;

		if (class_exists('KontorolClient'))
		{
			if ($baseClass == 'KObjectTaskEntryEngineBase' && $enumValue == KontorolObjectTaskType::DISTRIBUTE)
				return new KObjectTaskDistributeEngine();
		}
		else
		{
			$apiValue = self::getApiValue(DistributeObjectTaskType::DISTRIBUTE);
			$distributeObjectTaskCoreValue = kPluginableEnumsManager::apiToCore('ObjectTaskType', $apiValue);
			if($baseClass == 'KontorolObjectTask' && $enumValue == $distributeObjectTaskCoreValue)
				return new KontorolDistributeObjectTask();

			if ($baseClass == 'KObjectTaskEntryEngineBase' && $enumValue == $apiValue)
				return new KObjectTaskDistributeEngine();
		}

		return null;
	}
		
	/* (non-PHPdoc)
	 * @see IKontorolObjectLoader::getObjectClass()
	 */
	public static function getObjectClass($baseClass, $enumValue)
	{
		return null;
	}
	
	/**
	 * @return string external API value of dynamic enum.
	 */
	public static function getApiValue($valueName)
	{
		return self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
	}
}

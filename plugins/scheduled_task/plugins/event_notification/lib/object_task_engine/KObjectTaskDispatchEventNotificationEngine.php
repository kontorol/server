<?php

/**
 * @package plugins.scheduledTaskEventNotification
 * @subpackage lib.objectTaskEngine
 */
class KObjectTaskDispatchEventNotificationEngine extends KObjectTaskEntryEngineBase
{
	/**
	 * @param KontorolBaseEntry $object
	 */
	function processObject($object)
	{
		/** @var KontorolDispatchEventNotificationObjectTask $objectTask */
		$objectTask = $this->getObjectTask();
		if (is_null($objectTask))
			return;

		$client = $this->getClient();
		$templateId = $objectTask->eventNotificationTemplateId;
		$eventNotificationPlugin = KontorolEventNotificationClientPlugin::get($client);
		$scope = new KontorolEventNotificationScope();
		$scope->objectId =$object->id;
		$scope->scopeObjectType = KontorolEventNotificationEventObjectType::ENTRY;
		$eventNotificationPlugin->eventNotificationTemplate->dispatch($templateId, $scope);
	}
}

<?php
if($argc < 3)
{
	echo "Usage: php $argv[0] [adminKs] [serviceUrl] <dryRunMode> <maxEntries>".PHP_EOL;
	die("Not enough parameters" . "\n");
}

require_once(__DIR__ . '/../../../batch/bootstrap.php');

function getMRProfiles($client)
{
	$filter = new KontorolScheduledTaskProfileFilter();
	$pager = new KontorolFilterPager();
	$pager->pageIndex = 1;
	$pager->pageSize = 500;
	$scheduledTaskClient = KontorolScheduledTaskClientPlugin::get($client);
	$result = $scheduledTaskClient->scheduledTaskProfile->listAction($filter, $pager);
	kontorolLog::info("Found count {$result->totalCount} profiles");
	return $result->objects;
}

function filterProfiles($profiles)
{
	$subTasksProfiles = array();
	foreach ($profiles as $profile)
	{
		if(kString::beginsWith($profile->name, 'MR_'))
		{
			$subTasksProfiles[] = $profile;
		}
	}

	return $subTasksProfiles;
}

function getMetadataOnObject($metadataPlugin, $objectId, $metadataProfileId)
{
	$filter = new KontorolMetadataFilter();
	$filter->metadataProfileIdEqual = $metadataProfileId;
	$filter->objectIdEqual = $objectId;
	$result = $metadataPlugin->metadata->listAction($filter, null);
	if ($result->totalCount > 0)
	{
		return $result->objects[0];
	}

	return null;
}

function getClient($serviceUrl, $adminKs)
{
	$config = new KontorolConfiguration();
	$config->clientTag = 'KScheduledTaskRunner';
	$config->serviceUrl = $serviceUrl;
	$client = new KontorolClient($config);
	$client->setKs($adminKs);

	return $client;
}

/**
 * @param $client
 * @param $metadataPlugin
 * @param KontorolScheduledTaskProfile $profile
 * @param bool $dryRunMode
 * @param int $maxEntries
 * @param int $entriesHandledCount
 */
function processEntries($client, $metadataPlugin, $profile, $dryRunMode, $maxEntries, &$entriesHandledCount)
{
	$pager = new KontorolFilterPager();
	$pager->pageIndex = 1;
	$pager->pageSize = 500;
	do
	{
		$result = $client->baseEntry->listAction($profile->objectFilter, $pager);
		$resultCount = count($result->objects);
		kontorolLog::info("Found $resultCount entries for pageIndex {$pager->pageIndex}");
		$entries = $result->objects;
		$metadataProfileId = $profile->objectFilter->advancedSearch->items[0]->metadataProfileId;
		foreach ($entries as $entry)
		{
			$entriesHandledCount += handleEntry($entry, $metadataPlugin, $metadataProfileId, $dryRunMode);
			if ($entriesHandledCount >= $maxEntries)
			{
				kontorolLog::info("Reached max entries limit, {$entriesHandledCount} were handled");
				return;
			}
		}

		$pager->pageIndex++;
	}
	while($resultCount == $pager->pageSize);
}

function handleEntry($entry, $metadataPlugin, $metadataProfileId, $dryRunMode)
{
	$shouldUpdate = 0;
	$metadata = getMetadataOnObject($metadataPlugin, $entry->id, $metadataProfileId);
	$xml_string = ($metadata && $metadata->xml) ? $metadata->xml : null;
	if ($xml_string)
	{
		$xml = simplexml_load_string($xml_string);
		$mrpData = $xml->xpath('/metadata/MRPData');
		$mrpsOnEntry = $xml->xpath('/metadata/MRPsOnEntry');
		$dataRows = count($mrpData);
		if ($dataRows != count($mrpsOnEntry))
		{
			kontorolLog::err("{$entry->id} have bugged MR data");
			return 0;
		}

		$uniqueMrpsOnEntry = array();
		$newXml = new SimpleXMLElement("<metadata/>");
		$newXml->addChild('Status', 'Enabled');
		for ($i = 0; $i < $dataRows; $i++)
		{
			$mrpsOnEntryString = $mrpsOnEntry[$i][0]->__toString();
			if (isset($uniqueMrpsOnEntry[$mrpsOnEntryString]))
			{
				$shouldUpdate = 1;
			}
			else
			{
				$uniqueMrpsOnEntry[$mrpsOnEntryString] = true;
				$newXml->addChild('MRPsOnEntry', $mrpsOnEntryString);
				$newXml->addChild('MRPData', $mrpData[$i][0]->__toString());
			}
		}

		if ($shouldUpdate)
		{
			$newXmlString = $newXml->asXML();
			kontorolLog::info("{$entry->id} needs MR updated");
			kontorolLog::info("old data {$xml_string}");
			kontorolLog::info("new data {$newXmlString}");
			if ($dryRunMode)
			{
				kontorolLog::info("DRY RUN Updated {$entry->id} MRP");
			}
			else
			{
				$result = $metadataPlugin->metadata->update($metadata->id, $newXmlString);
				if ($result)
				{
					kontorolLog::info("Updated {$entry->id} MRP");
				}
				else
				{
					kontorolLog::err("Failed to update {$entry->id} MRP");
				}
			}
		}
	}

	return $shouldUpdate;
}

function main($serviceUrl, $adminKs, $dryRunMode, $maxEntries)
{
	$client = getClient($serviceUrl, $adminKs);
	$metadataPlugin = KontorolMetadataClientPlugin::get($client);
	$entriesHandledCount = 0;
	$scheduledTaskProfiles = getMRProfiles($client);
	$profiles = filterProfiles($scheduledTaskProfiles);
	foreach($profiles as $profile)
	{
		/** @var KontorolScheduledTaskProfile $profile */
		kontorolLog::info("working on profile {$profile->name} id {$profile->id}");
		processEntries($client, $metadataPlugin, $profile, $dryRunMode, $maxEntries, $entriesHandledCount);
		if($entriesHandledCount >= $maxEntries)
		{
			return;
		}
	}

	return $entriesHandledCount;
}

$adminKs = $argv[1];
$serviceUrl = $argv[2];
$dryRunMode = true;
$maxEntries = 1000;

if(isset($argv[3]))
{
	$dryRunMode = $argv[3];
}

if(isset($argv[4]))
{
	$maxEntries = $argv[4];
}
$entriesHandledCount = main($serviceUrl, $adminKs, $dryRunMode, $maxEntries);
kontorolLog::info("Remove duplicate old MR script finished and updated {$entriesHandledCount} entries");

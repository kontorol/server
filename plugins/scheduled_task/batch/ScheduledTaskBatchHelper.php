<?php

class ScheduledTaskBatchHelper
{
	/**
	 * @param KontorolClient $client
	 * @param KontorolScheduledTaskProfile $scheduledTaskProfile
	 * @param KontorolFilterPager $pager
	 * @param KontorolFilter $filter
	 * @return KontorolObjectListResponse
	 */
	public static function query(KontorolClient $client, KontorolScheduledTaskProfile $scheduledTaskProfile, KontorolFilterPager $pager, $filter = null)
	{
		$objectFilterEngineType = $scheduledTaskProfile->objectFilterEngineType;
		$objectFilterEngine = KObjectFilterEngineFactory::getInstanceByType($objectFilterEngineType, $client);
		$objectFilterEngine->setPageSize($pager->pageSize);
		$objectFilterEngine->setPageIndex($pager->pageIndex);
		if(!$filter)
			$filter = $scheduledTaskProfile->objectFilter;

		return $objectFilterEngine->query($filter);
	}

	/**
	 * @param KontorolBaseEntryArray $entries
	 * @param $createAtTime
	 * @return array
	 */
	public static function getEntriesIdWithSameCreateAtTime($entries, $createAtTime)
	{
		$result = array();
		foreach ($entries as $entry)
		{
			if($entry->createdAt == $createAtTime)
				$result[] = $entry->id;
		}

		return $result;
	}

	/**
	 * @param  KontorolMediaType $mediaType
	 * @return string
	 */
	public static function getMediaTypeString($mediaType)
	{
		$relectionClass =  new ReflectionClass ('KontorolMediaType');
		$mapping = $relectionClass->getConstants();
		return array_search($mediaType, $mapping);
	}
}

<?php

/**
 * @package plugins.scheduledTask
 * @subpackage lib.objectTaskEngine
 */
class KObjectTaskDeleteEntryEngine extends KObjectTaskEntryEngineBase
{
	/**
	 * @param KontorolBaseEntry $object
	 */
	function processObject($object)
	{
		$client = $this->getClient();
		$entryId = $object->id;
		KontorolLog::info('Deleting entry '. $entryId);
		$client->baseEntry->delete($entryId);
	}
}

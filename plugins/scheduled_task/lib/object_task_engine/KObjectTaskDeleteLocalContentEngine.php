<?php

/**
 * @package plugins.scheduledTask
 * @subpackage lib.objectTaskEngine
 */
class KObjectTaskDeleteLocalContentEngine extends KObjectTaskEntryEngineBase
{

	/**
	 * @param KontorolBaseEntry $object
	 */
	function processObject($object)
	{
		$client = $this->getClient();
		$entryId = $object->id;
		KontorolLog::info("Deleting local content for entry [$entryId]");
		$flavors = $this->getEntryFlavors($object, $client);
		if (!count($flavors))
			return;

		foreach ($flavors as $flavor)
		{
			$this->deleteFlavor($flavor->id, $flavor->partnerId);
		}
	}

	protected function getEntryFlavors($object){
		$client = $this->getClient();
		$pager = new KontorolFilterPager();
		$pager->pageSize = 500; // use max size, throw exception in case we got more than 500 flavors where pagination is not supported
		$filter = new KontorolFlavorAssetFilter();
		$filter->entryIdEqual = $object->id;
		$flavorsResponse = $client->flavorAsset->listAction($filter);

		if ($flavorsResponse->totalCount > $pager->pageSize)
			throw new Exception('Too many flavors were found where pagination is not supported');

		$flavors = $flavorsResponse->objects;
		KontorolLog::info('Found '.count($flavors). ' flavors');
		return $flavors;
	}


	/**
	 * @param $id
	 * @param $partnerId
	 */
	protected function deleteFlavor($id, $partnerId)
	{
		$client = $this->getClient();
		try
		{
			$client->flavorAsset->deleteLocalContent($id);
			KontorolLog::info("Local content of flavor id [$id] was deleted");
		}
		catch(Exception $ex)
		{
			KontorolLog::err($ex->getMessage());
			KontorolLog::err("Failed to delete local content of flavor id [$id]");
		}
	}
}

<?php

/**
 * @package plugins.scheduledTask
 * @subpackage lib.objectTaskEngine
 */
class KObjectTaskModifyCategoriesEngine extends KObjectTaskEntryEngineBase
{
	/**
	 * @param KontorolBaseEntry $object
	 */
	function processObject($object)
	{
		/** @var KontorolModifyCategoriesObjectTask $objectTask */
		$objectTask = $this->getObjectTask();
		if (is_null($objectTask))
			return;

		$entryId = $object->id;
		$addRemoveType = $objectTask->addRemoveType;
		$taskCategoryIds = array();
		if (!is_array($objectTask->categoryIds))
			$objectTask->categoryIds = array();
		foreach($objectTask->categoryIds as $categoryIntValue)
		{
			/** @var KontorolString $categoryIntValue */
			$taskCategoryIds[] = $categoryIntValue->value;
		}

		if ($addRemoveType == KontorolScheduledTaskAddOrRemoveType::MOVE)
		{
			$this->removeAllCategories($entryId, $object->partnerId);
			$addRemoveType = KontorolScheduledTaskAddOrRemoveType::ADD;
		}

		// remove all categories if nothing was configured in the list
		if (count($taskCategoryIds) == 0 && $addRemoveType == KontorolScheduledTaskAddOrRemoveType::REMOVE)
		{
			$this->removeAllCategories($entryId, $object->partnerId);
		}
		else
		{
			foreach($taskCategoryIds as $categoryId)
			{
				try
				{
					$this->processCategory($entryId, $categoryId, $addRemoveType);
				}
				catch(Exception $ex)
				{
					KontorolLog::err($ex);
				}
			}
		}
	}

	/**
	 * @param $entryId
	 * @param $categoryId
	 * @param $addRemoveType
	 */
	public function processCategory($entryId, $categoryId, $addRemoveType)
	{
		$client = $this->getClient();
		$categoryEntry = null;
		$filter = new KontorolCategoryEntryFilter();
		$filter->entryIdEqual = $entryId;
		$filter->categoryIdEqual = $categoryId;
		$categoryEntryListResponse = $client->categoryEntry->listAction($filter);
		/** @var KontorolCategoryEntry $categoryEntry */
		if (count($categoryEntryListResponse->objects))
			$categoryEntry = $categoryEntryListResponse->objects[0];

		if (is_null($categoryEntry) && $addRemoveType == KontorolScheduledTaskAddOrRemoveType::ADD)
		{
			$categoryEntry = new KontorolCategoryEntry();
			$categoryEntry->entryId = $entryId;
			$categoryEntry->categoryId = $categoryId;
			$client->categoryEntry->add($categoryEntry);
		}
		elseif (!is_null($categoryEntry) && $addRemoveType == KontorolScheduledTaskAddOrRemoveType::REMOVE)
		{
			$client->categoryEntry->delete($entryId, $categoryId);
		}
	}

	/**
	 * @param $entryId
	 * @param $partnerId
	 */
	public function removeAllCategories($entryId, $partnerId)
	{
		try
		{
			$this->doRemoveAllCategories($entryId);
		}
		catch(Exception $ex)
		{
			KontorolLog::err($ex);
		}
	}

	/**
	 * @param $entryId
	 */
	public function doRemoveAllCategories($entryId)
	{
		$client = $this->getClient();
		$filter = new KontorolCategoryEntryFilter();
		$filter->entryIdEqual = $entryId;
		$categoryEntryListResponse = $client->categoryEntry->listAction($filter);
		foreach($categoryEntryListResponse->objects as $categoryEntry)
		{
			/** @var $categoryEntry KontorolCategoryEntry */
			$client->categoryEntry->delete($entryId, $categoryEntry->categoryId);
		}
	}
}

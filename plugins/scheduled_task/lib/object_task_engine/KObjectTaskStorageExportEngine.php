<?php

/**
 * @package plugins.scheduledTask
 * @subpackage lib.objectTaskEngine
 */
class KObjectTaskStorageExportEngine extends KObjectTaskEntryEngineBase
{

	/**
	 * @param KontorolBaseEntry $object
	 */
	function processObject($object)
	{
		/** @var KontorolStorageExportObjectTask $objectTask */
		$objectTask = $this->getObjectTask();
		if (is_null($objectTask))
			return;

		$entryId = $object->id;
		$storageId = $objectTask->storageId;
		if (!$storageId)
			throw new Exception('Storage profile was not configured');

		KontorolLog::info("Submitting entry export for entry $entryId to remote storage $storageId");

		$client = $this->getClient();
		$client->baseEntry->export($entryId, $storageId);
	}
}

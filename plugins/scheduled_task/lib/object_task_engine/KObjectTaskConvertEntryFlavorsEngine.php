<?php

/**
 * @package plugins.scheduledTask
 * @subpackage lib.objectTaskEngine
 */
class KObjectTaskConvertEntryFlavorsEngine extends KObjectTaskEntryEngineBase
{
	/**
	 * @param KontorolBaseEntry $object
	 */
	function processObject($object)
	{
		/** @var KontorolConvertEntryFlavorsObjectTask $objectTask */
		$objectTask = $this->getObjectTask();
		$entryId = $object->id;
		$reconvert = $objectTask->reconvert;

		$client = $this->getClient();
		$flavorParamsIds = explode(',', $objectTask->flavorParamsIds);
		foreach($flavorParamsIds as $flavorParamsId)
		{
			try
			{
				$flavorAssetFilter = new KontorolFlavorAssetFilter();
				$flavorAssetFilter->entryIdEqual = $entryId;
				$flavorAssetFilter->flavorParamsIdEqual = $flavorParamsId;
				$flavorAssetFilter->statusEqual = KontorolFlavorAssetStatus::READY;
				$flavorAssetResponse = $client->flavorAsset->listAction($flavorAssetFilter);
				if (!count($flavorAssetResponse->objects) || $reconvert)
					$client->flavorAsset->convert($entryId, $flavorParamsId);

			}
			catch(Exception $ex)
			{
				KontorolLog::err(sprintf('Failed to convert entry id %s with flavor params id %s', $entryId, $flavorParamsId));
				KontorolLog::err($ex);
			}
		}
	}
}

<?php

/**
 * @package plugins.scheduledTask
 * @subpackage api.objects
 */
abstract class KontorolObjectTask extends KontorolObject
{
	/**
	 * @readonly
	 * @var KontorolObjectTaskType
	 */
	public $type;

	/**
	 * @var bool
	 */
	public $stopProcessingOnError;

	/*
	 */
	private static $map_between_objects = array(
		'type',
		'stopProcessingOnError',
	);

	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}

	public function toObject($dbObject = null, $skip = array())
	{
		if (is_null($dbObject))
			$dbObject = new kObjectTask();

		return parent::toObject($dbObject, $skip);
	}

	/**
	 * @param array $propertiesToSkip
	 */
	public function validateForInsert($propertiesToSkip = array())
	{
		$this->validatePropertyNotNull('stopProcessingOnError');
	}

	static function getInstanceByDbObject(kObjectTask $dbObject)
	{
		switch($dbObject->getType())
		{
			case ObjectTaskType::DELETE_ENTRY:
				return new KontorolDeleteEntryObjectTask();
			case ObjectTaskType::MODIFY_CATEGORIES:
				return new KontorolModifyCategoriesObjectTask();
			case ObjectTaskType::DELETE_ENTRY_FLAVORS:
				return new KontorolDeleteEntryFlavorsObjectTask();
			case ObjectTaskType::CONVERT_ENTRY_FLAVORS:
				return new KontorolConvertEntryFlavorsObjectTask();
			case ObjectTaskType::DELETE_LOCAL_CONTENT:
				return new KontorolDeleteLocalContentObjectTask();
			case ObjectTaskType::STORAGE_EXPORT:
				return new KontorolStorageExportObjectTask();
			case ObjectTaskType::MODIFY_ENTRY:
				return new KontorolModifyEntryObjectTask();
			case ObjectTaskType::MAIL_NOTIFICATION:
				return new KontorolMailNotificationObjectTask();
			default:
				return KontorolPluginManager::loadObject('KontorolObjectTask', $dbObject->getType());
		}
	}
}

<?php

/**
 * @package plugins.scheduledTask
 * @subpackage api.objects
 */
class KontorolObjectTaskArray extends KontorolTypedArray
{
	public function __construct()
	{
		parent::__construct('KontorolObjectTask');
	}

	public static function fromDbArray(array $dbArray, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$apiArray = new KontorolObjectTaskArray();
		foreach($dbArray as $dbObject)
		{
			/** @var kObjectTask $dbObject */
			$apiObject = KontorolObjectTask::getInstanceByDbObject($dbObject);
			if (is_null($apiObject))
			{
				throw new Exception('Couldn\'t load api object for db object '.$dbObject->getType());
			}
			$apiObject->fromObject($dbObject, $responseProfile);;
			$apiArray[] = $apiObject;
		}

		return $apiArray;
	}
}

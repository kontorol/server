<?php
/**
 * @package plugins.scheduledTask
 * @subpackage api.objects
 */
class KontorolScheduledTaskProfileListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolScheduledTaskProfileArray
	 * @readonly
	 */
	public $objects;
}

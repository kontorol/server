<?php
/**
 * @package plugins.scheduledTask
 * @subpackage api.filters
 */
class KontorolScheduledTaskProfileFilter extends KontorolScheduledTaskProfileBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new ScheduledTaskProfileFilter();
	}
}

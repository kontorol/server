<?php

/**
 * @package plugins.scheduledTask
 * @subpackage api.objects.objectTasks
 */
class KontorolModifyCategoriesObjectTask extends KontorolObjectTask
{
	/**
	 * Should the object task add or remove categories?
	 *
	 * @var KontorolScheduledTaskAddOrRemoveType
	 */
	public $addRemoveType;

	/**
	 * The list of category ids to add or remove
	 *
	 * @var KontorolIntegerValueArray
	 */
	public $categoryIds;

	public function __construct()
	{
		$this->type = ObjectTaskType::MODIFY_CATEGORIES;
	}

	public function toObject($dbObject = null, $skip = array())
	{
		/** @var kObjectTask $dbObject */
		$dbObject = parent::toObject($dbObject, $skip);
		$dbObject->setDataValue('addRemoveType', $this->addRemoveType);
		$dbObject->setDataValue('categoryIds', $this->categoryIds);
		return $dbObject;
	}

	public function doFromObject($srcObj, KontorolDetachedResponseProfile $responseProfile = null)
	{
		parent::doFromObject($srcObj, $responseProfile);

		/** @var kObjectTask $srcObj */
		$this->addRemoveType = $srcObj->getDataValue('addRemoveType');
		$this->categoryIds = $srcObj->getDataValue('categoryIds');
	}
}

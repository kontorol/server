<?php

/**
 * @package plugins.scheduledTask
 * @subpackage api.objects.objectTasks
 */
class KontorolDeleteEntryObjectTask extends KontorolObjectTask
{
	public function __construct()
	{
		$this->type = ObjectTaskType::DELETE_ENTRY;
	}
}

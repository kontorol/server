<?php

/**
 * @package plugins.scheduledTask
 * @subpackage api.objects.objectTasks
 */
class KontorolDeleteLocalContentObjectTask extends KontorolObjectTask
{
	public function __construct()
	{
		$this->type = ObjectTaskType::DELETE_LOCAL_CONTENT;
	}
}

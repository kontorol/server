<?php

/**
 * @package plugins.scheduledTask
 * @subpackage api.enum
 * @see ScheduledTaskProfileStatus
 */
class KontorolScheduledTaskProfileStatus extends KontorolEnum implements ScheduledTaskProfileStatus
{
}

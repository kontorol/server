<?php

/**
 * @package plugins.scheduledTask
 * @subpackage api.enum
 * @see ScheduledTaskAddOrRemoveType
 */
class KontorolScheduledTaskAddOrRemoveType extends KontorolEnum implements ScheduledTaskAddOrRemoveType
{
}

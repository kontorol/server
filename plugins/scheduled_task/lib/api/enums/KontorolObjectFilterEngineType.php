<?php

/**
 * @package plugins.scheduledTask
 * @subpackage api.enum
 * @see ObjectFilterEngineType
 */
class KontorolObjectFilterEngineType extends KontorolDynamicEnum implements ObjectFilterEngineType
{
	public static function getEnumClass()
	{
		return 'ObjectFilterEngineType';
	}
}

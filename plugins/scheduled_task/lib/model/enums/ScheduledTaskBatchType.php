<?php
/**
 * @package plugins.scheduledTask
 * @subpackage model.enum
 */ 
class ScheduledTaskBatchType implements IKontorolPluginEnum, BatchJobType
{
	const SCHEDULED_TASK = 'ScheduledTask';
	
	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalValues()
	 */
	public static function getAdditionalValues()
	{
		return array(
			'SCHEDULED_TASK' => self::SCHEDULED_TASK,
		);
	}

	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalDescriptions()
	 */
	public static function getAdditionalDescriptions() 
	{
		return array();
	}
}

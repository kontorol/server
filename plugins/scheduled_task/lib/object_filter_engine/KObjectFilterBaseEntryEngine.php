<?php

/**
 * @package plugins.scheduledTask
 * @subpackage lib.objectFilterEngine
 */
class KObjectFilterBaseEntryEngine extends KObjectFilterEngineBase
{
	/**
	 * @param KontorolFilter $filter
	 * @return array
	 */
	public function query(KontorolFilter $filter)
	{
		return $this->_client->baseEntry->listAction($filter, $this->getPager());
	}
}

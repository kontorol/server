<?php

/**
 * @package plugins.scheduledTask
 * @subpackage lib.objectFilterEngine
 */
class KObjectFilterEntryVendorTaskEngine extends KObjectFilterEngineBase
{
	/**
	 * @param KontorolFilter $filter
	 * @return array
	 */
	public function query(KontorolFilter $filter)
	{
		return $this->_client->entryVendorTask->listAction($filter, $this->getPager());
	}
}

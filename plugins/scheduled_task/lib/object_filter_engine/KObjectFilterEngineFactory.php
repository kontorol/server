<?php

/**
 * @package plugins.scheduledTask
 * @subpackage lib.objectFilterEngine
 */
class KObjectFilterEngineFactory
{
	/**
	 * @param $type
	 * @param KontorolClient $client
	 * @return KObjectFilterEngineBase
	 */
	public static function getInstanceByType($type, KontorolClient $client)
	{
		switch($type)
		{
			case KontorolObjectFilterEngineType::ENTRY:
				return new KObjectFilterBaseEntryEngine($client);
			case KontorolObjectFilterEngineType::ENTRY_VENDOR_TASK:
				return new KObjectFilterEntryVendorTaskEngine($client);
			default:
				return KontorolPluginManager::loadObject('KObjectFilterEngineBase', $type, array($client));
		}
	}
} 

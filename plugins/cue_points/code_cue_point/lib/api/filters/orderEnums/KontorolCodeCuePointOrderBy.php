<?php
/**
 * @package plugins.codeCuePoint
 * @subpackage api.filters.enum
 */
class KontorolCodeCuePointOrderBy extends KontorolCuePointOrderBy
{
	const END_TIME_ASC = "+endTime";
	const END_TIME_DESC = "-endTime";
	const DURATION_ASC = "+duration";
	const DURATION_DESC = "-duration";
}

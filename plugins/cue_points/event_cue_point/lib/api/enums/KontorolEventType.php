<?php
/**
 * @package plugins.eventCuePoint
 * @subpackage api.enum
 */
class KontorolEventType extends KontorolDynamicEnum implements EventType
{
	public static function getEnumClass()
	{
		return 'EventType';
	}
}

<?php
/**
 * @package Scheduler
 * @subpackage copyCuePoints
 */

class KAsyncCopyCuePoints extends KJobHandlerWorker
{
	const MAX_CUE_POINTS_TO_COPY_TO_VOD = 500;

	/*
	 * (non-PHPdoc)
	 *  @see KBatchBase::getJobType();
	 */
	const ATTEMPT_ALLOWED = 3;

	public static function getType()
	{
		return KontorolBatchJobType::COPY_CUE_POINTS;
	}

	/*
	 * (non-PHPdoc)
	 *  @see KBatchBase::getJobType();
	 */
	public static function getJobType()
	{
		return KontorolBatchJobType::COPY_CUE_POINTS;
	}


	/**
	 * @param KontorolBatchJob $job
	 * @return KontorolBatchJob
	 */
	protected function exec(KontorolBatchJob $job)
	{
		$engine = KCopyCuePointEngine::initEngine($job->jobSubType, $job->data, $job->partnerId);
		if (!$engine)
			return $this->closeJob($job, KontorolBatchJobErrorTypes::APP, KontorolBatchJobAppErrors::ENGINE_NOT_FOUND,
							"Cannot find copy engine [{$job->jobSubType}]", KontorolBatchJobStatus::FAILED);
		if (!$engine->validateJobData())
			return $this->closeJob($job, KontorolBatchJobErrorTypes::APP, KontorolBatchJobAppErrors::MISSING_PARAMETERS,
				"Job subType [{$job->jobSubType}] has missing job data", KontorolBatchJobStatus::FAILED);
		if (!$engine->copyCuePoints())
			return $this->closeJob($job, KontorolBatchJobErrorTypes::APP, null,
				"Job has failed in copy the cue points", KontorolBatchJobStatus::FAILED);

		return $this->closeJob($job, null, null, "All Cue Point Copied ", KontorolBatchJobStatus::FINISHED);
	}

}

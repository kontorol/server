<?php
/**
 * Handles cue point ingestion from XML bulk upload
 * @package plugins.cuePoint
 * @subpackage batch
 */
abstract class CuePointBulkUploadXmlHandler implements IKontorolBulkUploadXmlHandler
{
	/**
	 * @var BulkUploadEngineXml
	 */
	protected $xmlBulkUploadEngine = null;
	
	/**
	 * @var KontorolCuePointClientPlugin
	 */
	protected $cuePointPlugin = null;
	
	/**
	 * @var int
	 */
	protected $entryId = null;
	
	/**
	 * @var array ingested cue points
	 */
	protected $ingested = array();
	
	/**
	 * @var array each item operation
	 */
	protected $operations = array();
	
	/**
	 * @var array of existing Cue Points with systemName
	 */
	protected static $existingCuePointsBySystemName = null;
	
	protected function __construct()
	{
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolBulkUploadXmlHandler::configureBulkUploadXmlHandler()
	 */
	public function configureBulkUploadXmlHandler(BulkUploadEngineXml $xmlBulkUploadEngine)
	{
		$this->xmlBulkUploadEngine = $xmlBulkUploadEngine;
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolBulkUploadXmlHandler::handleItemAdded()
	 */
	public function handleItemAdded(KontorolObjectBase $object, SimpleXMLElement $item)
	{
		if(!($object instanceof KontorolBaseEntry))
			return;
			
		if(empty($item->scenes))
			return;
			
		$this->entryId = $object->id;
		$this->cuePointPlugin = KontorolCuePointClientPlugin::get(KBatchBase::$kClient);
		
		KBatchBase::impersonate($this->xmlBulkUploadEngine->getCurrentPartnerId());
		KBatchBase::$kClient->startMultiRequest();
	
		$items = array();
		foreach($item->scenes->children() as $scene)
			if($this->addCuePoint($scene))
				$items[] = $scene;
			
		$results = KBatchBase::$kClient->doMultiRequest();
		KBatchBase::unimpersonate();
		
		if(is_array($results) && is_array($items))
			$this->handleResults($results, $items);
	}

	/* (non-PHPdoc)
	 * @see IKontorolBulkUploadXmlHandler::handleItemUpdated()
	 */
	public function handleItemUpdated(KontorolObjectBase $object, SimpleXMLElement $item)
	{
		if(!($object instanceof KontorolBaseEntry))
			return;
			
		if(empty($item->scenes))
			return;

		$action = KBulkUploadEngine::$actionsMap[KontorolBulkUploadAction::UPDATE];
		if(isset($item->scenes->action))
			$action = strtolower($item->scenes->action);
			
		switch ($action)
		{
			case KBulkUploadEngine::$actionsMap[KontorolBulkUploadAction::UPDATE]:
				break;
			default:
				throw new KontorolBatchException("scenes->action: $action is not supported", KontorolBatchJobAppErrors::BULK_ACTION_NOT_SUPPORTED);
		}
			
		$this->entryId = $object->id;
		$this->cuePointPlugin = KontorolCuePointClientPlugin::get(KBatchBase::$kClient);
		
		KBatchBase::impersonate($this->xmlBulkUploadEngine->getCurrentPartnerId());
		
		$this->getExistingCuePointsBySystemName($this->entryId);
		KBatchBase::$kClient->startMultiRequest();
		
		$items = array();
		foreach($item->scenes->children() as $scene)
		{
			if($this->updateCuePoint($scene))
				$items[] = $scene;
		}
			
		$results = KBatchBase::$kClient->doMultiRequest();
		KBatchBase::unimpersonate();

		if(is_array($results) && is_array($items))
			$this->handleResults($results, $items);
	}

	/* (non-PHPdoc)
	 * @see IKontorolBulkUploadXmlHandler::handleItemDeleted()
	 */
	public function handleItemDeleted(KontorolObjectBase $object, SimpleXMLElement $item)
	{
		// No handling required
	}

	/**
	 * @param string $entryId
	 * @return array of cuepoint that have systemName
	 */
	protected function getExistingCuePointsBySystemName($entryId)
	{
		if (is_array(self::$existingCuePointsBySystemName))
			return;
		
		$filter = new KontorolCuePointFilter();
		$filter->entryIdEqual = $entryId;
		
		$pager = new KontorolFilterPager();
		$pager->pageSize = 500;
		
		$cuePoints = $this->cuePointPlugin->cuePoint->listAction($filter, $pager);
		self::$existingCuePointsBySystemName = array();
		
		if (!isset($cuePoints->objects))
			return;

		foreach ($cuePoints->objects as $cuePoint)
		{
			if($cuePoint->systemName != '')
				self::$existingCuePointsBySystemName[$cuePoint->systemName] = $cuePoint->id;
		}
	}
	
	
	protected function handleResults(array $results, array $items)
	{
		if(count($results) != count($this->operations) || count($this->operations) != count($items))
		{
			KontorolLog::err("results count [" . count($results) . "] operations count [" . count($this->operations) . "] items count [" . count($items) . "]");
			return;
		}
			
		$pluginsInstances = KontorolPluginManager::getPluginInstances('IKontorolBulkUploadXmlHandler');
		
		foreach($results as $index => $cuePoint)
		{
			if(is_array($cuePoint) && isset($cuePoint['code']))
				throw new Exception($cuePoint['message']);
			
			foreach($pluginsInstances as $pluginsInstance)
			{
				/* @var $pluginsInstance IKontorolBulkUploadXmlHandler */
				
				$pluginsInstance->configureBulkUploadXmlHandler($this->xmlBulkUploadEngine);
				
				if($this->operations[$index] == KontorolBulkUploadAction::ADD)
					$pluginsInstance->handleItemAdded($cuePoint, $items[$index]);
				elseif($this->operations[$index] == KontorolBulkUploadAction::UPDATE)
					$pluginsInstance->handleItemUpdated($cuePoint, $items[$index]);
				elseif($this->operations[$index] == KontorolBulkUploadAction::DELETE)
					$pluginsInstance->handleItemDeleted($cuePoint, $items[$index]);
			}
		}
	}

	/**
	 * @return KontorolCuePoint
	 */
	abstract protected function getNewInstance();

	/**
	 * @param SimpleXMLElement $scene
	 * @return KontorolCuePoint
	 */
	protected function parseCuePoint(SimpleXMLElement $scene)
	{
		$cuePoint = $this->getNewInstance();
		
		if(isset($scene['systemName']) && $scene['systemName'])
			$cuePoint->systemName = $scene['systemName'] . '';
			
		$cuePoint->startTime = kXml::timeToInteger($scene->sceneStartTime);
		
		if(!isset($scene->tags))
			return $cuePoint;
	
		$tags = array();
		foreach ($scene->tags->children() as $tag)
		{
			$value = "$tag";
			if($value)
				$tags[] = $value;
		}
		$cuePoint->tags = implode(',', $tags);
		
		return $cuePoint;
	}
	
	/**
	 * @param SimpleXMLElement $scene
	 */
	protected function addCuePoint(SimpleXMLElement $scene)
	{
		$cuePoint = $this->parseCuePoint($scene);
		if(!$cuePoint)
			return false;
			
		$cuePoint->entryId = $this->entryId;
		$ingestedCuePoint = $this->cuePointPlugin->cuePoint->add($cuePoint);
		$this->operations[] = KontorolBulkUploadAction::ADD;
		if($cuePoint->systemName)
			$this->ingested[$cuePoint->systemName] = $ingestedCuePoint;
			
		return true;
	}

	/**
	 * @param SimpleXMLElement $scene
	 */
	protected function updateCuePoint(SimpleXMLElement $scene)
	{
		$cuePoint = $this->parseCuePoint($scene);
		if(!$cuePoint)
			return false;

		if(isset($scene['sceneId']) && $scene['sceneId'])
		{
			$cuePointId = $scene['sceneId'];
			$ingestedCuePoint = $this->cuePointPlugin->cuePoint->update($cuePointId, $cuePoint);
			$this->operations[] = KontorolBulkUploadAction::UPDATE;
		}
		elseif(isset($cuePoint->systemName) && isset(self::$existingCuePointsBySystemName[$cuePoint->systemName]))
		{
			$cuePoint = $this->removeNonUpdatbleFields($cuePoint);
			$cuePointId = self::$existingCuePointsBySystemName[$cuePoint->systemName];
			$ingestedCuePoint = $this->cuePointPlugin->cuePoint->update($cuePointId, $cuePoint);
			$this->operations[] = KontorolBulkUploadAction::UPDATE;
		}
		else
		{
			$cuePoint->entryId = $this->entryId;
			$ingestedCuePoint = $this->cuePointPlugin->cuePoint->add($cuePoint);
			$this->operations[] = KontorolBulkUploadAction::ADD;
		}
		if($cuePoint->systemName)
			$this->ingested[$cuePoint->systemName] = $ingestedCuePoint;
			
		return true;
	}
	
	/**
	 * @param string $cuePointSystemName
	 * @return string
	 */
	protected function getCuePointId($systemName)
	{
		if(isset($this->ingested[$systemName]))
		{
			$id = $this->ingested[$systemName]->id;
			return "$id";
		}
		return null;
	
//		Won't work in the middle of multi request
//		
//		$filter = new KontorolAnnotationFilter();
//		$filter->entryIdEqual = $this->entryId;
//		$filter->systemNameEqual = $systemName;
//		
//		$pager = new KontorolFilterPager();
//		$pager->pageSize = 1;
//		
//		try
//		{
//			$cuePointListResponce = $this->cuePointPlugin->cuePoint->listAction($filter, $pager);
//		}
//		catch(Exception $e)
//		{
//			return null;
//		}
//		
//		if($cuePointListResponce->totalCount && $cuePointListResponce->objects[0] instanceof KontorolAnnotation)
//			return $cuePointListResponce->objects[0]->id;
//			
//		return null;
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolConfigurator::getContainerName()
	*/
	public function getContainerName()
	{
		return 'scenes';
	}
	
	/**
	 * Removes all non updatble fields from the cuepoint
	 * @param KontorolCuePoint $entry
	 */
	protected function removeNonUpdatbleFields(KontorolCuePoint $cuePoint)
	{
		return $cuePoint;
	}
}

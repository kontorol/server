<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolClipDescription extends KontorolObject
{
	/**
	 * @var string
	 */
	public $sourceEntryId;
	
	/**
	 * 
	 * @var int
	 */
	public $startTime;
	
	/**
	 * 
	 * @var int
	 */
	public $duration;

	/**
	 *
	 * @var int
	 */
	public $offsetInDestination;
	
	private static $map_between_objects = array
	(
		"sourceEntryId" ,
		"startTime" ,
		"duration" ,
		"offsetInDestination"
	);


	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects ( )
	{
		return array_merge ( parent::getMapBetweenObjects() , self::$map_between_objects );
	}

	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbObject = null, $skip = array())
	{
		if(!$dbObject)
			$dbObject = new kClipDescription();
			
		return parent::toObject($dbObject, $skip);
	}
}

<?php
/**
 * @package plugins.cuePoint
 * @subpackage api.enum
 */
class KontorolCuePointType extends KontorolDynamicEnum implements CuePointType
{
	public static function getEnumClass()
	{
		return 'CuePointType';
	}
}

<?php
/**
 * @package plugins.cuePoint
 * @subpackage api.filters
 */
class KontorolCuePointFilter extends KontorolCuePointBaseFilter
{
	/**
	 * @var string
	 */
	public $freeText;

	/**
	 * @var KontorolNullableBoolean
	 */
	public $userIdEqualCurrent;
	
	/**
	 * @var KontorolNullableBoolean
	 */
	public $userIdCurrent;
	
	static private $map_between_objects = array
	(
		"cuePointTypeEqual" => "_eq_type",
		"cuePointTypeIn" => "_in_type",
		"freeText" => "_free_text",
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}
	
	protected function validateEntryIdFiltered()
	{
		if(!$this->idEqual && !$this->idIn && !$this->entryIdEqual && !$this->entryIdIn)
			throw new KontorolAPIException(KontorolErrors::PROPERTY_VALIDATION_CANNOT_BE_NULL,
					$this->getFormattedPropertyNameWithClassName('idEqual') . '/' . $this->getFormattedPropertyNameWithClassName('idIn') . '/' .
					$this->getFormattedPropertyNameWithClassName('entryIdEqual') . '/' . $this->getFormattedPropertyNameWithClassName('entryIdIn'));
	}

	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new CuePointFilter();
	}
	
	protected function translateUserIds()
	{		
		if($this->userIdCurrent == KontorolNullableBoolean::TRUE_VALUE)
		{
			if(kCurrentContext::$ks_kuser_id)
			{
				$this->userIdEqual = kCurrentContext::$ks_kuser_id;
			}
			else
			{
				$this->isPublicEqual = KontorolNullableBoolean::TRUE_VALUE;
			}
			$this->userIdCurrent = null;
		}
		
		if(isset($this->userIdEqual)){
			$dbKuser = kuserPeer::getKuserByPartnerAndUid(kCurrentContext::$ks_partner_id, $this->userIdEqual);
			if (! $dbKuser) {
				throw new KontorolAPIException ( KontorolErrors::INVALID_USER_ID );
			}
			$this->userIdEqual = $dbKuser->getId();
		}
		
		if(isset($this->userIdIn)){
			$userIds = explode(",", $this->userIdIn);
			foreach ($userIds as $userId){
				$dbKuser = kuserPeer::getKuserByPartnerAndUid(kCurrentContext::$ks_partner_id, $userId);
				if (! $dbKuser) {
				    throw new KontorolAPIException ( KontorolErrors::INVALID_USER_ID );
			}
				$kuserIds = $dbKuser->getId().",";
			}
			
			$this->userIdIn = $kuserIds;
		}
	}
	
	protected function getCriteria()
	{
	    return KontorolCriteria::create(CuePointPeer::OM_CLASS);
	}
	
	protected function doGetListResponse(KontorolFilterPager $pager, $type = null)
	{
		$this->validateEntryIdFiltered();
		
		if (!is_null($this->userIdEqualCurrent) && $this->userIdEqualCurrent)
		{
			$this->userIdEqual = kCurrentContext::getCurrentKsKuserId();
		}
		else
		{
			$this->translateUserIds();
		}
		
		$c = $this->getCriteria();

		if($type)
		{
			$this->cuePointTypeEqual = $type;
			$this->cuePointTypeIn = null;
		}

		$entryIds = $this->getFilteredEntryIds();
		if (!is_null($entryIds))
		{
			$entryIds = entryPeer::filterEntriesByPartnerOrKontorolNetwork ( $entryIds, kCurrentContext::getCurrentPartnerId());
			if (!$entryIds)
			{
				return array(array(), 0);
			}
			
			$this->entryIdEqual = null;
			$this->entryIdIn = implode ( ',', $entryIds );
			$this->applyPartnerOnCurrentContext($entryIds);
		}

		$cuePointFilter = $this->toObject();
		$cuePointFilter->attachToCriteria($c);

		$pager->attachToCriteria($c);
			
		$list = CuePointPeer::doSelect($c);
		
		return array($list, $c->getRecordsCount());
	}
	
	public function getTypeListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null, $type = null)
	{
		//Was added to avoid braking backward compatibility for old player chapters module
		if(isset($this->tagsLike) && $this->tagsLike==KontorolAnnotationFilter::CHAPTERS_PUBLIC_TAG)
			KontorolCriterion::disableTag(KontorolCriterion::TAG_WIDGET_SESSION);

		list($list, $totalCount) = $this->doGetListResponse($pager, $type);
		$response = new KontorolCuePointListResponse();
		$response->objects = KontorolCuePointArray::fromDbArray($list, $responseProfile);
		$response->totalCount = $totalCount;
	
		return $response;
	}
	
	/* (non-PHPdoc)
	 * @see KontorolRelatedFilter::getListResponse()
	 */
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		return $this->getTypeListResponse($pager, $responseProfile);
	}

	/* (non-PHPdoc)
	 * @see KontorolRelatedFilter::validateForResponseProfile()
	 */
	public function validateForResponseProfile()
	{
		if(		!kCurrentContext::$is_admin_session
			&&	!$this->idEqual
			&&	!$this->idIn
			&&	!$this->systemNameEqual
			&&	!$this->systemNameIn)
		{
			if(PermissionPeer::isValidForPartner(PermissionName::FEATURE_ENABLE_RESPONSE_PROFILE_USER_CACHE, kCurrentContext::getCurrentPartnerId()))
			{
				KontorolResponseProfileCacher::useUserCache();
				return;
			}
			
			throw new KontorolAPIException(KontorolCuePointErrors::USER_KS_CANNOT_LIST_RELATED_CUE_POINTS, get_class($this));
		}
	}
	
	public function applyPartnerOnCurrentContext($entryIds)
	{
		if(kCurrentContext::getCurrentPartnerId() >= 0 || !$entryIds)
			return;
		
		$entryId = reset($entryIds);
		$entry = entryPeer::retrieveByPKNoFilter($entryId);
		if($entry)
		{
			kCurrentContext::$partner_id = $entry->getPartnerId();
		}
		else
		{
			KontorolLog::debug("Entry id not filtered, If partner id not correctly defined wrong results set may be returned");
		}
	}
	
	public function getFilteredEntryIds()
	{
		$entryIds = null;
		
		if ($this->entryIdEqual)
		{
			$entryIds = array($this->entryIdEqual);
		}
		elseif ($this->entryIdIn)
		{
			$entryIds = explode(',', $this->entryIdIn);
		}
		
		return $entryIds;
	}
}

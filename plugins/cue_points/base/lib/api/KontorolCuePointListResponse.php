<?php
/**
 * @package plugins.cuePoint
 * @subpackage api.objects
 */
class KontorolCuePointListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolCuePointArray
	 * @readonly
	 */
	public $objects;
}

<?php
/**
 * @package plugins.cuePoint
 * @subpackage model.enum
 */
class CuePointObjectFeatureType implements IKontorolPluginEnum, ObjectFeatureType
{
	const CUE_POINT = 'CuePoint';
	
	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalValues()
	 */
	public static function getAdditionalValues() 
	{
		return array
		(
			'CUE_POINT' => self::CUE_POINT,
		);
		
	}

	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalDescriptions()
	 */
	public static function getAdditionalDescriptions() {
		return array();
		
	}
}

<?php
// This file generated by Propel  convert-conf target
// from XML runtime conf file C:\opt\kontorol\app\alpha\config\runtime-conf.xml
return array (
  'datasources' => 
  array (
    'kontorol' =>
    array (
      'adapter' => 'mysql',
      'connection' => 
      array (
        'phptype' => 'mysql',
        'database' => 'kontorol',
        'hostspec' => 'localhost',
        'username' => 'root',
        'password' => 'root',
      ),
    ),
    'default' => 'kontorol',
  ),
  'log' => 
  array (
    'ident' => 'kontorol',
    'level' => '7',
  ),
  'generator_version' => '1.4.2',
  'classmap' => 
  array (
    'CuePointTableMap' => 'lib/model/map/CuePointTableMap.php',
    'CuePointPeer' => 'lib/model/CuePointPeer.php',
    'CuePoint' => 'lib/model/CuePoint.php',
  ),
);

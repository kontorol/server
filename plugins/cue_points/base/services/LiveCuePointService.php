<?php
/**
 * Live Cue Point service
 *
 * @service liveCuePoint
 * @package plugins.cuePoint
 * @subpackage api.services
 * @throws KontorolErrors::SERVICE_FORBIDDEN
 */
class LiveCuePointService extends KontorolBaseService
{
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		$this->applyPartnerFilterForClass('CuePoint');

		// when session is not admin, allow access to user entries only
		if (!$this->getKs() || !$this->getKs()->isAdmin()) {
			KontorolCriterion::enableTag(KontorolCriterion::TAG_USER_SESSION);
			CuePointPeer::setUserContentOnly(true);
		}
		
		if(!CuePointPlugin::isAllowedPartner($this->getPartnerId()))
			throw new KontorolAPIException(KontorolErrors::FEATURE_FORBIDDEN, CuePointPlugin::PLUGIN_NAME);
		
		if(!$this->getPartner()->getEnabledService(PermissionName::FEATURE_KONTOROL_LIVE_STREAM))
			throw new KontorolAPIException(KontorolErrors::FEATURE_FORBIDDEN, 'Kontorol Live Streams');
	}

	/**
	 * Creates periodic metadata sync-point events on a live stream
	 * 
	 * @action createPeriodicSyncPoints
	 * @actionAlias liveStream.createPeriodicSyncPoints
	 * @deprecated This actions is not required, sync points are sent automatically on the stream.
	 * @param string $entryId Kontorol live-stream entry id
	 * @param int $interval Events interval in seconds 
	 * @param int $duration Duration in seconds
	 * 
	 * @throws KontorolErrors::ENTRY_ID_NOT_FOUND
	 * @throws KontorolErrors::NO_MEDIA_SERVER_FOUND
	 * @throws KontorolErrors::MEDIA_SERVER_SERVICE_NOT_FOUND
	 */
	function createPeriodicSyncPoints($entryId, $interval, $duration)
	{
	}
}

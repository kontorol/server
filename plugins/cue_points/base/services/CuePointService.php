<?php
/**
 * Cue Point service
 *
 * @service cuePoint
 * @package plugins.cuePoint
 * @subpackage api.services
 * @throws KontorolErrors::SERVICE_FORBIDDEN
 */
class CuePointService extends KontorolBaseService
{
	/**
	 * @return CuePointType or null to limit the service type
	 */
	protected function getCuePointType()
	{
		return null;
	}
	
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		

		// Play-Server and Media-Server list entries of all partners
		// This is not too expensive as the requests are cached conditionally and performed on sphinx
		$allowedSystemPartners = array(
			Partner::MEDIA_SERVER_PARTNER_ID,
			Partner::PLAY_SERVER_PARTNER_ID,
			Partner::BATCH_PARTNER_ID,
		);
		
		if(in_array($this->getPartnerId(), $allowedSystemPartners) && $actionName == 'list')
		{
			myPartnerUtils::resetPartnerFilter('entry');
		}
		else 
		{	
			$this->applyPartnerFilterForClass('CuePoint');
		}

		$ks = $this->getKs();
		// when session is not admin, allow access to user entries only
		if (!$ks || (!$ks->isAdmin() && !$ks->verifyPrivileges(ks::PRIVILEGE_LIST, ks::PRIVILEGE_WILDCARD))) {
			KontorolCriterion::enableTag(KontorolCriterion::TAG_USER_SESSION);
			CuePointPeer::setUserContentOnly(true);
		}
		
		if (!$ks || $ks->isAnonymousSession())
		{
			KontorolCriterion::enableTag(KontorolCriterion::TAG_WIDGET_SESSION);
		}
		
		if(!CuePointPlugin::isAllowedPartner($this->getPartnerId()))
			throw new KontorolAPIException(KontorolErrors::FEATURE_FORBIDDEN, CuePointPlugin::PLUGIN_NAME);
	}

	/**
	 * Allows you to add an cue point object associated with an entry
	 *
	 * @action add
	 * @param KontorolCuePoint $cuePoint
	 * @return KontorolCuePoint
	 * @throws KontorolAPIException
	 * @throws PropelException
	 */
	function addAction(KontorolCuePoint $cuePoint)
	{
		$dbCuePoint = $cuePoint->toInsertableObject();

		// check if we have a limitEntry set on the KS, and if so verify that it is the same entry we work on
		$limitEntry = $this->getKs()->getLimitEntry();
		if ($limitEntry && $limitEntry != $cuePoint->entryId)
		{
			throw new KontorolAPIException(KontorolCuePointErrors::NO_PERMISSION_ON_ENTRY, $cuePoint->entryId);
		}

		if($cuePoint->systemName)
		{
			$existingCuePoint = CuePointPeer::retrieveBySystemName($cuePoint->entryId, $cuePoint->systemName);
			if($existingCuePoint)
				throw new KontorolAPIException(KontorolCuePointErrors::CUE_POINT_SYSTEM_NAME_EXISTS, $cuePoint->systemName, $existingCuePoint->getId());
		}

		/* @var $dbCuePoint CuePoint */
		$dbCuePoint->setPartnerId($this->getPartnerId());
		$dbCuePoint->setPuserId(is_null($cuePoint->userId) ? $this->getKuser()->getPuserId() : $cuePoint->userId);
		$dbCuePoint->setStatus(CuePointStatus::READY); 
					
		if($this->getCuePointType())
			$dbCuePoint->setType($this->getCuePointType());
			
		$created = $dbCuePoint->save();
		if(!$created)
		{
			KontorolLog::err("Cue point not created");
			return null;
		}
		
		$cuePoint = KontorolCuePoint::getInstance($dbCuePoint, $this->getResponseProfile());
		if(!$cuePoint)
		{
			KontorolLog::err("API Cue point not instantiated");
			return null;
		}
			
		return $cuePoint;
	}

	/**
	 * Allows you to add multiple cue points objects by uploading XML that contains multiple cue point definitions
	 * 
	 * @action addFromBulk
	 * @param file $fileData
	 * @return KontorolCuePointListResponse
	 * @throws KontorolCuePointErrors::XML_FILE_NOT_FOUND
	 * @throws KontorolCuePointErrors::XML_INVALID
	 */
	function addFromBulkAction($fileData)
	{
		try
		{
			$list = kCuePointManager::addFromXml($fileData['tmp_name'], $this->getPartnerId());
		}
		catch (kCoreException $e)
		{
			throw new KontorolAPIException($e->getCode());
		}
		
		$response = new KontorolCuePointListResponse();
		$response->objects = KontorolCuePointArray::fromDbArray($list, $this->getResponseProfile());
		$response->totalCount = count($list);
	
		return $response;
	}
	
	/**
	 * Download multiple cue points objects as XML definitions
	 * 
	 * @action serveBulk
	 * @param KontorolCuePointFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return file
	 */
	function serveBulkAction(KontorolCuePointFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
			$filter = new KontorolCuePointFilter();
		else
			$this->resetUserContentFilter($filter);

		$c = KontorolCriteria::create(CuePointPeer::OM_CLASS);
		if($this->getCuePointType())
			$c->add(CuePointPeer::TYPE, $this->getCuePointType());
		
		$cuePointFilter = $filter->toObject();
		
		$cuePointFilter->attachToCriteria($c);
		if ($pager)
			$pager->attachToCriteria($c);
			
		$list = CuePointPeer::doSelect($c);
		$xml = kCuePointManager::generateXml($list);
		
		header("Content-Type: text/xml; charset=UTF-8");
		echo $xml;
		kFile::closeDbConnections();
		exit(0);
	}
	
	/**
	 * Retrieve an CuePoint object by id
	 * 
	 * @action get
	 * @param string $id 
	 * @return KontorolCuePoint
	 * @throws KontorolCuePointErrors::INVALID_CUE_POINT_ID
	 */		
	function getAction($id)
	{
		$dbCuePoint = CuePointPeer::retrieveByPK( $id );

		if(!$dbCuePoint)
			throw new KontorolAPIException(KontorolCuePointErrors::INVALID_CUE_POINT_ID, $id);
			
		if($this->getCuePointType() && $dbCuePoint->getType() != $this->getCuePointType())
			throw new KontorolAPIException(KontorolCuePointErrors::INVALID_CUE_POINT_ID, $id);
			
		$cuePoint = KontorolCuePoint::getInstance($dbCuePoint, $this->getResponseProfile());
		if(!$cuePoint)
			return null;
			
		return $cuePoint;
	}
	
	/**
	 * List cue point objects by filter and pager
	 * 
	 * @action list
	 * @param KontorolCuePointFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return KontorolCuePointListResponse
	 */
	function listAction(KontorolCuePointFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$pager)
		{
			$pager = new KontorolFilterPager();
			$pager->pageSize = baseObjectFilter::getMaxInValues();			// default to the max for compatibility reasons
		}

		if (!$filter)
			$filter = new KontorolCuePointFilter();
		else
			$this->resetUserContentFilter($filter);
		return $filter->getTypeListResponse($pager, $this->getResponseProfile(), $this->getCuePointType());
	}
	
	/**
	 * count cue point objects by filter
	 * 
	 * @action count
	 * @param KontorolCuePointFilter $filter
	 * @return int
	 */
	function countAction(KontorolCuePointFilter $filter = null)
	{
		if (!$filter)
			$filter = new KontorolCuePointFilter();
		else
			$this->resetUserContentFilter($filter);
						
		$c = KontorolCriteria::create(CuePointPeer::OM_CLASS);
		if($this->getCuePointType())
			$c->add(CuePointPeer::TYPE, $this->getCuePointType());
		
		$filter->applyPartnerOnCurrentContext($filter->getFilteredEntryIds());
		$cuePointFilter = $filter->toObject();
		$cuePointFilter->attachToCriteria($c);
		
		$c->applyFilters();
		return $c->getRecordsCount();
	}
	
	/**
	 * Update cue point by id 
	 * 
	 * @action update
	 * @param string $id
	 * @param KontorolCuePoint $cuePoint
	 * @return KontorolCuePoint
	 * @throws KontorolCuePointErrors::INVALID_CUE_POINT_ID
	 * @validateUser CuePoint id editcuepoint
	 */
	function updateAction($id, KontorolCuePoint $cuePoint)
	{
		$dbCuePoint = CuePointPeer::retrieveByPK($id);

		if (!$dbCuePoint)
			throw new KontorolAPIException(KontorolCuePointErrors::INVALID_CUE_POINT_ID, $id);

		if($this->getCuePointType() && $dbCuePoint->getType() != $this->getCuePointType())
			throw new KontorolAPIException(KontorolCuePointErrors::INVALID_CUE_POINT_ID, $id);

		// check if we have a limitEntry set on the KS, and if so verify that it is the same entry we work on
		$limitEntry = $this->getKs()->getLimitEntry();
		if ($limitEntry && $limitEntry != $dbCuePoint->getEntryId())
		{
			throw new KontorolAPIException(KontorolCuePointErrors::NO_PERMISSION_ON_ENTRY, $dbCuePoint->getEntryId());
		}

		if($cuePoint->systemName)
		{
			$existingCuePoint = CuePointPeer::retrieveBySystemName($dbCuePoint->getEntryId(), $cuePoint->systemName);
			if($existingCuePoint && $existingCuePoint->getId() != $id)
				throw new KontorolAPIException(KontorolCuePointErrors::CUE_POINT_SYSTEM_NAME_EXISTS, $cuePoint->systemName, $existingCuePoint->getId());
		}
		
		$dbCuePoint = $cuePoint->toUpdatableObject($dbCuePoint);

		$this->validateUserLog($dbCuePoint);
		
		$dbCuePoint->save();
		
		$cuePoint->fromObject($dbCuePoint, $this->getResponseProfile());
		return $cuePoint;
	}
	
	/**
	 * delete cue point by id, and delete all children cue points
	 * 
	 * @action delete
	 * @param string $id 
	 * @throws KontorolCuePointErrors::INVALID_CUE_POINT_ID
	 * @validateUser CuePoint id editcuepoint
	 */		
	function deleteAction($id)
	{
		$dbCuePoint = CuePointPeer::retrieveByPK( $id );
		
		if(!$dbCuePoint)
			throw new KontorolAPIException(KontorolCuePointErrors::INVALID_CUE_POINT_ID, $id);
			
		if($this->getCuePointType() && $dbCuePoint->getType() != $this->getCuePointType())
			throw new KontorolAPIException(KontorolCuePointErrors::INVALID_CUE_POINT_ID, $id);
		
		$this->validateUserLog($dbCuePoint);
		
		$dbCuePoint->setStatus(CuePointStatus::DELETED);
		$dbCuePoint->save();
	}
	
	/*
	 * Track delete and update API calls to identify if enabling validateUser annotation will 
	 * break any existing functionality
	 */
	private function validateUserLog($dbObject)
	{
		$log = 'validateUserLog: action ['.$this->actionName.'] client tag ['.kCurrentContext::$client_lang.'] ';
		if (!$this->getKs()){
			$log = $log.'Error: No KS ';
			KontorolLog::err($log);
			return;
		}		

		$log = $log.'ks ['.$this->getKs()->getOriginalString().'] ';
		// if admin always allowed
		if (kCurrentContext::$is_admin_session)
			return;

		if (strtolower($dbObject->getPuserId()) != strtolower(kCurrentContext::$ks_uid)) 
		{
			$log = $log.'Error: User not an owner ';
			KontorolLog::err($log);
		}
	}
	
	/**
	 * Update cuePoint status by id
	 *
	 * @action updateStatus
	 * @param string $id
	 * @param KontorolCuePointStatus $status
	 * @throws KontorolCuePointErrors::INVALID_CUE_POINT_ID
	 */
	function updateStatusAction($id, $status)
	{
		$dbCuePoint = CuePointPeer::retrieveByPK($id);
		
		if (!$dbCuePoint)
			throw new KontorolAPIException(KontorolCuePointErrors::INVALID_CUE_POINT_ID, $id);
			
		if($this->getCuePointType() && $dbCuePoint->getType() != $this->getCuePointType())
			throw new KontorolAPIException(KontorolCuePointErrors::INVALID_CUE_POINT_ID, $id);
	
		$this->validateUserLog($dbCuePoint);
		
		$dbCuePoint->setStatus($status);
		$dbCuePoint->save();
	}


	/**
	 *
	 * @action updateCuePointsTimes
	 * @param string $id
	 * @param int $startTime
	 * @param int $endTime
	 * @return KontorolCuePoint
	 * @throws KontorolCuePointErrors::INVALID_CUE_POINT_ID
	 */
	function updateCuePointsTimesAction($id, $startTime,$endTime= null)
	{
		$dbCuePoint = CuePointPeer::retrieveByPK($id);

		if (!$dbCuePoint)
			throw new KontorolAPIException(KontorolCuePointErrors::INVALID_CUE_POINT_ID, $id);

		if($this->getCuePointType() && $dbCuePoint->getType() != $this->getCuePointType())
			throw new KontorolAPIException(KontorolCuePointErrors::INVALID_CUE_POINT_ID, $id);

		$this->validateUserLog($dbCuePoint);

		$dbCuePoint->setStartTime($startTime);
		if ($endTime)
		{
			$dbCuePoint->setEndTime($endTime);
		}
		$dbCuePoint->save();
		$cuePoint = KontorolCuePoint::getInstance($dbCuePoint, $this->getResponseProfile());
		return $cuePoint;
	}

	/**
	 * Clone cuePoint with id to given entry
	 *
	 * @action clone
	 * @param string $id
	 * @param string $entryId
	 * @return KontorolCuePoint
	 * @throws KontorolCuePointErrors::INVALID_CUE_POINT_ID
	 * @throws KontorolErrors::ENTRY_ID_NOT_FOUND
	 */
	function cloneAction($id, $entryId)
	{
		$newdbCuePoint = $this->doClone($id, $entryId);
		$newdbCuePoint->save();
		$cuePoint = KontorolCuePoint::getInstance($newdbCuePoint, $this->getResponseProfile());
		return $cuePoint;
	}

	/**
	 * @param $id
	 * @param $entryId
	 * @return mixed|null
	 * @throws KontorolAPIException
	 * @throws Exception
	 */
	protected function doClone($id, $entryId)
	{
		$dbCuePoint = CuePointPeer::retrieveByPK($id);
		if (!$dbCuePoint)
			throw new KontorolAPIException(KontorolCuePointErrors::INVALID_CUE_POINT_ID, $id);
		$dbEntry = entryPeer::retrieveByPK($entryId);
		if (!$dbEntry)
			throw new KontorolAPIException(KontorolErrors::ENTRY_ID_NOT_FOUND, $entryId);
		$newdbCuePoint = null;
		try
		{
			$newdbCuePoint = $dbCuePoint->copyToEntry($dbEntry);
		}
		catch(Exception $ex)
		{
			if ($ex instanceof kCoreException)
				$this->handleCoreException($ex, $id, $entryId);
			else
				throw $ex;
		}
		return $newdbCuePoint;
	}

	/**
	 * @param kCoreException $ex
	 * @param $cuePointId
	 * @param $entryId
	 * @throws KontorolAPIException
	 * @throws Exception
	 */
	private function handleCoreException(kCoreException $ex, $cuePointId, $entryId)
	{
		switch($ex->getCode())
		{
			case kCuePointException::COPY_CUE_POINT_TO_ENTRY_NOT_PERMITTED:
				throw new KontorolAPIException(KontorolCuePointErrors::COPY_CUE_POINT_TO_ENTRY_NOT_PERMITTED, $cuePointId, $entryId);
			default:
				throw $ex;
		}
	}

	private function resetUserContentFilter($filter)
	{
		if (CuePointPeer::getUserContentOnly())
		{
			$entryFilter = $filter->entryIdEqual ? $filter->entryIdEqual : $filter->entryIdIn;
			if($entryFilter && $this->getKs()->verifyPrivileges(ks::PRIVILEGE_LIST, $entryFilter))
				CuePointPeer::setUserContentOnly(false);
		}
	}
}

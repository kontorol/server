<?php
/**
 * @package plugins.adCuePoint
 * @subpackage api.enum
 */
class KontorolAdProtocolType extends KontorolDynamicEnum implements AdProtocolType
{
	public static function getEnumClass()
	{
		return 'AdProtocolType';
	}
}

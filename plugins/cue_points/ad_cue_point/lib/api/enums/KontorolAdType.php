<?php
/**
 * @package plugins.adCuePoint
 * @subpackage api.enum
 */
class KontorolAdType extends KontorolDynamicEnum implements AdType
{
	public static function getEnumClass()
	{
		return 'AdType';
	}
}

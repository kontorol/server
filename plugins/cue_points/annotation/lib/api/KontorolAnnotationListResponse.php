<?php
/**
 * @package plugins.annotation
 * @subpackage api.objects
 */
class KontorolAnnotationListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolAnnotationArray
	 * @readonly
	 */
	public $objects;
}

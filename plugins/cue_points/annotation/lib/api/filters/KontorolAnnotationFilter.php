<?php
/**
 * @package plugins.annotation
 * @subpackage api.filters
 */
class KontorolAnnotationFilter extends KontorolAnnotationBaseFilter
{
	const CHAPTERS_PUBLIC_TAG = 'chaptering';
	
	/* (non-PHPdoc)
 	 * @see KontorolFilter::getCoreFilter()
 	 */
	protected function getCoreFilter()
	{
		return new AnnotationFilter();
	}
	
	/* (non-PHPdoc)
	 * @see KontorolRelatedFilter::validateForResponseProfile()
	 */
	public function validateForResponseProfile()
	{
		if(		!kCurrentContext::$is_admin_session
			&&	!$this->isPublicEqual)
		{
			parent::validateForResponseProfile();
		}
	}

	public function getTypeListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null, $type = null)
	{
		return parent::getTypeListResponse($pager, $responseProfile, AnnotationPlugin::getCuePointTypeCoreValue(AnnotationCuePointType::ANNOTATION));
	}
}

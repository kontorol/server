<?php
/**
 * Enable indexing and searching answers cue point objects in sphinx
 * @package plugins.cuePoint
 */
class QuizSphinxPlugin extends KontorolPlugin implements IKontorolCriteriaFactory, IKontorolPending
{
	const PLUGIN_NAME = 'quizSphinx';
	
	/* (non-PHPdoc)
	 * @see IKontorolPlugin::getPluginName()
	 */
	public static function getPluginName()
	{
		return self::PLUGIN_NAME;
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolPending::dependsOn()
	 */
	public static function dependsOn()
	{
	    $cuePointDependency = new KontorolDependency(CuePointPlugin::getPluginName());
	    $quizDependency = new KontorolDependency(QuizPlugin::getPluginName());
	
	    return array($cuePointDependency , $quizDependency);
	}	
	
	/* (non-PHPdoc)
	 * @see IKontorolCriteriaFactory::getKontorolCriteria()
	 */
	public static function getKontorolCriteria($objectType)
	{
		if ($objectType == 'AnswerCuePoint')
			return new SphinxAnswerCuePointCriteria();
			
		return null;
	}
}

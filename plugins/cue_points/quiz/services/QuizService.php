<?php
/**
 * Allows user to handle quizzes
 *
 * @service quiz
 * @package plugins.quiz
 * @subpackage api.services
 */

class QuizService extends KontorolBaseService
{

	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);

		if(!QuizPlugin::isAllowedPartner($this->getPartnerId()))
		{
			throw new KontorolAPIException(KontorolErrors::FEATURE_FORBIDDEN, QuizPlugin::PLUGIN_NAME);
		}
	}

	/**
	 * Allows to add a quiz to an entry
	 *
	 * @action add
	 * @param string $entryId
	 * @param KontorolQuiz $quiz
	 * @return KontorolQuiz
	 * @throws KontorolErrors::ENTRY_ID_NOT_FOUND
	 * @throws KontorolErrors::INVALID_USER_ID
	 * @throws KontorolQuizErrors::PROVIDED_ENTRY_IS_ALREADY_A_QUIZ
	 */
	public function addAction( $entryId, KontorolQuiz $quiz )
	{
		$dbEntry = entryPeer::retrieveByPK($entryId);
		if (!$dbEntry)
			throw new KontorolAPIException(KontorolErrors::ENTRY_ID_NOT_FOUND, $entryId);

		if ( !is_null( QuizPlugin::getQuizData($dbEntry) ) )
			throw new KontorolAPIException(KontorolQuizErrors::PROVIDED_ENTRY_IS_ALREADY_A_QUIZ, $entryId);

		return $this->validateAndUpdateQuizData( $dbEntry, $quiz );
	}

	/**
	 * Allows to update a quiz
	 *
	 * @action update
	 * @param string $entryId
	 * @param KontorolQuiz $quiz
	 * @return KontorolQuiz
	 * @throws KontorolErrors::ENTRY_ID_NOT_FOUND
	 * @throws KontorolErrors::INVALID_USER_ID
	 * @throws KontorolQuizErrors::PROVIDED_ENTRY_IS_NOT_A_QUIZ
	 */
	public function updateAction( $entryId, KontorolQuiz $quiz )
	{
		$dbEntry = entryPeer::retrieveByPK($entryId);
		$kQuiz = QuizPlugin::validateAndGetQuiz( $dbEntry );
		return $this->validateAndUpdateQuizData( $dbEntry, $quiz, $kQuiz->getVersion(), $kQuiz );
	}

	/**
	 * if user is entitled for this action will update quizData on entry
	 * @param entry $dbEntry
	 * @param KontorolQuiz $quiz
	 * @param int $currentVersion
	 * @param kQuiz|null $newQuiz
	 * @return KontorolQuiz
	 * @throws KontorolAPIException
	 */
	private function validateAndUpdateQuizData( entry $dbEntry, KontorolQuiz $quiz, $currentVersion = 0, kQuiz $newQuiz = null )
	{
		if ( !kEntitlementUtils::isEntitledForEditEntry($dbEntry) ) {
			KontorolLog::debug('Update quiz allowed only with admin KS or entry owner or co-editor');
			throw new KontorolAPIException(KontorolErrors::INVALID_USER_ID);
		}
		$quizData = $quiz->toObject($newQuiz);
		$quizData->setVersion( $currentVersion+1 );
		QuizPlugin::setQuizData( $dbEntry, $quizData );
		$dbEntry->save();
		$quiz->fromObject( $quizData );
		return $quiz;
	}

	/**
	 * Allows to get a quiz
	 *
	 * @action get
	 * @param string $entryId
	 * @return KontorolQuiz
	 * @throws KontorolErrors::ENTRY_ID_NOT_FOUND
	 *
	 */
	public function getAction( $entryId )
	{
		$dbEntry = entryPeer::retrieveByPK($entryId);
		if (!$dbEntry)
			throw new KontorolAPIException(KontorolErrors::ENTRY_ID_NOT_FOUND, $entryId);

		$kQuiz = QuizPlugin::getQuizData($dbEntry);
		if ( is_null( $kQuiz ) )
			throw new KontorolAPIException(KontorolQuizErrors::PROVIDED_ENTRY_IS_NOT_A_QUIZ, $entryId);

		$quiz = new KontorolQuiz();
		$quiz->fromObject( $kQuiz );
		return $quiz;
	}

	/**
	 * List quiz objects by filter and pager
	 *
	 * @action list
	 * @param KontorolQuizFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return KontorolQuizListResponse
	 */
	function listAction(KontorolQuizFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
			$filter = new KontorolQuizFilter;

		if (! $pager)
			$pager = new KontorolFilterPager ();

		return $filter->getListResponse($pager, $this->getResponseProfile());
	}

	/**
	 * creates a pdf from quiz object
	 * The Output type defines the file format in which the quiz will be generated
	 * Currently only PDF files are supported
	 * @action serve
	 * @param string $entryId
	 * @param KontorolQuizOutputType $quizOutputType
	 * @return file
	 * @throws KontorolErrors::ENTRY_ID_NOT_FOUND
	 * @throws KontorolQuizErrors::PROVIDED_ENTRY_IS_NOT_A_QUIZ
	 */
	public function serveAction($entryId, $quizOutputType)
	{
		KontorolLog::debug("Create a PDF Document for entry id [ " .$entryId. " ]");
		$dbEntry = entryPeer::retrieveByPK($entryId);

		//validity check
		if (!$dbEntry)
			throw new KontorolAPIException(KontorolErrors::ENTRY_ID_NOT_FOUND, $entryId);

		//validity check
		$kQuiz = QuizPlugin::getQuizData($dbEntry);
		if ( is_null( $kQuiz ) )
			throw new KontorolAPIException(KontorolQuizErrors::PROVIDED_ENTRY_IS_NOT_A_QUIZ, $entryId);

		//validity check
		if (!$kQuiz->getAllowDownload())
		{
			throw new KontorolAPIException(KontorolQuizErrors::QUIZ_CANNOT_BE_DOWNLOAD);
		}
		//create a pdf
		$kp = new kQuizPdf($entryId);
		$kp->createQuestionPdf();
		$resultPdf = $kp->submitDocument();
		$fileName = $dbEntry->getName().".pdf";
		header('Content-Disposition: attachment; filename="'.$fileName.'"');
		return new kRendererString($resultPdf, 'application/x-download');
	}


	/**
	 * sends a with an api request for pdf from quiz object
	 *
	 * @action getUrl
	 * @param string $entryId
	 * @param KontorolQuizOutputType $quizOutputType
	 * @return string
	 * @throws KontorolErrors::ENTRY_ID_NOT_FOUND
	 * @throws KontorolQuizErrors::PROVIDED_ENTRY_IS_NOT_A_QUIZ
	 * @throws KontorolQuizErrors::QUIZ_CANNOT_BE_DOWNLOAD
	 */
	public function getUrlAction($entryId, $quizOutputType)
	{
		KontorolLog::debug("Create a URL PDF Document download for entry id [ " .$entryId. " ]");

		$dbEntry = entryPeer::retrieveByPK($entryId);
		if (!$dbEntry)
			throw new KontorolAPIException(KontorolErrors::ENTRY_ID_NOT_FOUND, $entryId);

		$kQuiz = QuizPlugin::getQuizData($dbEntry);
		if ( is_null( $kQuiz ) )
			throw new KontorolAPIException(KontorolQuizErrors::PROVIDED_ENTRY_IS_NOT_A_QUIZ, $entryId);

		//validity check
		if (!$kQuiz->getAllowDownload())
		{
			throw new KontorolAPIException(KontorolQuizErrors::QUIZ_CANNOT_BE_DOWNLOAD);
		}

		$finalPath ='/api_v3/service/quiz_quiz/action/serve/quizOutputType/';

		$finalPath .="$quizOutputType";
		$finalPath .= '/entryId/';
		$finalPath .="$entryId";
		$ksObj = $this->getKs();
		$ksStr = ($ksObj) ? $ksObj->getOriginalString() : null;
		$finalPath .= "/ks/".$ksStr;

		$partnerId = $this->getPartnerId();
		$downloadUrl = myPartnerUtils::getCdnHost($partnerId) . $finalPath;

		return $downloadUrl;
	}
}

<?php
/**
 * @package plugins.quiz
 * @subpackage api.objects
 */
class KontorolQuiz extends KontorolObject
{
	/**
	 *
	 * @var int
	 * @readonly
	 */
	public $version;

	/**
	 * Array of key value ui related objects
	 * @var KontorolKeyValueArray
	 */
	public $uiAttributes;

	/**
	 * @var KontorolNullableBoolean
	 */
	public $showResultOnAnswer;

	/**
	 * @var KontorolNullableBoolean
	 */
	public $showCorrectKeyOnAnswer;

	/**
	 * @var KontorolNullableBoolean
	 */
	public $allowAnswerUpdate;

	/**
	 * @var KontorolNullableBoolean
	 */
	public $showCorrectAfterSubmission;


	/**
	 * @var KontorolNullableBoolean
	 */
	public $allowDownload;

	/**
	 * @var KontorolNullableBoolean
	 */
	public $showGradeAfterSubmission;

	/**
	 * @var int
	 */
	public $attemptsAllowed;

	/**
	 * @var KontorolScoreType
	 */
	public $scoreType;


	private static $mapBetweenObjects = array
	(
		"version",
		"uiAttributes",
		"showResultOnAnswer" => "showCorrect",
		"showCorrectKeyOnAnswer" => "showCorrectKey",
		"allowAnswerUpdate",
		"showCorrectAfterSubmission",
		"allowDownload",
		"showGradeAfterSubmission",
		"attemptsAllowed",
		"scoreType",
	);

	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$mapBetweenObjects);
	}

	/* (non-PHPdoc)
	 * @see KontorolObject::toObject($object_to_fill, $props_to_skip)
	 */
	public function toObject($dbObject = null, $propsToSkip = array())
	{
		if (!$dbObject)
		{
			$dbObject = new kQuiz();
		}

		return parent::toObject($dbObject, $propsToSkip);
	}
}

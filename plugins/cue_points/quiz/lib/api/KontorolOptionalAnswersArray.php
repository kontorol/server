<?php
/**
 *
 * Associative array of KontorolOptionalAnswer
 *
 * @package plugins.quiz
 * @subpackage api.objects
 */

class KontorolOptionalAnswersArray extends KontorolTypedArray {

	public function __construct()
	{
		return parent::__construct("KontorolOptionalAnswer");
	}

	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolOptionalAnswersArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
			$answerObj = new KontorolOptionalAnswer();
			$answerObj->fromObject($obj, $responseProfile);
			$newArr[] = $answerObj;
		}

		return $newArr;
	}
}

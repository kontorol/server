<?php
/**
 * @package plugins.quiz
 * @subpackage api.objects
 */
class KontorolQuizListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolQuizArray
	 * @readonly
	 */
	public $objects;
}

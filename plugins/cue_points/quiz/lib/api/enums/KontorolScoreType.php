<?php
/**
 * @package plugins.cuePoint
 * @subpackage api.en
 */
class KontorolScoreType  extends KontorolEnum
{
	const HIGHEST = 1;
	const LOWEST = 2;
	const LATEST = 3;
	const FIRST = 4;
	const AVERAGE = 5;
}

<?php
/**
 * @package plugins.quiz
 * @subpackage api.objects
 */
class KontorolQuestionCuePoint extends KontorolCuePoint
{

	/**
	 * Array of key value answerKey->optionAnswer objects
	 * @var KontorolOptionalAnswersArray
	 */
	public $optionalAnswers;


	/**
	 * @var string
	 */
	public $hint;


	/**
	 * @var string
	 * @filter like,mlikeor,mlikeand
	 */
	public $question;

	/**
	 * @var string
	 */
	public $explanation;


	/**
	 * @var KontorolQuestionType.
	 */
	public $questionType;

	/**
	 * @var int
	 */
	public $presentationOrder;

	/**
	 * @var KontorolNullableBoolean
	 */
	public $excludeFromScore;

	public function __construct()
	{
		$this->cuePointType = QuizPlugin::getApiValue(QuizCuePointType::QUIZ_QUESTION);
	}

	private static $map_between_objects = array
	(
		"optionalAnswers",
		"hint",
		"question" => "name",
		"explanation",
		"questionType",
		"presentationOrder",
		"excludeFromScore"
	);

	/* (non-PHPdoc)
	 * @see KontorolCuePoint::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}

	/* (non-PHPdoc)
	* @see KontorolObject::toObject($object_to_fill, $props_to_skip)
	*/
	public function toObject($dbObject = null, $propsToSkip = array())
	{
		if (!$dbObject)
		{
			$dbObject = new QuestionCuePoint();
		}

		return parent::toObject($dbObject, $propsToSkip);
	}

	/* (non-PHPdoc)
	 * @see KontorolObject::fromObject()
	 */
	public function doFromObject($dbObject, KontorolDetachedResponseProfile $responseProfile = null)
	{
		parent::doFromObject($dbObject, $responseProfile);
		$this->optionalAnswers = KontorolOptionalAnswersArray::fromDbArray($dbObject->getOptionalAnswers(), $responseProfile);
		$dbEntry = entryPeer::retrieveByPK($dbObject->getEntryId());
		if ( !kEntitlementUtils::isEntitledForEditEntry($dbEntry) ) {
			foreach ( $this->optionalAnswers as $answer ) {
				$answer->isCorrect = KontorolNullableBoolean::NULL_VALUE;
			}
			$this->explanation = null;
		}
	}

	/* (non-PHPdoc)
	 * @see KontorolCuePoint::validateForInsert()
	 */
	public function validateForInsert($propertiesToSkip = array())
	{
		parent::validateForInsert($propertiesToSkip);
		$dbEntry = entryPeer::retrieveByPK($this->entryId);
		QuizPlugin::validateAndGetQuiz($dbEntry);
		if ( !kEntitlementUtils::isEntitledForEditEntry($dbEntry) ) {
			throw new KontorolAPIException(KontorolErrors::INVALID_USER_ID);
		}
	}

}

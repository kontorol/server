<?php
/**
 * @package plugins.quiz
 * @subpackage api.filters
 */
class KontorolAnswerCuePointFilter extends KontorolAnswerCuePointBaseFilter
{
    /* (non-PHPdoc)
     * @see KontorolCuePointFilter::getCriteria()
     */
    protected function getCriteria()
    {
        return KontorolCriteria::create('AnswerCuePoint');
    }
    
	/* (non-PHPdoc)
	 * @see KontorolCuePointFilter::getTypeListResponse()
	 */
	public function getTypeListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null, $type = null)
	{
		if ($this->quizUserEntryIdIn || $this->quizUserEntryIdEqual)
		{
			KontorolCriterion::disableTag(KontorolCriterion::TAG_WIDGET_SESSION);
		}
		return parent::getTypeListResponse($pager, $responseProfile, QuizPlugin::getCoreValue('CuePointType',QuizCuePointType::QUIZ_ANSWER));
	}
	
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		kApiCache::disableCache();
		return new AnswerCuePointFilter();
	}	
}

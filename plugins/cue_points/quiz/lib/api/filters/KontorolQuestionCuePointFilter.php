<?php
/**
 * @package plugins.quiz
 * @subpackage api.filters
 */
class KontorolQuestionCuePointFilter extends KontorolQuestionCuePointBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolRelatedFilter::validateForResponseProfile()
	 */
	public function validateForResponseProfile()
	{
		// override KontorolCuePointFilter::validateForResponseProfile because all question cue-points are public
	}

	public function getTypeListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null, $type = null)
	{
		return parent::getTypeListResponse($pager, $responseProfile, QuizPlugin::getCoreValue('CuePointType',QuizCuePointType::QUIZ_QUESTION));
	}
}

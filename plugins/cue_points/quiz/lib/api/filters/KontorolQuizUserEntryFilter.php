<?php
/**
 * @package plugins.quiz
 * @subpackage api.filters
 */
class KontorolQuizUserEntryFilter extends KontorolQuizUserEntryBaseFilter
{

	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$this->typeEqual = QuizPlugin::getApiValue(QuizUserEntryType::QUIZ);
		UserEntryPeer::setDefaultCriteriaOrderBy(UserEntryPeer::ID);
		$response = parent::getListResponse($pager, $responseProfile);
		return $response;
	}
}

<?php
/**
 * @package plugins.quiz
 * @subpackage api.objects
 */
class KontorolQuizArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolQuizArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
			$kQuiz = QuizPlugin::getQuizData($obj);
			if ( !is_null($kQuiz) ) {
				$quiz = new KontorolQuiz();
				$quiz->fromObject( $kQuiz, $responseProfile );
				$newArr[] = $quiz;
			}
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolQuiz");
	}
}

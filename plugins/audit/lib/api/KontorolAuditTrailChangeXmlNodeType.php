<?php
/**
 * @package plugins.audit
 * @subpackage api.enums
 */
class KontorolAuditTrailChangeXmlNodeType extends KontorolEnum
{
	const CHANGED = 1;
	const ADDED = 2;
	const REMOVED = 3;
}

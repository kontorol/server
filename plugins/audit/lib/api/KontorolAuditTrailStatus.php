<?php
/**
 * @package plugins.audit
 * @subpackage api.enums
 */
class KontorolAuditTrailStatus extends KontorolEnum
{
	const PENDING = 1;
	const READY = 2;
	const FAILED = 3;
}

<?php
/**
 * @package plugins.audit
 * @subpackage api.enums
 */
class KontorolAuditTrailFileSyncType extends KontorolEnum
{
	const FILE = 1;
	const LINK = 2;
	const URL = 3;
}

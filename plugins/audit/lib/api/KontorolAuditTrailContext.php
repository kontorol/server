<?php
/**
 * @package plugins.audit
 * @subpackage api.enums
 */
class KontorolAuditTrailContext extends KontorolEnum
{
	const CLIENT = -1;
	const SCRIPT = 0;
	const PS2 = 1;
	const API_V3 = 2;
}

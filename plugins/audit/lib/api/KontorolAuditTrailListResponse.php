<?php
/**
 * @package plugins.audit
 * @subpackage api.objects
 */
class KontorolAuditTrailListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolAuditTrailArray
	 * @readonly
	 */
	public $objects;
}

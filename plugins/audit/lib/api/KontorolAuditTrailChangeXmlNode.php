<?php
/**
 * @package plugins.audit
 * @subpackage api.objects
 */
class KontorolAuditTrailChangeXmlNode extends KontorolAuditTrailChangeItem
{
	/**
	 * @var KontorolAuditTrailChangeXmlNodeType
	 */
	public $type;
}

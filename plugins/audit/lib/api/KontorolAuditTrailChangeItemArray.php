<?php
/**
 * @package plugins.audit
 * @subpackage api.objects
 */
class KontorolAuditTrailChangeItemArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolAuditTrailChangeItemArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
    		$nObj = new KontorolAuditTrailChangeItem();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct()
	{
		parent::__construct("KontorolAuditTrailChangeItem");
	}
	
	public function toObjectArray()
	{
		$ret = array();
		
		foreach($this as $item)
			$ret[] = $item->toObject();
			
		return $ret;
	}
}

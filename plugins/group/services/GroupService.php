<?php
/**
 * @service group
 * @package plugins.group
 * @subpackage api.services
 */

class GroupService extends KontorolBaseUserService
{
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		$kuser = kCurrentContext::getCurrentKsKuser();
		if(!$kuser && !kCurrentContext::$is_admin_session)
		{
			throw new KontorolAPIException(KontorolErrors::USER_ID_NOT_PROVIDED_OR_EMPTY);
		}
	}

	/**
	 * Adds a new group (user of type group).
	 *
	 * @action add
	 * @param KontorolGroup $group a new group
	 * @return KontorolGroup The new group
	 *
	 * @throws KontorolErrors::DUPLICATE_USER_BY_ID
	 * @throws KontorolErrors::PROPERTY_VALIDATION_CANNOT_BE_NULL
	 * @throws KontorolErrors::INVALID_FIELD_VALUE
	 * @throws KontorolErrors::UNKNOWN_PARTNER_ID
	 * @throws KontorolErrors::PASSWORD_STRUCTURE_INVALID
	 * @throws KontorolErrors::DUPLICATE_USER_BY_LOGIN_ID
	 */
	public function addAction(KontorolGroup $group)
	{
		$group->type = KuserType::GROUP;
		$lockKey = "user_add_" . $this->getPartnerId() . $group->id;
		$ret =  kLock::runLocked($lockKey, array($this, 'adduserImpl'), array($group));
		return $ret;
	}

	/**
	 * Retrieves a group object for a specified group ID.
	 * @action get
	 * @param string $groupId The unique identifier in the partner's system
	 * @return KontorolGroup The specified user object
	 *
	 * @throws KontorolGroupErrors::INVALID_GROUP_ID
	 */
	public function getAction($groupId)
	{
		$dbGroup = $this->getGroup($groupId);

		if (!kCurrentContext::$is_admin_session )//TODO - add validation function to allow access to the group
			throw new KontorolAPIException(KontorolErrors::CANNOT_RETRIEVE_ANOTHER_USER_USING_NON_ADMIN_SESSION, $groupId);


		if (!$dbGroup)
			throw new KontorolAPIException(KontorolGroupErrors::INVALID_GROUP_ID, $groupId);

		$group = new KontorolGroup();
		$group->fromObject($dbGroup, $this->getResponseProfile());

		return $group;
	}

	/**
	 * Delete group by ID
	 *
	 * @action delete
	 * @param string $groupId The unique identifier in the partner's system
	 * @return KontorolGroup The deleted  object
	 * @throws KontorolErrors::INVALID_USER_ID
	 */
	public function deleteAction($groupId)
	{
		$dbGroup = self::getGroup($groupId);
		$dbGroup->setStatus(KontorolUserStatus::DELETED);
		$dbGroup->save();
		$group = new KontorolGroup();
		$group->fromObject($dbGroup, $this->getResponseProfile());
		return $group;
	}

	/**
	 * Update group by ID
	 *
	 * @action update
	 * @param string $groupId The unique identifier in the partner's system
	 * @param KontorolGroup the updated object
	 * @return KontorolGroup The updated  object
	 * @throws KontorolErrors::INVALID_USER_ID
	 */
	public function updateAction($groupId, KontorolGroup $group)
	{
		$dbGroup = self::getGroup($groupId);
		$dbGroup = $group->toUpdatableObject($dbGroup);
		$dbGroup->save();
		$group = new KontorolGroup();
		$group->fromObject($dbGroup, $this->getResponseProfile());
		return $group;
	}

	/**
	 * Lists group  objects that are associated with an account.
	 * Blocked users are listed unless you use a filter to exclude them.
	 * Deleted users are not listed unless you use a filter to include them.
	 *
	 * @action list
	 * @param KontorolGroupFilter $filter
	 * @param KontorolFilterPager $pager A limit for the number of records to display on a page
	 * @return KontorolGroupListResponse The list of user objects
	 */
	public function listAction(KontorolGroupFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
			$filter = new KontorolGroupFilter();

		if(!$pager)
			$pager = new KontorolFilterPager();

		return $filter->getListResponse($pager, $this->getResponseProfile());

	}

	protected function getGroup($groupId)
	{
		$dbGroup = kuserPeer::getKuserByPartnerAndUid($this->getPartnerId(), $groupId);
		if(!$dbGroup || $dbGroup->getType() != KuserType::GROUP)
		{
			throw new KontorolAPIException(KontorolGroupErrors::INVALID_GROUP_ID);
		}
		return $dbGroup;
	}

	/**
	 * @action searchGroup
	 * @actionAlias elasticsearch_esearch.searchGroup
	 * @param KontorolESearchGroupParams $searchParams
	 * @param KontorolPager $pager
	 * @return KontorolESearchGroupResponse
	 */
	public function searchGroupAction(KontorolESearchGroupParams $searchParams, KontorolPager $pager = null)
	{
		$userSearch = new kUserSearch();
		list($coreResults, $objectCount) = self::initAndSearch($userSearch, $searchParams, $pager);
		$response = new KontorolESearchGroupResponse();
		$response->objects = KontorolESearchGroupResultArray::fromDbArray($coreResults, $this->getResponseProfile());
		$response->totalCount = $objectCount;
		return $response;
	}

	/**
	 * @param kBaseSearch $coreSearchObject
	 * @param $searchParams
	 * @param $pager
	 * @return array
	 */
	protected function initAndSearch($coreSearchObject, $searchParams, $pager)
	{
		list($coreSearchOperator, $objectStatusesArr, $objectId, $kPager, $coreOrder) =
			self::initSearchActionParams($searchParams, $pager);
		$elasticResults = $coreSearchObject->doSearch($coreSearchOperator, $kPager, $objectStatusesArr, $objectId, $coreOrder);

		list($coreResults, $objectCount) = kESearchCoreAdapter::transformElasticToCoreObject($elasticResults, $coreSearchObject);
		return array($coreResults, $objectCount);
	}

	protected static function initSearchActionParams($searchParams, KontorolPager $pager = null)
	{

		/**
		 * @var ESearchParams $coreParams
		 */
		$coreParams = $searchParams->toObject();

		$groupTypeItem = new ESearchUserItem();
		$groupTypeItem->setSearchTerm(KuserType::GROUP);
		$groupTypeItem->setItemType(ESearchItemType::EXACT_MATCH);
		$groupTypeItem->setFieldName(ESearchUserFieldName::TYPE);

		$baseOperator = new ESearchOperator();
		$baseOperator->setOperator(ESearchOperatorType::AND_OP);
		$baseOperator->setSearchItems(array($coreParams->getSearchOperator(), $groupTypeItem));

		$objectStatusesArr = array();
		$objectStatuses = $coreParams->getObjectStatuses();
		if (!empty($objectStatuses))
		{
			$objectStatusesArr = explode(',', $objectStatuses);
		}

		$kPager = null;
		if ($pager)
		{
			$kPager = $pager->toObject();
		}

		return array($baseOperator, $objectStatusesArr, $coreParams->getObjectId(), $kPager, $coreParams->getOrderBy());
	}

	/**
	 * clone the group (groupId), and set group id with the neeGroupName.
	 *
	 * @action clone
	 * @param string $originalGroupId The unique identifier in the partner's system
	 * @param string $newGroupId The unique identifier in the partner's system
	 * @param string $newGroupName The name of the new cloned group
	 * @return KontorolGroup The cloned group
	 *
	 * @throws KontorolErrors::INVALID_FIELD_VALUE
	 * @throws KontorolGroupErrors::INVALID_GROUP_ID
	 */
	public function cloneAction($originalGroupId, $newGroupId, $newGroupName = null)
	{
		$dbGroup = $this->getGroup($originalGroupId);

		if (!$dbGroup)
		{
			throw new KontorolAPIException(KontorolGroupErrors::INVALID_GROUP_ID, $originalGroupId);
		}

		$dbNewGroup = kuserPeer::getKuserByPartnerAndUid($this->getPartnerId(), $newGroupId);
		if ($dbNewGroup)
		{
			throw new KontorolAPIException(KontorolGroupErrors::DUPLICATE_GROUP_BY_ID, $newGroupId);
		}

		$group = new KontorolGroup();
		if ($newGroupName == null)
		{
			$newGroupName = $newGroupId;
		}
		$newDbGroup = $group->clonedObject($dbGroup, $newGroupId, $newGroupName);
		$group->validateForInsert($newDbGroup);

		$newDbGroup->save();

		$groupUsers =  KuserKgroupPeer::retrieveKuserKgroupByKgroupId($dbGroup->getId());
		$kusers = $this->getKusersFromKuserKgroup($groupUsers);
		$GroupUser = new GroupUserService();
		$isAsync = $GroupUser->addGroupUsersToClonedGroup($kusers, $newDbGroup, $dbGroup->getId());
		if($isAsync)
		{
			$newDbGroup->setProcessStatus(GroupProcessStatus::PROCESSING);
			$newDbGroup->save();
		}

		$group->fromObject($newDbGroup, $this->getResponseProfile());

		return $group;
	}

	protected function getKusersFromKuserKgroup($groupUsers)
	{
		$kusers = array();
		foreach ($groupUsers as $groupUser)
		{
			$kuserId = $groupUser->getKuserId();
			$kusers[] = kuserPeer::retrieveByPK($kuserId);
		}
		return $kusers;
	}
}

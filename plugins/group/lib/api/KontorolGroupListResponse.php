<?php
/**
 * @package plugins.group
 * @subpackage api.objects
 */

class KontorolGroupListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolGroupArray
	 * @readonly
	 */
	public $objects;
}

<?php
/**
 * @package plugins.group
 * @subpackage api.filters
 */
class KontorolGroupFilter extends KontorolUserFilter
{
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$c = KontorolCriteria::create(kuserPeer::OM_CLASS);
		$groupFilter = $this->toObject();
		$groupFilter->attachToCriteria($c);
		$c->addAnd(kuserPeer::TYPE,KuserType::GROUP);
		$c->addAnd(kuserPeer::PUSER_ID, NULL, KontorolCriteria::ISNOTNULL);
		$pager->attachToCriteria($c);
		$list = kuserPeer::doSelect($c);
		$totalCount = $c->getRecordsCount();
		$newList = KontorolGroupArray::fromDbArray($list, $responseProfile);
		$response = new KontorolGroupListResponse();
		$response->objects = $newList;
		$response->totalCount = $totalCount;
		return $response;
	}
}

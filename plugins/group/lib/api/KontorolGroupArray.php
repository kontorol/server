<?php
/**
 * @package plugins.group
 * @subpackage api.objects
 */

class KontorolGroupArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolGroupArray();
		foreach ( $arr as $obj )
		{
			$nObj = new KontorolGroup();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}

		return $newArr;
	}

	public function __construct( )
	{
		return parent::__construct ( "KontorolGroup" );
	}
}

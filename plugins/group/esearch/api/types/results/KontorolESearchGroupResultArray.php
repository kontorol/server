<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolESearchGroupResultArray extends KontorolTypedArray
{
	public function __construct()
	{
		return parent::__construct("KontorolESearchGroupResult");
	}

	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$outputArray = new KontorolESearchGroupResultArray();
		foreach ( $arr as $obj )
		{
			$nObj = new KontorolESearchGroupResult();
			$nObj->fromObject($obj, $responseProfile);
			$outputArray[] = $nObj;
		}
		return $outputArray;
	}

}

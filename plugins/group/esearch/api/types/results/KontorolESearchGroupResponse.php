<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolESearchGroupResponse extends KontorolESearchResponse
{
	/**
	 * @var KontorolESearchGroupResultArray
	 * @readonly
	 */
	public $objects;
}

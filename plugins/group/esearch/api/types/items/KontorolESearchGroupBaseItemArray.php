<?php
/**
 * @package plugins.group
 * @subpackage api.objects
 */
class KontorolESearchGroupBaseItemArray extends KontorolTypedArray
{

	public function __construct()
	{
		return parent::__construct("KontorolESearchGroupBaseItem");
	}

}

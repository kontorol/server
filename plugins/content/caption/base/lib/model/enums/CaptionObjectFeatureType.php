<?php
/**
 * @package plugins.captions
 * @subpackage model.enum
 */
class CaptionObjectFeatureType implements IKontorolPluginEnum, ObjectFeatureType
{
	const CAPTIONS = 'Captions';
	
	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalValues()
	 */
	public static function getAdditionalValues() 
	{
		return array
		(
			'CAPTIONS' => self::CAPTIONS,
		);
		
	}

	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalDescriptions()
	 */
	public static function getAdditionalDescriptions() {
		return array();
		
	}
}

<?php
/**
 * @package plugins.caption
 * @subpackage api.objects
 */
class KontorolCaptionPlaybackPluginDataArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolCaptionPlaybackPluginDataArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
			$nObj = new KontorolCaptionPlaybackPluginData();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}

		return $newArr;
	}

	public function __construct()
	{
		parent::__construct("KontorolCaptionPlaybackPluginData");
	}
}

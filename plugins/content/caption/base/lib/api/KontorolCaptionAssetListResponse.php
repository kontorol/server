<?php
/**
 * @package plugins.caption
 * @subpackage api.objects
 */
class KontorolCaptionAssetListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolCaptionAssetArray
	 * @readonly
	 */
	public $objects;
}

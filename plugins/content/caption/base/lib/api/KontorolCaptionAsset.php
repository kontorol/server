<?php
/**
 * @package plugins.caption
 * @subpackage api.objects
 * @relatedService CaptionAssetService
 */
class KontorolCaptionAsset extends KontorolAsset
{
	/**
	 * The Caption Params used to create this Caption Asset
	 * 
	 * @var int
	 * @insertonly
	 * @filter eq,in
	 */
	public $captionParamsId;
	
	/**
	 * The language of the caption asset content
	 * 
	 * @var KontorolLanguage
	 */
	public $language;
	
	/**
	 * The language of the caption asset content
	 * 
	 * @var KontorolLanguageCode
	 * @readonly
	 */
	public $languageCode;
	
	/**
	 * Is default caption asset of the entry
	 * 
	 * @var KontorolNullableBoolean
	 */
	public $isDefault;
	
	/**
	 * Friendly label
	 * 
	 * @var string
	 */
	public $label;
	
	/**
	 * The caption format
	 * 
	 * @var KontorolCaptionType
	 * @filter eq,in
	 * @insertonly
	 */
	public $format;
	
	/**
	 * The status of the asset
	 * 
	 * @var KontorolCaptionAssetStatus
	 * @readonly 
	 * @filter eq,in,notin
	 */
	public $status;

	/**
	 * The parent id of the asset
	 * @var string
	 * @insertonly
	 *
	 */
	public $parentId;

	/**
	 * The Accuracy of the caption content
	 * @var int 
	 */
	public $accuracy;
	
	/**
	 * The Accuracy of the caption content
	 * @var bool
	 */
	public $displayOnPlayer;
	
	/**
	 * List of associated transcript asset id's, comma separated
	 * @var string
	 */
	public $associatedTranscriptIds;

	private static $map_between_objects = array
	(
		"captionParamsId" => "flavorParamsId",
		"language",
		"isDefault" => "default",
		"label",
		"format" => "containerFormat",
		"status",
		"parentId",
		"accuracy",
		"displayOnPlayer",
		"associatedTranscriptIds",
	);
	
	public function getMapBetweenObjects ( )
	{
		return array_merge ( parent::getMapBetweenObjects() , self::$map_between_objects );
	}
	
	public function doFromObject($source_object, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$ret = parent::doFromObject($source_object, $responseProfile);
				
		if($this->shouldGet('languageCode', $responseProfile))
		{
			$this->languageCode = strtolower(languageCodeManager::getLanguageKey($this->language));
		}
			
		return $ret;
	}

	public function toInsertableObject ( $object_to_fill = null , $props_to_skip = array() )
	{
		if (!is_null($this->captionParamsId))
		{
			$dbAssetParams = assetParamsPeer::retrieveByPK($this->captionParamsId);
			if ($dbAssetParams)
			{
				$object_to_fill->setFromAssetParams($dbAssetParams);
			}
		}
		
		if ($this->format === null &&
			$object_to_fill->getContainerFormat() === null)		// not already set by setFromAssetParams
		{
			$this->format = KontorolCaptionType::SRT;
		}
		
		return parent::toInsertableObject ($object_to_fill, $props_to_skip);
	}


	/* (non-PHPdoc)
	 * @see KontorolObject::validateForInsert()
	 */
	public function validateForInsert($propertiesToSkip = array())
	{
		parent::validateForInsert($propertiesToSkip);
	}
}

<?php
/**
 * @package plugins.caption
 * @subpackage api.filters
 */
class KontorolCaptionParamsFilter extends KontorolCaptionParamsBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolAssetParamsFilter::getTypeListResponse()
	 */
	public function getTypeListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null, array $types = null)
	{
		list($list, $totalCount) = $this->doGetListResponse($pager, $types);
		
		$response = new KontorolCaptionParamsListResponse();
		$response->objects = KontorolCaptionParamsArray::fromDbArray($list, $responseProfile);
		$response->totalCount = $totalCount;
		return $response;  
	}
}

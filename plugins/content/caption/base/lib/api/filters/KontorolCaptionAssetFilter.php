<?php
/**
 * @package plugins.caption
 * @subpackage api.filters
 */
class KontorolCaptionAssetFilter extends KontorolCaptionAssetBaseFilter
{

	static private $map_between_objects = array
	(
		"captionParamsIdEqual" => "_eq_flavor_params_id",
		"captionParamsIdIn" => "_in_flavor_params_id",
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}	

	/* (non-PHPdoc)
	 * @see KontorolAssetFilter::getTypeListResponse()
	 */
	public function getTypeListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null, array $types = null)
	{
		$this->validateEntryIdsFiltered();
		$entryIds = $this->retrieveEntryIdsFiltered();
		$entryIds = kParentChildEntryUtils::getParentEntryIds($entryIds);
		$this->entryIdEqual = null;
		$this->entryIdIn =  implode(',', $entryIds);

		list($list, $totalCount) = $entryIds ? $this->doGetListResponse($pager, $types) : array(array(), 0);

		$response = new KontorolCaptionAssetListResponse();
		$response->objects = KontorolCaptionAssetArray::fromDbArray($list, $responseProfile);
		$response->totalCount = $totalCount;
		return $response;  
	}

	/* (non-PHPdoc)
	 * @see KontorolAssetFilter::getListResponse()
	 */
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$types = KontorolPluginManager::getExtendedTypes(assetPeer::OM_CLASS, CaptionPlugin::getAssetTypeCoreValue(CaptionAssetType::CAPTION));
		return $this->getTypeListResponse($pager, $responseProfile, $types);
	}
}

<?php
/**
 * @package plugins.caption
 * @subpackage api.enum
 */
class KontorolCaptionType extends KontorolDynamicEnum implements CaptionType
{
	public static function getEnumClass()
	{
		return 'CaptionType';
	}
}

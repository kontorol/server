<?php
/**
 * @package plugins.caption
 * @subpackage api.objects
 */
class KontorolCaptionParamsListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolCaptionParamsArray
	 * @readonly
	 */
	public $objects;
}

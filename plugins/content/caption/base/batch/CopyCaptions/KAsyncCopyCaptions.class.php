<?php
/**
 * @package plugins.caption
 * @subpackage Scheduler
 */
class KAsyncCopyCaptions extends KJobHandlerWorker
{

	const START_TIME_ASC = "+startTime";

	/*
	 * @var KontorolCaptionSearchClientPlugin
	 */
	private $captionSearchClientPlugin = null;

	/*
	* @var KontorolCaptionClientPlugin
	*/
	private $captionClientPlugin = null;

	/**
	 * @param KSchedularTaskConfig $taskConfig
	 */
	public function __construct($taskConfig = null)
	{
		parent::__construct($taskConfig);

		$this->captionSearchClientPlugin = KontorolCaptionSearchClientPlugin::get(self::$kClient);
		$this->captionClientPlugin = KontorolCaptionClientPlugin::get(self::$kClient);
	}


	public static function getType()
	{
		return KontorolBatchJobType::COPY_CAPTIONS;
	}
	/**
	 * (non-PHPdoc)
	 * @see KBatchBase::getJobType()
	 */
	protected function getJobType()
	{
		return KontorolBatchJobType::COPY_CAPTIONS;
	}

	/* (non-PHPdoc)
	 * @see KJobHandlerWorker::exec()
	 */
	protected function exec(KontorolBatchJob $job)
	{
		return $this->copyCaptions($job, $job->data);
	}

	/**
	 * copy captions on specific time frame
	 * @throws kApplicativeException
	 */
	private function copyCaptions(KontorolBatchJob $job, KontorolCopyCaptionsJobData $data)
	{
		$firstClip = $data->clipsDescriptionArray[0];
		$this->updateJob($job, "Start copying captions from [$firstClip->sourceEntryId] to [$data->entryId]", KontorolBatchJobStatus::PROCESSING);
		self::impersonate($job->partnerId);
		$this->copyFromClipToDestination($data, $data->clipsDescriptionArray);
		self::unimpersonate();
		$this->closeJob($job, null, null, 'Finished copying captions', KontorolBatchJobStatus::FINISHED);
		return $job;
	}


	private function getAllCaptionAsset($entryId)
	{
		KontorolLog::info("Retrieve all caption assets for: [$entryId]");
		$filter = new KontorolAssetFilter();
		$filter->entryIdEqual = $entryId;
		try
		{
			$captionAssetsList = $this->captionClientPlugin->captionAsset->listAction($filter);
		}
		catch(Exception $e)
		{
			KontorolLog::info("Can't list caption assets for entry id [$entryId] " . $e->getMessage());
		}
		return $captionAssetsList->objects;
	}

	private function retrieveCaptionAssetsOnlyFromSupportedTypes($originalCaptionAssets)
	{
		$unsupportedFormats = $this->getUnsupportedFormats();
		$originalCaptionAssetsFiltered = array();
		foreach ($originalCaptionAssets as $originalCaptionAsset)
		{
			if (!in_array($originalCaptionAsset->format, $unsupportedFormats))
				array_push($originalCaptionAssetsFiltered, $originalCaptionAsset);
		}
		$objectsNum = count($originalCaptionAssetsFiltered);
		KontorolLog::info("[$objectsNum] caption assets left after filtering");
		return $originalCaptionAssetsFiltered;
	}


	private function getUnsupportedFormats()
	{
		$unsupportedFormats = array (CaptionType::CAP, CaptionType::SCC);
		return $unsupportedFormats;
	}

	private function cloneCaption($targetEntryId, $originalCaptionAsset)
	{
		KontorolLog::info("Start copying properties from caption asset: [{$originalCaptionAsset->id}] to new caption asset on entryId: [$targetEntryId]");
		$captionAsset = new KontorolCaptionAsset();
		$propertiesToCopy = array("tags", "fileExt", "language", "label", "format", "isDefault", "displayOnPlayer", "accuracy");
		foreach ($propertiesToCopy as $property)
			$captionAsset->$property = $originalCaptionAsset->$property;
		try
		{
			$newCaption = $this->captionClientPlugin->captionAsset->add($targetEntryId , $captionAsset);
		}
		catch(Exception $e)
		{
			KontorolLog::info("Couldn't create new caption asset for entry id: [$targetEntryId]" . $e->getMessage());
		}
		return $newCaption;
	}

	private function loadNewCaptionAssetFile($captionAssetId, $contentResource)
	{
		try
		{
			$updatedCaption = $this->captionClientPlugin->captionAsset->setContent($captionAssetId, $contentResource);
		}
		catch(Exception $e)
		{
			KontorolLog::info("Can't set content to caption asset id: [$captionAssetId]" . $e->getMessage());
			return null;
		}
		return $updatedCaption;
	}


	private function createNewCaptionsFile($captionAssetId, $offset, $duration , $format, $fullCopy, $globalOffset){
		KontorolLog::info("Create new caption file based on captionAssetId:[$captionAssetId] in format: [$format] with offset: [$offset] and duration: [$duration]");
		$captionContent = "";

		$unsupported_formats = $this->getUnsupportedFormats();

		if($fullCopy)
		{
			KontorolLog::info("fullCopy mode - copy the content of captionAssetId: [$captionAssetId] without editing");
			$captionContent = $this->getCaptionContent($captionAssetId);
		}
		else
		{
			KontorolLog::info("Copy only the relevant content of captionAssetId: [$captionAssetId]");
			$endTime = $offset + $duration;

			if (!in_array($format, $unsupported_formats))
			{
				$captionContent = $this->getCaptionContent($captionAssetId);
				$captionsContentManager = kCaptionsContentManager::getCoreContentManager($format);
				$captionContent = $captionsContentManager->buildFile($captionContent, $offset, $endTime, $globalOffset);
			}
			else
				KontorolLog::info("copying captions for format: [$format] is not supported");
		}

		return $captionContent;
	}


	private function getCaptionContent($captionAssetId)
	{
		KontorolLog::info("Retrieve caption assets content for captionAssetId: [$captionAssetId]");

		try
		{
			$captionAssetContentUrl= $this->captionClientPlugin->captionAsset->serve($captionAssetId);
			$captionAssetContent = KCurlWrapper::getContent($captionAssetContentUrl);
		}
		catch(Exception $e)
		{
			KontorolLog::info("Can't serve caption asset id [$captionAssetId] " . $e->getMessage());
		}
		return $captionAssetContent;
	}

	/**
	 * @param KontorolCopyCaptionsJobData $data
	 * @param KontorolClipDescriptionArray $clipDescriptionArray
	 * @throws kApplicativeException
	 */
	private function copyFromClipToDestination(KontorolCopyCaptionsJobData $data, $clipDescriptionArray)
	{
		$errorMsg = '';
		//currently only one source
		$originalCaptionAssets = $this->getAllCaptionAsset($clipDescriptionArray[0]->sourceEntryId);
		if (!$data->fullCopy)
			$originalCaptionAssets = $this->retrieveCaptionAssetsOnlyFromSupportedTypes($originalCaptionAssets);
		foreach ($originalCaptionAssets as $originalCaptionAsset)
		{
			if ($originalCaptionAsset->status != KontorolCaptionAssetStatus::READY)
				continue;
			$newCaptionAsset = $this->cloneCaption($data->entryId, $originalCaptionAsset);
			$newCaptionAssetResource = new KontorolStringResource();
			$this->clipAndConcatSub($data, $clipDescriptionArray, $originalCaptionAsset, $newCaptionAsset, $newCaptionAssetResource,$errorMsg);
			$updatedCaption = $this->loadNewCaptionAssetFile($newCaptionAsset->id, $newCaptionAssetResource);
			if (!$updatedCaption)
				throw new kApplicativeException(KontorolBatchJobAppErrors::MISSING_ASSETS, "Created caption asset with id: [$newCaptionAsset->id], but couldn't load the new captions file to it");
		}
		if ($errorMsg)
			throw new kApplicativeException(KontorolBatchJobAppErrors::MISSING_ASSETS, $errorMsg);
	}

	/**
	 * @param KontorolCopyCaptionsJobData $data
	 * @param $clipDescriptionArray
	 * @param $originalCaptionAsset
	 * @param $newCaptionAsset
	 * @param $newCaptionAssetResource
	 * @param string $errorMsg
	 * @return string
	 */
	private function clipAndConcatSub(KontorolCopyCaptionsJobData $data, $clipDescriptionArray, $originalCaptionAsset, $newCaptionAsset, $newCaptionAssetResource, &$errorMsg)
	{
		foreach ($clipDescriptionArray as $clipDescription)
		{
			$toAppend = $this->createNewCaptionsFile($originalCaptionAsset->id, $clipDescription->startTime, $clipDescription->duration,
				$newCaptionAsset->format, $data->fullCopy, $clipDescription->offsetInDestination);
			if ($toAppend && $newCaptionAssetResource->content)
			{
				$captionsContentManager = kCaptionsContentManager::getCoreContentManager($newCaptionAsset->format);
				$newCaptionAssetResource->content = $captionsContentManager->merge($newCaptionAssetResource->content, $toAppend);
			}
			elseif(!$newCaptionAssetResource->content)
				$newCaptionAssetResource->content = $toAppend;
			if (is_null($toAppend))
			{
				$errorMsg = "Couldn't create new captions file for captionAssetId: [$originalCaptionAsset->id] and format: [$newCaptionAsset->format]";
			}
		}
	}

}

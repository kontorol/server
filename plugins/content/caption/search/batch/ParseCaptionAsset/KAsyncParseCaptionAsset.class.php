<?php
/**
 * @package plugins.captionSearch
 * @subpackage Scheduler
 */
class KAsyncParseCaptionAsset extends KJobHandlerWorker
{
	/* (non-PHPdoc)
	 * @see KBatchBase::getType()
	 */
	public static function getType()
	{
		return KontorolBatchJobType::PARSE_CAPTION_ASSET;
	}
	
	/* (non-PHPdoc)
	 * @see KJobHandlerWorker::exec()
	 */
	protected function exec(KontorolBatchJob $job)
	{
		return $this->parse($job, $job->data);
	}
	
	protected function parse(KontorolBatchJob $job, KontorolParseCaptionAssetJobData $data)
	{
		try
		{
			$this->updateJob($job, "Start parsing caption asset [$data->captionAssetId]", KontorolBatchJobStatus::QUEUED);
			
			$captionSearchPlugin = KontorolCaptionSearchClientPlugin::get(self::$kClient);
			$captionSearchPlugin->captionAssetItem->parse($data->captionAssetId);
			
			$this->closeJob($job, null, null, "Finished parsing", KontorolBatchJobStatus::FINISHED);
		}
		catch(Exception $ex)
		{
			$this->closeJob($job, KontorolBatchJobErrorTypes::RUNTIME, $ex->getCode(), "Error: " . $ex->getMessage(), KontorolBatchJobStatus::FAILED, $data);
		}
		return $job;
	}
}

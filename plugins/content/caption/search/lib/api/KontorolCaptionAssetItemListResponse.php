<?php
/**
 * @package plugins.captionSearch
 * @subpackage api.objects
 */
class KontorolCaptionAssetItemListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolCaptionAssetItemArray
	 * @readonly
	 */
	public $objects;
}

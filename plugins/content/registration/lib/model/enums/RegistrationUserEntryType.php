<?php
/**
 * @package plugins.registration
 * @subpackage model.enum
 */
class RegistrationUserEntryType implements IKontorolPluginEnum, UserEntryType
{
	const REGISTRATION = 'REGISTRATION';

	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalValues()
	 */
	public static function getAdditionalValues()
	{
		return array(
			'REGISTRATION' => self::REGISTRATION,
		);
	}

	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalDescriptions()
	 */
	public static function getAdditionalDescriptions()
	{
		return array(
			self::REGISTRATION => 'Registration User Entry Type',
		);
	}
}

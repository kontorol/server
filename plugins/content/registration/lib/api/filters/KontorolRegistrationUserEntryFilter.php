<?php
/**
 * @package plugins.registration
 * @subpackage api.filters
 */
class KontorolRegistrationUserEntryFilter extends KontorolUserEntryFilter
{
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$this->typeEqual = RegistrationPlugin::getApiValue(RegistrationUserEntryType::REGISTRATION);
		$response = parent::getListResponse($pager, $responseProfile);
		return $response;
	}
}

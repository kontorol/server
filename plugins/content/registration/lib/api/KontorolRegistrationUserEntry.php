<?php
/**
 * @package plugins.registration
 * @subpackage api
 */
class KontorolRegistrationUserEntry extends KontorolUserEntry
{
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbObject = null, $propertiesToSkip = array())
	{
		if(is_null($dbObject))
		{
			$dbObject = new RegistrationUserEntry();
		}
		return parent::toObject($dbObject, $propertiesToSkip);
	}
}

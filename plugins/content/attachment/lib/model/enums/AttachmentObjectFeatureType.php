<?php
/**
 * @package plugins.attachment
 * @subpackage model.enum
 */
class AttachmentObjectFeatureType implements IKontorolPluginEnum, ObjectFeatureType
{
	const ATTACHMENT = 'Attachment';
	
	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalValues()
	 */
	public static function getAdditionalValues() 
	{
		return array
		(
			'ATTACHMENT' => self::ATTACHMENT,
		);
		
	}

	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalDescriptions()
	 */
	public static function getAdditionalDescriptions() {
		return array();
		
	}
}

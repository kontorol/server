<?php
/**
 * @package plugins.attachment
 * @subpackage api.filters
 */
class KontorolAttachmentAssetFilter extends KontorolAttachmentAssetBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolAssetFilter::getTypeListResponse()
	 */
	public function getTypeListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null, array $types = null)
	{
		list($list, $totalCount) = $this->doGetListResponse($pager, $types);
		
		$response = new KontorolAttachmentAssetListResponse();
		$response->objects = KontorolAttachmentAssetArray::fromDbArray($list, $responseProfile);
		$response->totalCount = $totalCount;
		return $response;  
	}

	/* (non-PHPdoc)
	 * @see KontorolAssetFilter::getListResponse()
	 */
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$types = KontorolPluginManager::getExtendedTypes(assetPeer::OM_CLASS, AttachmentPlugin::getAssetTypeCoreValue(AttachmentAssetType::ATTACHMENT));
		return $this->getTypeListResponse($pager, $responseProfile, $types);
	}
}

<?php
/**
 * @package plugins.attachment
 * @relatedService AttachmentAssetService
 * @subpackage api.filters.base
 * @abstract
 */
abstract class KontorolAttachmentAssetBaseFilter extends KontorolAssetFilter
{
	static private $map_between_objects = array
	(
		"formatEqual" => "_eq_format",
		"formatIn" => "_in_format",
		"statusEqual" => "_eq_status",
		"statusIn" => "_in_status",
		"statusNotIn" => "_notin_status",
	);

	static private $order_by_map = array
	(
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}

	public function getOrderByMap()
	{
		return array_merge(parent::getOrderByMap(), self::$order_by_map);
	}

	/**
	 * @var KontorolAttachmentType
	 */
	public $formatEqual;

	/**
	 * @dynamicType KontorolAttachmentType
	 * @var string
	 */
	public $formatIn;

	/**
	 * @var KontorolAttachmentAssetStatus
	 */
	public $statusEqual;

	/**
	 * @var string
	 */
	public $statusIn;

	/**
	 * @var string
	 */
	public $statusNotIn;
}

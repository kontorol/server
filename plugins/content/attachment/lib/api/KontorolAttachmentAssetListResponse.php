<?php
/**
 * @package plugins.attachment
 * @subpackage api.objects
 */
class KontorolAttachmentAssetListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolAttachmentAssetArray
	 * @readonly
	 */
	public $objects;
}

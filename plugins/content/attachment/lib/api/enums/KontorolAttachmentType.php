<?php
/**
 * @package plugins.attachment
 * @subpackage api.enum
 */
class KontorolAttachmentType extends KontorolDynamicEnum implements AttachmentType
{
	public static function getEnumClass()
	{
		return 'AttachmentType';
	}
}

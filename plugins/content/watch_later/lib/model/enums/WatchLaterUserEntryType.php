<?php
/**
 * @package plugins.watchLater
 * @subpackage model.enum
 */
class WatchLaterUserEntryType implements IKontorolPluginEnum, UserEntryType
{
	const WATCH_LATER = 'WATCH_LATER';

	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalValues()
	 */
	public static function getAdditionalValues()
	{
		return array(
			'WATCH_LATER' => self::WATCH_LATER,
		);
	}

	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalDescriptions()
	 */
	public static function getAdditionalDescriptions()
	{
		return array(
			self::WATCH_LATER => 'Watch Later User Entry Type',
		);
	}
}

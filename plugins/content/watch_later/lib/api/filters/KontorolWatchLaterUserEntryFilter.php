<?php
/**
 * @package plugins.watchLater
 * @subpackage api.filters
 */
class KontorolWatchLaterUserEntryFilter extends KontorolUserEntryFilter
{
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$this->typeEqual = WatchLaterPlugin::getApiValue(WatchLaterUserEntryType::WATCH_LATER);
		$response = parent::getListResponse($pager, $responseProfile);
		return $response;
	}
}

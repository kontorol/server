<?php
/**
 * @package plugins.watchLater
 * @subpackage api
 */
class KontorolWatchLaterUserEntry extends KontorolUserEntry
{
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbObject = null, $propertiesToSkip = array())
	{
		if(is_null($dbObject))
		{
			$dbObject = new WatchLaterUserEntry();
		}

		return parent::toObject($dbObject, $propertiesToSkip);
	}
}

<?php

/**
 * Auto-generated class.
 * 
 * Used to search KontorolExternalMediaEntry attributes. Use KontorolExternalMediaEntryCompareAttribute enum to provide attribute name.
*/
class KontorolExternalMediaEntryCompareAttributeCondition extends KontorolSearchComparableAttributeCondition
{
	/**
	 * @var KontorolExternalMediaEntryCompareAttribute
	 */
	public $attribute;

	private static $mapBetweenObjects = array
	(
		"attribute" => "attribute",
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects() , self::$mapBetweenObjects);
	}

	protected function getIndexClass()
	{
		return 'entryIndex';
	}
}


<?php

/**
 * Auto-generated class.
 * 
 * Used to search KontorolExternalMediaEntry attributes. Use KontorolExternalMediaEntryMatchAttribute enum to provide attribute name.
*/
class KontorolExternalMediaEntryMatchAttributeCondition extends KontorolSearchMatchAttributeCondition
{
	/**
	 * @var KontorolExternalMediaEntryMatchAttribute
	 */
	public $attribute;

	private static $mapBetweenObjects = array
	(
		"attribute" => "attribute",
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects() , self::$mapBetweenObjects);
	}

	protected function getIndexClass()
	{
		return 'entryIndex';
	}
}


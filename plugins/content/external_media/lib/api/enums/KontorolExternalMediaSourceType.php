<?php
/**
 * @package plugins.externalMedia
 * @subpackage api.enum
 */
class KontorolExternalMediaSourceType extends KontorolDynamicEnum implements ExternalMediaSourceType
{
	public static function getEnumClass()
	{
		return 'ExternalMediaSourceType';
	}
}

<?php
/**
 * @package plugins.externalMedia
 * @subpackage api.objects
 */
class KontorolExternalMediaEntryListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolExternalMediaEntryArray
	 * @readonly
	 */
	public $objects;
	
}

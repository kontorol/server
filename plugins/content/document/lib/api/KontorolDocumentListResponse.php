<?php
/**
 * @package plugins.document
 * @subpackage api.objects
 */
class KontorolDocumentListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolDocumentEntryArray
	 * @readonly
	 */
	public $objects;
}

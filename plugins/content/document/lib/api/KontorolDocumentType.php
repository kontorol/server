<?php
/**
 * @package plugins.document
 * @subpackage api.enum
 */
class KontorolDocumentType extends KontorolEnum
{
	const DOCUMENT = 11;
	const SWF = 12;
	const PDF = 13;
	
}

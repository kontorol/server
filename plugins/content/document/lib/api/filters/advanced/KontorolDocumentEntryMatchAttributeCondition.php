<?php

/**
 * Auto-generated class.
 * 
 * Used to search KontorolDocumentEntry attributes. Use KontorolDocumentEntryMatchAttribute enum to provide attribute name.
*/
class KontorolDocumentEntryMatchAttributeCondition extends KontorolSearchMatchAttributeCondition
{
	/**
	 * @var KontorolDocumentEntryMatchAttribute
	 */
	public $attribute;

	private static $mapBetweenObjects = array
	(
		"attribute" => "attribute",
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects() , self::$mapBetweenObjects);
	}

	protected function getIndexClass()
	{
		return 'entryIndex';
	}
}


<?php

/**
 * Auto-generated class.
 * 
 * Used to search KontorolDocumentEntry attributes. Use KontorolDocumentEntryCompareAttribute enum to provide attribute name.
*/
class KontorolDocumentEntryCompareAttributeCondition extends KontorolSearchComparableAttributeCondition
{
	/**
	 * @var KontorolDocumentEntryCompareAttribute
	 */
	public $attribute;

	private static $mapBetweenObjects = array
	(
		"attribute" => "attribute",
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects() , self::$mapBetweenObjects);
	}

	protected function getIndexClass()
	{
		return 'entryIndex';
	}
}


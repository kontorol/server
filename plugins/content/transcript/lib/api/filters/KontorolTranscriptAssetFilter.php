<?php
/**
 * @package plugins.transcript
 * @subpackage api.filters
 */
class KontorolTranscriptAssetFilter extends KontorolTranscriptAssetBaseFilter
{	
	/* (non-PHPdoc)
	 * @see KontorolAssetFilter::getTypeListResponse()
	 */
	public function getTypeListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null, array $types = null)
	{
		$types = KontorolPluginManager::getExtendedTypes(assetPeer::OM_CLASS, TranscriptPlugin::getAssetTypeCoreValue(TranscriptAssetType::TRANSCRIPT));
		list($list, $totalCount) = $this->doGetListResponse($pager, $types);

		$response = new KontorolTranscriptAssetListResponse();
		$response->objects = KontorolTranscriptAssetArray::fromDbArray($list, $responseProfile);
		$response->totalCount = $totalCount;
		return $response;
	}

	/* (non-PHPdoc)
	 * @see KontorolAssetFilter::getListResponse()
	 */
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$types = KontorolPluginManager::getExtendedTypes(assetPeer::OM_CLASS, TranscriptPlugin::getAssetTypeCoreValue(TranscriptAssetType::TRANSCRIPT));
		return $this->getTypeListResponse($pager, $responseProfile, $types);
	}
}

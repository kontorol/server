<?php
/**
 * @package plugins.transcript
 * @subpackage api.objects
 */
class KontorolTranscriptAssetListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolTranscriptAssetArray
	 * @readonly
	 */
	public $objects;
}

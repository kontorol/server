<?php
/**
 * @package plugins.transcript
 * @subpackage api.enum
 */
class KontorolTranscriptProviderType extends KontorolDynamicEnum implements TranscriptProviderType
{
	public static function getEnumClass()
	{
		return 'TranscriptProviderType';
	}
}

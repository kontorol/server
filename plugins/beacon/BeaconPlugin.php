<?php

/**
 * Sending beacons on various objects
 * @package plugins.beacon
 */
class BeaconPlugin extends KontorolPlugin implements IKontorolServices, IKontorolPermissions, IKontorolPending
{
	const PLUGIN_NAME = "beacon";
	const BEACON_MANAGER = 'kBeaconManager';
	
	/* (non-PHPdoc)
	 * @see IKontorolServices::getServicesMap()
	 */
	public static function getServicesMap()
	{
		$map = array(
			'beacon' => 'BeaconService',
		);
		return $map;
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolPermissions::isAllowedPartner()
	 */
	public static function isAllowedPartner($partnerId)
	{
		return true;
	}
	
	
	/* (non-PHPdoc)
	 * @see IKontorolPlugin::getPluginName()
	 */
	public static function getPluginName()
	{
		return self::PLUGIN_NAME;
	}
	
	/* (non-PHPdoc)
 	 * @see IKontorolPending::dependsOn()
 	*/
	public static function dependsOn()
	{
		$rabbitMqDependency = new KontorolDependency(RabbitMQPlugin::getPluginName());
		$elasticSearchDependency = new KontorolDependency(ElasticSearchPlugin::getPluginName());
		return array($rabbitMqDependency, $elasticSearchDependency);
	}
	/**
	 * @return string external API value of dynamic enum.
	 */
	public static function getApiValue($valueName)
	{
		return self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
	}
}

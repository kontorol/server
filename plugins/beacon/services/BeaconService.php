<?php

/**
 * Sending beacons on objects
 *
 * @service beacon
 * @package plugins.beacon
 * @subpackage api.services
 */
class BeaconService extends KontorolBaseService
{
	public function initService($serviceId, $serviceName, $actionName)
	{
		if (($actionName == 'getLast' || $actionName == 'enhanceSearch') && !kCurrentContext::$is_admin_session)
			throw new KontorolAPIException(KontorolErrors::SERVICE_FORBIDDEN, $this->serviceName . '->' . $this->actionName);
		
		parent::initService($serviceId, $serviceName, $actionName);
	}
	
	/**
	 * @action add
	 * @param KontorolBeacon $beacon
	 * @param KontorolNullableBoolean $shouldLog
	 * @return bool
	 */
	public function addAction(KontorolBeacon $beacon, $shouldLog = KontorolNullableBoolean::FALSE_VALUE)
	{
		$beaconObj = $beacon->toInsertableObject();
		$res = $beaconObj->index($shouldLog);
		
		return $res;
	}
	
	/**
	 * @action list
	 * @param KontorolBeaconFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return KontorolBeaconListResponse
	 * @throws KontorolAPIException
	 */
	public function listAction(KontorolBeaconFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
			$filter = new KontorolBeaconFilter();
		
		if (!$pager)
			$pager = new KontorolFilterPager();
		
		return $filter->getListResponse($pager);
	}
	
	/**
	 * @action enhanceSearch
	 * @param KontorolBeaconEnhanceFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return KontorolBeaconListResponse
	 * @throws KontorolAPIException
	 */
	
	public function enhanceSearchAction(KontorolBeaconEnhanceFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
			$filter = new KontorolBeaconEnhanceFilter();
		
		if(!$pager)
			$pager = new KontorolFilterPager();
		
		return $filter->enhanceSearch($pager);
	}

	/**
	 * @action searchScheduledResource
	 * @param KontorolBeaconSearchParams $searchParams
	 * @param KontorolPager $pager
	 * @return KontorolBeaconListResponse
	 * @throws KontorolAPIException
	 */

	public function searchScheduledResourceAction(KontorolBeaconSearchParams $searchParams, KontorolPager $pager = null)
	{
		$scheduledResourceSearch = new kScheduledResourceSearch();
		$searchMgr = new kBeaconSearchQueryManger();
		$elasticResponse = $this->initAndSearch($scheduledResourceSearch, $searchParams, $pager);
		$totalCount = $searchMgr->getTotalCount($elasticResponse);
		$responseArray = $searchMgr->getHitsFromElasticResponse($elasticResponse);
		$response = new KontorolBeaconListResponse();
		$response->objects = KontorolBeaconArray::fromDbArray($responseArray);
		$response->totalCount = $totalCount;
		return $response;
	}

	/**
	 * @param kBaseSearch $coreSearchObject
	 * @param $searchParams
	 * @param $pager
	 * @return array
	 */
	private function initAndSearch($coreSearchObject, $searchParams, $pager)
	{
		list($coreParams, $kPager) = self::initSearchActionParams($searchParams, $pager);
		$elasticResults = $coreSearchObject->doSearch($coreParams->getSearchOperator(), $kPager, array(), $coreParams->getObjectId(), $coreParams->getOrderBy());
		return $elasticResults;
	}

	protected static function initSearchActionParams($searchParams, KontorolPager $pager = null)
	{
		$coreParams = $searchParams->toObject();
		$kPager = null;
		if ($pager)
		{
			$kPager = $pager->toObject();
		}

		return array($coreParams, $kPager);
	}

}

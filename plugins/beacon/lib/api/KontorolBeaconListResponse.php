<?php

/**
 * @package plugins.beacon
 * @subpackage api.objects
 */
class KontorolBeaconListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolBeaconArray
	 * @readonly
	 */
	public $objects;
}

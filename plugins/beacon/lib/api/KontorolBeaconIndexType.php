<?php

/**
 * @package plugins.beacon
 * @subpackage api.enum
 */
class KontorolBeaconIndexType extends KontorolStringEnum implements BeaconIndexType
{
	public static function getEnumClass()
	{
		return 'BeaconIndexType';
	}
}

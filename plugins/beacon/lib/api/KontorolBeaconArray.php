<?php

/**
 * @package plugins.beacon
 * @subpackage api.objects
 */
class KontorolBeaconArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolBeaconArray();
		if ($arr == null)
			return $newArr;
		
		foreach ($arr as $obj) 
		{
			$nObj = new KontorolBeacon();
			$nObj->fromArray($obj);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct()
	{
		parent::__construct("KontorolBeacon");
	}
}

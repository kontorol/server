<?php
/**
 * @package plugins.beacon
 * @subpackage api.objects
 */
class KontorolBeaconScheduledResourceItem extends KontorolBeaconAbstractScheduledResourceItem
{
	/**
	 * @var KontorolBeaconScheduledResourceFieldName
	 */
	public $fieldName;

	private static $map_between_objects = array(
		'fieldName'
	);

	private static $map_dynamic_enum = array();

	private static $map_field_enum = array(
		KontorolBeaconScheduledResourceFieldName::EVENT_TYPE => BeaconScheduledResourceFieldName::EVENT_TYPE,
		KontorolBeaconScheduledResourceFieldName::OBJECT_ID => BeaconScheduledResourceFieldName::OBJECT_ID,
		KontorolBeaconScheduledResourceFieldName::IS_LOG => BeaconScheduledResourceFieldName::IS_LOG,
		KontorolBeaconScheduledResourceFieldName::STATUS => BeaconScheduledResourceFieldName::STATUS,
		KontorolBeaconScheduledResourceFieldName::RECORDING => BeaconScheduledResourceFieldName::RECORDING,
		KontorolBeaconScheduledResourceFieldName::RESOURCE_NAME => BeaconScheduledResourceFieldName::RESOURCE_NAME,
		KontorolBeaconScheduledResourceFieldName::UPDATED_AT => BeaconScheduledResourceFieldName::UPDATED_AT,
	);

	protected function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}

	public function toObject($object_to_fill = null, $props_to_skip = array())
	{
		if (!$object_to_fill)
		{
			$object_to_fill = new kBeaconScheduledResourceItem();
		}

		return parent::toObject($object_to_fill, $props_to_skip);
	}

	protected function getItemFieldName()
	{
		return $this->fieldName;
	}

	protected function getFieldEnumMap()
	{
		return self::$map_field_enum;
	}

	protected function getDynamicEnumMap()
	{
		return self::$map_dynamic_enum;
	}
}

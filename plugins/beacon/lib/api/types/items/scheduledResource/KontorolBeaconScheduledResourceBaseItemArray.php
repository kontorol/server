<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolBeaconScheduledResourceBaseItemArray extends KontorolTypedArray
{

	public function __construct()
	{
		return parent::__construct("KontorolBeaconScheduledResourceBaseItem");
	}

}

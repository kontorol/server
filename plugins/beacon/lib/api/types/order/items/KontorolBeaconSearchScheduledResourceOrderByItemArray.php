<?php
/**
 * @package plugins.beacon
 * @subpackage api.objects
 */
class KontorolBeaconSearchScheduledResourceOrderByItemArray extends KontorolTypedArray
{

	public function __construct()
	{
		return parent::__construct("KontorolBeaconSearchScheduledResourceOrderByItem");
	}

}

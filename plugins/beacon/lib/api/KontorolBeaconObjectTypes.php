<?php

/**
 * @package plugins.beacon
 * @subpackage api.enum
 */
class KontorolBeaconObjectTypes extends KontorolDynamicEnum implements BeaconObjectTypes
{
	public static function getEnumClass()
	{
		return 'BeaconObjectTypes';
	}
}

<?php

/**
 * @package plugins.beacon
 * @subpackage api.filters
 */
class KontorolBeaconEnhanceFilter extends KontorolFilter
{
	/**
	 * @var string
	 */
	public $externalElasticQueryObject;
	
	/**
	 * @var KontorolBeaconIndexType
	 */
	public $indexTypeEqual;
	
	public function getCoreFilter()
	{
		return null;
	}
	
	public function enhanceSearch(KontorolFilterPager $pager)
	{
		$utf8Query = utf8_encode($this->externalElasticQueryObject);
		$queryJsonObject = json_decode($utf8Query, true);
		
		if(!$queryJsonObject)
			throw new KontorolAPIException(APIErrors::INTERNAL_SERVERL_ERROR);
		
		if(!isset($queryJsonObject['query']))
			throw new KontorolAPIException(APIErrors::INTERNAL_SERVERL_ERROR);
		
		$searchQuery = array();
		$searchQuery['body']['query']['bool']['must'] = $queryJsonObject['query'];
		$searchQuery['body']['query']['bool']['filter'][]['term'] = array ("partner_id" => kCurrentContext::getCurrentPartnerId());
		
		if($this->indexTypeEqual)
			$searchQuery['body']['query']['bool']['filter'][]['term'] = array (kBeacon::FIELD_IS_LOG => ($this->indexTypeEqual == KontorolBeaconIndexType::LOG) ? true : false);
		
		$searchQuery[elasticClient::ELASTIC_INDEX_KEY] = kBeacon::ELASTIC_BEACONS_INDEX_NAME;
		$searchQuery[kESearchQueryManager::BODY_KEY][elasticClient::ELASTIC_SIZE_KEY] = $pager->pageSize;
		$searchQuery[kESearchQueryManager::BODY_KEY][elasticClient::ELASTIC_FROM_KEY] = $pager->calcOffset();
		
		if(isset($queryJsonObject['sort']))
			$searchQuery[kESearchQueryManager::BODY_KEY][kESearchQueryManager::SORT_KEY] = $queryJsonObject['sort'];
		
		$searchMgr = new kBeaconSearchQueryManger();
		$responseArray = $searchMgr->search($searchQuery);
		$totalCount = $searchMgr->getTotalCount($responseArray);
		$responseArray = $searchMgr->getHitsFromElasticResponse($responseArray);
		
		$response = new KontorolBeaconListResponse();
		$response->objects = KontorolBeaconArray::fromDbArray($responseArray);
		$response->totalCount = $totalCount;
		return $response;
	}
}

<?php
/**
 * @package plugins.kontiki
 *  @subpackage model.enum
 */
class KontikiStorageProfileProtocol implements IKontorolPluginEnum, StorageProfileProtocol
{
	const KONTIKI = 'KONTIKI';
	
	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalValues()
	 */
	public static function getAdditionalValues() {
		return array('KONTIKI' => self::KONTIKI);
		
	}

	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalDescriptions()
	 */
	public static function getAdditionalDescriptions() {
		return array();
		
	}

	
}

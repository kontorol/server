<?php
/**
 * @package plugins.like
 * @subpackage api.objects
 */
class KontorolLikeListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolLikeArray
	 * @readonly
	 */
	public $objects;
}

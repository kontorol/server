<?php
/**
 * Enable 'liking' or 'unliking' an entry as the current user, rather than anonymously ranking it.
 * @package plugins.like
 */
class LikePlugin extends KontorolPlugin implements IKontorolServices, IKontorolPermissions
{
    const PLUGIN_NAME = "like";
    
	/* (non-PHPdoc)
     * @see IKontorolServices::getServicesMap()
     */
    public static function getServicesMap ()
    {
        $map = array(
			'like' => 'LikeService',
		);
		return $map;
    }

	/* (non-PHPdoc)
	 * @see IKontorolPermissions::isAllowedPartner()
	 */
	public static function isAllowedPartner($partnerId)
	{
		$partner = PartnerPeer::retrieveByPK($partnerId);
		
		return $partner->getEnabledService(KontorolPermissionName::FEATURE_LIKE);
	}
	

	/* (non-PHPdoc)
     * @see IKontorolPlugin::getPluginName()
     */
    public static function getPluginName ()
    {
        return self::PLUGIN_NAME;
    }

    
}

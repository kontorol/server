<?php

/**
 * Represents the Bulk upload job data for filter bulk upload
 * @package plugins.bulkUploadFilter
 * @subpackage api.objects
 */
class KontorolBulkUploadFilterJobData extends KontorolBulkUploadJobData
{	
	/**
	 * Filter for extracting the objects list to upload 
	 * @var KontorolFilter
	 */
	public $filter;

	/**
	 * Template object for new object creation
	 * @var KontorolObject
	 */
	public $templateObject;
	
	/**
	 * 
	 * Maps between objects and the properties
	 * @var array
	 */
	private static $map_between_objects = array
	(
		"filter",
	);

	
	public function getMapBetweenObjects ( )
	{
		return array_merge ( parent::getMapBetweenObjects() , self::$map_between_objects );
	}
	
	public function toObject($dbData = null, $props_to_skip = array()) 
	{
		if(is_null($dbData))
			$dbData = new kBulkUploadFilterJobData();
		
		switch (get_class($this->templateObject))
	    {
	        case 'KontorolCategoryEntry':
	           	$dbData->setTemplateObject(new categoryEntry());
	           	$this->templateObject->toObject($dbData->getTemplateObject());
	            break;
	        default:
	            break;
	    }
	    
		return parent::toObject($dbData);
	}

	/* (non-PHPdoc)
	 * @see KontorolObject::fromObject()
	 */
	public function doFromObject($source_object, KontorolDetachedResponseProfile $responseProfile = null)
	{
	    parent::doFromObject($source_object, $responseProfile);
	    
	    /* @var $source_object kBulkUploadFilterJobData */
	    $this->filter = null;
	    switch (get_class($source_object->getFilter()))
	    {
	        case 'categoryEntryFilter':
	            $this->filter = new KontorolCategoryEntryFilter();
	            break;
	        case 'entryFilter':
	            $this->filter = new KontorolBaseEntryFilter();
	            break;
		case 'UserEntryFilter':
	  	    $this->filter = new KontorolUserEntryFilter();
		    break;
	        default:
	            break;
	    }
	    
	    if ($this->filter)
	    {
	        $this->filter->fromObject($source_object->getFilter());
	    }       
	    
	   	$this->templateObject = null;
	   	
	    switch (get_class($source_object->getTemplateObject()))
	    {
	        case 'categoryEntry':
	            $this->templateObject = new KontorolCategoryEntry();
	            break;
	        default:
	            break;
	    }
	    
	    if ($this->templateObject)
	    {
	        $this->templateObject->fromObject($source_object->getTemplateObject());
	    }       
	}
	
	public function toInsertableObject($object_to_fill = null , $props_to_skip = array())
	{
	    $dbObj = parent::toInsertableObject($object_to_fill, $props_to_skip);
	    
	    $this->setType();
	    
	    return $dbObj;
	}
	
	public function setType ()
	{
	    $this->type = kPluginableEnumsManager::coreToApi("KontorolBulkUploadType", BulkUploadFilterPlugin::getApiValue(BulkUploadFilterType::FILTER));
	}
}

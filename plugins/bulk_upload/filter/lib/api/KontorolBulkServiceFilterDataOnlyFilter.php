<?php

/**
 * Represents the Bulk service input for filter bulk upload
 * @package plugins.bulkUploadFilter
 * @subpackage api.objects
 */
class KontorolBulkServiceFilterDataBase extends KontorolBulkServiceData
{
	/**
	 * Filter for extracting the objects list to upload
	 * @var KontorolFilter
	 */
	public $filter;


	public function getType ()
	{
		return kPluginableEnumsManager::apiToCore("BulkUploadType", BulkUploadFilterPlugin::getApiValue(BulkUploadFilterType::FILTER));
	}

	public function toBulkUploadJobData(KontorolBulkUploadJobData $jobData)
	{
		$jobData->filter = $this->filter;
	}
}

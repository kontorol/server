<?php

/**
 * Represents the Bulk service input for filter bulk upload
 * @package plugins.bulkUploadFilter
 * @subpackage api.objects
 */
class KontorolBulkServiceFilterData extends KontorolBulkServiceFilterDataBase
{
	/**
	 * Template object for new object creation
	 * @var KontorolObject
	 */
	public $templateObject;

	
	public function toBulkUploadJobData(KontorolBulkUploadJobData $jobData)
	{
		$jobData->filter = $this->filter;
		$jobData->templateObject = $this->templateObject;
	}
}

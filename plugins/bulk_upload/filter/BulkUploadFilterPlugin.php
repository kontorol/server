<?php
/**
 * @package plugins.bulkUploadFilter
 */
class BulkUploadFilterPlugin extends KontorolPlugin implements IKontorolBulkUpload, IKontorolPending
{
	const PLUGIN_NAME = 'bulkUploadFilter';
	
	/**
	 *
	 * Returns the plugin name
	 */
	public static function getPluginName()
	{
		return self::PLUGIN_NAME;
	}

	/* (non-PHPdoc)
	 * @see IKontorolPending::dependsOn()
	 */
	public static function dependsOn()
	{
		$bulkUploadDependency = new KontorolDependency(BulkUploadPlugin::PLUGIN_NAME);
		
		$bulkUploadXmlDependency = new KontorolDependency(BulkUploadXmlPlugin::PLUGIN_NAME);
		
		return array($bulkUploadDependency, $bulkUploadXmlDependency);
	}
	
	/**
	 * @return array<string> list of enum classes names that extend the base enum name
	 */
	public static function getEnums($baseEnumName = null)
	{
		if(is_null($baseEnumName))
			return array('BulkUploadFilterType', 'BulkUploadJobObjectType');
	
		if($baseEnumName == 'BulkUploadType')
			return array('BulkUploadFilterType');
		
		if ($baseEnumName == 'BulkUploadObjectType')
			return array('BulkUploadJobObjectType');
		
		return array();
	}
	
	/**
	 * @param string $baseClass
	 * @param string $enumValue
	 * @param array $constructorArgs
	 * @return object
	 */
	public static function loadObject($baseClass, $enumValue, array $constructorArgs = null)
	{
		 //Gets the right job for the engine
		if($baseClass == 'kBulkUploadJobData' && (!$enumValue || $enumValue == self::getBulkUploadTypeCoreValue(BulkUploadFilterType::FILTER)))
			return new kBulkUploadFilterJobData();
		
		 //Gets the right job for the engine
		if($baseClass == 'KontorolBulkUploadJobData' && (!$enumValue || $enumValue == self::getBulkUploadTypeCoreValue(BulkUploadFilterType::FILTER)))
			return new KontorolBulkUploadFilterJobData();
			
		 //Gets the service data for the engine
//		if($baseClass == 'KontorolBulkServiceData' && (!$enumValue || $enumValue == self::getBulkUploadTypeCoreValue(BulkUploadFilterType::FILTER)))
//			return new KontorolBulkServiceFilterData();
			
		
		//Gets the engine (only for clients)
		if($baseClass == 'KBulkUploadEngine' && class_exists('KontorolClient') && (!$enumValue || $enumValue == KontorolBulkUploadType::FILTER))
		{
			list($job) = $constructorArgs;
			/* @var $job KontorolBatchJob */
			switch ($job->data->bulkUploadObjectType)
			{
			    case KontorolBulkUploadObjectType::CATEGORY_ENTRY:
			        return new BulkUploadCategoryEntryEngineFilter($job);
			    case KontorolBulkUploadObjectType::USER_ENTRY:
				return new BulkUploadUserEntryEngineFilter($job);
			    case KontorolBulkUploadObjectType::ENTRY:
				return new BulkUploadMediaEntryEngineFilter($job);
			    default:
			        throw new KontorolException("Bulk upload object type [{$job->data->bulkUploadObjectType}] not found", KontorolBatchJobAppErrors::ENGINE_NOT_FOUND);
			        break;
			}
			
		}
				
		return null;
	}

	/* (non-PHPdoc)
	 * @see IKontorolObjectLoader::getObjectClass()
	 */
	public static function getObjectClass($baseClass, $enumValue)
	{
		// BulkUploadResultPeer::OM_CLASS = 'BulkUploadResult'
		if ($baseClass == 'BulkUploadResult' && $enumValue == self::getBulkUploadObjectTypeCoreValue(BulkUploadJobObjectType::JOB))
		{
			return 'BulkUploadResultJob';
		}
		
		return null;
	}
	
	/**
	 * Returns the correct file extension for bulk upload type
	 * @param int $enumValue code API value
	 */
	public static function getFileExtension($enumValue)
	{
		if($enumValue == self::getBulkUploadTypeCoreValue(BulkUploadFilterType::FILTER))
			return null;
	}
	
	
	/**
	 * Returns the log file for bulk upload job
	 * @param BatchJob $batchJob bulk upload batchjob
	 */
	public static function writeBulkUploadLogFile($batchJob)
	{
		if($batchJob->getJobSubType() && ($batchJob->getJobSubType() != self::getBulkUploadTypeCoreValue(BulkUploadFilterType::FILTER))){
			return;
		}
		//TODO:
		header("Content-Type: text/plain; charset=UTF-8");
				$criteria = new Criteria();
		$criteria->add(BulkUploadResultPeer::BULK_UPLOAD_JOB_ID, $batchJob->getId());
		$criteria->addAscendingOrderByColumn(BulkUploadResultPeer::LINE_INDEX);
		$criteria->setLimit(100);
		$bulkUploadResults = BulkUploadResultPeer::doSelect($criteria);
		
		if(!count($bulkUploadResults))
			die("Log file is not ready");
			
		$STDOUT = fopen('php://output', 'w');
		$data = $batchJob->getData();
        /* @var $data kBulkUploadFilterJobData */		
		$handledResults = 0;
		while(count($bulkUploadResults))
		{
			$handledResults += count($bulkUploadResults);
			foreach($bulkUploadResults as $bulkUploadResult)
			{				
	            $values = array();
	            $values['bulkUploadResultStatus'] = $bulkUploadResult->getStatus();
				$values['objectId'] = $bulkUploadResult->getObjectId();
				$values['objectStatus'] = $bulkUploadResult->getObjectStatus();
				$values['errorDescription'] = preg_replace('/[\n\r\t]/', ' ', $bulkUploadResult->getErrorDescription());
					
				fwrite($STDOUT, print_r($values,true));
			}
			
    		if(count($bulkUploadResults) < $criteria->getLimit())
    			break;
	    		
    		kMemoryManager::clearMemory();
    		$criteria->setOffset($handledResults);
			$bulkUploadResults = BulkUploadResultPeer::doSelect($criteria);
		}
		fclose($STDOUT);
		
		kFile::closeDbConnections();
		exit;
	}
	
	/**
	 * @return int id of dynamic enum in the DB.
	 */
	public static function getBulkUploadTypeCoreValue($valueName)
	{
		$value = self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
		return kPluginableEnumsManager::apiToCore('BulkUploadType', $value);
	}
	
	/**
	 * @return int id of dynamic enum in the DB.
	 */
	public static function getBulkUploadObjectTypeCoreValue($valueName)
	{
		$value = self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
		return kPluginableEnumsManager::apiToCore('BulkUploadObjectType', $value);
	}
	
	/**
	 * @return string external API value of dynamic enum.
	 */
	public static function getApiValue($valueName)
	{
		return self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
	}

}

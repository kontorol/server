<?php
/**
 * This engine supports deletion of user entries based on the input filter
 *
 * @package plugins.bulkUploadFilter
 * @subpackage batch
 */
class BulkUploadUserEntryEngineFilter extends BulkUploadEngineFilter
{
	const OBJECT_TYPE_TITLE = 'user entry';

	/* get a list of objects according to the input filter
	 *
	 * @see BulkUploadEngineFilter::listObjects()
	 */
	protected function listObjects(KontorolFilter $filter, KontorolFilterPager $pager = null)
	{
		if($filter instanceof KontorolUserEntryFilter)
		{
			return KBatchBase::$kClient->userEntry->listAction($filter, $pager);
		}

		else
		{
			throw new KontorolBatchException("Unsupported filter: {get_class($filter)}", KontorolBatchJobAppErrors::BULK_VALIDATION_FAILED);
		}
	}

	protected function createObjectFromResultAndJobData (KontorolBulkUploadResult $bulkUploadResult)
	{
		//in bulk delete, there is no need to create objects from bulk upload result.
		return;
	}

	protected function deleteObjectFromResult (KontorolBulkUploadResult $bulkUploadResult)
	{
		return KBatchBase::$kClient->userEntry->delete($bulkUploadResult->userEntryId);
	}

	/**
	 * create specific instance of BulkUploadResult and set it's properties
	 * @param $object - Result is being created from KontorolUserEntry
	 *
	 * @see BulkUploadEngineFilter::fillUploadResultInstance()
	 */
	protected function fillUploadResultInstance ($object)
	{
		$bulkUploadResult = new KontorolBulkUploadResultUserEntry();
		if($object instanceof KontorolUserEntry)
		{
			//get user entry object based on the entry details
			$filter = new KontorolUserEntryFilter();
			$filter->idEqual = $object->id;
			$filter->userIdEqual = $object->userId;
			$filter->partnerId = $object->partnerId;
			$list = $this->listObjects($filter);
			if(count($list->objects))
			{
				$userEntry = reset($list->objects);
			}
		}
		if($userEntry)
		{
			$bulkUploadResult->objectId = $userEntry->id;
			$bulkUploadResult->objectStatus = $userEntry->status;
			$bulkUploadResult->userEntryId = $userEntry->id;
			$bulkUploadResult->action = KontorolBulkUploadAction::DELETE;
		}
		return $bulkUploadResult;
	}

	protected function getBulkUploadResultObjectType()
	{
		return KontorolBulkUploadObjectType::USER_ENTRY;
	}

	public function getObjectTypeTitle()
	{
		return self::OBJECT_TYPE_TITLE;
	}

}

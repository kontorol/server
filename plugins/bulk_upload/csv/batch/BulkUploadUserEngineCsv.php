<?php
/**
 * Class which parses the bulk upload CSV and creates the objects listed in it. 
 * This engine class parses CSVs which describe users.
 * 
 * @package plugins.bulkUploadCsv
 * @subpackage batch
 */
class BulkUploadUserEngineCsv extends BulkUploadEngineCsv
{
	const OBJECT_TYPE_TITLE = 'user';
	private $groupActionsList;

	public function __construct(KontorolBatchJob $job)
	{
		parent::__construct($job);
		$this->groupActionsList = array();
	}

	/**
     * (non-PHPdoc)
     * @see BulkUploadGeneralEngineCsv::createUploadResult()
     */
    protected function createUploadResult($values, $columns)
	{
		$bulkUploadResult = parent::createUploadResult($values, $columns);
		if (!$bulkUploadResult)
			return;

		$bulkUploadResult->bulkUploadResultObjectType = KontorolBulkUploadObjectType::USER;

		// trim the values
		array_walk($values, array('BulkUploadUserEngineCsv', 'trimArray'));

		// sets the result values
		$dateOfBirth = null;

		foreach($columns as $index => $column)
		{
			if(!is_numeric($index))
				continue;

			if ($column == 'dateOfBirth')
			{
			    $dateOfBirth = $values[$index];
			}

			if(iconv_strlen($values[$index], 'UTF-8'))
			{
				$bulkUploadResult->$column = $values[$index];
				KontorolLog::info("Set value $column [{$bulkUploadResult->$column}]");
			}
			else
			{
				KontorolLog::info("Value $column is empty");
			}
		}

		if(isset($columns['plugins']))
		{
			$bulkUploadPlugins = array();

			foreach($columns['plugins'] as $index => $column)
			{
				$bulkUploadPlugin = new KontorolBulkUploadPluginData();
				$bulkUploadPlugin->field = $column;
				$bulkUploadPlugin->value = iconv_strlen($values[$index], 'UTF-8') ? $values[$index] : null;
				$bulkUploadPlugins[] = $bulkUploadPlugin;

				KontorolLog::info("Set plugin value $column [{$bulkUploadPlugin->value}]");
			}

			$bulkUploadResult->pluginsData = $bulkUploadPlugins;
		}

		$bulkUploadResult->objectStatus = KontorolUserStatus::ACTIVE;
		$bulkUploadResult->status = KontorolBulkUploadResultStatus::IN_PROGRESS;

		if (!$bulkUploadResult->action)
		{
		    $bulkUploadResult->action = KontorolBulkUploadAction::ADD;
		}

		$bulkUploadResult = $this->validateBulkUploadResult($bulkUploadResult, $dateOfBirth);
		if($bulkUploadResult)
			$this->bulkUploadResults[] = $bulkUploadResult;
	}

	protected function validateBulkUploadResult (KontorolBulkUploadResult $bulkUploadResult, $dateOfBirth)
	{
	    /* @var $bulkUploadResult KontorolBulkUploadResultUser */
		if (!$bulkUploadResult->userId)
		{
		    $bulkUploadResult->status = KontorolBulkUploadResultStatus::ERROR;
			$bulkUploadResult->errorType = KontorolBatchJobErrorTypes::APP;
			$bulkUploadResult->errorDescription = 'Mandatory Column [userId] missing from CSV.';
		}

		if ($dateOfBirth && !self::isFormatedDate($dateOfBirth, true))
		{
		    $bulkUploadResult->status = KontorolBulkUploadResultStatus::ERROR;
			$bulkUploadResult->errorType = KontorolBatchJobErrorTypes::APP;
			$bulkUploadResult->errorDescription = "Format of property dateOfBirth is incorrect [$dateOfBirth].";
		}

		if ($bulkUploadResult->gender && !self::isValidEnumValue("KontorolGender", $bulkUploadResult->gender))
		{
		    $bulkUploadResult->status = KontorolBulkUploadResultStatus::ERROR;
			$bulkUploadResult->errorType = KontorolBatchJobErrorTypes::APP;
			$bulkUploadResult->errorDescription = "Wrong value passed for property gender [$bulkUploadResult->gender]";
		}

		if ($bulkUploadResult->action == KontorolBulkUploadAction::ADD_OR_UPDATE)
		{
		    KBatchBase::impersonate($this->currentPartnerId);;
		    try
		    {
		        $user = KBatchBase::$kClient->user->get($bulkUploadResult->userId);
    		    if ( $user )
    		    {
    		        $bulkUploadResult->action = KontorolBulkUploadAction::UPDATE;
    		    }
		    }
	        catch (Exception $e)
	        {
	            $bulkUploadResult->action = KontorolBulkUploadAction::ADD;
		    }
		    KBatchBase::unimpersonate();
		}


		if($this->maxRecords && $this->lineNumber > $this->maxRecords) // check max records
		{
			$bulkUploadResult->status = KontorolBulkUploadResultStatus::ERROR;
			$bulkUploadResult->errorType = KontorolBatchJobErrorTypes::APP;
			$bulkUploadResult->errorDescription = "Exeeded max records count per bulk";
		}

		if($bulkUploadResult->status == KontorolBulkUploadResultStatus::ERROR)
		{
			$this->addBulkUploadResult($bulkUploadResult);
			return null;
		}

		$bulkUploadResult->dateOfBirth = self::parseFormatedDate($bulkUploadResult->dateOfBirth, true);

		return $bulkUploadResult;
	}


    protected function addBulkUploadResult(KontorolBulkUploadResult $bulkUploadResult)
	{
		parent::addBulkUploadResult($bulkUploadResult);

	}
	/**
	 *
	 * Create the entries from the given bulk upload results
	 */
	protected function createObjects()
	{
		// start a multi request for add entries
		KBatchBase::$kClient->startMultiRequest();

		KontorolLog::info("job[{$this->job->id}] start creating users");
		$bulkUploadResultChunk = array(); // store the results of the created entries

		foreach($this->bulkUploadResults as $bulkUploadResult)
		{
			$this->addGroupUser($bulkUploadResult);

			/* @var $bulkUploadResult KontorolBulkUploadResultUser */
		    KontorolLog::info("Handling bulk upload result: [". $bulkUploadResult->userId ."]");
		    switch ($bulkUploadResult->action)
		    {
		        case KontorolBulkUploadAction::ADD:
    		        $user = $this->createUserFromResultAndJobData($bulkUploadResult);

        			$bulkUploadResultChunk[] = $bulkUploadResult;

        			KBatchBase::impersonate($this->currentPartnerId);;
        			KBatchBase::$kClient->user->add($user);
        			KBatchBase::unimpersonate();

		            break;

		        case KontorolBulkUploadAction::UPDATE:
		            $category = $this->createUserFromResultAndJobData($bulkUploadResult);

        			$bulkUploadResultChunk[] = $bulkUploadResult;

        			KBatchBase::impersonate($this->currentPartnerId);;
        			KBatchBase::$kClient->user->update($bulkUploadResult->userId, $category);
        			KBatchBase::unimpersonate();


		            break;

		        case KontorolBulkUploadAction::DELETE:
		            $bulkUploadResultChunk[] = $bulkUploadResult;

        			KBatchBase::impersonate($this->currentPartnerId);;
        			KBatchBase::$kClient->user->delete($bulkUploadResult->userId);
        			KBatchBase::unimpersonate();

		            break;

		        default:
		            $bulkUploadResult->status = KontorolBulkUploadResultStatus::ERROR;
		            $bulkUploadResult->errorDescription = "Unknown action passed: [".$bulkUploadResult->action ."]";
		            break;
		    }

		    if(KBatchBase::$kClient->getMultiRequestQueueSize() >= $this->multiRequestSize)
			{
				// make all the media->add as the partner
				$requestResults = KBatchBase::$kClient->doMultiRequest();

				$this->multiUpdateResults($requestResults, $bulkUploadResultChunk);
				$this->checkAborted();
				KBatchBase::$kClient->startMultiRequest();
				$bulkUploadResultChunk = array();
			}
		}

		// make all the category actions as the partner
		$requestResults = KBatchBase::$kClient->doMultiRequest();

		if($requestResults && count($requestResults))
			$this->updateObjectsResults($requestResults, $bulkUploadResultChunk);


		$this->handleAllGroups();

		KontorolLog::info("job[{$this->job->id}] finish modifying users");
	}

	/**
	 * Function to create a new user from bulk upload result.
	 * @param KontorolBulkUploadResultUser $bulkUploadUserResult
	 */
	protected function createUserFromResultAndJobData (KontorolBulkUploadResultUser $bulkUploadUserResult)
	{
	    $user = new KontorolUser();
	    //Prepare object
	    if ($bulkUploadUserResult->userId)
	        $user->id = $bulkUploadUserResult->userId;

	    if ($bulkUploadUserResult->screenName)
	        $user->screenName = $bulkUploadUserResult->screenName;

	    if ($bulkUploadUserResult->tags)
	        $user->tags = $bulkUploadUserResult->tags;

	    if ($bulkUploadUserResult->firstName)
	        $user->firstName = $bulkUploadUserResult->firstName;

	    if ($bulkUploadUserResult->lastName)
	        $user->lastName = $bulkUploadUserResult->lastName;

	    if ($bulkUploadUserResult->email)
	        $user->email = $bulkUploadUserResult->email;

	    if ($bulkUploadUserResult->city)
	        $user->city = $bulkUploadUserResult->city;

	    if ($bulkUploadUserResult->country)
	        $user->country = $bulkUploadUserResult->country;

	    if ($bulkUploadUserResult->state)
	        $user->state = $bulkUploadUserResult->state;

	    if ($bulkUploadUserResult->zip)
	        $user->zip = $bulkUploadUserResult->zip;

	    if ($bulkUploadUserResult->gender)
	        $user->gender = $bulkUploadUserResult->gender;

	    if ($bulkUploadUserResult->dateOfBirth)
	        $user->dateOfBirth = $bulkUploadUserResult->dateOfBirth;

	    if ($bulkUploadUserResult->partnerData)
	        $user->partnerData = $bulkUploadUserResult->partnerData;

	    return $user;
	}

	/**
	 *
	 * Gets the columns for V1 csv file
	 */
	protected function getColumns()
	{
		return array(
		    "action",
		    "userId",
		    "screenName",
		    "firstName",
		    "lastName",
		    "email",
		    "tags",
		    "gender",
		    "zip",
		    "country",
		    "state",
			"city",
		    "dateOfBirth",
			"partnerData",
			"group",
			"userRole"
		);
	}


    protected function updateObjectsResults(array $requestResults, array $bulkUploadResults)
	{
		KontorolLog::info("Updating " . count($requestResults) . " results");
		$dummy=array();
		// checking the created entries
		foreach($requestResults as $index => $requestResult)
		{
			$bulkUploadResult = $bulkUploadResults[$index];
			$this->handleMultiRequest($dummy);
			if(is_array($requestResult) && isset($requestResult['code']))
			{
			    $bulkUploadResult->status = KontorolBulkUploadResultStatus::ERROR;
				$bulkUploadResult->errorType = KontorolBatchJobErrorTypes::KONTOROL_API;
				$bulkUploadResult->objectStatus = $requestResult['code'];
				$bulkUploadResult->errorDescription = $requestResult['message'];
				$this->addBulkUploadResult($bulkUploadResult);
				continue;
			}

			if($requestResult instanceof Exception)
			{
				$bulkUploadResult->status = KontorolBulkUploadResultStatus::ERROR;
				$bulkUploadResult->errorType = KontorolBatchJobErrorTypes::KONTOROL_API;
				$bulkUploadResult->errorDescription = $requestResult->getMessage();
				$this->addBulkUploadResult($bulkUploadResult);
				continue;
			}

			$this->addBulkUploadResult($bulkUploadResult);
		}
		$this->handleMultiRequest($dummy,true);
	}


	private function addGroupUser(KontorolBulkUploadResultUser $userResult)
	{
		if($userResult->group)
			$this->groupActionsList[]=$userResult;
	}

	private function getGroupActionList(&$usersToAddList,&$userGroupToDeleteMap)
	{
		foreach ($this->groupActionsList as $group)
		{
			if (strpos($group->group, "-") !== 0)
			{
				$usersToAddList[] = $group;
			}
			else
			{
				$userGroupToDeleteMap[] = array($group->userId, substr($group->group, 1));
			}
		}
	}

	private function getUsers($usersList)
	{
		$ret = array();
		foreach ($usersList as $group)
		{
			$this->handleMultiRequest($ret);
			KBatchBase::$kClient->user->get($group->group);
		}
		$this->handleMultiRequest($ret,true);
		return $ret;
	}

	private function deleteUsers($usersMap)
	{
		$ret = array();
		foreach ($usersMap as $user)
		{
			list($userId,$group) = $user;
			$this->handleMultiRequest($ret);
			KBatchBase::$kClient->groupUser->delete($userId, $group);
		}
		$this->handleMultiRequest($ret,true);
		return $ret;
	}

	private function multiUpdateResults($results , $bulkUploadRequest)
	{
		KBatchBase::unimpersonate();
		$this->updateObjectsResults($results,$bulkUploadRequest);
		KBatchBase::impersonate($this->currentPartnerId);
	}

	private function addUserOfTypeGroup($actualGroupUsersList , $expectedGroupUsersList)
	{
		$ret=array();
		foreach ($actualGroupUsersList as $index => $user)
		{
			//check if value does not exist
			if( !($user instanceof KontorolUser)  ||  ($user->type != KontorolUserType::GROUP))
			{
				$this->handleMultiRequest($ret);
				KontorolLog::debug("Adding User of type group" . $expectedGroupUsersList[$index]->group );
				$groupUser = new KontorolUser();
				$groupUser->id = $expectedGroupUsersList[$index]->group;
				$groupUser->type = KontorolUserType::GROUP;
				KBatchBase::$kClient->user->add($groupUser);
			}
		}
		$this->handleMultiRequest($ret,true);
		return $ret;
	}

	private function handleMultiRequest(&$ret,$finish=false)
	{
		$count = KBatchBase::$kClient->getMultiRequestQueueSize();
		//Start of new multi request session
		if($count)
		{
			if (($count % $this->multiRequestSize) == 0 || $finish)
			{
				$result = KBatchBase::$kClient->doMultiRequest();
				if (count($result))
					$ret = array_merge($ret, $result);
				if (!$finish)
					KBatchBase::$kClient->startMultiRequest();
			}
		}
		elseif (!$finish)
		{
			KBatchBase::$kClient->startMultiRequest();
		}
	}

	private function addGroupUsers($groupUsersList)
	{
		$ret = array();
		foreach ($groupUsersList as $groupUserParams)
		{
			$this->handleMultiRequest($ret);
			$groupUser = new KontorolGroupUser();
			$groupUser->userId = $groupUserParams->userId;
			$groupUser->groupId = $groupUserParams->group;
			$groupUser->creationMode = KontorolGroupUserCreationMode::AUTOMATIC;
			if (isset($groupUserParams->userRole))
			{
				$groupUser->userRole = $groupUserParams->userRole;
			}
			KBatchBase::$kClient->groupUser->add($groupUser);
		}
		$this->handleMultiRequest($ret,true);
		return $ret;
	}

	private function handleAllGroups()
	{
		KontorolLog::info("Handling user/group association");
		KBatchBase::impersonate($this->currentPartnerId);
		$userGroupToDeleteMap = array();
		$groupUsersToAddList= array();
		$this->multiRequestSize = 100;
		$this->getGroupActionList($groupUsersToAddList,$userGroupToDeleteMap);
		$this->deleteUsers($userGroupToDeleteMap);
		if(count($groupUsersToAddList))
		{
			$requestResults = $this->getUsers($groupUsersToAddList);
			$this->addUserOfTypeGroup($requestResults, $groupUsersToAddList);

			$ret = $this->addGroupUsers($groupUsersToAddList);
			$this->multiUpdateResults($ret, $groupUsersToAddList);
		}
		KBatchBase::unimpersonate();
	}

	protected function getUploadResultInstance ()
	{
	    return new KontorolBulkUploadResultUser();
	}

	protected function getUploadResultInstanceType()
	{
		return KontorolBulkUploadObjectType::USER;
	}

	public function getObjectTypeTitle()
	{
		return self::OBJECT_TYPE_TITLE;
	}
}

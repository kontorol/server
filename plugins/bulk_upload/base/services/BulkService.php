<?php
/**
 * Bulk upload service is used to upload & manage bulk uploads
 *
 * @service bulk
 * @package plugins.bulkUpload
 * @subpackage services
 */
class BulkService extends KontorolBaseService
{
	const PARTNER_DEFAULT_CONVERSION_PROFILE_ID = -1;
	
	const SERVICE_NAME = "bulkUpload";

	/**
	 * Add new bulk upload batch job
	 * Conversion profile id can be specified in the API or in the CSV file, the one in the CSV file will be stronger.
	 * If no conversion profile was specified, partner's default will be used
	 * 
	 * @action addEntries
	 * @actionAlias media.bulkUploadAdd
	 * @param file $fileData
	 * @param KontorolBulkUploadType $bulkUploadType
	 * @param KontorolBulkUploadJobData $bulkUploadData
	 * @return KontorolBulkUpload
	 */
	function addEntriesAction($fileData, KontorolBulkUploadJobData $bulkUploadData = null, KontorolBulkUploadEntryData $bulkUploadEntryData = null)
	{
		if(get_class($bulkUploadData) == 'KontorolBulkUploadJobData')
			throw new KontorolAPIException(KontorolErrors::OBJECT_TYPE_ABSTRACT, 'KontorolBulkUploadJobData');

		$validContent = myXmlUtils::validateXmlFileContent($fileData['tmp_name']);
		if(!$validContent)
		{
			throw new KontorolAPIException(KontorolErrors::FILE_CONTENT_NOT_SECURE);
		}


	    if($bulkUploadEntryData->conversionProfileId == self::PARTNER_DEFAULT_CONVERSION_PROFILE_ID)
			$bulkUploadEntryData->conversionProfileId = $this->getPartner()->getDefaultConversionProfileId();
	    
	    if (!$bulkUploadData)
	    {
	       $bulkUploadData = KontorolPluginManager::loadObject('KontorolBulkUploadJobData', null);
	    }
	    
	    if (!$bulkUploadEntryData)
	    {
	        $bulkUploadEntryData = new KontorolBulkUploadEntryData();
	    }
		if(!$bulkUploadData->fileName)
			$bulkUploadData->fileName = $fileData["name"];
		
		$dbBulkUploadJobData = $bulkUploadData->toInsertableObject();
		$bulkUploadCoreType = kPluginableEnumsManager::apiToCore("BulkUploadType", $bulkUploadData->type);
		/* @var $dbBulkUploadJobData kBulkUploadJobData */
		$dbBulkUploadJobData->setBulkUploadObjectType(BulkUploadObjectType::ENTRY);
		$dbBulkUploadJobData->setUserId($this->getKuser()->getPuserId());
		$dbObjectData = $bulkUploadEntryData->toInsertableObject();
		$dbBulkUploadJobData->setObjectData($dbObjectData);
		$dbBulkUploadJobData->setFilePath($fileData["tmp_name"]);
		
		$dbJob = kJobsManager::addBulkUploadJob($this->getPartner(), $dbBulkUploadJobData, $bulkUploadCoreType);
		$dbJobLog = BatchJobLogPeer::retrieveByBatchJobId($dbJob->getId());
		if(!$dbJobLog)
			return null;
			
		$bulkUpload = new KontorolBulkUpload();
		$bulkUpload->fromObject($dbJobLog, $this->getResponseProfile());
		
		return $bulkUpload;
	}
	
	/**
	 * @action addCategories
	 * @actionAlias category.addFromBulkUpload
	 * 
	 * Action adds categories from a bulkupload CSV file
	 * @param file $fileData
	 * @param KontorolBulkUploadJobData $bulkUploadData
	 * @param KontorolBulkUploadCategoryData $bulkUploadCategoryData
	 * @return KontorolBulkUpload
	 */
	public function addCategoriesAction ($fileData, KontorolBulkUploadJobData $bulkUploadData = null, KontorolBulkUploadCategoryData $bulkUploadCategoryData = null)
	{
	    if (!$bulkUploadData)
	    {
	       $bulkUploadData = KontorolPluginManager::loadObject('KontorolBulkUploadJobData', null);
	    }
	    
	    if (!$bulkUploadCategoryData)
	    {
	        $bulkUploadCategoryData = new KontorolBulkUploadCategoryData();
	    }
	    
		if(!$bulkUploadData->fileName)
			$bulkUploadData->fileName = $fileData["name"];
		
		$dbBulkUploadJobData = $bulkUploadData->toInsertableObject();
		$bulkUploadCoreType = kPluginableEnumsManager::apiToCore("BulkUploadType", $bulkUploadData->type);
		
		$dbBulkUploadJobData->setBulkUploadObjectType(BulkUploadObjectType::CATEGORY);
		$dbBulkUploadJobData->setUserId($this->getKuser()->getPuserId());
		$dbObjectData = $bulkUploadCategoryData->toInsertableObject();
		$dbBulkUploadJobData->setObjectData($dbObjectData);
		$dbBulkUploadJobData->setFilePath($fileData["tmp_name"]);
		
		$dbJob = kJobsManager::addBulkUploadJob($this->getPartner(), $dbBulkUploadJobData, $bulkUploadCoreType);
		$dbJobLog = BatchJobLogPeer::retrieveByBatchJobId($dbJob->getId());
		if(!$dbJobLog)
			return null;
		
		$bulkUpload = new KontorolBulkUpload();
		$bulkUpload->fromObject($dbJobLog, $this->getResponseProfile());
		
		return $bulkUpload;
	}
	
	/**
	 * @action addCategoryUsers
	 * @actionAlias categoryUser.addFromBulkUpload
	 * Action adds CategoryUsers from a bulkupload CSV file
	 * @param file $fileData
	 * @param KontorolBulkUploadJobData $bulkUploadData
	 * @param KontorolBulkUploadCategoryUserData $bulkUploadCategoryUserData
	 * @return KontorolBulkUpload
	 */
	public function addCategoryUsersAction ($fileData, KontorolBulkUploadJobData $bulkUploadData = null, KontorolBulkUploadCategoryUserData $bulkUploadCategoryUserData = null)
	{
	    if (!$bulkUploadData)
	    {
	       $bulkUploadData = KontorolPluginManager::loadObject('KontorolBulkUploadJobData', null);
	    }
	    
        if (!$bulkUploadCategoryUserData)
        {
            $bulkUploadCategoryUserData = new KontorolBulkUploadCategoryUserData();
        }
		
		if(!$bulkUploadData->fileName)
			$bulkUploadData->fileName = $fileData["name"];
		
		$dbBulkUploadJobData = $bulkUploadData->toInsertableObject();
		$bulkUploadCoreType = kPluginableEnumsManager::apiToCore("BulkUploadType", $bulkUploadData->type);
		$dbBulkUploadJobData->setBulkUploadObjectType(BulkUploadObjectType::CATEGORY_USER);
		$dbBulkUploadJobData->setUserId($this->getKuser()->getPuserId());
		$dbObjectData = $bulkUploadCategoryUserData->toInsertableObject();
		$dbBulkUploadJobData->setObjectData($dbObjectData);
		$dbBulkUploadJobData->setFilePath($fileData["tmp_name"]);
		
		$dbJob = kJobsManager::addBulkUploadJob($this->getPartner(), $dbBulkUploadJobData, $bulkUploadCoreType);
		$dbJobLog = BatchJobLogPeer::retrieveByBatchJobId($dbJob->getId());
		if(!$dbJobLog)
			return null;
		
		$bulkUpload = new KontorolBulkUpload();
		$bulkUpload->fromObject($dbJobLog, $this->getResponseProfile());
		
		return $bulkUpload;
	}
	
	/**
	 * @action addUsers
	 * @actionAlias user.addFromBulkUpload
	 * Action adds users from a bulkupload CSV file
	 * @param file $fileData
	 * @param KontorolBulkUploadJobData $bulkUploadData
	 * @param KontorolBulkUploadUserData $bulkUploadUserData
	 * @return KontorolBulkUpload
	 */
	public function addUsersAction($fileData, KontorolBulkUploadJobData $bulkUploadData = null, KontorolBulkUploadUserData $bulkUploadUserData = null)
	{
	   if (!$bulkUploadData)
	   {
	       $bulkUploadData = KontorolPluginManager::loadObject('KontorolBulkUploadJobData', null);
	   }
	   
	   if (!$bulkUploadUserData)
	   {
	       $bulkUploadUserData = new KontorolBulkUploadUserData();
	   }
		
		if(!$bulkUploadData->fileName)
			$bulkUploadData->fileName = $fileData["name"];
		
		$dbBulkUploadJobData = $bulkUploadData->toInsertableObject();
		$bulkUploadCoreType = kPluginableEnumsManager::apiToCore("BulkUploadType", $bulkUploadData->type);
		$dbBulkUploadJobData->setBulkUploadObjectType(BulkUploadObjectType::USER);
		$dbBulkUploadJobData->setUserId($this->getKuser()->getPuserId());
		$dbObjectData = $bulkUploadUserData->toInsertableObject();
		$dbBulkUploadJobData->setObjectData($dbObjectData);
		$dbBulkUploadJobData->setFilePath($fileData["tmp_name"]);
		
		$dbJob = kJobsManager::addBulkUploadJob($this->getPartner(), $dbBulkUploadJobData, $bulkUploadCoreType);
		$dbJobLog = BatchJobLogPeer::retrieveByBatchJobId($dbJob->getId());
		if(!$dbJobLog)
			return null;
			
		$bulkUpload = new KontorolBulkUpload();
		$bulkUpload->fromObject($dbJobLog, $this->getResponseProfile());
		
		return $bulkUpload;
	}
	
	/**
	 * @action addCategoryEntries
	 * @actionAlias categoryEntry.addFromBulkUpload
	 * Action adds active category entries
	 * @param KontorolBulkServiceData $bulkUploadData
	 * @param KontorolBulkUploadCategoryEntryData $bulkUploadCategoryEntryData
	 * @return KontorolBulkUpload
	 */
	public function addCategoryEntriesAction (KontorolBulkServiceData $bulkUploadData, KontorolBulkUploadCategoryEntryData $bulkUploadCategoryEntryData = null)
	{
		if($bulkUploadData instanceof  KontorolBulkServiceFilterData){
			if($bulkUploadData->filter instanceof KontorolBaseEntryFilter){
				if(	$bulkUploadData->filter->idEqual == null &&
					$bulkUploadData->filter->idIn == null &&
					$bulkUploadData->filter->categoriesIdsMatchOr == null &&
					$bulkUploadData->filter->categoriesMatchAnd == null &&
					$bulkUploadData->filter->categoriesMatchOr == null &&
					$bulkUploadData->filter->categoriesIdsMatchAnd == null)
						throw new KontorolAPIException(KontorolErrors::MUST_FILTER_ON_ENTRY_OR_CATEGORY);
			}
			else if($bulkUploadData->filter instanceof KontorolCategoryEntryFilter){
				if(	$bulkUploadData->filter->entryIdEqual == null &&
					$bulkUploadData->filter->categoryIdIn == null &&
					$bulkUploadData->filter->categoryIdEqual == null )
						throw new KontorolAPIException(KontorolErrors::MUST_FILTER_ON_ENTRY_OR_CATEGORY);
			}
		}
	   	$bulkUploadJobData = KontorolPluginManager::loadObject('KontorolBulkUploadJobData', $bulkUploadData->getType());
	   	$bulkUploadData->toBulkUploadJobData($bulkUploadJobData);
	    
        if (!$bulkUploadCategoryEntryData)
        {
            $bulkUploadCategoryEntryData = new KontorolBulkUploadCategoryEntryData();
        }
				
		$dbBulkUploadJobData = $bulkUploadJobData->toInsertableObject();
		$bulkUploadCoreType = kPluginableEnumsManager::apiToCore("BulkUploadType", $bulkUploadJobData->type);
		$dbBulkUploadJobData->setBulkUploadObjectType(BulkUploadObjectType::CATEGORY_ENTRY);
		$dbBulkUploadJobData->setUserId($this->getKuser()->getPuserId());
		$dbObjectData = $bulkUploadCategoryEntryData->toInsertableObject();
		$dbBulkUploadJobData->setObjectData($dbObjectData);
		
		$dbJob = kJobsManager::addBulkUploadJob($this->getPartner(), $dbBulkUploadJobData, $bulkUploadCoreType);
		$dbJobLog = BatchJobLogPeer::retrieveByBatchJobId($dbJob->getId());
		if(!$dbJobLog)
			return null;
		
		$bulkUpload = new KontorolBulkUpload();
		$bulkUpload->fromObject($dbJobLog, $this->getResponseProfile());
		
		return $bulkUpload;
	}
	/**
	 * Get bulk upload batch job by id
	 *
	 * @action get
	 * @param int $id
	 * @return KontorolBulkUpload
	 */
	function getAction($id)
	{
	    $c = new Criteria();
	    $c->addAnd(BatchJobLogPeer::JOB_ID, $id);
		$c->addAnd(BatchJobLogPeer::PARTNER_ID, $this->getPartnerId());
		$c->addAnd(BatchJobLogPeer::JOB_TYPE, BatchJobType::BULKUPLOAD);
		$batchJob = BatchJobLogPeer::doSelectOne($c);
		
		if (!$batchJob)
		    throw new KontorolAPIException(KontorolErrors::BULK_UPLOAD_NOT_FOUND, $id);
		    
		$ret = new KontorolBulkUpload();
		$ret->fromObject($batchJob, $this->getResponseProfile());
		return $ret;
	}
	
	/**
	 * List bulk upload batch jobs
	 *
	 * @action list
	 * @param KontorolBulkUploadFilter $bulkUploadFilter
	 * @param KontorolFilterPager $pager
	 * @return KontorolBulkUploadListResponse
	 */
	function listAction(KontorolBulkUploadFilter $bulkUploadFilter = null, KontorolFilterPager $pager = null)
	{
		if (!$bulkUploadFilter)
		$bulkUploadFilter = new KontorolBulkUploadFilter();
	    
		if (!$pager)
			$pager = new KontorolFilterPager();
		
 		$response = new KontorolBulkUploadListResponse();

		$coreBulkUploadFilter = new BatchJobLogFilter();
        	$bulkUploadFilter->toObject($coreBulkUploadFilter);
			
	    	$c = new Criteria();
		
		// when filtering the last hour logs, limit list to last 30K records in order to constrain query performance
		if ($bulkUploadFilter->uploadedOnGreaterThanOrEqual)// && $bulkUploadFilter->uploadedOnGreaterThanOrEqual > time() - 3600)
		{
			$c2 = new Criteria();
			$c2->addDescendingOrderByColumn(BatchJobLogPeer::ID);
			$lastLog = BatchJobLogPeer::doSelectOne($c2);
			if (!$lastLog)
				return $response;

			$lastId = $lastLog->getId() - 300000;
			$c->addAnd(BatchJobLogPeer::ID, $lastId, Criteria::GREATER_THAN);
		}
		
		$c->addAnd(BatchJobLogPeer::PARTNER_ID, $this->getPartnerId());
		$c->addAnd(BatchJobLogPeer::JOB_TYPE, BatchJobType::BULKUPLOAD);
		
		$crit = $c->getNewCriterion(BatchJobLogPeer::ABORT, null);
		$critOr = $c->getNewCriterion(BatchJobLogPeer::ABORT, 0);
		$crit->addOr($critOr);
		$c->add($crit);
		
		$c->addDescendingOrderByColumn(BatchJobLogPeer::ID);
		
		$coreBulkUploadFilter->attachToCriteria($c);
		$count = BatchJobLogPeer::doCount($c);
		$pager->attachToCriteria($c);
		$jobs = BatchJobLogPeer::doSelect($c);
		
		$response->objects = KontorolBulkUploads::fromBatchJobArray($jobs);
		$response->totalCount = $count; 
		
		return $response;
	}
	
	
	
	
	/**
	 * serve action returns the original file.
	 * 
	 * @action serve
	 * @param int $id job id
	 * @return file
	 * 
	 */
	function serveAction($id)
	{
		$c = new Criteria();
		$c->addAnd(BatchJobPeer::ID, $id);
		$c->addAnd(BatchJobPeer::PARTNER_ID, $this->getPartnerId());
		$c->addAnd(BatchJobPeer::JOB_TYPE, BatchJobType::BULKUPLOAD);
		$batchJob = BatchJobPeer::doSelectOne($c);
		
		if (!$batchJob)
			throw new KontorolAPIException(KontorolErrors::BULK_UPLOAD_NOT_FOUND, $id);
			 
		KontorolLog::info("Batch job found for jobid [$id] bulk upload type [". $batchJob->getJobSubType() . "]");

		$syncKey = $batchJob->getSyncKey(BatchJob::FILE_SYNC_BATCHJOB_SUB_TYPE_BULKUPLOAD);
		list($fileSync, $local) = kFileSyncUtils::getReadyFileSyncForKey($syncKey, true, false);
		
		header("Content-Type: text/plain; charset=UTF-8");

		if($local)
		{
			$filePath = $fileSync->getFullPath();
			$mimeType = kFile::mimeType($filePath);
			return $this->dumpFile($filePath, $mimeType);
		}
		else
		{
			$remoteUrl = kDataCenterMgr::getRedirectExternalUrl($fileSync);
			KontorolLog::info("Redirecting to [$remoteUrl]");
			header("Location: $remoteUrl");
			die;
		}	
	}
	
	
	/**
	 * serveLog action returns the log file for the bulk-upload job.
	 * 
	 * @action serveLog
	 * @param int $id job id
	 * @return file
	 * 
	 */
	function serveLogAction($id)
	{
		$c = new Criteria();
		$c->addAnd(BatchJobPeer::ID, $id);
		$c->addAnd(BatchJobPeer::PARTNER_ID, $this->getPartnerId());
		$c->addAnd(BatchJobPeer::JOB_TYPE, BatchJobType::BULKUPLOAD);
		$batchJob = BatchJobPeer::doSelectOne($c);
		
		if (!$batchJob)
			throw new KontorolAPIException(KontorolErrors::BULK_UPLOAD_NOT_FOUND, $id);
			 
		KontorolLog::info("Batch job found for jobid [$id] bulk upload type [". $batchJob->getJobSubType() . "]");
			
		$pluginInstances = KontorolPluginManager::getPluginInstances('IKontorolBulkUpload');
		foreach($pluginInstances as $pluginInstance)
		{
			/* @var $pluginInstance IKontorolBulkUpload */
			$pluginInstance->writeBulkUploadLogFile($batchJob);
		}	
	}
	
	/**
	 * Aborts the bulk upload and all its child jobs
	 * 
	 * @action abort
	 * @param int $id job id
	 * @return KontorolBulkUpload
	 */
	function abortAction($id)
	{
	    $c = new Criteria();
	    $c->addAnd(BatchJobPeer::ID, $id);
		$c->addAnd(BatchJobPeer::PARTNER_ID, $this->getPartnerId());
		$c->addAnd(BatchJobPeer::JOB_TYPE, BatchJobType::BULKUPLOAD);
		$batchJob = BatchJobPeer::doSelectOne($c);
		
	    if (!$batchJob)
		    throw new KontorolAPIException(KontorolErrors::BULK_UPLOAD_NOT_FOUND, $id);
		
		kJobsManager::abortJob($id, BatchJobType::BULKUPLOAD, true);
		
		$batchJobLog = BatchJobLogPeer::retrieveByBatchJobId($id);
		
		$ret = new KontorolBulkUpload();
		if ($batchJobLog)
    		$ret->fromObject($batchJobLog, $this->getResponseProfile());
    	
    	return $ret;
	}

	/**
	 * @action updateCategoryEntriesStatus
	 * @actionAlias categoryEntry.updateStatusFromBulk
	 * Action activate or rejects categoryEntry objects from a bulkupload CSV file
	 * @param file $fileData
	 * @param KontorolBulkUploadJobData $bulkUploadData
	 * @param KontorolBulkUploadCategoryEntryData $bulkUploadCategoryEntryData
	 * @return KontorolBulkUpload
	 */
	public function updateCategoryEntriesStatusAction($fileData, KontorolBulkUploadJobData $bulkUploadData = null, KontorolBulkUploadCategoryEntryData $bulkUploadCategoryEntryData = null)
	{
		if (!$bulkUploadData)
		{
			$bulkUploadData = KontorolPluginManager::loadObject('KontorolBulkUploadJobData', null);
		}

		if (!$bulkUploadCategoryEntryData)
		{
			$bulkUploadCategoryEntryData = new KontorolBulkUploadCategoryEntryData();
		}

		if(!$bulkUploadData->fileName)
		{
			$bulkUploadData->fileName = $fileData["name"];
		}

		$dbBulkUploadJobData = $bulkUploadData->toInsertableObject();
		$bulkUploadCoreType = kPluginableEnumsManager::apiToCore("BulkUploadType", $bulkUploadData->type);
		$dbBulkUploadJobData->setBulkUploadObjectType(BulkUploadObjectType::CATEGORY_ENTRY);
		$dbBulkUploadJobData->setUserId($this->getKuser()->getPuserId());
		$dbObjectData = $bulkUploadCategoryEntryData->toInsertableObject();
		$dbBulkUploadJobData->setObjectData($dbObjectData);
		$dbBulkUploadJobData->setFilePath($fileData["tmp_name"]);

		$dbJob = kJobsManager::addBulkUploadJob($this->getPartner(), $dbBulkUploadJobData, $bulkUploadCoreType);
		$dbJobLog = BatchJobLogPeer::retrieveByBatchJobId($dbJob->getId());
		if(!$dbJobLog)
		{
			return null;
		}

		$bulkUpload = new KontorolBulkUpload();
		$bulkUpload->fromObject($dbJobLog, $this->getResponseProfile());

		return $bulkUpload;
	}

	/**
	 * @action userEntryBulkDelete
	 * @actionAlias userEntry.bulkDelete
	 * Action delete userEntry objects from filter in bulk
	 * @param KontorolUserEntryFilter $filter
	 * @throws KontorolErrors::FAILED_TO_CREATE_BULK_DELETE
	 * @return int
	 */
	public function userEntryBulkDeleteAction(KontorolUserEntryFilter $filter)
	{
		$bulkUploadData = new KontorolBulkServiceFilterDataBase();
		$bulkUploadData->filter = $filter;
		$bulkUploadObjectType = BulkUploadObjectType::USER_ENTRY;
		$bulkUpload = $this->bulkDelete($bulkUploadData, $bulkUploadObjectType);
		return $bulkUpload->id;
	}

	protected function bulkDelete(KontorolBulkServiceFilterDataBase $bulkUploadData, $bulkUploadObjectType)
	{
		$bulkUploadJobData = KontorolPluginManager::loadObject('KontorolBulkUploadJobData', $bulkUploadData->getType());
		$bulkUploadData->toBulkUploadJobData($bulkUploadJobData);

		$dbBulkUploadJobData = $bulkUploadJobData->toInsertableObject();
		$bulkUploadCoreType = kPluginableEnumsManager::apiToCore("BulkUploadType", $bulkUploadJobData->type);
		$dbBulkUploadJobData->setBulkUploadObjectType($bulkUploadObjectType);
		$dbBulkUploadJobData->setUserId($this->getKuser()->getPuserId());

		$dbJob = kJobsManager::addBulkUploadJob($this->getPartner(), $dbBulkUploadJobData, $bulkUploadCoreType);
		$dbJobLog = BatchJobLogPeer::retrieveByBatchJobId($dbJob->getId());
		if(!$dbJobLog)
		{
			throw new KontorolAPIException(KontorolErrors::FAILED_TO_CREATE_BULK_DELETE);
		}

		$bulkUpload = new KontorolBulkUpload();
		$bulkUpload->fromObject($dbJobLog, $this->getResponseProfile());

		return $bulkUpload;
	}


}

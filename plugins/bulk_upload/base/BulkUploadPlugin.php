<?php

/**
 * This plugin replaces the deprecated BulkUploadService. It includes a service for uploading entries, categories, users and categoryUsers in bulks.
 *@package plugins.bulkUpload
 *
 */
class BulkUploadPlugin extends KontorolPlugin implements IKontorolServices, IKontorolEventConsumers
{
    const PLUGIN_NAME = "bulkUpload";

	/* (non-PHPdoc)
     * @see IKontorolPlugin::getPluginName()
     */
    public static function getPluginName ()
    {
        return self::PLUGIN_NAME;
        
    }

    public static function getServicesMap()
	{
		$map = array(
			'bulk' => 'BulkService',
		);
		return $map;
	}

	/* (non-PHPdoc)
     * @see IKontorolEventConsumers::getEventConsumers()
     */
    public static function getEventConsumers ()
    {
        return array('kBatchJobLogManager');
    }
}

<?php
/**
 * Plugins can handle bulk upload xml additional data by implementing this interface
 * @package plugins.bulkUploadXml
 * @subpackage lib
 */
interface IKontorolBulkUploadXmlHandler
{
	/**
	 * Configures the handler by passing all the required configuration 
	 * @param BulkUploadEngineXml $xmlBulkUploadEngine  
	 */
	public function configureBulkUploadXmlHandler(BulkUploadEngineXml $xmlBulkUploadEngine);
	
	/**
	 * Handles plugin data for new created object 
	 * @param KontorolObjectBase $object
	 * @param SimpleXMLElement $item
	 * @throws KontorolBulkUploadXmlException
	 */
	public function handleItemAdded(KontorolObjectBase $object, SimpleXMLElement $item);

	/**
	 * 
	 * Handles plugin data for updated object  
	 * @param KontorolObjectBase $object
	 * @param SimpleXMLElement $item
	 * @throws KontorolBulkUploadXmlException
	 */
	public function handleItemUpdated(KontorolObjectBase $object, SimpleXMLElement $item);

	/**
	 * 
	 * Handles plugin data for deleted object  
	 * @param KontorolObjectBase $object
	 * @param SimpleXMLElement $item
	 * @throws KontorolBulkUploadXmlException
	 */
	public function handleItemDeleted(KontorolObjectBase $object, SimpleXMLElement $item);
	
	/**
	 * Return the container name to be handeled
	 */
	public function getContainerName();
}

<?php
/**
 * @package plugins.bulkUploadXml
 * @subpackage Scheduler.BulkUpload
 */
class KontorolBulkUploadXmlException extends KontorolException
{
	public function __construct($message, $code, $arguments = null)
	{
		parent::__construct($message, $code, $arguments);
	}
}

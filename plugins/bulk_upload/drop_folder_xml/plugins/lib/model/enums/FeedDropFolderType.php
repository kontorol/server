<?php
/**
 * @package plugins.FeedDropFolder
 * @subpackage model.enum
 */
class FeedDropFolderType implements IKontorolPluginEnum, DropFolderType
{
	const FEED = 'FEED';
	
	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalValues()
	 */
	public static function getAdditionalValues() {
		return array('FEED' => self::FEED);
		
	}

	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalDescriptions()
	 */
	public static function getAdditionalDescriptions() {
		return array();
		
	}
}

<?php
/**
 * @package plugins.ApFeedDropFolder
 * @subpackage model.enum
 */
class ApFeedDropFolderType implements IKontorolPluginEnum, DropFolderType
{
	const AP_FEED = 'AP_FEED';
	
	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalValues()
	 */
	public static function getAdditionalValues() {
		return array('AP_FEED' => self::AP_FEED);
		
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalDescriptions()
	 */
	public static function getAdditionalDescriptions() {
		return array();
		
	}
}

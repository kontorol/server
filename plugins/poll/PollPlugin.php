<?php
/**
 * @package plugins.poll
 */
class PollPlugin extends KontorolPlugin implements IKontorolServices {

	const PLUGIN_NAME = 'poll';

	/* (non-PHPdoc)
	 * @see IKontorolServices::getPluginName()
	 */
	public static function getPluginName() {
		return self::PLUGIN_NAME;
	}

	/* (non-PHPdoc)
	 * @see IKontorolServices::getServicesMap()
	 */
	public static function getServicesMap()
	{
		$map = array(
			'poll' => 'PollService',
		);
		return $map;
	}

	/* (non-PHPdoc)
	 * @see IKontorolPermissions::isAllowedPartner()
	 */
	public static function isAllowedPartner($partnerId)
	{
		return true;
	}
}

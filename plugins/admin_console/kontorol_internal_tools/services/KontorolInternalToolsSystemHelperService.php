<?php
/**
 * 
 * Internal Tools Service
 * 
 * @service kontorolInternalToolsSystemHelper
 * @package plugins.KontorolInternalTools
 * @subpackage api.services
 */
class KontorolInternalToolsSystemHelperService extends KontorolBaseService
{

	/**
	 * KS from Secure String
	 * @action fromSecureString
	 * @param string $str
	 * @return KontorolInternalToolsSession
	 * 
	 */
	public function fromSecureStringAction($str)
	{
		$ks =  ks::fromSecureString ( $str );
		
		$ksFromSecureString = new KontorolInternalToolsSession();
		$ksFromSecureString->fromObject($ks, $this->getResponseProfile());
		
		return $ksFromSecureString;
	}
	
	/**
	 * from ip to country
	 * @action iptocountry
	 * @param string $remote_addr
	 * @return string
	 * 
	 */
	public function iptocountryAction($remote_addr)
	{
		$ip_geo = new myIPGeocoder();
		$res = $ip_geo->iptocountry($remote_addr); 
		return $res;
	}
	
	/**
	 * @action getRemoteAddress
	 * @return string
	 * 
	 */
	public function getRemoteAddressAction()
	{
		$remote_addr = requestUtils::getRemoteAddress();
		return $remote_addr;	
	}
}

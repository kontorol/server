<?php
/**
 * @package plugins.KontorolInternalTools
 */
class KontorolInternalToolsPlugin extends KontorolPlugin implements IKontorolServices, IKontorolAdminConsolePages
{
	const PLUGIN_NAME = 'KontorolInternalTools';
	
	public static function getPluginName()
	{
		return self::PLUGIN_NAME;
	}
	
	/**
	 * @return array<string,string> in the form array[serviceName] = serviceClass
	 */
	public static function getServicesMap()
	{
		$map = array(
			'KontorolInternalTools' => 'KontorolInternalToolsService',
			'KontorolInternalToolsSystemHelper' => 'KontorolInternalToolsSystemHelperService',
		);
		return $map;
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolAdminConsolePages::getApplicationPages()
	 */
	public static function getApplicationPages()
	{
		$KontorolInternalTools = array(new KontorolInternalToolsPluginSystemHelperAction(),new KontorolInternalToolsPluginFlavorParams());
		return $KontorolInternalTools;
	}
	
	public static function isAllowedPartner($partnerId)
	{
		if($partnerId == Partner::ADMIN_CONSOLE_PARTNER_ID)
			return true;
		
		return false;
	}
}

<?php
/**
 * @package plugins.adminConsoleGallery
 */
class AdminConsoleGalleryPlugin extends KontorolPlugin implements IKontorolAdminConsolePages
{
	const PLUGIN_NAME = 'adminConsoleGallery';
	
	public static function getPluginName()
	{
		return self::PLUGIN_NAME;
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolAdminConsolePages::getApplicationPages()
	 */
	public static function getApplicationPages()
	{
		$pages = array();
		$pages[] = new AdminConsoleGalleryAction();
		return $pages;
	}
}

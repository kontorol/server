<?php
/**
 * @service reportAdmin
 * @package plugins.adminConsole
 * @subpackage api.services
 */
class ReportAdminService extends KontorolBaseService
{
    /* (non-PHPdoc)
     * @see KontorolBaseService::initService()
     */
    public function initService($serviceId, $serviceName, $actionName)
    {
        parent::initService($serviceId, $serviceName, $actionName);

		if(!AdminConsolePlugin::isAllowedPartner($this->getPartnerId()))
			throw new KontorolAPIException(KontorolErrors::FEATURE_FORBIDDEN, AdminConsolePlugin::PLUGIN_NAME);
	}
    
	/**
	 * @action add
	 * @param KontorolReport $report
	 * @return KontorolReport
	 */
	function addAction(KontorolReport $report)
	{
		$dbReport = new Report();
		$report->toInsertableObject($dbReport);
		$dbReport->save();
		
		$report->fromObject($dbReport, $this->getResponseProfile());
		return $report;
	}
	
	/**
	 * @action get
	 * @param int $id
	 * @return KontorolReport
	 */
	function getAction($id)
	{
		$dbReport = ReportPeer::retrieveByPK($id);
		if (is_null($dbReport))
			throw new KontorolAPIException(KontorolErrors::REPORT_NOT_FOUND, $id);
			
		$report = new KontorolReport();
		$report->fromObject($dbReport, $this->getResponseProfile());
		return $report;
	}
	
	/**
	 * @action list
	 * @param KontorolReportFilter $filter
	 * @param KontorolReport $report
	 * @return KontorolReportListResponse
	 */
	function listAction(KontorolReportFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
			$filter = new KontorolReportFilter();
			
		if (!$pager)
			$pager = new KontorolFilterPager();
			
		$reportFilter = new ReportFilter();
		
		$filter->toObject($reportFilter);
		$c = new Criteria();
		$reportFilter->attachToCriteria($c);
		$pager->attachToCriteria($c);
		
		$dbList = ReportPeer::doSelect($c);
		$c->setLimit(null);
		$totalCount = ReportPeer::doCount($c);

		$list = KontorolReportArray::fromDbArray($dbList, $this->getResponseProfile());
		$response = new KontorolReportListResponse();
		$response->objects = $list;
		$response->totalCount = $totalCount;
		return $response;    
	}
	
	/**
	 * @action update
	 * @param int $id
	 * @param KontorolReport $report
	 * @return KontorolReport
	 */
	function updateAction($id, KontorolReport $report)
	{
		$dbReport = ReportPeer::retrieveByPK($id);
		if (is_null($dbReport))
			throw new KontorolAPIException(KontorolErrors::REPORT_NOT_FOUND, $id);
			
		$report->toUpdatableObject($dbReport);
		$dbReport->save();
		
		$report->fromObject($dbReport, $this->getResponseProfile());
		return $report;
	}
	
	/**
	 * @param int $id
	 * @action delete
	 */
	function deleteAction($id)
	{
		$dbReport = ReportPeer::retrieveByPK($id);
		if (is_null($dbReport))
			throw new KontorolAPIException(KontorolErrors::REPORT_NOT_FOUND, $id);
			
		$dbReport->setDeletedAt(time());
		$dbReport->save();
	}
	
	/**
	 * @action executeDebug
	 * @param int $id
	 * @param KontorolKeyValueArray $params
	 * @return KontorolReportResponse
	 */
	function executeDebugAction($id, KontorolKeyValueArray $params = null)
	{
		$dbReport = ReportPeer::retrieveByPK($id);
		if (is_null($dbReport))
			throw new KontorolAPIException(KontorolErrors::REPORT_NOT_FOUND, $id);
			
		$query = $dbReport->getQuery();
		$matches = null;
		$execParams = KontorolReportHelper::getValidateExecutionParameters($dbReport, $params);
		
		try 
		{
			$kReportsManager = new kReportManager($dbReport);
			list($columns, $rows) = $kReportsManager->execute($execParams);
		}
		catch(Exception $ex)
		{
			KontorolLog::err($ex);
			throw new KontorolAPIException(KontorolErrors::INTERNAL_SERVERL_ERROR_DEBUG, $ex->getMessage());
		}
		
		$reportResponse = KontorolReportResponse::fromColumnsAndRows($columns, $rows);
		
		return $reportResponse;
	}
	
	/**
	 * @action getParameters
	 * @param int $id
	 * @return KontorolStringArray
	 */
	function getParametersAction($id)
	{
		$dbReport = ReportPeer::retrieveByPK($id);
		if (is_null($dbReport))
			throw new KontorolAPIException(KontorolErrors::REPORT_NOT_FOUND, $id);
			
		return KontorolStringArray::fromStringArray($dbReport->getParameters());
	}
	
	/**
	 * @action getCsvUrl
	 * @param int $id
	 * @param int $reportPartnerId
	 * @return string
	 */
	function getCsvUrlAction($id, $reportPartnerId)
	{
		$dbReport = ReportPeer::retrieveByPK($id);
		if (is_null($dbReport))
			throw new KontorolAPIException(KontorolErrors::REPORT_NOT_FOUND, $id);

		$dbPartner = PartnerPeer::retrieveByPK($reportPartnerId);
		if (is_null($dbPartner))
			throw new KontorolAPIException(KontorolErrors::INVALID_PARTNER_ID, $reportPartnerId);

		// allow creating urls for reports that are associated with partner 0 and the report owner
		if ($dbReport->getPartnerId() !== 0 && $dbReport->getPartnerId() !== $reportPartnerId) 
			throw new KontorolAPIException(KontorolErrors::REPORT_NOT_PUBLIC, $id);
		
		$ks = new ks();
		$ks->valid_until = time() + 2 * 365 * 24 * 60 * 60; // 2 years 
		$ks->type = ks::TYPE_KS;
		$ks->partner_id = $reportPartnerId;
		$ks->master_partner_id = null;
		$ks->partner_pattern = $reportPartnerId;
		$ks->error = 0;
		$ks->rand = microtime(true);
		$ks->user = '';
		$ks->privileges = 'setrole:REPORT_VIEWER_ROLE';
		$ks->additional_data = null;
		$ks_str = $ks->toSecureString();
		
		$paramsArray = $this->getParametersAction($id);
		$paramsStrArray = array();
		foreach($paramsArray as $param)
		{
			$paramsStrArray[] = ($param->value.'={'.$param->value.'}');
		}

		$url = "http://" . kConf::get("www_host") . "/api_v3/index.php/service/report/action/getCsvFromStringParams/id/{$id}/ks/" . $ks_str . "/params/" . implode(';', $paramsStrArray);
		return $url;
	}
}

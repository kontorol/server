<?php
/**
 * @package plugins.systemPartner
 * @subpackage api.objects
 */
class KontorolSystemPartnerPackageArray extends KontorolTypedArray
{
	public function __construct()
	{
		return parent::__construct("KontorolSystemPartnerPackage");
	}
	
	public function fromArray($arr)
	{
		foreach($arr as $item)
		{
			$obj = new KontorolSystemPartnerPackage();
			$obj->fromArray($item);
			$this[] = $obj;
		}
	}
}

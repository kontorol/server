<?php
/**
 * @package plugins.systemPartner
 * @subpackage api.objects
 */
class KontorolSystemPartnerUsageArray extends KontorolTypedArray
{
	public function __construct()
	{
		return parent::__construct("KontorolSystemPartnerUsageItem");
	}
}

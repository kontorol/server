<?php
/**
 * @package plugins.systemPartner
 * @subpackage api.objects
 */
class KontorolSystemPartnerUsageListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolSystemPartnerUsageArray
	 */
	public $objects;
}

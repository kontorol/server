<?php
/**
 * @package plugins.systemPartner
 * @subpackage api.objects
 */
class KontorolSystemPartnerLimitArray extends KontorolTypedArray
{
	/**
	 * @param Partner $partner
	 * @return KontorolSystemPartnerLimitArray
	 */
	public static function fromPartner(Partner $partner)
	{
		$arr = new KontorolSystemPartnerLimitArray();
		$reflector = KontorolTypeReflectorCacher::get('KontorolSystemPartnerLimitType');
		$types = $reflector->getConstants();
		foreach($types as $typeInfo) {
			$typeValue = $typeInfo->getDefaultValue();
			$limits = array();
			if ($typeValue == KontorolSystemPartnerLimitType::LIVE_CONCURRENT_BY_ADMIN_TAG)
			{
				$limits =  KontorolSystemPartnerLiveAdminTagLimit::getArrayFromPartner($typeValue, $partner);
			} 
			else
			{
				$limits = KontorolSystemPartnerOveragedLimit::getArrayFromPartner($typeValue, $partner);
			}
			foreach ($limits as $limit)
			{
				$arr[] = $limit;
			}
		}
		return $arr;
	} 
	
	public function __construct()
	{
		return parent::__construct("KontorolSystemPartnerLimit");
	}
}

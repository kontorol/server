<?php
/**
 * @package plugins.systemPartner
 * @subpackage api.objects
 */
class KontorolSystemPartnerLiveAdminTagLimit extends KontorolSystemPartnerLimit
{
	/**
	 * @var string
	 */
	public $adminTag;

	protected static function createLiveAdminTagLimit($adminTag, $value)
	{
		$res = new KontorolSystemPartnerLiveAdminTagLimit();
		$res->type = KontorolSystemPartnerLimitType::LIVE_CONCURRENT_BY_ADMIN_TAG;
		$res->adminTag = $adminTag;
		$res->max = $value;
		return $res;
	}

	/**
	 * @param KontorolSystemPartnerLimitType $type
	 * @param Partner $partner
	 * @return KontorolSystemPartnerLimitArray
	 */
	public static function getArrayFromPartner($type, $partner)
	{
		$res = new KontorolSystemPartnerLimitArray();
		$limits = $partner->getMaxConcurrentLiveByAdminTag();
		foreach ($limits as $adminTag => $value)
		{
			$res[] = self::createLiveAdminTagLimit($adminTag, $value);
		}
		return $res;
	}
	
	public function validate()
	{
		$this->validatePropertyMinValue('max', 0, true);
	}

	/**
	 * @param Partner $partner
	 */
	public function apply(Partner $partner)
	{
		if($this->isNull('max'))
		{
			$this->max = null;
		}
		$limits = $partner->getMaxConcurrentLiveByAdminTag();
		$limits[$this->adminTag] = $this->max;
		$partner->setMaxConcurrentLiveByAdminTag($limits);
	}
}

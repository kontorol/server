<?php
/**
 * Enable log view for admin-console entry investigation page
 * @package plugins.logView
 */
class LogViewPlugin extends KontorolPlugin implements IKontorolApplicationPartialView, IKontorolAdminConsolePages
{
	const PLUGIN_NAME = 'logView';

	/* (non-PHPdoc)
	 * @see IKontorolPlugin::getPluginName()
	 */
	public static function getPluginName()
	{
		return self::PLUGIN_NAME;
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolApplicationPartialView::getApplicationPartialViews()
	 */
	public static function getApplicationPartialViews($controller, $action)
	{
		if($controller == 'batch' && $action == 'entryInvestigation')
		{
			return array(
				new Kontorol_View_Helper_EntryInvestigateLogView(),
			);
		}
		
		return array();
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolAdminConsolePages::getApplicationPages()
	 */
	public static function getApplicationPages()
	{
		$KontorolInternalTools = array(
			new KontorolLogViewAction(),
			new KontorolObjectInvestigateLogAction(),
		);
		return $KontorolInternalTools;
	}
}

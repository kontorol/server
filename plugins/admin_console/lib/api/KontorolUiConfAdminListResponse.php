<?php
/**
 * @package plugins.adminConsole
 * @subpackage api.objects
 */
class KontorolUiConfAdminListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolUiConfAdminArray
	 * @readonly
	 */
	public $objects;
}

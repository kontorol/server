<?php
/**
 * @package plugins.adminConsole
 * @subpackage api.objects
 */
class KontorolTrackEntryListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolTrackEntryArray
	 * @readonly
	 */
	public $objects;
}

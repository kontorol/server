<?php
/**
 * @package plugins.adminConsole
 * @subpackage api.objects
 */
class KontorolUiConfAdminArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolUiConfAdminArray();
		foreach ( $arr as $obj )
		{
			$nObj = new KontorolUiConfAdmin();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct()
	{
		return parent::__construct("KontorolUiConfAdmin");
	}
}

<?php
/**
 * @package plugins.adminConsole
 * @subpackage api.objects
 */
class KontorolInvestigateEntryData extends KontorolObject
{
	/**
	 * @var KontorolBaseEntry
	 * @readonly
	 */
	public $entry;

	/**
	 * @var KontorolFileSyncListResponse
	 * @readonly
	 */
	public $fileSyncs;

	/**
	 * @var KontorolBatchJobListResponse
	 * @readonly
	 */
	public $jobs;
	
	/**
	 * @var KontorolInvestigateFlavorAssetDataArray
	 * @readonly
	 */
	public $flavorAssets;
	
	/**
	 * @var KontorolInvestigateThumbAssetDataArray
	 * @readonly
	 */
	public $thumbAssets;
	
	/**
	 * @var KontorolTrackEntryArray
	 * @readonly
	 */
	public $tracks;
}

<?php
/**
 * @package plugins.adminConsole
 * @subpackage api.objects
 */
class KontorolInvestigateFlavorAssetDataArray extends KontorolTypedArray
{
	public function __construct()
	{
		parent::__construct("KontorolInvestigateFlavorAssetData");
	}
}

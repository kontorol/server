<?php
/**
 * @package plugins.adminConsole
 * @subpackage api.objects
 */
class KontorolInvestigateThumbAssetData extends KontorolObject
{
	/**
	 * @var KontorolThumbAsset
	 * @readonly
	 */
	public $thumbAsset;

	/**
	 * @var KontorolFileSyncListResponse
	 * @readonly
	 */
	public $fileSyncs;

	/**
	 * @var KontorolThumbParams
	 * @readonly
	 */
	public $thumbParams;

	/**
	 * @var KontorolThumbParamsOutputListResponse
	 * @readonly
	 */
	public $thumbParamsOutputs;
}

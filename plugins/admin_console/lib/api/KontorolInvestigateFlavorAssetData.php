<?php
/**
 * @package plugins.adminConsole
 * @subpackage api.objects
 */
class KontorolInvestigateFlavorAssetData extends KontorolObject
{
	/**
	 * @var KontorolFlavorAsset
	 * @readonly
	 */
	public $flavorAsset;

	/**
	 * @var KontorolFileSyncListResponse
	 * @readonly
	 */
	public $fileSyncs;

	/**
	 * @var KontorolMediaInfoListResponse
	 * @readonly
	 */
	public $mediaInfos;

	/**
	 * @var KontorolFlavorParams
	 * @readonly
	 */
	public $flavorParams;

	/**
	 * @var KontorolFlavorParamsOutputListResponse
	 * @readonly
	 */
	public $flavorParamsOutputs;
}

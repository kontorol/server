<?php
/**
 * @package plugins.adminConsole
 * @subpackage api.objects
 */
class KontorolInvestigateThumbAssetDataArray extends KontorolTypedArray
{
	public function __construct()
	{
		parent::__construct("KontorolInvestigateThumbAssetData");
	}
}

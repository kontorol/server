<?php
/**
 * @package plugins.sso
 * @subpackage api.filters
 */
class KontorolSsoFilter extends KontorolSsoBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new SsoFilter();
	}


	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$c = new Criteria();
		$ssoFilter = $this->toObject();
		$ssoFilter->attachToCriteria($c);
		$pager->attachToCriteria($c);

		$list = SsoPeer::doSelect($c);

		$resultCount = count($list);
		if ($resultCount && $resultCount < $pager->pageSize)
		{
			$totalCount = ($pager->pageIndex - 1) * $pager->pageSize + $resultCount;
		}
		else
		{
			KontorolFilterPager::detachFromCriteria($c);
			$totalCount = SsoPeer::doCount($c);
		}

		$response = new KontorolSsoListResponse();
		$response->objects = KontorolSsoArray::fromDbArray($list, $responseProfile);
		$response->totalCount = $totalCount;
		return $response;
	}
}

<?php
/**
 * @package plugins.sso
 * @subpackage api.objects
 */
class KontorolSsoListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolSsoArray
	 * @readonly
	 */
	public $objects;
}

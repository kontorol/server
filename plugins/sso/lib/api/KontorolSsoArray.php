<?php
/**
 * @package plugins.sso
 * @subpackage api.objects
 */
class KontorolSsoArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolSsoArray();
		foreach ( $arr as $obj )
		{
			$nObj = new KontorolSso();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		return $newArr;
	}

	public function __construct( )
	{
		return parent::__construct ('KontorolSso');
	}
}

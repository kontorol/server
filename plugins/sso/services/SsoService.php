<?php
/**
 * @service sso
 * @package plugins.sso
 * @subpackage api.services
 */
class SsoService extends KontorolBaseService
{
	/**
	 * Adds a new sso configuration.
	 *
	 * @action add
	 * @param KontorolSso $sso a new sso configuration
	 * @return KontorolSso The new sso configuration
	 * @throws KontorolSsoErrors::DUPLICATE_SSO
	 */
	public function addAction(KontorolSso $sso)
	{
		$dbSso = $sso->toInsertableObject();
		$dbSso->setStatus(SsoStatus::ACTIVE);
		$dbSso->save();
		$sso->fromObject($dbSso, $this->getResponseProfile());
		return $sso;
	}

	/**
	 * Retrieves sso object
	 * @action get
	 * @param int $ssoId The unique identifier in the sso's object
	 * @return KontorolSso The specified sso object
	 * @throws KontorolSsoErrors::INVALID_SSO_ID
	 */
	public function getAction($ssoId)
	{
		$dbSso = SsoPeer::retrieveByPK($ssoId);
		if (!$dbSso)
		{
			throw new KontorolAPIException(KontorolSsoErrors::INVALID_SSO_ID, $ssoId);
		}
		$sso = new KontorolSso();
		$sso->fromObject($dbSso, $this->getResponseProfile());
		return $sso;
	}

	/**
	 * Delete sso by ID
	 *
	 * @action delete
	 * @param int $ssoId The unique identifier in the sso's object
	 * @return KontorolSso The deleted  object
	 * @throws KontorolSsoErrors::INVALID_SSO_ID
	 */
	public function deleteAction($ssoId)
	{
		$dbSso = SsoPeer::retrieveByPK($ssoId);
		if (!$dbSso)
		{
			throw new KontorolAPIException(KontorolSsoErrors::INVALID_SSO_ID, $ssoId);
		}
		$dbSso->setStatus(SsoStatus::DELETED);
		$dbSso->save();
		$sso = new KontorolSso();
		$sso->fromObject($dbSso, $this->getResponseProfile());
		return $sso;
	}

	/**
	 * Update sso by ID
	 *
	 * @action update
	 * @param int $ssoId The unique identifier in the sso's object
	 * @param KontorolSso $sso The updated object
	 * @return KontorolSso The updated  object
	 * @throws KontorolSsoErrors::INVALID_SSO_ID
	 */
	public function updateAction($ssoId, KontorolSso $sso)
	{
		$dbSso = SsoPeer::retrieveByPK($ssoId);
		if (!$dbSso)
		{
			throw new KontorolAPIException(KontorolSsoErrors::INVALID_SSO_ID, $ssoId);
		}
		$dbSso = $sso->toUpdatableObject($dbSso);
		$dbSso->save();
		$sso = new KontorolSso();
		$sso->fromObject($dbSso, $this->getResponseProfile());
		return $sso;
	}

	/**
	 * Lists sso objects that are associated with an account.
	 *
	 * @action list
	 * @param KontorolSsoFilter $filter
	 * @param KontorolFilterPager $pager A limit for the number of records to display on a page
	 * @return KontorolSsoListResponse The list of sso objects
	 */
	public function listAction(KontorolSsoFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
		{
			$filter = new KontorolSsoFilter();
		}
		if(!$pager)
		{
			$pager = new KontorolFilterPager();
		}
		return $filter->getListResponse($pager, $this->getResponseProfile());
	}

	/**
	 * Login with SSO, getting redirect url according to application type and partner Id
	 * or according to application type and domain
	 * @action login
	 * @param string $userId
	 * @param string $applicationType
	 * @param int $partnerId
	 * @return string $redirectUrl
	 * @throws KontorolSsoErrors::SSO_NOT_FOUND
	 */
	public function loginAction($userId = null, $applicationType , $partnerId = null)
	{
		if (!$userId)
		{
			$this->validatePartnerUsingSso($partnerId);
			$dbSso = KontorolSso::getSso($partnerId, $applicationType, null);
		}
		else
		{
			$domain = KontorolSso::getDomainFromUser($userId);
			if ($partnerId)
			{
				$this->validatePartnerUsingSso($partnerId);
				$dbSso = KontorolSso::getSso($partnerId, $applicationType, $domain);
			}
			else
			{
				list($dbSso, $partnerId) = $this->getSsoWithoutPID($userId, $applicationType, $domain);
			}
			self::setLastLogin($partnerId, $userId);
		}
		$sso = new KontorolSso();
		$sso->fromObject($dbSso, $this->getResponseProfile());
		return $sso->redirectUrl;
	}

	protected static function setLastLogin($partnerId, $userId)
	{
		if (!$partnerId)
		{
			return;
		}
		kuserPeer::setUseCriteriaFilter(false);
		$kuser = kuserPeer::getActiveKuserByPartnerAndUid($partnerId, $userId);
		kuserPeer::setUseCriteriaFilter(true);
		$loginData = UserLoginDataPeer::getByEmail($userId);
		if ($kuser && $loginData)
		{
			UserLoginDataPeer::setLastLoginFields($loginData, $kuser);
		}
	}

	protected function getSsoWithoutPID($userId, $applicationType, $domain)
	{
		try
		{
			$partnerId = UserLoginDataPeer::getPartnerIdFromLoginData($userId);
			$this->validatePartnerUsingSso($partnerId);
			//try login by PID
			$dbSso = KontorolSso::getSso($partnerId, $applicationType, $domain);
		}
		catch (Exception $e)
		{
			if ($e->getCode() == kUserException::LOGIN_DATA_NOT_FOUND)
			{
				$partnerId = null;
			}
			//try login by DOMAIN
			$dbSso = KontorolSso::getSso(null, $applicationType, $domain);
		}
		return array($dbSso, $partnerId);
	}

	protected function validatePartnerUsingSso($partnerId)
	{
		$partner = PartnerPeer::retrieveByPK($partnerId);
		if (!$partner || !$partner->getUseSso())
		{
			KontorolLog::debug("FEATURE_FORBIDDEN, $this->serviceId . '->' . $this->actionName");
			throw new KontorolAPIException(KontorolSsoErrors::SSO_NOT_FOUND);
		}
	}

}

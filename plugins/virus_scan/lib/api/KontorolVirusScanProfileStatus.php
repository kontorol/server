<?php
/**
 * @package plugins.virusScan
 * @subpackage api.enum
 */
class KontorolVirusScanProfileStatus extends KontorolEnum implements VirusScanProfileStatus
{
	// see VirusScanProfileStatus interface
}

<?php
/**
 * @package plugins.virusScan
 * @subpackage api.filters
 */
class KontorolVirusScanProfileFilter extends KontorolVirusScanProfileBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new VirusScanProfileFilter();
	}
}

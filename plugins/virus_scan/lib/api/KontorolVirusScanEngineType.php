<?php
/**
 * @package plugins.virusScan
 * @subpackage api.objects
 */
class KontorolVirusScanEngineType extends KontorolDynamicEnum implements VirusScanEngineType
{
	/**
	 * @return string
	 */
	public static function getEnumClass()
	{
		return 'VirusScanEngineType';
	}
}

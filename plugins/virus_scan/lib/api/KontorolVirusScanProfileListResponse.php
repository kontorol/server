<?php
/**
 * @package plugins.virusScan
 * @subpackage api.objects
 */
class KontorolVirusScanProfileListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolVirusScanProfileArray
	 * @readonly
	 */
	public $objects;
}

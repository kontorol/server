<?php

/**
 * @package plugins.queue
 */
class QueuePlugin extends KontorolPlugin implements IKontorolVersion, IKontorolRequire
{
	const PLUGIN_NAME = 'queue';
	const PLUGIN_VERSION_MAJOR = 1;
	const PLUGIN_VERSION_MINOR = 0;
	const PLUGIN_VERSION_BUILD = 0;
	
	/* (non-PHPdoc)
	 * @see IKontorolPlugin::getPluginName()
	 */
	public static function getPluginName()
	{
		return self::PLUGIN_NAME;
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolVersion::getVersion()
	 */
	public static function getVersion()
	{
		return new KontorolVersion(
			self::PLUGIN_VERSION_MAJOR,
			self::PLUGIN_VERSION_MINOR,
			self::PLUGIN_VERSION_BUILD
		);		
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolRequire::requires()
	 */	
	public static function requires()
	{
	    return array("IKontorolQueuePlugin");
	}
}

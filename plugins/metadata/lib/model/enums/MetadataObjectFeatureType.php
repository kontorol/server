<?php
/**
 * @package plugins.metadata
 *  @subpackage model.enum
 */
class MetadataObjectFeatureType implements IKontorolPluginEnum, ObjectFeatureType
{
	const CUSTOM_DATA = 'CustomData';
	
	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalValues()
	 */
	public static function getAdditionalValues() 
	{
		return array
		(
			'CUSTOM_DATA' => self::CUSTOM_DATA,
		);
		
	}

	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalDescriptions()
	 */
	public static function getAdditionalDescriptions() {
		return array();
		
	}
}

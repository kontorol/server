<?php
/**
 * @package plugins.metadata
 * @subpackage Scheduler.Index
 */
class KIndexingMetadataEngine extends KIndexingEngine
{
	/**
	 * @param KontorolFilter $filter
	 * @param bool $shouldUpdate
	 * @return int
	 */
	protected function index(KontorolFilter $filter, $shouldUpdate)
	{
		return $this->indexMetadataObjects($filter, $shouldUpdate);
	}

	/**
	 * @param KontorolMetadataFilter $filter
	 * @param $shouldUpdate
	 * @return int
	 */
	protected function indexMetadataObjects(KontorolMetadataFilter $filter, $shouldUpdate)
	{
		$filter->orderBy = KontorolMetadataOrderBy::CREATED_AT_ASC;
		$metadataPlugin = KontorolMetadataClientPlugin::get(KBatchBase::$kClient);
		$metadataList = $metadataPlugin->metadata->listAction($filter, $this->pager);
		if(!count($metadataList->objects))
			return 0;
			
		KBatchBase::$kClient->startMultiRequest();
		foreach($metadataList->objects as $metadata)
		{
			$metadataPlugin->metadata->index($metadata->id, $shouldUpdate);
		}
		
		$results = KBatchBase::$kClient->doMultiRequest();
		foreach($results as $index => $result)
			if(!is_int($result))
				unset($results[$index]);
				
		if(!count($results))
			return 0;
				
		$lastIndexId = end($results);
		$this->setLastIndexId($lastIndexId);
		
		return count($results);
	}
}

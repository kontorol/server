<?php
/**
 * @package plugins.metadata
 * @subpackage api.enum
 */
class KontorolMetadataProfileFieldStatus extends KontorolEnum
{
	const ACTIVE = 1;
	const DEPRECATED = 2;
	const NONE_SEARCHABLE = 3;
}

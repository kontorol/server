<?php
/**
 * @package plugins.metadata
 * @subpackage api.enum
 */
class KontorolMetadataObjectType extends KontorolDynamicEnum implements MetadataObjectType
{
	public static function getEnumClass()
	{
		return 'MetadataObjectType';
	}
}

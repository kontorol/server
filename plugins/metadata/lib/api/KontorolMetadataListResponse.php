<?php
/**
 * @package plugins.metadata
 * @subpackage api.objects
 */
class KontorolMetadataListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolMetadataArray
	 * @readonly
	 */
	public $objects;
}

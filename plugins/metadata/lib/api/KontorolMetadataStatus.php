<?php
/**
 * @package plugins.metadata
 * @subpackage api.enum
 */
class KontorolMetadataStatus extends KontorolEnum
{
	const VALID = 1;
	const INVALID = 2;
	const DELETED = 3;
}

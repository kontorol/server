<?php
/**
 * @package plugins.metadata
 * @subpackage api.objects
 */
class KontorolMetadataProfileFieldListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolMetadataProfileFieldArray
	 * @readonly
	 */
	public $objects;
}

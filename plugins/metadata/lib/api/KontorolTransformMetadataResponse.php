<?php
/**
 * @package plugins.metadata
 * @subpackage api.objects
 */
class KontorolTransformMetadataResponse extends KontorolObject
{
	/**
	 * @var KontorolMetadataArray
	 * @readonly
	 */
	public $objects;

	/**
	 * @var int
	 * @readonly
	 */
	public $totalCount;

	/**
	 * @var int
	 * @readonly
	 */
	public $lowerVersionCount;
}

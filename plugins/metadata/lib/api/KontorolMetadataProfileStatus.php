<?php
/**
 * @package plugins.metadata
 * @subpackage api.enum
 */
class KontorolMetadataProfileStatus extends KontorolEnum
{
	const ACTIVE = 1;
	const DEPRECATED = 2;
	const TRANSFORMING = 3;
}

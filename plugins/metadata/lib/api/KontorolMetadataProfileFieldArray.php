<?php
/**
 * @package plugins.metadata
 * @subpackage api.objects
 */
class KontorolMetadataProfileFieldArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolMetadataProfileFieldArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
    		$nObj = new KontorolMetadataProfileField();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolMetadataProfileField");
	}
}

<?php
/**
 * @package plugins.metadata
 * @subpackage api.objects
 */
class KontorolMetadataProfileListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolMetadataProfileArray
	 * @readonly
	 */
	public $objects;
}

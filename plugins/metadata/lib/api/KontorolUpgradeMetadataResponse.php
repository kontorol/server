<?php
/**
 * @package plugins.metadata
 * @subpackage api.objects
 */
class KontorolUpgradeMetadataResponse extends KontorolObject
{
	/**
	 * @var int
	 * @readonly
	 */
	public $totalCount;

	/**
	 * @var int
	 * @readonly
	 */
	public $lowerVersionCount;
}

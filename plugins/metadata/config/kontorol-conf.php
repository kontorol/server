<?php
// This file generated by Propel  convert-conf target
// from XML runtime conf file C:\opt\kontorol\app\alpha\config\runtime-conf.xml
return array (
  'datasources' => 
  array (
    'kontorol' =>
    array (
      'adapter' => 'mysql',
      'connection' => 
      array (
        'phptype' => 'mysql',
        'database' => 'kontorol',
        'hostspec' => 'localhost',
        'username' => 'root',
        'password' => 'root',
      ),
    ),
    'default' => 'kontorol',
  ),
  'log' => 
  array (
    'ident' => 'kontorol',
    'level' => '7',
  ),
  'generator_version' => '1.4.2',
  'classmap' => 
  array (
    'MetadataProfileTableMap' => 'lib/model/map/MetadataProfileTableMap.php',
    'MetadataProfilePeer' => 'lib/model/MetadataProfilePeer.php',
    'MetadataProfile' => 'lib/model/MetadataProfile.php',
    'MetadataProfileFieldTableMap' => 'lib/model/map/MetadataProfileFieldTableMap.php',
    'MetadataProfileFieldPeer' => 'lib/model/MetadataProfileFieldPeer.php',
    'MetadataProfileField' => 'lib/model/MetadataProfileField.php',
    'MetadataTableMap' => 'lib/model/map/MetadataTableMap.php',
    'MetadataPeer' => 'lib/model/MetadataPeer.php',
    'Metadata' => 'lib/model/Metadata.php',
  ),
);

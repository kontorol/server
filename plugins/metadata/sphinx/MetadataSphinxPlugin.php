<?php

/**
 * Enable indexing and searching of metadata objects in sphinx
 * @package plugins.metadataSphinx
 */
class MetadataSphinxPlugin extends KontorolPlugin implements IKontorolCriteriaFactory
{
	const PLUGIN_NAME = 'metadataSphinx';
	
	/* (non-PHPdoc)
	 * @see IKontorolPlugin::getPluginName()
	 */
	public static function getPluginName()
	{
		return self::PLUGIN_NAME;
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolCriteriaFactory::getKontorolCriteria()
	 */
	public static function getKontorolCriteria($objectType)
	{
		if ($objectType == "Metadata")
			return new SphinxMetadataCriteria();
			
		return null;
	}
}

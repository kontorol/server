<?php
/**
 * 
 */
class KWebexDropFolderEngine extends KDropFolderEngine
{
	const ZERO_DATE = '12/31/1971 00:00:01';
	const ARF_FORMAT = 'ARF';
	const MAX_QUERY_DATE_RANGE_DAYS = 25; //Maximum querying date range is 28 days we define it as less than that
	const MIN_TIME_BEFORE_HANDLING_UPLOADING = 60; //the time in seconds
	const ADMIN_TAG_WEBEX = 'webexentry';
	private static $unsupported_file_formats = array('WARF');
	private $serviceTypes = null;
	private $dropFolderFilesMap = null;
	/**
	 * Webex wrapper
	 * @var webexWrapper
	 */
	private $webexWrapper;

	private function getServiceTypes()
	{
		if(!$this->serviceTypes)
		{
			$dropFolderServiceTypes = $this->dropFolder->webexServiceType ? explode(',', $this->dropFolder->webexServiceType) :
				array(WebexXmlComServiceTypeType::_MEETINGCENTER);
			$this->serviceTypes = webexWrapper::stringServicesTypesToWebexXmlArray($dropFolderServiceTypes);
		}

		return $this->serviceTypes;
	}

	/**
	 * @param $dropFolder KontorolWebexDropFolder
	 */
	public function setDropFolder($dropFolder)
	{
		$this->dropFolder = $dropFolder;
	}

	public function watchFolder(KontorolDropFolder $dropFolder)
	{
		/* @var $dropFolder KontorolWebexDropFolder */
		$this->dropFolder = $dropFolder;
		$this->webexWrapper = new webexWrapper($this->dropFolder->webexServiceUrl . '/' . $this->dropFolder->path, $this->getWebexClientSecurityContext(),
			array('KontorolLog', 'err'), array('KontorolLog', 'debug'));

		KontorolLog::info('Watching folder ['.$this->dropFolder->id.']');
		$startTime = null;
		$endTime = null;
		if ($this->dropFolder->incremental)
		{
			$startTime = time()-self::MAX_QUERY_DATE_RANGE_DAYS*86400;
			$pastPeriod = $this->getMaximumExecutionTime() ?  $this->getMaximumExecutionTime() : 3600;
			if ( $this->dropFolder->lastFileTimestamp && ( ($this->dropFolder->lastFileTimestamp - $pastPeriod) > (time()-self::MAX_QUERY_DATE_RANGE_DAYS*86400)) )
				$startTime = $this->dropFolder->lastFileTimestamp - $pastPeriod;
			
			$startTime = date('m/j/Y H:i:s', $startTime);
			$endTime = (date('m/j/Y H:i:s', time()+86400));
		}

		$result = $this->listAllRecordings($startTime, $endTime);
		if (!empty($result))
		{
			$this->HandleNewFiles($result);
		}
		else
		{
			KontorolLog::info('No new files to handle at this time');
		}

		if ($this->dropFolder->fileDeletePolicy != KontorolDropFolderFileDeletePolicy::MANUAL_DELETE)
		{
			$this->purgeFiles();
		}
	}

	private function getDropFolderFilesMap()
	{
		if(!$this->dropFolderFilesMap)
		{
			$this->dropFolderFilesMap = $this->loadDropFolderFiles();
		}

		return $this->dropFolderFilesMap;
	}

	/**
	 * @param $physicalFiles array
	 * @return kWebexHandleFilesResult
	 */
	public function HandleNewFiles($physicalFiles)
	{
		$result = new kWebexHandleFilesResult();
		$dropFolderFilesMap = $this->getDropFolderFilesMap();
		$maxTime = $this->dropFolder->lastFileTimestamp;
		foreach ($physicalFiles as $physicalFile)
		{
			/* @var $physicalFile WebexXmlEpRecordingType */
			$physicalFileName = $physicalFile->getName() . '_' . $physicalFile->getRecordingID();
			if (in_array($physicalFile->getFormat(),self::$unsupported_file_formats))
			{
				KontorolLog::info('Recording with id [' . $physicalFile->getRecordingID() . '] format [' . $physicalFile->getFormat() . '] is incompatible with the Kontorol conversion processes. Ignoring.');
				$result->addFileName(kWebexHandleFilesResult::FILE_NOT_ADDED_TO_DROP_FOLDER, $physicalFileName);
				continue;
			}

			if(!array_key_exists($physicalFileName, $dropFolderFilesMap))
			{
				if($this->handleFileAdded($physicalFile))
				{
					$maxTime = max(strtotime($physicalFile->getCreateTime()), $maxTime);
					KontorolLog::info("Added new file with name [$physicalFileName]. maxTime updated: $maxTime");
					$result->addFileName(kWebexHandleFilesResult::FILE_ADDED_TO_DROP_FOLDER, $physicalFileName);
				}
				else
					$result->addFileName(kWebexHandleFilesResult::FILE_NOT_ADDED_TO_DROP_FOLDER, $physicalFileName);
			}
			else //drop folder file entry found
			{
				$dropFolderFile = $dropFolderFilesMap[$physicalFileName];
				unset($dropFolderFilesMap[$physicalFileName]);
				if ($dropFolderFile->status == KontorolDropFolderFileStatus::UPLOADING && $this->handleExistingDropFolderFile($dropFolderFile))
					$result->addFileName(kWebexHandleFilesResult::FILE_HANDLED, $physicalFileName);
				else
					$result->addFileName(kWebexHandleFilesResult::FILE_NOT_HANDLED, $physicalFileName);
			}
		}

		if ($this->dropFolder->incremental && $maxTime > $this->dropFolder->lastFileTimestamp)
		{
			$updateDropFolder = new KontorolDropFolder();
			$updateDropFolder->lastFileTimestamp = $maxTime;
			$this->dropFolderPlugin->dropFolder->update($this->dropFolder->id, $updateDropFolder);
		}

		return $result;
	}

	public function handleUploadingFiles()
	{
		$minHandlingTime = time() - self::MIN_TIME_BEFORE_HANDLING_UPLOADING;
		$dropFolderFilesMap = $this->loadDropFolderUpLoadingFiles($minHandlingTime);
		foreach($dropFolderFilesMap as $name => $dropFolderFile)
		{
			$this->handleExistingDropFolderFile($dropFolderFile);
		}
	}

	public function processFolder (KontorolBatchJob $job, KontorolDropFolderContentProcessorJobData $data)
	{
		KBatchBase::impersonate ($job->partnerId);
		
		/* @var $data KontorolWebexDropFolderContentProcessorJobData */
		$dropFolder = $this->dropFolderPlugin->dropFolder->get ($data->dropFolderId);
		//In the case of the webex drop folder engine, the only possible contentMatch policy is ADD_AS_NEW.
		//Any other policy should cause an error.
		switch ($data->contentMatchPolicy)
		{
			case KontorolDropFolderContentFileHandlerMatchPolicy::ADD_AS_NEW:
				$this->addAsNewContent($job, $data, $dropFolder);
				break;
			default:
				throw new kApplicativeException(KontorolDropFolderErrorCode::DROP_FOLDER_APP_ERROR, 'Content match policy not allowed for Webex drop folders');
				break;
		}
		
		KBatchBase::unimpersonate();
	}

	protected function listRecordings ($startTime = null, $endTime = null, $startFrom = 1)
	{
		KontorolLog::info("Fetching list of recordings from Webex, startTime [$startTime], endTime [$endTime] of types ".print_r($this->getServiceTypes()));
		$result = $this->webexWrapper->listRecordings($this->getServiceTypes(), $startTime, $endTime, $startFrom);
		if($result)
		{
			$recording = $result->getRecording();
			KontorolLog::info('Recordings fetched: ' . print_r($recording, true));
		}

		return $result;
	}

	protected function listAllRecordings ($startTime = null, $endTime = null)
	{
		KontorolLog::info("Fetching list of all recordings from Webex, startTime [$startTime], endTime [$endTime] of types ".print_r($this->getServiceTypes()));
		$result = $this->webexWrapper->listAllRecordings($this->getServiceTypes(), $startTime, $endTime);
		KontorolLog::info('Recordings fetched: '.print_r($result, true) );
		return $result;
	}

	public function getWebexClientSecurityContext()
	{
		$securityContext = new WebexXmlSecurityContext();
		$securityContext->setUid($this->dropFolder->webexUserId); // webex username
		$securityContext->setPwd($this->dropFolder->webexPassword); // webex password
		$securityContext->setSiteName($this->dropFolder->webexSiteName); // webex partner id
		$securityContext->setSid($this->dropFolder->webexSiteId); // webex site id
		$securityContext->setPid($this->dropFolder->webexPartnerId); // webex partner id

		return $securityContext;
	}
	
	/**
	 * @throws Exception
	 */
	protected function purgeFiles ()
	{
		$createTimeEnd = date('m/j/Y H:i:s');
		$createTimeStart = date('m/j/Y H:i:s', time() - self::MAX_QUERY_DATE_RANGE_DAYS * 86400);
		if ($this->dropFolder->deleteFromTimestamp && $this->dropFolder->deleteFromTimestamp > (time() - self::MAX_QUERY_DATE_RANGE_DAYS * 86400))
			$createTimeStart = date('m/j/Y H:i:s', $this->dropFolder->deleteFromTimestamp);

		KontorolLog::info("Finding files to purge.");
		$result = $this->listAllRecordings($createTimeStart, $createTimeEnd);
		if($result)
		{
			KontorolLog::info("Files to delete: " . count($result));
			$dropFolderFilesMap = $this->getDropFolderFilesMap();
		}

		foreach ($result as $file)
		{
			$physicalFileName = $file->getName() . '_' . $file->getRecordingID();
			if (!$this->shouldPurgeFile($dropFolderFilesMap, $physicalFileName))
				continue;

			try
			{
				$this->webexWrapper->deleteRecordById($file->getRecordingID());
			}
			catch (Exception $e)
			{
				KontorolLog::err('Error occurred: ' . print_r($e, true));
				continue;
			}

			if ($this->dropFolder->deleteFromRecycleBin)
			{
				try
				{
					$this->webexWrapper->deleteRecordByName($file->getName(), $this->getServiceTypes(), true);
				}
				catch (Exception $e)
				{
					KontorolLog::err("File [$physicalFileName] could not be removed from recycle bin. Purge manually");
					continue;
				}
			}

			KontorolLog::info("File [$physicalFileName] successfully purged. Purging drop folder file");
			$this->dropFolderFileService->updateStatus($dropFolderFilesMap[$physicalFileName]->id, KontorolDropFolderFileStatus::PURGED);
		}
	}

	/**
	 * @param array $dropFolderFilesMap
	 * @param string $physicalFileName
	 * @return bool
	 */
	private function shouldPurgeFile($dropFolderFilesMap, $physicalFileName)
	{
		if (!array_key_exists($physicalFileName, $dropFolderFilesMap))
		{
			KontorolLog::info("File with name $physicalFileName not handled yet. Ignoring");
			return false;
		}

		$dropFolderFile = $dropFolderFilesMap[$physicalFileName];
		/* @var $dropFolderFile KontorolWebexDropFolderFile */
		if (!in_array($dropFolderFile->status, array(KontorolDropFolderFileStatus::HANDLED, KontorolDropFolderFileStatus::DELETED)))
		{
			KontorolLog::info("File with name $physicalFileName not in final status. Ignoring");
			return false;
		}

		$deleteTime = $dropFolderFile->updatedAt + $this->dropFolder->autoFileDeleteDays*86400;
		if (time() < $deleteTime)
		{
			KontorolLog::info("File with name $physicalFileName- not time to delete yet. Ignoring");
			return false;
		}

		KontorolLog::info("Going to purge file:$physicalFileName.");
		return true;
	}
	
	protected function handleFileAdded (WebexXmlEpRecordingType $webexFile)
	{
		try 
		{
			$newDropFolderFile = new KontorolWebexDropFolderFile();
	    	$newDropFolderFile->dropFolderId = $this->dropFolder->id;
	    	$newDropFolderFile->fileName = $webexFile->getName() . '_' . $webexFile->getRecordingID();
	    	$newDropFolderFile->fileSize = WebexPlugin::getSizeFromWebexContentUrl($webexFile->getFileURL());
	    	$newDropFolderFile->lastModificationTime = $webexFile->getCreateTime(); 
			$newDropFolderFile->description = $webexFile->getDescription();
			$newDropFolderFile->confId = $webexFile->getConfID();
			$newDropFolderFile->recordingId = $webexFile->getRecordingID();
			$newDropFolderFile->webexHostId = $webexFile->getHostWebExID();
			$newDropFolderFile->contentUrl = $webexFile->getFileURL();
			KontorolLog::debug("Adding new WebexDropFolderFile: " . print_r($newDropFolderFile, true));
			$dropFolderFile = $this->dropFolderFileService->add($newDropFolderFile);
			
			return $dropFolderFile;
		}
		catch(Exception $e)
		{
			KontorolLog::err('Cannot add new drop folder file with name ['.$webexFile->getName() . '_' . $webexFile->getRecordingID().'] - '.$e->getMessage());
			return null;
		}
	}

	protected function handleExistingDropFolderFile (KontorolWebexDropFolderFile $dropFolderFile)
	{
		try
		{
			$updatedFileSize = WebexPlugin::getSizeFromWebexContentUrl($dropFolderFile->contentUrl);
		}
		catch (Exception $e)
		{
			$this->handleFileError($dropFolderFile->id, KontorolDropFolderFileStatus::ERROR_HANDLING, KontorolDropFolderFileErrorCode::ERROR_READING_FILE,
					DropFolderPlugin::ERROR_READING_FILE_MESSAGE, $e);
			return null;
		}

		if (!$dropFolderFile->fileSize)
		{
			$this->handleFileError($dropFolderFile->id, KontorolDropFolderFileStatus::ERROR_HANDLING, KontorolDropFolderFileErrorCode::ERROR_READING_FILE,
				DropFolderPlugin::ERROR_READING_FILE_MESSAGE . '[' . $dropFolderFile->contentUrl . ']');
		}
		else if ($dropFolderFile->fileSize < $updatedFileSize)
		{
			try
			{
				$updateDropFolderFile = new KontorolDropFolderFile();
				$updateDropFolderFile->fileSize = $updatedFileSize;

				return $this->dropFolderFileService->update($dropFolderFile->id, $updateDropFolderFile);
			}
			catch (Exception $e)
			{
				$this->handleFileError($dropFolderFile->id, KontorolDropFolderFileStatus::ERROR_HANDLING, KontorolDropFolderFileErrorCode::ERROR_UPDATE_FILE,
					DropFolderPlugin::ERROR_UPDATE_FILE_MESSAGE, $e);
				return null;
			}
		}
		else // file sizes are equal
		{
			$time = time();
			$fileSizeLastSetAt = $this->dropFolder->fileSizeCheckInterval + $dropFolderFile->fileSizeLastSetAt;

			KontorolLog::info("time [$time] fileSizeLastSetAt [$fileSizeLastSetAt]");

			// check if fileSizeCheckInterval time has passed since the last file size update
			if ($time > $fileSizeLastSetAt) {
				try {
					return $this->dropFolderFileService->updateStatus($dropFolderFile->id, KontorolDropFolderFileStatus::PENDING);
				} catch (KontorolException $e) {
					$this->handleFileError($dropFolderFile->id, KontorolDropFolderFileStatus::ERROR_HANDLING, KontorolDropFolderFileErrorCode::ERROR_UPDATE_FILE,
						DropFolderPlugin::ERROR_UPDATE_FILE_MESSAGE, $e);
					return null;
				}
			}
		}
	}

	protected function addAsNewContent (KontorolBatchJob $job, KontorolWebexDropFolderContentProcessorJobData $data, KontorolWebexDropFolder $folder)
	{
		/* @var $data KontorolWebexDropFolderContentProcessorJobData */
		$resource = $this->getIngestionResource($job, $data);
		$newEntry = new KontorolMediaEntry();
		$newEntry->mediaType = KontorolMediaType::VIDEO;
		$newEntry->conversionProfileId = $data->conversionProfileId;
		$newEntry->name = $data->parsedSlug;
		$newEntry->description = $data->description;
		$newEntry->userId = $data->parsedUserId ? $data->parsedUserId : $this->retrieveUserFromWebexHostId($data, $folder);
		$newEntry->creatorId = $newEntry->userId;
		$newEntry->referenceId = $data->parsedSlug;
		$newEntry->adminTags = self::ADMIN_TAG_WEBEX;
			
		KBatchBase::$kClient->startMultiRequest();
		$addedEntry = KBatchBase::$kClient->media->add($newEntry, null);
		KBatchBase::$kClient->baseEntry->addContent($addedEntry->id, $resource);
		$result = KBatchBase::$kClient->doMultiRequest();
		
		if ($result [1] && $result[1] instanceof KontorolBaseEntry)
		{
			$entry = $result [1];
			$this->createCategoryAssociations ($folder, $entry->userId, $entry->id);
		}
	}

	protected function retrieveUserFromWebexHostId (KontorolWebexDropFolderContentProcessorJobData $data, KontorolWebexDropFolder $folder)
	{
		if ($folder->metadataProfileId && $folder->webexHostIdMetadataFieldName && $data->webexHostId)
		{
			$filter = new KontorolUserFilter();
			$filter->advancedSearch = new KontorolMetadataSearchItem();
			$filter->advancedSearch->metadataProfileId = $folder->metadataProfileId;
			$webexHostIdSearchCondition = new KontorolSearchCondition();
			$webexHostIdSearchCondition->field = $folder->webexHostIdMetadataFieldName;
			$webexHostIdSearchCondition->value = $data->webexHostId;
			$filter->advancedSearch->items = array($webexHostIdSearchCondition);
			try
			{
				$result = KBatchBase::$kClient->user->listAction ($filter, new KontorolFilterPager());
				
				if ($result->totalCount)
				{
					$user = $result->objects[0];
					return $user->id;
				}
			}
			catch (Exception $e)
			{
				KontorolLog::err('Error encountered. Code: ['. $e->getCode() . '] Message: [' . $e->getMessage() . ']');
			}

		}
		
		return $data->webexHostId;
	}
	
}

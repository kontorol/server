<?php
/**
 * @package plugins.webexDropFolder
 *  @subpackage model.enum
 */
class WebexDropFolderType implements IKontorolPluginEnum, DropFolderType
{
	const WEBEX = 'WEBEX';
	
	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalValues()
	 */
	public static function getAdditionalValues() {
		return array('WEBEX' => self::WEBEX);
		
	}

	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalDescriptions()
	 */
	public static function getAdditionalDescriptions() {
		return array();
		
	}
}

<?php
/**
 * @package plugins.sip
 * @subpackage api.filters
 */
class KontorolSipEntryServerNodeFilter extends KontorolSipEntryServerNodeBaseFilter
{
	public function __construct()
	{
		$this->serverTypeIn = SipPlugin::getApiValue(SipEntryServerNodeType::SIP_ENTRY_SERVER );
	}
}

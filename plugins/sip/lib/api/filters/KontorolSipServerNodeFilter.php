<?php
/**
 * @package plugins.sip
 * @subpackage api.filters
 */
class KontorolSipServerNodeFilter extends KontorolSipServerNodeBaseFilter
{
	public function getTypeListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null, $type = null)
	{
		if(!$type)
		{
			$type = SipPlugin::getCoreValue('serverNodeType',SipServerNodeType::SIP_SERVER);
		}
	
		return parent::getTypeListResponse($pager, $responseProfile, $type);
	}
}

<?php
/**
 * @package plugins.varConsole
 * @subpackage api.types
 */
class KontorolPartnerUsageListResponse extends KontorolListResponse
{
    /**
     * @var KontorolVarPartnerUsageItem
     */
    public $total;
    /**
     * @var KontorolVarPartnerUsageArray
     */
    public $objects;
}

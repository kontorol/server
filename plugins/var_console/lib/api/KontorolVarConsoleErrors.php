<?php
/**
 * @package plugins.varConsole
 * @subpackage api.errors
 */
class KontorolVarConsoleErrors extends KontorolErrors
{
    const MAX_SUB_PUBLISHERS_EXCEEDED = "MAX_SUB_PUBLISHERS_EXCEEDED;;Maximum number of sub-publishers exceeded";
}

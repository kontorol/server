<?php
/**
 * @package plugins.varConsole
 * @subpackage api.objects
 */
class KontorolVarPartnerUsageArray extends KontorolTypedArray
{
	public function __construct()
	{
		return parent::__construct("KontorolVarPartnerUsageItem");
	}
}

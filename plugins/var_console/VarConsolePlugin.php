<?php
/**
 * @package plugins.varConsole
 */
class VarConsolePlugin extends KontorolPlugin implements IKontorolServices, IKontorolPermissions
{
    const PLUGIN_NAME = "varConsole";

	/* (non-PHPdoc)
     * @see IKontorolPlugin::getPluginName()
     */
    public static function getPluginName ()
    {    
        return self::PLUGIN_NAME;
    }


	/* (non-PHPdoc)
     * @see IKontorolServices::getServicesMap()
     */
    public static function getServicesMap ()
    {
        $map = array(
			'varConsole' => 'VarConsoleService',
		);
		
		return $map;
    }
    
    /* (non-PHPdoc)
     * @see IKontorolPermissions::isAllowedPartner($partnerId)
     */
    public static function isAllowedPartner($partnerId)
    {
        $partner = PartnerPeer::retrieveByPK($partnerId);
		
		return $partner->getEnabledService(KontorolPermissionName::FEATURE_VAR_CONSOLE_LOGIN);
    }

}

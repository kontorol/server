<?php
/**
 * @package plugins.vlc
 * @subpackage lib
 */
class KOperationEngineVlc  extends KSingleOutputOperationEngine
{

	public function __construct($cmd, $outFilePath)
	{
		parent::__construct($cmd,$outFilePath);
		KontorolLog::info(": cmd($cmd), outFilePath($outFilePath)");
	}

	protected function getCmdLine()
	{
		$exeCmd =  parent::getCmdLine();
		KontorolLog::info(print_r($this,true));
		return $exeCmd;
	}
}

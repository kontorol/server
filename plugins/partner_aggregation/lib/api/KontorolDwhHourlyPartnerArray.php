<?php
/**
 * @package plugins.partnerAggregation
 * @subpackage api.objects
 */
class KontorolDwhHourlyPartnerArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolDwhHourlyPartnerArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
    		$nObj = new KontorolDwhHourlyPartner();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolDwhHourlyPartner");
	}
}

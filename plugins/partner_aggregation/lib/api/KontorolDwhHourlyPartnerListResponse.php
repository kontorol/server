<?php
/**
 * @package plugins.partnerAggregation
 * @subpackage api.objects
 */
class KontorolDwhHourlyPartnerListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolDwhHourlyPartnerArray
	 * @readonly
	 */
	public $objects;
}

<?php
/**
 * Partner Aggregation service
 *
 * @service partnerAggregation
 * @package plugins.partnerAggregation
 * @subpackage api.services
 */
class PartnerAggregationService extends KontorolBaseService
{
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);

		$this->applyPartnerFilterForClass('DwhHourlyPartner');
	}
	
	/**
	 * List aggregated partner data
	 * 
	 * @action list
	 * @param KontorolDwhHourlyPartnerFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return KontorolDwhHourlyPartnerListResponse
	 */
	function listAction(KontorolDwhHourlyPartnerFilter $filter, KontorolFilterPager $pager = null)
	{
		$filter->validatePropertyNotNull('aggregatedTimeLessThanOrEqual');
		$filter->validatePropertyNotNull('aggregatedTimeGreaterThanOrEqual');

		if (!$pager)
			$pager = new KontorolFilterPager();
		
		$c = new Criteria();			
		$dwhHourlyPartnerFilter = $filter->toObject();
		$dwhHourlyPartnerFilter->attachToCriteria($c);
		$count = DwhHourlyPartnerPeer::doCount($c);
		$pager->attachToCriteria($c);
		$list = DwhHourlyPartnerPeer::doSelect($c);
		
		$response = new KontorolDwhHourlyPartnerListResponse();
		$response->objects = KontorolDwhHourlyPartnerArray::fromDbArray($list, $this->getResponseProfile());
		$response->totalCount = $count;
	
		return $response;
	}	
}

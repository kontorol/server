<?php
/**
 * @package plugins.conference
 * @subpackage api.filters
 */
class KontorolConferenceEntryServerNodeFilter extends KontorolConferenceEntryServerNodeBaseFilter
{
	public function __construct()
	{
		$this->serverTypeIn = ConferencePlugin::getApiValue(ConferenceEntryServerNodeType::CONFERENCE_ENTRY_SERVER );
	}
}

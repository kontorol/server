<?php
/**
 * @package plugins.conference
 * @subpackage api.filters
 */
class KontorolConferenceServerNodeFilter extends KontorolConferenceServerNodeBaseFilter
{
	public function getTypeListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null, $type = null)
	{
		if(!$type)
			$type = ConferencePlugin::getCoreValue('serverNodeType',ConferenceServerNodeType::CONFERENCE_SERVER);
	
		return parent::getTypeListResponse($pager, $responseProfile, $type);
	}
}

<?php
/**
 * @package plugins.widevine
 */
class WidevinePlugin extends BaseDrmPlugin implements IKontorolEnumerator, IKontorolServices , IKontorolPermissions, IKontorolObjectLoader, IKontorolEventConsumers, IKontorolTypeExtender, IKontorolSearchDataContributor, IKontorolPending, IKontorolPlaybackContextDataContributor
{
	const PLUGIN_NAME = 'widevine';
	const WIDEVINE_EVENTS_CONSUMER = 'kWidevineEventsConsumer';
	const WIDEVINE_RESPONSE_TYPE = 'widevine';
	const WIDEVINE_ENABLE_DISTRIBUTION_DATES_SYNC_PERMISSION = 'WIDEVINE_ENABLE_DISTRIBUTION_DATES_SYNC';
	const SEARCH_DATA_SUFFIX = 's';
	
	const REGISTER_ASSET_URL_PART = '/registerasset/';
	const GET_ASSET_URL_PART = '/getasset/';
	
	//Default values
	const KONTOROL_PROVIDER = 'kontorol';
	const DEFAULT_POLICY = 'default';
	const DEFAULT_LICENSE_START = '1970-01-01 00:00:01';
	const DEFAULT_LICENSE_END = '2033-05-18 00:00:00';

	
	/* (non-PHPdoc)
	 * @see IKontorolPlugin::getPluginName()
	 */
	public static function getPluginName()
	{
		return self::PLUGIN_NAME;
	}

	/* (non-PHPdoc)
	 * @see IKontorolPending::dependsOn()
	 */
	public static function dependsOn()
	{
		$drmDependency = new KontorolDependency(DrmPlugin::getPluginName());
		
		return array($drmDependency);
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolEnumerator::getEnums()
	 */
	public static function getEnums($baseEnumName = null)
	{	
		if(is_null($baseEnumName))
			return array('WidevineConversionEngineType', 'WidevineAssetType', 'WidevinePermissionName', 'WidevineBatchJobType', 'WidevineProviderType', 'WidevineSchemeName');
		if($baseEnumName == 'conversionEngineType')
			return array('WidevineConversionEngineType');
		if($baseEnumName == 'assetType')
			return array('WidevineAssetType');
		if($baseEnumName == 'PermissionName')
			return array('WidevinePermissionName');
		if($baseEnumName == 'BatchJobType')
			return array('WidevineBatchJobType');		
		if($baseEnumName == 'DrmProviderType')
			return array('WidevineProviderType');
		if ($baseEnumName == 'DrmSchemeName')
			return array('WidevineSchemeName');
			
		return array();
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolObjectLoader::loadObject()
	 */
	public static function loadObject($baseClass, $enumValue, array $constructorArgs = null)
	{
		if($baseClass == 'KontorolFlavorParams' && $enumValue == WidevinePlugin::getAssetTypeCoreValue(WidevineAssetType::WIDEVINE_FLAVOR))
			return new KontorolWidevineFlavorParams();
	
		if($baseClass == 'KontorolFlavorParamsOutput' && $enumValue == WidevinePlugin::getAssetTypeCoreValue(WidevineAssetType::WIDEVINE_FLAVOR))
			return new KontorolWidevineFlavorParamsOutput();
		
		if($baseClass == 'KontorolFlavorAsset' && $enumValue == WidevinePlugin::getAssetTypeCoreValue(WidevineAssetType::WIDEVINE_FLAVOR))
			return new KontorolWidevineFlavorAsset();
			
		if($baseClass == 'assetParams' && $enumValue == WidevinePlugin::getAssetTypeCoreValue(WidevineAssetType::WIDEVINE_FLAVOR))
			return new WidevineFlavorParams();
	
		if($baseClass == 'assetParamsOutput' && $enumValue == WidevinePlugin::getAssetTypeCoreValue(WidevineAssetType::WIDEVINE_FLAVOR))
			return new WidevineFlavorParamsOutput();
			
		if($baseClass == 'asset' && $enumValue == WidevinePlugin::getAssetTypeCoreValue(WidevineAssetType::WIDEVINE_FLAVOR))
			return new WidevineFlavorAsset();
			
		if($baseClass == 'flavorAsset' && $enumValue == WidevinePlugin::getAssetTypeCoreValue(WidevineAssetType::WIDEVINE_FLAVOR))
			return new WidevineFlavorAsset();
		
		if($baseClass == 'KOperationEngine' && $enumValue == KontorolConversionEngineType::WIDEVINE)
			return new KWidevineOperationEngine($constructorArgs['params'], $constructorArgs['outFilePath']);
			
		if($baseClass == 'KDLOperatorBase' && $enumValue == self::getApiValue(WidevineConversionEngineType::WIDEVINE))
			return new KDLOperatorWidevine($enumValue);

		if($baseClass == 'KontorolSerializer' && $enumValue == self::WIDEVINE_RESPONSE_TYPE)
			return new KontorolWidevineSerializer();
			
		if ($baseClass == 'KontorolJobData')
		{
		    if ($enumValue == WidevinePlugin::getApiValue(WidevineBatchJobType::WIDEVINE_REPOSITORY_SYNC))
			{
				return new KontorolWidevineRepositorySyncJobData();
			}
		}		
		if($baseClass == 'KontorolDrmProfile' && $enumValue == WidevinePlugin::getWidevineProviderCoreValue())
			return new KontorolWidevineProfile();
		if($baseClass == 'KontorolDrmProfile' && $enumValue == self::getApiValue(WidevineProviderType::WIDEVINE))
			return new KontorolWidevineProfile();

		if($baseClass == 'DrmProfile' && $enumValue == WidevinePlugin::getWidevineProviderCoreValue())
			return new WidevineProfile();

		if (class_exists('Kontorol_Client_Client'))
		{
			if ($baseClass == 'Kontorol_Client_Drm_Type_DrmProfile' && $enumValue == Kontorol_Client_Drm_Enum_DrmProviderType::WIDEVINE)
    		{
    			return new Kontorol_Client_Widevine_Type_WidevineProfile();
    		}
    		if ($baseClass == 'Form_DrmProfileConfigureExtend_SubForm' && $enumValue == Kontorol_Client_Drm_Enum_DrmProviderType::WIDEVINE)
    		{
     			return new Form_WidevineProfileConfigureExtend_SubForm();
    		}	   		

		}

		return null;
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolObjectLoader::getObjectClass()
	 */
	public static function getObjectClass($baseClass, $enumValue)
	{	
		if($baseClass == 'KontorolFlavorParams' && $enumValue == WidevinePlugin::getAssetTypeCoreValue(WidevineAssetType::WIDEVINE_FLAVOR))
			return 'KontorolWidevineFlavorParams';
	
		if($baseClass == 'KontorolFlavorParamsOutput' && $enumValue == WidevinePlugin::getAssetTypeCoreValue(WidevineAssetType::WIDEVINE_FLAVOR))
			return 'KontorolWidevineFlavorParamsOutput';
		
		if($baseClass == 'KontorolFlavorAsset' && $enumValue == WidevinePlugin::getAssetTypeCoreValue(WidevineAssetType::WIDEVINE_FLAVOR))
			return 'KontorolWidevineFlavorAsset';

		if($baseClass == 'assetParams' && $enumValue == WidevinePlugin::getAssetTypeCoreValue(WidevineAssetType::WIDEVINE_FLAVOR))
			return 'WidevineFlavorParams';
	
		if($baseClass == 'assetParamsOutput' && $enumValue == WidevinePlugin::getAssetTypeCoreValue(WidevineAssetType::WIDEVINE_FLAVOR))
			return 'WidevineFlavorParamsOutput';
			
		if($baseClass == 'asset' && $enumValue == WidevinePlugin::getAssetTypeCoreValue(WidevineAssetType::WIDEVINE_FLAVOR))
			return 'WidevineFlavorAsset';
			
		if($baseClass == 'flavorAsset' && $enumValue == WidevinePlugin::getAssetTypeCoreValue(WidevineAssetType::WIDEVINE_FLAVOR))
			return 'WidevineFlavorAsset';			
		
		if($baseClass == 'KOperationEngine' && $enumValue == KontorolConversionEngineType::WIDEVINE)
			return 'KWidevineOperationEngine';
			
		if($baseClass == 'KDLOperatorBase' && $enumValue == self::getApiValue(WidevineConversionEngineType::WIDEVINE))
			return 'KDLOperatorWidevine';
			
		if($baseClass == 'KontorolSerializer' && $enumValue == self::WIDEVINE_RESPONSE_TYPE)
			return 'KontorolWidevineSerializer';
		
		if ($baseClass == 'KontorolJobData')
		{
		    if ($enumValue == WidevinePlugin::getApiValue(WidevineBatchJobType::WIDEVINE_REPOSITORY_SYNC))
			{
				return 'KontorolWidevineRepositorySyncJobData';
			}
		}		
		if($baseClass == 'KontorolDrmProfile' && $enumValue == WidevinePlugin::getWidevineProviderCoreValue())
			return 'KontorolWidevineProfile';
		if($baseClass == 'KontorolDrmProfile' && $enumValue == self::getApiValue(WidevineProviderType::WIDEVINE))
			return 'KontorolWidevineProfile';

		if($baseClass == 'DrmProfile' && $enumValue == WidevinePlugin::getWidevineProviderCoreValue())
			return 'WidevineProfile';

		if (class_exists('Kontorol_Client_Client'))
		{
			if ($baseClass == 'Kontorol_Client_Drm_Type_DrmProfile' && $enumValue == Kontorol_Client_Drm_Enum_DrmProviderType::WIDEVINE)
    		{
    			return 'Kontorol_Client_Widevine_Type_WidevineProfile';
    		}

    		if ($baseClass == 'Form_DrmProfileConfigureExtend_SubForm' && $enumValue == Kontorol_Client_Drm_Enum_DrmProviderType::WIDEVINE)
    		{
     			return 'Form_WidevineProfileConfigureExtend_SubForm';
    		}	   		
		}
			
		return null;
	}
	
	/**
	 * @return string external API value of dynamic enum.
	 */
	public static function getApiValue($valueName)
	{
		return self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
	}

	/**
	 * @return int id of dynamic enum in the DB.
	 */
	public static function getCoreValue($type, $valueName)
	{
		$value = self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
		return kPluginableEnumsManager::apiToCore($type, $value);
	}
	
	public static function getConversionEngineCoreValue($valueName)
	{
		$value = self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
		return kPluginableEnumsManager::apiToCore('conversionEngineType', $value);
	}
	
	/**
	 * @return int id of dynamic enum in the DB.
	 */
	public static function getAssetTypeCoreValue($valueName)
	{
		$value = self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
		return kPluginableEnumsManager::apiToCore('assetType', $value);
	}
	
	/**
	 * @return int id of dynamic enum in the DB.
	 */
	public static function getWidevineProviderCoreValue()
	{
		$value = self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . WidevineProviderType::WIDEVINE;
		return kPluginableEnumsManager::apiToCore('DrmProviderType', $value);
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolTypeExtender::getExtendedTypes()
	 */
	public static function getExtendedTypes($baseClass, $enumValue) {
		$supportedBaseClasses = array(
			assetPeer::OM_CLASS,
			assetParamsPeer::OM_CLASS,
			assetParamsOutputPeer::OM_CLASS,
		);
		
		if(in_array($baseClass, $supportedBaseClasses) && $enumValue == assetType::FLAVOR)
		{
			return array(
				WidevinePlugin::getAssetTypeCoreValue(WidevineAssetType::WIDEVINE_FLAVOR),
			);
		}
		
		return null;		
	}

	/* (non-PHPdoc)
	 * @see IKontorolServices::getServicesMap()
	 */
	public static function getServicesMap() {
		$map = array(
			'widevineDrm' => 'WidevineDrmService',
		);
		return $map;	
	}

	/* (non-PHPdoc)
	 * @see IKontorolPermissions::isAllowedPartner()
	 */
	public static function isAllowedPartner($partnerId) {	
		$partner = PartnerPeer::retrieveByPK($partnerId);
		if(!$partner)
			return false;
		return $partner->getPluginEnabled(self::PLUGIN_NAME);			
	}
	
	/**
	 * @return array
	 */
	public static function getEventConsumers()
	{
		return array(
			self::WIDEVINE_EVENTS_CONSUMER,
		);
	}
	
	public static function getWidevineAssetIdSearchData($wvAssetId)
	{
		return self::getPluginName() . $wvAssetId . self::SEARCH_DATA_SUFFIX;
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolSearchDataContributor::getSearchData()
	 */
	public static function getSearchData(BaseObject $object)
	{
		if($object instanceof entry)
		{
			$c = new Criteria();
			$c->add(assetPeer::ENTRY_ID, $object->getId());		
			$flavorType = self::getAssetTypeCoreValue(WidevineAssetType::WIDEVINE_FLAVOR);
			$c->add(assetPeer::TYPE, $flavorType);		
			$wvFlavorAssets = assetPeer::doSelect($c);
			if(count($wvFlavorAssets))
			{			
				$searchData = array();
				foreach ($wvFlavorAssets as $wvFlavorAsset) 
				{
					$searchData[] = self::getWidevineAssetIdSearchData($wvFlavorAsset->getWidevineAssetId());
				}				
				return array('plugins_data' => implode(' ', $searchData));
			}
		}
			
		return null;
	}
	
	public static function getWidevineConfigParam($key)
	{
		return DrmPlugin::getConfigParam(self::PLUGIN_NAME, $key);
	}

	public function contributeToPlaybackContextDataResult(entry $entry, kPlaybackContextDataParams $entryPlayingDataParams, kPlaybackContextDataResult $result, kContextDataHelper $contextDataHelper)
	{
		if ($entryPlayingDataParams->getType() == self::BASE_PLUGIN_NAME && self::shouldContributeToPlaybackContext($contextDataHelper->getContextDataResult()->getActions()) && $this->isSupportStreamerTypes($entryPlayingDataParams->getDeliveryProfile()->getStreamerType()))
		{
			foreach ($entryPlayingDataParams->getFlavors() as $flavor)
			{
					if ( !in_array("widevine",explode(",",$flavor->getTags())))
						$result->addToFlavorIdsToRemove($flavor->getId());
			}

			$widevineProfile = DrmProfilePeer::retrieveByProviderAndPartnerID(WidevinePlugin::getWidevineProviderCoreValue(), kCurrentContext::getCurrentPartnerId());
			if (!$widevineProfile)
			{
				$widevineProfile = new WidevineProfile();
				$widevineProfile->setName('default');
			}

			if ($widevineProfile)
			{
				/* @var WidevineProfile $widevineProfile */

				$signingKey = kConf::get('signing_key', 'drm', null);
				if ($signingKey)
				{
					$customDataJson = DrmLicenseUtils::createCustomDataForEntry($entry->getId(), $entryPlayingDataParams->getFlavors(), $signingKey);
					$customDataObject = reset($customDataJson);
					$data = new kDrmPlaybackPluginData();
					$licenseUrl = $this->constructUrl($widevineProfile, self::getPluginName(), $customDataObject);
					$data->setLicenseURL(DrmLicenseUtils::prepareUrl($licenseUrl));
					$data->setScheme($this->getDrmSchemeCoreValue());
					$result->addToPluginData(self::getPluginName(), $data);
				}
			}
		}
	}

	/**
	 * @return int id of dynamic enum in the DB.
	 */
	public static function getDrmSchemeCoreValue()
	{
		$value = self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . WidevineSchemeName::WIDEVINE;
		return kPluginableEnumsManager::apiToCore('DrmSchemeName', $value);
	}


	public function isSupportStreamerTypes($streamerType)
	{
		return in_array($streamerType , array(PlaybackProtocol::HTTP));
	}

	public function constructUrl($widevineProfile, $scheme, $customDataObject)
	{
		return $widevineProfile->getLicenseServerUrl() . "/" . $scheme . "/license?custom_data=" . $customDataObject['custom_data'] . "&signature=" . $customDataObject['signature'];
	}

}

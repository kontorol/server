<?php
/**
 * @package plugins.widevine
 * @subpackage api.errors
 */
class KontorolWidevineLicenseProxyException extends Exception
{
	private $wvCode;
	
	public function __construct ($code = null) 
	{
		if(!$code || !is_int($code))
			$this->wvCode = KontorolWidevineErrorCodes::UNKNOWN_ERROR;
		else
			$this->wvCode = $code;
	}
	
	public function getWvErrorCode()
	{
		return $this->wvCode;
	}
}

<?php

class KAsyncWidevineRepositorySync extends KJobHandlerWorker
{	
	/* (non-PHPdoc)
	 * @see KBatchBase::getType()
	 */
	public static function getType()
	{
		return KontorolBatchJobType::WIDEVINE_REPOSITORY_SYNC;
	}
	
	/* (non-PHPdoc)
	 * @see KBatchBase::getJobType()
	 */
	public function getJobType()
	{
		return self::getType();
	}
	
	/* (non-PHPdoc)
	 * @see KJobHandlerWorker::exec()
	 */
	protected function exec(KontorolBatchJob $job)
	{
		return $this->syncRepository($job, $job->data);			
	}

	protected function syncRepository(KontorolBatchJob $job, KontorolWidevineRepositorySyncJobData $data)
	{
		$job = $this->updateJob($job, "Start synchronization of Widevine repository", KontorolBatchJobStatus::QUEUED);
				
		switch ($data->syncMode)
		{
			case KontorolWidevineRepositorySyncMode::MODIFY:
				$this->sendModifyRequest($job, $data);
				break;
			default:
				throw new kApplicativeException(null, "Unknown sync mode [".$data->syncMode. "]");
		}

		return $this->closeJob($job, null, null, "Sync request sent successfully", KontorolBatchJobStatus::FINISHED, $data);
	}		

	/**
	 * Send asset notify request to VOD Dealer to update widevine assets
	 * 
	 * @param KontorolBatchJob $job
	 * @param KontorolWidevineRepositorySyncJobData $data
	 */
	private function sendModifyRequest(KontorolBatchJob $job, KontorolWidevineRepositorySyncJobData $data)
	{
		$dataWrap = new WidevineRepositorySyncJobDataWrap($data);		
		$widevineAssets = $dataWrap->getWidevineAssetIds();
		$licenseStartDate = $dataWrap->getLicenseStartDate();
		$licenseEndDate = $dataWrap->getLicenseEndDate();

		$this->impersonate($job->partnerId);

		$drmPlugin = KontorolDrmClientPlugin::get(KBatchBase::$kClient);
		$profile = $drmPlugin->drmProfile->getByProvider(KontorolDrmProviderType::WIDEVINE);

		foreach ($widevineAssets as $assetId) 
		{
			$this->updateWidevineAsset($assetId, $licenseStartDate, $licenseEndDate, $profile);
		}
		
		if($data->monitorSyncCompletion)
			$this->updateFlavorAssets($job, $dataWrap);

		$this->unimpersonate();
	}
	
	/**
	 * Execute register asset with new details to update exisiting asset
	 * 
	 * @param int $assetId
	 * @param string $licenseStartDate
	 * @param string $licenseEndDate
	 * @throws kApplicativeException
	 */
	private function updateWidevineAsset($assetId, $licenseStartDate, $licenseEndDate, $profile)
	{
		KontorolLog::debug("Update asset [".$assetId."] license start date [".$licenseStartDate.'] license end date ['.$licenseEndDate.']');
		
		$errorMessage = '';
		
		$wvAssetId = KWidevineBatchHelper::sendRegisterAssetRequest(
										$profile->regServerHost,
										null,
										$assetId,
										$profile->portal,
										null,
										$licenseStartDate,
										$licenseEndDate,
										$profile->iv, 
										$profile->key, 									
										$errorMessage);				
		
		if(!$wvAssetId)
		{
			KBatchBase::unimpersonate();
			
			$logMessage = 'Asset update failed, asset id: '.$assetId.' error: '.$errorMessage;
			KontorolLog::err($logMessage);
			throw new kApplicativeException(null, $logMessage);
		}			
	}
	
	/**
	 * Update flavorAsset in Kontorol after the distribution dates apllied to Wideivne asset
	 * 
	 * @param KontorolBatchJob $job
	 * @param WidevineRepositorySyncJobDataWrap $dataWrap
	 */
	private function updateFlavorAssets(KontorolBatchJob $job, WidevineRepositorySyncJobDataWrap $dataWrap)
	{	
		$startDate = $dataWrap->getLicenseStartDate();
		$endDate = $dataWrap->getLicenseEndDate();	
		
		$filter = new KontorolAssetFilter();
		$filter->entryIdEqual = $job->entryId;
		$filter->tagsLike = 'widevine';
		$flavorAssetsList = self::$kClient->flavorAsset->listAction($filter, new KontorolFilterPager());
		
		foreach ($flavorAssetsList->objects as $flavorAsset) 
		{
			if($flavorAsset instanceof KontorolWidevineFlavorAsset && $dataWrap->hasAssetId($flavorAsset->widevineAssetId))
			{
				$updatedFlavorAsset = new KontorolWidevineFlavorAsset();
				$updatedFlavorAsset->widevineDistributionStartDate = $startDate;
				$updatedFlavorAsset->widevineDistributionEndDate = $endDate;
				self::$kClient->flavorAsset->update($flavorAsset->id, $updatedFlavorAsset);
			}		
		}		
	}
}

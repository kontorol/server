<?php
/**
 * @package plugins.playReady
 * @subpackage api.objects
 */
class KontorolPlayReadyCopyEnablerHolderArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolPlayReadyCopyEnablerHolderArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $type)
		{
    		$nObj = new KontorolPlayReadyCopyEnablerHolder();
			$nObj->type = $type;
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct()
	{
		parent::__construct("KontorolPlayReadyCopyEnablerHolder");
	}
}

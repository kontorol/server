<?php
/**
 * @package plugins.playReady
 * @subpackage api.objects
 */
class KontorolPlayReadyAnalogVideoOPIdHolderArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolPlayReadyAnalogVideoOPIdHolderArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $type)
		{
    		$nObj = new KontorolPlayReadyAnalogVideoOPIdHolder();
			$nObj->type = $type;
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct()
	{
		parent::__construct("KontorolPlayReadyAnalogVideoOPIdHolder");
	}
}

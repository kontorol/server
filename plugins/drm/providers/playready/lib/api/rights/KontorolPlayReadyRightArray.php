<?php
/**
 * @package plugins.playReady
 * @subpackage api.objects
 */
class KontorolPlayReadyRightArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolPlayReadyRightArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
			$nObj = self::getInstanceByDbObject($obj);
			if($nObj)
			{
				$nObj->fromObject($obj, $responseProfile);
				$newArr[] = $nObj;
			}
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolPlayReadyRight");
	}
	
	private static function getInstanceByDbObject($obj)
	{
		if($obj instanceof PlayReadyCopyRight)
			return new KontorolPlayReadyCopyRight();
		if($obj instanceof PlayReadyPlayRight)
			return new KontorolPlayReadyPlayRight();
			
		return null;
	}
}

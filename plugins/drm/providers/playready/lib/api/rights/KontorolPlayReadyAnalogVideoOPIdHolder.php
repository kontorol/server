<?php
/**
 * @package plugins.playReady
 * @subpackage api.objects
 */
class KontorolPlayReadyAnalogVideoOPIdHolder extends KontorolObject
{
	/**
	 * The type of the play enabler
	 * 
	 * @var KontorolPlayReadyAnalogVideoOPId
	 */
	public $type;
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbObject = null, $skip = array())
	{
		return $this->type;
	}
	
	private static $mapBetweenObjects = array
	(
		'type',
	);
	
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$mapBetweenObjects);
	}
}

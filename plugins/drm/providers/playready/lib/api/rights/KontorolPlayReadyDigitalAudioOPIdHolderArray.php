<?php
/**
 * @package plugins.playReady
 * @subpackage api.objects
 */
class KontorolPlayReadyDigitalAudioOPIdHolderArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolPlayReadyDigitalAudioOPIdHolderArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $type)
		{
    		$nObj = new KontorolPlayReadyDigitalAudioOPIdHolder();
			$nObj->type = $type;
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct()
	{
		parent::__construct("KontorolPlayReadyDigitalAudioOPIdHolder");
	}
}

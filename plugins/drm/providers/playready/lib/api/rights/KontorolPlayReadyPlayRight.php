<?php
/**
 * @package plugins.playReady
 * @subpackage api.objects
 */
class KontorolPlayReadyPlayRight extends KontorolPlayReadyRight
{
    /**
	 * @var KontorolPlayReadyAnalogVideoOPL
	 */
	public $analogVideoOPL ;
	
	/**
	 * @var KontorolPlayReadyAnalogVideoOPIdHolderArray
	 */
	public $analogVideoOutputProtectionList ;
	
    /**
	 * @var KontorolPlayReadyDigitalAudioOPL
	 */
	public $compressedDigitalAudioOPL ;
	
    /**
	 * @var KontorolPlayReadyCompressedDigitalVideoOPL
	 */
	public $compressedDigitalVideoOPL ;

	/**
	 * @var KontorolPlayReadyDigitalAudioOPIdHolderArray
	 */
	public $digitalAudioOutputProtectionList; 
	
	/**
	 * @var KontorolPlayReadyDigitalAudioOPL
	 */	
	public $uncompressedDigitalAudioOPL;

    /**
	 * @var KontorolPlayReadyUncompressedDigitalVideoOPL
	 */
	public $uncompressedDigitalVideoOPL; 
	
    /**
	 * @var int
	 */
	public $firstPlayExpiration;
	
    /**
	 * @var KontorolPlayReadyPlayEnablerHolderArray
	 */
	public $playEnablers; 
	
	
	private static $map_between_objects = array(
		'analogVideoOPL',
    	'analogVideoOutputProtectionList',
    	'compressedDigitalAudioOPL',
    	'compressedDigitalVideoOPL',
		'digitalAudioOutputProtectionList',
		'uncompressedDigitalAudioOPL',
		'uncompressedDigitalVideoOPL',
		'firstPlayExpiration',
		'playEnablers',
	 );
		 
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}
	
	public function toObject($dbObject = null, $skip = array())
	{
		if (is_null($dbObject))
			$dbObject = new PlayReadyPlayRight();
			
		parent::toObject($dbObject, $skip);
					
		return $dbObject;
	}
}



<?php
/**
 * @package plugins.playReady
 * @subpackage api.objects
 */
class KontorolPlayReadyPolicy extends KontorolDrmPolicy
{
    /**
	 * @var int
	 */
	public $gracePeriod;	
	
	/**
	 * @var KontorolPlayReadyLicenseRemovalPolicy
	 */
	public $licenseRemovalPolicy;	
	
	/**
	 * @var int
	 */
	public $licenseRemovalDuration;	
	
	/**
	 * @var KontorolPlayReadyMinimumLicenseSecurityLevel
	 */
	public $minSecurityLevel;	
	
	/**
	 * @var KontorolPlayReadyRightArray
	 */
	public $rights;	
	
	
	private static $map_between_objects = array(
		'gracePeriod',
		'licenseRemovalPolicy',
		'licenseRemovalDuration',
		'minSecurityLevel',
		'rights',
	 );
		 
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}
	
	public function toObject($dbObject = null, $skip = array())
	{
		if (is_null($dbObject))
			$dbObject = new PlayReadyPolicy();
			
		parent::toObject($dbObject, $skip);
					
		return $dbObject;
	}
	
	public function validatePolicy()
	{
		if(count($this->rights))
		{
			foreach ($this->rights as $right) 
			{
				if($right instanceof KontorolPlayReadyPlayRight)
				{
					$this->validatePlayRight($right);
				}
				else if($right instanceof KontorolPlayReadyCopyRight)
				{
					$this->validateCopyRight($right);
				}
			}
		}
		
		parent::validatePolicy();
	}
	
	private function validatePlayRight(KontorolPlayReadyPlayRight $right)
	{
		if(	count($right->analogVideoOutputProtectionList) && 
			in_array(KontorolPlayReadyAnalogVideoOPId::EXPLICIT_ANALOG_TV, $right->analogVideoOutputProtectionList) &&
			in_array(KontorolPlayReadyAnalogVideoOPId::BEST_EFFORT_EXPLICIT_ANALOG_TV, $right->analogVideoOutputProtectionList))
		{
			throw new KontorolAPIException(KontorolPlayReadyErrors::ANALOG_OUTPUT_PROTECTION_ID_NOT_ALLOWED, KontorolPlayReadyAnalogVideoOPId::EXPLICIT_ANALOG_TV, KontorolPlayReadyAnalogVideoOPId::BEST_EFFORT_EXPLICIT_ANALOG_TV);
		}
	}
	
	private function validateCopyRight(KontorolPlayReadyCopyRight $right)
	{
		if($right->copyCount > 0 && !count($right->copyEnablers))
		{
			throw new KontorolAPIException(KontorolPlayReadyErrors::COPY_ENABLER_TYPE_MISSING);
		}
	}
	
}


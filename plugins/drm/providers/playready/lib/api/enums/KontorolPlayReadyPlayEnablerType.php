<?php
/**
 * @package plugins.playReady
 * @subpackage api.enum
 */
class KontorolPlayReadyPlayEnablerType extends KontorolStringEnum implements PlayReadyPlayEnablerType
{
	// see PlayReadyPlayEnablerType interface
}

<?php
/**
 * @package plugins.playReady
 * @subpackage api.enum
 */
class KontorolPlayReadyAnalogVideoOPId extends KontorolStringEnum implements PlayReadyAnalogVideoOPId
{
	// see PlayReadyDigitalAudioOPId interface
}

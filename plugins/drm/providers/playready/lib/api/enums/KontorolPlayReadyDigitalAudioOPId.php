<?php
/**
 * @package plugins.playReady
 * @subpackage api.enum
 */
class KontorolPlayReadyDigitalAudioOPId extends KontorolStringEnum implements PlayReadyDigitalAudioOPId
{
	// see PlayReadyDigitalAudioOPId interface
}

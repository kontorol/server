<?php
/**
 * @package plugins.playReady
 * @subpackage api.enum
 */
class KontorolPlayReadyCopyEnablerType extends KontorolStringEnum implements PlayReadyCopyEnablerType
{
	// see PlayReadyCopyEnablerType interface
}

<?php
/**
 * @package plugins.playReady
 * @subpackage api.enum
 */
class KontorolPlayReadyMinimumLicenseSecurityLevel extends KontorolEnum implements PlayReadyMinimumLicenseSecurityLevel
{
	// see PlayReadyMinimumLicenseSecurityLevel interface
}

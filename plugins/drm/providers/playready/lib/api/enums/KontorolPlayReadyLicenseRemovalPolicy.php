<?php
/**
 * @package plugins.playReady
 * @subpackage api.enum
 */
class KontorolPlayReadyLicenseRemovalPolicy extends KontorolEnum implements PlayReadyLicenseRemovalPolicy
{
	// see PlayReadyLicenseRemovalPolicy interface
}

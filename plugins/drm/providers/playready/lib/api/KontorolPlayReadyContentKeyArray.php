<?php
/**
 * @package plugins.playReady
 * @subpackage api.objects
 */
class KontorolPlayReadyContentKeyArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolPlayReadyContentKeyArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
			$nObj = new KontorolPlayReadyContentKey();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolPlayReadyContentKey");
	}
}

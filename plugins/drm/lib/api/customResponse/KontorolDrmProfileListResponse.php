<?php
/**
 * @package plugins.drm
 * @subpackage api.objects
 */
class KontorolDrmProfileListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolDrmProfileArray
	 * @readonly
	 */
	public $objects;
}

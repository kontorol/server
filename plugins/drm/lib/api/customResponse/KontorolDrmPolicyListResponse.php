<?php
/**
 * @package plugins.drm
 * @subpackage api.objects
 */
class KontorolDrmPolicyListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolDrmPolicyArray
	 * @readonly
	 */
	public $objects;
}

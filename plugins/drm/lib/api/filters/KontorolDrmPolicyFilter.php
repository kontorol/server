<?php
/**
 * @package plugins.drm
 * @subpackage api.filters
 */
class KontorolDrmPolicyFilter extends KontorolDrmPolicyBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new DrmPolicyFilter();
	}
}

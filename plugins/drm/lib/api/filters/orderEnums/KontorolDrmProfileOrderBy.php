<?php
/**
 * @package plugins.drm
 * @subpackage api.filters.enum
 */
class KontorolDrmProfileOrderBy extends KontorolStringEnum
{
	const ID_ASC = "+id";
	const ID_DESC = "-id";
	const NAME_ASC = "+name";
	const NAME_DESC = "-name";
}

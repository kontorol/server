<?php
/**
 * @package plugins.drm
 * @subpackage api.filters
 */
class KontorolDrmProfileFilter extends KontorolDrmProfileBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new DrmProfileFilter();
	}
}

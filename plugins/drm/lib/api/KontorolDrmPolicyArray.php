<?php
/**
 * @package plugins.drm
 * @subpackage api.objects
 */
class KontorolDrmPolicyArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolDrmPolicyArray();
		foreach ( $arr as $obj )
		{
		    $nObj = KontorolDrmPolicy::getInstanceByType($obj->getProvider());
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
		 
	}
	
	public function __construct( )
	{
		return parent::__construct ( 'KontorolDrmPolicy' );
	}
}

<?php
/**
 * @package plugins.drm
 * @subpackage api.enum
 */
class KontorolDrmLicenseScenario extends KontorolDynamicEnum implements DrmLicenseScenario
{
	public static function getEnumClass()
	{
		return 'DrmLicenseScenario';
	}
}

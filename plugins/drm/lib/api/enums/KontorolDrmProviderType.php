<?php
/**
 * @package plugins.drm
 * @subpackage api.enum
 */
class KontorolDrmProviderType extends KontorolDynamicEnum implements DrmProviderType
{
	public static function getEnumClass()
	{
		return 'DrmProviderType';
	}
}

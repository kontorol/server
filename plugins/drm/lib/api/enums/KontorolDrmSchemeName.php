<?php
/**
 * @package api
 * @subpackage api.enum
 */
class KontorolDrmSchemeName extends KontorolDynamicEnum implements DrmSchemeName
{
	public static function getEnumClass()
	{
		return 'DrmSchemeName';
	}
}

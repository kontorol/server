<?php
/**
 * @package plugins.drm
 * @subpackage api.enum
 */
class KontorolDrmPolicyStatus extends KontorolEnum implements DrmPolicyStatus
{
	// see DrmPolicyStatus interface
}

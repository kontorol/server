<?php
/**
 * @package plugins.drm
 * @subpackage api.enum
 */
class KontorolDrmProfileStatus extends KontorolEnum implements DrmProfileStatus
{
	// see DrmProfileStatus interface
}

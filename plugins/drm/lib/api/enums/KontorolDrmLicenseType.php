<?php
/**
 * @package plugins.drm
 * @subpackage api.enum
 */
class KontorolDrmLicenseType extends KontorolDynamicEnum implements DrmLicenseType
{
	public static function getEnumClass()
	{
		return 'DrmLicenseType';
	}
}

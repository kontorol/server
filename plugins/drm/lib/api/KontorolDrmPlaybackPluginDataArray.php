<?php
/**
 * @package plugins.drm
 * @subpackage api.objects
 */
class KontorolDrmPlaybackPluginDataArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolDrmPlaybackPluginDataArray();
		foreach ( $arr as $obj )
		{
			$nObj = KontorolPluginManager::loadObject('KontorolDrmPlaybackPluginData', get_class($obj));
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
		 
	}
	
	public function __construct( )
	{
		return parent::__construct ( 'KontorolDrmPlaybackPluginData' );
	}
}

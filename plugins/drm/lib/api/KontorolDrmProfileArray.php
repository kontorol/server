<?php
/**
 * @package plugins.drm
 * @subpackage api.objects
 */
class KontorolDrmProfileArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolDrmProfileArray();
		foreach ( $arr as $obj )
		{
		    $nObj = KontorolDrmProfile::getInstanceByType($obj->getProvider());
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
		 
	}
	
	public function __construct( )
	{
		return parent::__construct ( 'KontorolDrmProfile' );
	}
}

<?php
/**
 * 
 * @service drmProfile
 * @package plugins.drm
 * @subpackage api.services
 */
class DrmProfileService extends KontorolBaseService
{
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		$this->applyPartnerFilterForClass('DrmProfile');
		
		if (!DrmPlugin::isAllowedPartner($this->getPartnerId()))
			throw new KontorolAPIException(KontorolErrors::FEATURE_FORBIDDEN, DrmPlugin::PLUGIN_NAME);
	}
	
	/**
	 * Allows you to add a new DrmProfile object
	 * 
	 * @action add
	 * @param KontorolDrmProfile $drmProfile
	 * @return KontorolDrmProfile
	 * 
	 * @throws KontorolErrors::PLUGIN_NOT_AVAILABLE_FOR_PARTNER
	 * @throws KontorolErrors::INVALID_PARTNER_ID
	 * @throws KontorolErrors::PROPERTY_VALIDATION_CANNOT_BE_NULL
	 * @throws DrmErrors::ACTIVE_PROVIDER_PROFILE_ALREADY_EXIST
	 */
	public function addAction(KontorolDrmProfile $drmProfile)
	{
		// check for required parameters
		$drmProfile->validatePropertyNotNull('name');
		$drmProfile->validatePropertyNotNull('status');
		$drmProfile->validatePropertyNotNull('provider');
		$drmProfile->validatePropertyNotNull('partnerId');
		
		// validate values						
		if (!PartnerPeer::retrieveByPK($drmProfile->partnerId)) {
			throw new KontorolAPIException(KontorolErrors::INVALID_PARTNER_ID, $drmProfile->partnerId);
		}
		
		if (!DrmPlugin::isAllowedPartner($drmProfile->partnerId))
		{
			throw new KontorolAPIException(KontorolErrors::PLUGIN_NOT_AVAILABLE_FOR_PARTNER, DrmPlugin::getPluginName(), $drmProfile->partnerId);
		}
		
		$dbDrmProfile = $drmProfile->toInsertableObject();
		
		if(DrmProfilePeer::retrieveByProvider($dbDrmProfile->getProvider()))
		{
			throw new KontorolAPIException(DrmErrors::ACTIVE_PROVIDER_PROFILE_ALREADY_EXIST, $drmProfile->provider);
		}

		// save in database
		
		$dbDrmProfile->save();
		
		// return the saved object
		$drmProfile = KontorolDrmProfile::getInstanceByType($dbDrmProfile->getProvider());
		$drmProfile->fromObject($dbDrmProfile, $this->getResponseProfile());
		return $drmProfile;		
	}
	
	/**
	 * Retrieve a KontorolDrmProfile object by ID
	 * 
	 * @action get
	 * @param int $drmProfileId 
	 * @return KontorolDrmProfile
	 * 
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 */		
	public function getAction($drmProfileId)
	{
		$dbDrmProfile = DrmProfilePeer::retrieveByPK($drmProfileId);
		
		if (!$dbDrmProfile) {
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $drmProfileId);
		}
		$drmProfile = KontorolDrmProfile::getInstanceByType($dbDrmProfile->getProvider());
		$drmProfile->fromObject($dbDrmProfile, $this->getResponseProfile());
		
		return $drmProfile;
	}
	

	/**
	 * Update an existing KontorolDrmProfile object
	 * 
	 * @action update
	 * @param int $drmProfileId
	 * @param KontorolDrmProfile $drmProfile
	 * @return KontorolDrmProfile
	 *
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 */	
	public function updateAction($drmProfileId, KontorolDrmProfile $drmProfile)
	{
		$dbDrmProfile = DrmProfilePeer::retrieveByPK($drmProfileId);
		
		if (!$dbDrmProfile) {
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $drmProfileId);
		}
								
		$dbDrmProfile = $drmProfile->toUpdatableObject($dbDrmProfile);
		$dbDrmProfile->save();
			
		$drmProfile = KontorolDrmProfile::getInstanceByType($dbDrmProfile->getProvider());
		$drmProfile->fromObject($dbDrmProfile, $this->getResponseProfile());
		
		return $drmProfile;
	}

	/**
	 * Mark the KontorolDrmProfile object as deleted
	 * 
	 * @action delete
	 * @param int $drmProfileId 
	 * @return KontorolDrmProfile
	 *
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 */		
	public function deleteAction($drmProfileId)
	{
		$dbDrmProfile = DrmProfilePeer::retrieveByPK($drmProfileId);
		
		if (!$dbDrmProfile) {
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $drmProfileId);
		}

		$dbDrmProfile->setStatus(DrmProfileStatus::DELETED);
		$dbDrmProfile->save();
			
		$drmProfile = KontorolDrmProfile::getInstanceByType($dbDrmProfile->getProvider());
		$drmProfile->fromObject($dbDrmProfile, $this->getResponseProfile());
		
		return $drmProfile;
	}
	
	/**
	 * List KontorolDrmProfile objects
	 * 
	 * @action list
	 * @param KontorolDrmProfileFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return KontorolDrmProfileListResponse
	 */
	public function listAction(KontorolDrmProfileFilter  $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
			$filter = new KontorolDrmProfileFilter();

		$drmProfileFilter = $filter->toObject();
		$c = new Criteria();
		$drmProfileFilter->attachToCriteria($c);
		$count = DrmProfilePeer::doCount($c);
		if (! $pager)
			$pager = new KontorolFilterPager ();
		$pager->attachToCriteria ( $c );
		$list = DrmProfilePeer::doSelect($c);
		
		$response = new KontorolDrmProfileListResponse();
		$response->objects = KontorolDrmProfileArray::fromDbArray($list, $this->getResponseProfile());
		$response->totalCount = $count;
		
		return $response;
	}
	
	/**
	 * Retrieve a KontorolDrmProfile object by provider, if no specific profile defined return default profile
	 * 
	 * @action getByProvider
	 * @param KontorolDrmProviderType $provider
	 * @return KontorolDrmProfile
	 */
	public function getByProviderAction($provider)
	{	
		$drmProfile = KontorolDrmProfile::getInstanceByType($provider);
		$drmProfile->provider = $provider;
		$tmpDbProfile = $drmProfile->toObject();
			
		$dbDrmProfile = DrmProfilePeer::retrieveByProvider($tmpDbProfile->getProvider());
		if(!$dbDrmProfile)
		{
            if ($provider == KontorolDrmProviderType::CENC)
            {
                $dbDrmProfile = new DrmProfile();
            }
            else
            {
                $dbDrmProfile = KontorolPluginManager::loadObject('DrmProfile', $tmpDbProfile->getProvider());
            }
			$dbDrmProfile->setName('default');
			$dbDrmProfile->setProvider($tmpDbProfile->getProvider());
		}		
		$drmProfile->fromObject($dbDrmProfile, $this->getResponseProfile());

		return $drmProfile;
	}
}

<?php
/**
 * 
 * @service drmPolicy
 * @package plugins.drm
 * @subpackage api.services
 */
class DrmPolicyService extends KontorolBaseService
{	
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		if (!DrmPlugin::isAllowedPartner($this->getPartnerId()))
			throw new KontorolAPIException(KontorolErrors::SERVICE_FORBIDDEN, $this->serviceName.'->'.$this->actionName);
			
		$this->applyPartnerFilterForClass('DrmPolicy');
	}
	
	/**
	 * Allows you to add a new DrmPolicy object
	 * 
	 * @action add
	 * @param KontorolDrmPolicy $drmPolicy
	 * @return KontorolDrmPolicy
	 * 
	 * @throws KontorolErrors::PROPERTY_VALIDATION_CANNOT_BE_NULL
	 */
	public function addAction(KontorolDrmPolicy $drmPolicy)
	{
		// check for required parameters
		$drmPolicy->validatePropertyNotNull('name');
		$drmPolicy->validatePropertyNotNull('status');
		$drmPolicy->validatePropertyNotNull('provider');
		$drmPolicy->validatePropertyNotNull('systemName');
		$drmPolicy->validatePropertyNotNull('scenario');
		$drmPolicy->validatePropertyNotNull('partnerId');
		
		// validate values
		$drmPolicy->validatePolicy();
						
		if (!PartnerPeer::retrieveByPK($drmPolicy->partnerId)) {
			throw new KontorolAPIException(KontorolErrors::INVALID_PARTNER_ID, $drmPolicy->partnerId);
		}
		
		if (!DrmPlugin::isAllowedPartner($drmPolicy->partnerId))
		{
			throw new KontorolAPIException(KontorolErrors::PLUGIN_NOT_AVAILABLE_FOR_PARTNER, DrmPlugin::getPluginName(), $drmPolicy->partnerId);
		}

		if(DrmPolicyPeer::retrieveBySystemName($drmPolicy->systemName))
		{
			throw new KontorolAPIException(DrmErrors::DRM_POLICY_DUPLICATE_SYSTEM_NAME, $drmPolicy->systemName);
		}
				
		// save in database
		$dbDrmPolicy = $drmPolicy->toInsertableObject();
		$dbDrmPolicy->save();
		
		// return the saved object
		$drmPolicy = KontorolDrmPolicy::getInstanceByType($dbDrmPolicy->getProvider());
		$drmPolicy->fromObject($dbDrmPolicy, $this->getResponseProfile());
		return $drmPolicy;
		
	}
	
	/**
	 * Retrieve a KontorolDrmPolicy object by ID
	 * 
	 * @action get
	 * @param int $drmPolicyId 
	 * @return KontorolDrmPolicy
	 * 
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 */		
	public function getAction($drmPolicyId)
	{
		$dbDrmPolicy = DrmPolicyPeer::retrieveByPK($drmPolicyId);
		
		if (!$dbDrmPolicy) {
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $drmPolicyId);
		}
			
		$drmPolicy = KontorolDrmPolicy::getInstanceByType($dbDrmPolicy->getProvider());
		$drmPolicy->fromObject($dbDrmPolicy, $this->getResponseProfile());
		
		return $drmPolicy;
	}
	

	/**
	 * Update an existing KontorolDrmPolicy object
	 * 
	 * @action update
	 * @param int $drmPolicyId
	 * @param KontorolDrmPolicy $drmPolicy
	 * @return KontorolDrmPolicy
	 *
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 */	
	public function updateAction($drmPolicyId, KontorolDrmPolicy $drmPolicy)
	{
		$dbDrmPolicy = DrmPolicyPeer::retrieveByPK($drmPolicyId);
		
		if (!$dbDrmPolicy) {
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $drmPolicyId);
		}
		
		$drmPolicy->validatePolicy();
						
		$dbDrmPolicy = $drmPolicy->toUpdatableObject($dbDrmPolicy);
		$dbDrmPolicy->save();
	
		$drmPolicy = KontorolDrmPolicy::getInstanceByType($dbDrmPolicy->getProvider());
		$drmPolicy->fromObject($dbDrmPolicy, $this->getResponseProfile());
		
		return $drmPolicy;
	}

	/**
	 * Mark the KontorolDrmPolicy object as deleted
	 * 
	 * @action delete
	 * @param int $drmPolicyId 
	 * @return KontorolDrmPolicy
	 *
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 */		
	public function deleteAction($drmPolicyId)
	{
		$dbDrmPolicy = DrmPolicyPeer::retrieveByPK($drmPolicyId);
		
		if (!$dbDrmPolicy) {
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $drmPolicyId);
		}

		$dbDrmPolicy->setStatus(DrmPolicyStatus::DELETED);
		$dbDrmPolicy->save();
			
		$drmPolicy = KontorolDrmPolicy::getInstanceByType($dbDrmPolicy->getProvider());
		$drmPolicy->fromObject($dbDrmPolicy, $this->getResponseProfile());
		
		return $drmPolicy;
	}
	
	/**
	 * List KontorolDrmPolicy objects
	 * 
	 * @action list
	 * @param KontorolDrmPolicyFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return KontorolDrmPolicyListResponse
	 */
	public function listAction(KontorolDrmPolicyFilter  $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
			$filter = new KontorolDrmPolicyFilter();
			
		$drmPolicyFilter = $filter->toObject();

		$c = new Criteria();
		$drmPolicyFilter->attachToCriteria($c);
		$count = DrmPolicyPeer::doCount($c);		
		if (! $pager)
			$pager = new KontorolFilterPager ();
		$pager->attachToCriteria ( $c );
		$list = DrmPolicyPeer::doSelect($c);
		
		$response = new KontorolDrmPolicyListResponse();
		$response->objects = KontorolDrmPolicyArray::fromDbArray($list, $this->getResponseProfile());
		$response->totalCount = $count;
		
		return $response;
	}

}

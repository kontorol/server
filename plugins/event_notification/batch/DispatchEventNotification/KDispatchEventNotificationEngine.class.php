<?php
/**
 * @package plugins.eventNotification
 * @subpackage Scheduler
 */
abstract class KDispatchEventNotificationEngine
{	
	
	/**
	 * @param KontorolEventNotificationTemplate $eventNotificationTemplate
	 * @param KontorolEventNotificationDispatchJobData $data
	 */
	abstract public function dispatch(KontorolEventNotificationTemplate $eventNotificationTemplate, KontorolEventNotificationDispatchJobData &$data);
}

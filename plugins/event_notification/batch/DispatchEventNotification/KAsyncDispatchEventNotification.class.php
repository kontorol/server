<?php
/**
 * @package plugins.eventNotification
 * @subpackage Scheduler
 */
class KAsyncDispatchEventNotification extends KJobHandlerWorker
{
	/* (non-PHPdoc)
	 * @see KBatchBase::getType()
	 */
	public static function getType()
	{
		return KontorolBatchJobType::EVENT_NOTIFICATION_HANDLER;
	}
	
	/* (non-PHPdoc)
	 * @see KJobHandlerWorker::exec()
	 */
	protected function exec(KontorolBatchJob $job)
	{
		return $this->dispatch($job, $job->data);
	}
	
	protected function dispatch(KontorolBatchJob $job, KontorolEventNotificationDispatchJobData $data)
	{
		$this->updateJob($job, "Dispatch template [$data->templateId]", KontorolBatchJobStatus::QUEUED);
		
		$eventNotificationPlugin = KontorolEventNotificationClientPlugin::get(self::$kClient);
		$eventNotificationTemplate = $eventNotificationPlugin->eventNotificationTemplate->get($data->templateId);
		
		$engine = $this->getEngine($job->jobSubType);
		if(!$engine)
			return $this->closeJob($job, KontorolBatchJobErrorTypes::APP, KontorolBatchJobAppErrors::ENGINE_NOT_FOUND, "Engine not found", KontorolBatchJobStatus::FAILED);
		
		$this->impersonate($job->partnerId);
		$engine->dispatch($eventNotificationTemplate, $data);
		$this->unimpersonate();
		
		return $this->closeJob($job, null, null, "Dispatched", KontorolBatchJobStatus::FINISHED, $data);
	}

	/**
	 * @param KontorolEventNotificationTemplateType $type
	 * @return KDispatchEventNotificationEngine
	 */
	protected function getEngine($type)
	{
		return KontorolPluginManager::loadObject('KDispatchEventNotificationEngine', $type);
	}
}

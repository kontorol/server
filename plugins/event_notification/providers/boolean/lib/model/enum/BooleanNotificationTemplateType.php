<?php
/**
 * @package plugins.booleanNotification
 * @subpackage model.enum
 */
class BooleanNotificationTemplateType implements IKontorolPluginEnum, EventNotificationTemplateType
{
	const BOOLEAN = 'Boolean';

	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalValues()
	 */
	public static function getAdditionalValues()
	{
		return array(
			'BOOLEAN' => self::BOOLEAN,
		);
	}

	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalDescriptions()
	 */
	public static function getAdditionalDescriptions()
	{
		return array(
			self::BOOLEAN => 'Boolean event notification',
		);
	}
}

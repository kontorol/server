<?php
/**
 * Business-process case service lets you get information about processes
 * @service businessProcessCase
 * @package plugins.businessProcessNotification
 * @subpackage api.services
 */
class BusinessProcessCaseService extends KontorolBaseService
{
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		
		$partnerId = $this->getPartnerId();
		if (!EventNotificationPlugin::isAllowedPartner($partnerId))
			throw new KontorolAPIException(KontorolErrors::FEATURE_FORBIDDEN, EventNotificationPlugin::PLUGIN_NAME);
			
		$this->applyPartnerFilterForClass('EventNotificationTemplate');
	}
	
	/**
	 * Abort business-process case
	 * 
	 * @action abort
	 * @param KontorolEventNotificationEventObjectType $objectType
	 * @param string $objectId
	 * @param int $businessProcessStartNotificationTemplateId
	 *
	 * @throws KontorolEventNotificationErrors::EVENT_NOTIFICATION_TEMPLATE_NOT_FOUND
	 * @throws KontorolBusinessProcessNotificationErrors::BUSINESS_PROCESS_CASE_NOT_FOUND
	 * @throws KontorolBusinessProcessNotificationErrors::BUSINESS_PROCESS_SERVER_NOT_FOUND
	 */		
	public function abortAction($objectType, $objectId, $businessProcessStartNotificationTemplateId)
	{
		$dbObject = kEventNotificationFlowManager::getObject($objectType, $objectId);
		if(!$dbObject)
		{
			throw new KontorolAPIException(KontorolErrors::OBJECT_NOT_FOUND);
		}
		
		$dbTemplate = EventNotificationTemplatePeer::retrieveByPK($businessProcessStartNotificationTemplateId);
		if(!$dbTemplate || !($dbTemplate instanceof BusinessProcessStartNotificationTemplate))
		{
			throw new KontorolAPIException(KontorolEventNotificationErrors::EVENT_NOTIFICATION_TEMPLATE_NOT_FOUND, $businessProcessStartNotificationTemplateId);
		}
		
		$caseIds = $dbTemplate->getCaseIds($dbObject, false);
		if(!count($caseIds))
		{
			throw new KontorolAPIException(KontorolBusinessProcessNotificationErrors::BUSINESS_PROCESS_CASE_NOT_FOUND);
		}
		
		$dbBusinessProcessServer = BusinessProcessServerPeer::retrieveByPK($dbTemplate->getServerId());
		if (!$dbBusinessProcessServer)
		{
			throw new KontorolAPIException(KontorolBusinessProcessNotificationErrors::BUSINESS_PROCESS_SERVER_NOT_FOUND, $dbTemplate->getServerId());
		}
		
		$server = new KontorolActivitiBusinessProcessServer();
		$server->fromObject($dbBusinessProcessServer);
		$provider = kBusinessProcessProvider::get($server);
		
		foreach($caseIds as $caseId)
		{
			$provider->abortCase($caseId);
		}
	}

	/**
	 * Server business-process case diagram
	 * 
	 * @action serveDiagram
	 * @param KontorolEventNotificationEventObjectType $objectType
	 * @param string $objectId
	 * @param int $businessProcessStartNotificationTemplateId
	 * @return file
	 *
	 * @throws KontorolEventNotificationErrors::EVENT_NOTIFICATION_TEMPLATE_NOT_FOUND
	 * @throws KontorolBusinessProcessNotificationErrors::BUSINESS_PROCESS_CASE_NOT_FOUND
	 * @throws KontorolBusinessProcessNotificationErrors::BUSINESS_PROCESS_SERVER_NOT_FOUND
	 */		
	public function serveDiagramAction($objectType, $objectId, $businessProcessStartNotificationTemplateId)
	{
		$dbObject = kEventNotificationFlowManager::getObject($objectType, $objectId);
		if(!$dbObject)
		{
			throw new KontorolAPIException(KontorolErrors::OBJECT_NOT_FOUND);
		}
		
		$dbTemplate = EventNotificationTemplatePeer::retrieveByPK($businessProcessStartNotificationTemplateId);
		if(!$dbTemplate || !($dbTemplate instanceof BusinessProcessStartNotificationTemplate))
		{
			throw new KontorolAPIException(KontorolEventNotificationErrors::EVENT_NOTIFICATION_TEMPLATE_NOT_FOUND, $businessProcessStartNotificationTemplateId);
		}
		
		$caseIds = $dbTemplate->getCaseIds($dbObject, false);
		if(!count($caseIds))
		{
			throw new KontorolAPIException(KontorolBusinessProcessNotificationErrors::BUSINESS_PROCESS_CASE_NOT_FOUND);
		}
		
		$dbBusinessProcessServer = BusinessProcessServerPeer::retrieveByPK($dbTemplate->getServerId());
		if (!$dbBusinessProcessServer)
		{
			throw new KontorolAPIException(KontorolBusinessProcessNotificationErrors::BUSINESS_PROCESS_SERVER_NOT_FOUND, $dbTemplate->getServerId());
		}
		
		$businessProcessServer = KontorolBusinessProcessServer::getInstanceByType($dbBusinessProcessServer->getType());
		$businessProcessServer->fromObject($dbBusinessProcessServer);
		$provider = kBusinessProcessProvider::get($businessProcessServer);
		
		$caseId = end($caseIds);
		
		$filename = myContentStorage::getFSCacheRootPath() . 'bpm_diagram/bpm_';
		$filename .= $objectId . '_';
		$filename .= $businessProcessStartNotificationTemplateId . '_';
		$filename .= $caseId . '.jpg';
		
		$provider->getCaseDiagram($caseId, $filename);
		$mimeType = kFile::mimeType($filename);			
		return $this->dumpFile($filename, $mimeType);
	}
	
	/**
	 * list business-process cases
	 * 
	 * @action list
	 * @param KontorolEventNotificationEventObjectType $objectType
	 * @param string $objectId
	 * @return KontorolBusinessProcessCaseArray
	 * 
	 * @throws KontorolBusinessProcessNotificationErrors::BUSINESS_PROCESS_CASE_NOT_FOUND
	 * @throws KontorolBusinessProcessNotificationErrors::BUSINESS_PROCESS_SERVER_NOT_FOUND
	 */
	public function listAction($objectType, $objectId)
	{
		$dbObject = kEventNotificationFlowManager::getObject($objectType, $objectId);
		if(!$dbObject)
		{
			throw new KontorolAPIException(KontorolErrors::OBJECT_NOT_FOUND);
		}
		
		$cases = BusinessProcessCasePeer::retrieveCasesByObjectIdObjecType($objectId, $objectType);
		if(!count($cases))
		{
			throw new KontorolAPIException(KontorolBusinessProcessNotificationErrors::BUSINESS_PROCESS_CASE_NOT_FOUND);
		}
		
		$array = new KontorolBusinessProcessCaseArray();
		foreach($cases as $case)
		{
			/* @var $case BusinessProcessCase */
			$dbBusinessProcessServer = BusinessProcessServerPeer::retrieveByPK($case->getServerId());
			if (!$dbBusinessProcessServer)
			{
				KontorolLog::info("Business-Process server [" . $case->getServerId() . "] not found");
				continue;
			}
			
			$businessProcessServer = KontorolBusinessProcessServer::getInstanceByType($dbBusinessProcessServer->getType());
			$businessProcessServer->fromObject($dbBusinessProcessServer);
			$provider = kBusinessProcessProvider::get($businessProcessServer);
			if(!$provider)
			{
				KontorolLog::info("Provider [" . $businessProcessServer->type . "] not found");
				continue;
			}

			$latestCaseId = $case->getCaseId();
			if($latestCaseId)
			{
				try {
					$case = $provider->getCase($latestCaseId);
					$businessProcessCase = new KontorolBusinessProcessCase();
					$businessProcessCase->businessProcessStartNotificationTemplateId = $templateId;
					$businessProcessCase->fromObject($case);
					$array[] = $businessProcessCase;
				} catch (ActivitiClientException $e) {
					KontorolLog::err("Case [$latestCaseId] not found: " . $e->getMessage());
				}
			}
		}
		
		return $array;
	}
}

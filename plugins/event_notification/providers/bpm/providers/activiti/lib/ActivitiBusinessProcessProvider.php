<?php
/**
 * @package plugins.activitiBusinessProcessNotification
 * @subpackage model.enum
 */
class ActivitiBusinessProcessProvider implements IKontorolPluginEnum, BusinessProcessProvider
{
	const ACTIVITI = 'Activiti';
	
	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalValues()
	 */
	public static function getAdditionalValues()
	{
		return array(
			'ACTIVITI' => self::ACTIVITI,
		);
	}

	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalDescriptions()
	 */
	public static function getAdditionalDescriptions() 
	{
		return array(
			self::ACTIVITI => 'Activiti BPM Platform',
		);
	}
}

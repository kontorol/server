<?php
/**
 * @package plugins.activitiBusinessProcessNotification
 * @subpackage api.enum
 */
class KontorolActivitiBusinessProcessServerProtocol extends KontorolStringEnum
{
	const HTTP = 'http';
	const HTTPS = 'https';
}

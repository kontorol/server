<?php
/**
 * @package plugins.activitiBusinessProcessNotification
 */
class ActivitiBusinessProcessNotificationPlugin extends KontorolPlugin implements IKontorolPermissions, IKontorolPending, IKontorolObjectLoader, IKontorolEnumerator
{
	const PLUGIN_NAME = 'activitiBusinessProcessNotification';
	
	const BPM_NOTIFICATION_PLUGIN_NAME = 'businessProcessNotification';
	const BPM_NOTIFICATION_PLUGIN_VERSION_MAJOR = 1;
	const BPM_NOTIFICATION_PLUGIN_VERSION_MINOR = 0;
	const BPM_NOTIFICATION_PLUGIN_VERSION_BUILD = 0;
	
	/* (non-PHPdoc)
	 * @see IKontorolPlugin::getPluginName()
	 */
	public static function getPluginName()
	{
		return self::PLUGIN_NAME;
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolPermissions::isAllowedPartner()
	 */
	public static function isAllowedPartner($partnerId)
	{
		$partner = PartnerPeer::retrieveByPK($partnerId);
		if ($partner)
			return $partner->getPluginEnabled(self::PLUGIN_NAME);

		return false;
	}
			
	/* (non-PHPdoc)
	 * @see IKontorolEnumerator::getEnums()
	 */
	public static function getEnums($baseEnumName = null)
	{
		if(is_null($baseEnumName))
			return array('ActivitiBusinessProcessProvider');
	
		if($baseEnumName == 'BusinessProcessProvider')
			return array('ActivitiBusinessProcessProvider');
			
		return array();
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolObjectLoader::loadObject()
	 */
	public static function loadObject($baseClass, $enumValue, array $constructorArgs = null)
	{
		$class = self::getObjectClass($baseClass, $enumValue);
		if($class)
		{
			if(is_array($constructorArgs))
			{
				$reflect = new ReflectionClass($class);
				return $reflect->newInstanceArgs($constructorArgs);
			}
			
			return new $class();
		}
			
		return null;
	}
		
	/* (non-PHPdoc)
	 * @see IKontorolObjectLoader::getObjectClass()
	 */
	public static function getObjectClass($baseClass, $enumValue)
	{
		if($baseClass == 'kBusinessProcessProvider')
		{
			if(class_exists('KontorolClient') && defined('KontorolBusinessProcessProvider::ACTIVITI'))
			{
				if($enumValue == KontorolBusinessProcessProvider::ACTIVITI)
					return 'kActivitiBusinessProcessProvider';
			}
			elseif(class_exists('Kontorol_Client_Client') && defined('Kontorol_Client_BusinessProcessNotification_Enum_BusinessProcessProvider::ACTIVITI'))
			{
				if($enumValue == Kontorol_Client_BusinessProcessNotification_Enum_BusinessProcessProvider::ACTIVITI)
					return 'kActivitiBusinessProcessProvider';
			}
			elseif($enumValue == self::getApiValue(ActivitiBusinessProcessProvider::ACTIVITI))
			{
				return 'kActivitiBusinessProcessProvider';
			}
		}
			
		if($baseClass == 'BusinessProcessServer' && $enumValue == self::getActivitiBusinessProcessProviderCoreValue(ActivitiBusinessProcessProvider::ACTIVITI))
			return 'ActivitiBusinessProcessServer';
			
		if($baseClass == 'KontorolBusinessProcessServer' && $enumValue == self::getActivitiBusinessProcessProviderCoreValue(ActivitiBusinessProcessProvider::ACTIVITI))
			return 'KontorolActivitiBusinessProcessServer';
					
		return null;
	}

	/* (non-PHPdoc)
	 * @see IKontorolPending::dependsOn()
	 */
	public static function dependsOn() 
	{
		$minVersion = new KontorolVersion(
			self::BPM_NOTIFICATION_PLUGIN_VERSION_MAJOR,
			self::BPM_NOTIFICATION_PLUGIN_VERSION_MINOR,
			self::BPM_NOTIFICATION_PLUGIN_VERSION_BUILD
		);
		$dependency = new KontorolDependency(self::BPM_NOTIFICATION_PLUGIN_NAME, $minVersion);
		
		return array($dependency);
	}

	/**
	 * @return int id of dynamic enum in the DB.
	 */
	public static function getActivitiBusinessProcessProviderCoreValue($valueName)
	{
		$value = self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
		return kPluginableEnumsManager::apiToCore('BusinessProcessProvider', $value);
	}
	
	/**
	 * @return string external API value of dynamic enum.
	 */
	public static function getApiValue($valueName)
	{
		return self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
	}
}

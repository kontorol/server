<?php
/**
 * @package plugins.businessProcessNotification
 */
class BusinessProcessNotificationPlugin extends KontorolPlugin implements IKontorolVersion, IKontorolPending, IKontorolObjectLoader, IKontorolEnumerator, IKontorolServices, IKontorolApplicationPartialView, IKontorolAdminConsolePages, IKontorolEventConsumers, IKontorolApplicationTranslations
{
	const PLUGIN_NAME = 'businessProcessNotification';
	const PLUGIN_VERSION_MAJOR = 1;
	const PLUGIN_VERSION_MINOR = 0;
	const PLUGIN_VERSION_BUILD = 0;
	
	const EVENT_NOTIFICATION_PLUGIN_NAME = 'eventNotification';
	const EVENT_NOTIFICATION_PLUGIN_VERSION_MAJOR = 1;
	const EVENT_NOTIFICATION_PLUGIN_VERSION_MINOR = 0;
	const EVENT_NOTIFICATION_PLUGIN_VERSION_BUILD = 0;
	
	const BUSINESS_PROCESS_NOTIFICATION_FLOW_MANAGER = 'kBusinessProcessNotificationFlowManager';
	
	/* (non-PHPdoc)
	 * @see IKontorolPlugin::getPluginName()
	 */
	public static function getPluginName()
	{
		return self::PLUGIN_NAME;
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolVersion::getVersion()
	 */
	public static function getVersion()
	{
		return new KontorolVersion(
			self::PLUGIN_VERSION_MAJOR,
			self::PLUGIN_VERSION_MINOR,
			self::PLUGIN_VERSION_BUILD
		);		
	}
			
	/* (non-PHPdoc)
	 * @see IKontorolEventConsumers::getEventConsumers()
	 */
	public static function getEventConsumers()
	{
		return array(self::BUSINESS_PROCESS_NOTIFICATION_FLOW_MANAGER);
	}
			
	/* (non-PHPdoc)
	 * @see IKontorolEnumerator::getEnums()
	 */
	public static function getEnums($baseEnumName = null)
	{
		if(is_null($baseEnumName))
			return array('BusinessProcessNotificationTemplateType');
	
		if($baseEnumName == 'EventNotificationTemplateType')
			return array('BusinessProcessNotificationTemplateType');
			
		return array();
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolObjectLoader::loadObject()
	 */
	public static function loadObject($baseClass, $enumValue, array $constructorArgs = null)
	{
		$class = self::getObjectClass($baseClass, $enumValue);
		if($class)
		{
			if(is_array($constructorArgs))
			{
				$reflect = new ReflectionClass($class);
				return $reflect->newInstanceArgs($constructorArgs);
			}
			
			return new $class();
		}
			
		return null;
	}
		
	/* (non-PHPdoc)
	 * @see IKontorolObjectLoader::getObjectClass()
	 */
	public static function getObjectClass($baseClass, $enumValue)
	{
		if($baseClass == 'KontorolEventNotificationDispatchJobData')
		{
			if(
				$enumValue == self::getBusinessProcessNotificationTemplateTypeCoreValue(BusinessProcessNotificationTemplateType::BPM_START) || 
				$enumValue == self::getBusinessProcessNotificationTemplateTypeCoreValue(BusinessProcessNotificationTemplateType::BPM_SIGNAL) || 
				$enumValue == self::getBusinessProcessNotificationTemplateTypeCoreValue(BusinessProcessNotificationTemplateType::BPM_ABORT)
			)
				return 'KontorolBusinessProcessNotificationDispatchJobData';
		}
		
		if($baseClass == 'EventNotificationTemplate')
		{
			if($enumValue == self::getBusinessProcessNotificationTemplateTypeCoreValue(BusinessProcessNotificationTemplateType::BPM_START))
				return 'BusinessProcessStartNotificationTemplate';
				
			if($enumValue == self::getBusinessProcessNotificationTemplateTypeCoreValue(BusinessProcessNotificationTemplateType::BPM_SIGNAL))
				return 'BusinessProcessSignalNotificationTemplate';
				
			if($enumValue == self::getBusinessProcessNotificationTemplateTypeCoreValue(BusinessProcessNotificationTemplateType::BPM_ABORT))
				return 'BusinessProcessAbortNotificationTemplate';
		}
	
		if($baseClass == 'KontorolEventNotificationTemplate')
		{
			if($enumValue == self::getBusinessProcessNotificationTemplateTypeCoreValue(BusinessProcessNotificationTemplateType::BPM_START))
				return 'KontorolBusinessProcessStartNotificationTemplate';
				
			if($enumValue == self::getBusinessProcessNotificationTemplateTypeCoreValue(BusinessProcessNotificationTemplateType::BPM_SIGNAL))
				return 'KontorolBusinessProcessSignalNotificationTemplate';
				
			if($enumValue == self::getBusinessProcessNotificationTemplateTypeCoreValue(BusinessProcessNotificationTemplateType::BPM_ABORT))
				return 'KontorolBusinessProcessAbortNotificationTemplate';
		}
	
		if($baseClass == 'Form_EventNotificationTemplateConfiguration')
		{
			if(
				$enumValue == Kontorol_Client_EventNotification_Enum_EventNotificationTemplateType::BPM_START ||
				$enumValue == Kontorol_Client_EventNotification_Enum_EventNotificationTemplateType::BPM_SIGNAL ||
				$enumValue == Kontorol_Client_EventNotification_Enum_EventNotificationTemplateType::BPM_ABORT
			)
				return 'Form_BusinessProcessNotificationTemplateConfiguration';
		}
	
		if($baseClass == 'Kontorol_Client_EventNotification_Type_EventNotificationTemplate')
		{
			if($enumValue == Kontorol_Client_EventNotification_Enum_EventNotificationTemplateType::BPM_START)
				return 'Kontorol_Client_BusinessProcessNotification_Type_BusinessProcessStartNotificationTemplate';
				
			if($enumValue == Kontorol_Client_EventNotification_Enum_EventNotificationTemplateType::BPM_SIGNAL)
				return 'Kontorol_Client_BusinessProcessNotification_Type_BusinessProcessSignalNotificationTemplate';
				
			if($enumValue == Kontorol_Client_EventNotification_Enum_EventNotificationTemplateType::BPM_ABORT)
				return 'Kontorol_Client_BusinessProcessNotification_Type_BusinessProcessAbortNotificationTemplate';
		}
	
		if($baseClass == 'KDispatchEventNotificationEngine')
		{
			if(
				$enumValue == KontorolEventNotificationTemplateType::BPM_START ||
				$enumValue == KontorolEventNotificationTemplateType::BPM_SIGNAL ||
				$enumValue == KontorolEventNotificationTemplateType::BPM_ABORT
			)
				return 'KDispatchBusinessProcessNotificationEngine';
		}
			
		return null;
	}

	/* (non-PHPdoc)
	 * @see IKontorolPending::dependsOn()
	 */
	public static function dependsOn() 
	{
		$minVersion = new KontorolVersion(
			self::EVENT_NOTIFICATION_PLUGIN_VERSION_MAJOR,
			self::EVENT_NOTIFICATION_PLUGIN_VERSION_MINOR,
			self::EVENT_NOTIFICATION_PLUGIN_VERSION_BUILD
		);
		$dependency = new KontorolDependency(self::EVENT_NOTIFICATION_PLUGIN_NAME, $minVersion);
		
		return array($dependency);
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolApplicationPartialView::getApplicationPartialViews()
	 */
	public static function getApplicationPartialViews($controller, $action)
	{
		if($controller == 'plugin' && $action == 'EventNotificationTemplateConfigureAction')
		{
			return array(
				new Kontorol_View_Helper_BusinessProcessNotificationTemplateConfigure(),
			);
		}
	
		if($controller == 'batch' && $action == 'entryInvestigation')
		{
			return array(
				new Kontorol_View_Helper_EntryBusinessProcess(),
			);
		}
		
		return array();
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolAdminConsolePages::getApplicationPages()
	 */
	public static function getApplicationPages() 
	{
		return array(
			new BusinessProcessNotificationTemplatesListProcessesAction(),
		);
	}

	/**
	 * @return int id of dynamic enum in the DB.
	 */
	public static function getBusinessProcessNotificationTemplateTypeCoreValue($valueName)
	{
		$value = self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
		return kPluginableEnumsManager::apiToCore('EventNotificationTemplateType', $value);
	}
	
	/**
	 * @return string external API value of dynamic enum.
	 */
	public static function getApiValue($valueName)
	{
		return self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
	}

	/* (non-PHPdoc)
	 * @see IKontorolServices::getServicesMap()
	 */
	public static function getServicesMap() 
	{
		return array(
			'businessProcessServer' => 'BusinessProcessServerService',
			'businessProcessCase' => 'BusinessProcessCaseService',
		);
	}

	/* (non-PHPdoc)
	 * @see IKontorolApplicationTranslations::getTranslations()
	 */
	public static function getTranslations($locale)
	{
		$array = array();
		
		$langFilePath = __DIR__ . "/config/lang/$locale.php";
		if(!file_exists($langFilePath))
		{
			$default = 'en';
			$langFilePath = __DIR__ . "/config/lang/$default.php";
		}
		
		$array = include($langFilePath);
	
		return array($locale => $array);
	}
}

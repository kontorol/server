<?php
/**
 * @package plugins.businessProcessNotification
 * @subpackage Scheduler
 */
class KDispatchBusinessProcessNotificationEngine extends KDispatchEventNotificationEngine
{
	/**
	 * @param KontorolBusinessProcessServer $server
	 * @return kBusinessProcessProvider
	 */
	public function getBusinessProcessProvider(KontorolBusinessProcessServer $server)
	{
		$provider = kBusinessProcessProvider::get($server);
		$provider->enableDebug(true);
		
		return $provider;
	}
	
	/* (non-PHPdoc)
	 * @see KDispatchEventNotificationEngine::dispatch()
	 */
	public function dispatch(KontorolEventNotificationTemplate $eventNotificationTemplate, KontorolEventNotificationDispatchJobData &$data)
	{
		$job = KJobHandlerWorker::getCurrentJob();
	
		$variables = array();
		if(is_array($data->contentParameters) && count($data->contentParameters))
		{
			foreach($data->contentParameters as $contentParameter)
			{
				/* @var $contentParameter KontorolKeyValue */
				$variables[$contentParameter->key] = $contentParameter->value;
			}		
		}
		
		switch ($job->jobSubType)
		{
			case KontorolEventNotificationTemplateType::BPM_START:
				return $this->startBusinessProcess($eventNotificationTemplate, $data, $variables);
				
			case KontorolEventNotificationTemplateType::BPM_SIGNAL:
				return $this->signalCase($eventNotificationTemplate, $data, $variables);
				
			case KontorolEventNotificationTemplateType::BPM_ABORT:
				return $this->abortCase($eventNotificationTemplate, $data);
		}
	}

	/**
	 * @param KontorolBusinessProcessStartNotificationTemplate $template
	 * @param KontorolBusinessProcessNotificationDispatchJobData $data
	 */
	public function startBusinessProcess(KontorolBusinessProcessStartNotificationTemplate $template, KontorolBusinessProcessNotificationDispatchJobData &$data, $variables)
	{	
		$provider = $this->getBusinessProcessProvider($data->server);
		KontorolLog::info("Starting business-process [{$template->processId}] with variables [" . print_r($variables, true) . "]");
		$data->caseId = $provider->startBusinessProcess($template->processId, $variables);
		KontorolLog::info("Started business-process case [{$data->caseId}]");
	}

	/**
	 * @param KontorolBusinessProcessSignalNotificationTemplate $template
	 * @param KontorolBusinessProcessNotificationDispatchJobData $data
	 */
	public function signalCase(KontorolBusinessProcessSignalNotificationTemplate $template, KontorolBusinessProcessNotificationDispatchJobData &$data, $variables)
	{
		$provider = $this->getBusinessProcessProvider($data->server);
		KontorolLog::info("Signaling business-process [{$template->processId}] case [{$data->caseId}] with message [{$template->message}] on blocking event [{$template->eventId}]");
		$provider->signalCase($data->caseId, $template->eventId, $template->message, $variables);
	}

	/**
	 * @param KontorolBusinessProcessStartNotificationTemplate $template
	 * @param KontorolBusinessProcessNotificationDispatchJobData $data
	 */
	public function abortCase(KontorolBusinessProcessAbortNotificationTemplate $template, KontorolBusinessProcessNotificationDispatchJobData &$data)
	{
		$provider = $this->getBusinessProcessProvider($data->server);
		KontorolLog::info("Aborting business-process [{$template->processId}] case [{$data->caseId}]");
		$provider->abortCase($data->caseId);
	}
}

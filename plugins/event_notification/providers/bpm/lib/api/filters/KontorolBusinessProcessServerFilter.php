<?php
/**
 * @package plugins.businessProcessNotification
 * @subpackage api.filters
 */
class KontorolBusinessProcessServerFilter extends KontorolBusinessProcessServerBaseFilter
{
	/**
	 * @var KontorolNullableBoolean
	 */
	public $currentDcOrExternal;

	/**
	 * @var KontorolNullableBoolean
	 */
	public $currentDc;

	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new BusinessProcessServerFilter();
	}

	/* (non-PHPdoc)
	 * @see KontorolFilter::toObject()
	 */
	public function toObject ( $object_to_fill = null, $props_to_skip = array() )
	{
		if(!$this->isNull('currentDc') && KontorolNullableBoolean::toBoolean($this->currentDc))
			$this->dcEqual = kDataCenterMgr::getCurrentDcId();

		elseif(!$this->isNull('currentDcOrExternal') && KontorolNullableBoolean::toBoolean($this->currentDcOrExternal))
		{
			$this->dcEqOrNull = kDataCenterMgr::getCurrentDcId();
		}

		return parent::toObject($object_to_fill, $props_to_skip);
	}
}

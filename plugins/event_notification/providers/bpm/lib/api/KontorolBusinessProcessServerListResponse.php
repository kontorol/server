<?php
/**
 * @package plugins.businessProcessNotification
 * @subpackage api.objects
 */
class KontorolBusinessProcessServerListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolBusinessProcessServerArray
	 * @readonly
	 */
	public $objects;
}

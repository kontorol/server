<?php
/**
 * @package plugins.businessProcessNotification
 * @subpackage api.enum
 * @see BusinessProcessProvider
 */
class KontorolBusinessProcessProvider extends KontorolDynamicEnum implements BusinessProcessProvider
{
	public static function getEnumClass()
	{
		return 'BusinessProcessProvider';
	}
}

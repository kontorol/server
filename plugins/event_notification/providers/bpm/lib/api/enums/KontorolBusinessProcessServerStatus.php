<?php
/**
 * @package plugins.businessProcessNotification
 * @subpackage api.enum
 */
class KontorolBusinessProcessServerStatus extends KontorolDynamicEnum implements BusinessProcessServerStatus
{
	public static function getEnumClass()
	{
		return 'BusinessProcessServerStatus';
	}
}

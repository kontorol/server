<?php
/**
 * @package plugins.businessProcessNotification
 * @subpackage api.objects
 */
class KontorolBusinessProcessNotificationDispatchJobData extends KontorolEventNotificationDispatchJobData
{
	/**
	 * @var KontorolBusinessProcessServer
	 */
	public $server;
	
	/**
	 * @var string
	 */
	public $caseId;
	
	private static $map_between_objects = array
	(
		'caseId',
	);

	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects ( )
	{
		return array_merge ( parent::getMapBetweenObjects() , self::$map_between_objects );
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::fromObject()
	 */
	protected function doFromObject($dbObject, KontorolDetachedResponseProfile $responseProfile = null)
	{
		/* @var $dbObject kBusinessProcessNotificationDispatchJobData */
		parent::doFromObject($dbObject, $responseProfile);
		
		$server = $dbObject->getServer();
		$this->server = KontorolBusinessProcessServer::getInstanceByType($server->getType());
		$this->server->fromObject($server);
	}
}

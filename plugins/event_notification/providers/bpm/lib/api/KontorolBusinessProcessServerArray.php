<?php
/**
 * @package plugins.businessProcessNotification
 * @subpackage api.objects
 */
class KontorolBusinessProcessServerArray extends KontorolTypedArray
{
	public static function fromDbArray($arr)
	{
		$newArr = new KontorolBusinessProcessServerArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
			/* @var $obj BusinessProcessServer */
    		$nObj = KontorolBusinessProcessServer::getInstanceByType($obj->getType());
    		if(!$nObj)
    		{
    			KontorolLog::err("Business-Process server could not find matching type for [" . $obj->getType() . "]");
    			continue;
    		}
			$nObj->fromObject($obj);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolBusinessProcessServer");
	}
}

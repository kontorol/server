<?php
/**
 * @package plugins.businessProcessNotification
 * @subpackage api.objects
 */
class KontorolBusinessProcessCaseArray extends KontorolTypedArray
{
	public static function fromDbArray($arr)
	{
		$newArr = new KontorolBusinessProcessCaseArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
			/* @var $obj kBusinessProcessCase */
    		$nObj = new KontorolBusinessProcessCase();
			$nObj->fromObject($obj);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolBusinessProcessCase");
	}
}

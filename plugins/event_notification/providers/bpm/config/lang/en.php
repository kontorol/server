<?php

return array(
	'entry-investigate bpm title' => 'Business-Process Cases',
	'entry-investigate bpm template id th' => 'Template ID',
	'entry-investigate bpm template name th' => 'Template Name',
	'entry-investigate bpm case id th' => 'Case ID',

	'Kontorol_Client_EventNotification_Enum_EventNotificationTemplateType::BPM_START' => 'Start Business-Process',
	'Kontorol_Client_EventNotification_Enum_EventNotificationTemplateType::BPM_SIGNAL' => 'Signal Business-Process',
	'Kontorol_Client_EventNotification_Enum_EventNotificationTemplateType::BPM_ABORT' => 'Abort Business-Process',
);

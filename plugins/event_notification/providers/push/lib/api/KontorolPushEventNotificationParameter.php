<?php
/**
 * @package plugins.pushNotification
 * @subpackage api.objects
 */
class KontorolPushEventNotificationParameter extends KontorolEventNotificationParameter
{
	/**
	 * @var string
	 */
	public $queueKeyToken;

	private static $map_between_objects = array('queueKeyToken');

	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}

	/* (non-PHPdoc)
   * @see KontorolObject::toObject()
   */
	public function toObject($dbObject = null, $propertiesToSkip = array())
	{
		if(is_null($dbObject))
			$dbObject = new kPushEventNotificationParameter();

		return parent::toObject($dbObject, $propertiesToSkip);
	}
}

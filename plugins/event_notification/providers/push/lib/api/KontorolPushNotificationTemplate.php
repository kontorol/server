<?php
/**
 * @package plugins.pushNotification
 * @subpackage api.objects
*/
class KontorolPushNotificationTemplate extends KontorolEventNotificationTemplate
{
	/**
	 * Define the content dynamic parameters
	 * @var KontorolPushEventNotificationParameterArray
	 * @requiresPermission update
	 */
	public $queueNameParameters;
	
	/**
	 * Define the content dynamic parameters
	 * @var KontorolPushEventNotificationParameterArray
	 * @requiresPermission update
	 */
	public $queueKeyParameters;
	
    /**
     * Kontorol API object type
     * @var string
     */
    public $apiObjectType;
    
    /**
     * Kontorol Object format
     * @var KontorolResponseType
     */    
    public $objectFormat;
    
    /**
     * Kontorol response-profile id
     * @var int
     */    
    public $responseProfileId;
    

    private static $map_between_objects = array('apiObjectType', 'objectFormat', 'responseProfileId', 'queueNameParameters', 'queueKeyParameters');
    
    public function __construct()
    {
        $this->type = PushNotificationPlugin::getApiValue(PushNotificationTemplateType::PUSH);
    }
    
    /* (non-PHPdoc)
     * @see KontorolObject::getMapBetweenObjects()
     */
    public function getMapBetweenObjects()
    {
        return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
    }    

    /* (non-PHPdoc)
     * @see KontorolObject::validateForUpdate()
     */
    public function validateForUpdate($sourceObject, $propertiesToSkip = array())
    {
        $propertiesToSkip[] = 'type';
        return parent::validateForUpdate($sourceObject, $propertiesToSkip);
    }
    
    /* (non-PHPdoc)
     * @see KontorolObject::toObject()
     */
    public function toObject($dbObject = null, $propertiesToSkip = array())
    {
        if(is_null($dbObject))
            $dbObject = new PushNotificationTemplate();
        	
        return parent::toObject($dbObject, $propertiesToSkip);
    }
}

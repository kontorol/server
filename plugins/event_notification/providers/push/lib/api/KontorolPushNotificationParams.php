<?php
/**
 * Object which contains contextual entry-related data.
 * @package plugins.pushNotification
 * @subpackage api.objects
 */
class KontorolPushNotificationParams extends KontorolObject
{	
	/**
	 * User params
	 * @var KontorolPushEventNotificationParameterArray
	 */
	public $userParams;

	private static $map_between_objects = array('userParams');

	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}

	/* (non-PHPdoc)
   * @see KontorolObject::toObject()
   */
	public function toObject($dbObject = null, $propertiesToSkip = array())
	{
		if(is_null($dbObject))
			$dbObject = new kPushNotificationParams();

		return parent::toObject($dbObject, $propertiesToSkip);
	}
	
}

<?php
/**
 * @package plugins.pushNotification
 * @subpackage model.enum
 */
class PushNotificationTemplateType implements IKontorolPluginEnum, EventNotificationTemplateType
{
	const PUSH = 'Push';
	
	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalValues()
	 */
	public static function getAdditionalValues()
	{
		return array(
			'PUSH' => self::PUSH,
		);
	}

	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalDescriptions()
	 */
	public static function getAdditionalDescriptions() 
	{
		return array(
			self::PUSH => 'Push event notification',
		);
	}
}

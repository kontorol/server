<?php
/**
 * @package plugins.pushNotification
* @subpackage admin
*/
class Form_PushNotificationTemplateConfiguration extends Form_EventNotificationTemplateConfiguration
{
	public function populateFromObject($object, $add_underscore = true)
	{
		if(!($object instanceof Kontorol_Client_PushNotification_Type_PushNotificationTemplate))
			return;
		
		if($object->queueNameParameters && count($object->queueNameParameters))
		{
			$queueNameParameters = array();
			foreach($object->queueNameParameters as $index => $parameter)
				$queueNameParameters[] = $this->getParameterDescription($parameter);
		
			$queueNameParametersList = new Infra_Form_HtmlList('queueNameParameters', array(
					'legend'		=> 'queue Name Parameters',
					'list'			=> $queueNameParameters,
			));
			$this->addElements(array($queueNameParametersList));
		}

		if($object->queueKeyParameters && count($object->queueKeyParameters))
		{
			$queueKeyParameters = array();
			foreach($object->queueKeyParameters as $index => $parameter)
				$queueKeyParameters[] = $this->getParameterDescription($parameter);
		
			$queueKeyParametersList = new Infra_Form_HtmlList('queueKeyParameters', array(
					'legend'		=> 'queue Key Parameters',
					'list'			=> $queueKeyParameters,
			));
			$this->addElements(array($queueKeyParametersList));
		}
		
		parent::populateFromObject($object, $add_underscore);
	}
	
    protected function addTypeElements(Kontorol_Client_EventNotification_Type_EventNotificationTemplate $eventNotificationTemplate)
    {
        $element = new Infra_Form_Html('http_title', array(
            'content' => '<b>Notification Handler Service  Details</b>',
        ));
        $this->addElements(array($element));
        
        $this->addElement('select', 'api_object_type', array(
            'label'			=> 'Object Type (KontorolObject):',
 			'default'       => $eventNotificationTemplate->apiObjectType,
            'filters'		=> array('StringTrim'),
            'required'		=> true,            
            'multiOptions' 	=> array(
                'KontorolBaseEntry' => 'Base Entry',
                'KontorolDataEntry' => 'Data Entry',
                'KontorolDocumentEntry' => 'Document Entry',
                'KontorolMediaEntry' => 'Media Entry',
                'KontorolExternalMediaEntry' => 'External Media Entry',
                'KontorolLiveStreamEntry' => 'Live Stream Entry',
                'KontorolPlaylist' => 'Playlist',
                'KontorolCategory' => 'Category',
                'KontorolUser' => 'User',
                'KontorolCuePoint' => 'CuePoint',
                'KontorolAdCuePoint' => 'Ad Cue-Point',
                'KontorolAnnotation' => 'Annotation',
                'KontorolCodeCuePoint' => 'Code Cue-Point',
				'KontorolThumbCuePoint' => 'Thumb Cue-Point',
                'KontorolDistributionProfile' => 'Distribution Profile',
                'KontorolEntryDistribution' => 'Entry Distribution',
                'KontorolMetadata' => 'Metadata',
                'KontorolAsset' => 'Asset',
                'KontorolFlavorAsset' => 'Flavor Asset',
                'KontorolThumbAsset' => 'Thumbnail Asset',
                'KontorolAccessControlProfile' => 'Access Control',
                'KontorolBatchJob' => 'BatchJob',
                'KontorolBulkUploadResultEntry' => 'Bulk-Upload Entry Result',
                'KontorolBulkUploadResultCategory' => 'Bulk-Upload Category Result',
                'KontorolBulkUploadResultUser' => 'Bulk-Upload User Result',
                'KontorolBulkUploadResultCategoryUser' => 'Bulk-Upload Category - User Result',
                'KontorolCategoryUser' => 'Category - User',
                'KontorolConversionProfile' => 'Conversion Profile',
                'KontorolFlavorParams' => 'Flavor Params',
                'KontorolConversionProfileAssetParams' => 'Asset Params - Conversion Profile',
                'KontorolFlavorParamsOutput' => 'Flavor Params Output',
                'KontorolGenericsynDicationFeed' => 'Genericsyn Dication Feed',
                'KontorolPartner' => 'Partner',
                'KontorolPermission' => 'Permission',
                'KontorolPermissionItem' => 'Permission Item',
                'KontorolScheduler' => 'Scheduler',
                'KontorolSchedulerConfig' => 'Scheduler Config',
                'KontorolSchedulerStatus' => 'Scheduler Status',
                'KontorolSchedulerWorker' => 'Scheduler Worker',
                'KontorolStorageProfile' => 'Storage Profile',
                'KontorolThumbParams' => 'Thumbnail Params',
                'KontorolThumbParamsOutput' => 'Thumbnail Params Output',
                'KontorolUploadToken' => 'Upload Token',
                'KontorolUserLoginData' => 'User Login Data',
                'KontorolUserRole' => 'User Role',
                'KontorolWidget' => 'Widget',
                'KontorolCategoryEntry' => 'Category - Entry',
                'KontorolLiveStreamScheduleEvent' => 'Schedule Live-Stream Event',
                'KontorolRecordScheduleEvent' => 'Schedule Recorded Event',
                'KontorolLocationScheduleResource' => 'Schedule Location Resource',
                'KontorolLiveEntryScheduleResource' => 'Schedule Live-Entry Resource',
                'KontorolCameraScheduleResource' => 'Schedule Camera Resource',
                'KontorolScheduleEventResource' => 'Schedule Event-Resource',
                'KontorolClippingTaskEntryServerNode' => 'Clipping Task Entry-Server-Node',
            ),
        ));
    
        $this->addElement('select', 'object_format', array(
            'label'			=> 'Format:',
            'filters'		=> array('StringTrim'),
            'required'		=> true,
            'multiOptions' 	=> array(
                Kontorol_Client_Enum_ResponseType::RESPONSE_TYPE_JSON => 'JSON',
                Kontorol_Client_Enum_ResponseType::RESPONSE_TYPE_XML => 'XML',
                Kontorol_Client_Enum_ResponseType::RESPONSE_TYPE_PHP => 'PHP',
            ),
        ));

        $responseProfile = new Kontorol_Form_Element_ObjectSelect('response_profile_id', array(
        	'label' => 'Response Profile:',
        	'nameAttribute' => 'name',
        	'service' => 'responseProfile',
        	'pageSize' => 500,
        	'impersonate' => $eventNotificationTemplate->partnerId,
			'addNull' => true,
        ));
        $this->addElements(array($responseProfile));
    }    
}

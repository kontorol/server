<?php
/**
 * @package plugins.emailNotification
 * @subpackage model.enum
 */
class EmailNotificationTemplateType implements IKontorolPluginEnum, EventNotificationTemplateType
{
	const EMAIL = 'Email';
	
	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalValues()
	 */
	public static function getAdditionalValues()
	{
		return array(
			'EMAIL' => self::EMAIL,
		);
	}

	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalDescriptions()
	 */
	public static function getAdditionalDescriptions() 
	{
		return array(
			self::EMAIL => 'Email event notification',
		);
	}
}

<?php
/**
 * @package plugins.emailNotification
 * @subpackage api.objects
 */
class KontorolEmailNotificationRecipientArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolEmailNotificationRecipientArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
    		$nObj = new KontorolEmailNotificationRecipient();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolEmailNotificationRecipient");
	}
}

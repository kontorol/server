<?php
/**
 * @package plugins.emailNotification
 * @subpackage api.enum
 * @see EmailNotificationFormat
 */
class KontorolEmailNotificationFormat extends KontorolDynamicEnum implements EmailNotificationFormat
{
	public static function getEnumClass()
	{
		return 'EmailNotificationFormat';
	}
}

<?php
/**
 * @package plugins.emailNotification
 * @subpackage api.objects
 */
class KontorolEmailNotificationRecipient extends KontorolObject
{
	/**
	 * Recipient e-mail address
	 * @var KontorolStringValue
	 */
	public $email;
	
	/**
	 * Recipient name
	 * @var KontorolStringValue
	 */
	public $name;
	
	private static $map_between_objects = array
	(
		'email',
		'name',
	);

	public function getMapBetweenObjects ( )
	{
		return array_merge ( parent::getMapBetweenObjects() , self::$map_between_objects );
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbObject = null, $skip = array())
	{
		if(!$dbObject)
			$dbObject = new kEmailNotificationRecipient();
			
		return parent::toObject($dbObject, $skip);
	}
	 
	/* (non-PHPdoc)
	 * @see KontorolObject::fromObject()
	 */
	public function doFromObject($dbObject, KontorolDetachedResponseProfile $responseProfile = null)
	{
		/* @var $dbObject kEmailNotificationRecipient */
		parent::doFromObject($dbObject, $responseProfile);
		
		
		$emailType = get_class($dbObject->getEmail());
		switch ($emailType)
		{
			case 'kStringValue':
				$this->email = new KontorolStringValue();
				break;
				
			case 'kEvalStringField':
				$this->email = new KontorolEvalStringField();
				break;
				
			case 'kUserEmailContextField':
				$this->email = new KontorolUserEmailContextField();
				break;
				
			default:
				$this->email = KontorolPluginManager::loadObject('KontorolStringValue', $emailType);
				break;
		}
		if($this->email)
			$this->email->fromObject($dbObject->getEmail());
		
			
		$nameType = get_class($dbObject->getName());
		switch ($nameType)
		{
			case 'kStringValue':
				$this->name = new KontorolStringValue();
				break;
				
			case 'kEvalStringField':
				$this->name = new KontorolEvalStringField();
				break;
				
			default:
				$this->name = KontorolPluginManager::loadObject('KontorolStringValue', $nameType);
				break;
		}
		if($this->name)
			$this->name->fromObject($dbObject->getName());
	}
}

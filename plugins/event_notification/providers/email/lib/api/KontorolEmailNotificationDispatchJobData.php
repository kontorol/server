<?php
/**
 * @package plugins.emailNotification
 * @subpackage api.objects
 */
class KontorolEmailNotificationDispatchJobData extends KontorolEventNotificationDispatchJobData
{
	
	/**
	 * Define the email sender email
	 * @var string
	 */
	public $fromEmail;
	
	/**
	 * Define the email sender name
	 * @var string
	 */
	public $fromName;
	
	/**
	 * Email recipient emails and names, key is mail address and value is the name
	 * @var KontorolEmailNotificationRecipientJobData
	 */
	public $to;
	
	/**
	 * Email cc emails and names, key is mail address and value is the name
	 * @var KontorolEmailNotificationRecipientJobData
	 */
	public $cc;
	
	/**
	 * Email bcc emails and names, key is mail address and value is the name
	 * @var KontorolEmailNotificationRecipientJobData
	 */
	public $bcc;
	
	/**
	 * Email addresses that a replies should be sent to, key is mail address and value is the name
	 * 
	 * @var KontorolEmailNotificationRecipientJobData
	 */
	public $replyTo;
	
	/**
	 * Define the email priority
	 * @var KontorolEmailNotificationTemplatePriority
	 */
	public $priority;
	
	/**
	 * Email address that a reading confirmation will be sent to
	 * 
	 * @var string
	 */
	public $confirmReadingTo;
	
	/**
	 * Hostname to use in Message-Id and Received headers and as default HELO string. 
	 * If empty, the value returned by SERVER_NAME is used or 'localhost.localdomain'.
	 * 
	 * @var string
	 */
	public $hostname;
	
	/**
	 * Sets the message ID to be used in the Message-Id header.
	 * If empty, a unique id will be generated.
	 * 
	 * @var string
	 */
	public $messageID;
	
	/**
	 * Adds a e-mail custom header
	 * 
	 * @var KontorolKeyValueArray
	 */
	public $customHeaders;
	
	private static $map_between_objects = array
	(
		'fromEmail',
		'fromName',
		'to',
		'cc',
		'bcc',
		'replyTo',
		'priority',
		'confirmReadingTo',
		'hostname',
		'messageID',
		'customHeaders',
	);

	public function getMapBetweenObjects ( )
	{
		return array_merge ( parent::getMapBetweenObjects() , self::$map_between_objects );
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::fromObject()
	 */
	public function doFromObject($dbObject, KontorolDetachedResponseProfile $responseProfile = null)
	{
		/* @var $dbObject kEmailNotificationDispatchJobData */
		parent::doFromObject($dbObject, $responseProfile);
		
		$this->to = KontorolEmailNotificationRecipientJobData::getDataInstance($dbObject->getTo());
		$this->cc = KontorolEmailNotificationRecipientJobData::getDataInstance($dbObject->getCc());
		$this->bcc = KontorolEmailNotificationRecipientJobData::getDataInstance($dbObject->getBcc());
		$this->replyTo = KontorolEmailNotificationRecipientJobData::getDataInstance($dbObject->getReplyTo());
		$this->customHeaders = KontorolKeyValueArray::fromKeyValueArray($dbObject->getCustomHeaders());
	}
}

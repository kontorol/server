<?php
/**
 * @package plugins.emailNotification
 * @subpackage api.objects
 * @deprecated use KontorolEventNotificationParameter instead
 */
class KontorolEmailNotificationParameter extends KontorolEventNotificationParameter
{
}

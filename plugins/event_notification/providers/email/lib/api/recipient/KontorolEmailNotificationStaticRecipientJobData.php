<?php
/**
 * JobData representing the static receipient array
 *
 * @package plugins.emailNotification
 * @subpackage model.data
 */
class KontorolEmailNotificationStaticRecipientJobData extends KontorolEmailNotificationRecipientJobData
{
	/**
	 * Email to emails and names
	 * @var KontorolKeyValueArray
	 */
	public $emailRecipients;
	
	private static $map_between_objects = array(
		'emailRecipients',
	);
	
	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolEmailNotificationRecipientJobData::setProviderType()
	 */
	protected function setProviderType() 
	{
		$this->providerType = KontorolEmailNotificationRecipientProviderType::STATIC_LIST;
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::fromObject($source_object)
	 */
	public function doFromObject($dbObject, KontorolDetachedResponseProfile $responseProfile = null)
	{
		/* @var $dbObject kEmailNotificationStaticRecipientJobData */
		parent::doFromObject($dbObject, $responseProfile);
		$this->setProviderType();
		
		$this->emailRecipients = KontorolKeyValueArray::fromKeyValueArray($dbObject->getEmailRecipients());
	}

	/* (non-PHPdoc)
	 * @see KontorolObject::toObject($object_to_fill, $props_to_skip)
	 */
	public function toObject($dbObject = null, $propertiesToSkip = array())
	{
		if (is_null($dbObject))
			$dbObject = new kEmailNotificationStaticRecipientJobData();
		
		return parent::toObject($dbObject, $propertiesToSkip);
	}
}

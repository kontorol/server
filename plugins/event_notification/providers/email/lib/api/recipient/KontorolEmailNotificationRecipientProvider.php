<?php
/**
 * Abstract core class  which provides the recipients (to, CC, BCC) for an email notification
 * @package plugins.emailNotification
 * @subpackage model.data
 */
abstract class KontorolEmailNotificationRecipientProvider extends KontorolObject
{
	public static function getProviderInstance ($dbObject)
	{
		switch (get_class($dbObject))
		{
			case 'kEmailNotificationStaticRecipientProvider':
				$instance = new KontorolEmailNotificationStaticRecipientProvider();
				break;
			case 'kEmailNotificationCategoryRecipientProvider':
				$instance = new KontorolEmailNotificationCategoryRecipientProvider();
				break;
			case 'kEmailNotificationUserRecipientProvider':
				$instance = new KontorolEmailNotificationUserRecipientProvider();
				break;
			case 'kEmailNotificationGroupRecipientProvider':
				$instance = new KontorolEmailNotificationGroupRecipientProvider();
				break;
			default:
				$instance = KontorolPluginManager::loadObject('kEmailNotificationRecipientProvider', get_class($dbObject));
				break;
		}
		
		if ($instance)
			$instance->fromObject($dbObject);
		
		return $instance;
	}
}

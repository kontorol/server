<?php
/**
 * API object which provides the recipients of category related notifications.
 *
 * @package plugins.emailNotification
 * @subpackage model.data
 */
class KontorolEmailNotificationCategoryRecipientProvider extends KontorolEmailNotificationRecipientProvider
{
	/**
	 * The ID of the category whose subscribers should receive the email notification.
	 * @var KontorolStringValue
	 */
	public $categoryId;

	/**
	 * The IDs of the categories whose subscribers should receive the email notification.
	 * @var KontorolStringValue
	 */
	public $categoryIds;
	
	/**
	 *
	 * @var KontorolCategoryUserProviderFilter
	 */
	public $categoryUserFilter;

	private static $map_between_objects = array(
		'categoryId',
		'categoryIds',
		'categoryUserFilter',
	);
	
	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject($object_to_fill, $props_to_skip)
	 */
	public function toObject($dbObject = null, $propertiesToSkip = array())
	{
		$this->validate();
		if (is_null($dbObject))
			$dbObject = new kEmailNotificationCategoryRecipientProvider();
			
		return parent::toObject($dbObject, $propertiesToSkip);
	}	
	
	/**
	 * Validation function
	 * @throws KontorolEmailNotificationErrors::INVALID_FILTER_PROPERTY
	 */
	protected function validate ()
	{
		if ($this->categoryUserFilter)
		{
			if (isset ($this->categoryUserFilter->categoryIdEqual))
			{
				throw new KontorolAPIException(KontorolEmailNotificationErrors::INVALID_FILTER_PROPERTY, 'categoryIdEqual');
			}
		}
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::fromObject($source_object)
	 */
	public function doFromObject($dbObject, KontorolDetachedResponseProfile $responseProfile = null)
	{
		parent::doFromObject($dbObject, $responseProfile);
		/* @var $dbObject kEmailNotificationCategoryRecipientProvider */
		$categoryIdFieldType = get_class($dbObject->getCategoryId());
		KontorolLog::info("Retrieving API object for categoryId field of type [$categoryIdFieldType]");
		switch ($categoryIdFieldType)
		{
			case 'kObjectIdField':
				$this->categoryId = new KontorolObjectIdField();
				break;
			case 'kEvalStringField':
				$this->categoryId = new KontorolEvalStringField();
				break;
			case 'kStringValue':
				$this->categoryId = new KontorolStringValue();
				break;
			default:
				$this->categoryId = KontorolPluginManager::loadObject('KontorolStringValue', $categoryIdFieldType);
				break;
		}
		
		if ($this->categoryId)
		{
			$this->categoryId->fromObject($dbObject->getCategoryId());
		}

		$categoryIdsFieldType = get_class($dbObject->getCategoryIds());
		KontorolLog::info("Retrieving API object for categoryIds field of type [$categoryIdsFieldType]");
		switch ($categoryIdsFieldType)
		{
			case 'kEvalStringField':
				$this->categoryIds = new KontorolEvalStringField();
				break;
			case 'kStringValue':
				$this->categoryIds = new KontorolStringValue();
				break;
			default:
				$this->categoryIds = KontorolPluginManager::loadObject('KontorolStringValue', $categoryIdFieldType);
				break;
		}

		if ($this->categoryIds)
		{
			$this->categoryIds->fromObject($dbObject->getCategoryIds());
		}

		if ($dbObject->getCategoryUserFilter())
		{
			$this->categoryUserFilter = new KontorolCategoryUserProviderFilter();
			$this->categoryUserFilter->fromObject($dbObject->getCategoryUserFilter());
		}

	}
} 

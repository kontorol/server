<?php
/**
 * Job Data representing the provider of recipients for a single categoryId
 *
 * @package plugins.emailNotification
 * @subpackage model.data
 */
class KontorolEmailNotificationCategoryRecipientJobData extends KontorolEmailNotificationRecipientJobData
{
	/**
	 * @var KontorolCategoryUserFilter
	 */
	public $categoryUserFilter;
	
	private static $map_between_objects = array(
		'categoryUserFilter',
	);
	
	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolEmailNotificationRecipientJobData::setProviderType()
	 */
	protected function setProviderType() 
	{
		$this->providerType = KontorolEmailNotificationRecipientProviderType::CATEGORY;
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::fromObject($source_object)
	 */
	public function doFromObject($source_object, KontorolDetachedResponseProfile $responseProfile = null)
	{
		parent::doFromObject($source_object, $responseProfile);
		$this->setProviderType();
		if ($source_object->getCategoryUserFilter())
		{
			$this->categoryUserFilter = new KontorolCategoryUserFilter();
			$this->categoryUserFilter->fromObject($source_object->getCategoryUserFilter());
		}
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject($object_to_fill, $props_to_skip)
	 */
	public function toObject($dbObject = null, $propertiesToSkip = array())
	{
		if (is_null($dbObject))
			$dbObject = new kEmailNotificationCategoryRecipientJobData();
		
		return parent::toObject($dbObject, $propertiesToSkip);
	}
}

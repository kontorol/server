<?php
/**
 * JobData representing the dynamic user receipient array
 *
 * @package plugins.emailNotification
 * @subpackage model.data
 */
class KontorolEmailNotificationGroupRecipientJobData extends KontorolEmailNotificationRecipientJobData
{
	/**
	 * @var string
	 */
	public $groupId;
	
	private static $map_between_objects = array(
		'groupId',
	);
	
	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolEmailNotificationRecipientJobData::setProviderType()
	 */
	protected function setProviderType() {
		$this->providerType = KontorolEmailNotificationRecipientProviderType::GROUP;
		
	}

	/* (non-PHPdoc)
	 * @see KontorolObject::fromObject($source_object)
	 */
	public function doFromObject($dbObject, KontorolDetachedResponseProfile $responseProfile = null)
	{
		/* @var $dbObject kEmailNotificationStaticRecipientJobData */
		parent::doFromObject($dbObject, $responseProfile);
		$this->setProviderType();
	}

	/* (non-PHPdoc)
	 * @see KontorolObject::toObject($object_to_fill, $props_to_skip)
	 */
	public function toObject($dbObject = null, $propertiesToSkip = array())
	{
		if (is_null($dbObject))
			$dbObject = new kEmailNotificationGroupRecipientJobData();
		
		return parent::toObject($dbObject, $propertiesToSkip);
	}
}

<?php
/**
 * Abstract class representing the final output recipients going into the batch mechanism
 * @package plugins.emailNotification
 * @subpackage model.data
 */
abstract class KontorolEmailNotificationRecipientJobData extends KontorolObject
{
	 /**
	  * Provider type of the job data.
	  * @var KontorolEmailNotificationRecipientProviderType
	  * 
	  * @readonly
	  */
	 public $providerType;
	 
	/**
	 * Protected setter to set the provider type of the job data
	 */
	abstract protected function setProviderType ();
	
	/**
	 * Function returns correct API recipient data type based on the DB class received.
	 * @param kEmailNotificationRecipientJobData $dbData
	 * @return Kontorol
	 */
	public static function getDataInstance ($dbData)
	{
		$instance = null;
		if ($dbData)
		{
			switch (get_class($dbData))
			{
				case 'kEmailNotificationCategoryRecipientJobData':
					$instance = new KontorolEmailNotificationCategoryRecipientJobData();
					break;
				case 'kEmailNotificationStaticRecipientJobData':
					$instance = new KontorolEmailNotificationStaticRecipientJobData();
					break;
				case 'kEmailNotificationUserRecipientJobData':
					$instance = new KontorolEmailNotificationUserRecipientJobData();
					break;
				case 'kEmailNotificationGroupRecipientJobData':
					$instance = new KontorolEmailNotificationGroupRecipientJobData();
					break;
				default:
					$instance = KontorolPluginManager::loadObject('KontorolEmailNotificationRecipientJobData', $dbData->getProviderType());
					break;
			}
			
			if ($instance)
				$instance->fromObject($dbData);
		}
			
		return $instance;
		
	}
}

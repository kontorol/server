<?php
/**
 * Enum class for recipient provider types
 * 
 * @package plugins.emailNotification
 * @subpackage api.enums
 */
class KontorolEmailNotificationRecipientProviderType extends KontorolDynamicEnum implements EmailNotificationRecipientProviderType
{
	public static function getEnumClass()
	{
		return 'EmailNotificationRecipientProviderType';
	}
}

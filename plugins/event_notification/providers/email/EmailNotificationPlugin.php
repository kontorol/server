<?php
/**
 * @package plugins.emailNotification
 * 
 * 
 * TODO
 * Add event consumer to consume new email jobs and dispath event notification instead
 * Untill all mails are sent throgh events
 */
class EmailNotificationPlugin extends KontorolPlugin implements IKontorolPermissions, IKontorolPending, IKontorolObjectLoader, IKontorolEnumerator
{
	const PLUGIN_NAME = 'emailNotification';
	
	const EVENT_NOTIFICATION_PLUGIN_NAME = 'eventNotification';
	const EVENT_NOTIFICATION_PLUGIN_VERSION_MAJOR = 1;
	const EVENT_NOTIFICATION_PLUGIN_VERSION_MINOR = 0;
	const EVENT_NOTIFICATION_PLUGIN_VERSION_BUILD = 0;
	
	/* (non-PHPdoc)
	 * @see IKontorolPlugin::getPluginName()
	 */
	public static function getPluginName()
	{
		return self::PLUGIN_NAME;
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolPermissions::isAllowedPartner()
	 */
	public static function isAllowedPartner($partnerId)
	{
		$partner = PartnerPeer::retrieveByPK($partnerId);
		if ($partner)
			return $partner->getPluginEnabled(self::PLUGIN_NAME);

		return false;
	}
			
	/* (non-PHPdoc)
	 * @see IKontorolEnumerator::getEnums()
	 */
	public static function getEnums($baseEnumName = null)
	{
		if(is_null($baseEnumName))
			return array('EmailNotificationTemplateType', 'EmailNotificationFileSyncObjectType');
	
		if($baseEnumName == 'EventNotificationTemplateType')
			return array('EmailNotificationTemplateType');
			
		if($baseEnumName == 'FileSyncObjectType')
			return array('EmailNotificationFileSyncObjectType');
			
		return array();
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolObjectLoader::loadObject()
	 */
	public static function loadObject($baseClass, $enumValue, array $constructorArgs = null)
	{
		if($baseClass == 'ISyncableFile' && $enumValue == self::getEmailNotificationFileSyncObjectTypeCoreValue(EmailNotificationFileSyncObjectType::EMAIL_NOTIFICATION_TEMPLATE) && isset($constructorArgs['objectId']))
			return EventNotificationTemplatePeer::retrieveTypeByPK(self::getEmailNotificationTemplateTypeCoreValue(EmailNotificationTemplateType::EMAIL), $constructorArgs['objectId']);
		
			
		if ($baseClass == 'KEmailNotificationRecipientEngine')
		{
			list($recipientJobData) = $constructorArgs;
			switch ($enumValue)	
			{
				case KontorolEmailNotificationRecipientProviderType::CATEGORY:
					return new KEmailNotificationCategoryRecipientEngine($recipientJobData);
					break;
				case KontorolEmailNotificationRecipientProviderType::STATIC_LIST:
					return new KEmailNotificationStaticRecipientEngine($recipientJobData);
					break;
				case KontorolEmailNotificationRecipientProviderType::USER:
					return new KEmailNotificationUserRecipientEngine($recipientJobData);
					break;
				case KontorolEmailNotificationRecipientProviderType::GROUP:
					return new KEMailNotificationGroupRecipientEngine($recipientJobData);
					break;
			}
		}
		
		$class = self::getObjectClass($baseClass, $enumValue);
		if($class)
		{
			if(is_array($constructorArgs))
			{
				$reflect = new ReflectionClass($class);
				return $reflect->newInstanceArgs($constructorArgs);
			}
			
			return new $class();
		}
			
		return null;
	}
		
	/* (non-PHPdoc)
	 * @see IKontorolObjectLoader::getObjectClass()
	 */
	public static function getObjectClass($baseClass, $enumValue)
	{
		if($baseClass == 'KontorolEventNotificationDispatchJobData' && $enumValue == self::getEmailNotificationTemplateTypeCoreValue(EmailNotificationTemplateType::EMAIL))
			return 'KontorolEmailNotificationDispatchJobData';
	
		if($baseClass == 'EventNotificationTemplate' && $enumValue == self::getEmailNotificationTemplateTypeCoreValue(EmailNotificationTemplateType::EMAIL))
			return 'EmailNotificationTemplate';
	
		if($baseClass == 'KontorolEventNotificationTemplate' && $enumValue == self::getEmailNotificationTemplateTypeCoreValue(EmailNotificationTemplateType::EMAIL))
			return 'KontorolEmailNotificationTemplate';
	
		if($baseClass == 'Form_EventNotificationTemplateConfiguration' && $enumValue == Kontorol_Client_EventNotification_Enum_EventNotificationTemplateType::EMAIL)
			return 'Form_EmailNotificationTemplateConfiguration';
	
		if($baseClass == 'Kontorol_Client_EventNotification_Type_EventNotificationTemplate' && $enumValue == Kontorol_Client_EventNotification_Enum_EventNotificationTemplateType::EMAIL)
			return 'Kontorol_Client_EmailNotification_Type_EmailNotificationTemplate';
	
		if($baseClass == 'KDispatchEventNotificationEngine' && $enumValue == KontorolEventNotificationTemplateType::EMAIL)
			return 'KDispatchEmailNotificationEngine';
			
		return null;
	}

	/* (non-PHPdoc)
	 * @see IKontorolPending::dependsOn()
	 */
	public static function dependsOn() 
	{
		$minVersion = new KontorolVersion(
			self::EVENT_NOTIFICATION_PLUGIN_VERSION_MAJOR,
			self::EVENT_NOTIFICATION_PLUGIN_VERSION_MINOR,
			self::EVENT_NOTIFICATION_PLUGIN_VERSION_BUILD
		);
		$dependency = new KontorolDependency(self::EVENT_NOTIFICATION_PLUGIN_NAME, $minVersion);
		
		return array($dependency);
	}

	/**
	 * @return int id of dynamic enum in the DB.
	 */
	public static function getEmailNotificationFileSyncObjectTypeCoreValue($valueName)
	{
		$value = self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
		return kPluginableEnumsManager::apiToCore('FileSyncObjectType', $value);
	}
	
	/**
	 * @return int id of dynamic enum in the DB.
	 */
	public static function getEmailNotificationTemplateTypeCoreValue($valueName)
	{
		$value = self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
		return kPluginableEnumsManager::apiToCore('EventNotificationTemplateType', $value);
	}
	
	/**
	 * @return string external API value of dynamic enum.
	 */
	public static function getApiValue($valueName)
	{
		return self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
	}
}

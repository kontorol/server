<?php

/**
 * @package plugins.httpNotification
 * @subpackage api.errors
 */
class KontorolHttpNotificationErrors extends KontorolEventNotificationErrors
{
	const HTTP_NOTIFICATION_INVALID_OBJECT_TYPE = "HTTP_NOTIFICATION_INVALID_OBJECT_TYPE;;Invalid API object type";
}

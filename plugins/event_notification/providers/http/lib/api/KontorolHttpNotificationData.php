<?php
/**
 * @package plugins.httpNotification
 * @subpackage api.objects
 * @abstract
 */
abstract class KontorolHttpNotificationData extends KontorolObject
{
	/**
	 * @param kHttpNotificationData $coreObject
	 * @return KontorolHttpNotificationData
	 */
	public static function getInstance(kHttpNotificationData $coreObject)
	{
		$dataType = get_class($coreObject);
		$data = null;
		switch ($dataType)
		{
			case 'kHttpNotificationDataFields':
				$data = new KontorolHttpNotificationDataFields();
				break;
				
			case 'kHttpNotificationDataText':
				$data = new KontorolHttpNotificationDataText();
				break;
				
			case 'kHttpNotificationObjectData':
				$data = new KontorolHttpNotificationObjectData();
				break;
				
			default:
				$data = KontorolPluginManager::loadObject('KontorolHttpNotificationData', $dataType);
				break;
		}
		
		if($data)
			$data->fromObject($coreObject);
			
		return $data;
	}

	/**
	 * @param $jobData kHttpNotificationDispatchJobData
	 * @return string the data to be sent
	 */
	abstract public function getData(kHttpNotificationDispatchJobData $jobData = null);

	public function getContentType()
	{
		return null;
	}

}

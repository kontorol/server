<?php
/**
 * @package plugins.httpNotification
 * @subpackage api.enum
 */
class KontorolHttpNotificationSslVersion extends KontorolEnum
{
	const V2 = 2;
	const V3 = 3;
}

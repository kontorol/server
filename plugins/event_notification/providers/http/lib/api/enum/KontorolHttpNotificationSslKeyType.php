<?php
/**
 * @package plugins.httpNotification
 * @subpackage api.enum
 */
class KontorolHttpNotificationSslKeyType extends KontorolStringEnum
{
	const PEM = 'PEM';
	const DER = 'DER';
	const ENG = 'ENG';
}

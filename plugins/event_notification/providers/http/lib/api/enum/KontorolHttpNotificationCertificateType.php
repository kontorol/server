<?php
/**
 * @package plugins.httpNotification
 * @subpackage api.enum
 */
class KontorolHttpNotificationCertificateType extends KontorolStringEnum
{
	const PEM = 'PEM';
	const DER = 'DER';
	const ENG = 'ENG';
}

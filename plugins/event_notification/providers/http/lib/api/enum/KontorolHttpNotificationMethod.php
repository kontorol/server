<?php
/**
 * @package plugins.httpNotification
 * @subpackage api.enum
 */
class KontorolHttpNotificationMethod extends KontorolEnum
{
	const GET = 1;
	const POST = 2;
	const PUT = 3;
	const DELETE = 4;
}

<?php
/**
 * Evaluates PHP statement, depends on the execution context
 * 
 * @package plugins.httpNotification
 * @subpackage api.objects
 */
class KontorolHttpNotificationObjectData extends KontorolHttpNotificationData
{
	/**
	 * Kontorol API object type
	 * @var string
	 */
	public $apiObjectType;
	
	/**
	 * Data format
	 * @var KontorolResponseType
	 */
	public $format;
	
	/**
	 * Ignore null attributes during serialization
	 * @var bool
	 */
	public $ignoreNull;
	
	/**
	 * PHP code
	 * @var string
	 */
	public $code;

	/**
	 * An array of pattern-replacement pairs used for data string regex replacements
	 * @var KontorolKeyValueArray
	 */
	public $dataStringReplacements;

	/**
	 * Serialized object, protected on purpose, used by getData
	 * @see KontorolHttpNotificationObjectData::getData()
	 * @var string
	 */
	protected $coreObject;

	static private $map_between_objects = array
	(
		'apiObjectType' => 'objectType',
		'format',
		'ignoreNull',
		'code',
		'dataStringReplacements',
	);

	/* (non-PHPdoc)
	 * @see KontorolValue::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbObject = null, $skip = array())
	{
		if(!$this->apiObjectType || !is_subclass_of($this->apiObjectType, 'KontorolObject'))
			throw new KontorolAPIException(KontorolHttpNotificationErrors::HTTP_NOTIFICATION_INVALID_OBJECT_TYPE);
			
		if(!$dbObject)
			$dbObject = new kHttpNotificationObjectData();
			
		return parent::toObject($dbObject, $skip);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::fromObject($srcObj)
	 */
	public function doFromObject($srcObj, KontorolDetachedResponseProfile $responseProfile = null)
	{
		/* @var $srcObj kHttpNotificationObjectData */
		parent::doFromObject($srcObj, $responseProfile);
		$this->coreObject = $srcObj->getCoreObject();
	}
	
	/* (non-PHPdoc)
	 * @see KontorolHttpNotificationData::getData()
	 */
	public function getData(kHttpNotificationDispatchJobData $jobData = null)
	{
		$coreObject = unserialize($this->coreObject);

		$apiObject = new $this->apiObjectType;
		/* @var $apiObject KontorolObject */
		$apiObject->fromObject($coreObject);
		
		$httpNotificationTemplate = EventNotificationTemplatePeer::retrieveByPK($jobData->getTemplateId());
		
		$notification = new KontorolHttpNotification();
		$notification->object = $apiObject;
		$notification->eventObjectType = kPluginableEnumsManager::coreToApi('EventNotificationEventObjectType', $httpNotificationTemplate->getObjectType());
		$notification->eventNotificationJobId = $jobData->getJobId();
		$notification->templateId = $httpNotificationTemplate->getId();
		$notification->templateName = $httpNotificationTemplate->getName();
		$notification->templateSystemName = $httpNotificationTemplate->getSystemName();
		$notification->eventType = $httpNotificationTemplate->getEventType();

		$data = '';
		switch ($this->format)
		{
			case KontorolResponseType::RESPONSE_TYPE_XML:
				$serializer = new KontorolXmlSerializer($this->ignoreNull);
				$data = '<notification>' . $serializer->serialize($notification) . '</notification>';
				break;
				
			case KontorolResponseType::RESPONSE_TYPE_PHP:
				$serializer = new KontorolPhpSerializer($this->ignoreNull);
				$data = $serializer->serialize($notification);
				break;
				
			case KontorolResponseType::RESPONSE_TYPE_JSON:
				$serializer = new KontorolJsonSerializer($this->ignoreNull);
				$data = $serializer->serialize($notification);

				if($this->dataStringReplacements)
				{
					KontorolLog::info("replacing data string");
					$patterns = array();
					$replacements = array();
					foreach($this->dataStringReplacements->toArray() as $dataStringReplacement)
					{
						$patterns[] = "/" . $dataStringReplacement->key . "/";
						$replacements[] = $dataStringReplacement->value;
					}

					if(!empty($patterns))
						$data = preg_replace($patterns, $replacements, $data);
				}

				if (!$httpNotificationTemplate->getUrlEncode())
					return $data;

				$data = urlencode($data);
				break;
		}
		
		return "data=$data";
	}

	public function getContentType()
	{
		$contentType = null;

		switch ($this->format)
		{
			case KontorolResponseType::RESPONSE_TYPE_JSON:
				$contentType = 'application/json';
				break;

			case KontorolResponseType::RESPONSE_TYPE_XML:
				$contentType = 'application/xml';
				break;
		}

		return $contentType;
	}
}

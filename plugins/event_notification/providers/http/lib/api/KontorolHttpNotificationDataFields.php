<?php
/**
 * If this class used as the template data, the fields will be taken from the content parameters
 * @package plugins.httpNotification
 * @subpackage api.objects
 */
class KontorolHttpNotificationDataFields extends KontorolHttpNotificationData
{
	/**
	 * It's protected on purpose, used by getData
	 * @see KontorolHttpNotificationDataFields::getData()
	 * @var string
	 */
	protected $data;
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbObject = null, $propertiesToSkip = array())
	{
		if(is_null($dbObject))
			$dbObject = new kHttpNotificationDataFields();
			
		return parent::toObject($dbObject, $propertiesToSkip);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::fromObject($srcObj)
	 */
	public function doFromObject($srcObj, KontorolDetachedResponseProfile $responseProfile = null)
	{
		/* @var $srcObj kHttpNotificationDataFields */
		parent::doFromObject($srcObj, $responseProfile);
		
		if($this->shouldGet('data', $responseProfile))
			$this->data = $srcObj->getData();
	}
	
	/* (non-PHPdoc)
	 * @see KontorolHttpNotificationData::getData()
	 */
	public function getData(kHttpNotificationDispatchJobData $jobData = null)
	{
		return $this->data;
	}
}

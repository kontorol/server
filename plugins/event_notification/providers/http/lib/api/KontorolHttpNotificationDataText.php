<?php
/**
 * @package plugins.httpNotification
 * @subpackage api.objects
 */
class KontorolHttpNotificationDataText extends KontorolHttpNotificationData
{
	/**
	 * @var KontorolStringValue
	 */
	public $content;
	
	/**
	 * It's protected on purpose, used by getData
	 * @see KontorolHttpNotificationDataText::getData()
	 * @var string
	 */
	protected $data;
	
	private static $map_between_objects = array
	(
		'content',
	);

	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects ( )
	{
		return array_merge ( parent::getMapBetweenObjects() , self::$map_between_objects );
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbObject = null, $propertiesToSkip = array())
	{
		if(is_null($dbObject))
			$dbObject = new kHttpNotificationDataText();
			
		return parent::toObject($dbObject, $propertiesToSkip);
	}
	 
	/* (non-PHPdoc)
	 * @see KontorolObject::fromObject()
	 */
	public function doFromObject($dbObject, KontorolDetachedResponseProfile $responseProfile = null)
	{
		/* @var $dbObject kHttpNotificationDataText */
		parent::doFromObject($dbObject, $responseProfile);
		
		if($this->shouldGet('content', $responseProfile))
		{
			$contentType = get_class($dbObject->getContent());
			switch ($contentType)
			{
				case 'kStringValue':
					$this->content = new KontorolStringValue();
					break;
					
				case 'kEvalStringField':
					$this->content = new KontorolEvalStringField();
					break;
					
				default:
					$this->content = KontorolPluginManager::loadObject('KontorolStringValue', $contentType);
					break;
			}
			
			if($this->content)
				$this->content->fromObject($dbObject->getContent());
		}
			
		if($this->shouldGet('data', $responseProfile))
			$this->data = $dbObject->getData();
	}
	
	/* (non-PHPdoc)
	 * @see KontorolHttpNotificationData::getData()
	 */
	public function getData(kHttpNotificationDispatchJobData $jobData = null)
	{
		return $this->data;
	}
}

<%@ page import = "java.util.Map.Entry" %>
<%@ page import = "java.util.HashMap" %>
<%@ page import = "org.w3c.dom.Element" %>
<%@ page import = "com.kontorol.client.utils.ParseUtils" %>
<%@ page import = "com.kontorol.client.utils.XmlUtils" %>
<%@ page import = "com.kontorol.client.types.KontorolHttpNotification" %>
<%
String xmlData = request.getParameter("data");
Element xmlElement = XmlUtils.parseXml(xmlData);
KontorolHttpNotification httpNotification = ParseUtils.parseObject(KontorolHttpNotification.class, xmlElement);
HashMap<String, String> params = httpNotification.toParams();
for (Entry<String, String> itr : params.entrySet()) {
	out.println(itr.getKey() + " => " + itr.getValue());
}
%>

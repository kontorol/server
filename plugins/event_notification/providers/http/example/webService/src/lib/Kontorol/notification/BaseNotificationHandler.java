package lib.Kontorol.notification;

import lib.Kontorol.config.SessionConfig;
import lib.Kontorol.output.Console;

import com.kontorol.client.KontorolApiException;
import com.kontorol.client.KontorolClient;
import com.kontorol.client.enums.KontorolSessionType;
import com.kontorol.client.types.KontorolHttpNotification;

/**
 * This class is a base class for all the notification handlers 
 */
public abstract class BaseNotificationHandler {

	/** Kontorol client */
	private static KontorolClient apiClient = null;
	
	/** The console this handler use*/
	protected Console console;
	
	/**
	 * Constructor
	 * @param console
	 */
	public BaseNotificationHandler(Console console) {
		this.console = console;
	}

	/**
	 * @return The Kontorol client
	 * @throws Exception
	 */
	protected static KontorolClient getClient() {
		if (apiClient == null) {
			// Generates the Kontorol client. The parameters can be changed according to the need
			try {
				apiClient = SessionConfig.getClient(KontorolSessionType.ADMIN, "", 86400, "");
			} catch (Exception e) {
				throw new NotificationHandlerException("Failed to generate client : " + e.getMessage(), NotificationHandlerException.ERROR_PROCESSING);
			}
		}
		return apiClient;
	}

	/**
	 * This function decides whether this handle should handle the notification
	 * @param httpNotification The notification that is considered to be handled
	 * @return Whether this handler should handle this notification
	 */
	abstract public boolean shouldHandle(KontorolHttpNotification httpNotification);

	/**
	 * The handling function. 
	 * @param httpNotification The notification that should be handled
	 * @throws KontorolApiException In case something bad happened
	 */
	abstract public void handle(KontorolHttpNotification httpNotification);

	/**
	 * @return The notification processing timing
	 */
	public HandlerProcessType getType() {
		return HandlerProcessType.PROCESS;
	}
}

package lib.Kontorol.config;


import com.kontorol.client.KontorolClient;
import com.kontorol.client.KontorolConfiguration;
import com.kontorol.client.enums.KontorolSessionType;

/**
 * This class centralizes the session configuration 
 */
public class SessionConfig {
	
	/** The partner who is executing this client */
	public static final int KONTOROL_PARTNER_ID = PARTNER_ID;
	/** The secret of the indicated partner */
	public static final String KONTOROL_ADMIN_SECRET = "KONTOROL_ADMIN_SECRET";
	/** Kontorol service url - the end point*/
	public static final String KONTOROL_SERVICE_URL = "END-POINT";
	
	/**
	 * This function generates Kontorol Client according to the given ids
	 * @param sessionType KontorolSessionType - whether the session is admin or user session
	 * @param userId String - The user ID.
	 * @param sessionExpiry int - The session expire value. 
	 * @param sessionPrivileges String - The session privileges. 
	 * @return The generated client
	 * @throws Exception In case the client generation failed for some reason.
	 */
	public static KontorolClient getClient(KontorolSessionType sessionType, String userId, int sessionExpiry, String sessionPrivileges) throws Exception {
		
		// Create KontorolClient object using the accound configuration
		KontorolConfiguration config = new KontorolConfiguration();
		config.setPartnerId(KONTOROL_PARTNER_ID);
		config.setEndpoint(KONTOROL_SERVICE_URL);
		KontorolClient client = new KontorolClient(config);
		
		// Generate KS string locally, without calling the API
		String ks = client.generateSession(
			KONTOROL_ADMIN_SECRET,
			userId,
			sessionType,
			config.getPartnerId(),
			sessionExpiry,
			sessionPrivileges
		);
		client.setSessionId(ks);
		
		// Returns the KontorolClient object
		return client;
	}
}

package lib.Kontorol.notification.handlers;

import lib.Kontorol.notification.BaseNotificationHandler;
import lib.Kontorol.notification.NotificationHandlerException;
import lib.Kontorol.output.Console;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.kontorol.client.KontorolApiException;
import com.kontorol.client.enums.KontorolEventNotificationEventObjectType;
import com.kontorol.client.enums.KontorolEventNotificationEventType;
import com.kontorol.client.enums.KontorolMetadataObjectType;
import com.kontorol.client.types.KontorolHttpNotification;
import com.kontorol.client.types.KontorolMediaEntry;
import com.kontorol.client.types.KontorolMetadata;
import com.kontorol.client.types.KontorolMetadataFilter;
import com.kontorol.client.types.KontorolMetadataListResponse;
import com.kontorol.client.utils.XmlUtils;

public class SyncSampleHandler extends BaseNotificationHandler {

	// Handler constants
	protected static final int METADATA_PROFILE_ID = METADATA_PROFILE_ID;
	protected static final String APPROVAL_FIELD_NAME = "ApprovalStatus";
	protected static final String SYNC_FIELD_NAME = "SyncStatus";
	
	// Constants
	protected static final String SYNC_NEEDED = "Sync Needed";
	protected static final String SYNC_DONE = "Sync Done";
	
	public SyncSampleHandler(Console console) {
		super(console);
	}
	
	public boolean shouldHandle(KontorolHttpNotification httpNotification) {
		// Only handles if the event type is custom-metadata field changed.
		if(!((httpNotification.eventType.equals(KontorolEventNotificationEventType.OBJECT_DATA_CHANGED)) &&
				(httpNotification.eventObjectType.equals(KontorolEventNotificationEventObjectType.METADATA))))
			return false;
		
		// Only handle metadata of entries
		KontorolMetadata object = (KontorolMetadata)httpNotification.object;
		
		// Test that the metadata profile is the one we test
		if(object.metadataProfileId != METADATA_PROFILE_ID)
			return false;
		
		return (object.metadataObjectType == KontorolMetadataObjectType.ENTRY);
	}
	
	/**
	 * The handling function. 
	 * @param httpNotification The notification that should be handled
	 * @throws KontorolApiException In case something bad happened
	 */
	public void handle(KontorolHttpNotification httpNotification) {
	
		try {
			// Since the custom-metadata is the returned object, there is no need in querying it.
			KontorolMetadata metadata = (KontorolMetadata)httpNotification.object;
			
			// If the custom metadata in within another custom-metadata profile, retrieve it by executing
			// KontorolMetadata extraMetadata = fetchMetadata(metadata.objectId, OTHER METADATA PROFILE ID)
			
			String approvalStatus = getValue(metadata.xml, APPROVAL_FIELD_NAME);
			if(approvalStatus == null)
				return;
			
			// Entry retrieval for basic and common attributes.
			KontorolMediaEntry entry = fetchEntry(metadata.objectId);
			
			if(approvalStatus.equals("Ready For Website")) {
				handleReadyForSite(entry, metadata);
			} else if (approvalStatus.equals("Deleted")) {
				handleDelete(entry, metadata);
			} 
			// TODO - Add other cases here, in this code sample we're only monitoring these values. 
			
		} catch (KontorolApiException e) {
			console.write("Failed while handling notification");
			console.write(e.getMessage());
			throw new NotificationHandlerException("Failed while handling notification" + e.getMessage(), NotificationHandlerException.ERROR_PROCESSING);
		}
	}
	
	/**
     * Fetch an entry using the API
     *
     * @param String, entryId: id of the entry you want to fetch
     * @return 
     * @throws KontorolApiException
     * @throws Exception 
     *
     */
	protected KontorolMediaEntry fetchEntry(String entryId) throws KontorolApiException{
		 return getClient().getMediaService().get(entryId);
	}
	
	/**
	 * This function fetches the metadata of a given type for a given entry
	 * @param entryId The entry for which we fetch the metadata
	 * @param metadataProfileId The metadata profile id
	 * @return The matching metadata
	 * @throws KontorolApiException
	 */
	protected KontorolMetadata fetchMetadata(String entryId, int metadataProfileId)
			throws KontorolApiException {
		KontorolMetadataFilter filter = new KontorolMetadataFilter();
		filter.objectIdEqual = entryId;
		filter.metadataProfileIdEqual = metadataProfileId;
		KontorolMetadataListResponse metadatas = getClient().getMetadataService().list(filter);
		if(metadatas.totalCount == 0) {
			console.write("Failed to retrieve metadata for entry " + entryId + " and profile " + metadataProfileId);
			return null;
		}
		
		KontorolMetadata metadata = metadatas.objects.get(0);
		console.write("Successfully retrieved metadata. ID " + metadata.id);
		return metadata;
	}
	
	/**
	 * This function handles the case in which an entry was marked as deleted
	 * @param entry The entry.
	 * @param syncMetadata The SyncMetadataObject
	 * @throws KontorolApiException
	 */
	protected void handleDelete(KontorolMediaEntry entry, KontorolMetadata syncMetadata) throws KontorolApiException {
		if(!SYNC_DONE.equals(getValue(syncMetadata.xml, SYNC_FIELD_NAME))) {
			console.write("Entry is not marked as synched with the CMS, do nothing");
			return;
		}
		
		console.write("The entry " + entry.name + " has been marked as deleted on Kontorol. Sync this delete with customer's website CMS");
		deleteReference(entry, syncMetadata);
		// Mark the entry again as sync needed as we removed it from the CMS
		updateSyncStatus(syncMetadata, SYNC_NEEDED);
	}

	/**
	 * This function handles the case in which an entry is ready for site
	 * @param entry The entry.
	 * @param syncMetadata The SyncMetadataObject
	 * @throws KontorolApiException
	 */
	protected void handleReadyForSite(KontorolMediaEntry entry, KontorolMetadata syncMetadata) throws KontorolApiException {
		if(!SYNC_NEEDED.equals(getValue(syncMetadata.xml, SYNC_FIELD_NAME))) {
			console.write("No sync is needed");
			return;
		}
		
		console.write("The entry " + entry.name + " has been approved to be synced with customer's website CMS");
		syncReference(entry, syncMetadata);
		updateSyncStatus(syncMetadata, SYNC_DONE);
	}

	/**
	 * This function updates the sync field value
	 * @param object The metadata object we'd like to update
	 * @param newValue The new value for the sync field
	 * @throws KontorolApiException
	 */
	protected void updateSyncStatus(KontorolMetadata object, String newValue) throws KontorolApiException {
		String xml = object.xml;
		String oldValue = getValue(xml, SYNC_FIELD_NAME);
		String oldStr = "<" + SYNC_FIELD_NAME +">" + oldValue + "</" + SYNC_FIELD_NAME +">";
		String newStr = "<" + SYNC_FIELD_NAME +">" + newValue + "</" + SYNC_FIELD_NAME +">";
		xml = xml.replaceAll(oldStr, newStr);
		
		getClient().getMetadataService().update(object.id, xml);
	}

	/**
	 * This function parses an XML and returns a specific field value from it
	 * @param xml The parsed XML
	 * @param fieldName The field name we want to retrieve
	 * @return The field avtual value
	 * @throws KontorolApiException
	 */
	protected static String getValue(String xml, String fieldName) throws KontorolApiException {
		Element xmlElement = XmlUtils.parseXml(xml);
		NodeList childNodes = xmlElement.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node aNode = childNodes.item(i);
			String nodeName = aNode.getNodeName();
			if (nodeName.equals(fieldName))
				return aNode.getTextContent();
		}
		return null;
	}
	
	// Customer specific functions
	
	/**
	 * This function should delete an object reference from the external system 
	 * @param entry The entry that has to be deleted
	 * @param object The metadata object describing the object
	 */
	protected void deleteReference(KontorolMediaEntry entry, KontorolMetadata object) {
		console.write("Delete this entry's reference from your external system");
		// TODO - Add your code here
	}
	
	/**
	 * This function should sync an object reference to the external system 
	 * @param entry The entry that has to be synced
	 * @param object The metadata object describing the object
	 */
	protected void syncReference(KontorolMediaEntry entry, KontorolMetadata object) {
		console.write("Sync the entry to your external system");
		// TODO - Add your code here
	}
}

<%@page import="java.io.BufferedReader"%>
<%@page import="org.w3c.dom.Element"%>
<%@page import="com.kontorol.client.utils.XmlUtils"%>
<%@page import="lib.Kontorol.HttpNotificationHandler"%>
<%@page import="com.kontorol.client.types.KontorolHttpNotification"%>
<%@page import="com.kontorol.client.utils.ParseUtils"%>
<%@page import="lib.Kontorol.RequestHandler"%>
<%

BufferedReader reader = request.getReader();
StringBuffer sb = new StringBuffer("");
String line;
while ((line = reader.readLine()) != null){
	sb.append(new String(line.getBytes("ISO-8859-1"), "UTF-8"));
}
reader.reset();

String xml = sb.toString();
String signature = request.getHeader("x-kontorol-signature");
RequestHandler.validateSignature(xml, SessionConfig.KONTOROL_ADMIN_SECRET, signature);

int dataOffset = xml.indexOf("data=");
if(dataOffset < 0) {
	System.out.println("Couldn't find data");
}

String xmlData = xml.substring(5);
Element xmlElement = XmlUtils.parseXml(xmlData);
KontorolHttpNotification httpNotification = ParseUtils.parseObject(KontorolHttpNotification.class, xmlElement);

HttpNotificationHandler handler = new HttpNotificationHandler();
handler.handle(httpNotification);
handler.finalize();

%>

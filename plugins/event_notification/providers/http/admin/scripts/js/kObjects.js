
var kObjects = {
	coreObjectType: {
		label: 			'Event',
		subSelections:	{
			baseEntry:						{label: 'Base Entry', coreType: 'entry', apiType: 'KontorolBaseEntry'},
			dataEntry:						{label: 'Data Entry', coreType: 'entry', apiType: 'KontorolDataEntry'},
			documentEntry:					{label: 'Document Entry', coreType: 'entry', apiType: 'KontorolDocumentEntry'},
			mediaEntry:						{label: 'Media Entry', coreType: 'entry', apiType: 'KontorolMediaEntry'},
			externalMediaEntry:				{label: 'External Media Entry', coreType: 'entry', apiType: 'KontorolExternalMediaEntry'},
			liveStreamEntry:				{label: 'Live Stream Entry', coreType: 'entry', apiType: 'KontorolLiveStreamEntry'},
			playlist:						{label: 'Playlist', coreType: 'entry', apiType: 'KontorolPlaylist'},
			category:						{label:	'Category', apiType: 'KontorolCategory'},
			kuser:							{label:	'User', apiType: 'KontorolUser'},
	        CuePoint:						{label:	'CuePoint', apiType: 'KontorolCuePoint'},
	        AdCuePoint:						{label:	'Ad Cue-Point', apiType: 'KontorolAdCuePoint'},
	        Annotation:						{label:	'Annotation', apiType: 'KontorolAnnotation'},
	        CodeCuePoint:					{label:	'Code Cue-Point', apiType: 'KontorolCodeCuePoint'},
	        DistributionProfile:			{label:	'Distribution Profile', apiType: 'KontorolDistributionProfile'},
	        EntryDistribution:				{label:	'Entry Distribution', apiType: 'KontorolEntryDistribution'},
	        Metadata:						{label:	'Metadata', apiType: 'KontorolMetadata'},
	        asset:							{label:	'Asset', apiType: 'KontorolAsset'},
	        attachmentAsset:				{label: 'AttachmentAsset', apiType: 'KontorolAttachmentAsset'},
	        flavorAsset:					{label:	'Flavor Asset', apiType: 'KontorolFlavorAsset'},
	        thumbAsset:						{label:	'Thumbnail Asset', apiType: 'KontorolThumbAsset'},
	        accessControl:					{label:	'Access Control', apiType: 'KontorolAccessControlProfile'},
	        BatchJob:						{label:	'BatchJob', apiType: 'KontorolBatchJob'},
	        BulkUploadResultEntry:			{label:	'Bulk-Upload Entry Result', apiType: 'KontorolBulkUploadResultEntry'},
	        BulkUploadResultCategory:		{label:	'Bulk-Upload Category Result', apiType: 'KontorolBulkUploadResultCategory'},
	        BulkUploadResultKuser:			{label:	'Bulk-Upload User Result', apiType: 'KontorolBulkUploadResultUser'},
	        BulkUploadResultCategoryKuser:	{label:	'Bulk-Upload Category - User Result', apiType: 'KontorolBulkUploadResultCategoryUser'},
	        categoryKuser:					{label:	'Category - User', apiType: 'KontorolCategoryUser'},
	        conversionProfile2:				{label:	'Conversion Profile', apiType: 'KontorolConversionProfile'},
	        flavorParams:					{label:	'Flavor Params', apiType: 'KontorolFlavorParams'},
	        flavorParamsConversionProfile:	{label:	'Asset Params - Conversion Profile', apiType: 'KontorolConversionProfileAssetParams'},
	        flavorParamsOutput:				{label:	'Flavor Params Output', apiType: 'KontorolFlavorParamsOutput'},
	        genericsynDicationFeed:			{label:	'Genericsyn Dication Feed', apiType: 'KontorolGenericsynDicationFeed'},
	        Partner:						{label:	'Partner', apiType: 'KontorolPartner'},
	        Permission:						{label:	'Permission', apiType: 'KontorolPermission'},
	        PermissionItem:					{label:	'Permission Item', apiType: 'KontorolPermissionItem'},
	        Scheduler:						{label:	'Scheduler', apiType: 'KontorolScheduler'},
	        SchedulerConfig:				{label:	'Scheduler Config', apiType: 'KontorolSchedulerConfig'},
	        SchedulerStatus:				{label:	'Scheduler Status', apiType: 'KontorolSchedulerStatus'},
	        SchedulerWorker:				{label:	'Scheduler Worker', apiType: 'KontorolSchedulerWorker'},
	        StorageProfile:					{label:	'Storage Profile', apiType: 'KontorolStorageProfile'},
	        thumbParams:					{label:	'Thumbnail Params', apiType: 'KontorolThumbParams'},
	        thumbParamsOutput:				{label:	'Thumbnail Params Output', apiType: 'KontorolThumbParamsOutput'},
	        UploadToken:					{label:	'Upload Token', apiType: 'KontorolUploadToken'},
	        UserLoginData:					{label:	'User Login Data', apiType: 'KontorolUserLoginData'},
	        UserRole:						{label:	'User Role', apiType: 'KontorolUserRole'},
	        widget:							{label:	'Widget', apiType: 'KontorolWidget'},
	        categoryEntry:					{label:	'Category - Entry', apiType: 'KontorolCategoryEntry'},
	        entryVendorTask:					{label: 'Entry Vendor Task', apiType: 'KontorolEntryVendorTask'}
		},
		subLabel:		'Select Object Type',
		getData:		function(subCode, variables){
							var coreType = variables.value;
							if(variables.coreType != null)
								coreType = variables.coreType;
								
							var ret = {
								code: '(($scope->getEvent()->getObject() instanceof ' + coreType + ') ? $scope->getEvent()->getObject() : null)',
								coreType: coreType
							};
							
							if(variables.apiType != null)
								ret.apiName = variables.apiType;
								
							return ret;
		}
	}
};

<?php
/**
 * @package plugins.eventNotification
 * @subpackage model.enum
 */ 
class EventNotificationBatchType implements IKontorolPluginEnum, BatchJobType
{
	const EVENT_NOTIFICATION_HANDLER = 'EventNotificationHandler';
	
	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalValues()
	 */
	public static function getAdditionalValues()
	{
		return array(
			'EVENT_NOTIFICATION_HANDLER' => self::EVENT_NOTIFICATION_HANDLER,
		);
	}

	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalDescriptions()
	 */
	public static function getAdditionalDescriptions() 
	{
		return array();
	}

}

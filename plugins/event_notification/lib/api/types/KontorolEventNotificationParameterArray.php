<?php
/**
 * @package plugins.eventNotification
 * @subpackage api.objects
 */
class KontorolEventNotificationParameterArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolEventNotificationParameterArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
			$parameterType = get_class($obj);
			switch ($parameterType)
			{
				case 'kEventNotificationParameter':
    				$nObj = new KontorolEventNotificationParameter();
					break;
					
				case 'kEventNotificationArrayParameter':
    				$nObj = new KontorolEventNotificationArrayParameter();
					break;
					
				default:
    				$nObj = KontorolPluginManager::loadObject('KontorolEventNotificationParameter', $parameterType);
			}
			
			if($nObj)
			{
				$nObj->fromObject($obj, $responseProfile);
				$newArr[] = $nObj;
			}
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolEventNotificationParameter");
	}
}

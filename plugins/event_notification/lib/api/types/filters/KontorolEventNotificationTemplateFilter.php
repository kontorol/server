<?php
/**
 * @package plugins.eventNotification
 * @subpackage api.filters
 */
class KontorolEventNotificationTemplateFilter extends KontorolEventNotificationTemplateBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new EventNotificationTemplateFilter();
	}
}

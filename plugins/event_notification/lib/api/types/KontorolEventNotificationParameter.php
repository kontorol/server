<?php
/**
 * @package plugins.eventNotification
 * @subpackage api.objects
 */
class KontorolEventNotificationParameter extends KontorolObject
{
	/**
	 * The key in the subject and body to be replaced with the dynamic value
	 * @var string
	 */
	public $key;

	/**
	 * @var string
	 */
	public $description;
	
	/**
	 * The dynamic value to be placed in the final output
	 * @var KontorolStringValue
	 */
	public $value;
	
	private static $map_between_objects = array
	(
		'key',
		'description',
		'value',
	);

	public function getMapBetweenObjects ( )
	{
		return array_merge ( parent::getMapBetweenObjects() , self::$map_between_objects );
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbObject = null, $skip = array())
	{
		if(!$dbObject)
			$dbObject = new kEventNotificationParameter();
			
		return parent::toObject($dbObject, $skip);
	}
	 
	/* (non-PHPdoc)
	 * @see KontorolObject::fromObject()
	 */
	public function doFromObject($dbObject, KontorolDetachedResponseProfile $responseProfile = null)
	{
		/* @var $dbObject kEventValueCondition */
		parent::doFromObject($dbObject, $responseProfile);
		
		$valueType = get_class($dbObject->getValue());
		KontorolLog::debug("Loading KontorolStringValue from type [$valueType]");
		switch ($valueType)
		{
			case 'kMetadataField':
				$this->value = new KontorolMetadataField();
				break;
				
			case 'kStringValue':
				$this->value = new KontorolStringValue();
				break;
				
			case 'kEvalStringField':
				$this->value = new KontorolEvalStringField();
				break;
				
			default:
				$this->value = KontorolPluginManager::loadObject('KontorolStringValue', $valueType);
				break;
		}
		
		if($this->value)
			$this->value->fromObject($dbObject->getValue());
	}
}

<?php
/**
 * @package plugins.eventNotification
 * @subpackage api.objects
 */
class KontorolEventNotificationTemplateListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolEventNotificationTemplateArray
	 * @readonly
	 */
	public $objects;
}

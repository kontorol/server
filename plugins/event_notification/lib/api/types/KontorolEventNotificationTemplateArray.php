<?php
/**
 * @package plugins.eventNotification
 * @subpackage api.objects
 */
class KontorolEventNotificationTemplateArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolEventNotificationTemplateArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
    		$nObj = KontorolEventNotificationTemplate::getInstanceByType($obj->getType());
    		if(!$nObj)
    		{
    			KontorolLog::err("Event notification template could not find matching type for [" . $obj->getType() . "]");
    			continue;
    		}
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolEventNotificationTemplate");
	}
}

<?php
/**
 * @package plugins.eventNotification
 * @subpackage api.enum
 * @see EventNotificationTemplateStatus
 */
class KontorolEventNotificationTemplateStatus extends KontorolEnum implements EventNotificationTemplateStatus
{
}

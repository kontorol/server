<?php
/**
 * @package plugins.eventNotification
 * @subpackage api.enum
 * @see EventNotificationEventObjectType
 */
class KontorolEventNotificationEventObjectType extends KontorolDynamicEnum implements EventNotificationEventObjectType
{
	public static function getEnumClass()
	{
		return 'EventNotificationEventObjectType';
	}
}

<?php
/**
 * @package plugins.eventNotification
 * @subpackage api.enum
 * @see EventNotificationTemplateType
 */
class KontorolEventNotificationTemplateType extends KontorolDynamicEnum implements EventNotificationTemplateType
{
	public static function getEnumClass()
	{
		return 'EventNotificationTemplateType';
	}
}

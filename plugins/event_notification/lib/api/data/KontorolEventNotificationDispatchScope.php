<?php
/**
 * @package plugins.eventNotification
 * @subpackage api.objects
 */
class KontorolEventNotificationDispatchScope extends KontorolEventNotificationScope
{
	/**
	 * @var KontorolKeyValueArray
	 */
	public $dynamicValues;

	public function toObject($objectToFill = null, $propsToSkip = array())
	{
		$objectToFill = parent::toObject($objectToFill, $propsToSkip);
		if ($this->dynamicValues)
		{
			foreach ($this->dynamicValues as $keyValueObject)
			{
				/* @var $keyValueObject KontorolKeyValue */
				$objectToFill->addDynamicValue($keyValueObject->key, new kStringValue($keyValueObject->value));
			}
		}
		return $objectToFill;
	}

}

<?php
/**
 * @package plugins.eventNotification
 * @subpackage api.objects
 */
class KontorolEventObjectChangedCondition extends KontorolCondition
{	
	/**
	 * Comma seperated column names to be tested
	 * @var string
	 */
	public $modifiedColumns;

	private static $map_between_objects = array
	(
		'modifiedColumns' ,
	);

	/* (non-PHPdoc)
	 * @see KontorolCondition::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects ( )
	{
		return array_merge ( parent::getMapBetweenObjects() , self::$map_between_objects );
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbObject = null, $skip = array())
	{
		if(!$dbObject)
			$dbObject = new kEventObjectChangedCondition();
	
		return parent::toObject($dbObject, $skip);
	}
}

<?php
/**
 * @package plugins.eventNotification
 * @subpackage api.objects
 */
class KontorolEventNotificationScope extends KontorolScope
{
	/**
	 * @var string
	 */
	public $objectId;

	/**
	 * @var KontorolEventNotificationEventObjectType
	 */
	public $scopeObjectType;

	public function toObject($objectToFill = null, $propsToSkip = array())
	{
		if (is_null($objectToFill))
			$objectToFill = new kEventNotificationScope();

		/** @var kEventNotificationScope $objectToFill */
		$objectToFill = parent::toObject($objectToFill);

		$objectClassName = KontorolPluginManager::getObjectClass('EventNotificationEventObjectType', kPluginableEnumsManager::apiToCore('EventNotificationEventObjectType', $this->scopeObjectType));
		$peerClass = $objectClassName.'Peer';
		$objectId = $this->objectId;
		if (class_exists($peerClass))
		{
			$objectToFill->setObject($peerClass::retrieveByPk($objectId));
		}
		else
		{
			$b = new $objectClassName();
			$peer = $b->getPeer();
			$object = $peer::retrieveByPK($objectId);
			$objectToFill->setObject($object);
		}

		if (is_null($objectToFill->getObject()))
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $this->objectId);

		return $objectToFill;
	}
}

<?php
/**
 * @package plugins.eventNotification
 * @subpackage api.objects
 * @deprecated
 */
class KontorolEventConditionArray extends KontorolTypedArray
{
	public function __construct()
	{
		parent::__construct("KontorolEventCondition");
	}
}

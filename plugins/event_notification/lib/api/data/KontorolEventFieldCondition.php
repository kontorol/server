<?php
/**
 * @package plugins.eventNotification
 * @subpackage api.objects
 */
class KontorolEventFieldCondition extends KontorolCondition
{	
	/**
	 * The field to be evaluated at runtime
	 * @var KontorolBooleanField
	 */
	public $field;

	private static $map_between_objects = array
	(
		'field' ,
	);

	public function getMapBetweenObjects ( )
	{
		return array_merge ( parent::getMapBetweenObjects() , self::$map_between_objects );
	}
	
	/**
	 * Init object type
	 */
	public function __construct()
	{
		$this->type = EventNotificationPlugin::getConditionTypeCoreValue(EventNotificationConditionType::EVENT_NOTIFICATION_FIELD);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbObject = null, $skip = array())
	{
		if(!$dbObject)
			$dbObject = new kEventFieldCondition();
	
		return parent::toObject($dbObject, $skip);
	}
	 
	/* (non-PHPdoc)
	 * @see KontorolObject::fromObject()
	 */
	public function doFromObject($dbObject, KontorolDetachedResponseProfile $responseProfile = null)
	{
		/* @var $dbObject kEventFieldCondition */
		parent::doFromObject($dbObject, $responseProfile);
		
		$fieldType = get_class($dbObject->getField());
		KontorolLog::debug("Loading KontorolBooleanField from type [$fieldType]");
		switch ($fieldType)
		{
			case 'kEvalBooleanField':
				$this->field = new KontorolEvalBooleanField();
				break;
				
			default:
				$this->field = KontorolPluginManager::loadObject('KontorolBooleanField', $fieldType);
				break;
		}
		
		if($this->field)
			$this->field->fromObject($dbObject->getField());
	}
}

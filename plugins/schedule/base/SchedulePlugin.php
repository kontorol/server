<?php
/**
 * @package plugins.schedule
 */
class SchedulePlugin extends KontorolPlugin implements IKontorolServices, IKontorolEventConsumers, IKontorolVersion, IKontorolObjectLoader, IKontorolScheduleEventProvider
{
	const PLUGIN_NAME = 'schedule';
	const PLUGIN_VERSION_MAJOR = 1;
	const PLUGIN_VERSION_MINOR = 0;
	const PLUGIN_VERSION_BUILD = 0;
	const SCHEDULE_EVENTS_CONSUMER = 'kScheduleEventsConsumer';
	const ICAL_RESPONSE_TYPE = 'ical';
	
	public static function dependsOn()
	{
		$metadataDependency = new KontorolDependency(self::METADATA_PLUGIN_NAME);
		
		return array($metadataDependency);
	}
	public static function getPluginName()
	{
		return self::PLUGIN_NAME;
	}
	
	/*
	 * (non-PHPdoc)
	 * @see IKontorolVersion::getVersion()
	 */
	public static function getVersion()
	{
		return new KontorolVersion(self::PLUGIN_VERSION_MAJOR, self::PLUGIN_VERSION_MINOR, self::PLUGIN_VERSION_BUILD);
	}
	
	/*
	 * (non-PHPdoc)
	 * @see IKontorolServices::getServicesMap()
	 */
	public static function getServicesMap()
	{
		$map = array('scheduleEvent' => 'ScheduleEventService', 'scheduleResource' => 'ScheduleResourceService', 'scheduleEventResource' => 'ScheduleEventResourceService');
		return $map;
	}
	
	/*
	 * (non-PHPdoc)
	 * @see IKontorolEventConsumers::getEventConsumers()
	 */
	public static function getEventConsumers()
	{
		return array(self::SCHEDULE_EVENTS_CONSUMER);
	}
	
	/*
	 * (non-PHPdoc)
	 * @see IKontorolObjectLoader::loadObject()
	 */
	public static function loadObject($baseClass, $enumValue, array $constructorArgs = null)
	{
		if($baseClass == 'KontorolSerializer' && $enumValue == self::ICAL_RESPONSE_TYPE)
			return new KontorolICalSerializer();
		
		return null;
	}
	
	/*
	 * (non-PHPdoc)
	 * @see IKontorolObjectLoader::getObjectClass()
	 */
	public static function getObjectClass($baseClass, $enumValue)
	{
		if($baseClass == 'KontorolSerializer' && $enumValue == self::ICAL_RESPONSE_TYPE)
			return 'KontorolICalSerializer';
		
		return null;
	}

	public static function getSingleScheduleEventMaxDuration()
	{
		$maxSingleScheduleEventDuration = 60 * 60 * 24; // 24 hours
		return kConf::get('max_single_schedule_event_duration', 'local', $maxSingleScheduleEventDuration);
	}

	public static function getScheduleEventmaxDuration()
	{
		$maxDuration = 60 * 60 * 24 * 365 * 2; // two years
		return kConf::get('max_schedule_event_duration', 'local', $maxDuration);
	}
	
	public static function getScheduleEventmaxRecurrences()
	{
		$maxRecurrences = 1000;
		return kConf::get('max_schedule_event_recurrences', 'local', $maxRecurrences);
	}

	/**
	 * @param string $entryId
	 * @param array $types
	 * @param int $startTime
	 * @param int $endTime
	 * @return array<IScheduleEvent>
	 */
	public function getScheduleEvents($entryId, $types, $startTime, $endTime)
	{
		$events = array();
		$scheduleEvents = ScheduleEventPeer::retrieveByTemplateEntryIdAndTypes($entryId, $types, $startTime, $endTime);
		foreach ($scheduleEvents as $scheduleEvent)
		{
			/* @var LiveStreamScheduleEvent $scheduleEvent*/
			if ($scheduleEvent->isRangeIntersects($startTime, $endTime))
			{
				$events[] = $scheduleEvent;
			}
		}
		return $events;
	}
}

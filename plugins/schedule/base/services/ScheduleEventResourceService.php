<?php

/**
 * The ScheduleEventResource service enables you create and manage (update, delete, retrieve, etc.) the connections between recording events and the resources required for these events (cameras, capture devices, etc.).
 * @service scheduleEventResource
 * @package plugins.schedule
 * @subpackage api.services
 */
class ScheduleEventResourceService extends KontorolBaseService
{
	/* (non-PHPdoc)
	 * @see KontorolBaseService::initService()
	 */
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		
		$this->applyPartnerFilterForClass('ScheduleEvent');
		$this->applyPartnerFilterForClass('ScheduleResource');
		$this->applyPartnerFilterForClass('ScheduleEventResource');
	}
	
	/**
	 * Allows you to add a new KontorolScheduleEventResource object
	 * 
	 * @action add
	 * @param KontorolScheduleEventResource $scheduleEventResource
	 * @return KontorolScheduleEventResource
	 */
	public function addAction(KontorolScheduleEventResource $scheduleEventResource)
	{
		$resourceReservator = new kResourceReservation();
		if (!$resourceReservator->checkAvailable($scheduleEventResource->resourceId))
			throw new KontorolAPIException(KontorolErrors::RESOURCE_IS_RESERVED, $scheduleEventResource->resourceId);
		$resourceReservator->reserve($scheduleEventResource->resourceId);
		
		// save in database
		$dbScheduleEventResource = $scheduleEventResource->toInsertableObject();
		$dbScheduleEventResource->save();
		
		// return the saved object
		$scheduleEventResource = new KontorolScheduleEventResource();
		$scheduleEventResource->fromObject($dbScheduleEventResource, $this->getResponseProfile());

		$resourceReservator->deleteReservation($scheduleEventResource->resourceId);
		return $scheduleEventResource;
	
	}
	
	/**
	 * Retrieve a KontorolScheduleEventResource object by ID
	 * 
	 * @action get
	 * @param int $scheduleEventId
	 * @param int $scheduleResourceId 
	 * @return KontorolScheduleEventResource
	 * 
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 */
	public function getAction($scheduleEventId, $scheduleResourceId)
	{
		$dbScheduleEventResource = ScheduleEventResourcePeer::retrieveByEventAndResource($scheduleEventId, $scheduleResourceId);
		if(!$dbScheduleEventResource)
		{
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, "$scheduleEventId,$scheduleResourceId");
		}
		
		$scheduleEventResource = new KontorolScheduleEventResource();
		$scheduleEventResource->fromObject($dbScheduleEventResource, $this->getResponseProfile());
		
		return $scheduleEventResource;
	}
	
	/**
	 * Update an existing KontorolScheduleEventResource object
	 * 
	 * @action update
	 * @param int $scheduleEventId
	 * @param int $scheduleResourceId 
	 * @param KontorolScheduleEventResource $scheduleEventResource
	 * @return KontorolScheduleEventResource
	 *
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 */
	public function updateAction($scheduleEventId, $scheduleResourceId, KontorolScheduleEventResource $scheduleEventResource)
	{
		$dbScheduleEventResource = ScheduleEventResourcePeer::retrieveByEventAndResource($scheduleEventId, $scheduleResourceId);
		if(!$dbScheduleEventResource)
		{
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, "$scheduleEventId,$scheduleResourceId");
		}
		
		$dbScheduleEventResource = $scheduleEventResource->toUpdatableObject($dbScheduleEventResource);
		$dbScheduleEventResource->save();
		
		$scheduleEventResource = new KontorolScheduleEventResource();
		$scheduleEventResource->fromObject($dbScheduleEventResource, $this->getResponseProfile());
		
		return $scheduleEventResource;
	}
	
	/**
	 * Mark the KontorolScheduleEventResource object as deleted
	 * 
	 * @action delete
	 * @param int $scheduleEventId
	 * @param int $scheduleResourceId 
	 *
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 */
	public function deleteAction($scheduleEventId, $scheduleResourceId)
	{
		$dbScheduleEventResource = ScheduleEventResourcePeer::retrieveByEventAndResource($scheduleEventId, $scheduleResourceId);
		if(!$dbScheduleEventResource)
		{
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, "$scheduleEventId,$scheduleResourceId");
		}
		
		$dbScheduleEventResource->delete();
		kEventsManager::raiseEvent(new kObjectErasedEvent($dbScheduleEventResource));
	}
	
	/**
	 * List KontorolScheduleEventResource objects
	 * 
	 * @action list
	 * @param KontorolScheduleEventResourceFilter $filter
	 * @param KontorolFilterPager $pager
	 * @param bool $filterBlackoutConflicts
	 * @return KontorolScheduleEventResourceListResponse
	 */
	public function listAction(KontorolScheduleEventResourceFilter $filter = null, KontorolFilterPager $pager = null,
							   $filterBlackoutConflicts = true)
	{
		if (!$filter)
		{
			$filter = new KontorolScheduleEventResourceFilter();
		}

		if(!$pager)
		{
			$pager = new KontorolFilterPager();
		}

		return $filter->getListResponse($pager, $this->getResponseProfile(), $filterBlackoutConflicts);
	}
}

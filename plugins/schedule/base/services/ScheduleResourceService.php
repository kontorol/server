<?php

/**
 * The ScheduleResource service enables you to create and manage (update, delete, retrieve, etc.) the resources required for scheduled events (cameras, capture devices, etc.).
 * @service scheduleResource
 * @package plugins.schedule
 * @subpackage api.services
 */
class ScheduleResourceService extends KontorolBaseService
{
	/* (non-PHPdoc)
	 * @see KontorolBaseService::initService()
	 */
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		
		$this->applyPartnerFilterForClass('ScheduleEvent');
		$this->applyPartnerFilterForClass('ScheduleResource');
		$this->applyPartnerFilterForClass('ScheduleEventResource');
	}
	
	/**
	 * Allows you to add a new KontorolScheduleResource object
	 * 
	 * @action add
	 * @param KontorolScheduleResource $scheduleResource
	 * @return KontorolScheduleResource
	 */
	public function addAction(KontorolScheduleResource $scheduleResource)
	{
		// save in database
		$dbScheduleResource = $scheduleResource->toInsertableObject();
		$dbScheduleResource->save();
		
		// return the saved object
		$scheduleResource = KontorolScheduleResource::getInstance($dbScheduleResource, $this->getResponseProfile());
		$scheduleResource->fromObject($dbScheduleResource, $this->getResponseProfile());
		return $scheduleResource;
	
	}
	
	/**
	 * Retrieve a KontorolScheduleResource object by ID
	 * 
	 * @action get
	 * @param int $scheduleResourceId 
	 * @return KontorolScheduleResource
	 * 
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 */
	public function getAction($scheduleResourceId)
	{
		$dbScheduleResource = ScheduleResourcePeer::retrieveByPK($scheduleResourceId);
		if(!$dbScheduleResource)
		{
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $scheduleResourceId);
		}
		
		$scheduleResource = KontorolScheduleResource::getInstance($dbScheduleResource, $this->getResponseProfile());
		$scheduleResource->fromObject($dbScheduleResource, $this->getResponseProfile());
		
		return $scheduleResource;
	}
	
	/**
	 * Update an existing KontorolScheduleResource object
	 * 
	 * @action update
	 * @param int $scheduleResourceId
	 * @param KontorolScheduleResource $scheduleResource
	 * @return KontorolScheduleResource
	 *
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 */
	public function updateAction($scheduleResourceId, KontorolScheduleResource $scheduleResource)
	{
		$dbScheduleResource = ScheduleResourcePeer::retrieveByPK($scheduleResourceId);
		if(!$dbScheduleResource)
		{
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $scheduleResourceId);
		}
		
		$dbScheduleResource = $scheduleResource->toUpdatableObject($dbScheduleResource);
		$dbScheduleResource->save();
		
		$scheduleResource = KontorolScheduleResource::getInstance($dbScheduleResource, $this->getResponseProfile());
		$scheduleResource->fromObject($dbScheduleResource, $this->getResponseProfile());
		
		return $scheduleResource;
	}
	
	/**
	 * Mark the KontorolScheduleResource object as deleted
	 * 
	 * @action delete
	 * @param int $scheduleResourceId 
	 * @return KontorolScheduleResource
	 *
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 */
	public function deleteAction($scheduleResourceId)
	{
		$dbScheduleResource = ScheduleResourcePeer::retrieveByPK($scheduleResourceId);
		if(!$dbScheduleResource)
		{
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $scheduleResourceId);
		}
		
		$dbScheduleResource->setStatus(ScheduleResourceStatus::DELETED);
		$dbScheduleResource->save();
		
		$scheduleResource = KontorolScheduleResource::getInstance($dbScheduleResource, $this->getResponseProfile());
		$scheduleResource->fromObject($dbScheduleResource, $this->getResponseProfile());
		
		return $scheduleResource;
	}
	
	/**
	 * List KontorolScheduleResource objects
	 * 
	 * @action list
	 * @param KontorolScheduleResourceFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return KontorolScheduleResourceListResponse
	 */
	public function listAction(KontorolScheduleResourceFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
			$filter = new KontorolScheduleResourceFilter();
			
		if(!$pager)
			$pager = new KontorolFilterPager();
			
		return $filter->getListResponse($pager, $this->getResponseProfile());
	}
}

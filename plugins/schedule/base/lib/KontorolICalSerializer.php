<?php

class KontorolICalSerializer extends KontorolSerializer
{
	private $calendar;
	
	public function __construct()
	{
		$this->calendar = new kSchedulingICalCalendar();
	}
	/**
	 * {@inheritDoc}
	 * @see KontorolSerializer::setHttpHeaders()
	 */
	public function setHttpHeaders()
	{
		header("Content-Type: text/calendar; charset=UTF-8");		
	}

	/**
	 * {@inheritDoc}
	 * @see KontorolSerializer::getHeader()
	 */
	public function getHeader() 
	{
		return $this->calendar->begin();
	}


	/**
	 * {@inheritDoc}
	 * @see KontorolSerializer::serialize()
	 */
	public function serialize($object)
	{
		if($object instanceof KontorolScheduleEvent)
		{
			$event = kSchedulingICalEvent::fromObject($object);
			return $event->write();
		}
		elseif($object instanceof KontorolScheduleEventArray)
		{
			$ret = '';
			foreach($object as $item)
			{
				$ret .= $this->serialize($item);
			}
			return $ret;
		}
		elseif($object instanceof KontorolScheduleEventListResponse)
		{
			$ret = $this->serialize($object->objects);
			$ret .= $this->calendar->writeField('X-KONTOROL-TOTAL-COUNT', $object->totalCount);
			return $ret;
		}
		elseif($object instanceof KontorolAPIException)
		{
			$ret = $this->calendar->writeField('BEGIN', 'VERROR');
			$ret .= $this->calendar->writeField('X-KONTOROL-CODE', $object->getCode());
			$ret .= $this->calendar->writeField('X-KONTOROL-MESSAGE', $object->getMessage());
			$ret .= $this->calendar->writeField('X-KONTOROL-ARGUMENTS', implode(';', $object->getArgs()));
			$ret .= $this->calendar->writeField('END', 'VERROR');
			return $ret;
		}
		else
		{
			$ret = $this->calendar->writeField('BEGIN', get_class($object));
			$ret .= $this->calendar->writeField('END', get_class($object));
			
			return $ret;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see KontorolSerializer::getFooter()
	 */
	public function getFooter($execTime = null)
	{
		if($execTime)
			$this->calendar->writeField('x-kontorol-execution-time', $execTime);
		
		return $this->calendar->end();
	}
}

<?php
/**
 * @package plugins.schedule
 * @subpackage api.objects
 */
class KontorolRecordScheduleEvent extends KontorolEntryScheduleEvent
{
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject($object_to_fill, $props_to_skip)
	 */
	public function toObject($sourceObject = null, $propertiesToSkip = array())
	{
		if(is_null($sourceObject))
		{
			$sourceObject = new RecordScheduleEvent();
		}
		
		return parent::toObject($sourceObject, $propertiesToSkip);
	}
	
	/**
	 * {@inheritDoc}
	 * @see KontorolScheduleEvent::getScheduleEventType()
	 */
	public function getScheduleEventType()
	{
		return ScheduleEventType::RECORD;
	}
}

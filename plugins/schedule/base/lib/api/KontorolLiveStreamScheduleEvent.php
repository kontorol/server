<?php
/**
 * @package plugins.schedule
 * @subpackage api.objects
 */
class KontorolLiveStreamScheduleEvent extends KontorolEntryScheduleEvent
{
	/**
	 * Defines the expected audience.
	 * @var int
	 * @minValue 0
	 */
	public $projectedAudience;

	/**
	 * The entry ID of the source entry (for simulive)
	 * @var string
	 */
	public $sourceEntryId;

	/**
	 * The time relative time before the startTime considered as preStart time
	 * @var int
	 */
	public $preStartTime;

	/**
	 * The time relative time before the endTime considered as postEnd time
	 * @var int
	 */
	public $postEndTime;

	/* (non-PHPdoc)
	 * @see KontorolObject::toObject($object_to_fill, $props_to_skip)
	 */
	public function toObject($sourceObject = null, $propertiesToSkip = array())
	{
		if(is_null($sourceObject))
		{
			$sourceObject = new LiveStreamScheduleEvent();
		}
		
		return parent::toObject($sourceObject, $propertiesToSkip);
	}

	/*
	 * Mapping between the field on this object (on the left) and the setter/getter on the entry object (on the right)
	 */
	private static $map_between_objects = array
	(
		'projectedAudience',
		'sourceEntryId',
		'preStartTime',
		'postEndTime'
	);

	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}

	/**
	 * {@inheritDoc}
	 * @see KontorolScheduleEvent::getScheduleEventType()
	 */
	public function getScheduleEventType()
	{
		return ScheduleEventType::LIVE_STREAM;
	}

	/**
	 * @throws KontorolAPIException
	 */
	public function validateForInsert($propertiesToSkip = array())
	{
		$this->validateLiveStreamEventFields();
		parent::validateForInsert($propertiesToSkip);
	}

	/**
	 * @throws KontorolAPIException
	 */
	public function validateForUpdate($sourceObject, $propertiesToSkip = array())
	{
		$this->validateLiveStreamEventFields();
		parent::validateForUpdate($sourceObject, $propertiesToSkip = array());
	}

	/**
	 * @throws KontorolAPIException
	 */
	protected function validateLiveStreamEventFields()
	{
		if (isset($this->sourceEntryId) && !entryPeer::retrieveByPK($this->sourceEntryId))
		{
			throw new KontorolAPIException(KontorolErrors::ENTRY_ID_NOT_FOUND, $this->sourceEntryId);
		}
		if (isset($this->preStartTime) && $this->preStartTime < 0)
		{
			throw new KontorolAPIException(APIErrors::INVALID_FIELD_VALUE, 'preStartTime');
		}
		if (isset($this->postEndTime) && $this->postEndTime < 0)
		{
		    throw new KontorolAPIException(APIErrors::INVALID_FIELD_VALUE, 'postEndTime');
		}
	}
}

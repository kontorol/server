<?php
/**
 * @package plugins.schedule
 * @subpackage api.filters
 */
class KontorolBlackoutScheduleEventFilter extends KontorolRecordScheduleEventBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolScheduleEventFilter::getListResponseType()
	 */
	protected function getListResponseType()
	{
		return ScheduleEventType::BLACKOUT;
	}
}

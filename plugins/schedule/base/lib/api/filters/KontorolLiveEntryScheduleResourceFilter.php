<?php
/**
 * @package plugins.schedule
 * @subpackage api.filters
 */
class KontorolLiveEntryScheduleResourceFilter extends KontorolLiveEntryScheduleResourceBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolScheduleResourceFilter::getListResponseType()
	 */
	protected function getListResponseType()
	{
		return ScheduleResourceType::LIVE_ENTRY;
	}
}

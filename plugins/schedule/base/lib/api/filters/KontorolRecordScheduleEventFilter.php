<?php
/**
 * @package plugins.schedule
 * @subpackage api.filters
 */
class KontorolRecordScheduleEventFilter extends KontorolRecordScheduleEventBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolScheduleEventFilter::getListResponseType()
	 */
	protected function getListResponseType()
	{
		return ScheduleEventType::RECORD;
	}
}

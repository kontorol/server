<?php
/**
 * @package plugins.schedule
 * @subpackage api.filters
 */
class KontorolCameraScheduleResourceFilter extends KontorolCameraScheduleResourceBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolScheduleResourceFilter::getListResponseType()
	 */
	protected function getListResponseType()
	{
		return ScheduleResourceType::CAMERA;
	}
}

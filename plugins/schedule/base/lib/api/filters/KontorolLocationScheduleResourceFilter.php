<?php
/**
 * @package plugins.schedule
 * @subpackage api.filters
 */
class KontorolLocationScheduleResourceFilter extends KontorolLocationScheduleResourceBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolScheduleResourceFilter::getListResponseType()
	 */
	protected function getListResponseType()
	{
		return ScheduleResourceType::LOCATION;
	}
}

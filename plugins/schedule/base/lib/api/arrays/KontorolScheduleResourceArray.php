<?php
/**
 * @package plugins.schedule
 * @subpackage api.objects
 */
class KontorolScheduleResourceArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolScheduleResourceArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
			$newArr[] = KontorolScheduleResource::getInstance($obj, $responseProfile);
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolScheduleResource");
	}
}

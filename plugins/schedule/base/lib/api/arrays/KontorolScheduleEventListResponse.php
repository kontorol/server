<?php
/**
 * @package plugins.schedule
 * @subpackage api.objects
 */
class KontorolScheduleEventListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolScheduleEventArray
	 * @readonly
	 */
	public $objects;
}

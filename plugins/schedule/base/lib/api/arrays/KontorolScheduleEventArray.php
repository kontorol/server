<?php
/**
 * @package plugins.schedule
 * @subpackage api.objects
 */
class KontorolScheduleEventArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolScheduleEventArray();
		if ($arr == null)
			return $newArr;

		// preload all parents in order to have them in the instance pool
		$parentIds = array();
		foreach ($arr as $obj)
		{
			/* @var $obj ScheduleEvent */
			if($obj->getParentId())
			{
				$parentIds[$obj->getParentId()] = true;
			} 
		}
		if(count($parentIds))
		{
			ScheduleEventPeer::retrieveByPKs(array_keys($parentIds));
		}
		
		foreach ($arr as $obj)
		{
			$newArr[] = KontorolScheduleEvent::getInstance($obj, $responseProfile);
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolScheduleEvent");
	}
}

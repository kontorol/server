<?php
/**
 * @package plugins.schedule
 * @subpackage api.objects
 */
class KontorolScheduleResourceListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolScheduleResourceArray
	 * @readonly
	 */
	public $objects;
}

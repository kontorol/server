<?php
/**
 * @package plugins.schedule
 * @subpackage api.objects
 */
class KontorolScheduleEventRecurrenceArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolScheduleEventRecurrenceArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
    		$nObj = new KontorolScheduleEventRecurrence();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolScheduleEventRecurrence");
	}
}

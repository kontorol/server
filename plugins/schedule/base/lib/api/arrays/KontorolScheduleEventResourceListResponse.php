<?php
/**
 * @package plugins.schedule
 * @subpackage api.objects
 */
class KontorolScheduleEventResourceListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolScheduleEventResourceArray
	 * @readonly
	 */
	public $objects;
}

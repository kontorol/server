<?php
/**
 * @package plugins.scheduleDropFolder
 */
class DropFolderSchedulePlugin extends KontorolPlugin implements IKontorolEnumerator, IKontorolObjectLoader, IKontorolEventConsumers, IKontorolBulkUpload, IKontorolPending
{
	const PLUGIN_NAME = 'scheduleDropFolder';
	const DROP_FOLDER_EVENTS_CONSUMER = 'kDropFolderICalEventsConsumer';
	
	/**
	 * Returns the plugin name
	 */
	public static function getPluginName()
	{
		return self::PLUGIN_NAME;
	}
	
	/*
	 * (non-PHPdoc)
	 * @see IKontorolPending::dependsOn()
	 */
	public static function dependsOn()
	{
		$bulkUploadDependency = new KontorolDependency(BulkUploadSchedulePlugin::PLUGIN_NAME);
		$dropFolderDependency = new KontorolDependency(DropFolderPlugin::PLUGIN_NAME);
		
		return array($bulkUploadDependency, $dropFolderDependency);
	}
	
	/**
	 *
	 * @return array<string> list of enum classes names that extend the base enum name
	 */
	public static function getEnums($baseEnumName = null)
	{
		if(is_null($baseEnumName))
			return array('DropFolderFileHandlerScheduleType', 'DropFolderScheduleType');

		if($baseEnumName == 'DropFolderFileHandlerType')
			return array('DropFolderFileHandlerScheduleType');

		if($baseEnumName == 'BulkUploadType')
			return array('DropFolderScheduleType');
		
		return array();
	}

	/**
	 * {@inheritDoc}
	 * @see IKontorolObjectLoader::loadObject()
	 */
	public static function loadObject($baseClass, $enumValue, array $constructorArgs = null)
	{
		if($baseClass == 'Kontorol_Client_DropFolder_Type_DropFolderFileHandlerConfig' && $enumValue == Kontorol_Client_DropFolder_Enum_DropFolderFileHandlerType::ICAL)
		{
			return new Kontorol_Client_ScheduleDropFolder_Type_DropFolderICalBulkUploadFileHandlerConfig();
		}
		
		if($baseClass == 'Form_BaseFileHandlerConfig' && $enumValue == Kontorol_Client_DropFolder_Enum_DropFolderFileHandlerType::ICAL)
		{
			return new Form_ICalFileHandlerConfig();
		}
		
		if($baseClass == 'KontorolDropFolderFileHandlerConfig' && $enumValue == self::getFileHandlerTypeCoreValue(DropFolderFileHandlerScheduleType::ICAL))
		{
			return new KontorolDropFolderICalBulkUploadFileHandlerConfig();
		}

		if($baseClass == 'kBulkUploadJobData' && $enumValue == self::getBulkUploadTypeCoreValue(DropFolderScheduleType::DROP_FOLDER_ICAL))
		{
			return new kBulkUploadICalJobData();
		}
		
		if($baseClass == 'KontorolBulkUploadJobData' && $enumValue == self::getBulkUploadTypeCoreValue(DropFolderScheduleType::DROP_FOLDER_ICAL))
		{
			return new KontorolBulkUploadICalJobData();
		}
				
		if($baseClass == 'KBulkUploadEngine' && class_exists('KontorolClient'))
		{	
			list($job) = $constructorArgs;
			if($enumValue == KontorolBulkUploadType::DROP_FOLDER_ICAL)
			{
				return new BulkUploadEngineDropFolderICal($job);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * @see IKontorolObjectLoader::getObjectClass()
	 */
	public static function getObjectClass($baseClass, $enumValue)
	{
		return null;
	}

	/**
	 * {@inheritDoc}
	 * @see IKontorolEventConsumers::getEventConsumers()
	 */
	public static function getEventConsumers()
	{
		return array(
			self::DROP_FOLDER_EVENTS_CONSUMER,
		);
	}
	
	/**
	 * Returns the correct file extension for bulk upload type
	 *
	 * @param int $enumValue code API value
	 */
	public static function getFileExtension($enumValue)
	{
		if($enumValue == self::getBulkUploadTypeCoreValue(DropFolderScheduleType::DROP_FOLDER_ICAL))
			return 'ics';
	}
	
	/**
	 * Returns the log file for bulk upload job
	 *
	 * @param BatchJob $batchJob bulk upload batchjob
	 */
	public static function writeBulkUploadLogFile($batchJob)
	{
		if($batchJob->getJobSubType() != self::getBulkUploadTypeCoreValue(DropFolderScheduleType::DROP_FOLDER_ICAL))
		{
			return;
		}
		
		BulkUploadSchedulePlugin::writeICalBulkUploadLogFile($batchJob);
	}
	
	/**
	 *
	 * @return int id of dynamic enum in the DB.
	 */
	public static function getFileHandlerTypeCoreValue($valueName)
	{
		$value = self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
		return kPluginableEnumsManager::apiToCore('DropFolderFileHandlerType', $value);
	}
	
	/**
	 *
	 * @return int id of dynamic enum in the DB.
	 */
	public static function getBulkUploadTypeCoreValue($valueName)
	{
		$value = self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
		return kPluginableEnumsManager::apiToCore('BulkUploadType', $value);
	}
	
	/**
	 *
	 * @return string external API value of dynamic enum.
	 */
	public static function getApiValue($valueName)
	{
		return self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
	}
}

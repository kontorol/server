<?php
/**
 * Bulk upload service is used to upload & manage events in bulk
 *
 * @service scheduleBulk
 * @package plugins.scheduleBulkUpload
 * @subpackage services
 */
class ScheduleBulkService extends KontorolBaseService
{
	/**
	 * Add new bulk upload batch job
	 * 
	 * @action addScheduleEvents
	 * @actionAlias schedule_scheduleEvent.addFromBulkUpload
	 * @param file $fileData
	 * @param KontorolBulkUploadScheduleEventJobData $bulkUploadData
	 * @return KontorolBulkUpload
	 */
	function addScheduleEventsAction($fileData, KontorolBulkUploadScheduleEventJobData $bulkUploadData = null)
	{
		$bulkUploadObjectCoreType = BulkUploadSchedulePlugin::getBulkUploadObjectTypeCoreValue(BulkUploadObjectScheduleType::SCHEDULE_EVENT);

		if (!$bulkUploadData)
		{
			throw new KontorolAPIException(KontorolErrors::MISSING_MANDATORY_PARAMETER, 'bulkUploadData');
		}

		if(!$bulkUploadData->fileName)
			$bulkUploadData->fileName = $fileData["name"];
		
		$dbBulkUploadJobData = $bulkUploadData->toInsertableObject();
		/* @var $dbBulkUploadJobData kBulkUploadJobData */
		$bulkUploadCoreType = kPluginableEnumsManager::apiToCore("BulkUploadType", $bulkUploadData->type);
		
		$dbBulkUploadJobData->setBulkUploadObjectType($bulkUploadObjectCoreType);
		$dbBulkUploadJobData->setUserId($this->getKuser()->getPuserId());
		$dbBulkUploadJobData->setFilePath($fileData["tmp_name"]);
		
		$dbJob = kJobsManager::addBulkUploadJob($this->getPartner(), $dbBulkUploadJobData, $bulkUploadCoreType);
		$dbJobLog = BatchJobLogPeer::retrieveByBatchJobId($dbJob->getId());
		if(!$dbJobLog)
			return null;
			
		$bulkUpload = new KontorolBulkUpload();
		$bulkUpload->fromObject($dbJobLog, $this->getResponseProfile());
		
		return $bulkUpload;
	}
	
	/**
	 * Add new bulk upload batch job
	 * 
	 * @action addScheduleResources
	 * @actionAlias schedule_scheduleResource.addFromBulkUpload
	 * @param file $fileData
	 * @param KontorolBulkUploadCsvJobData $bulkUploadData
	 * @return KontorolBulkUpload
	 */
	function addScheduleResourcesAction($fileData, KontorolBulkUploadCsvJobData $bulkUploadData = null)
	{	    
	    if (!$bulkUploadData)
	    {
	       $bulkUploadData = KontorolPluginManager::loadObject('KontorolBulkUploadJobData', null);
	    }
	    
		$bulkUploadObjectCoreType = BulkUploadSchedulePlugin::getBulkUploadObjectTypeCoreValue(BulkUploadObjectScheduleType::SCHEDULE_RESOURCE);
		
		if(!$bulkUploadData->fileName)
			$bulkUploadData->fileName = $fileData["name"];
		
		$dbBulkUploadJobData = $bulkUploadData->toInsertableObject();
		/* @var $dbBulkUploadJobData kBulkUploadJobData */

		$bulkUploadCoreType = kPluginableEnumsManager::apiToCore("BulkUploadType", $bulkUploadData->type);
		
		$dbBulkUploadJobData->setBulkUploadObjectType($bulkUploadObjectCoreType);
		$dbBulkUploadJobData->setUserId($this->getKuser()->getPuserId());
		$dbBulkUploadJobData->setFilePath($fileData["tmp_name"]);
		
		$dbJob = kJobsManager::addBulkUploadJob($this->getPartner(), $dbBulkUploadJobData, $bulkUploadCoreType);
		$dbJobLog = BatchJobLogPeer::retrieveByBatchJobId($dbJob->getId());
		if(!$dbJobLog)
			return null;
			
		$bulkUpload = new KontorolBulkUpload();
		$bulkUpload->fromObject($dbJobLog, $this->getResponseProfile());
		
		return $bulkUpload;
	}
}

<?php
/**
 * @package plugins.captureSpace
 * @subpackage api.objects
 */
class KontorolCaptureSpaceUpdateResponseInfo extends KontorolObject {
    /**
     * @var string
     */
    public $url;

    /**
     * @var KontorolCaptureSpaceUpdateResponseInfoHash
     */
    public $hash;
}

<?php
/**
 * @package plugins.captureSpace
 * @subpackage api.objects
 */
class KontorolCaptureSpaceUpdateResponse extends KontorolObject {
    /**
     * @var KontorolCaptureSpaceUpdateResponseInfo
     */
    public $info;
}

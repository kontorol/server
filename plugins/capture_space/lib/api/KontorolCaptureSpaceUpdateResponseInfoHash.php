<?php
/**
 * @package plugins.captureSpace
 * @subpackage api.objects
 */
class KontorolCaptureSpaceUpdateResponseInfoHash extends KontorolObject {
    /**
     * @var KontorolCaptureSpaceHashAlgorithm
     */
    public $algorithm;

    /**
     * @var string
     */
    public $value;
}

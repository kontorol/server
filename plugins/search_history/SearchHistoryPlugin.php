<?php
/**
 * @package plugins.searchHistory
 */
class SearchHistoryPlugin extends KontorolPlugin implements IKontorolPending, IKontorolServices, IKontorolEventConsumers
{

    const PLUGIN_NAME = 'searchHistory';
    const SEARCH_HISTORY_MANAGER = 'kESearchHistoryManager';

    /**
     * @return string the name of the plugin
     */
    public static function getPluginName()
    {
        return self::PLUGIN_NAME;
    }

    /**
     * @return array
     */
    public static function getEventConsumers()
    {
        return array(
            self::SEARCH_HISTORY_MANAGER,
        );
    }

    /* (non-PHPdoc)
    * @see IKontorolPending::dependsOn()
    */
    public static function dependsOn()
    {
        $rabbitMqDependency = new KontorolDependency(RabbitMQPlugin::getPluginName());
        $elasticSearchDependency = new KontorolDependency(ElasticSearchPlugin::getPluginName());
        return array($rabbitMqDependency, $elasticSearchDependency);
    }

    public static function getServicesMap()
    {
        $map = array(
            'SearchHistory' => 'ESearchHistoryService',
        );
        return $map;
    }

}

<?php
/**
 * @service searchHistory
 * @package plugins.searchHistory
 * @subpackage api.services
 */
class ESearchHistoryService extends KontorolBaseService
{

    /**
     * @action list
     * @param KontorolESearchHistoryFilter|null $filter
     * @return KontorolESearchHistoryListResponse
     * @throws KontorolAPIException
     */
    public function listAction(KontorolESearchHistoryFilter $filter = null)
    {
        if (!$filter)
            $filter = new KontorolESearchHistoryFilter();

        try
        {
            $response = $filter->getListResponse();
        }
        catch (kESearchHistoryException $e)
        {
            $this->handleSearchHistoryException($e);
        }
        return $response;
    }

    /**
     * @action delete
     * @param string $searchTerm
     * @throws KontorolAPIException
     */
    public function deleteAction($searchTerm)
    {
        if (is_null($searchTerm) || $searchTerm == '')
        {
            throw new KontorolAPIException(KontorolESearchHistoryErrors::EMPTY_DELETE_SEARCH_TERM_NOT_ALLOWED);
        }

        try
        {
            $historyClient = new kESearchHistoryElasticClient();
            $historyClient->deleteSearchTermForUser($searchTerm);
        }
        catch (kESearchHistoryException $e)
        {
            $this->handleSearchHistoryException($e);
        }
    }

    private function handleSearchHistoryException($exception)
    {
        $code = $exception->getCode();
        $data = $exception->getData();
        switch ($code)
        {
            case kESearchHistoryException::INVALID_USER_ID:
                throw new KontorolAPIException(KontorolESearchHistoryErrors::INVALID_USER_ID);

            default:
                throw new KontorolAPIException(KontorolESearchHistoryErrors::INTERNAL_SERVERL_ERROR);
        }
    }

}

<?php
/**
 * @package plugins.searchHistory
 * @subpackage api.objects
 */
class KontorolESearchHistoryArray extends KontorolTypedArray
{

    public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
    {
        $newArr = new KontorolESearchHistoryArray();
        if ($arr == null)
            return $newArr;

        foreach ($arr as $obj)
        {
            $nObj = new KontorolESearchHistory();
            $nObj->fromObject($obj);
            $newArr[] = $nObj;
        }

        return $newArr;
    }

    public function __construct()
    {
        parent::__construct("KontorolESearchHistory");
    }

}

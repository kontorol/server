<?php
/**
 * @package plugins.searchHistory
 * @subpackage api.objects
 */
class KontorolESearchHistoryListResponse extends KontorolListResponse
{
    /**
     * @var KontorolESearchHistoryArray
     * @readonly
     */
    public $objects;
}

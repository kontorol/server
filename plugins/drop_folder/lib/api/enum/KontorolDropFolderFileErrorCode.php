<?php
/**
 * @package plugins.dropFolder
 * @subpackage api.enum
 */
class KontorolDropFolderFileErrorCode extends KontorolDynamicEnum implements DropFolderFileErrorCode
{
	// see DropFolderFileErrorCode interface
	
	public static function getEnumClass()
	{
		return 'DropFolderFileErrorCode';
	}
}

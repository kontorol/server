<?php
/**
 * @package plugins.dropFolder
 * @subpackage api.enum
 */
class KontorolDropFolderFileHandlerType extends KontorolDynamicEnum implements DropFolderFileHandlerType
{
	public static function getEnumClass()
	{
		return 'DropFolderFileHandlerType';
	}
}

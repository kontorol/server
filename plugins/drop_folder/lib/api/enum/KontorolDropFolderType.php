<?php
/**
 * @package plugins.dropFolder
 * @subpackage api.enum
 */
class KontorolDropFolderType extends KontorolDynamicEnum implements DropFolderType
{
	public static function getEnumClass()
	{
		return 'DropFolderType';
	}
}

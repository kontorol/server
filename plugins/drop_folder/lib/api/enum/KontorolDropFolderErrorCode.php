<?php
/**
 * @package plugins.dropFolder
 * @subpackage api.enum
 */
class KontorolDropFolderErrorCode extends KontorolDynamicEnum implements DropFolderErrorCode
{
	// see DropFolderErrorCode interface
	
	public static function getEnumClass()
	{
		return 'DropFolderErrorCode';
	}
}

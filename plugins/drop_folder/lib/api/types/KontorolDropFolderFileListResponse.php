<?php
/**
 * @package plugins.dropFolder
 * @subpackage api.objects
 */
class KontorolDropFolderFileListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolDropFolderFileArray
	 * @readonly
	 */
	public $objects;
}

<?php
/**
 * @package plugins.dropFolder
 * @subpackage api.objects
 */
class KontorolDropFolderListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolDropFolderArray
	 * @readonly
	 */
	public $objects;
}

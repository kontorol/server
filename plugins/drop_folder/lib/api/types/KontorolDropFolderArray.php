<?php
/**
 * @package plugins.dropFolder
 * @subpackage api.objects
 */
class KontorolDropFolderArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolDropFolderArray();
		foreach ( $arr as $obj )
		{
		    $nObj = KontorolDropFolder::getInstanceByType($obj->getType());
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
		 
	}
	
	public function __construct( )
	{
		return parent::__construct ( 'KontorolDropFolder' );
	}
}

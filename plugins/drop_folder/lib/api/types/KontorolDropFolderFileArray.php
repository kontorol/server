<?php
/**
 * @package plugins.dropFolder
 * @subpackage api.objects
 */
class KontorolDropFolderFileArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolDropFolderFileArray();
		foreach ( $arr as $obj )
		{
			$nObj = KontorolDropFolderFile::getInstanceByType($obj->getType());
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
		 
	}
	
	public function __construct( )
	{
		return parent::__construct ( 'KontorolDropFolderFile' );
	}
}

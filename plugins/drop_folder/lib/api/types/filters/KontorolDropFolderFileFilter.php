<?php
/**
 * @package plugins.dropFolder
 * @subpackage api.filters
 */
class KontorolDropFolderFileFilter extends KontorolDropFolderFileBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new DropFolderFileFilter();
	}
	
	/**
	 * @param KontorolFilterPager $pager
	 * @param KontorolDetachedResponseProfile $responseProfile
	 * @return KontorolListResponse
	 */
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$dropFolderFileFilter = $this->toObject();
		
		$c = new Criteria();
		$dropFolderFileFilter->attachToCriteria($c);
		$pager->attachToCriteria($c);
		$list = DropFolderFilePeer::doSelect($c);
		
		$totalCount = 0;
		$resultCount = count($list);
		if (($pager->pageIndex == 1 || $resultCount) && $resultCount < $pager->pageSize)
		{
			$totalCount = ($pager->pageIndex - 1) * $pager->pageSize + $resultCount;
		}
		else
		{
			KontorolFilterPager::detachFromCriteria($c);
			$totalCount = DropFolderFilePeer::doCount($c);
		}
		
		$response = new KontorolDropFolderFileListResponse();
		$response->objects = KontorolDropFolderFileArray::fromDbArray($list, $responseProfile);
		$response->totalCount = $totalCount;
		return $response;
	}
}

<?php
/**
 * @package plugins.dropFolder
 * @subpackage api.filters
 */
class KontorolDropFolderFilter extends KontorolDropFolderBaseFilter
{
	/**
	 * @var KontorolNullableBoolean
	 */
	public $currentDc;

	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new DropFolderFilter();
	}
	
	/* (non-PHPdoc)
	 * @see KontorolFilter::toObject()
	 */
	public function toObject ( $object_to_fill = null, $props_to_skip = array() )
	{
		if(!$this->isNull('currentDc'))
			$this->dcEqual = kDataCenterMgr::getCurrentDcId();
			
		return parent::toObject($object_to_fill, $props_to_skip);		
	}	
}

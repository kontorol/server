<?php
/**
 * 
 */
abstract class KDropFolderEngine implements IKontorolLogger
{
	protected $dropFolder;
	
	protected $dropFolderPlugin;
	
	protected $dropFolderFileService;

	private $maximumExecutionTime = null;
	
	public function __construct ()
	{
		$this->dropFolderPlugin = KontorolDropFolderClientPlugin::get(KBatchBase::$kClient);
		$this->dropFolderFileService = $this->dropFolderPlugin->dropFolderFile;
	}
	
	public static function getInstance ($dropFolderType)
	{
		switch ($dropFolderType) {
			case KontorolDropFolderType::FTP:
			case KontorolDropFolderType::SFTP:
			case KontorolDropFolderType::LOCAL:
				return new KDropFolderFileTransferEngine ();
				break;
			
			default:
				return KontorolPluginManager::loadObject('KDropFolderEngine', $dropFolderType);
				break;
		}
	}
	
	abstract public function watchFolder (KontorolDropFolder $dropFolder);
	
	abstract public function processFolder (KontorolBatchJob $job, KontorolDropFolderContentProcessorJobData $data);

	/**
	 * Load all the files from the database that their status is not PURGED, PARSED or DETECTED
	 * @param KontorolFilterPager $pager
	 * @return array
	 */
	protected function loadDropFolderFilesByPage($pager)
	{
		$dropFolderFiles =null;

		$dropFolderFileFilter = new KontorolDropFolderFileFilter();
		$dropFolderFileFilter->dropFolderIdEqual = $this->dropFolder->id;
		$dropFolderFileFilter->statusNotIn = KontorolDropFolderFileStatus::PARSED.','.KontorolDropFolderFileStatus::DETECTED;
		$dropFolderFileFilter->orderBy = KontorolDropFolderFileOrderBy::CREATED_AT_ASC;

		$dropFolderFiles = $this->dropFolderFileService->listAction($dropFolderFileFilter, $pager);
		return $dropFolderFiles->objects;
	}

	/**
	 * Load all the files from the database that their status is not PURGED, PARSED or DETECTED
	 * @return array
	 */
	protected function loadDropFolderFiles()
	{
		$dropFolderFiles =null;

		$dropFolderFileFilter = new KontorolDropFolderFileFilter();
		$dropFolderFileFilter->dropFolderIdEqual = $this->dropFolder->id;
		$dropFolderFileFilter->statusNotIn = KontorolDropFolderFileStatus::PARSED.','.KontorolDropFolderFileStatus::DETECTED;
		$dropFolderFileFilter->orderBy = KontorolDropFolderFileOrderBy::CREATED_AT_ASC;

		$pager = new KontorolFilterPager();
		$pager->pageSize = 500;
		if(KBatchBase::$taskConfig && KBatchBase::$taskConfig->params->pageSize)
			$pager->pageSize = KBatchBase::$taskConfig->params->pageSize;

		return $this->loadDropFolderFilesMap($dropFolderFileFilter, $pager);
	}

	/**
	 * Load all the files from the database that their status is UPLOADING and updatedAt LessThan Or Equal $updatedAt
	 * @param $updatedAt time
	 * @return array
	 */
	protected function loadDropFolderUpLoadingFiles($updatedAt)
	{
		$dropFolderFiles =null;

		$dropFolderFileFilter = new KontorolDropFolderFileFilter();
		$dropFolderFileFilter->dropFolderIdEqual = $this->dropFolder->id;
		$dropFolderFileFilter->statusEqual = KontorolDropFolderFileStatus::UPLOADING;
		$dropFolderFileFilter->updatedAtLessThanOrEqual = $updatedAt;
		$dropFolderFileFilter->orderBy = KontorolDropFolderFileOrderBy::CREATED_AT_ASC;

		$pager = new KontorolFilterPager();
		$pager->pageSize = 500;
		if(KBatchBase::$taskConfig && KBatchBase::$taskConfig->params->pageSize)
			$pager->pageSize = KBatchBase::$taskConfig->params->pageSize;

		return $this->loadDropFolderFilesMap($dropFolderFileFilter, $pager);
	}

	/**
	 * @param $dropFolderFileFilter KontorolDropFolderFileFilter
	 * @param $pager KontorolFilterPager
	 * @return array
	 */
	protected function loadDropFolderFilesMap($dropFolderFileFilter, $pager)
	{
		$dropFolderFilesMap = array();
		$totalCount = 0;
		do
		{
			$pager->pageIndex++;
			$dropFolderFiles = $this->dropFolderFileService->listAction($dropFolderFileFilter, $pager);
			if (!$totalCount)
			{
				$totalCount = $dropFolderFiles->totalCount;
			}

			$dropFolderFiles = $dropFolderFiles->objects;
			foreach ($dropFolderFiles as $dropFolderFile)
			{
				$dropFolderFilesMap[$dropFolderFile->fileName] = $dropFolderFile;
			}

		} while (count($dropFolderFiles) >= $pager->pageSize);

		$mapCount = count($dropFolderFilesMap);
		KontorolLog::debug("Drop folder [" . $this->dropFolder->id . "] has [$totalCount] file");
		if ($totalCount != $mapCount)
		{
			KontorolLog::warning("Map is missing files - Drop folder [" . $this->dropFolder->id . "] has [$totalCount] file from list BUT has [$mapCount] files in map");
		}

		return $dropFolderFilesMap;
	}

	/**
	 * Update drop folder entity with error
	 * @param int $dropFolderFileId
	 * @param int $errorStatus
	 * @param int $errorCode
	 * @param string $errorMessage
	 * @param Exception $e
	 */
	protected function handleFileError($dropFolderFileId, $errorStatus, $errorCode, $errorMessage, Exception $e = null)
	{
		try 
		{
			if($e)
				KontorolLog::err('Error for drop folder file with id ['.$dropFolderFileId.'] - '.$e->getMessage());
			else
				KontorolLog::err('Error for drop folder file with id ['.$dropFolderFileId.'] - '.$errorMessage);
			
			$updateDropFolderFile = new KontorolDropFolderFile();
			$updateDropFolderFile->errorCode = $errorCode;
			$updateDropFolderFile->errorDescription = $errorMessage;
			$this->dropFolderFileService->update($dropFolderFileId, $updateDropFolderFile);
			return $this->dropFolderFileService->updateStatus($dropFolderFileId, $errorStatus);				
		}
		catch (KontorolException $e)
		{
			KontorolLog::err('Cannot set error details for drop folder file id ['.$dropFolderFileId.'] - '.$e->getMessage());
			return null;
		}
	}
	
	/**
	 * Mark file status as PURGED
	 * @param int $dropFolderFileId
	 */
	protected function handleFilePurged($dropFolderFileId)
	{
		try 
		{
			return $this->dropFolderFileService->updateStatus($dropFolderFileId, KontorolDropFolderFileStatus::PURGED);
		}
		catch(Exception $e)
		{
			$this->handleFileError($dropFolderFileId, KontorolDropFolderFileStatus::ERROR_HANDLING, KontorolDropFolderFileErrorCode::ERROR_UPDATE_FILE,
									DropFolderPlugin::ERROR_UPDATE_FILE_MESSAGE, $e);
			
			return null;
		}		
	}
	
	/**
	 * Retrieve all the relevant drop folder files according to the list of id's passed on the job data.
	 * Create resource object based on the conversion profile as an input to the ingestion API
	 * @param KontorolBatchJob $job
	 * @param KontorolDropFolderContentProcessorJobData $data
	 */
	protected function getIngestionResource(KontorolBatchJob $job, KontorolDropFolderContentProcessorJobData $data)
	{
		$filter = new KontorolDropFolderFileFilter();
		$filter->idIn = $data->dropFolderFileIds;
		$dropFolderFiles = $this->dropFolderFileService->listAction($filter); 
		
		$resource = null;
		if($dropFolderFiles->totalCount == 1 && is_null($dropFolderFiles->objects[0]->parsedFlavor)) //only source is ingested
		{
			$resource = new KontorolDropFolderFileResource();
			$resource->dropFolderFileId = $dropFolderFiles->objects[0]->id;			
		}
		else //ingest all the required flavors
		{			
			$fileToFlavorMap = array();
			foreach ($dropFolderFiles->objects as $dropFolderFile) 
			{
				$fileToFlavorMap[$dropFolderFile->parsedFlavor] = $dropFolderFile->id;			
			}
			
			$assetContainerArray = array();
		
			$assetParamsFilter = new KontorolConversionProfileAssetParamsFilter();
			$assetParamsFilter->conversionProfileIdEqual = $data->conversionProfileId;
			$assetParamsList = KBatchBase::$kClient->conversionProfileAssetParams->listAction($assetParamsFilter);
			foreach ($assetParamsList->objects as $assetParams)
			{
				if(array_key_exists($assetParams->systemName, $fileToFlavorMap))
				{
					$assetContainer = new KontorolAssetParamsResourceContainer();
					$assetContainer->assetParamsId = $assetParams->assetParamsId;
					$assetContainer->resource = new KontorolDropFolderFileResource();
					$assetContainer->resource->dropFolderFileId = $fileToFlavorMap[$assetParams->systemName];
					$assetContainerArray[] = $assetContainer;				
				}			
			}		
			$resource = new KontorolAssetsParamsResourceContainers();
			$resource->resources = $assetContainerArray;
		}
		return $resource;		
	}

	protected function createCategoryAssociations (KontorolDropFolder $folder, $userId, $entryId)
	{
		if ($folder->metadataProfileId && $folder->categoriesMetadataFieldName)
		{
			$filter = new KontorolMetadataFilter();
			$filter->metadataProfileIdEqual = $folder->metadataProfileId;
			$filter->objectIdEqual = $userId;
			$filter->metadataObjectTypeEqual = KontorolMetadataObjectType::USER;
			
			try
			{
				$metadataPlugin = KontorolMetadataClientPlugin::get(KBatchBase::$kClient);
				//Expect only one result
				$res = $metadataPlugin->metadata->listAction($filter, new KontorolFilterPager());
				
				if(!$res->objects || !count($res->objects))
					return;
				
				$metadataObj = $res->objects[0];
				$xmlElem = new SimpleXMLElement($metadataObj->xml);
				$categoriesXPathRes = $xmlElem->xpath($folder->categoriesMetadataFieldName);
				$categories = array();
				foreach ($categoriesXPathRes as $catXPath)
				{
					$categories[] = strval($catXPath);
				}
				
				$categoryFilter = new KontorolCategoryFilter();
				$categoryFilter->idIn = implode(',', $categories);
				$categoryListResponse = KBatchBase::$kClient->category->listAction ($categoryFilter, new KontorolFilterPager());
				if ($categoryListResponse->objects && count($categoryListResponse->objects))
				{
					if (!$folder->enforceEntitlement)
					{
						//easy
						$this->createCategoryEntriesNoEntitlement ($categoryListResponse->objects, $entryId);
					}
					else {
						//write your will
						$this->createCategoryEntriesWithEntitlement ($categoryListResponse->objects, $entryId, $userId);
					}
				}
			}
			catch (Exception $e)
			{
				KontorolLog::err('Error encountered. Code: ['. $e->getCode() . '] Message: [' . $e->getMessage() . ']');
			}
		}
	}

	private function createCategoryEntriesNoEntitlement (array $categoriesArr, $entryId)
	{
		KBatchBase::$kClient->startMultiRequest();
		foreach ($categoriesArr as $category)
		{
			$categoryEntry = new KontorolCategoryEntry();
			$categoryEntry->entryId = $entryId;
			$categoryEntry->categoryId = $category->id;
			KBatchBase::$kClient->categoryEntry->add($categoryEntry);
		}
		KBatchBase::$kClient->doMultiRequest();
	}
	
	private function createCategoryEntriesWithEntitlement (array $categoriesArr, $entryId, $userId)
	{
		$partnerInfo = KBatchBase::$kClient->partner->get(KBatchBase::$kClientConfig->partnerId);
		
		$clientConfig = new KontorolConfiguration($partnerInfo->id);
		$clientConfig->serviceUrl = KBatchBase::$kClient->getConfig()->serviceUrl;
		$clientConfig->setLogger($this);
		$client = new KontorolClient($clientConfig);
		foreach ($categoriesArr as $category)
		{
			/* @var $category KontorolCategory */
			$ks = $client->generateSessionV2($partnerInfo->adminSecret, $userId, KontorolSessionType::ADMIN, $partnerInfo->id, 86400, 'enableentitlement,privacycontext:'.$category->privacyContexts);
			$client->setKs($ks);
			$categoryEntry = new KontorolCategoryEntry();
			$categoryEntry->categoryId = $category->id;
			$categoryEntry->entryId = $entryId;
			try
			{
				$client->categoryEntry->add ($categoryEntry);
			}
			catch (Exception $e)
			{
				KontorolLog::err("Could not add entry $entryId to category {$category->id}. Exception thrown.");
			}
		}
	}
	
	function log($message)
	{
		KontorolLog::log($message);
	}
	
	public function setMaximumExecutionTime($maximumExecutionTime = null)
	{
		if (is_null($this->maximumExecutionTime))
			$this->maximumExecutionTime = $maximumExecutionTime;
	}

	public function getMaximumExecutionTime()
	{
		return $this->maximumExecutionTime;
	}
}

<?php

class KAsyncDropFolderContentProcessor extends KJobHandlerWorker
{
	/**
	 * @var KontorolDropFolderClientPlugin
	 */
	protected $dropFolderPlugin = null;
	
	/* (non-PHPdoc)
	 * @see KBatchBase::getType()
	 */
	public static function getType()
	{
		return KontorolBatchJobType::DROP_FOLDER_CONTENT_PROCESSOR;
	}
	
	/* (non-PHPdoc)
	 * @see KJobHandlerWorker::exec()
	 */
	protected function exec(KontorolBatchJob $job)
	{
		try 
		{
			return $this->process($job, $job->data);
		}
		catch(kTemporaryException $e)
		{
			$this->unimpersonate();
			if($e->getResetJobExecutionAttempts())
				throw $e;
			return $this->closeJob($job, KontorolBatchJobErrorTypes::RUNTIME, $e->getCode(), "Error: " . $e->getMessage(), KontorolBatchJobStatus::FAILED);
		}
		catch(KontorolClientException $e)
		{
			$this->unimpersonate();
			return $this->closeJob($job, KontorolBatchJobErrorTypes::KONTOROL_CLIENT, $e->getCode(), "Error: " . $e->getMessage(), KontorolBatchJobStatus::FAILED);
		}
	}

	protected function process(KontorolBatchJob $job, KontorolDropFolderContentProcessorJobData $data)
	{
		$job = $this->updateJob($job, "Start processing drop folder files [$data->dropFolderFileIds]", KontorolBatchJobStatus::QUEUED);
		$engine = KDropFolderEngine::getInstance($job->jobSubType);
		$engine->processFolder($job, $data);
		return $this->closeJob($job, null, null, null, KontorolBatchJobStatus::FINISHED);
	}
		
}

<?php

/**
 * DropFolderFile service lets you create and manage drop folder files
 * @service dropFolderFile
 * @package plugins.dropFolder
 * @subpackage api.services
 */
class DropFolderFileService extends KontorolBaseService
{
	const MYSQL_CODE_DUPLICATE_KEY = 23000;
	
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		
		if (!DropFolderPlugin::isAllowedPartner($this->getPartnerId()))
			throw new KontorolAPIException(KontorolErrors::FEATURE_FORBIDDEN, DropFolderPlugin::PLUGIN_NAME);
		
		$this->applyPartnerFilterForClass('DropFolder');
		$this->applyPartnerFilterForClass('DropFolderFile');
	}
		
	/**
	 * Allows you to add a new KontorolDropFolderFile object
	 * 
	 * @action add
	 * @param KontorolDropFolderFile $dropFolderFile
	 * @return KontorolDropFolderFile
	 * 
	 * @throws KontorolErrors::PROPERTY_VALIDATION_CANNOT_BE_NULL
	 * @throws KontorolDropFolderErrors::DROP_FOLDER_NOT_FOUND
	 */
	public function addAction(KontorolDropFolderFile $dropFolderFile)
	{
		return $this->newFileAddedOrDetected($dropFolderFile, DropFolderFileStatus::UPLOADING);
	}
	
	/**
	 * Retrieve a KontorolDropFolderFile object by ID
	 * 
	 * @action get
	 * @param int $dropFolderFileId 
	 * @return KontorolDropFolderFile
	 * 
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 */		
	public function getAction($dropFolderFileId)
	{
		$dbDropFolderFile = DropFolderFilePeer::retrieveByPK($dropFolderFileId);
		
		if (!$dbDropFolderFile) {
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $dropFolderFileId);
		}
			
		$dropFolderFile = KontorolDropFolderFile::getInstanceByType($dbDropFolderFile->getType());
		$dropFolderFile->fromObject($dbDropFolderFile, $this->getResponseProfile());
		
		return $dropFolderFile;
	}
	

	/**
	 * Update an existing KontorolDropFolderFile object
	 * 
	 * @action update
	 * @param int $dropFolderFileId
	 * @param KontorolDropFolderFile $dropFolderFile
	 * @return KontorolDropFolderFile
	 *
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 */	
	public function updateAction($dropFolderFileId, KontorolDropFolderFile $dropFolderFile)
	{
		$dbDropFolderFile = DropFolderFilePeer::retrieveByPK($dropFolderFileId);
		
		if (!$dbDropFolderFile) {
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $dropFolderFileId);
		}
		
		if (!is_null($dropFolderFile->fileSize)) {
			$dropFolderFile->validatePropertyMinValue('fileSize', 0);
		}
					
		$dbDropFolderFile = $dropFolderFile->toUpdatableObject($dbDropFolderFile);
		$dbDropFolderFile->save();
	
		$dropFolderFile = new KontorolDropFolderFile();
		$dropFolderFile->fromObject($dbDropFolderFile, $this->getResponseProfile());
		
		return $dropFolderFile;
	}
	

	/**
	 * Update status of KontorolDropFolderFile
	 * 
	 * @action updateStatus
	 * @param int $dropFolderFileId
	 * @param KontorolDropFolderFileStatus $status
	 * @return KontorolDropFolderFile
	 *
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 */	
	public function updateStatusAction($dropFolderFileId, $status)
	{
		$dbDropFolderFile = DropFolderFilePeer::retrieveByPK($dropFolderFileId);
		if (!$dbDropFolderFile)
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $dropFolderFileId);
			
		if ($status != KontorolDropFolderFileStatus::PURGED && $dbDropFolderFile->getStatus() == KontorolDropFolderFileStatus::DELETED)
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $dropFolderFileId);
		
		$dbDropFolderFile->setStatus($status);
		$dbDropFolderFile->save();
	
		$dropFolderFile = KontorolDropFolderFile::getInstanceByType($dbDropFolderFile->getType());
		$dropFolderFile->fromObject($dbDropFolderFile, $this->getResponseProfile());
		
		return $dropFolderFile;
	}

	/**
	 * Mark the KontorolDropFolderFile object as deleted
	 * 
	 * @action delete
	 * @param int $dropFolderFileId 
	 * @return KontorolDropFolderFile
	 *
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 */		
	public function deleteAction($dropFolderFileId)
	{
		$dbDropFolderFile = DropFolderFilePeer::retrieveByPK($dropFolderFileId);

		if (!$dbDropFolderFile) {
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $dropFolderFileId);
		}
		
		$dbDropFolderFile->setStatus(DropFolderFileStatus::DELETED);
		$dbDropFolderFile->save();
			
		$dropFolderFile = KontorolDropFolderFile::getInstanceByType($dbDropFolderFile->getType());
		$dropFolderFile->fromObject($dbDropFolderFile, $this->getResponseProfile());
		
		return $dropFolderFile;
	}
	
	/**
	 * List KontorolDropFolderFile objects
	 * 
	 * @action list
	 * @param KontorolDropFolderFileFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return KontorolDropFolderFileListResponse
	 */
	public function listAction(KontorolDropFolderFileFilter  $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
		{
			$filter = new KontorolDropFolderFileFilter();
		}
		
		if(!$pager)
		{
			$pager = new KontorolFilterPager();
		}
		
		return $filter->getListResponse($pager, $this->getResponseProfile());
	}
	
	
	/**
	 * Set the KontorolDropFolderFile status to ignore (KontorolDropFolderFileStatus::IGNORE)
	 * 
	 * @action ignore
	 * @param int $dropFolderFileId 
	 * @return KontorolDropFolderFile
	 *
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 */		
	public function ignoreAction($dropFolderFileId)
	{
		$dbDropFolderFile = DropFolderFilePeer::retrieveByPK($dropFolderFileId);
		
		if (!$dbDropFolderFile) {
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $dropFolderFileId);
		}

		$dbDropFolderFile->setStatus(DropFolderFileStatus::IGNORE);
		$dbDropFolderFile->save();
			
		$dropFolderFile = new KontorolDropFolderFile();
		$dropFolderFile->fromObject($dbDropFolderFile, $this->getResponseProfile());
		
		return $dropFolderFile;
	}
	
	private function newFileAddedOrDetected(KontorolDropFolderFile $dropFolderFile, $fileStatus)
	{
		// check for required parameters
		$dropFolderFile->validatePropertyNotNull('dropFolderId');
		$dropFolderFile->validatePropertyNotNull('fileName');
		$dropFolderFile->validatePropertyMinValue('fileSize', 0);
		
		// check that drop folder id exists in the system
		$dropFolder = DropFolderPeer::retrieveByPK($dropFolderFile->dropFolderId);
		if (!$dropFolder) {
			throw new KontorolAPIException(KontorolDropFolderErrors::DROP_FOLDER_NOT_FOUND, $dropFolderFile->dropFolderId);
		}
				
		// save in database
		$dropFolderFile->status = null;		
		$dbDropFolderFile = $dropFolderFile->toInsertableObject();
		/* @var $dbDropFolderFile DropFolderFile  */
		$dbDropFolderFile->setPartnerId($dropFolder->getPartnerId());
		$dbDropFolderFile->setStatus($fileStatus);
		$dbDropFolderFile->setType($dropFolder->getType());
		try 
		{
			$dbDropFolderFile->save();	
		}
		catch(PropelException $e)
		{
			if($e->getCause() && $e->getCause()->getCode() == self::MYSQL_CODE_DUPLICATE_KEY) //unique constraint
			{
				$existingDropFolderFile = DropFolderFilePeer::retrieveByDropFolderIdAndFileName($dropFolderFile->dropFolderId, $dropFolderFile->fileName);
				switch($existingDropFolderFile->getStatus())
				{					
					case DropFolderFileStatus::PARSED:
						KontorolLog::info('Exisiting file status is PARSED, updating status to ['.$fileStatus.']');
						$existingDropFolderFile = $dropFolderFile->toUpdatableObject($existingDropFolderFile);
						$existingDropFolderFile->setStatus($fileStatus);						
						$existingDropFolderFile->save();
						$dbDropFolderFile = $existingDropFolderFile;
						break;
					case DropFolderFileStatus::DETECTED:
						KontorolLog::info('Exisiting file status is DETECTED, updating status to ['.$fileStatus.']');
						$existingDropFolderFile = $dropFolderFile->toUpdatableObject($existingDropFolderFile);
						if($existingDropFolderFile->getStatus() != $fileStatus)
							$existingDropFolderFile->setStatus($fileStatus);
						$existingDropFolderFile->save();
						$dbDropFolderFile = $existingDropFolderFile;
						break;
					case DropFolderFileStatus::UPLOADING:
						if($fileStatus == DropFolderFileStatus::UPLOADING)
						{
							KontorolLog::log('Exisiting file status is UPLOADING, updating properties');
							$existingDropFolderFile = $dropFolderFile->toUpdatableObject($existingDropFolderFile);
							$existingDropFolderFile->save();
							$dbDropFolderFile = $existingDropFolderFile;
							break;							
						}
					default:
						KontorolLog::log('Setting current file to PURGED ['.$existingDropFolderFile->getId().']');
						$existingDropFolderFile->setStatus(DropFolderFileStatus::PURGED);				
						$existingDropFolderFile->save();
						
						$newDropFolderFile = $dbDropFolderFile->copy(true);
						if(	$existingDropFolderFile->getLeadDropFolderFileId() && 
							$existingDropFolderFile->getLeadDropFolderFileId() != $existingDropFolderFile->getId())
						{
							KontorolLog::info('Updating lead id ['.$existingDropFolderFile->getLeadDropFolderFileId().']');
							$newDropFolderFile->setLeadDropFolderFileId($existingDropFolderFile->getLeadDropFolderFileId());	
						}
						$newDropFolderFile->save();
						$dbDropFolderFile = $newDropFolderFile;
				}
			}
			else 
			{
				throw $e;
			}
		}	
		// return the saved object
		$dropFolderFile = KontorolDropFolderFile::getInstanceByType($dbDropFolderFile->getType());
		$dropFolderFile->fromObject($dbDropFolderFile, $this->getResponseProfile());
		return $dropFolderFile;		
		
	}
	
}

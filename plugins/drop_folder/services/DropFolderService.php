<?php

/**
 * DropFolder service lets you create and manage drop folders
 * @service dropFolder
 * @package plugins.dropFolder
 * @subpackage api.services
 */
class DropFolderService extends KontorolBaseService
{
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		
		if (!DropFolderPlugin::isAllowedPartner($this->getPartnerId()))
			throw new KontorolAPIException(KontorolErrors::FEATURE_FORBIDDEN, DropFolderPlugin::PLUGIN_NAME);
			
		$this->applyPartnerFilterForClass('DropFolder');
		$this->applyPartnerFilterForClass('DropFolderFile');
	}
		
	
	
	/**
	 * Allows you to add a new KontorolDropFolder object
	 * 
	 * @action add
	 * @param KontorolDropFolder $dropFolder
	 * @return KontorolDropFolder
	 * 
	 * @throws KontorolErrors::PROPERTY_VALIDATION_CANNOT_BE_NULL
	 * @throws KontorolErrors::INGESTION_PROFILE_ID_NOT_FOUND
	 * @throws KontorolDropFolderErrors::DROP_FOLDER_ALREADY_EXISTS
	 * @throws KontorolErrors::DATA_CENTER_ID_NOT_FOUND
	 */
	public function addAction(KontorolDropFolder $dropFolder)
	{
		// check for required parameters
		$dropFolder->validatePropertyNotNull('name');
		$dropFolder->validatePropertyNotNull('status');
		$dropFolder->validatePropertyNotNull('type');
		$dropFolder->validatePropertyNotNull('dc');
		$dropFolder->validatePropertyNotNull('path');
		$dropFolder->validatePropertyNotNull('partnerId');
		$dropFolder->validatePropertyMinValue('fileSizeCheckInterval', 0, true);
		$dropFolder->validatePropertyMinValue('autoFileDeleteDays', 0, true);
		$dropFolder->validatePropertyNotNull('fileHandlerType');
		$dropFolder->validatePropertyNotNull('fileHandlerConfig');
		
		// validate values
		
		if (is_null($dropFolder->fileSizeCheckInterval)) {
			$dropFolder->fileSizeCheckInterval = DropFolder::FILE_SIZE_CHECK_INTERVAL_DEFAULT_VALUE;
		}
		
		if (is_null($dropFolder->fileNamePatterns)) {
			$dropFolder->fileNamePatterns = DropFolder::FILE_NAME_PATTERNS_DEFAULT_VALUE;
		}
		
		if (!kDataCenterMgr::dcExists($dropFolder->dc)) {
			throw new KontorolAPIException(KontorolErrors::DATA_CENTER_ID_NOT_FOUND, $dropFolder->dc);
		}
		
		if (!PartnerPeer::retrieveByPK($dropFolder->partnerId)) {
			throw new KontorolAPIException(KontorolErrors::INVALID_PARTNER_ID, $dropFolder->partnerId);
		}
		
		if (!DropFolderPlugin::isAllowedPartner($dropFolder->partnerId))
		{
			throw new KontorolAPIException(KontorolErrors::PLUGIN_NOT_AVAILABLE_FOR_PARTNER, DropFolderPlugin::getPluginName(), $dropFolder->partnerId);
		}

		if($dropFolder->type == KontorolDropFolderType::LOCAL)
		{
			$existingDropFolder = DropFolderPeer::retrieveByPathDefaultFilter($dropFolder->path);
			if ($existingDropFolder) {
				throw new KontorolAPIException(KontorolDropFolderErrors::DROP_FOLDER_ALREADY_EXISTS, $dropFolder->path);
			}
		}
		
		if (!is_null($dropFolder->conversionProfileId)) {
			$conversionProfileDb = conversionProfile2Peer::retrieveByPK($dropFolder->conversionProfileId);
			if (!$conversionProfileDb) {
				throw new KontorolAPIException(KontorolErrors::INGESTION_PROFILE_ID_NOT_FOUND, $dropFolder->conversionProfileId);
			}
		}
		
		// save in database
		$dbDropFolder = $dropFolder->toInsertableObject();
		$dbDropFolder->save();
		
		// return the saved object
		$dropFolder = KontorolDropFolder::getInstanceByType($dbDropFolder->getType());
		$dropFolder->fromObject($dbDropFolder, $this->getResponseProfile());
		return $dropFolder;
		
	}
	
	/**
	 * Retrieve a KontorolDropFolder object by ID
	 * 
	 * @action get
	 * @param int $dropFolderId 
	 * @return KontorolDropFolder
	 * 
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 */		
	public function getAction($dropFolderId)
	{
		$dbDropFolder = DropFolderPeer::retrieveByPK($dropFolderId);
		
		if (!$dbDropFolder) {
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $dropFolderId);
		}
			
		$dropFolder = KontorolDropFolder::getInstanceByType($dbDropFolder->getType());
		$dropFolder->fromObject($dbDropFolder, $this->getResponseProfile());
		
		return $dropFolder;
	}
	

	/**
	 * Update an existing KontorolDropFolder object
	 * 
	 * @action update
	 * @param int $dropFolderId
	 * @param KontorolDropFolder $dropFolder
	 * @return KontorolDropFolder
	 *
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 * @throws KontorolErrors::INGESTION_PROFILE_ID_NOT_FOUND
	 * @throws KontorolErrors::DATA_CENTER_ID_NOT_FOUND
	 */	
	public function updateAction($dropFolderId, KontorolDropFolder $dropFolder)
	{
		$dbDropFolder = DropFolderPeer::retrieveByPK($dropFolderId);
		
		if (!$dbDropFolder) {
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $dropFolderId);
		}
		
		$dropFolder->validatePropertyMinValue('fileSizeCheckInterval', 0, true);
		$dropFolder->validatePropertyMinValue('autoFileDeleteDays', 0, true);
		
		if (!is_null($dropFolder->path) && $dropFolder->path != $dbDropFolder->getPath() && $dropFolder->type == KontorolDropFolderType::LOCAL)
		{
			$existingDropFolder = DropFolderPeer::retrieveByPathDefaultFilter($dropFolder->path);
			if ($existingDropFolder) {
				throw new KontorolAPIException(KontorolDropFolderErrors::DROP_FOLDER_ALREADY_EXISTS, $dropFolder->path);
			}
		}
		
		if (!is_null($dropFolder->dc)) {
			if (!kDataCenterMgr::dcExists($dropFolder->dc)) {
				throw new KontorolAPIException(KontorolErrors::DATA_CENTER_ID_NOT_FOUND, $dropFolder->dc);
			}
		}
		
		if (!is_null($dropFolder->conversionProfileId)) {
			$conversionProfileDb = conversionProfile2Peer::retrieveByPK($dropFolder->conversionProfileId);
			if (!$conversionProfileDb) {
				throw new KontorolAPIException(KontorolErrors::INGESTION_PROFILE_ID_NOT_FOUND, $dropFolder->conversionProfileId);
			}
		}

		$dbDropFolder = $dropFolder->toUpdatableObject($dbDropFolder);
		$dbDropFolder->save();
	
		$dropFolder = KontorolDropFolder::getInstanceByType($dbDropFolder->getType());
		$dropFolder->fromObject($dbDropFolder, $this->getResponseProfile());
		
		return $dropFolder;
	}

	/**
	 * Mark the KontorolDropFolder object as deleted
	 * 
	 * @action delete
	 * @param int $dropFolderId 
	 * @return KontorolDropFolder
	 *
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 */		
	public function deleteAction($dropFolderId)
	{
		$dbDropFolder = DropFolderPeer::retrieveByPK($dropFolderId);
		
		if (!$dbDropFolder) {
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $dropFolderId);
		}

		$dbDropFolder->setStatus(DropFolderStatus::DELETED);
		$dbDropFolder->save();
			
		$dropFolder = KontorolDropFolder::getInstanceByType($dbDropFolder->getType());
		$dropFolder->fromObject($dbDropFolder, $this->getResponseProfile());
		
		return $dropFolder;
	}
	
	/**
	 * List KontorolDropFolder objects
	 * 
	 * @action list
	 * @param KontorolDropFolderFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return KontorolDropFolderListResponse
	 */
	public function listAction(KontorolDropFolderFilter  $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
			$filter = new KontorolDropFolderFilter();
			
		$dropFolderFilter = $filter->toObject();

		$c = new Criteria();
		$dropFolderFilter->attachToCriteria($c);
		$count = DropFolderPeer::doCount($c);
		
		if (! $pager)
			$pager = new KontorolFilterPager ();
		$pager->attachToCriteria ( $c );
		$list = DropFolderPeer::doSelect($c);
		
		$response = new KontorolDropFolderListResponse();
		$response->objects = KontorolDropFolderArray::fromDbArray($list, $this->getResponseProfile());
		$response->totalCount = $count;
		
		return $response;
	}

	/**
	 * getExclusive KontorolDropFolder object
	 *
	 * @action getExclusiveDropFolder
	 * @param string $tag
	 * @param int $maxTime
	 * @return KontorolDropFolder
	 */
	public function getExclusiveDropFolderAction($tag, $maxTime)
	{
		$allocateDropFolder = kDropFolderAllocator::getDropFolder($tag, $maxTime);
		if ($allocateDropFolder && self::isValidForWatch($allocateDropFolder))
		{
			$dropFolder = KontorolDropFolder::getInstanceByType($allocateDropFolder->getType());
			$dropFolder->fromObject($allocateDropFolder, $this->getResponseProfile());
			return $dropFolder;
		}
	}
 	
	/**
	 * freeExclusive KontorolDropFolder object
	 *
	 * @action freeExclusiveDropFolder
	 * @param int $dropFolderId
	 * @param string $errorCode
	 * @param string $errorDescription
	 * @throws KontorolAPIException
	 * @return KontorolDropFolder
	 */
	public function freeExclusiveDropFolderAction($dropFolderId, $errorCode = null, $errorDescription = null)
	{
		kDropFolderAllocator::freeDropFolder($dropFolderId);

		$dbDropFolder = DropFolderPeer::retrieveByPK($dropFolderId);
		if (!$dbDropFolder)
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $dropFolderId);

		$dbDropFolder->setLastAccessedAt(time());
		$dbDropFolder->setErrorCode($errorCode);
		$dbDropFolder->setErrorDescription($errorDescription);
		$dbDropFolder->save();

		$dropFolder = KontorolDropFolder::getInstanceByType($dbDropFolder->getType());
		$dropFolder->fromObject($dbDropFolder, $this->getResponseProfile());

		return $dropFolder;
	}

	private static function isValidForWatch(DropFolder $dropFolder)
	{
		$partner = PartnerPeer::retrieveByPK($dropFolder->getPartnerId());
		if (!$partner || $partner->getStatus() != Partner::PARTNER_STATUS_ACTIVE
			|| !$partner->getPluginEnabled(DropFolderPlugin::PLUGIN_NAME))
			return false;

		return true;
	}

    /**
     * @action updateStatus
     * @param int $dropFolderId
     * @param KontorolDropFolderStatus $status
     */
    public function updateStatusAction($dropFolderId, $status)
    {
        $dbDropFolder = DropFolderPeer::retrieveByPK($dropFolderId);
        if (!$dbDropFolder)
            throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $dropFolderId);

        $dbDropFolder->setStatus($status);
        $dbDropFolder->save();
    }

    /**
     * @action updateBasicFields
     * @param int $dropFolderId
     * @param KontorolBasicFieldsDropFolder $dropFolder
     *
     * @return KontorolDropFolder
     */
    public function updateBasicFieldsAction($dropFolderId, KontorolBasicFieldsDropFolder $dropFolder)
    {
        $dbDropFolder = DropFolderPeer::retrieveByPK($dropFolderId);
        if (!$dbDropFolder)
            throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $dropFolderId);

        $dbDropFolder = $dropFolder->toUpdatableObject($dbDropFolder);
        $dbDropFolder->save();

        $dropFolder = KontorolDropFolder::getInstanceByType($dbDropFolder->getType());
        $dropFolder->fromObject($dbDropFolder, $this->getResponseProfile());

        return $dropFolder;
    }
	
}

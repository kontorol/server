<?php
/**
 * @package plugins.sphinxSearch
 */
class SphinxSearchPlugin extends KontorolPlugin implements IKontorolEventConsumers, IKontorolCriteriaFactory, IKontorolPending
{
	const PLUGIN_NAME = 'sphinxSearch';
	const SPHINX_SEARCH_MANAGER = 'kSphinxSearchManager';
	
	public static function getPluginName()
	{
		return self::PLUGIN_NAME;
	}
	
	/**
	 * @return array
	 */
	public static function getEventConsumers()
	{
		return array(
			self::SPHINX_SEARCH_MANAGER,
		);
	}
	
	/**
	 * Creates a new KontorolCriteria for the given object name
	 * 
	 * @param string $objectType object type to create Criteria for.
	 * @return KontorolCriteria derived object
	 */
	public static function getKontorolCriteria($objectType)
	{
		if ($objectType == "entry")
			return new SphinxEntryCriteria();
			
		if ($objectType == "category")
			return new SphinxCategoryCriteria();
			
		if ($objectType == "kuser")
			return new SphinxKuserCriteria();
		
		if ($objectType == "categoryKuser")
			return new SphinxCategoryKuserCriteria();
			
		return null;
	}

	/**
	 * Returns a Kontorol dependency object that defines the relationship between two plugins.
	 *
	 * @return array<KontorolDependency> The Kontorol dependency object
	 */
	public static function dependsOn()
	{
		$searchDependency = new KontorolDependency(SearchPlugin::getPluginName());
		return array($searchDependency);
	}
}

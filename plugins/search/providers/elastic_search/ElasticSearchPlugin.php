<?php
/**
 * @package plugins.elasticSearch
 */
class ElasticSearchPlugin extends KontorolPlugin implements IKontorolEventConsumers, IKontorolPending, IKontorolServices, IKontorolObjectLoader, IKontorolExceptionHandler, IKontorolEnumerator, IKontorolFilterExecutor
{
	const PLUGIN_NAME = 'elasticSearch';
	const ELASTIC_SEARCH_MANAGER = 'kElasticSearchManager';
	const ELASTIC_CORE_EXCEPTION = 'kESearchException';
	const ELASTIC_DYNAMIC_MAP = 'elasticDynamicMap';
	const CUTOFF_FREQUENCY = 'cutoff_frequency';
	const CUTOFF_FREQUENCY_DEFAULT = 0.001;
	const MAX_WORDS_NGRAM = 'max_words_for_ngram';
	const MAX_WORDS_NGRAM_DEFAULT = 1;
	const NON_REDUCE_RESULTS_PARTNER_LIST = 'non_reduced_results_partner_list';
	const DEDICATED_ENTRY_INDEX_PARTNER_LIST = 'dedicated_entry_index_partner_list';
	const DEDICATED_ENTRY_INDEX_NAME = 'dedicated_entry_index_name';
	const FILTER_TAGS_MAP_NAME = 'elasticDynamicMap';
	const FILTER_TAGS_PARAM_NAME = 'filterExecutionTags';
	const DEDICATE_INDEX_PARTNER_LIST = 'dedicate_index_partner_list';
	const MAX_METADATA_INDEX_LENGTH = 'max_metadata_index_length';

	public static function getPluginName()
	{
		return self::PLUGIN_NAME;
	}

	/**
	 * @return array
	 */
	public static function getEventConsumers()
	{
		return array(
			self::ELASTIC_SEARCH_MANAGER,
		);
	}

	/**
	 * Returns a Kontorol dependency object that defines the relationship between two plugins.
	 *
	 * @return array<KontorolDependency> The Kontorol dependency object
	 */
	public static function dependsOn()
	{
		$searchDependency = new KontorolDependency(SearchPlugin::getPluginName());
		return array($searchDependency);
	}

	public static function getServicesMap()
	{
		$map = array(
			'ESearch' => 'ESearchService',
		);
		return $map;
	}

	/* (non-PHPdoc)
	 * @see IKontorolObjectLoader::loadObject()
	 */
	public static function loadObject($baseClass, $enumValue, array $constructorArgs = null)
	{
		if ($baseClass == 'KontorolESearchItemData' && $enumValue == KontorolESearchItemDataType::CAPTION)
			return new KontorolESearchCaptionItemData();

		if ($baseClass == 'ESearchItemData' && $enumValue == ESearchItemDataType::CAPTION)
			return new ESearchCaptionItemData();

		if ($baseClass == 'KontorolESearchItemData' && $enumValue == KontorolESearchItemDataType::METADATA)
			return new KontorolESearchMetadataItemData();

		if ($baseClass == 'ESearchItemData' && $enumValue == ESearchItemDataType::METADATA)
			return new ESearchMetadataItemData();

		if ($baseClass == 'KontorolESearchItemData' && $enumValue == KontorolESearchItemDataType::CUE_POINTS)
			return new KontorolESearchCuePointItemData();

		if ($baseClass == 'ESearchItemData' && $enumValue == ESearchItemDataType::CUE_POINTS)
			return new ESearchCuePointItemData();

		if ($baseClass == 'KObjectExportEngine' && $enumValue == KontorolExportObjectType::ESEARCH_MEDIA)
		{
			return new KExportMediaEsearchEngine($constructorArgs);
		}
	
		if($baseClass == 'KontorolJobData' && $enumValue == BatchJobType::EXPORT_CSV && (isset($constructorArgs['coreJobSubType']) &&  $constructorArgs['coreJobSubType']== self::getExportTypeCoreValue(EsearchMediaEntryExportObjectType::ESEARCH_MEDIA)))
		{
			return new KontorolMediaEsearchExportToCsvJobData();
		}
	
		if ($baseClass == 'KontorolESearchOrderByItem' && $enumValue == 'ESearchMetadataOrderByItem')
		{
			return new KontorolESearchMetadataOrderByItem($constructorArgs);
		}
	
		return null;
	}

	/* (non-PHPdoc)
	* @see IKontorolObjectLoader::loadObject()
	*/
	public static function getObjectClass($baseClass, $enumValue)
	{
	   return null;
	}

	public static function handleESearchException($exception)
	{
		$code = $exception->getCode();
		$data = $exception->getData();
		switch ($code)
		{
			case kESearchException::SEARCH_TYPE_NOT_ALLOWED_ON_FIELD:
				$object = new KontorolAPIException(KontorolESearchErrors::SEARCH_TYPE_NOT_ALLOWED_ON_FIELD, $data['itemType'], $data['fieldName']);
				break;
			case kESearchException::EMPTY_SEARCH_TERM_NOT_ALLOWED:
				$object = new KontorolAPIException(KontorolESearchErrors::EMPTY_SEARCH_TERM_NOT_ALLOWED, $data['fieldName'], $data['itemType']);
				break;
			case kESearchException::SEARCH_TYPE_NOT_ALLOWED_ON_UNIFIED_SEARCH:
				$object = new KontorolAPIException(KontorolESearchErrors::SEARCH_TYPE_NOT_ALLOWED_ON_UNIFIED_SEARCH, $data['itemType']);
				break;
			case kESearchException::EMPTY_SEARCH_ITEMS_NOT_ALLOWED:
				$object = new KontorolAPIException(KontorolESearchErrors::EMPTY_SEARCH_ITEMS_NOT_ALLOWED);
				break;
			case kESearchException::UNMATCHING_BRACKETS:
				$object = new KontorolAPIException(KontorolESearchErrors::UNMATCHING_BRACKETS);
				break;
			case kESearchException::MISSING_QUERY_OPERAND:
				$object = new KontorolAPIException(KontorolESearchErrors::MISSING_QUERY_OPERAND);
				break;
			case kESearchException::UNMATCHING_QUERY_OPERAND:
				$object = new KontorolAPIException(KontorolESearchErrors::UNMATCHING_QUERY_OPERAND);
				break;
			case kESearchException::CONSECUTIVE_OPERANDS_MISMATCH:
				$object = new KontorolAPIException(KontorolESearchErrors::CONSECUTIVE_OPERANDS_MISMATCH);
				break;
			case kESearchException::INVALID_FIELD_NAME:
				$object = new KontorolAPIException(KontorolESearchErrors::INVALID_FIELD_NAME, $data['fieldName']);
				break;
			case kESearchException::MISSING_MANDATORY_PARAMETERS_IN_ORDER_ITEM:
				$object = new KontorolAPIException(KontorolESearchErrors::MISSING_MANDATORY_PARAMETERS_IN_ORDER_ITEM);
				break;
			case kESearchException::MIXED_SEARCH_ITEMS_IN_NESTED_OPERATOR_NOT_ALLOWED:
				$object = new KontorolAPIException(KontorolESearchErrors::MIXED_SEARCH_ITEMS_IN_NESTED_OPERATOR_NOT_ALLOWED);
				break;
			case kESearchException::MISSING_OPERATOR_TYPE:
				$object = new KontorolAPIException(KontorolESearchErrors::MISSING_OPERATOR_TYPE);
				break;
			case kESearchException::UNABLE_TO_EXECUTE_ENTRY_CAPTION_ADVANCED_FILTER:
				$object = new KontorolAPIException(KontorolESearchErrors::UNABLE_TO_EXECUTE_ENTRY_CAPTION_ADVANCED_FILTER);
				break;

			default:
				$object = null;
		}
		return $object;
	}

	public function getExceptionMap()
	{
		return array(
			self::ELASTIC_CORE_EXCEPTION => array('ElasticSearchPlugin', 'handleESearchException'),
		);
	}

	/**
	 * @return int id of dynamic enum in the DB.
	 * @throws kCoreException
	 */
	public static function getExportTypeCoreValue($valueName)
	{
		$value = self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
		return kPluginableEnumsManager::apiToCore('ExportObjectType', $value);
	}
	
	/**
	 * @return string external API value of dynamic enum.
	 */
	public static function getApiValue($valueName)
	{
		return self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
	}
	
	/**
	 * @return array<string> list of enum classes names that extend the base enum name
	 */
	public static function getEnums($baseEnumName = null)
	{
		if(is_null($baseEnumName))
			return array('EsearchMediaEntryExportObjectType');
		
		if($baseEnumName == 'ExportObjectType')
			return array('EsearchMediaEntryExportObjectType');
		
		return array();
	}

	public static function canExecuteFilter(KontorolRelatedFilter $filter, $coreFilter)
	{
		$adapter = ESearchAdapterFactory::getAdapter($filter);
		return self::isValidClientsTagsForFilterExecutor() && $adapter && $adapter::canTransformFilter($coreFilter);
	}

	public static function executeFilter(KontorolRelatedFilter $filter, $coreFilter, KontorolFilterPager $pager, $responseProfile = null)
	{
		$corePager = $pager->toObject();
		$eSearchAdapter = ESearchAdapterFactory::getAdapter($filter);
		list($list, $totalCount) = $eSearchAdapter->retrieveElasticQueryCoreEntries($coreFilter, $corePager);
		$newList = KontorolBaseEntryArray::fromDbArray($list, $responseProfile);
		$response = new KontorolListResponse();
		$response->objects = $newList;
		$response->totalCount = $totalCount;

		return $response;
	}

	protected static function isValidClientsTagsForFilterExecutor()
	{
		$result = false;
		$params = infraRequestUtils::getRequestParams();
		$clientsTags = isset($params[infraRequestUtils::CLIENT_TAG]) ? $params[infraRequestUtils::CLIENT_TAG] : null;
		$tagsForExecutor = kConf::get(self::FILTER_TAGS_PARAM_NAME, self::FILTER_TAGS_MAP_NAME, array());
		foreach($tagsForExecutor as $tag)
		{
			$result = (strpos($clientsTags, $tag) !== false);
			if($result)
			{
				break;
			}
		}

		return $result;
	}
}

<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolESearchEntryOrderByItem extends KontorolESearchOrderByItem
{
    /**
     *  @var KontorolESearchEntryOrderByFieldName
     */
    public $sortField;

    private static $map_between_objects = array(
        'sortField',
    );

    private static $map_field_enum = array(
        KontorolESearchEntryOrderByFieldName::CREATED_AT => ESearchEntryOrderByFieldName::CREATED_AT,
        KontorolESearchEntryOrderByFieldName::UPDATED_AT => ESearchEntryOrderByFieldName::UPDATED_AT,
        KontorolESearchEntryOrderByFieldName::END_DATE => ESearchEntryOrderByFieldName::END_DATE,
        KontorolESearchEntryOrderByFieldName::START_DATE => ESearchEntryOrderByFieldName::START_DATE,
        KontorolESearchEntryOrderByFieldName::NAME => ESearchEntryOrderByFieldName::NAME,
        KontorolESearchEntryOrderByFieldName::VIEWS => ESearchEntryOrderByFieldName::VIEWS,
        KontorolESearchEntryOrderByFieldName::VOTES => ESearchEntryOrderByFieldName::VOTES,
        KontorolESearchEntryOrderByFieldName::PLAYS => ESearchEntryOrderByFieldName::PLAYS,
	    KontorolESearchEntryOrderByFieldName::RANK => ESearchEntryOrderByFieldName::RANK,
        KontorolESearchEntryOrderByFieldName::LAST_PLAYED_AT => ESearchEntryOrderByFieldName::LAST_PLAYED_AT,
        KontorolESearchEntryOrderByFieldName::PLAYS_LAST_30_DAYS => ESearchEntryOrderByFieldName::PLAYS_LAST_30_DAYS,
        KontorolESearchEntryOrderByFieldName::VIEWS_LAST_30_DAYS => ESearchEntryOrderByFieldName::VIEWS_LAST_30_DAYS,
        KontorolESearchEntryOrderByFieldName::PLAYS_LAST_7_DAYS => ESearchEntryOrderByFieldName::PLAYS_LAST_7_DAYS,
        KontorolESearchEntryOrderByFieldName::VIEWS_LAST_7_DAYS => ESearchEntryOrderByFieldName::VIEWS_LAST_7_DAYS,
        KontorolESearchEntryOrderByFieldName::PLAYS_LAST_1_DAY => ESearchEntryOrderByFieldName::PLAYS_LAST_1_DAY,
        KontorolESearchEntryOrderByFieldName::VIEWS_LAST_1_DAY => ESearchEntryOrderByFieldName::VIEWS_LAST_1_DAY,
    );

    public function getMapBetweenObjects()
    {
        return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
    }

    public function toObject($object_to_fill = null, $props_to_skip = array())
    {
        if (!$object_to_fill)
            $object_to_fill = new ESearchEntryOrderByItem();
        return parent::toObject($object_to_fill, $props_to_skip);
    }

    public function getFieldEnumMap()
    {
        return self::$map_field_enum;
    }

    public function getItemFieldName()
    {
        return $this->sortField;
    }

}

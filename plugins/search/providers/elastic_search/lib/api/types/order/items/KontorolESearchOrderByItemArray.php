<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolESearchOrderByItemArray extends KontorolTypedArray
{

    public function __construct()
    {
        return parent::__construct("KontorolESearchOrderByItem");
    }
	
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		KontorolLog::debug(print_r($arr, true));
		$newArr = new KontorolESearchOrderByItemArray();
		if ($arr == null)
			return $newArr;
		
		foreach ($arr as $obj)
		{
			switch (get_class($obj))
			{
				case 'ESearchEntryOrderByItem':
					$nObj = new KontorolESearchEntryOrderByItem();
					break;
				
				case 'ESearchCategoryOrderByItem':
					$nObj = new KontorolESearchCategoryOrderByItem();
					break;
				
				case 'ESearchUserOrderByItem':
					$nObj = new KontorolESearchUserOrderByItem();
					break;
				
				default:
					$nObj = KontorolPluginManager::loadObject('KontorolESearchOrderByItem', get_class($obj));
					break;
			}
			
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
}

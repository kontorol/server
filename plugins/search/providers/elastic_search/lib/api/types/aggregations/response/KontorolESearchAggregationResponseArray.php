<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolESearchAggregationResponseArray extends KontorolTypedArray
{
	public function __construct()
	{
		return parent::__construct("KontorolESearchAggregationResponseItem");
	}

	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$outputArray = new KontorolESearchAggregationResponseArray();
		foreach ( $arr as $obj )
		{
			$nObj = new KontorolESearchAggregationResponseItem();
			$nObj->fromObject($obj, $responseProfile);
			$outputArray[] = $nObj;
		}
		return $outputArray;
	}


}

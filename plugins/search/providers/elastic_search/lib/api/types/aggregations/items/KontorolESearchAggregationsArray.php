<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */

class KontorolESearchAggregationsArray extends KontorolTypedArray
{
	public function __construct()
	{
		return parent::__construct("KontorolESearchAggregationItem");
	}
}

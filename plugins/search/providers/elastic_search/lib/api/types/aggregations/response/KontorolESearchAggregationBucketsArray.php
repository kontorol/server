<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolESearchAggregationBucketsArray extends KontorolTypedArray
{
	public function __construct()
	{
	    parent::__construct("KontorolESearchAggregationBucket");
	}
}

<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */


class KontorolESearchMetadataAggregationItem extends KontorolESearchAggregationItem
{
	/**
	 *  @var KontorolESearchMetadataAggregateByFieldName
	 */
	public $fieldName;

	const FIELD_NAME = 3;

	public function toObject($object_to_fill = null, $props_to_skip = array())
	{
		if (!$object_to_fill)
		{
			$object_to_fill = new ESearchMetadataAggregationItem();
		}
		return parent::toObject($object_to_fill, $props_to_skip);
	}

	public function getFieldEnumMap()
	{
		return array ();
	}

	protected function getMetadataFieldNameFromXpath($xpath)
	{
		$token = explode("'", $xpath);
		return $token[self::FIELD_NAME];
	}

	public function coreToApiResponse($coreResponse, $fieldName=null)
	{
		$ret = array();

		$buckets = $coreResponse[ESearchAggregationItem::NESTED_BUCKET][ESearchAggregations::BUCKETS];
		if ($buckets)
		{
			foreach ($buckets as $bucket)
			{
				$agg = new KontorolESearchAggregationResponseItem();
				$agg->name = ESearchMetadataAggregationItem::KEY;
				//get the field name from the xpath
				$metadataFieldName = $this->getMetadataFieldNameFromXpath($bucket[ESearchAggregations::KEY]);
				$agg->fieldName = $metadataFieldName;
				// loop over the sub aggregations
				$bucketsArray = new KontorolESearchAggregationBucketsArray();
				$subBuckets = $bucket[ESearchMetadataAggregationItem::SUB_AGG][ESearchAggregations::BUCKETS];
				if(!$subBuckets)
				{
					continue;
				}
				foreach($subBuckets as $subBucket)
				{
					$responseBucket = new KontorolESearchAggregationBucket();
					$responseBucket->fromArray($subBucket);
					$bucketsArray[] = $responseBucket;
				}
				$agg->buckets = $bucketsArray;
				$ret[] = $agg;
			}
		}
		return $ret;
	}

	public function validateForUsage($sourceObject, $propertiesToSkip = array())
	{
		KontorolObject::validateForUsage($sourceObject, $propertiesToSkip);
	}

}

<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.errors
 */

class KontorolESearchAggregationErrors extends KontorolESearchErrors
{
	const AGGREGATION_FIELD_NAME_MUST_BE_SUPPLIED = 'AGGREGATION_FIELD_NAME_MUST_BE_SUPPLIED;;Aggregation field name must be supplied';
}

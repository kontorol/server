<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */


class KontorolESearchEntryAggregationItem extends KontorolESearchAggregationItem
{

	/**
	 *  @var KontorolESearchEntryAggregateByFieldName
	 */
	public $fieldName;


	public function toObject($object_to_fill = null, $props_to_skip = array())
	{
		if (!$object_to_fill)
		{
			$object_to_fill = new ESearchEntryAggregationItem();
		}
		return parent::toObject($object_to_fill, $props_to_skip);
	}

	public function getFieldEnumMap()
	{
		return array (
			KontorolESearchEntryAggregateByFieldName::ENTRY_TYPE => ESearchEntryAggregationFieldName::ENTRY_TYPE,
			KontorolESearchEntryAggregateByFieldName::MEDIA_TYPE => ESearchEntryAggregationFieldName::MEDIA_TYPE,
			KontorolESearchEntryAggregateByFieldName::TAGS => ESearchEntryAggregationFieldName::TAGS,
			KontorolESearchEntryAggregateByFieldName::ACCESS_CONTROL_PROFILE => ESearchEntryAggregationFieldName::ACCESS_CONTROL_PROFILE);
	}

	public function coreToApiResponse($coreResponse, $fieldName = null)
	{
		$agg = new KontorolESearchAggregationResponseItem();
		$agg->fieldName = $fieldName;
		$agg->name = ESearchEntryAggregationItem::KEY;
		$bucketsArray = new KontorolESearchAggregationBucketsArray();
		$buckets = $coreResponse[ESearchAggregations::BUCKETS];
		if ($buckets)
		{
			foreach ($buckets as $bucket)
			{
				$responseBucket = new KontorolESearchAggregationBucket();
				$responseBucket->fromArray($bucket);
				$bucketsArray[] = $responseBucket;
			}
		}
		$agg->buckets = $bucketsArray;
		return array($agg);
	}


}

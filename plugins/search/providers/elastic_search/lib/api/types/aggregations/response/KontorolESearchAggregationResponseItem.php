<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolESearchAggregationResponseItem extends KontorolObject
{
	/**
	 * @var string
	 */
	public $name;

	/**
	 * @var string
	 */
	public $fieldName;

	/**
	 * @var KontorolESearchAggregationBucketsArray
	 */
	public $buckets;
}

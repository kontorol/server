<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolESearchEntryBaseItemArray extends KontorolTypedArray
{
	
	public function __construct()
	{
		return parent::__construct("KontorolESearchEntryBaseItem");
	}
	
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		KontorolLog::debug(print_r($arr, true));
		$newArr = new KontorolESearchEntryBaseItemArray();
		if ($arr == null)
			return $newArr;
		
		foreach ($arr as $obj)
		{
			switch (get_class($obj))
			{
				case 'ESearchEntryItem':
					$nObj = new KontorolESearchEntryItem();
					break;
				
				case 'ESearchOperator':
					$nObj = new KontorolESearchEntryOperator();
					break;
				
				case 'ESearchMetadataItem':
					$nObj = new KontorolESearchEntryMetadataItem();
					break;
				
				case 'ESearchCuePointItem':
					$nObj = new KontorolESearchCuePointItem();
					break;
				
				case 'ESearchCaptionItem':
					$nObj = new KontorolESearchCaptionItem();
					break;
				
				case 'ESearchCategoryEntryNameItem':
					$nObj = new KontorolESearchCategoryEntryItem();
					break;
				
				case 'ESearchUnifiedItem':
					$nObj = new KontorolESearchUnifiedItem();
					break;
				
				case 'ESearchNestedOperator':
					$nObj = new KontorolESearchNestedOperator();
					break;
					
				default:
					$nObj = KontorolPluginManager::loadObject('KontorolESearchEntryBaseItem', get_class($obj));
					break;
			}
			
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
}

<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolESearchUserItem extends KontorolESearchAbstractUserItem
{

	const KUSER_ID_THAT_DOESNT_EXIST = -1;

	/**
	 * @var KontorolESearchUserFieldName
	 */
	public $fieldName;

	private static $map_between_objects = array(
		'fieldName'
	);

	private static $map_dynamic_enum = array();

	private static $map_field_enum = array(
		KontorolESearchUserFieldName::SCREEN_NAME => ESearchUserFieldName::SCREEN_NAME,
		KontorolESearchUserFieldName::EMAIL => ESearchUserFieldName::EMAIL,
		KontorolESearchUserFieldName::TYPE => ESearchUserFieldName::TYPE,
		KontorolESearchUserFieldName::TAGS => ESearchUserFieldName::TAGS,
		KontorolESearchUserFieldName::UPDATED_AT => ESearchUserFieldName::UPDATED_AT,
		KontorolESearchUserFieldName::CREATED_AT => ESearchUserFieldName::CREATED_AT,
		KontorolESearchUserFieldName::LAST_NAME => ESearchUserFieldName::LAST_NAME,
		KontorolESearchUserFieldName::FIRST_NAME => ESearchUserFieldName::FIRST_NAME,
		KontorolESearchUserFieldName::PERMISSION_NAMES => ESearchUserFieldName::PERMISSION_NAMES,
		KontorolESearchUserFieldName::GROUP_IDS => ESearchUserFieldName::GROUP_IDS,
		KontorolESearchUserFieldName::ROLE_IDS => ESearchUserFieldName::ROLE_IDS,
		KontorolESearchUserFieldName::USER_ID => ESearchUserFieldName::PUSER_ID,
	);

	protected function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}

	public function toObject($object_to_fill = null, $props_to_skip = array())
	{
		if (!$object_to_fill)
			$object_to_fill = new ESearchUserItem();

		if (in_array($this->fieldName, array(KontorolESearchUserFieldName::GROUP_IDS)))
		{
			$kuserId = self::KUSER_ID_THAT_DOESNT_EXIST;
			$kuser = kuserPeer::getKuserByPartnerAndUid(kCurrentContext::getCurrentPartnerId(), $this->searchTerm, true);
			if ($kuser)
			{
				$kuserId = $kuser->getId();
			}

			$this->searchTerm = $kuserId;
		}
		return parent::toObject($object_to_fill, $props_to_skip);
	}

	protected function getItemFieldName()
	{
		return $this->fieldName;
	}

	protected function getDynamicEnumMap()
	{
		return self::$map_dynamic_enum;
	}

	protected function getFieldEnumMap()
	{
		return self::$map_field_enum;
	}

}

<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolESearchEntryNestedBaseItemArray extends KontorolTypedArray
{
	
	public function __construct()
	{
		return parent::__construct("KontorolESearchEntryNestedBaseItem");
	}
	
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		KontorolLog::debug(print_r($arr, true));
		$newArr = new KontorolESearchEntryNestedBaseItemArray();
		if ($arr == null)
			return $newArr;
		
		foreach ($arr as $obj)
		{
			switch (get_class($obj))
			{
				case 'ESearchMetadataItem':
					$nObj = new KontorolESearchEntryMetadataItem();
					break;
				
				case 'ESearchCuePointItem':
					$nObj = new KontorolESearchCuePointItem();
					break;
				
				case 'ESearchCaptionItem':
					$nObj = new KontorolESearchCaptionItem();
					break;
				
				case 'ESearchNestedOperator':
					$nObj = new KontorolESearchNestedOperator();
					break;
				
				default:
					$nObj = KontorolPluginManager::loadObject('KontorolESearchEntryNestedBaseItem', get_class($obj));
					break;
			}
			
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
}

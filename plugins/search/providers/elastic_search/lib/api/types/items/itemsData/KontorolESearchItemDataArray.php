<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolESearchItemDataArray extends KontorolTypedArray
{

    public function __construct()
    {
        return parent::__construct("KontorolESearchItemData");
    }

	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolESearchItemDataArray();
		foreach ( $arr as $obj )
		{
			$nObj = KontorolPluginManager::loadObject('KontorolESearchItemData', $obj->getType());
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}

		return $newArr;
	}

}

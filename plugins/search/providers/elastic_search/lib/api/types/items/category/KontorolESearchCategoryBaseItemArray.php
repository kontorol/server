<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolESearchCategoryBaseItemArray extends KontorolTypedArray
{
	
	public function __construct()
	{
		return parent::__construct("KontorolESearchCategoryBaseItem");
	}
	
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		KontorolLog::debug(print_r($arr, true));
		$newArr = new KontorolESearchCategoryBaseItemArray();
		if ($arr == null)
			return $newArr;
		
		foreach ($arr as $obj)
		{
			switch (get_class($obj))
			{
				case 'ESearchCategoryItem':
					$nObj = new KontorolESearchCategoryItem();
					break;
				
				default:
					$nObj = KontorolPluginManager::loadObject('KontorolESearchCategoryBaseItem', get_class($obj));
					break;
			}
			
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
}

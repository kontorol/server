<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolESearchUserBaseItemArray extends KontorolTypedArray
{
	
	public function __construct()
	{
		return parent::__construct("KontorolESearchUserBaseItem");
	}
	
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		KontorolLog::debug(print_r($arr, true));
		$newArr = new KontorolESearchUserBaseItemArray();
		if ($arr == null)
			return $newArr;
		
		foreach ($arr as $obj)
		{
			switch (get_class($obj))
			{
				case 'ESearchUserItem':
					$nObj = new KontorolESearchUserItem();
					break;
				
				default:
					$nObj = KontorolPluginManager::loadObject('KontorolESearchUserBaseItem', get_class($obj));
					break;
			}
			
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
}

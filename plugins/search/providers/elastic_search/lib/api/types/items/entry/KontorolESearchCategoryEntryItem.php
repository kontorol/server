<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolESearchCategoryEntryItem extends KontorolESearchAbstractEntryItem
{
	/**
	 * @var KontorolESearchCategoryEntryFieldName
	 */
	public $fieldName;

	/**
	 * @var KontorolCategoryEntryStatus
	 */
	public $categoryEntryStatus;

	private static $map_between_objects = array(
		'fieldName',
		'categoryEntryStatus',
	);

	private static $map_dynamic_enum = array();

	private static $map_field_enum = array(
		KontorolESearchCategoryEntryFieldName::ID => ESearchCategoryEntryFieldName::ID,
		KontorolESearchCategoryEntryFieldName::FULL_IDS => ESearchCategoryEntryFieldName::FULL_IDS,
		KontorolESearchCategoryEntryFieldName::NAME => ESearchCategoryEntryFieldName::NAME,
		KontorolESearchCategoryEntryFieldName::ANCESTOR_ID => ESearchCategoryEntryFieldName::ANCESTOR_ID,
		KontorolESearchCategoryEntryFieldName::ANCESTOR_NAME => ESearchCategoryEntryFieldName::ANCESTOR_NAME,
	);

	protected function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}

	public function toObject($object_to_fill = null, $props_to_skip = array())
	{
		if (!$object_to_fill)
			$object_to_fill = ESearchCategoryEntryItemFactory::getCoreItemByFieldName($this->fieldName);

		return parent::toObject($object_to_fill, $props_to_skip);
	}

	protected function getItemFieldName()
	{
		return $this->fieldName;
	}

	protected function getDynamicEnumMap()
	{
		return self::$map_dynamic_enum;
	}

	protected function getFieldEnumMap()
	{
		return self::$map_field_enum;
	}

}

<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolESearchCuePointItem extends KontorolESearchEntryAbstractNestedItem
{

	/**
	 * @var KontorolESearchCuePointFieldName
	 */
	public $fieldName;

	private static $map_between_objects = array(
		'fieldName',
	);

	private static $map_dynamic_enum = array(
		KontorolESearchCuePointFieldName::TYPE => 'KontorolCuePointType',
	);

	private static $map_field_enum = array(
		KontorolESearchCuePointFieldName::ANSWERS => ESearchCuePointFieldName::ANSWERS,
		KontorolESearchCuePointFieldName::END_TIME => ESearchCuePointFieldName::END_TIME,
		KontorolESearchCuePointFieldName::EXPLANATION => ESearchCuePointFieldName::EXPLANATION,
		KontorolESearchCuePointFieldName::HINT => ESearchCuePointFieldName::HINT,
		KontorolESearchCuePointFieldName::ID => ESearchCuePointFieldName::ID,
		KontorolESearchCuePointFieldName::NAME => ESearchCuePointFieldName::NAME,
		KontorolESearchCuePointFieldName::QUESTION => ESearchCuePointFieldName::QUESTION,
		KontorolESearchCuePointFieldName::START_TIME => ESearchCuePointFieldName::START_TIME,
		KontorolESearchCuePointFieldName::TAGS => ESearchCuePointFieldName::TAGS,
		KontorolESearchCuePointFieldName::TEXT => ESearchCuePointFieldName::TEXT,
		KontorolESearchCuePointFieldName::SUB_TYPE => ESearchCuePointFieldName::SUB_TYPE,
		KontorolESearchCuePointFieldName::TYPE => ESearchCuePointFieldName::TYPE,
	);

	protected function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}

	public function toObject($object_to_fill = null, $props_to_skip = array())
	{
		if (!$object_to_fill)
			$object_to_fill = new ESearchCuePointItem();

		return parent::toObject($object_to_fill, $props_to_skip);
	}
	
	protected function doFromObject($srcObj, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$this->fieldName = self::getApiFieldName($srcObj->getFieldName());
		return parent::doFromObject($srcObj, $responseProfile);
	}
	
	protected static function getApiFieldName ($srcFieldName)
	{
		foreach (self::$map_field_enum as $key => $value)
		{
			if ($value == $srcFieldName)
			{
				return $key;
			}
		}
		
		return null;
	}

	protected function getItemFieldName()
	{
		return $this->fieldName;
	}

	protected function getDynamicEnumMap()
	{
		return self::$map_dynamic_enum;
	}

	protected function getFieldEnumMap()
	{
		return self::$map_field_enum;
	}

}

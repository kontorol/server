<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolESearchHighlightArray extends KontorolTypedArray
{
	public function __construct()
	{
		return parent::__construct("KontorolESearchHighlight");
	}

	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolESearchHighlightArray();
		foreach ( $arr as $obj )
		{
			$nObj = new KontorolESearchHighlight();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}

		return $newArr;
	}
}

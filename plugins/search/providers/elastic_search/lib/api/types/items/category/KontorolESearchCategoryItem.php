<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolESearchCategoryItem extends KontorolESearchAbstractCategoryItem
{

	/**
	 * @var KontorolESearchCategoryFieldName
	 */
	public $fieldName;

	private static $map_between_objects = array(
		'fieldName'
	);

	private static $map_dynamic_enum = array();

	private static $map_field_enum = array(
		KontorolESearchCategoryFieldName::ID => ESearchCategoryFieldName::ID,
		KontorolESearchCategoryFieldName::PRIVACY => ESearchCategoryFieldName::PRIVACY,
		KontorolESearchCategoryFieldName::PRIVACY_CONTEXT => ESearchCategoryFieldName::PRIVACY_CONTEXT,
		KontorolESearchCategoryFieldName::PRIVACY_CONTEXTS => ESearchCategoryFieldName::PRIVACY_CONTEXTS,
		KontorolESearchCategoryFieldName::PARENT_ID => ESearchCategoryFieldName::PARENT_ID,
		KontorolESearchCategoryFieldName::DEPTH => ESearchCategoryFieldName::DEPTH,
		KontorolESearchCategoryFieldName::NAME => ESearchCategoryFieldName::NAME,
		KontorolESearchCategoryFieldName::FULL_NAME => ESearchCategoryFieldName::FULL_NAME,
		KontorolESearchCategoryFieldName::FULL_IDS => ESearchCategoryFieldName::FULL_IDS,
		KontorolESearchCategoryFieldName::DESCRIPTION => ESearchCategoryFieldName::DESCRIPTION,
		KontorolESearchCategoryFieldName::TAGS => ESearchCategoryFieldName::TAGS,
		KontorolESearchCategoryFieldName::DISPLAY_IN_SEARCH => ESearchCategoryFieldName::DISPLAY_IN_SEARCH,
		KontorolESearchCategoryFieldName::INHERITANCE_TYPE => ESearchCategoryFieldName::INHERITANCE_TYPE,
		KontorolESearchCategoryFieldName::USER_ID => ESearchCategoryFieldName::KUSER_ID,
		KontorolESearchCategoryFieldName::REFERENCE_ID => ESearchCategoryFieldName::REFERENCE_ID,
		KontorolESearchCategoryFieldName::INHERITED_PARENT_ID => ESearchCategoryFieldName::INHERITED_PARENT_ID,
		KontorolESearchCategoryFieldName::MODERATION => ESearchCategoryFieldName::MODERATION,
		KontorolESearchCategoryFieldName::CONTRIBUTION_POLICY => ESearchCategoryFieldName::CONTRIBUTION_POLICY,
		KontorolESearchCategoryFieldName::ENTRIES_COUNT => ESearchCategoryFieldName::ENTRIES_COUNT,
		KontorolESearchCategoryFieldName::DIRECT_ENTRIES_COUNT => ESearchCategoryFieldName::DIRECT_ENTRIES_COUNT,
		KontorolESearchCategoryFieldName::DIRECT_SUB_CATEGORIES_COUNT => ESearchCategoryFieldName::DIRECT_SUB_CATEGORIES_COUNT,
		KontorolESearchCategoryFieldName::MEMBERS_COUNT => ESearchCategoryFieldName::MEMBERS_COUNT,
		KontorolESearchCategoryFieldName::PENDING_MEMBERS_COUNT => ESearchCategoryFieldName::PENDING_MEMBERS_COUNT,
		KontorolESearchCategoryFieldName::PENDING_ENTRIES_COUNT => ESearchCategoryFieldName::PENDING_ENTRIES_COUNT,
		KontorolESearchCategoryFieldName::CREATED_AT => ESearchCategoryFieldName::CREATED_AT,
		KontorolESearchCategoryFieldName::UPDATED_AT => ESearchCategoryFieldName::UPDATED_AT,
	);

	protected function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}

	public function toObject($object_to_fill = null, $props_to_skip = array())
	{
		if (!$object_to_fill)
			$object_to_fill = new ESearchCategoryItem();
		return parent::toObject($object_to_fill, $props_to_skip);
	}

	protected function getItemFieldName()
	{
		return $this->fieldName;
	}

	protected function getDynamicEnumMap()
	{
		return self::$map_dynamic_enum;
	}

	protected function getFieldEnumMap()
	{
		return self::$map_field_enum;
	}

}

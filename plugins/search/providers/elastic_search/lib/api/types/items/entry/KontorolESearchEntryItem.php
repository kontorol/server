<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolESearchEntryItem extends KontorolESearchAbstractEntryItem
{

	const KUSER_ID_THAT_DOESNT_EXIST = -1;

	/**
	 * @var KontorolESearchEntryFieldName
	 */
	public $fieldName;

	private static $map_between_objects = array(
		'fieldName'
	);

	private static $map_dynamic_enum = array(
		KontorolESearchEntryFieldName::ENTRY_TYPE => 'KontorolEntryType',
		KontorolESearchEntryFieldName::SOURCE_TYPE => 'KontorolSourceType',
		KontorolESearchEntryFieldName::EXTERNAL_SOURCE_TYPE => 'KontorolExternalMediaSourceType'
	);

	private static $map_field_enum = array(
		KontorolESearchEntryFieldName::ID => ESearchEntryFieldName::ID,
		KontorolESearchEntryFieldName::NAME => ESearchEntryFieldName::NAME,
		KontorolESearchEntryFieldName::DESCRIPTION => ESearchEntryFieldName::DESCRIPTION,
		KontorolESearchEntryFieldName::TAGS => ESearchEntryFieldName::TAGS,
		KontorolESearchEntryFieldName::USER_ID => ESearchEntryFieldName::USER_ID,
		KontorolESearchEntryFieldName::CREATOR_ID => ESearchEntryFieldName::CREATOR_ID,
		KontorolESearchEntryFieldName::START_DATE => ESearchEntryFieldName::START_DATE,
		KontorolESearchEntryFieldName::END_DATE => ESearchEntryFieldName::END_DATE,
		KontorolESearchEntryFieldName::REFERENCE_ID => ESearchEntryFieldName::REFERENCE_ID,
		KontorolESearchEntryFieldName::CONVERSION_PROFILE_ID => ESearchEntryFieldName::CONVERSION_PROFILE_ID,
		KontorolESearchEntryFieldName::REDIRECT_ENTRY_ID => ESearchEntryFieldName::REDIRECT_ENTRY_ID,
		KontorolESearchEntryFieldName::ENTITLED_USER_EDIT => ESearchEntryFieldName::ENTITLED_USER_EDIT,
		KontorolESearchEntryFieldName::ENTITLED_USER_PUBLISH => ESearchEntryFieldName::ENTITLED_USER_PUBLISH,
		KontorolESearchEntryFieldName::ENTITLED_USER_VIEW => ESearchEntryFieldName::ENTITLED_USER_VIEW,
		KontorolESearchEntryFieldName::TEMPLATE_ENTRY_ID => ESearchEntryFieldName::TEMPLATE_ENTRY_ID,
		KontorolESearchEntryFieldName::PARENT_ENTRY_ID => ESearchEntryFieldName::PARENT_ENTRY_ID,
		KontorolESearchEntryFieldName::MEDIA_TYPE => ESearchEntryFieldName::MEDIA_TYPE,
		KontorolESearchEntryFieldName::SOURCE_TYPE => ESearchEntryFieldName::SOURCE_TYPE,
		KontorolESearchEntryFieldName::RECORDED_ENTRY_ID => ESearchEntryFieldName::RECORDED_ENTRY_ID,
		KontorolESearchEntryFieldName::PUSH_PUBLISH => ESearchEntryFieldName::PUSH_PUBLISH,
		KontorolESearchEntryFieldName::LENGTH_IN_MSECS => ESearchEntryFieldName::LENGTH_IN_MSECS,
		KontorolESearchEntryFieldName::CREATED_AT => ESearchEntryFieldName::CREATED_AT,
		KontorolESearchEntryFieldName::UPDATED_AT => ESearchEntryFieldName::UPDATED_AT,
		KontorolESearchEntryFieldName::MODERATION_STATUS => ESearchEntryFieldName::MODERATION_STATUS,
		KontorolESearchEntryFieldName::ENTRY_TYPE => ESearchEntryFieldName::ENTRY_TYPE,
		KontorolESearchEntryFieldName::ADMIN_TAGS => ESearchEntryFieldName::ADMIN_TAGS,
		KontorolESearchEntryFieldName::CREDIT => ESearchEntryFieldName::CREDIT,
		KontorolESearchEntryFieldName::SITE_URL => ESearchEntryFieldName::SITE_URL,
		KontorolESearchEntryFieldName::ACCESS_CONTROL_ID => ESearchEntryFieldName::ACCESS_CONTROL_ID,
		KontorolESearchEntryFieldName::EXTERNAL_SOURCE_TYPE => ESearchEntryFieldName::EXTERNAL_SOURCE_TYPE,
		KontorolESearchEntryFieldName::IS_QUIZ => ESearchEntryFieldName::IS_QUIZ,
		KontorolESearchEntryFieldName::IS_LIVE => ESearchEntryFieldName::IS_LIVE,
		KontorolESearchEntryFieldName::USER_NAMES => ESearchEntryFieldName::USER_NAMES,
		KontorolESearchEntryFieldName::ROOT_ID => ESearchEntryFieldName::ROOT_ID,
		KontorolESearchEntryFieldName::PARTNER_SORT_VALUE => ESearchEntryFieldName::PARTNER_SORT_VALUE,
		KontorolESearchEntryFieldName::CAPTIONS_CONTENT => ESearchEntryFieldName::CAPTIONS_CONTENT,
		KontorolESearchEntryFieldName::LAST_PLAYED_AT => ESearchEntryFieldName::LAST_PLAYED_AT,
		KontorolESearchEntryFieldName::PLAYS => ESearchEntryFieldName::PLAYS,
		KontorolESearchEntryFieldName::RANK => ESearchEntryFieldName::RANK,
		KontorolESearchEntryFieldName::VOTES => ESearchEntryFieldName::VOTES,
	);

	protected function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}

	public function toObject($object_to_fill = null, $props_to_skip = array())
	{
		if (!$object_to_fill)
			$object_to_fill = new ESearchEntryItem();

		if(in_array($this->fieldName, array(KontorolESearchEntryFieldName::USER_ID, KontorolESearchEntryFieldName::ENTITLED_USER_EDIT,
			KontorolESearchEntryFieldName::ENTITLED_USER_PUBLISH, KontorolESearchEntryFieldName::ENTITLED_USER_VIEW, KontorolESearchEntryFieldName::CREATOR_ID)))
		{
			$kuserId = self::KUSER_ID_THAT_DOESNT_EXIST;
			$kuser = kuserPeer::getKuserByPartnerAndUid(kCurrentContext::getCurrentPartnerId(), $this->searchTerm, true);
			if($kuser)
				$kuserId = $kuser->getId();

			$this->searchTerm = $kuserId;
		}

		return parent::toObject($object_to_fill, $props_to_skip);
	}

	protected function getItemFieldName()
	{
		return $this->fieldName;
	}

	protected function getDynamicEnumMap()
	{
		return self::$map_dynamic_enum;
	}

	protected function getFieldEnumMap()
	{
		return self::$map_field_enum;
	}
}

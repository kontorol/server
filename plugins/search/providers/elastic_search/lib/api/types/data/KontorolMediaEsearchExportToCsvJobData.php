<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */

class KontorolMediaEsearchExportToCsvJobData extends KontorolExportCsvJobData
{
	/**
	 * Esearch parameters for the entry search
	 *
	 * @var KontorolESearchEntryParams
	 */
	public $searchParams;
	/**
	 * options
	 * @var KontorolExportToCsvOptionsArray
	 */
	public $options;
	
	private static $map_between_objects = array
	(
		'options',
		'searchParams',
	);
	
	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects ( )
	{
		return array_merge ( parent::getMapBetweenObjects() , self::$map_between_objects );
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject($object_to_fill, $props_to_skip)
	 */
	public function toObject($object_to_fill = null, $props_to_skip = array())
	{
		if (is_null($object_to_fill))
		{
			throw new KontorolAPIException(KontorolErrors::OBJECT_TYPE_ABSTRACT, "KontorolExportCsvJobData");
		}
		
		return parent::toObject($object_to_fill, $props_to_skip);
	}
}

<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolESearchEntryResult extends KontorolESearchResult
{
	/**
	 * @var KontorolBaseEntry
	 */
	public $object;

	private static $map_between_objects = array(
		'object',
	);

	protected function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}

	protected function doFromObject($srcObj, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$isAdmin = kCurrentContext::$ks_object->isAdmin();
		$object = KontorolEntryFactory::getInstanceByType($srcObj->getObject()->getType(), $isAdmin);
		$object->fromObject($srcObj->getObject(), $responseProfile);
		$this->object = $object;
		return parent::doFromObject($srcObj, $responseProfile);
	}

}

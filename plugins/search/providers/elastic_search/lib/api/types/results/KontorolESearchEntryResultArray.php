<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolESearchEntryResultArray extends KontorolTypedArray
{
    public function __construct()
    {
        return parent::__construct("KontorolESearchEntryResult");
    }

	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$outputArray = new KontorolESearchEntryResultArray();
		foreach ( $arr as $obj )
		{
			$nObj = new KontorolESearchEntryResult();
			$nObj->fromObject($obj, $responseProfile);
			$outputArray[] = $nObj;
		}
		return $outputArray;
	}

}

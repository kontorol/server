<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolESearchUserResponse extends KontorolESearchResponse
{
	/**
	 * @var KontorolESearchUserResultArray
	 * @readonly
	 */
	public $objects;
}

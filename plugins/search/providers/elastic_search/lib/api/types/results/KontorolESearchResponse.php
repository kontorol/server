<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
abstract class KontorolESearchResponse extends KontorolObject
{
    /**
     * @var int
     * @readonly
     */
    public $totalCount;

}

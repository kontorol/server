<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolESearchCategoryResultArray extends KontorolTypedArray
{
    public function __construct()
    {
        return parent::__construct("KontorolESearchCategoryResult");
    }

	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$outputArray = new KontorolESearchCategoryResultArray();
		foreach ( $arr as $obj )
		{
			$nObj = new KontorolESearchCategoryResult();
			$nObj->fromObject($obj, $responseProfile);
			$outputArray[] = $nObj;
		}
		return $outputArray;
	}
}

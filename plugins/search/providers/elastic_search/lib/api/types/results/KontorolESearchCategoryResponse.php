<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolESearchCategoryResponse extends KontorolESearchResponse
{
	/**
	 * @var KontorolESearchCategoryResultArray
	 * @readonly
	 */
	public $objects;
}

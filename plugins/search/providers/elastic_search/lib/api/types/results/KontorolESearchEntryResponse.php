<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolESearchEntryResponse extends KontorolESearchResponse
{
	/**
	 * @var KontorolESearchEntryResultArray
	 * @readonly
	 */
	public $objects;

	/**
	 * @var KontorolESearchAggregationResponseArray
	 * @readonly
	 */
	public $aggregations;
}

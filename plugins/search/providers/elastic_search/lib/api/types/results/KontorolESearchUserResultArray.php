<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.objects
 */
class KontorolESearchUserResultArray extends KontorolTypedArray
{
    public function __construct()
    {
        return parent::__construct("KontorolESearchUserResult");
    }

	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$outputArray = new KontorolESearchUserResultArray();
		foreach ( $arr as $obj )
		{
			$nObj = new KontorolESearchUserResult();
			$nObj->fromObject($obj, $responseProfile);
			$outputArray[] = $nObj;
		}
		return $outputArray;
	}

}

<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.filters
 */
abstract class KontorolESearchBaseFilter extends KontorolObject
{
	private static $mapBetweenObjects = array();

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$mapBetweenObjects);
	}

}

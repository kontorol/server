<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.enum
 */

class KontorolESearchCuePointAggregateByFieldName extends KontorolStringEnum
{
	const TAGS = 'tags';
	const TYPE = 'type';
}

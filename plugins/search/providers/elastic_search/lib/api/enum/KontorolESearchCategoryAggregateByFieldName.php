<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.enum
 */

class KontorolESearchCategoryAggregateByFieldName extends KontorolStringEnum
{
	const CATEGORY_NAME = 'category_name';
}

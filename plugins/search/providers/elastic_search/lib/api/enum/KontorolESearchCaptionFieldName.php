<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.enum
 */
class KontorolESearchCaptionFieldName extends KontorolStringEnum
{
	const CONTENT = 'content';
	const START_TIME = 'start_time';
	const END_TIME = 'end_time';
	const LANGUAGE = 'language';
	const LABEL = 'label';
	const CAPTION_ASSET_ID = 'caption_asset_id';
}

<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.enum
 */

class KontorolESearchEntryAggregateByFieldName extends KontorolStringEnum
{
	const ENTRY_TYPE = 'entry_type';
	const MEDIA_TYPE = 'media_type';
	const TAGS = 'tags';
	const ACCESS_CONTROL_PROFILE = 'access_control_profile_id';
}

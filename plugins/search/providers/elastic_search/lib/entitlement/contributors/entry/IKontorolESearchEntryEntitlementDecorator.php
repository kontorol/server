<?php

/**
 * @package plugins.elasticSearch
 * @subpackage lib.entitlement
 */

interface IKontorolESearchEntryEntitlementDecorator extends IKontorolESearchEntitlementDecorator
{
	public static function applyCondition(&$entryQuery, &$parentEntryQuery);
}

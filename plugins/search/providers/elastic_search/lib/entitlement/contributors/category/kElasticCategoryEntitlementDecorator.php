<?php
/**
 * @package plugins.elasticSearch
 * @subpackage lib.entitlement
 */

abstract class kElasticCategoryEntitlementDecorator implements IKontorolESearchEntitlementDecorator
{
	public static function shouldContribute()
	{
		return kEntitlementUtils::getEntitlementEnforcement();
	}
}

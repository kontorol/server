<?php

/**
 * @package plugins.elasticSearch
 * @subpackage lib.entitlement
 */

interface IKontorolESearchEntitlementDecorator
{
	public static function shouldContribute();
	public static function getEntitlementCondition(array $params = array(), $fieldPrefix ='');
}

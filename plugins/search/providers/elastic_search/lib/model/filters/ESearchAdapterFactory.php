<?php
/**
 * @package plugins.elasticSearch
 * @subpackage model.filters
 */

class ESearchAdapterFactory
{
	/**
	 * @param KontorolRelatedFilter $filter
	 * @return ESearchQueryFromFilter
	 */
	public static function getAdapter(KontorolRelatedFilter $filter)
	{
		if($filter instanceof KontorolBaseEntryBaseFilter)
		{
			return new ESearchEntryQueryFromFilter();
		}

		if($filter instanceof KontorolCaptionAssetBaseFilter)
		{
			return new ESearchCaptionQueryFromFilter();
		}

		return null;
	}
}

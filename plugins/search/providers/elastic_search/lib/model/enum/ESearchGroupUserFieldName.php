<?php
/**
 * @package plugins.elasticSearch
 * @subpackage api.enum
 */
class ESearchGroupUserFieldName extends KontorolStringEnum
{
	const GROUP_USER_DATA = 'group_user_data';
}

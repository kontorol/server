<?php
/**
 * @package plugins.elasticSearch
 * @subpackage model.items
 */
class ESearchCategoryEntryItemFactory
{

	public static function getCoreItemByFieldName($fieldName)
	{
		switch ($fieldName)
		{
			case KontorolESearchCategoryEntryFieldName::ID:
				return new ESearchCategoryEntryIdItem();
			case KontorolESearchCategoryEntryFieldName::NAME:
				return new ESearchCategoryEntryNameItem();
			case KontorolESearchCategoryEntryFieldName::FULL_IDS:
				return new ESearchCategoryEntryFullIdsItem();
			case KontorolESearchCategoryEntryFieldName::ANCESTOR_ID:
				return new ESearchCategoryEntryAncestorIdItem();
			case KontorolESearchCategoryEntryFieldName::ANCESTOR_NAME:
				return new ESearchCategoryEntryAncestorNameItem();
			default:
				KontorolLog::err("Unknown field name $fieldName in ESearchCategoryEntryItemFactory");
				return null;
		}
	}

}

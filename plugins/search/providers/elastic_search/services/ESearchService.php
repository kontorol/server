<?php
/**
 * @service eSearch
 * @package plugins.elasticSearch
 * @subpackage api.services
 */
class ESearchService extends KontorolBaseService
{
	const MAX_RESULT_WINDOW = 10000;
	/**
	 *
	 * @action searchEntry
	 * @param KontorolESearchEntryParams $searchParams
	 * @param KontorolPager $pager
	 * @return KontorolESearchEntryResponse
	 */
	function searchEntryAction(KontorolESearchEntryParams $searchParams, KontorolPager $pager = null)
	{
		$entrySearch = new kEntrySearch();
		list($coreResults, $objectCount, $aggregations) = $this->initAndSearch($entrySearch, $searchParams, $pager);
		$response = new KontorolESearchEntryResponse();
		$response->objects = KontorolESearchEntryResultArray::fromDbArray($coreResults, $this->getResponseProfile());
		$response->totalCount = $objectCount;
		if($aggregations)
		{
			$aggregationResponse = new KontorolESearchAggregationResponse();
			$response->aggregations = $aggregationResponse->resultToApi($aggregations);
		}
		return $response;
	}

	/**
	 *
	 * @action searchCategory
	 * @param KontorolESearchCategoryParams $searchParams
	 * @param KontorolPager $pager
	 * @return KontorolESearchCategoryResponse
	 */
	function searchCategoryAction(KontorolESearchCategoryParams $searchParams, KontorolPager $pager = null)
	{
		$categorySearch = new kCategorySearch();
		list($coreResults, $objectCount) = $this->initAndSearch($categorySearch, $searchParams, $pager);
		$response = new KontorolESearchCategoryResponse();
		$response->objects = KontorolESearchCategoryResultArray::fromDbArray($coreResults, $this->getResponseProfile());
		$response->totalCount = $objectCount;
		return $response;
	}

	/**
	 *
	 * @action searchUser
	 * @param KontorolESearchUserParams $searchParams
	 * @param KontorolPager $pager
	 * @return KontorolESearchUserResponse
	 */
	function searchUserAction(KontorolESearchUserParams $searchParams, KontorolPager $pager = null)
	{
		$userSearch = new kUserSearch();
		list($coreResults, $objectCount) = $this->initAndSearch($userSearch, $searchParams, $pager);
		$response = new KontorolESearchUserResponse();
		$response->objects = KontorolESearchUserResultArray::fromDbArray($coreResults, $this->getResponseProfile());
		$response->totalCount = $objectCount;
		return $response;
	}

	/**
	 * Creates a batch job that sends an email with a link to download a CSV containing a list of entries
	 *
	 * @action entryExportToCsv
	 * @actionAlias media.exportToCsv
	 * @param KontorolMediaEsearchExportToCsvJobData $data job data indicating filter to pass to the job
	 * @return string
	 *
	 * @throws APIErrors::USER_EMAIL_NOT_FOUND
	 */
	public function entryExportToCsvAction (KontorolMediaEsearchExportToCsvJobData $data)
	{
		if(!$data->userName || !$data->userMail)
			throw new KontorolAPIException(APIErrors::USER_EMAIL_NOT_FOUND, '');
		
		$kJobdData = $data->toObject(new kMediaEsearchExportToCsvJobData());
		
		kJobsManager::addExportCsvJob($kJobdData, $this->getPartnerId(), ElasticSearchPlugin::getExportTypeCoreValue(EsearchMediaEntryExportObjectType::ESEARCH_MEDIA));
		
		return $data->userMail;
	}

	/**
	 * @param kBaseSearch $coreSearchObject
	 * @param $searchParams
	 * @param $pager
	 * @return array
	 */
	protected function initAndSearch($coreSearchObject, $searchParams, $pager)
	{
		list($coreSearchOperator, $objectStatusesArr, $objectId, $kPager, $coreOrder, $aggregations) =
			self::initSearchActionParams($searchParams, $pager);
		$elasticResults = $coreSearchObject->doSearch($coreSearchOperator, $kPager, $objectStatusesArr, $objectId, $coreOrder, $aggregations);

		list($coreResults, $objectCount, $aggregationsResult) = kESearchCoreAdapter::transformElasticToCoreObject($elasticResults, $coreSearchObject);
		return array($coreResults, $objectCount, $aggregationsResult);
	}

	protected static function initSearchActionParams($searchParams, KontorolPager $pager = null)
	{
		/**
		 * @var ESearchParams $coreParams
		 */
		$coreParams = $searchParams->toObject();

		$objectStatusesArr = array();
		$objectStatuses = $coreParams->getObjectStatuses();
		if (!empty($objectStatuses))
		{
			$objectStatusesArr = explode(',', $objectStatuses);
		}

		$kPager = null;
		if ($pager)
		{
			/* @var kPager $kPager */
			$kPager = $pager->toObject();
			if( ($kPager->getPageSize() * $kPager->getPageIndex()) > self::MAX_RESULT_WINDOW )
			{
				throw new KontorolAPIException(KontorolESearchErrors::CRITERIA_EXCEEDED_MAX_MATCHES_ALLOWED);
			}
		}

		return array($coreParams->getSearchOperator(), $objectStatusesArr, $coreParams->getObjectId(), $kPager, $coreParams->getOrderBy(), $coreParams->getAggregations());
	}

}

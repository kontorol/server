<?php
/**
 * @package plugins
 * @subpackage api
 */
class KontorolPluginReplacementOptionsArray extends KontorolTypedArray
{
	public function __construct( )
	{
		return parent::__construct ("KontorolPluginReplacementOptionsItem");
	}
}

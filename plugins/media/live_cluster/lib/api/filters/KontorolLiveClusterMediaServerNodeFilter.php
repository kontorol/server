<?php
/**
 * @package plugins.liveCluster
 * @subpackage api.filters
 */

class KontorolLiveClusterMediaServerNodeFilter extends KontorolLiveClusterMediaServerNodeBaseFilter
{
    public function getTypeListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null, $type = null)
    {
        if (!$type)
        {
            $type = LiveClusterPlugin::getLiveClusterMediaServerTypeCoreValue(LiveClusterMediaServerNodeType::LIVE_CLUSTER_MEDIA_SERVER);
        }

        return parent::getTypeListResponse($pager, $responseProfile, $type);
    }
}

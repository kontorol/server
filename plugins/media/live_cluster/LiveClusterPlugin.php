<?php
/**
 * Enable using the new live cluster
 * @package plugins.liveCluster
 */
class LiveClusterPlugin extends KontorolPlugin implements IKontorolObjectLoader, IKontorolEnumerator
{
    const PLUGIN_NAME = 'liveCluster';

    /* (non-PHPdoc)
     * @see IKontorolPlugin::getPluginName()
     */
    public static function getPluginName()
    {
        return self::PLUGIN_NAME;
    }

    /* (non-PHPdoc)
     * @see IKontorolEnumerator::getEnums()
     */
    public static function getEnums($baseEnumName = null)
    {
        if (is_null($baseEnumName) || $baseEnumName === 'serverNodeType')
        {
            return array('LiveClusterMediaServerNodeType');
        }
        return array();
    }

    /* (non-PHPdoc)
     * @see IKontorolObjectLoader::loadObject()
     */
    public static function loadObject($baseClass, $enumValue, array $constructorArgs = null)
    {
        $class = self::getObjectClass($baseClass, $enumValue);
        if ($class && class_exists($class))
        {
            return new $class();
        }
    }

    /* (non-PHPdoc)
     * @see IKontorolObjectLoader::getObjectClass()
     */
    public static function getObjectClass($baseClass, $enumValue)
    {
        if ($baseClass === 'ServerNode' && $enumValue == self::getLiveClusterMediaServerTypeCoreValue(LiveClusterMediaServerNodeType::LIVE_CLUSTER_MEDIA_SERVER))
        {
            return 'LiveClusterMediaServerNode';
        }

        if ($baseClass === 'KontorolServerNode' && $enumValue == self::getLiveClusterMediaServerTypeCoreValue(LiveClusterMediaServerNodeType::LIVE_CLUSTER_MEDIA_SERVER))
        {
            return 'KontorolLiveClusterMediaServerNode';
        }
    }

    /* (non-PHPdoc)
     * @see IKontorolCuePoint::getCuePointTypeCoreValue()
     */
    public static function getLiveClusterMediaServerTypeCoreValue($valueName)
    {
        $value = self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
        return kPluginableEnumsManager::apiToCore('serverNodeType', $value);
    }
}

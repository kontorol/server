<?php
/**
 * @package plugins.wowza
 * @subpackage api.filters
 */
class KontorolWowzaMediaServerNodeFilter extends KontorolWowzaMediaServerNodeBaseFilter
{
	public function getTypeListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null, $type = null)
	{
		if(!$type)
			$type = WowzaPlugin::getWowzaMediaServerTypeCoreValue(WowzaMediaServerNodeType::WOWZA_MEDIA_SERVER);
	
		return parent::getTypeListResponse($pager, $responseProfile, $type);
	}
}

<?php
/**
 * @package plugins.wowza
 * @subpackage errors
 */
class WowzaErrors extends KontorolErrors
{
	const INVALID_STREAM_NAME = "INVALID_STREAM_NAME;STREAM_NAME;Invalid stream name [@STREAM_NAME@]";
}

<?php
/**
 * @package plugins.fileSync
 * @subpackage api.objects
 */
class KontorolFileSyncListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolFileSyncArray
	 * @readonly
	 */
	public $objects;
}

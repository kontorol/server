<?php
/**
 * @package plugins.fileSync
 * @subpackage api.objects
 */
class KontorolFileSyncArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolFileSyncArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
			$nObj = new KontorolFileSync();
			try
			{
				$nObj->fromObject($obj, $responseProfile);
			}
			catch(kFileSyncException $e)
			{
				continue;
			}
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolFileSync");
	}
}

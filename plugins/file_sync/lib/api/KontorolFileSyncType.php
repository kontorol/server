<?php
/**
 * @package plugins.fileSync
 * @subpackage api.enum
 */
class KontorolFileSyncType extends KontorolEnum
{
	const FILE = 1;
	const LINK = 2;
	const URL = 3;
}

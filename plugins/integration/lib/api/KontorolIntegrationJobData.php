<?php
/**
 * @package plugins.integration
 * @subpackage api.objects
 */
class KontorolIntegrationJobData extends KontorolJobData
{
	/**
	 * @var string
	 * @readonly
	 */
	public $callbackNotificationUrl;
	
	/**
	 * @var KontorolIntegrationProviderType
	 */
	public $providerType;

	/**
	 * Additional data that relevant for the provider only
	 * @var KontorolIntegrationJobProviderData
	 */
	public $providerData;

	/**
	 * @var KontorolIntegrationTriggerType
	 */
	public $triggerType;

	/**
	 * Additional data that relevant for the trigger only
	 * @var KontorolIntegrationJobTriggerData
	 */
	public $triggerData;
	
	private static $map_between_objects = array
	(
		"callbackNotificationUrl",
		"providerType" ,
		"triggerType" ,
	);

	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects ( )
	{
		return array_merge ( parent::getMapBetweenObjects() , self::$map_between_objects );
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::fromObject($srcObj)
	 */
	public function doFromObject($sourceObject, KontorolDetachedResponseProfile $responseProfile = null)
	{
		parent::doFromObject($sourceObject, $responseProfile);
		
		$providerType = $sourceObject->getProviderType();
		$this->providerData = KontorolPluginManager::loadObject('KontorolIntegrationJobProviderData', $providerType);
		$providerData = $sourceObject->getProviderData();
		if($this->providerData && $providerData && $providerData instanceof kIntegrationJobProviderData)
			$this->providerData->fromObject($providerData);
			
		$triggerType = $sourceObject->getTriggerType();
		$this->triggerData = KontorolPluginManager::loadObject('KontorolIntegrationJobTriggerData', $triggerType);
		$triggerData = $sourceObject->getTriggerData();
		if($this->triggerData && $triggerData && $triggerData instanceof kIntegrationJobTriggerData)
			$this->triggerData->fromObject($triggerData);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject($object_to_fill, $props_to_skip)
	 */
	public function toObject($object = null, $skip = array())
	{
		if(is_null($object))
		{
			$object = new kIntegrationJobData();
		} 
		$object = parent::toObject($object, $skip);
				
		if($this->providerType && $this->providerData && $this->providerData instanceof KontorolIntegrationJobProviderData)
		{
			$providerData = KontorolPluginManager::loadObject('kIntegrationJobProviderData', $this->providerType);
			if($providerData)
			{
				$providerData = $this->providerData->toObject($providerData);
				$object->setProviderData($providerData);
			}
		}
		
		if($this->triggerType && $this->triggerData && $this->triggerData instanceof KontorolIntegrationJobTriggerData)
		{
			$triggerData = KontorolPluginManager::loadObject('kIntegrationJobTriggerData', $this->triggerType);
			if($triggerData)
			{
				$triggerData = $this->triggerData->toObject($triggerData);
				$object->setTriggerData($triggerData);
			}
		}
		
		return $object;
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::validateForUsage($sourceObject, $propertiesToSkip)
	 */
	public function validateForUsage($sourceObject, $propertiesToSkip = array())
	{
		parent::validateForUsage($sourceObject, $propertiesToSkip);
		
		$this->validatePropertyNotNull('providerType');
		$this->validatePropertyNotNull('providerData');
		$this->validatePropertyNotNull('triggerType');
		
		if ($this->triggerType != KontorolIntegrationTriggerType::MANUAL)
			$this->validatePropertyNotNull('triggerData');
	}
	
	/* (non-PHPdoc)
	 * @see KontorolJobData::toSubType()
	 */
	public function toSubType($subType)
	{
		return kPluginableEnumsManager::apiToCore('IntegrationProviderType', $subType);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolJobData::fromSubType()
	 */
	public function fromSubType($subType)
	{
		return kPluginableEnumsManager::coreToApi('IntegrationProviderType', $subType);
	}
}

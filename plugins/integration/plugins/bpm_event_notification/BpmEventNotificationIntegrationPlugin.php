<?php
/**
 * @package plugins.bpmEventNotificationIntegration
 */
class BpmEventNotificationIntegrationPlugin extends KontorolPlugin implements IKontorolEnumerator, IKontorolPending, IKontorolObjectLoader, IKontorolEventConsumers
{
	const PLUGIN_NAME = 'bpmEventNotificationIntegration';
	const FLOW_MANAGER = 'kBpmEventNotificationIntegrationFlowManager';
	
	const INTEGRATION_PLUGIN_VERSION_MAJOR = 1;
	const INTEGRATION_PLUGIN_VERSION_MINOR = 0;
	const INTEGRATION_PLUGIN_VERSION_BUILD = 0;
	
	const BPM_PLUGIN_VERSION_MAJOR = 1;
	const BPM_PLUGIN_VERSION_MINOR = 0;
	const BPM_PLUGIN_VERSION_BUILD = 0;

	/* (non-PHPdoc)
	 * @see IKontorolPlugin::getPluginName()
	 */
	public static function getPluginName()
	{
		return self::PLUGIN_NAME;
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolPending::dependsOn()
	 */
	public static function dependsOn()
	{
		$integrationVersion = new KontorolVersion(
			self::INTEGRATION_PLUGIN_VERSION_MAJOR,
			self::INTEGRATION_PLUGIN_VERSION_MINOR,
			self::INTEGRATION_PLUGIN_VERSION_BUILD
		);
		$integrationDependency = new KontorolDependency(IntegrationPlugin::getPluginName(), $integrationVersion);
		
		$bpmVersion = new KontorolVersion(
			self::BPM_PLUGIN_VERSION_MAJOR,
			self::BPM_PLUGIN_VERSION_MINOR,
			self::BPM_PLUGIN_VERSION_BUILD
		);
		$bpmDependency = new KontorolDependency(BusinessProcessNotificationPlugin::getPluginName(), $bpmVersion);
		
		return array($integrationDependency, $bpmDependency);
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolEventConsumers::getEventConsumers()
	 */
	public static function getEventConsumers()
	{
		return array(
			self::FLOW_MANAGER,
		);
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolEnumerator::getEnums()
	 */
	public static function getEnums($baseEnumName = null)
	{
		if(is_null($baseEnumName))
			return array('BpmEventNotificationIntegrationTrigger');
	
		if($baseEnumName == 'IntegrationTriggerType')
			return array('BpmEventNotificationIntegrationTrigger');
			
		return array();
	}

	/* (non-PHPdoc)
	 * @see IKontorolObjectLoader::loadObject()
	 */
	public static function loadObject($baseClass, $enumValue, array $constructorArgs = null)
	{			
		$objectClass = self::getObjectClass($baseClass, $enumValue);
		if (is_null($objectClass)) 
		{
			return null;
		}
		
		if (!is_null($constructorArgs))
		{
			$reflect = new ReflectionClass($objectClass);
			return $reflect->newInstanceArgs($constructorArgs);
		}
		else
		{
			return new $objectClass();
		}
	}

	/* (non-PHPdoc)
	 * @see IKontorolObjectLoader::getObjectClass()
	 */
	public static function getObjectClass($baseClass, $enumValue)
	{
		if($baseClass == 'kIntegrationJobTriggerData' && $enumValue == self::getApiValue(BpmEventNotificationIntegrationTrigger::BPM_EVENT_NOTIFICATION))
		{
			return 'kBpmEventNotificationIntegrationJobTriggerData';
		}
	
		if($baseClass == 'KontorolIntegrationJobTriggerData')
		{
			if($enumValue == self::getApiValue(BpmEventNotificationIntegrationTrigger::BPM_EVENT_NOTIFICATION) || $enumValue == self::getIntegrationTriggerCoreValue(BpmEventNotificationIntegrationTrigger::BPM_EVENT_NOTIFICATION))
				return 'KontorolBpmEventNotificationIntegrationJobTriggerData';
		}
	}

	/**
	 * @return int id of dynamic enum in the DB.
	 */
	public static function getIntegrationTriggerCoreValue($valueName)
	{
		$value = self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
		return kPluginableEnumsManager::apiToCore('IntegrationTriggerType', $value);
	}
	
	/**
	 * @return string external API value of dynamic enum.
	 */
	public static function getApiValue($valueName)
	{
		return self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
	}
}

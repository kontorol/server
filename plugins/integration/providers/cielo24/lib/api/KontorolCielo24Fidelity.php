<?php
/**
 * @package plugins.cielo24
 * @subpackage api.enum
 */
class KontorolCielo24Fidelity extends KontorolStringEnum
{
	const MECHANICAL = "MECHANICAL";
	const PREMIUM = "PREMIUM";
	const PROFESSIONAL = "PROFESSIONAL";
}

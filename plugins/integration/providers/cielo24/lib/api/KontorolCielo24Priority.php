<?php
/**
 * @package plugins.cielo24
 * @subpackage api.enum
 */
class KontorolCielo24Priority extends KontorolStringEnum
{
	const STANDARD = "STANDARD";
	const PRIORITY = "PRIORITY";
}

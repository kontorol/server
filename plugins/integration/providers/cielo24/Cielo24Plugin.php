<?php
/**
 * @package plugins.cielo24
 */
class Cielo24Plugin extends IntegrationProviderPlugin implements IKontorolEventConsumers
{
	const PLUGIN_NAME = 'cielo24';
	const FLOW_MANAGER = 'kCielo24FlowManager';
	
	const INTEGRATION_PLUGIN_VERSION_MAJOR = 1;
	const INTEGRATION_PLUGIN_VERSION_MINOR = 0;
	const INTEGRATION_PLUGIN_VERSION_BUILD = 0;
	
	const TRANSCRIPT_PLUGIN_VERSION_MAJOR = 1;
	const TRANSCRIPT_PLUGIN_VERSION_MINOR = 0;
	const TRANSCRIPT_PLUGIN_VERSION_BUILD = 0;
	
	/* (non-PHPdoc)
	 * @see IKontorolPlugin::getPluginName()
	 */
	public static function getPluginName()
	{
		return self::PLUGIN_NAME;
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolPending::dependsOn()
	 */
	public static function dependsOn()
	{
		$transcriptVersion = new KontorolVersion(
			self::TRANSCRIPT_PLUGIN_VERSION_MAJOR,
			self::TRANSCRIPT_PLUGIN_VERSION_MINOR,
			self::TRANSCRIPT_PLUGIN_VERSION_BUILD
		);
		$transcriptDependency = new KontorolDependency(TranscriptPlugin::getPluginName(), $transcriptVersion);

		return array_merge(parent::dependsOn(), array($transcriptDependency));
	}
	
	/* (non-PHPdoc)
	 * @see IntegrationProviderPlugin::getRequiredIntegrationPluginVersion()
	 */
	public static function getRequiredIntegrationPluginVersion()
	{
		return new KontorolVersion(
			self::INTEGRATION_PLUGIN_VERSION_MAJOR,
			self::INTEGRATION_PLUGIN_VERSION_MINOR,
			self::INTEGRATION_PLUGIN_VERSION_BUILD
		);
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolEventConsumers::getEventConsumers()
	 */
	public static function getEventConsumers()
	{
		return array(
			self::FLOW_MANAGER,
		);
	}
	
	/* (non-PHPdoc)
	 * @see IntegrationProviderPlugin::getIntegrationProviderClassName()
	 */
	public static function getIntegrationProviderClassName()
	{
		return 'Cielo24IntegrationProviderType';
	}
	
	/*
	 * @return IIntegrationProvider
	 */
	public function getProvider()
	{
		return new IntegrationCielo24Provider(); 
	}
	
	
	/* (non-PHPdoc)
	 * @see IKontorolObjectLoader::getObjectClass()
	 */
	public static function getObjectClass($baseClass, $enumValue)
	{
		if($baseClass == 'kIntegrationJobProviderData' && $enumValue == self::getApiValue(Cielo24IntegrationProviderType::CIELO24))
		{
			return 'kCielo24JobProviderData';
		}
	
		if($baseClass == 'KontorolIntegrationJobProviderData')
		{
			if($enumValue == self::getApiValue(Cielo24IntegrationProviderType::CIELO24) || $enumValue == self::getIntegrationProviderCoreValue(Cielo24IntegrationProviderType::CIELO24))
				return 'KontorolCielo24JobProviderData';
		}
	
		if($baseClass == 'KIntegrationEngine' || $baseClass == 'KIntegrationCloserEngine')
		{
			if($enumValue == KontorolIntegrationProviderType::CIELO24)
				return 'KCielo24IntegrationEngine';
		}
		if($baseClass == 'IIntegrationProvider' && $enumValue == self::getIntegrationProviderCoreValue(Cielo24IntegrationProviderType::CIELO24))
		{
			return 'IntegrationCielo24Provider';
		}
	}
	
	/**
	 * @return int id of dynamic enum in the DB.
	 */
	public static function getProviderTypeCoreValue($valueName)
	{
		$value = self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
		return kPluginableEnumsManager::apiToCore('IntegrationProviderType', $value);
	}
	
	/**
	 * @return int id of dynamic enum in the DB.
	 */
	public static function getTranscriptProviderTypeCoreValue($valueName)
	{
		$value = self::getPluginName() . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . $valueName;
		return kPluginableEnumsManager::apiToCore('TranscriptProviderType', $value);
	}
	
	public static function getClientHelper($username, $password, $baseUrl = null, $additionalParams = array())
	{
		return new Cielo24ClientHelper($username, $password, $baseUrl, $additionalParams);
	}
	
	/**
	 * @return Cielo24Options
	 */	
	public static function getPartnerCielo24Options($partnerId)
	{
		$partner = PartnerPeer::retrieveByPK($partnerId);
		if(!$partner)
			return null;
		return $partner->getFromCustomData(Cielo24IntegrationProviderType::CIELO24);
	}
	
	public static function setPartnerCielo24Options($partnerId, Cielo24Options $options)
	{
		$partner = PartnerPeer::retrieveByPK($partnerId);
		if(!$partner)
			return;
		$partner->putInCustomData(Cielo24IntegrationProviderType::CIELO24, $options);
		$partner->save();
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolEnumerator::getEnums()
	 */
	public static function getEnums($baseEnumName = null)
	{
		$enums = parent::getEnums($baseEnumName);
		
		if (is_null ($baseEnumName))
		{
			$enums[] = 'Cielo24TranscriptProviderType';
			return $enums;
		}
		
		if ($baseEnumName == 'TranscriptProviderType')
		{
			return array ('Cielo24TranscriptProviderType');
		}
		
		return $enums;
	}
}

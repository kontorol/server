<?php
/**
 * @package plugins.exampleIntegration
 * @subpackage Scheduler
 */
class KExampleIntegrationEngine implements KIntegrationCloserEngine
{	
	/* (non-PHPdoc)
	 * @see KIntegrationCloserEngine::dispatch()
	 */
	public function dispatch(KontorolBatchJob $job, KontorolIntegrationJobData &$data)
	{
		return $this->doDispatch($job, $data, $data->providerData);
	}

	/* (non-PHPdoc)
	 * @see KIntegrationCloserEngine::close()
	 */
	public function close(KontorolBatchJob $job, KontorolIntegrationJobData &$data)
	{
		return $this->doClose($job, $data, $data->providerData);
	}
	
	protected function doDispatch(KontorolBatchJob $job, KontorolIntegrationJobData &$data, KontorolExampleIntegrationJobProviderData $providerData)
	{
		KontorolLog::debug("Example URL [$providerData->exampleUrl]");
		
		// To finish, return true
		// To wait for closer, return false
		// To fail, throw exception
		
		return false;
	}
	
	protected function doClose(KontorolBatchJob $job, KontorolIntegrationJobData &$data, KontorolExampleIntegrationJobProviderData $providerData)
	{
		KontorolLog::debug("Example URL [$providerData->exampleUrl]");
		
		// To finish, return true
		// To keep open for future closer, return false
		// To fail, throw exception
		
		return true;
	}
}

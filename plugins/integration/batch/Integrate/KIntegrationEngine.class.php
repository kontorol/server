<?php
/**
 * @package plugins.integration
 * @subpackage Scheduler
 */
interface KIntegrationEngine
{	
	/**
	 * @param KontorolBatchJob $job
	 * @param KontorolIntegrationJobData $data
	 */
	public function dispatch(KontorolBatchJob $job, KontorolIntegrationJobData &$data);
}

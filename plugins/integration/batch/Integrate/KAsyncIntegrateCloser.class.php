<?php
/**
 * @package plugins.integration
 * @subpackage Scheduler
 */
class KAsyncIntegrateCloser extends KJobCloserWorker
{
	/* (non-PHPdoc)
	 * @see KBatchBase::getType()
	 */
	public static function getType()
	{
		return KontorolBatchJobType::INTEGRATION;
	}
	
	/* (non-PHPdoc)
	 * @see KJobHandlerWorker::exec()
	 */
	protected function exec(KontorolBatchJob $job)
	{
		return $this->close($job, $job->data);
	}
	
	protected function close(KontorolBatchJob $job, KontorolIntegrationJobData $data)
	{
		if(($job->queueTime + self::$taskConfig->params->maxTimeBeforeFail) < time())
		{
			return $this->closeJob($job, KontorolBatchJobErrorTypes::APP, KontorolBatchJobAppErrors::CLOSER_TIMEOUT, 'Timed out', KontorolBatchJobStatus::FAILED);
		}
		
		$engine = $this->getEngine($job->jobSubType);
		if(!$engine)
		{
			return $this->closeJob($job, KontorolBatchJobErrorTypes::APP, KontorolBatchJobAppErrors::ENGINE_NOT_FOUND, "Engine not found", KontorolBatchJobStatus::FAILED);
		}
		
		$this->impersonate($job->partnerId);
		$finished = $engine->close($job, $data);
		$this->unimpersonate();
		
		if(!$finished)
		{
			return $this->closeJob($job, null, null, null, KontorolBatchJobStatus::ALMOST_DONE, $data);
		}
		
		return $this->closeJob($job, null, null, "Integrated", KontorolBatchJobStatus::FINISHED, $data);
	}

	/**
	 * @param KontorolIntegrationProviderType $type
	 * @return KIntegrationCloserEngine
	 */
	protected function getEngine($type)
	{
		return KontorolPluginManager::loadObject('KIntegrationCloserEngine', $type);
	}
}

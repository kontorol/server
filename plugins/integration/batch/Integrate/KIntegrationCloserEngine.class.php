<?php
/**
 * @package plugins.integration
 * @subpackage Scheduler
 */
interface KIntegrationCloserEngine extends KIntegrationEngine
{	
	/**
	 * @param KontorolBatchJob $job
	 * @param KontorolIntegrationJobData $data
	 */
	public function close(KontorolBatchJob $job, KontorolIntegrationJobData &$data);
}

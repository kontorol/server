<?php
/**
 * @package plugins.integration
 * @subpackage Scheduler
 */
class KAsyncIntegrate extends KJobHandlerWorker
{
	/* (non-PHPdoc)
	 * @see KBatchBase::getType()
	 */
	public static function getType()
	{
		return KontorolBatchJobType::INTEGRATION;
	}
	
	/* (non-PHPdoc)
	 * @see KJobHandlerWorker::exec()
	 */
	protected function exec(KontorolBatchJob $job)
	{
		return $this->integrate($job, $job->data);
	}
	
	protected function integrate(KontorolBatchJob $job, KontorolIntegrationJobData $data)
	{
		$engine = $this->getEngine($job->jobSubType);
		if(!$engine)
		{
			return $this->closeJob($job, KontorolBatchJobErrorTypes::APP, KontorolBatchJobAppErrors::ENGINE_NOT_FOUND, "Engine not found", KontorolBatchJobStatus::FAILED);
		}
		
		$this->impersonate($job->partnerId);
		$finished = $engine->dispatch($job, $data);
		$this->unimpersonate();
		
		if(!$finished)
		{
			return $this->closeJob($job, null, null, null, KontorolBatchJobStatus::ALMOST_DONE, $data);
		}
		
		return $this->closeJob($job, null, null, "Integrated", KontorolBatchJobStatus::FINISHED, $data);
	}

	/**
	 * @param KontorolIntegrationProviderType $type
	 * @return KIntegrationEngine
	 */
	protected function getEngine($type)
	{
		return KontorolPluginManager::loadObject('KIntegrationEngine', $type);
	}
}

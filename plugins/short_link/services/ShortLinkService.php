<?php
/**
 * Short link service
 *
 * @service shortLink
 * @package plugins.shortLink
 * @subpackage api.services
 */
class ShortLinkService extends KontorolBaseService
{
	protected function partnerRequired($actionName)
	{
		if ($actionName === 'goto')
			return false;
			
		return parent::partnerRequired($actionName);
	}
	
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);

		if($actionName != 'goto')
		{
			$this->applyPartnerFilterForClass('ShortLink');
			$this->applyPartnerFilterForClass('kuser');
		}
	}
	
	/**
	 * List short link objects by filter and pager
	 * 
	 * @action list
	 * @param KontorolShortLinkFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return KontorolShortLinkListResponse
	 */
	function listAction(KontorolShortLinkFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
			$filter = new KontorolShortLinkFilter;
			
		$shortLinkFilter = $filter->toFilter($this->getPartnerId());
		
		$c = new Criteria();
		$shortLinkFilter->attachToCriteria($c);
		$count = ShortLinkPeer::doCount($c);
		
		if (! $pager)
			$pager = new KontorolFilterPager ();
		$pager->attachToCriteria ( $c );
		$list = ShortLinkPeer::doSelect($c);
		
		$response = new KontorolShortLinkListResponse();
		$response->objects = KontorolShortLinkArray::fromDbArray($list, $this->getResponseProfile());
		$response->totalCount = $count;
		
		return $response;
	}
	
	/**
	 * Allows you to add a short link object
	 * 
	 * @action add
	 * @param KontorolShortLink $shortLink
	 * @return KontorolShortLink
	 */
	function addAction(KontorolShortLink $shortLink)
	{
		$shortLink->validatePropertyNotNull('systemName');
		$shortLink->validatePropertyMinLength('systemName', 3);
		$shortLink->validatePropertyNotNull('fullUrl');
		$shortLink->validatePropertyMinLength('fullUrl', 10);
		
		if(!$shortLink->status)
			$shortLink->status = KontorolShortLinkStatus::ENABLED;
			
		if(!$shortLink->userId)
			$shortLink->userId = $this->getKuser()->getPuserId();
			
		$dbShortLink = new ShortLink();
		$dbShortLink = $shortLink->toInsertableObject($dbShortLink, array('userId'));
		$dbShortLink->setPartnerId($this->getPartnerId());
		$dbShortLink->setPuserId(is_null($shortLink->userId) ? $this->getKuser()->getPuserId() : $shortLink->userId);
		$dbShortLink->save();
		
		$shortLink = new KontorolShortLink();
		$shortLink->fromObject($dbShortLink, $this->getResponseProfile());
		
		return $shortLink;
	}
	
	/**
	 * Retrieve an short link object by id
	 * 
	 * @action get
	 * @param string $id 
	 * @return KontorolShortLink
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 */		
	function getAction($id)
	{
		$dbShortLink = ShortLinkPeer::retrieveByPK($id);
		
		if(!$dbShortLink)
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $id);
			
		$shortLink = new KontorolShortLink();
		$shortLink->fromObject($dbShortLink, $this->getResponseProfile());
		
		return $shortLink;
	}


	/**
	 * Update existing short link
	 * 
	 * @action update
	 * @param string $id
	 * @param KontorolShortLink $shortLink
	 * @return KontorolShortLink
	 *
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 */	
	function updateAction($id, KontorolShortLink $shortLink)
	{
		$dbShortLink = ShortLinkPeer::retrieveByPK($id);
	
		if (!$dbShortLink)
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $id);
		
		$dbShortLink = $shortLink->toUpdatableObject($dbShortLink);
		$dbShortLink->save();
	
		$shortLink->fromObject($dbShortLink, $this->getResponseProfile());
		
		return $shortLink;
	}

	/**
	 * Mark the short link as deleted
	 * 
	 * @action delete
	 * @param string $id 
	 * @return KontorolShortLink
	 *
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 */		
	function deleteAction($id)
	{
		$dbShortLink = ShortLinkPeer::retrieveByPK($id);
	
		if (!$dbShortLink)
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $id);
		
		$dbShortLink->setStatus(KontorolShortLinkStatus::DELETED);
		$dbShortLink->save();
			
		$shortLink = new KontorolShortLink();
		$shortLink->fromObject($dbShortLink, $this->getResponseProfile());
		
		return $shortLink;
	}

	/**
	 * Serves short link
	 * 
	 * @action goto
	 * @param string $id
	 * @param bool $proxy proxy the response instead of redirect
	 * @return file
	 * @ksIgnored
	 * 
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 */		
	function gotoAction($id, $proxy = false)
	{
		KontorolResponseCacher::disableCache();
		
		$dbShortLink = ShortLinkPeer::retrieveByPK($id);
	
		if (!$dbShortLink)
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $id);

		if($proxy)
			kFileUtils::dumpUrl($dbShortLink->getFullUrl(), true, true);
		
		header('Location: ' . $dbShortLink->getFullUrl());
		die;
	}
}

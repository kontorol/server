<?php
/**
 * @package plugins.shortLink
 * @subpackage api.objects
 */
class KontorolShortLinkListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolShortLinkArray
	 * @readonly
	 */
	public $objects;
}

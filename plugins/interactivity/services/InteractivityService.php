<?php
/**
 * @service interactivity
 * @package plugins.interactivity
 * @subpackage api.services
 */

class InteractivityService extends KontorolBaseService
{
	/**
	 * Retrieve a interactivity object by entry id
	 *
	 * @action get
	 * @param string $entryId
	 * @param KontorolInteractivityDataFilter $dataFilter
	 * @return KontorolInteractivity
	 * @throws kCoreException
	 * @throws KontorolAPIException
	 */
	public function get($entryId, $dataFilter = null)
	{
		$kInteractivity = new kInteractivity();
		$kInteractivity->setEntry($entryId);
		$kontorolInteractivity = new KontorolInteractivity();
		$kontorolInteractivity->fromObject($kInteractivity, $this->getResponseProfile());
		if($dataFilter)
		{
			$kDataFilter = $dataFilter->toObject();
			/* @var $kDataFilter kInteractivityDataFilter */
			$kontorolInteractivity->data = $kDataFilter->filterData($kontorolInteractivity->data);
		}

		return $kontorolInteractivity;
	}

	/**
	 * Update an existing interactivity object
	 *
	 * @action update
	 * @param string $entryId
	 * @param int $version
	 * @param KontorolInteractivity $kontorolInteractivity
	 * @return KontorolInteractivity
	 * @throws kCoreException
	 * @throws kFileSyncException
	 * @validateUser entry entryId edit
	 */
	public function update($entryId, $version, $kontorolInteractivity)
	{
		$kInteractivity = new kInteractivity();
		$kontorolInteractivity->toUpdatableObject($kInteractivity);
		$validator = kInteractivityDataValidatorFactory::getValidator($entryId);
		$validator->validate(json_decode($kInteractivity->getData(), true));
		$kInteractivity->update($entryId, $version);
		$kontorolInteractivity->fromObject($kInteractivity, $this->getResponseProfile());
		return $kontorolInteractivity;
	}

	/**
	 * Delete a interactivity object by entry id
	 *
	 * @action delete
	 * @param string $entryId
	 * @throws FileSyncException
	 * @throws KontorolAPIException
	 * @validateUser entry entryId edit
	 */
	public function delete($entryId)
	{
		$entry = entryPeer::retrieveByPK($entryId);
		if(!$entry)
		{
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $entryId);
		}

		$syncKey = $entry->getSyncKey(kEntryFileSyncSubType::INTERACTIVITY_DATA);
		if (!kFileSyncUtils::fileSync_exists($syncKey))
		{
			throw new KontorolAPIException(KontorolInteractivityErrors::NO_INTERACTIVITY_DATA, $entryId);
		}

		kFileSyncUtils::deleteSyncFileForKey($syncKey);
		$entry->removeCapability(InteractivityPlugin::getCapabilityCoreValue());
		$entry->save();
	}

	/**
	 * Add a interactivity object
	 *
	 * @action add
	 * @param string $entryId
	 * @param KontorolInteractivity $kontorolInteractivity
	 * @return KontorolInteractivity
	 * @throws KontorolAPIException
	 * @throws kCoreException
	 * @validateUser entry entryId edit
	 */
	public function add($entryId, $kontorolInteractivity)
	{
		/* @var $kInteractivity kInteractivity */
		$kInteractivity = $kontorolInteractivity->toInsertableObject();
		$validator = kInteractivityDataValidatorFactory::getValidator($entryId);
		$validator->validate(json_decode($kInteractivity->getData(), true));
		try
		{
			$kInteractivity->insert($entryId);
		}
		catch (kFileSyncException $exception)
		{
			if($exception->getCode() == kFileSyncException::FILE_SYNC_ALREADY_EXISTS)
			{
				throw new KontorolAPIException(KontorolInteractivityErrors::INTERACTIVITY_DATA_ALREADY_EXISTS);
			}
			else
			{
				throw $exception;
			}
		}

		$kontorolInteractivity->fromObject($kInteractivity, $this->getResponseProfile());
		return $kontorolInteractivity;
	}
}

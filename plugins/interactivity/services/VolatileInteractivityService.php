<?php
/**
 * @service volatileInteractivity
 * @package plugins.interactivity
 * @subpackage api.services
 */

class VolatileInteractivityService extends KontorolBaseService
{
	/**
	 * Retrieve a volatile interactivity object by entry id
	 *
	 * @action get
	 * @param string $entryId
	 * @return KontorolVolatileInteractivity
	 * @throws Exception
	 * @validateUser entry entryId edit
	 */
	public function get($entryId)
	{
		$kVolatileInteractivity = new kVolatileInteractivity();
		$kVolatileInteractivity->setEntry($entryId);
		$KontorolVolatileInteractivity = new KontorolVolatileInteractivity();
		$KontorolVolatileInteractivity->fromObject($kVolatileInteractivity, $this->getResponseProfile());
		return $KontorolVolatileInteractivity;
	}

	/**
	 * Update a volatile interactivity object
	 *
	 * @action update
	 * @param string $entryId
	 * @param int $version
	 * @param KontorolVolatileInteractivity $kontorolVolatileInteractivity
	 * @return KontorolVolatileInteractivity
	 * @throws kCoreException
	 * @throws kFileSyncException
	 * @validateUser entry entryId edit
	 */
	public function update($entryId, $version, $kontorolVolatileInteractivity)
	{
		$kVolatileInteractivity = new kVolatileInteractivity();
		$kontorolVolatileInteractivity->toUpdatableObject($kVolatileInteractivity);
		$kVolatileInteractivity->update($entryId, $version);
		$kontorolVolatileInteractivity->fromObject($kVolatileInteractivity, $this->getResponseProfile());
		return $kontorolVolatileInteractivity;
	}

	/**
	 * Delete a volatile interactivity object by entry id
	 *
	 * @action delete
	 * @param string $entryId
	 * @throws FileSyncException
	 * @throws KontorolAPIException
	 * @validateUser entry entryId edit
	 */
	public function delete($entryId)
	{
		$entry = entryPeer::retrieveByPK($entryId);
		if(!$entry)
		{
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $entryId);
		}

		$syncKey = $entry->getSyncKey(kEntryFileSyncSubType::VOLATILE_INTERACTIVITY_DATA);
		if (!kFileSyncUtils::fileSync_exists($syncKey))
		{
			throw new KontorolAPIException(KontorolInteractivityErrors::NO_VOLATILE_INTERACTIVITY_DATA, $entryId);
		}

		kFileSyncUtils::deleteSyncFileForKey($syncKey);
	}

	/**
	 * add a volatile interactivity object
	 *
	 * @action add
	 * @param string $entryId
	 * @param KontorolVolatileInteractivity $kontorolVolatileInteractivity
	 * @return KontorolVolatileInteractivity
	 * @throws KontorolAPIException
	 * @throws kCoreException
	 * @throws kFileSyncException
	 * @validateUser entry entryId edit
	 */
	public function add($entryId, $kontorolVolatileInteractivity)
	{
		/* @var $kVolatileInteractivity kVolatileInteractivity */
		$kVolatileInteractivity = $kontorolVolatileInteractivity->toInsertableObject();
		try
		{
			$kVolatileInteractivity->insert($entryId);
		}
		catch (kFileSyncException $exception)
		{
			if($exception->getCode() == kFileSyncException::FILE_SYNC_ALREADY_EXISTS)
			{
				throw new KontorolAPIException(KontorolInteractivityErrors::VOLATILE_INTERACTIVITY_DATA_ALREADY_EXISTS);
			}
			else
			{
				throw $exception;
			}
		}

		$kontorolVolatileInteractivity->fromObject($kVolatileInteractivity, $this->getResponseProfile());
		return $kontorolVolatileInteractivity;
	}
}

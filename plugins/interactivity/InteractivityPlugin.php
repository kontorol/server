<?php
/**
 * @package plugins.interactivity
 */
class InteractivityPlugin extends KontorolPlugin implements IKontorolServices, IKontorolPermissions, IKontorolPending, IKontorolExceptionHandler, IKontorolEnumerator
{
	const PLUGIN_NAME = 'interactivity';
	const INTERACTIVITY_CORE_EXCEPTION = 'kInteractivityException';

	public static function getPluginName()
	{
		return self::PLUGIN_NAME;
	}

	public static function getCapabilityCoreValue()
	{
		return kPluginableEnumsManager::apiToCore(KontorolEntryCapability::getEnumClass(), self::PLUGIN_NAME . IKontorolEnumerator::PLUGIN_VALUE_DELIMITER . self::PLUGIN_NAME);
	}

	/* (non-PHPdoc)
	 * @see IKontorolEnumerator::getEnums()
	 */
	public static function getEnums($baseEnumName = null)
	{
		if (is_null($baseEnumName))
		{
			return array('InteractivityEntryCapability');
		}

		if ($baseEnumName == KontorolEntryCapability::getEnumClass())
		{
			return array('InteractivityEntryCapability');
		}

		return array();
	}

	/* (non-PHPdoc)
	 * @see IKontorolPermissions::isAllowedPartner()
	 */
	public static function isAllowedPartner($partnerId)
	{
		return true;
	}

	public static function dependsOn()
	{
		$dependency = new KontorolDependency(FileSyncPlugin::getPluginName());
		return array($dependency);
	}

	public static function getServicesMap()
	{
		return array(
			'interactivity' => 'InteractivityService',
			'volatileInteractivity' => 'VolatileInteractivityService',
		);
	}

	/**
	 * @param $exception
	 * @return KontorolAPIException|null
	 * @throws Exception
	 */
	public static function handleInteractivityException($exception)
	{
		$code = $exception->getCode();
		$data = $exception->getData();
		switch ($code)
		{
			case kInteractivityException::DUPLICATE_INTERACTIONS_IDS:
				$object = new KontorolAPIException(KontorolInteractivityErrors::DUPLICATE_INTERACTIONS_IDS);
				break;
			case kInteractivityException::DUPLICATE_NODES_IDS:
				$object = new KontorolAPIException(KontorolInteractivityErrors::DUPLICATE_NODES_IDS);
				break;
			case kInteractivityException::EMPTY_INTERACTIVITY_DATA:
				$object = new KontorolAPIException(KontorolInteractivityErrors::EMPTY_INTERACTIVITY_DATA);
				break;
			case kInteractivityException::DIFFERENT_DATA_VERSION:
				$object = new KontorolAPIException(KontorolInteractivityErrors::DIFFERENT_DATA_VERSION, $data[kInteractivityErrorMessages::VERSION_PARAMETER]);
				break;
			case kInteractivityException::MISSING_MANDATORY_PARAMETERS:
				$object = new KontorolAPIException(KontorolInteractivityErrors::MISSING_MANDATORY_PARAMETER, $data[kInteractivityErrorMessages::MISSING_PARAMETER]);
				break;
			case kInteractivityException::ILLEGAL_FIELD_VALUE:
				$object = new KontorolAPIException(KontorolInteractivityErrors::ILLEGAL_FIELD_VALUE, $data[kInteractivityErrorMessages::ERR_MSG]);
				break;
			case kInteractivityException::ENTRY_ILLEGAL_NODE_NUMBER:
				$object = new KontorolAPIException(KontorolInteractivityErrors::ENTRY_ILLEGAL_NODE_NUMBER);
				break;
			case kInteractivityException::ILLEGAL_ENTRY_NODE_ENTRY_ID:
				$object = new KontorolAPIException(KontorolInteractivityErrors::ILLEGAL_ENTRY_NODE_ENTRY_ID);
				break;
			case kInteractivityException::UNSUPPORTED_PLAYLIST_TYPE:
				$object = new KontorolAPIException(KontorolInteractivityErrors::UNSUPPORTED_PLAYLIST_TYPE);
				break;
			case kInteractivityException::CANT_UPDATE_NO_DATA:
				switch($data[kInteractivityErrorMessages::TYPE_PARAMETER])
				{
					case kEntryFileSyncSubType::INTERACTIVITY_DATA:
						$object = new KontorolAPIException(KontorolInteractivityErrors::NO_INTERACTIVITY_DATA, $data[kInteractivityErrorMessages::ENTRY_ID]);
						break;
					case kEntryFileSyncSubType::VOLATILE_INTERACTIVITY_DATA:
						$object = new KontorolAPIException(KontorolInteractivityErrors::NO_VOLATILE_INTERACTIVITY_DATA, $data[kInteractivityErrorMessages::ENTRY_ID]);
						break;
				}
				break;
			default:
				$object = null;
		}

		return $object;
	}

	public function getExceptionMap()
	{
		return array(
			self::INTERACTIVITY_CORE_EXCEPTION => array('InteractivityPlugin', 'handleInteractivityException'),
		);
	}
}

<?php

/**
 * @package plugins.interactivity
 * @subpackage lib.enum
 */

class InteractivityEntryCapability implements IKontorolPluginEnum, EntryCapability
{
	/**
	 * @return array
	 */
	public static function getAdditionalValues()
	{
		return array(
			'KONTOROL_INTERACTIVITY_CAPABILITY_NAME' => InteractivityPlugin::PLUGIN_NAME
		);
	}

	/**
	 * @return array
	 */
	public static function getAdditionalDescriptions()
	{
		return array();
	}

}

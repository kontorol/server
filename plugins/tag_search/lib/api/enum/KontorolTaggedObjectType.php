<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolTaggedObjectType extends KontorolDynamicEnum implements taggedObjectType
{
	public static function getEnumClass()
	{
		return 'taggedObjectType';
	}
}

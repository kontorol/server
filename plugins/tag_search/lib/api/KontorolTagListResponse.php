<?php
/**
 * @package plugins.tagSearch
 * @subpackage api.objects
 */
class KontorolTagListResponse extends KontorolListResponse
{
    /**
	 * @var KontorolTagArray
	 * @readonly
	 */
	public $objects;

	
	public function __construct()
	{
	    $this->objects = array();
	    $this->totalCount = count($this->objects);
	}
}

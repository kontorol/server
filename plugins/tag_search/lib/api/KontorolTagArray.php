<?php
/**
 * @package plugins.tagSearch
 * @subpackage api.objects
 */
class KontorolTagArray extends KontorolTypedArray
{
    /**
     * Function returns an array of API objects for the array of DB 
     * objects it is passed.
     * @param array $arr
     * @return KontorolTagArray
     */
    public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolTagArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
			$nObj = new KontorolTag();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolTag");
	}
}

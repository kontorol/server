<?php
/**
 * @package plugins.tagSearch
 * @subpackage errors
 */
class TagErrors extends KontorolErrors
{
    const INVALID_TAG_LENGTH = "INVALID_TAG_LENGTH;;minimum search length for tag is 3 characters";
}

<?php
/**
 * @package plugins.tagSearch
 * @subpackage Scheduler
 */
class KAsyncTagIndex extends KJobHandlerWorker
{
	/* (non-PHPdoc)
	 * @see KJobHandlerWorker::exec()
	 */
	protected function exec(KontorolBatchJob $job) {
		
		$this->reIndexTags($job);
		
	}

	/* (non-PHPdoc)
	 * @see KBatchBase::getType()
	 */
	public static function getType()
	{
		return KontorolBatchJobType::INDEX_TAGS;
	}
	
	protected function reIndexTags (KontorolBatchJob $job)
	{
		KontorolLog::info("Re-indexing tags according to privacy contexts");
		$tagPlugin = KontorolTagSearchClientPlugin::get(self::$kClient);
		$this->impersonate($job->partnerId);
		try 
		{
			$tagPlugin->tag->indexCategoryEntryTags($job->data->changedCategoryId, $job->data->deletedPrivacyContexts, $job->data->addedPrivacyContexts);
		}
		catch (Exception $e)
		{
			$this->unimpersonate();
			return $this->closeJob($job, KontorolBatchJobErrorTypes::KONTOROL_API, $e->getCode(), $e->getMessage(), KontorolBatchJobStatus::FAILED);
		}
		$this->unimpersonate();
		return $this->closeJob($job, null, null, "Re-index complete", KontorolBatchJobStatus::FINISHED);
		
	}
}

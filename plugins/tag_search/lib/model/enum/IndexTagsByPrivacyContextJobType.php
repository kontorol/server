<?php
/**
 * @package plugins.tag_search
 *  @subpackage model.enum
 */
class IndexTagsByPrivacyContextJobType implements IKontorolPluginEnum, BatchJobType
{
	const INDEX_TAGS = 'IndexTagsByPrivacyContext';
	
	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalValues()
	 */
	public static function getAdditionalValues() {
		return array(
			'INDEX_TAGS' => self::INDEX_TAGS,
		);
		
	}

	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalDescriptions()
	 */
	public static function getAdditionalDescriptions() {
		return array();
		
	}


}

<?php
/**
 * @package plugins.tag_search
 *  @subpackage model.enum
 */
class TagResolveBatchJobType implements IKontorolPluginEnum, BatchJobType
{
	const TAG_RESOLVE = 'TagResolve';
	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalValues()
	 */
	public static function getAdditionalValues() {
		return array(
			'TAG_RESOLVE' => self::TAG_RESOLVE,
		);
		
	}

	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalDescriptions()
	 */
	public static function getAdditionalDescriptions() {
		return array();
		
	}


}

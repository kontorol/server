<?php
/**
 * @package plugins.reach
 * @subpackage Scheduler
 */
class KSyncReachCreditTaskRunner extends KPeriodicWorker
{
	/* (non-PHPdoc)
	 * @see KBatchBase::getType()
	 */
	public static function getType()
	{
		return KontorolBatchJobType::SYNC_REACH_CREDIT_TASK;
	}

	/* (non-PHPdoc)
	 * @see KBatchBase::getJobType()
	 */
	public function getJobType()
	{
		return self::getType();
	}

	/* (non-PHPdoc)
	 * @see KBatchBase::run()
	*/
	public function run($jobs = null)
	{
		$reachClient = $this->SyncReachClient();
		$filter = new KontorolReachProfileFilter();
		$filter->statusEqual = KontorolReachProfileStatus::ACTIVE;
		$pager = new KontorolFilterPager();
		$pager->pageIndex = 1;
		$pager->pageSize = 500;
		
		
		do {
			$result = $reachClient->reachProfile->listAction($filter, $pager);
			foreach ($result->objects as $reachProfile)
			{
				try
				{
					$this->syncReachProfileCredit($reachProfile);
				}
				catch (Exception $ex)
				{
					KontorolLog::err($ex);
				}
			}
			
			$pager->pageIndex++;
		}  while(count($result->objects) == $pager->pageSize);
	}

	/**
	 * @param KontorolReachProfile $reachProfile
	 */
	protected function syncReachProfileCredit(KontorolReachProfile $reachProfile)
	{
		$reachClient = $this->SyncReachClient();
		$this->impersonate($reachProfile->partnerId);
		try
		{
			$result = $reachClient->reachProfile->syncCredit($reachProfile->id);
			$this->unimpersonate();
		}
		catch (Exception $ex)
		{
			$this->unimpersonate();
			throw $ex;
		}
	}

	/**
	 * @return KontorolReachClientPlugin
	 */
	protected function SyncReachClient()
	{
		$client = $this->getClient();
		return KontorolReachClientPlugin::get($client);
	}
}

<?php

/**
 * @package plugins.reach
 * @subpackage api.filters
 */

class KontorolReachReportInputFilter extends KontorolReportInputFilter
{

	private static $map_between_objects = array
	(
		'serviceType',
		'serviceFeature',
		'turnAroundTime',
	);

	/**
	 * @var KontorolVendorServiceType
	 */
	public $serviceType;
	
	/**
	 * @var KontorolVendorServiceFeature
	 */
	public $serviceFeature;
	
	/**
	 * @var KontorolVendorServiceTurnAroundTime
	 */
	public $turnAroundTime;

	protected function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}

	public function toReportsInputFilter($reportInputFilter = null)
	{
		if (!$reportInputFilter)
			$reportInputFilter = new reachReportsInputFilter();

		return parent::toReportsInputFilter($reportInputFilter);
	}
}

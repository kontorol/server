<?php
/**
 * @package plugins.reach
 * @subpackage api.filters
 */
class KontorolCatalogItemAdvancedFilter extends KontorolSearchItem
{
	/**
	 * @var KontorolVendorServiceType
	 */
	public $serviceTypeEqual;
	
	/**
	 * @var string
	 */
	public $serviceTypeIn;
	
	/**
	 * @var KontorolVendorServiceFeature
	 */
	public $serviceFeatureEqual;
	
	/**
	 * @var string
	 */
	public $serviceFeatureIn;
	
	/**
	 * @var KontorolVendorServiceTurnAroundTime
	 */
	public $turnAroundTimeEqual;
	
	/**
	 * @var string
	 */
	public $turnAroundTimeIn;
	
	/**
	 * @var KontorolCatalogItemLanguage
	 */
	public $sourceLanguageEqual;
	
	/**
	 * @var KontorolCatalogItemLanguage
	 */
	public $targetLanguageEqual;
	
	
	private static $map_between_objects = array
	(
		'serviceTypeEqual',
		'serviceTypeIn',
		'serviceFeatureEqual',
		'serviceFeatureIn',
		'turnAroundTimeEqual',
		'turnAroundTimeIn',
		'sourceLanguageEqual',
		'targetLanguageEqual',
	);
	
	/* (non-PHPdoc)
 	 * @see KontorolCuePoint::getMapBetweenObjects()
 	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}
	
	public function toObject ( $object_to_fill = null , $props_to_skip = array() )
	{
		if(!$object_to_fill)
			$object_to_fill = new kCatalogItemAdvancedFilter();
		
		return parent::toObject($object_to_fill, $props_to_skip);
	}
}

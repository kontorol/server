<?php
/**
 * @package plugins.reach
 * @subpackage api.objects
 * @abstract
 */

abstract class KontorolBaseVendorCredit extends KontorolObject implements IApiObjectFactory
{
	/* (non-PHPdoc)
 	 * @see IApiObjectFactory::getInstance($sourceObject, KontorolDetachedResponseProfile $responseProfile)
 	 */
	public static function getInstance($sourceObject, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$creditType = get_class($sourceObject);
		$credit = null;
		switch ($creditType)
		{
			case 'kVendorCredit':
				$credit = new KontorolVendorCredit();
				break;

			case 'kTimeRangeVendorCredit':
				$credit = new KontorolTimeRangeVendorCredit();
				break;

			case 'kReoccurringVendorCredit':
				$credit = new KontorolReoccurringVendorCredit();
				break;

			case 'kUnlimitedVendorCredit':
				$credit = new KontorolUnlimitedVendorCredit();
				break;
		}

		if ($credit)
			/* @var $object KontorolBaseVendorCredit */
			$credit->fromObject($sourceObject, $responseProfile);

		return $credit;
	}

		/* (non-PHPdoc)
		* @see KontorolObject::validateForInsert()
		*/
	public function validateForInsert($propertiesToSkip = array())
  	{
		parent::validateForInsert($propertiesToSkip);
	}
	
	public function hasObjectChanged($sourceObject)
	{
		return false;
	}

	/**
	 * @param $object
	 * @return bool
	 */
	public function isMatchingCoreClass($object)
	{
		if (!$object)
		{
			return false;
		}
		return get_class($object) ===  $this->getMatchingCoreClassName();
	}
}

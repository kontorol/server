<?php
/**
 * @package plugins.reach
 * @subpackage api.objects
 */

class KontorolTimeRangeVendorCredit extends KontorolVendorCredit
{
	/**
	 *  @var time
	 */
	public $toDate;

	private static $map_between_objects = array (
		'toDate',
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}

	/* (non-PHPdoc)
 	 * @see KontorolObject::toObject($object_to_fill, $props_to_skip)
 	 */
	public function toObject($dbObject = null, $propsToSkip = array())
	{
		if (!$dbObject)
		{
			$dbObject = new kTimeRangeVendorCredit();
		}
		return parent::toObject($dbObject, $propsToSkip);
	}

	/* (non-PHPdoc)
	 * @see KontorolObject::validateForInsert()
	 */
	public function validateForInsert($propertiesToSkip = array())
	{
		$this->validatePropertyNotNull('toDate');
		parent::validateForInsert($propertiesToSkip);

		if ($this->fromDate > $this->toDate)
		{
			throw new KontorolAPIException(KontorolReachErrors::INVALID_CREDIT_DATES , $this->fromDate, $this->toDate);
		}
	}

	/* (non-PHPdoc)
	 * @see KontorolObject::validateForUpdate()
	 */
	public function validateForUpdate($sourceObject, $propertiesToSkip = array())
	{
		$this->validatePropertyNotNull('toDate');
		parent::validateForUpdate($sourceObject, $propertiesToSkip);

		if ($this->fromDate > $this->toDate)
		{
			throw new KontorolAPIException(KontorolReachErrors::INVALID_CREDIT_DATES, $this->fromDate, $this->toDate);
		}
	}

	public function hasObjectChanged($sourceObject)
	{
		if (parent::hasObjectChanged($sourceObject))
		{
			return true;
		}

		/* @var $sourceObject kTimeRangeVendorCredit */
		if ($this->toDate && ($this->toDate != $sourceObject->getToDate()))
		{
			return true;
		}
		return false;
	}

	/**
	 * @return string
	 */
	protected function getMatchingCoreClassName()
	{
		return 'kTimeRangeVendorCredit';
	}
}

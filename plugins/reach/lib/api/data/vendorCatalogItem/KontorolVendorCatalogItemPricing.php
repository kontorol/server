<?php
/**
 * @package plugins.reach
 * @subpackage api.objects
 */
class KontorolVendorCatalogItemPricing extends KontorolObject
{
	/**
	 * @var float
	 */
	public $pricePerUnit;
	
	/**
	 * @var KontorolVendorCatalogItemPriceFunction
	 */
	public $priceFunction;
	
	private static $map_between_objects = array
	(
		'pricePerUnit',
		'priceFunction',
	);
	
	/* (non-PHPdoc)
 	 * @see KontorolObject::getMapBetweenObjects()
 	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject($object_to_fill, $props_to_skip)
	 */
	public function toObject($dbObject = null, $propsToSkip = array())
	{
		if (!$dbObject)
		{
			$dbObject = new kVendorCatalogItemPricing();
		}
		
		return parent::toObject($dbObject, $propsToSkip);
	}
}

<?php
/**
 * @package plugins.reach
 * @subpackage api.objects
 */
class KontorolVendorChapteringCatalogItem extends KontorolVendorCatalogItem
{
	/**
	 * @var KontorolCatalogItemLanguage
	 * @filter eq,in
	 */
	public $sourceLanguage;

	private static $map_between_objects = array
	(
		'sourceLanguage',
	);
	
	protected function getServiceFeature()
	{
		return KontorolVendorServiceFeature::CHAPTERING;
	}
	
	/* (non-PHPdoc)
	 * @see KontorolCuePoint::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}
	
	/* (non-PHPdoc)
 * @see KontorolObject::toInsertableObject()
 */
	public function toInsertableObject($object_to_fill = null, $props_to_skip = array())
	{
		if (is_null($object_to_fill))
			$object_to_fill = new VendorChapteringCatalogItem();
		
		return parent::toInsertableObject($object_to_fill, $props_to_skip);
	}
	
	public function validateForInsert($propertiesToSkip = array())
	{
		$this->validatePropertyNotNull(array("sourceLanguage"));
		return parent::validateForInsert($propertiesToSkip);
	}
	
	/* (non-PHPdoc)
 	 * @see KontorolObject::toObject($object_to_fill, $props_to_skip)
 	 */
	public function toObject($sourceObject = null, $propertiesToSkip = array())
	{
		if(is_null($sourceObject))
		{
			$sourceObject = new VendorChapteringCatalogItem();
		}
		
		return parent::toObject($sourceObject, $propertiesToSkip);
	}
}

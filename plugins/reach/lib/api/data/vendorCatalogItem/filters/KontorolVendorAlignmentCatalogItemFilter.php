<?php
/**
 * @package plugins.reach
 * @subpackage api.filters
 */
class KontorolVendorAlignmentCatalogItemFilter extends KontorolVendorCaptionsCatalogItemBaseFilter
{
	public function getTypeListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null, $type = null)
	{
		if(!$type)
			$type = KontorolVendorServiceFeature::ALIGNMENT;
		
		return parent::getTypeListResponse($pager, $responseProfile, $type);
	}
}

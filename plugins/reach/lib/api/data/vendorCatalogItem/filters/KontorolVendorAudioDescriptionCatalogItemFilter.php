<?php
/**
 * @package plugins.reach
 * @subpackage api.filters
 */
class KontorolVendorAudioDescriptionCatalogItemFilter extends KontorolVendorCaptionsCatalogItemBaseFilter
{
	public function getTypeListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null, $type = null)
	{
		if(!$type)
			$type = KontorolVendorServiceFeature::AUDIO_DESCRIPTION;
		
		return parent::getTypeListResponse($pager, $responseProfile, $type);
	}
}

<?php
/**
 * @package plugins.reach
 * @subpackage api.filters
 */
class KontorolVendorCaptionsCatalogItemFilter extends KontorolVendorCaptionsCatalogItemBaseFilter
{
	public function getTypeListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null, $type = null)
	{
		if(!$type)
			$type = KontorolVendorServiceFeature::CAPTIONS;
		
		return parent::getTypeListResponse($pager, $responseProfile, $type);
	}
}

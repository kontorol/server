<?php
/**
 * @package plugins.reach
 * @subpackage api.filters
 */
class KontorolVendorTranslationCatalogItemFilter extends KontorolVendorTranslationCatalogItemBaseFilter
{
	public function getTypeListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null, $type = null)
	{
		if(!$type)
			$type = KontorolVendorServiceFeature::TRANSLATION;
			
		return parent::getTypeListResponse($pager, $responseProfile, $type);
	}
}

<?php
/**
 * @package plugins.reach
 * @subpackage api.filters
 */
class KontorolVendorChapteringCatalogItemFilter extends KontorolVendorCaptionsCatalogItemBaseFilter
{
	public function getTypeListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null, $type = null)
	{
		if(!$type)
			$type = KontorolVendorServiceFeature::CHAPTERING;
		
		return parent::getTypeListResponse($pager, $responseProfile, $type);
	}
}

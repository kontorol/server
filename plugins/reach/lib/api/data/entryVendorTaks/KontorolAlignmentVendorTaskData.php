<?php
/**
 * @package plugins.reach
 * @subpackage api.objects
 * @relatedService EntryVendorTaskService
 */
class KontorolAlignmentVendorTaskData extends KontorolVendorTaskDataCaptionAsset
{
	/**
	 * The id of the text transcript object the vendor should use while runing the alignment task
	 * @var string
	 */
	public $textTranscriptAssetId;
	
	/**
	 * Optional - The id of the json transcript object the vendor should update once alignment task processing is done
	 * @insertonly
	 * @var string
	 */
	public $jsonTranscriptAssetId;

	private static $map_between_objects = array
	(
		'textTranscriptAssetId',
		'jsonTranscriptAssetId',
	);
	
	/* (non-PHPdoc)
	 * @see KontorolCuePoint::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}
	
	/* (non-PHPdoc)
 	 * @see KontorolObject::toObject($object_to_fill, $props_to_skip)
 	 */
	public function toObject($dbObject = null, $propsToSkip = array())
	{
		if (!$dbObject)
		{
			$dbObject = new kAlignmentVendorTaskData();
		}
		
		return parent::toObject($dbObject, $propsToSkip);
	}
	
	/* (non-PHPdoc)
 	 * @see KontorolObject::toInsertableObject()
 	 */
	public function toInsertableObject($object_to_fill = null, $props_to_skip = array())
	{
		if (is_null($object_to_fill))
		{
			$object_to_fill = new kAlignmentVendorTaskData();
		}
		
		return parent::toInsertableObject($object_to_fill, $props_to_skip);
	}
	
	public function validateForInsert($propertiesToSkip = array())
	{
		$this->validatePropertyNotNull("textTranscriptAssetId");
		$this->validateTranscriptAsset($this->textTranscriptAssetId, KontorolAttachmentType::TEXT);
		
		if($this->jsonTranscriptAssetId)
		{
			$this->validateTranscriptAsset($this->jsonTranscriptAssetId, KontorolAttachmentType::JSON);
		}
		
		if($this->captionAssetId)
		{
			$this->validateCaptionAsset($this->captionAssetId);
		}

		return parent::validateForInsert($propertiesToSkip);
	}
	
	public function validateForUpdate($sourceObject, $propertiesToSkip = array())
	{
		/* @var $sourceObject kAlignmentVendorTaskData */
		if(isset($this->textTranscriptAssetId) && $sourceObject->getTextTranscriptAssetId() != $this->textTranscriptAssetId)
		{
			$this->validateTranscriptAsset($this->textTranscriptAssetId, KontorolAttachmentType::TEXT);
		}
		
		/* @var $sourceObject kAlignmentVendorTaskData */
		if(isset($this->jsonTranscriptAssetId) && $sourceObject->getJsonTranscriptAssetId() != $this->jsonTranscriptAssetId)
		{
			$this->validateTranscriptAsset($this->jsonTranscriptAssetId, KontorolAttachmentType::JSON);
		}
		
		/* @var $sourceObject kAlignmentVendorTaskData */
		if(isset($this->captionAssetId) && $sourceObject->getCaptionAssetId() != $this->captionAssetId)
		{
			$this->validateCaptionAsset($this->captionAssetId);
		}

		return parent::validateForUpdate($sourceObject, $propertiesToSkip);
	}

	protected function validateTranscriptAsset($transcriptAssetId, $expectedType)
	{
		$transcriptAssetDb = assetPeer::retrieveById($transcriptAssetId);
		if (!$transcriptAssetDb || !($transcriptAssetDb instanceof TranscriptAsset))
		{
			throw new KontorolAPIException(KontorolAttachmentErrors::ATTACHMENT_ASSET_ID_NOT_FOUND, $transcriptAssetId);
		}
		
		/* @var $transcriptAssetDb TranscriptAsset */
		if($transcriptAssetDb->getContainerFormat() != $expectedType)
		{
			throw new KontorolAPIException(KontorolAttachmentErrors::ATTACHMENT_ASSET_FORMAT_MISMATCH, $transcriptAssetId, $expectedType);
		}
	}
}

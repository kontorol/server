<?php
/**
 * @package plugins.reach
 * @subpackage api.objects
 * @relatedService EntryVendorTaskService
 */
abstract class KontorolVendorTaskData extends KontorolObject implements IApiObjectFactory
{
	
	/**
	 * The duration of the entry for which the task was created for in milliseconds
	 * @var int
	 * @readonly
	 */
	public $entryDuration;
	
	private static $map_between_objects = array
	(
		'entryDuration',
	);
	
	/* (non-PHPdoc)
	 * @see KontorolCuePoint::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}
	
	/* (non-PHPdoc)
 	 * @see KontorolObject::toObject($object_to_fill, $props_to_skip)
 	 */
	public function toObject($dbObject = null, $propsToSkip = array())
	{
		if (!$dbObject)
		{
			$dbObject = new kVendorTaskData();
		}
		
		return parent::toObject($dbObject, $propsToSkip);
	}
	
	/* (non-PHPdoc)
 	 * @see IApiObjectFactory::getInstance($sourceObject, KontorolDetachedResponseProfile $responseProfile)
 	 */
	public static function getInstance($sourceObject, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$taskDataType = get_class($sourceObject);
		$taskData = null;
		switch ($taskDataType)
		{
			case 'kAlignmentVendorTaskData':
				$taskData = new KontorolAlignmentVendorTaskData();
				break;

			case 'kTranslationVendorTaskData':
				$taskData = new KontorolTranslationVendorTaskData();
				break;
		}
		
		if ($taskData)
			/* @var $object KontorolVendorTaskData */
			$taskData->fromObject($sourceObject, $responseProfile);
		
		return $taskData;
	}
}

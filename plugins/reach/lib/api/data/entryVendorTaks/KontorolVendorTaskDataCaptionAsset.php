<?php
/**
 * @package plugins.reach
 * @subpackage api.objects
 * @relatedService EntryVendorTaskService
 */
abstract class KontorolVendorTaskDataCaptionAsset extends KontorolVendorTaskData
{
	/**
	 * Optional - The id of the caption asset object
	 * @insertonly
	 * @var string
	 */
	public $captionAssetId;

	private static $map_between_objects = array
	(
		'captionAssetId',
	);

	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}

	protected function validateCaptionAsset($captionAssetId)
	{
		$captionAssetDb = assetPeer::retrieveById($captionAssetId);
		if (!$captionAssetDb || !($captionAssetDb instanceof CaptionAsset))
		{
			throw new KontorolAPIException(KontorolCaptionErrors::CAPTION_ASSET_ID_NOT_FOUND, $captionAssetId);
		}
	}
}

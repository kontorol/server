<?php
/**
 * @package plugins.reach
 * @subpackage api.filters
 */
class KontorolReachProfileFilter extends KontorolReachProfileBaseFilter
{
	protected function getCoreFilter()
	{
		return new ReachProfileFilter();
	}
	
	/* (non-PHPdoc)
 	 * @see KontorolRelatedFilter::getListResponse()
 	 */
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$c = new Criteria();
		$filter = $this->toObject();
		$filter->attachToCriteria($c);
		$pager->attachToCriteria($c);
		
		$list = ReachProfilePeer::doSelect($c);
		
		$resultCount = count($list);
		if ($resultCount && $resultCount < $pager->pageSize)
			$totalCount = ($pager->pageIndex - 1) * $pager->pageSize + $resultCount;
		else
		{
			KontorolFilterPager::detachFromCriteria($c);
			$totalCount = ReachProfilePeer::doCount($c);
		}
		
		$response = new KontorolReachProfileListResponse();
		$response->objects = KontorolReachProfileArray::fromDbArray($list, $responseProfile);
		$response->totalCount = $totalCount;
		return $response;
	}
}

<?php
/**
 * @package plugins.reach
 * @subpackage api.objects
 */
class KontorolVendorCatalogItemArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolVendorCatalogItemArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
			$newArr[] = KontorolVendorCatalogItem::getInstance($obj, $responseProfile);
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolVendorCatalogItem");
	}
}

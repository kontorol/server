<?php
/**
 * @package plugins.reach
 * @subpackage api.objects
 */
class KontorolVendorCatalogItemListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolVendorCatalogItemArray
	 * @readonly
	 */
	public $objects;
}

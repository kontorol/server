<?php
/**
 * @package plugins.reach
 * @subpackage api.objects
 */
class KontorolEntryVendorTaskListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolEntryVendorTaskArray
	 * @readonly
	 */
	public $objects;
}

<?php
/**
 * @package plugins.reach
 * @subpackage api.objects
 */
class KontorolReachProfileListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolReachProfileArray
	 * @readonly
	 */
	public $objects;
}

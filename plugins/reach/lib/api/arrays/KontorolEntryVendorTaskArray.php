<?php
/**
 * @package plugins.reach
 * @subpackage api.objects
 */
class KontorolEntryVendorTaskArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolEntryVendorTaskArray();
		if ($arr == null)
			return $newArr;
		
		foreach ($arr as $obj)
		{
			$object = new KontorolEntryVendorTask();
			$object->fromObject($obj, $responseProfile);
			$newArr[] = $object;
		}
		
		return $newArr;
	}
	
	public function __construct()
	{
		parent::__construct("KontorolEntryVendorTask");
	}
}

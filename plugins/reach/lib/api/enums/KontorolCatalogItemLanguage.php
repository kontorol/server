<?php
/**
 * @package plugins.reach
 * @subpackage api.enum
 */
class KontorolCatalogItemLanguage extends KontorolStringEnum
{
	const EN = KontorolLanguage::EN;
	const EN_US = KontorolLanguage::EN_US;
	const EN_GB = KontorolLanguage::EN_GB;
	const NL = KontorolLanguage::NL;
	const FR = KontorolLanguage::FR;
	const DE = KontorolLanguage::DE;
	const IT = KontorolLanguage::IT;
	const ES = KontorolLanguage::ES;
	const AR = KontorolLanguage::AR;
	const ZH = KontorolLanguage::ZH;
	const CMN = KontorolLanguage::CMN;
	const YUE = KontorolLanguage::YUE;
	const HE = KontorolLanguage::HE;
	const HI = KontorolLanguage::HI;
	const JA = KontorolLanguage::JA;
	const KO = KontorolLanguage::KO;
	const PT = KontorolLanguage::PT;
	const PT_BR = KontorolLanguage::PT_BR;
	const RU = KontorolLanguage::RU;
	const TR = KontorolLanguage::TR;
	const TH = KontorolLanguage::TH;
	const SV = KontorolLanguage::SV;
	const DA = KontorolLanguage::DA;
	const NO = KontorolLanguage::NO;
	const FI = KontorolLanguage::FI;
	const IS = KontorolLanguage::IS;
	const PL = KontorolLanguage::PL;
	const IN = KontorolLanguage::IN;
	const EL = KontorolLanguage::EL;
	const RO = KontorolLanguage::RO;
	const HU = KontorolLanguage::HU;
	const GA = KontorolLanguage::GA;
	const GD = KontorolLanguage::GD;
	const CY = KontorolLanguage::CY;
	const UR = KontorolLanguage::UR;
	const TA = KontorolLanguage::TA;
	const ML = KontorolLanguage::ML;
	const ZU = KontorolLanguage::ZU;
	const VI = KontorolLanguage::VI;
	const UK = KontorolLanguage::UK;
	const FR_CA = KontorolLanguage::FR_CA;
	const ZH_TW = KontorolLanguage::ZH_TW;
	const ES_XL = KontorolLanguage::ES_XL;
}

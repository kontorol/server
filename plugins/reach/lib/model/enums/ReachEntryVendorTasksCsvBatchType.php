<?php

/**
 * @package plugins.reach
 * @subpackage model.enum
 */
class ReachEntryVendorTasksCsvBatchType implements IKontorolPluginEnum, BatchJobType
{
	const ENTRY_VENDOR_TASK_CSV = 'EntryVendorTasksCsv';

	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalValues()
	 */
	public static function getAdditionalValues()
	{
		return array(
			'ENTRY_VENDOR_TASK_CSV' => self::ENTRY_VENDOR_TASK_CSV,
		);
	}

	/* (non-PHPdoc)
	 * @see IKontorolPluginEnum::getAdditionalDescriptions()
	 */
	public static function getAdditionalDescriptions()
	{
		return array();
	}
}

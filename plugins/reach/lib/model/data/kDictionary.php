
<?php

/**
 * Define language Dictionary profile
 *
 * @package plugins.reach
 * @subpackage model
 *
 */
class kDictionary
{
	/**
	 * @var KontorolCatalogItemLanguage
	 */
	protected $language;

	/**
	 * @var string
	 */
	protected $data;

	/**
	 * @return the KontorolCatalogItemLanguage
	 */
	public function getLanguage()
	{
		return $this->language;
	}

	/**
	 * @return the data
	 */
	public function getData()
	{
		return $this->data;
	}

	/**
	 * @param KontorolCatalogItemLanguage $language
	 */
	public function setLanguage($language)
	{
		$this->language = $language;
	}

	/**
	 * @param string $data
	 */
	public function setData($data)
	{
		$this->data = $data;
	}
}

<?php
/**
 * Reach Profile Service
 *
 * @service reachProfile
 * @package plugins.reach
 * @subpackage api.services
 * @throws KontorolErrors::SERVICE_FORBIDDEN
 */

class ReachProfileService extends KontorolBaseService
{

	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);

		if (!ReachPlugin::isAllowedPartner($this->getPartnerId()))
			throw new KontorolAPIException(KontorolErrors::FEATURE_FORBIDDEN, ReachPlugin::PLUGIN_NAME);

		$this->applyPartnerFilterForClass('reachProfile');
	}

	/**
	 * Allows you to add a partner specific reach profile
	 *
	 * @action add
	 * @param KontorolReachProfile $reachProfile
	 * @return KontorolReachProfile
	 */
	public function addAction(KontorolReachProfile $reachProfile)
	{
		$dbReachProfile = $reachProfile->toInsertableObject();

		/* @var $dbReachProfile ReachProfile */
		$dbReachProfile->setPartnerId(kCurrentContext::getCurrentPartnerId());
		$dbReachProfile->setStatus(KontorolReachProfileStatus::ACTIVE);
		$credit = $dbReachProfile->getCredit();
		if ( $credit && $credit instanceof kReoccurringVendorCredit)
		{
			/* @var $credit kReoccurringVendorCredit */
			$credit->setPeriodDates();
			$dbReachProfile->setCredit($credit);
		}

		$dbReachProfile->save();

		// return the saved object
		$reachProfile->fromObject($dbReachProfile, $this->getResponseProfile());
		return $reachProfile;
	}

	/**
	 * Retrieve specific reach profile by id
	 *
	 * @action get
	 * @param int $id
	 * @return KontorolReachProfile
	 * @throws KontorolReachErrors::REACH_PROFILE_NOT_FOUND
	 */
	function getAction($id)
	{
		$dbReachProfile = ReachProfilePeer::retrieveByPK($id);
		if (!$dbReachProfile)
			throw new KontorolAPIException(KontorolReachErrors::REACH_PROFILE_NOT_FOUND, $id);
		
		$reachProfile = new KontorolReachProfile();
		$reachProfile->fromObject($dbReachProfile, $this->getResponseProfile());
		return $reachProfile;
	}

	/**
	 * List KontorolReachProfile objects
	 *
	 * @action list
	 * @param KontorolReachProfileFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return KontorolReachProfileListResponse
	 */
	public function listAction(KontorolReachProfileFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
			$filter = new KontorolReachProfileFilter();

		if (!$pager)
			$pager = new KontorolFilterPager();

		return $filter->getListResponse($pager, $this->getResponseProfile());
	}

	/**
	 * Update an existing reach profile object
	 *
	 * @action update
	 * @param int $id
	 * @param KontorolReachProfile $reachProfile
	 * @return KontorolReachProfile
	 *
	 * @throws KontorolReachErrors::REACH_PROFILE_NOT_FOUND
	 */
	public function updateAction($id, KontorolReachProfile $reachProfile)
	{
		// get the object
		/* @var $dbReachProfile ReachProfile */
		$dbReachProfile = ReachProfilePeer::retrieveByPK($id);
		if (!$dbReachProfile)
			throw new KontorolAPIException(KontorolReachErrors::REACH_PROFILE_NOT_FOUND, $id);

		// save the object
		$dbReachProfile = $reachProfile->toUpdatableObject($dbReachProfile);
		$credit = $dbReachProfile->getCredit();
		if ($credit)
		{
			if ($credit instanceof kReoccurringVendorCredit)
			{
				/* @var $credit kReoccurringVendorCredit */
				$credit->setPeriodDates();
				$dbReachProfile->setCredit($credit);
			}
			$dbReachProfile->calculateCreditPercentUsage();
		}
		$dbReachProfile->save();

		// return the saved object
		$reachProfile = new KontorolReachProfile();
		$reachProfile->fromObject($dbReachProfile, $this->getResponseProfile());
		return $reachProfile;
	}

	/**
	 * Update reach profile status by id
	 *
	 * @action updateStatus
	 * @param int $id
	 * @param KontorolReachProfileStatus $status
	 * @return KontorolReachProfile
	 *
	 * @throws KontorolReachErrors::REACH_PROFILE_NOT_FOUND
	 */
	function updateStatusAction($id, $status)
	{
		// get the object
		$dbReachProfile = ReachProfilePeer::retrieveByPK($id);
		if (!$dbReachProfile)
			throw new KontorolAPIException(KontorolReachErrors::CATALOG_ITEM_NOT_FOUND, $id);
		
		$dbReachProfile->setStatus($status);
		$credit = $dbReachProfile->getCredit();
		if ($status == KontorolReachProfileStatus::ACTIVE && $credit && $credit instanceof kReoccurringVendorCredit)
        {
	        /* @var $credit kReoccurringVendorCredit */
			$credit->setPeriodDates();
			$dbReachProfile->setCredit($credit);
		}
		
		// save the object
		$dbReachProfile->save();

		// return the saved object
		$reachProfile = new KontorolReachProfile();
		$reachProfile->fromObject($dbReachProfile, $this->getResponseProfile());
		return $reachProfile;
	}

	/**
	 * Delete vednor profile by id
	 *
	 * @action delete
	 * @param int $id
	 *
	 * @throws KontorolReachErrors::REACH_PROFILE_NOT_FOUND
	 */
	public function deleteAction($id)
	{
		// get the object
		$dbReachProfile = ReachProfilePeer::retrieveByPK($id);
		if (!$dbReachProfile)
			throw new KontorolAPIException(KontorolReachErrors::REACH_PROFILE_NOT_FOUND, $id);

		// set the object status to deleted
		$dbReachProfile->setStatus(KontorolReachProfileStatus::DELETED);
		$dbReachProfile->save();
	}

	/**
	 * sync vednor profile credit
	 *
	 * @action syncCredit
	 * @param int $reachProfileId
	 * @return KontorolReachProfile
	 * @throws KontorolReachErrors::REACH_PROFILE_NOT_FOUND
	 */
	public function syncCredit($reachProfileId)
	{
		$dbReachProfile = ReachProfilePeer::retrieveByPK($reachProfileId);
		if (!$dbReachProfile)
			throw new KontorolAPIException(KontorolReachErrors::REACH_PROFILE_NOT_FOUND, $reachProfileId);

		// set the object status to deleted
		if( $dbReachProfile->shouldSyncCredit())
		{
			//ignore updating updatedAt field since we are syncing only the credit within the profile and we update the lastSyncTime
			$dbReachProfile->setIgnoreUpdatedAt(true);
			$dbReachProfile->syncCredit();
			$dbReachProfile->save();
		}

		// return the saved object
		$reachProfile = new KontorolReachProfile();
		$reachProfile->fromObject($dbReachProfile, $this->getResponseProfile());
		return $reachProfile;
	}
}

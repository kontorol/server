<?php
/**
 * Partner Catalog Item Service
 *
 * @service PartnerCatalogItem
 * @package plugins.reach
 * @subpackage api.services
 * @throws KontorolErrors::SERVICE_FORBIDDEN
 */

class PartnerCatalogItemService extends KontorolBaseService
{

	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);

		if (!ReachPlugin::isAllowedPartner($this->getPartnerId()))
			throw new KontorolAPIException(KontorolErrors::FEATURE_FORBIDDEN, ReachPlugin::PLUGIN_NAME);

		$this->applyPartnerFilterForClass('PartnerCatalogItem');
	}

	/**
	 * Assign existing catalogItem to specific account
	 *
	 * @action add
	 * @param int $id source catalog item to assign to partner
	 * @throws KontorolReachErrors::CATALOG_ITEM_NOT_FOUND
	 * @throws KontorolReachErrors::VENDOR_CATALOG_ITEM_ALREADY_ENABLED_ON_PARTNER
	 *
	 * @return KontorolVendorCatalogItem
	 */
	public function addAction($id)
	{
		// get the object
		$dbVendorCatalogItem = VendorCatalogItemPeer::retrieveByPK($id);
		if (!$dbVendorCatalogItem)
			throw new KontorolAPIException(KontorolReachErrors::CATALOG_ITEM_NOT_FOUND, $id);

		//Check if catalog item already enabled on partner
		$dbPartnerCatalogItem = PartnerCatalogItemPeer::retrieveByCatalogItemId($id, kCurrentContext::getCurrentPartnerId());
		if ($dbPartnerCatalogItem)
			throw new KontorolAPIException(KontorolReachErrors::VENDOR_CATALOG_ITEM_ALREADY_ENABLED_ON_PARTNER, $id, kCurrentContext::getCurrentPartnerId());

		//Check if catalog item exists but deleted to re-use it
		$partnerCatalogItem = PartnerCatalogItemPeer::retrieveByCatalogItemIdNoFilter($id, kCurrentContext::getCurrentPartnerId());
		if (!$partnerCatalogItem)
		{
			$partnerCatalogItem = new PartnerCatalogItem();
			$partnerCatalogItem->setPartnerId($this->getPartnerId());
			$partnerCatalogItem->setCatalogItemId($id);
		}

		$partnerCatalogItem->setStatus(KontorolVendorCatalogItemStatus::ACTIVE);
		$partnerCatalogItem->save();

		// return the catalog item
		$vendorCatalogItem = KontorolVendorCatalogItem::getInstance($dbVendorCatalogItem, $this->getResponseProfile());
		$vendorCatalogItem->fromObject($dbVendorCatalogItem, $this->getResponseProfile());
		return $vendorCatalogItem;
	}

	/**
	 * Remove existing catalogItem from specific account
	 *
	 * @action delete
	 * @param int $id source catalog item to remove
	 * @throws KontorolReachErrors::CATALOG_ITEM_NOT_FOUND
	 * @throws KontorolReachErrors::VENDOR_CATALOG_ITEM_ALREADY_ENABLED_ON_PARTNER
	 */
	public function deleteAction($id)
	{
		$dbVendorCatalogItem = VendorCatalogItemPeer::retrieveByPK($id);
		if (!$dbVendorCatalogItem)
			throw new KontorolAPIException(KontorolReachErrors::CATALOG_ITEM_NOT_FOUND, $id);

		//Check if catalog item already enabled
		$dbPartnerCatalogItem = PartnerCatalogItemPeer::retrieveByCatalogItemId($id, kCurrentContext::getCurrentPartnerId());
		if (!$dbPartnerCatalogItem)
			throw new KontorolAPIException(KontorolReachErrors::PARTNER_CATALOG_ITEM_NOT_FOUND, $id);

		$dbPartnerCatalogItem->setStatus(VendorCatalogItemStatus::DELETED);
		$dbPartnerCatalogItem->save();
	}
}

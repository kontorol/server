<?php
/**
 * Enable user-conscious 5-star rating
 * @package plugins.rating
 */

class RatingPlugin extends KontorolPlugin implements IKontorolPermissions, IKontorolServices
{
	const PLUGIN_NAME = 'rating';
	
	/* (non-PHPdoc)
    * @see IKontorolServices::getServicesMap()
    */
	public static function getServicesMap()
	{
		$map = array(
			'rating' => 'RatingService',
		);
		return $map;
	}
	
	/* (non-PHPdoc)
	 * @see IKontorolPermissions::isAllowedPartner()
	 */
	public static function isAllowedPartner($partnerId)
	{
		$partner = PartnerPeer::retrieveByPK($partnerId);
		
		return $partner->getPluginEnabled(self::PLUGIN_NAME);
	}
	
	
	/* (non-PHPdoc)
     * @see IKontorolPlugin::getPluginName()
     */
	public static function getPluginName()
	{
		return self::PLUGIN_NAME;
	}
}

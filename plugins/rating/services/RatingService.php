<?php
/**
 * Allows user to manipulate their entry rating
 *
 * @service rating
 * @package plugins.rating
 * @subpackage api.services
 */

class RatingService extends KontorolBaseService
{
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		
		if(!RatingPlugin::isAllowedPartner($this->getPartnerId()))
		{
			throw new KontorolAPIException(KontorolErrors::FEATURE_FORBIDDEN, RatingPlugin::PLUGIN_NAME);
		}
		
		if (kCurrentContext::$ks_object->isAnonymousSession() && $actionName !== 'getRatingCounts')
		{
			throw new KontorolAPIException(KontorolErrors::ANONYMOUS_ACCESS_FORBIDDEN);
		}
	}
	
	/**
	 * @action rate
	 * Action for current kuser ro rate a specific entry
	 * @param string $entryId
	 * @param int $rank
	 * @throws KontorolErrors::ENTRY_ID_NOT_FOUND
	 * @throws KontorolErrors::INVALID_RANK_VALUE
	 * @return int
	 */
	public function rateAction ($entryId, $rank)
	{
		$dbEntry = entryPeer::retrieveByPK($entryId);
		if (!$dbEntry)
		{
			throw new KontorolAPIException(KontorolErrors::ENTRY_ID_NOT_FOUND, $entryId);
		}
		
		if ($rank <= 0 || $rank > 5)
		{
			throw new KontorolAPIException(KontorolErrors::INVALID_RANK_VALUE);
		}
		
		//Check if a kvote for current entryId and kuser already exists.
		$existingKVote = kvotePeer::doSelectByEntryIdAndPuserId($entryId, $this->getPartnerId(), kCurrentContext::$ks_uid);
		if ($existingKVote)
		{
			$existingKVote->setRank($rank);
			$existingKVote->save();
		}
		else
		{
			kvotePeer::createKvote($entryId, $this->getPartnerId(), kCurrentContext::$ks_uid, $rank, KVoteType::RANK);
		}
		
		return $rank;
	}
	
	/**
	 * @action removeRating
	 * Action to remove current kuser's rating for a specific entry
	 * @param string $entryId
	 * @throws KontorolErrors::ENTRY_ID_NOT_FOUND
	 * @throws KontorolRatingErrors::USER_RATING_FOR_ENTRY_NOT_FOUND
	 * @return bool
	 */
	public function removeRatingAction ($entryId)
	{
		$dbEntry = entryPeer::retrieveByPK($entryId);
		if (!$dbEntry)
		{
			throw new KontorolAPIException(KontorolErrors::ENTRY_ID_NOT_FOUND, $entryId);
		}
		
		$existingKVote = kvotePeer::doSelectByEntryIdAndPuserId($entryId, $this->getPartnerId(), kCurrentContext::$ks_uid);
		if (!$existingKVote)
		{
			throw new KontorolAPIException(KontorolRatingErrors::USER_RATING_FOR_ENTRY_NOT_FOUND);
		}
		
		
		if (kvotePeer::disableExistingKVote($entryId, $this->getPartnerId(), kCurrentContext::$ks_uid))
		{
			return true;
		}
		
		return false;
	}
	
	/**
	 * @action checkRating
	 * Action to check current kuser's rating for a specific entry
	 * @param string $entryId
	 * @throws KontorolErrors::ENTRY_ID_NOT_FOUND
	 * @return int
	 */
	public function checkRatingAction ($entryId)
	{
		if (!$entryId)
		{
			throw new KontorolAPIException(KontorolErrors::MISSING_MANDATORY_PARAMETER, 'entryId');
		}
		
		$dbEntry = entryPeer::retrieveByPK($entryId);
		if (!$dbEntry)
		{
			throw new KontorolAPIException(KontorolErrors::ENTRY_ID_NOT_FOUND, $entryId);
		}
		
		$existingKVote = kvotePeer::doSelectByEntryIdAndPuserId($entryId, $this->getPartnerId(), kCurrentContext::$ks_uid);
		if (!$existingKVote)
		{
			throw new KontorolAPIException(KontorolRatingErrors::USER_RATING_FOR_ENTRY_NOT_FOUND);
		}
		
		return $existingKVote->getRank();
		
	}
	
	
	/**
	 * @action getRatingCounts
	 * Action to check entry's rating counts breakdown
	 * @param KontorolRatingCountFilter $filter
	 * @throws KontorolErrors::ENTRY_ID_NOT_FOUND
	 * @throws KontorolRatingErrors::MUST_FILTER_BY_RANK
	 * @return KontorolRatingCountListResponse
	 */
	public function getRatingCountsAction (KontorolRatingCountFilter $filter)
	{
		if(!$filter->entryIdEqual || !entryPeer::retrieveByPK($filter->entryIdEqual))
		{
			throw new KontorolAPIException(KontorolErrors::ENTRY_ID_NOT_FOUND, $filter->entryIdEqual);
		}
		
		if(!$filter->rankIn)
		{
			throw new KontorolAPIException(KontorolRatingErrors::MUST_FILTER_BY_RANK);
		}
		
		return $filter->getListResponse(new KontorolFilterPager(), null);
	}
}

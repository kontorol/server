<?php
/**
 * @package plugins.rating
 * @subpackage api.filters
 */
class KontorolRatingCountFilter extends KontorolRatingCountBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	public function getCoreFilter()
	{
		return new RatingFilter();
	}
	
	/* (non-PHPdoc)
	 * @see KontorolRelatedFilter::getListResponse()
	 */
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		//Logically, it is impossible to end up with more than 30 results in this case since there are only 5 rank values available.
		$results = kRatingPeer::countEntryKvotesByRank($this->entryIdEqual, explode(',', $this->rankIn));
		
		$response = new KontorolRatingCountListResponse();
		
		$response->totalCount = count($results);
		$response->objects = KontorolRatingCountArray::fromDbArray($results, $responseProfile);
		
		return $response;
	}
}

<?php
/**
 * @package plugins.rating
 * @subpackage api.objects
 * @relatedService RatingService
 */

class KontorolRatingCountListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolRatingCountArray
	 * @readonly
	 */
	public $objects;
}

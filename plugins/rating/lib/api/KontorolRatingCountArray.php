<?php
/**
 * @package plugins.rating
 * @subpackage api.objects
 */
class KontorolRatingCountArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolRatingCountArray();
		if ($arr == null)
			return $newArr;
		
		foreach ($arr as $obj)
		{
			$nObj = new KontorolRatingCount();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct()
	{
		parent::__construct('KontorolRatingCount');
	}
}

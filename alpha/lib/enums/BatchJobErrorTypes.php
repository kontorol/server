<?php
/**
 * @package Core
 * @subpackage model.enum
 */ 
interface BatchJobErrorTypes extends BaseEnum
{
	const APP = 0;
	const RUNTIME = 1;
	const HTTP = 2; // codes list could be found in http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
	const CURL = 3; // codes list could be found in http://curl.haxx.se/libcurl/c/libcurl-errors.html
	const KONTOROL_API = 4;
	const KONTOROL_CLIENT = 5;
}

<?php

/**
 * @package Core
 * @subpackage model.enum
 */ 
interface XsltParameterName extends BaseEnum
{
    const KONTOROL_HAS_NEXT_ITEM = 'KontorolHasNextItem';
    const KONTOROL_CURRENT_TIMESTAMP = 'KontorolCurrentTimestamp';
    const KONTOROL_SYNDICATION_FEED_FLAVOR_PARAM_ID = 'KontorolSyndicationFeedFlavorParamId';
    const KONTOROL_SYNDICATION_FEED_PARTNER_ID = 'partnerId';
}

<?php
/**
 * @package api
 * @subpackage filters
 */
class kEntryIndexAdvancedFilter extends kIndexAdvancedFilter
{

	/* (non-PHPdoc)
	 * @see AdvancedSearchFilterItem::applyCondition()
	 */
	public function applyCondition(IKontorolDbQuery $query)
	{
		$this->applyConditionImpl($query, "int_id");
	}	
}

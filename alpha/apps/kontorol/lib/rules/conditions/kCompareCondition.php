<?php
/**
 * @package Core
 * @subpackage model.data
 * @abstract
 */
abstract class kCompareCondition extends kCondition
{
	/**
	 * Value to evaluate against the field and operator
	 * @var kIntegerValue
	 */
	protected $value;
	
	/**
	 * Comparing operator, enum of searchConditionComparison
	 * @var int
	 */
	protected $comparison;

	/**
	 * @return kIntegerValue
	 */
	public function getValue() 
	{
		return $this->value;
	}

	/**
	 * Comparing operator, enum of searchConditionComparison
	 * @return int
	 */
	public function getComparison() 
	{
		return $this->comparison;
	}

	/**
	 * @param kIntegerValue $value
	 */
	public function setValue(kIntegerValue $value) 
	{
		$this->value = $value;
	}

	/**
	 * Comparing operator, enum of searchConditionComparison
	 * @param int $comparison
	 */
	public function setComparison($comparison) 
	{
		$this->comparison = $comparison;
	}

	/**
	 * Return single integer or array of integers
	 * @param kScope $scope
	 * @return int|array<int> the field content
	 */
	abstract public function getFieldValue(kScope $scope);
	
	/**
	 * @return int
	 */
	function getIntegerValue($scope)
	{
		if(is_object($this->value))
		{
			if($this->value instanceof kIntegerField)
				$this->value->setScope($scope);
				
			return $this->value->getValue();
		}
		
		return intval($this);
	}
	
	/**
	 * @param int $field
	 * @param int $value
	 * @return bool
	 */
	protected function fieldFulfilled($field, $value)
	{
		switch($this->comparison)
		{
			case searchConditionComparison::GREATER_THAN:
				KontorolLog::debug("Compares field[$field] > value[$value]");
				return ($field > $value);
				
			case searchConditionComparison::GREATER_THAN_OR_EQUAL:
				KontorolLog::debug("Compares field[$field] >= value[$value]");
				return ($field >= $value);
				
			case searchConditionComparison::LESS_THAN:
				KontorolLog::debug("Compares field[$field] < value[$value]");
				return ($field < $value);
				
			case searchConditionComparison::LESS_THAN_OR_EQUAL:
				KontorolLog::debug("Compares field[$field] <= value[$value]");
				return ($field <= $value);
				
			case searchConditionComparison::EQUAL:
			default:
				KontorolLog::debug("Compares field[$field] == value[$value]");
				return ($field == $value);
		}
	}
	
	/* (non-PHPdoc)
	 * @see kCondition::internalFulfilled()
	 */
	protected function internalFulfilled(kScope $scope)
	{
		$field = $this->getFieldValue($scope);
		$value = $this->getIntegerValue($scope);
		
		KontorolLog::debug('Compares field [ ' . print_r($field, true) . " ] to value [$value]");
		if (is_null($value))
		{
			KontorolLog::debug("Value is null, condition is true");
			return true;
		}
		
		if (!$field)
		{
			KontorolLog::debug("Field is empty, condition is false");
			return false;
		}

		if(is_array($field))
		{
			foreach($field as $fieldItem)
			{
				if(!$this->fieldFulfilled($fieldItem, $value))
				{
					KontorolLog::debug("Field item [$fieldItem] does not fulfill, condition is false");
					return false;
				}
			}
			KontorolLog::debug("All field items fulfilled, condition is true");
			return true;
		}
		
		return $this->fieldFulfilled($field, $value);
	}

	/* (non-PHPdoc)
	 * @see kCondition::shouldDisableCache()
	 */
	public function shouldDisableCache($scope)
	{
		return (is_object($this->value) && $this->value->shouldDisableCache($scope)) ||
			$this->shouldFieldDisableCache($scope);
	}

	/**
	 * @param kScope $scope
	 * @return bool
	 */
	public function shouldFieldDisableCache($scope)
	{
		return true;
	}
}

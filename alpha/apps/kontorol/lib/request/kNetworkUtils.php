<?php
/**
 * @package server-infra
 * @subpackage request
 */
class kNetworkUtils
{
	const KONTOROL_AUTH_HEADER = 'HTTP_X_KONTOROL_AUTH';
	const DEFAULT_AUTH_HEADER_VERSION = 1;
	const AUTH_HEADER_TIMESTAMP_MARGIN = 60; //we allow 60 seconds diff in authentication time stamp value to overcome any clock sync issues
	
	/**
	 * @return bool
	 * @throws Exception
	 */
	public static function isAuthenticatedURI()
	{
		if (!isset($_SERVER[self::KONTOROL_AUTH_HEADER]))
		{
			KontorolLog::warning("Missing Header Parameter - ". self::KONTOROL_AUTH_HEADER);
			return false;
		}
		$xKontorolAuth = $_SERVER[self::KONTOROL_AUTH_HEADER];
		$parts = explode(',', $xKontorolAuth);
		if (count($parts) != 3)
		{
			KontorolLog::warning('Invalid Fromat for ' . self::KONTOROL_AUTH_HEADER);
			return false;
		}

		$version = $parts[0];
		$timestamp = $parts[1];
		$expectedSignature = $parts[2];
		
		$currentTimestamp = time();
		if( !is_numeric($timestamp) || abs($currentTimestamp - $timestamp) > self::AUTH_HEADER_TIMESTAMP_MARGIN )
		{
			KontorolLog::warning("Failed to validate signature, timestamp [$timestamp] currentTimestamp [$currentTimestamp]");
			return false;
		}

		$currentTimestamp = time();
		if( !is_numeric($timestamp) || abs($currentTimestamp - $timestamp) > self::AUTH_HEADER_TIMESTAMP_MARGIN )
		{
			KontorolLog::warning("Failed to validate signature, timestamp [$timestamp] currentTimestamp [$currentTimestamp]");
			return false;
		}

		$currentTimestamp = time();
		if( !is_numeric($timestamp) || abs($currentTimestamp - $timestamp) > self::AUTH_HEADER_TIMESTAMP_MARGIN )
		{
			KontorolLog::warning("Failed to validate signature, timestamp [$timestamp] currentTimestamp [$currentTimestamp]");
			return false;
		}

		$url = substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], '?'));

		$actualSignature = self::calculateSignature($version, $timestamp, $url);
		if(!$actualSignature)
		{
			return false;
		}
		KontorolLog::debug("Actual Signature [$actualSignature] - Expected Signature [$expectedSignature]" );
		if ( $actualSignature !== $expectedSignature)
		{
			KontorolLog::warning("Could not authenticate X-Kontorol-Auth");
			return false;
		}

		return true;
	}

	public static function calculateSignature($version, $timestamp, $url)
	{
		$secret = kConf::get('vod_packager_authentication_secret','local', null);
		if (!$secret)
		{
			KontorolLog::warning("Missing authentication_secret in configuration");
			return '';
		}

		KontorolLog::debug("Calculating signature for version [$version], time [$timestamp], url [$url]");
		return base64_encode(hash_hmac('sha256', "$version,$timestamp,$url", $secret, true));
	}
}

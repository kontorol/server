<?php
/**
 * @package Core
 * @subpackage model.filters.advanced
 */ 
class AdvancedSearchFilterItem
{
	/**
	 * @var string
	 */
	protected $kontorolClass;
	
	public $filterLimit;
	
	public $overrideFilterLimit;

	final public function apply(baseObjectFilter $filter, IKontorolDbQuery $query)
	{
		if($this->overrideFilterLimit)
		{
			$filter->setLimit($this->overrideFilterLimit);
		}
		$this->filterLimit = $filter->getLimit();
		$this->applyCondition($query);
	}
	
	public function getFreeTextConditions($partnerScope, $freeTexts)
	{
		return array();	
	}
	
	/**
	 * Adds conditions, matches and where clauses to the query
	 * @param IKontorolIndexQuery $query
	 */
	public function applyCondition(IKontorolDbQuery $query)
	{
	}
	
	public function addToXml(SimpleXMLElement &$xmlElement)
	{
		$xmlElement->addAttribute('kontorolClass', $this->kontorolClass);
	}
	
	public function fillObjectFromXml(SimpleXMLElement $xmlElement)
	{
		$attr = $xmlElement->attributes();
		if(isset($attr['kontorolClass']))
			$this->kontorolClass = (string) $attr['kontorolClass'];
	}
	
	/**
	 * @return the $kontorolClass
	 */
	public function getKontorolClass() {
		return $this->kontorolClass;
	}

	/**
	 * @param $kontorolClass the $kontorolClass to set
	 */
	public function setKontorolClass($kontorolClass) {
		$this->kontorolClass = $kontorolClass;
	}
}

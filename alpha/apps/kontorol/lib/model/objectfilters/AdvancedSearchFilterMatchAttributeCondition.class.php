<?php
/**
 * @package Core
 * @subpackage model.filters.advanced
 */ 
class AdvancedSearchFilterMatchAttributeCondition extends AdvancedSearchFilterMatchCondition
{
	/**
	 * Adds conditions, matches and where clauses to the query
	 * @param IKontorolDbQuery $query
	 */
	public function applyCondition(IKontorolDbQuery $query)
	{
		if (!$query instanceof IKontorolIndexQuery)
			return;

		$matchText = '"'.KontorolCriteria::escapeString($this->value).'"';
		if ($this->not)
			$matchText = '!'.$matchText;
		$query->addMatch("@$this->field (".$matchText.")");
	}
}

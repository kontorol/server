<?php
/**
 * @package Core
 * @subpackage model.filters.advanced
 */ 
class AdvancedSearchFilterComparableCondition extends AdvancedSearchFilterCondition
{
	/**
	 * @var KontorolSearchConditionComparison
	 */
	public $comparison;

	/**
	 * Adds conditions, matches and where clauses to the query
	 * @param IKontorolIndexQuery $query
	 */
	public function applyCondition(IKontorolDbQuery $query)
	{
		switch ($this->getComparison())
		{
			case KontorolSearchConditionComparison::EQUAL:
				$comparison = ' = ';
				break;
			case KontorolSearchConditionComparison::GREATER_THAN:
				$comparison = ' > ';
				break;
			case KontorolSearchConditionComparison::GREATER_THAN_OR_EQUAL:
				$comparison = ' >= ';
				break;
			case KontorolSearchConditionComparison::LESS_THAN:
				$comparison = " < ";
				break;
			case KontorolSearchConditionComparison::LESS_THAN_OR_EQUAL:
				$comparison = " <= ";
				break;
			case KontorolSearchConditionComparison::NOT_EQUAL:
				$comparison = " <> ";
				break;
			default:
				KontorolLog::err("Missing comparison type");
				return;
		}

		$field = $this->getField();
		$value = $this->getValue();
		$fieldValue = $this->getFieldValue($field);
		if (is_null($fieldValue))
		{
			KontorolLog::err('Unknown field [' . $field . ']');
			return;
		}

		$newCondition = $fieldValue . $comparison . KontorolCriteria::escapeString($value);

		$query->addCondition($newCondition);
	}

	protected function getFieldValue($field)
	{
		$fieldValue = null;
		switch($field)
		{
			case Criteria::CURRENT_DATE:
				$d = getdate();
				$fieldValue = mktime(0, 0, 0, $d['mon'], $d['mday'], $d['year']);
				break;

			case Criteria::CURRENT_TIME:
			case Criteria::CURRENT_TIMESTAMP:
				$fieldValue = time();
				break;
		}
		return $fieldValue ;
	}

	public function addToXml(SimpleXMLElement &$xmlElement)
	{
		parent::addToXml($xmlElement);
		
		$xmlElement->addAttribute('comparison', htmlspecialchars($this->comparison));
	}
	
	public function fillObjectFromXml(SimpleXMLElement $xmlElement)
	{
		parent::fillObjectFromXml($xmlElement);
		
		$attr = $xmlElement->attributes();
		if(isset($attr['comparison']))
			$this->comparison = (string) html_entity_decode($attr['comparison']);
	}
	
	/**
	 * @return KontorolSearchConditionComparison $comparison
	 */
	public function getComparison() {
		return $this->comparison;
	}

	/**
	 * @param KontorolSearchConditionComparison $comparison the $comparison to set
	 */
	public function setComparison($comparison) {
		$this->comparison = $comparison;
	}
}

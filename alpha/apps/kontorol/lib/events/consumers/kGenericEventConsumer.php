<?php
/**
 * Consume any type of event
 * @package Core
 * @subpackage events
 */
interface kGenericEventConsumer extends KontorolEventConsumer
{
	/**
	 * @param KontorolEvent $event
	 * @return bool true if should continue to the next consumer
	 */
	public function consumeEvent(KontorolEvent $event);
	
	/**
	 * @param KontorolEvent $event
	 * @return bool true if the consumer should handle the event
	 */
	public function shouldConsumeEvent(KontorolEvent $event);
}

<?php
/**
 * @package Core
 * @subpackage events
 */
class kObjectErasedEvent extends KontorolEvent implements IKontorolDatabaseEvent, IKontorolObjectRelatedEvent
{
    const EVENT_CONSUMER = 'kObjectErasedEventConsumer';
	
	/**
	 * @var BaseObject
	 */
	private $object;
    
	/* (non-PHPdoc)
     * @see KontorolEvent::doConsume()
     */
    protected function doConsume (KontorolEventConsumer $consumer)
    {
        if(!$consumer->shouldConsumeErasedEvent($this->object))
			return true;
			
		$additionalLog = '';
		if(method_exists($this->object, 'getId'))
			$additionalLog .= 'id [' . $this->object->getId() . ']';
			
		KontorolLog::debug('consumer [' . get_class($consumer) . '] started handling [' . get_class($this) . '] object type [' . get_class($this->object) . '] ' . $additionalLog);
		$result = $consumer->objectErased($this->object);
		KontorolLog::debug('consumer [' . get_class($consumer) . '] finished handling [' . get_class($this) . '] object type [' . get_class($this->object) . '] ' . $additionalLog);
		return $result;
    }

	/* (non-PHPdoc)
     * @see KontorolEvent::getConsumerInterface()
     */
    public function getConsumerInterface ()
    {
        return self::EVENT_CONSUMER;
        
    }
    
    public function __construct(BaseObject $object)
	{
		$this->object = $object;
		
		$additionalLog = '';
		if(method_exists($object, 'getId'))
			$additionalLog .= ' id [' . $object->getId() . ']';
			
		KontorolLog::debug("Event [" . get_class($this) . "] object type [" . get_class($object) . "]" . $additionalLog);
	}
	
	public function getKey()
	{
		if(method_exists($this->object, 'getId'))
			return get_class($this->object).$this->object->getId();
		
		return null;
	}
	
	/**
	 * @return BaseObject $object
	 */
	public function getObject() 
	{
		return $this->object;
	}
	
	/* (non-PHPdoc)
	 * @see KontorolEvent::getScope()
	 */
	public function getScope()
	{
		$scope = parent::getScope();
		if(method_exists($this->object, 'getPartnerId'))
			$scope->setPartnerId($this->object->getPartnerId());
			
		return $scope;
	}
}

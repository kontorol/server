<?php
/**
 * Marker interface, marks cancelable event as able to continue by the consumer
 * @package Core
 * @subpackage events
 */
interface IKontorolContinualEvent extends IKontorolCancelableEvent
{
	
}

<?php
/**
 * @package    Core
 * @subpackage KMC
 */
class varloginAction extends kontorolAction
{
	public function execute ( ) 
	{
		$this->beta = $this->getRequestParameter( "beta" );
		$this->kmc_login_version 	= kConf::get('kmc_login_version');
				
		sfView::SUCCESS;
	}
}

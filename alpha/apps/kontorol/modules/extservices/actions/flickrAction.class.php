<?php

/**
 * extservices actions.
 *
 * @package    Core
 * @subpackage externalServices
 */
class flickrAction extends kontorolAction
{
	public function execute()
	{
		$this->followRedirectCookie();
		
		$frob = @$_REQUEST['frob'];
			
		$kont_token = @$_COOKIE['flickr_konttoken'];

		if (!$kont_token)
		{
			$kuserId = $this->getLoggedInUserId();
			if ($kuserId)
				$kont_token = $kuserId.':';
		}
		else
			$kont_token = base64_decode($kont_token);

		if (!$frob || !$kont_token)
			return;

		myFlickrServices::setKuserToken($kont_token, $frob);

		return;
	}
}

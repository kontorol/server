<?php if ( $name != "" ) {echo "Dear ".$name;} else {echo "Hello";}  ?>, 

You have requested a password reset for your Kontorol account.

In order to reset your password, please browse to:

http://www.kontorol.com/index.php/login/signinTemp?p=<?php echo $tempCode ?>

If you have not initiated this request please notify us by email to mailto:security@kontorol.com

Sincerely, 
Kontorol Customer Service



Hi <?php echo $name ?> , 

Welcome, and thanks for joining Kontorol - where you can create and edit videos together with  others.
We’re excited to have you!

Your Kontorol account is now fully activated.

View your profile:
http://www.kontorol.com/mykontorol

See what’s new on Kontorol:
http://www.kontorol.com/index.php


Questions about your account or about Kontorol?
Go to: http://www.kontorol.com/help or email us at: support@kontorol.com

Kind regards,
The Kontorol Team



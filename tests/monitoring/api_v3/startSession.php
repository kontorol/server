<?php
$config = array();
$client = null;
/* @var $client KontorolClient */
require_once __DIR__  . '/common.php';

$options = getopt('', array(
	'service-url:',
	'debug',
	'ks-type:',
));

$secretField = 'secret';
if(isset($options['ks-type']) && $options['ks-type'] == 'admin')
	$secretField = 'adminSecret';

$start = microtime(true);
$monitorResult = new KontorolMonitorResult();
$apiCall = null;
try
{
	$apiCall = 'session.start';
	$ks = $client->session->start($config['monitor-partner'][$secretField], 'monitor-user', KontorolSessionType::USER, $config['monitor-partner']['id']);
	$end = microtime(true);
	
	$monitorResult->executionTime = $end - $start;
	$monitorResult->value = $monitorResult->executionTime;
	$monitorResult->description = "Start session execution time: $monitorResult->value seconds";
}
catch(KontorolException $e)
{
	$end = microtime(true);
	$monitorResult->executionTime = $end - $start;
	
	$error = new KontorolMonitorError();
	$error->code = $e->getCode();
	$error->description = $e->getMessage();
	$error->level = KontorolMonitorError::ERR;
	
	$monitorResult->errors[] = $error;
	$monitorResult->description = "Exception: " . get_class($e) . ", API: $apiCall, Code: " . $e->getCode() . ", Message: " . $e->getMessage();
}
catch(KontorolClientException $ce)
{
	$end = microtime(true);
	$monitorResult->executionTime = $end - $start;
	
	$error = new KontorolMonitorError();
	$error->code = $ce->getCode();
	$error->description = $ce->getMessage();
	$error->level = KontorolMonitorError::CRIT;
	
	$monitorResult->errors[] = $error;
	$monitorResult->description = "Exception: " . get_class($ce) . ", API: $apiCall, Code: " . $ce->getCode() . ", Message: " . $ce->getMessage();
}

echo "$monitorResult";
exit(0);

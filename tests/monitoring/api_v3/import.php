<?php
$config = array();
$client = null;
$serviceUrl = null;
/* @var $client KontorolClient */
require_once __DIR__  . '/common.php';

$options = getopt('', array(
	'service-url:',
	'debug',
	'timeout:',
	'media-url:',
));

if(!isset($options['timeout']))
{
	echo "Argument timeout is required";
	exit(-1);
}
$timeout = $options['timeout'];

$mediaUrl = $serviceUrl . '/content/templates/entry/data/kontorol_logo_animated_blue.flv';
if(isset($options['media-url']))
	$mediaUrl = $options['media-url'];

$start = microtime(true);
$monitorResult = new KontorolMonitorResult();
$apiCall = null;
try
{
	$apiCall = 'session.start';
	$ks = $client->session->start($config['monitor-partner']['secret'], 'monitor-user', KontorolSessionType::USER, $config['monitor-partner']['id']);
	$client->setKs($ks);
		
	 // Creates a new entry
	$entry = new KontorolMediaEntry();
	$entry->name = 'monitor-test';
	$entry->description = 'monitor-test';
	$entry->mediaType = KontorolMediaType::VIDEO;
	
	$resource = new KontorolUrlResource();
	$resource->url = $mediaUrl;
	
	$apiCall = 'multirequest';
	$client->startMultiRequest();
	$requestEntry = $client->media->add($entry);
	/* @var $requestEntry KontorolMediaEntry */
	$client->media->addContent($requestEntry->id, $resource);
	$client->media->get($requestEntry->id);
	
	$results = $client->doMultiRequest();
	foreach($results as $index => $result)
	{
		if ($client->isError($result))
			throw new KontorolException($result["message"], $result["code"]);
	}
		
	// Waits for the entry to start conversion
	$createdEntry = end($results);
	$timeoutTime = time() + $timeout;
	/* @var $createdEntry KontorolMediaEntry */
	while ($createdEntry)
	{
		if(time() > $timeoutTime)
			throw new Exception("timed out, entry id: $createdEntry->id");
			
		if($createdEntry->status == KontorolEntryStatus::IMPORT)
		{
			sleep(1);
			$apiCall = 'media.get';
			$createdEntry = $client->media->get($createdEntry->id);
			continue;
		}
		
		$monitorResult->executionTime = microtime(true) - $start;
		$monitorResult->value = $monitorResult->executionTime;
		
		if($createdEntry->status == KontorolEntryStatus::READY || $createdEntry->status == KontorolEntryStatus::PRECONVERT)
		{
			$monitorResult->description = "import time: $monitorResult->executionTime seconds";
		}
		elseif($createdEntry->status == KontorolEntryStatus::ERROR_IMPORTING)
		{
			$error = new KontorolMonitorError();
			$error->description = "import failed, entry id: $createdEntry->id";
			$error->level = KontorolMonitorError::CRIT;
			
			$monitorResult->errors[] = $error;
			$monitorResult->description = "import failed, entry id: $createdEntry->id";
		}
		else
		{
			$error = new KontorolMonitorError();
			$error->description = "unexpected entry status: $createdEntry->status, entry id: $createdEntry->id";
			$error->level = KontorolMonitorError::CRIT;
			
			$monitorResult->errors[] = $error;
			$monitorResult->description = "unexpected entry status: $createdEntry->status, entry id: $createdEntry->id";
		}
		
		break;
	}

	try
	{
		$apiCall = 'media.delete';
		$createdEntry = $client->media->delete($createdEntry->id);
	}
	catch(Exception $ex)
	{
		$error = new KontorolMonitorError();
		$error->code = $ex->getCode();
		$error->description = $ex->getMessage();
		$error->level = KontorolMonitorError::WARN;
		
		$monitorResult->errors[] = $error;
	}
}
catch(KontorolException $e)
{
	$monitorResult->executionTime = microtime(true) - $start;
	
	$error = new KontorolMonitorError();
	$error->code = $e->getCode();
	$error->description = $e->getMessage();
	$error->level = KontorolMonitorError::ERR;
	
	$monitorResult->errors[] = $error;
	$monitorResult->description = "Exception: " . get_class($e) . ", API: $apiCall, Code: " . $e->getCode() . ", Message: " . $e->getMessage();
}
catch(KontorolClientException $ce)
{
	$monitorResult->executionTime = microtime(true) - $start;
	
	$error = new KontorolMonitorError();
	$error->code = $ce->getCode();
	$error->description = $ce->getMessage();
	$error->level = KontorolMonitorError::CRIT;
	
	$monitorResult->errors[] = $error;
	$monitorResult->description = "Exception: " . get_class($ce) . ", API: $apiCall, Code: " . $ce->getCode() . ", Message: " . $ce->getMessage();
}
catch(Exception $ex)
{
	$monitorResult->executionTime = microtime(true) - $start;
	
	$error = new KontorolMonitorError();
	$error->code = $ex->getCode();
	$error->description = $ex->getMessage();
	$error->level = KontorolMonitorError::ERR;
	
	$monitorResult->errors[] = $error;
	$monitorResult->description = $ex->getMessage();
}

echo "$monitorResult";
exit(0);

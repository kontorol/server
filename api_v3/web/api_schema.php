<?php

require_once(__DIR__ . "/../bootstrap.php");

$root = myContentStorage::getFSContentRootPath();
$outputPathBase = "$root/content/clientlibs";

$fileLocation = "$outputPathBase/KontorolClient.xml";

if (!file_exists($fileLocation))
	die("KontorolClient.xml was not found");
	
header("Content-Type: text/xml");
readfile($fileLocation);

<?php

header('Access-Control-Expose-Headers: Server, Content-Length, Content-Range, Date, X-Kontorol, X-Kontorol-Session, X-Me');

if($_SERVER['REQUEST_METHOD'] == 'OPTIONS')
{
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Range, Cache-Control');
	header('Access-Control-Allow-Methods: POST, GET, HEAD, OPTIONS');
	header('Access-Control-Max-Age: 86400');
	exit;
}

$start = microtime(true);
// check cache before loading anything
require_once(dirname(__FILE__)."/../lib/KontorolResponseCacher.php");
$cache = new KontorolResponseCacher();
$cache->checkOrStart();

require_once(dirname(__FILE__)."/../bootstrap.php");

// Database
DbManager::setConfig(kConf::getDB());
DbManager::initialize();

ActKeyUtils::checkCurrent();

KontorolLog::debug(">------------------------------------- api_v3 -------------------------------------");
KontorolLog::info("API-start pid:".getmypid());
kInfraMemcacheCacheWrapper::outputStats();

$controller = KontorolFrontController::getInstance();
$result = $controller->run();

KontorolMonitorClient::monitorRequestEnd();

$end = microtime(true);
KontorolLog::info("API-end [".($end - $start)."]");
KontorolLog::debug("<------------------------------------- api_v3 -------------------------------------");

$cache->end($result);

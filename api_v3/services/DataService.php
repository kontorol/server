<?php

/**
 * Data service lets you manage data content (textual content)
 *
 * @service data
 * @package api
 * @subpackage services
 */
class DataService extends KontorolEntryService
{
	
	protected function kontorolNetworkAllowed($actionName)
	{
		if ($actionName === 'get') {
			return true;
		}
		return parent::kontorolNetworkAllowed($actionName);
	}
	
	
	/**
	 * Adds a new data entry
	 * 
	 * @action add
	 * @param KontorolDataEntry $dataEntry Data entry
	 * @return KontorolDataEntry The new data entry
	 */
	function addAction(KontorolDataEntry $dataEntry)
	{
		$dbEntry = $dataEntry->toObject(new entry());
		
		$this->checkAndSetValidUserInsert($dataEntry, $dbEntry);
		$this->checkAdminOnlyInsertProperties($dataEntry);
		$this->validateAccessControlId($dataEntry);
		$this->validateEntryScheduleDates($dataEntry, $dbEntry);
		
		$dbEntry->setPartnerId($this->getPartnerId());
		$dbEntry->setSubpId($this->getPartnerId() * 100);
		$dbEntry->setStatus(KontorolEntryStatus::READY);
		$dbEntry->setMediaType(entry::ENTRY_MEDIA_TYPE_AUTOMATIC); 
		$dbEntry->save();
		
		$trackEntry = new TrackEntry();
		$trackEntry->setEntryId($dbEntry->getId());
		$trackEntry->setTrackEventTypeId(TrackEntry::TRACK_ENTRY_EVENT_TYPE_ADD_ENTRY);
		$trackEntry->setDescription(__METHOD__ . ":" . __LINE__ . "::ENTRY_DATA");
		TrackEntry::addTrackEntry($trackEntry);
		
		$dataEntry->fromObject($dbEntry, $this->getResponseProfile());
		
		myNotificationMgr::createNotification(kNotificationJobData::NOTIFICATION_TYPE_ENTRY_ADD, $dbEntry);
		
		return $dataEntry;
	}
	
	/**
	 * Get data entry by ID.
	 * 
	 * @action get
	 * @param string $entryId Data entry id
	 * @param int $version Desired version of the data
	 * @return KontorolDataEntry The requested data entry
	 * 
	 * @throws KontorolErrors::ENTRY_ID_NOT_FOUND
	 */
	function getAction($entryId, $version = -1)
	{
		$dbEntry = entryPeer::retrieveByPK($entryId);

		if (!$dbEntry || $dbEntry->getType() != KontorolEntryType::DATA)
			throw new KontorolAPIException(KontorolErrors::ENTRY_ID_NOT_FOUND, $entryId);

		if ($version !== -1)
			$dbEntry->setDesiredVersion($version);
			
		$dataEntry = new KontorolDataEntry();
		$dataEntry->fromObject($dbEntry, $this->getResponseProfile());

		return $dataEntry;
	}
	
	/**
	 * Update data entry. Only the properties that were set will be updated.
	 * 
	 * @action update
	 * @param string $entryId Data entry id to update
	 * @param KontorolDataEntry $documentEntry Data entry metadata to update
	 * @return KontorolDataEntry The updated data entry
	 * 
	 * @throws KontorolErrors::ENTRY_ID_NOT_FOUND
	 * validateUser entry $entryId edit
	 */
	function updateAction($entryId, KontorolDataEntry $documentEntry)
	{
		return $this->updateEntry($entryId, $documentEntry, KontorolEntryType::DATA);
	}
	
	/**
	 * Delete a data entry.
	 *
	 * @action delete
	 * @param string $entryId Data entry id to delete
	 * 
 	 * @throws KontorolErrors::ENTRY_ID_NOT_FOUND
 	 * @validateUser entry entryId edit
	 */
	function deleteAction($entryId)
	{
		$this->deleteEntry($entryId, KontorolEntryType::DATA);
	}
	
	/**
	 * List data entries by filter with paging support.
	 * 
	 * @action list
     * @param KontorolDataEntryFilter $filter Document entry filter
	 * @param KontorolFilterPager $pager Pager
	 * @return KontorolDataListResponse Wrapper for array of document entries and total count
	 */
	function listAction(KontorolDataEntryFilter $filter = null, KontorolFilterPager $pager = null)
	{
	    if (!$filter)
			$filter = new KontorolDataEntryFilter();
			
	    $filter->typeEqual = KontorolEntryType::DATA;
	    list($list, $totalCount) = parent::listEntriesByFilter($filter, $pager);
	    
	    $newList = KontorolDataEntryArray::fromDbArray($list, $this->getResponseProfile());
		$response = new KontorolDataListResponse();
		$response->objects = $newList;
		$response->totalCount = $totalCount;
		return $response;
	}
	
	/**
	 * return the file from dataContent field.
	 * 
	 * @action serve
	 * @param string $entryId Data entry id
	 * @param int $version Desired version of the data
	 * @param bool $forceProxy force to get the content without redirect
	 * @return file
	 * 
	 * @throws KontorolErrors::ENTRY_ID_NOT_FOUND
	 */
	function serveAction($entryId, $version = -1, $forceProxy = false)
	{
		$dbEntry = entryPeer::retrieveByPK($entryId);

		if (!$dbEntry || $dbEntry->getType() != KontorolEntryType::DATA)
			throw new KontorolAPIException(KontorolErrors::ENTRY_ID_NOT_FOUND, $entryId);

		$ksObj = $this->getKs();
		$ks = ($ksObj) ? $ksObj->getOriginalString() : null;
		$securyEntryHelper = new KSecureEntryHelper($dbEntry, $ks, null, ContextType::DOWNLOAD);
		$securyEntryHelper->validateForDownload();	
		
		if ( ! $version || $version == -1 ) $version = null;
		
		$fileName = $dbEntry->getName();
		
		$syncKey = $dbEntry->getSyncKey( kEntryFileSyncSubType::DATA , $version);
		list($fileSync, $local) = kFileSyncUtils::getReadyFileSyncForKey($syncKey, true, false);
		
		header("Content-Disposition: attachment; filename=\"$fileName\"");

		if($local)
		{
			$filePath = $fileSync->getFullPath();
			$mimeType = kFile::mimeType($filePath);
			$key = $fileSync->isEncrypted() ? $fileSync->getEncryptionKey() : null;
			$iv = $key ? $fileSync->getIv() : null;
			return $this->dumpFile($filePath, $mimeType, $key, $iv);
		}
		else
		{
			$remoteUrl = kDataCenterMgr::getRedirectExternalUrl($fileSync);
			KontorolLog::info("Redirecting to [$remoteUrl]");
			if($forceProxy)
			{
				kFileUtils::dumpUrl($remoteUrl);
			}
			else
			{
				// or redirect if no proxy
				header("Location: $remoteUrl");
				die;
			}
		}	
	}


	/**
	* Update the dataContent of data entry using a resource
	*
	* @action addContent
	* @param string $entryId
	* @param KontorolGenericDataCenterContentResource $resource
	* @return string
	* @throws KontorolErrors::ENTRY_ID_NOT_FOUND
	* @validateUser entry entryId edit
	*/
	function addContentAction($entryId, KontorolGenericDataCenterContentResource $resource)
	{
		$dbEntry = entryPeer::retrieveByPK($entryId);

		if (!$dbEntry)
			throw new KontorolAPIException(KontorolErrors::ENTRY_ID_NOT_FOUND, $entryId);

		if ($dbEntry->getType() != KontorolEntryType::DATA)
			throw new KontorolAPIException(KontorolErrors::INVALID_ENTRY_TYPE,$entryId, $dbEntry->getType(), entryType::DATA);

		$resource->validateEntry($dbEntry);
		$kResource = $resource->toObject();
		$this->attachResource($kResource, $dbEntry);
		$resource->entryHandled($dbEntry);

		return $this->getEntry($entryId);
	}

	/**
	* @param kResource $resource
	* @param entry $dbEntry
	* @param asset $dbAsset
	* @return asset
	*/
	protected function attachResource(kResource $resource, entry $dbEntry, asset $dbAsset = null)
	{
		if(($resource->getType() == 'kLocalFileResource') && (!isset($resource->getSourceType) ||  $resource->getSourceType != KontorolSourceType::WEBCAM))
		{
			$file_path = $resource->getLocalFilePath();
			$fileType = kFile::mimeType($file_path);
			if((substr($fileType, 0, 5) == 'text/') || ($fileType == 'application/xml')) {
				$dbEntry->setDataContent(kFile::getFileContent($file_path));
			}
			else{
				KontorolLog::err("Resource of type [" . get_class($resource) . "] with file type ". $fileType. " is not supported");
				throw new KontorolAPIException(KontorolErrors::FILE_TYPE_NOT_SUPPORTED, $fileType);
			}
		}
		else
		{
			KontorolLog::err("Resource of type [" . get_class($resource) . "] is not supported");
			throw new KontorolAPIException(KontorolErrors::RESOURCE_TYPE_NOT_SUPPORTED, get_class($resource));
		}
	}
}

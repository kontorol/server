<?php
/**
 * batch service lets you handle different batch process from remote machines.
 * As opposed to other objects in the system, locking mechanism is critical in this case.
 * For this reason the GetExclusiveXX, UpdateExclusiveXX and FreeExclusiveXX actions are important for the system's integrity.
 * In general - updating batch object should be done only using the UpdateExclusiveXX which in turn can be called only after 
 * acuiring a batch objet properly (using  GetExclusiveXX).
 * If an object was aquired and should be returned to the pool in it's initial state - use the FreeExclusiveXX action 
 *
 *	Terminology:
 *		LocationId
 *		ServerID
 *		ParternGroups 
 * 
 * @service jobs
 * @package api
 * @subpackage services
 */
class JobsService extends KontorolBaseService
{
	// use initService to add a peer to the partner filter
	/**
	 * @ignore
	 */
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		
		if($this->getPartnerId() != Partner::ADMIN_CONSOLE_PARTNER_ID && $this->getPartnerId() != Partner::BATCH_PARTNER_ID)
			$this->applyPartnerFilterForClass('BatchJob'); 	
	}
	
	
// --------------------------------- ImportJob functions 	--------------------------------- //
	
	
	/**
	 * batch getImportStatusAction returns the status of import task
	 * 
	 * @action getImportStatus
	 * @param int $jobId the id of the import job  
	 * @return KontorolBatchJobResponse
	 */
	function getImportStatusAction($jobId)
	{
		return $this->getStatusAction($jobId, KontorolBatchJobType::IMPORT);
	}
	
	
	/**
	 * batch deleteImportAction deletes and returns the status of import task
	 * 
	 * @action deleteImport
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function deleteImportAction($jobId)
	{
		return $this->deleteJobAction($jobId, KontorolBatchJobType::IMPORT);
	}
	
	
	/**
	 * batch abortImportAction aborts and returns the status of import task
	 * 
	 * @action abortImport
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function abortImportAction($jobId)
	{
		return $this->abortJobAction($jobId, KontorolBatchJobType::IMPORT);
	}
	
	
	/**
	 * batch retryImportAction retries and returns the status of import task
	 * 
	 * @action retryImport
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function retryImportAction($jobId)
	{
		return $this->retryJobAction($jobId, KontorolBatchJobType::IMPORT);
	}
	
	/**
// --------------------------------- ImportJob functions 	--------------------------------- //

	
	
	
// --------------------------------- ProvisionProvideJob functions 	--------------------------------- //
	
	
	/**
	 * batch getProvisionProvideStatusAction returns the status of ProvisionProvide task
	 * 
	 * @action getProvisionProvideStatus
	 * @param int $jobId the id of the ProvisionProvide job  
	 * @return KontorolBatchJobResponse
	 */
	function getProvisionProvideStatusAction($jobId)
	{
		return $this->getStatusAction($jobId, KontorolBatchJobType::PROVISION_PROVIDE);
	}
	
	
	/**
	 * batch deleteProvisionProvideAction deletes and returns the status of ProvisionProvide task
	 * 
	 * @action deleteProvisionProvide
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function deleteProvisionProvideAction($jobId)
	{
		return $this->deleteJobAction($jobId, KontorolBatchJobType::PROVISION_PROVIDE);
	}
	
	
	/**
	 * batch abortProvisionProvideAction aborts and returns the status of ProvisionProvide task
	 * 
	 * @action abortProvisionProvide
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function abortProvisionProvideAction($jobId)
	{
		return $this->abortJobAction($jobId, KontorolBatchJobType::PROVISION_PROVIDE);
	}
	
	
	/**
	 * batch retryProvisionProvideAction retries and returns the status of ProvisionProvide task
	 * 
	 * @action retryProvisionProvide
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function retryProvisionProvideAction($jobId)
	{
		return $this->retryJobAction($jobId, KontorolBatchJobType::PROVISION_PROVIDE);
	}
	
	/**
// --------------------------------- ProvisionProvideJob functions 	--------------------------------- //

	
	
// --------------------------------- ProvisionDeleteJob functions 	--------------------------------- //
	
	
	/**
	 * batch getProvisionDeleteStatusAction returns the status of ProvisionDelete task
	 * 
	 * @action getProvisionDeleteStatus
	 * @param int $jobId the id of the ProvisionDelete job  
	 * @return KontorolBatchJobResponse
	 */
	function getProvisionDeleteStatusAction($jobId)
	{
		return $this->getStatusAction($jobId, KontorolBatchJobType::PROVISION_DELETE);
	}
	
	
	/**
	 * batch deleteProvisionDeleteAction deletes and returns the status of ProvisionDelete task
	 * 
	 * @action deleteProvisionDelete
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function deleteProvisionDeleteAction($jobId)
	{
		return $this->deleteJobAction($jobId, KontorolBatchJobType::PROVISION_DELETE);
	}
	
	
	/**
	 * batch abortProvisionDeleteAction aborts and returns the status of ProvisionDelete task
	 * 
	 * @action abortProvisionDelete
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function abortProvisionDeleteAction($jobId)
	{
		return $this->abortJobAction($jobId, KontorolBatchJobType::PROVISION_DELETE);
	}
	
	
	/**
	 * batch retryProvisionDeleteAction retries and returns the status of ProvisionDelete task
	 * 
	 * @action retryProvisionDelete
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function retryProvisionDeleteAction($jobId)
	{
		return $this->retryJobAction($jobId, KontorolBatchJobType::PROVISION_DELETE);
	}
	
	/**
// --------------------------------- ProvisionDeleteJob functions 	--------------------------------- //

	
	
// --------------------------------- BulkUploadJob functions 	--------------------------------- //
	
	
	/**
	 * batch getBulkUploadStatusAction returns the status of bulk upload task
	 * 
	 * @action getBulkUploadStatus
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function getBulkUploadStatusAction($jobId)
	{
		return $this->getStatusAction($jobId, KontorolBatchJobType::BULKUPLOAD);
	}
	
	
	/**
	 * batch deleteBulkUploadAction deletes and returns the status of bulk upload task
	 * 
	 * @action deleteBulkUpload
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function deleteBulkUploadAction($jobId)
	{
		return $this->deleteJobAction($jobId, KontorolBatchJobType::BULKUPLOAD);
	}
	
	
	/**
	 * batch abortBulkUploadAction aborts and returns the status of bulk upload task
	 * 
	 * @action abortBulkUpload
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function abortBulkUploadAction($jobId)
	{
		return $this->abortJobAction($jobId, KontorolBatchJobType::BULKUPLOAD);
	}
	
	
	/**
	 * batch retryBulkUploadAction retries and returns the status of bulk upload task
	 * 
	 * @action retryBulkUpload
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function retryBulkUploadAction($jobId)
	{
		return $this->retryJobAction($jobId, KontorolBatchJobType::BULKUPLOAD);
	}
	

	
// --------------------------------- BulkUploadJob functions 	--------------------------------- //

	
	
// --------------------------------- ConvertJob functions 	--------------------------------- //

	
	
	/**
	 * batch getConvertStatusAction returns the status of convert task
	 * 
	 * @action getConvertStatus
	 * @param int $jobId the id of the convert job  
	 * @return KontorolBatchJobResponse
	 */
	function getConvertStatusAction($jobId)
	{
		return $this->getStatusAction($jobId, KontorolBatchJobType::CONVERT);
	}
	
	
	
	/**
	 * batch getConvertCollectionStatusAction returns the status of convert task
	 * 
	 * @action getConvertCollectionStatus
	 * @param int $jobId the id of the convert profile job  
	 * @return KontorolBatchJobResponse
	 */
	function getConvertCollectionStatusAction($jobId)
	{
		return $this->getStatusAction($jobId, KontorolBatchJobType::CONVERT_COLLECTION);
	}
	
	
	
	/**
	 * batch getConvertProfileStatusAction returns the status of convert task
	 * 
	 * @action getConvertProfileStatus
	 * @param int $jobId the id of the convert profile job  
	 * @return KontorolBatchJobResponse
	 */
	function getConvertProfileStatusAction($jobId)
	{
		return $this->getStatusAction($jobId, KontorolBatchJobType::CONVERT_PROFILE);
	}
	
	
	
	/**
	 * batch addConvertProfileJobAction creates a new convert profile job
	 * 
	 * @action addConvertProfileJob
	 * @param string $entryId the id of the entry to be reconverted  
	 * @return KontorolBatchJobResponse
	 */
	function addConvertProfileJobAction($entryId)
	{
		$entry = entryPeer::retrieveByPK($entryId);
		if(!$entry)
			throw new KontorolAPIException(APIErrors::INVALID_ENTRY_ID, 'entry', $entryId);
			
		$flavorAsset = assetPeer::retrieveOriginalByEntryId($entryId);
		if(!$flavorAsset)
			throw new KontorolAPIException(KontorolErrors::ORIGINAL_FLAVOR_ASSET_IS_MISSING);
		
		$syncKey = $flavorAsset->getSyncKey(flavorAsset::FILE_SYNC_FLAVOR_ASSET_SUB_TYPE_ASSET);
		if(!kFileSyncUtils::file_exists($syncKey, true))
			throw new KontorolAPIException(APIErrors::NO_FILES_RECEIVED);

		$fileSync = $fileSync = kFileSyncUtils::getLocalFileSyncForKey($syncKey, false);
		$batchJob = kJobsManager::addConvertProfileJob(null, $entry, $flavorAsset->getId(), $fileSync);
		if(!$batchJob)
			throw new KontorolAPIException(APIErrors::UNABLE_TO_CONVERT_ENTRY);
		
		return $this->getStatusAction($batchJob->getId(), KontorolBatchJobType::CONVERT_PROFILE);
	}
	
	
	/**
	 * batch deleteConvertAction deletes and returns the status of convert task
	 * 
	 * @action deleteConvert
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function deleteConvertAction($jobId)
	{
		return $this->deleteJobAction($jobId, KontorolBatchJobType::CONVERT);
	}

	
	/**
	 * batch abortConvertAction aborts and returns the status of convert task
	 * 
	 * @action abortConvert
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function abortConvertAction($jobId)
	{
		return $this->abortJobAction($jobId, KontorolBatchJobType::CONVERT);
	}

	
	/**
	 * batch retryConvertAction retries and returns the status of convert task
	 * 
	 * @action retryConvert
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function retryConvertAction($jobId)
	{
		return $this->retryJobAction($jobId, KontorolBatchJobType::CONVERT);
	}

	
	/**
	 * batch deleteConvertCollectionAction deletes and returns the status of convert profile task
	 * 
	 * @action deleteConvertCollection
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function deleteConvertCollectionAction($jobId)
	{
		return $this->deleteJobAction($jobId, KontorolBatchJobType::CONVERT_COLLECTION);
	}

	
	/**
	 * batch deleteConvertProfileAction deletes and returns the status of convert profile task
	 * 
	 * @action deleteConvertProfile
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function deleteConvertProfileAction($jobId)
	{
		return $this->deleteJobAction($jobId, KontorolBatchJobType::CONVERT_PROFILE);
	}

	
	/**
	 * batch abortConvertCollectionAction aborts and returns the status of convert profile task
	 * 
	 * @action abortConvertCollection
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function abortConvertCollectionAction($jobId)
	{
		return $this->abortJobAction($jobId, KontorolBatchJobType::CONVERT_COLLECTION);
	}

	
	/**
	 * batch abortConvertProfileAction aborts and returns the status of convert profile task
	 * 
	 * @action abortConvertProfile
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function abortConvertProfileAction($jobId)
	{
		return $this->abortJobAction($jobId, KontorolBatchJobType::CONVERT_PROFILE);
	}

	
	/**
	 * batch retryConvertCollectionAction retries and returns the status of convert profile task
	 * 
	 * @action retryConvertCollection
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function retryConvertCollectionAction($jobId)
	{
		return $this->retryJobAction($jobId, KontorolBatchJobType::CONVERT_COLLECTION);
	}

	
	/**
	 * batch retryConvertProfileAction retries and returns the status of convert profile task
	 * 
	 * @action retryConvertProfile
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function retryConvertProfileAction($jobId)
	{
		return $this->retryJobAction($jobId, KontorolBatchJobType::CONVERT_PROFILE);
	}
	
// --------------------------------- ConvertJob functions 	--------------------------------- //

	
	
// --------------------------------- PostConvertJob functions 	--------------------------------- //

	
	/**
	 * batch getPostConvertStatusAction returns the status of post convert task
	 * 
	 * @action getPostConvertStatus
	 * @param int $jobId the id of the post convert job  
	 * @return KontorolBatchJobResponse
	 */
	function getPostConvertStatusAction($jobId)
	{
		return $this->getStatusAction($jobId, KontorolBatchJobType::POSTCONVERT);
	}
	
	
	/**
	 * batch deletePostConvertAction deletes and returns the status of post convert task
	 * 
	 * @action deletePostConvert
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function deletePostConvertAction($jobId)
	{
		return $this->deleteJobAction($jobId, KontorolBatchJobType::POSTCONVERT);
	}
	
	
	/**
	 * batch abortPostConvertAction aborts and returns the status of post convert task
	 * 
	 * @action abortPostConvert
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function abortPostConvertAction($jobId)
	{
		return $this->abortJobAction($jobId, KontorolBatchJobType::POSTCONVERT);
	}
	
	
	/**
	 * batch retryPostConvertAction retries and returns the status of post convert task
	 * 
	 * @action retryPostConvert
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function retryPostConvertAction($jobId)
	{
		return $this->retryJobAction($jobId, KontorolBatchJobType::POSTCONVERT);
	}
	

// --------------------------------- PostConvertJob functions 	--------------------------------- //

// --------------------------------- CaptureThumbJob functions 	--------------------------------- //

	
	/**
	 * batch getCaptureThumbStatusAction returns the status of capture thumbnail task
	 * 
	 * @action getCaptureThumbStatus
	 * @param int $jobId the id of the capture thumbnail job  
	 * @return KontorolBatchJobResponse
	 */
	function getCaptureThumbStatusAction($jobId)
	{
		return $this->getStatusAction($jobId, KontorolBatchJobType::CAPTURE_THUMB);
	}
	
	
	/**
	 * batch deleteCaptureThumbAction deletes and returns the status of capture thumbnail task
	 * 
	 * @action deleteCaptureThumb
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function deleteCaptureThumbAction($jobId)
	{
		return $this->deleteJobAction($jobId, KontorolBatchJobType::CAPTURE_THUMB);
	}
	
	
	/**
	 * batch abortCaptureThumbAction aborts and returns the status of capture thumbnail task
	 * 
	 * @action abortCaptureThumb
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function abortCaptureThumbAction($jobId)
	{
		return $this->abortJobAction($jobId, KontorolBatchJobType::CAPTURE_THUMB);
	}
	
	
	/**
	 * batch retryCaptureThumbAction retries and returns the status of capture thumbnail task
	 * 
	 * @action retryCaptureThumb
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function retryCaptureThumbAction($jobId)
	{
		return $this->retryJobAction($jobId, KontorolBatchJobType::CAPTURE_THUMB);
	}
	

// --------------------------------- CaptureThumbJob functions 	--------------------------------- //
	
	
// --------------------------------- ExtractMediaJob functions 	--------------------------------- //
	
	
	/**
	 * batch getExtractMediaStatusAction returns the status of extract media task
	 * 
	 * @action getExtractMediaStatus
	 * @param int $jobId the id of the extract media job  
	 * @return KontorolBatchJobResponse
	 */
	function getExtractMediaStatusAction($jobId)
	{
		return $this->getStatusAction($jobId, KontorolBatchJobType::EXTRACT_MEDIA);
	}
	
	
	/**
	 * batch deleteExtractMediaAction deletes and returns the status of extract media task
	 * 
	 * @action deleteExtractMedia
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function deleteExtractMediaAction($jobId)
	{
		return $this->deleteJobAction($jobId, KontorolBatchJobType::EXTRACT_MEDIA);
	}
	
	
	/**
	 * batch abortExtractMediaAction aborts and returns the status of extract media task
	 * 
	 * @action abortExtractMedia
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function abortExtractMediaAction($jobId)
	{
		return $this->abortJobAction($jobId, KontorolBatchJobType::EXTRACT_MEDIA);
	}
	
	
	/**
	 * batch retryExtractMediaAction retries and returns the status of extract media task
	 * 
	 * @action retryExtractMedia
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function retryExtractMediaAction($jobId)
	{
		return $this->retryJobAction($jobId, KontorolBatchJobType::EXTRACT_MEDIA);
	}
	

	
	
// --------------------------------- ExtractMediaJob functions 	--------------------------------- //
	
// --------------------------------- StorageExportJob functions 	--------------------------------- //
	
	
	/**
	 * batch getStorageExportStatusAction returns the status of export task
	 * 
	 * @action getStorageExportStatus
	 * @param int $jobId the id of the export job  
	 * @return KontorolBatchJobResponse
	 */
	function getStorageExportStatusAction($jobId)
	{
		return $this->getStatusAction($jobId, KontorolBatchJobType::STORAGE_EXPORT);
	}
	
	
	/**
	 * batch deleteStorageExportAction deletes and returns the status of export task
	 * 
	 * @action deleteStorageExport
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function deleteStorageExportAction($jobId)
	{
		return $this->deleteJobAction($jobId, KontorolBatchJobType::STORAGE_EXPORT);
	}
	
	
	/**
	 * batch abortStorageExportAction aborts and returns the status of export task
	 * 
	 * @action abortStorageExport
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function abortStorageExportAction($jobId)
	{
		return $this->abortJobAction($jobId, KontorolBatchJobType::STORAGE_EXPORT);
	}
	
	
	/**
	 * batch retryStorageExportAction retries and returns the status of export task
	 * 
	 * @action retryStorageExport
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function retryStorageExportAction($jobId)
	{
		return $this->retryJobAction($jobId, KontorolBatchJobType::STORAGE_EXPORT);
	}
	

	
	
// --------------------------------- StorageExportJob functions 	--------------------------------- //
	
// --------------------------------- StorageDeleteJob functions 	--------------------------------- //
	
	
	/**
	 * batch getStorageDeleteStatusAction returns the status of export task
	 * 
	 * @action getStorageDeleteStatus
	 * @param int $jobId the id of the export job  
	 * @return KontorolBatchJobResponse
	 */
	function getStorageDeleteStatusAction($jobId)
	{
		return $this->getStatusAction($jobId, KontorolBatchJobType::STORAGE_DELETE);
	}
	
	
	/**
	 * batch deleteStorageDeleteAction deletes and returns the status of export task
	 * 
	 * @action deleteStorageDelete
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function deleteStorageDeleteAction($jobId)
	{
		return $this->deleteJobAction($jobId, KontorolBatchJobType::STORAGE_DELETE);
	}
	
	
	/**
	 * batch abortStorageDeleteAction aborts and returns the status of export task
	 * 
	 * @action abortStorageDelete
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function abortStorageDeleteAction($jobId)
	{
		return $this->abortJobAction($jobId, KontorolBatchJobType::STORAGE_DELETE);
	}
	
	
	/**
	 * batch retryStorageDeleteAction retries and returns the status of export task
	 * 
	 * @action retryStorageDelete
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function retryStorageDeleteAction($jobId)
	{
		return $this->retryJobAction($jobId, KontorolBatchJobType::STORAGE_DELETE);
	}
	

	
	
// --------------------------------- StorageDeleteJob functions 	--------------------------------- //
	
// --------------------------------- ImportJob functions 	--------------------------------- //
	
	/**
	 * batch getNotificationStatusAction returns the status of Notification task
	 * 
	 * @action getNotificationStatus
	 * @param int $jobId the id of the Notification job  
	 * @return KontorolBatchJobResponse
	 */
	function getNotificationStatusAction($jobId)
	{
		return $this->getStatusAction($jobId, KontorolBatchJobType::NOTIFICATION);
	}
	
	
	/**
	 * batch deleteNotificationAction deletes and returns the status of notification task
	 * 
	 * @action deleteNotification
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function deleteNotificationAction($jobId)
	{
		return $this->deleteJobAction($jobId, KontorolBatchJobType::NOTIFICATION);
	}
	
	
	/**
	 * batch abortNotificationAction aborts and returns the status of notification task
	 * 
	 * @action abortNotification
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function abortNotificationAction($jobId)
	{
		return $this->abortJobAction($jobId, KontorolBatchJobType::NOTIFICATION);
	}
	
	
	/**
	 * batch retryNotificationAction retries and returns the status of notification task
	 * 
	 * @action retryNotification
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function retryNotificationAction($jobId)
	{
		return $this->retryJobAction($jobId, KontorolBatchJobType::NOTIFICATION);
	}
	
	
// --------------------------------- Notification functions 	--------------------------------- //


	
// --------------------------------- MailJob functions 	--------------------------------- //	
	
	
	/**
	 * batch getMailStatusAction returns the status of mail task
	 * 
	 * @action getMailStatus
	 * @param int $jobId the id of the mail job  
	 * @return KontorolBatchJobResponse
	 */
	function getMailStatusAction($jobId)
	{
		return $this->getStatusAction($jobId, KontorolBatchJobType::MAIL);
	}
	
	
	/**
	 * batch deleteMailAction deletes and returns the status of mail task
	 * 
	 * @action deleteMail
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function deleteMailAction($jobId)
	{
		return $this->deleteJobAction($jobId, KontorolBatchJobType::MAIL);
	}
	
	
	/**
	 * batch abortMailAction aborts and returns the status of mail task
	 * 
	 * @action abortMail
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function abortMailAction($jobId)
	{
		return $this->abortJobAction($jobId, KontorolBatchJobType::MAIL);
	}
	
	
	/**
	 * batch retryMailAction retries and returns the status of mail task
	 * 
	 * @action retryMail
	 * @param int $jobId the id of the bulk upload job  
	 * @return KontorolBatchJobResponse
	 */
	function retryMailAction($jobId)
	{
		return $this->retryJobAction($jobId, KontorolBatchJobType::MAIL);
	}
	
	/**
	 * Adds new mail job
	 * 
	 * @action addMailJob
	 * @param KontorolMailJobData $mailJobData
	 */
	function addMailJobAction(KontorolMailJobData $mailJobData)
	{
		$mailJobData->validatePropertyNotNull("mailType");
		$mailJobData->validatePropertyNotNull("recipientEmail");
		
		if (is_null($mailJobData->mailPriority))
			$mailJobData->mailPriority = kMailJobData::MAIL_PRIORITY_NORMAL;
			
		if (is_null($mailJobData->fromEmail))
			$mailJobData->fromEmail = kConf::get("default_email");

		if (is_null($mailJobData->fromName))
			$mailJobData->fromName = kConf::get("default_email_name");
			
		$batchJob = new BatchJob();
		$batchJob->setPartnerId($this->getPartnerId());
		
		$mailJobDataDb = $mailJobData->toObject(new kMailJobData());
			
		kJobsManager::addJob($batchJob, $mailJobDataDb, BatchJobType::MAIL, $mailJobDataDb->getMailType());
	}
	
// --------------------------------- MailJob functions 	--------------------------------- //
	
		
// --------------------------------- generic functions 	--------------------------------- //
	
	
	/**
	 * batch addBatchJob action allows to add a generic BatchJob 
	 * 
	 * @action addBatchJob
	 * @param KontorolBatchJob $batchJob
	 * @return KontorolBatchJob
	 */
	function addBatchJobAction(KontorolBatchJob $batchJob)
	{
		kJobsManager::addJob($batchJob->toObject(), $batchJob->data, $batchJob->jobType, $batchJob->jobSubType);	
	}

	
	
	/**
	 * batch getStatusAction returns the status of task
	 * 
	 * @action getStatus
	 * @param int $jobId the id of the job  
	 * @param KontorolBatchJobType $jobType the type of the job
	 * @param KontorolFilterPager $pager pager for the child jobs
	 * @return KontorolBatchJobResponse
	 */
	function getStatusAction($jobId, $jobType, KontorolFilterPager $pager = null)
	{
		$dbJobType = kPluginableEnumsManager::apiToCore('BatchJobType', $jobType);
		
		$dbBatchJob = BatchJobPeer::retrieveByPK($jobId);
		if($dbBatchJob->getJobType() != $dbJobType)
			throw new KontorolAPIException(APIErrors::GET_EXCLUSIVE_JOB_WRONG_TYPE, $jobType, $dbBatchJob->getId());
		
		$dbBatchJobLock = BatchJobLockPeer::retrieveByPK($jobId);
		
		$job = new KontorolBatchJob();
		$job->fromBatchJob($dbBatchJob,$dbBatchJobLock);
		
		$batchJobResponse = new KontorolBatchJobResponse();
		$batchJobResponse->batchJob = $job;
		
		if(!$pager)
			$pager = new KontorolFilterPager();
			
		$c = new Criteria();
		$pager->attachToCriteria($c);
		
		$childBatchJobs = $dbBatchJob->getChildJobs($c);
		$batchJobResponse->childBatchJobs = KontorolBatchJobArray::fromBatchJobArray($childBatchJobs);
		
		return $batchJobResponse;
	}

	
	
	/**
	 * batch deleteJobAction deletes and returns the status of task
	 * 
	 * @action deleteJob
	 * @param int $jobId the id of the job  
	 * @param KontorolBatchJobType $jobType the type of the job
	 * @return KontorolBatchJobResponse
	 */
	function deleteJobAction($jobId, $jobType)
	{
		$dbJobType = kPluginableEnumsManager::apiToCore('BatchJobType', $jobType);
		kJobsManager::deleteJob($jobId, $dbJobType);
		return $this->getStatusAction($jobId, $jobType);
	}

	
	
	/**
	 * batch abortJobAction aborts and returns the status of task
	 * 
	 * @action abortJob
	 * @param int $jobId the id of the job  
	 * @param KontorolBatchJobType $jobType the type of the job
	 * @return KontorolBatchJobResponse
	 */
	function abortJobAction($jobId, $jobType)
	{
		$dbJobType = kPluginableEnumsManager::apiToCore('BatchJobType', $jobType);
		kJobsManager::abortJob($jobId, $dbJobType);
		return $this->getStatusAction($jobId, $jobType);
	}

	
	
	/**
	 * batch retryJobAction aborts and returns the status of task
	 * 
	 * @action retryJob
	 * @param int $jobId the id of the job  
	 * @param KontorolBatchJobType $jobType the type of the job
	 * @param bool $force should we force the restart. 
	 * @return KontorolBatchJobResponse
	 */
	function retryJobAction($jobId, $jobType, $force = false)
	{
		$dbJobType = kPluginableEnumsManager::apiToCore('BatchJobType', $jobType);
		kJobsManager::retryJob($jobId, $dbJobType, $force);
		return $this->getStatusAction($jobId, $jobType);
	}
	
	/**
	 * batch boostEntryJobsAction boosts all the jobs associated with the entry
	 * 
	 * @action boostEntryJobs
	 * @param string $entryId the id of the entry to be boosted  
	 */
	function boostEntryJobsAction($entryId)
	{
		kJobsManager::boostEntryJobs($entryId);
	}

	/**
	 * list Batch Jobs 
	 * 
	 * @action listBatchJobs
	 * @param KontorolBatchJobFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return KontorolBatchJobListResponse
	 */
	function listBatchJobsAction(KontorolBatchJobFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter) 
			$filter = new KontorolBatchJobFilter();
			
		$batchJobFilter = new BatchJobFilter (true);
		$filter->toObject($batchJobFilter);
		
		$c = new Criteria();
//		$c->add(BatchJobPeer::DELETED_AT, null);
		
		$batchJobFilter->attachToCriteria($c);
		
		if(!$pager)
		   $pager = new KontorolFilterPager();
		
		$pager->attachToCriteria($c);
		
		myDbHelper::$use_alternative_con = myDbHelper::DB_HELPER_CONN_PROPEL2;
		
		$list = BatchJobPeer::doSelect($c);
		
		$c->setLimit(false);
		$count = BatchJobPeer::doCount($c);

		$newList = KontorolBatchJobArray::fromStatisticsBatchJobArray($list );
		
		$response = new KontorolBatchJobListResponse();
		$response->objects = $newList;
		$response->totalCount = $count;
		
		return $response;
	}
	
// --------------------------------- generic functions 	--------------------------------- //	
	
	
	
}

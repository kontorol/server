<?php

/**
 * Add & Manage Access Controls
 *
 * @service accessControl
 * @deprecated use accessControlProfile service instead
 */
class AccessControlService extends KontorolBaseService
{
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		$this->applyPartnerFilterForClass('accessControl'); 	
	}
	
	/**
	 * Add new Access Control Profile
	 * 
	 * @action add
	 * @param KontorolAccessControl $accessControl
	 * @return KontorolAccessControl
	 */
	function addAction(KontorolAccessControl $accessControl)
	{
		$accessControl->validatePropertyMinLength("name", 1);
		$accessControl->partnerId = $this->getPartnerId();
		
		$dbAccessControl = new accessControl();
		$accessControl->toObject($dbAccessControl);
		$dbAccessControl->save();
		
		$accessControl = new KontorolAccessControl();
		$accessControl->fromObject($dbAccessControl, $this->getResponseProfile());
		return $accessControl;
	}
	
	/**
	 * Get Access Control Profile by id
	 * 
	 * @action get
	 * @param int $id
	 * @return KontorolAccessControl
	 */
	function getAction($id)
	{
		$dbAccessControl = accessControlPeer::retrieveByPK($id);
		if (!$dbAccessControl)
			throw new KontorolAPIException(KontorolErrors::ACCESS_CONTROL_ID_NOT_FOUND, $id);
			
		$accessControl = new KontorolAccessControl();
		$accessControl->fromObject($dbAccessControl, $this->getResponseProfile());
		return $accessControl;
	}
	
	/**
	 * Update Access Control Profile by id
	 * 
	 * @action update
	 * @param int $id
	 * @param KontorolAccessControl $accessControl
	 * @return KontorolAccessControl
	 * 
	 * @throws KontorolErrors::ACCESS_CONTROL_ID_NOT_FOUND
	 * @throws KontorolErrors::ACCESS_CONTROL_NEW_VERSION_UPDATE
	 */
	function updateAction($id, KontorolAccessControl $accessControl)
	{
		$dbAccessControl = accessControlPeer::retrieveByPK($id);
		if (!$dbAccessControl)
			throw new KontorolAPIException(KontorolErrors::ACCESS_CONTROL_ID_NOT_FOUND, $id);
	
		$rules = $dbAccessControl->getRulesArray();
		foreach($rules as $rule)
		{
			if(!($rule instanceof kAccessControlRestriction))
				throw new KontorolAPIException(KontorolErrors::ACCESS_CONTROL_NEW_VERSION_UPDATE, $id);
		}
		
		$accessControl->validatePropertyMinLength("name", 1, true);
			
		$accessControl->toUpdatableObject($dbAccessControl);
		$dbAccessControl->save();
		
		$accessControl = new KontorolAccessControl();
		$accessControl->fromObject($dbAccessControl, $this->getResponseProfile());
		return $accessControl;
	}
	
	/**
	 * Delete Access Control Profile by id
	 * 
	 * @action delete
	 * @param int $id
	 */
	function deleteAction($id)
	{
		$dbAccessControl = accessControlPeer::retrieveByPK($id);
		if (!$dbAccessControl)
			throw new KontorolAPIException(KontorolErrors::ACCESS_CONTROL_ID_NOT_FOUND, $id);

		if ($dbAccessControl->getIsDefault())
			throw new KontorolAPIException(KontorolErrors::CANNOT_DELETE_DEFAULT_ACCESS_CONTROL);

		$dbAccessControl->setDeletedAt(time());
		try
		{
			$dbAccessControl->save();
		}
		catch(kCoreException $e)
		{
			$code = $e->getCode();
			switch($code)
			{
				case kCoreException::EXCEEDED_MAX_ENTRIES_PER_ACCESS_CONTROL_UPDATE_LIMIT :
					throw new KontorolAPIException(KontorolErrors::EXCEEDED_ENTRIES_PER_ACCESS_CONTROL_FOR_UPDATE, $id);
				case kCoreException::NO_DEFAULT_ACCESS_CONTROL :
					throw new KontorolAPIException(KontorolErrors::CANNOT_TRANSFER_ENTRIES_TO_ANOTHER_ACCESS_CONTROL_OBJECT);
				default:
					throw $e;
			}
		}
	}
	
	/**
	 * List Access Control Profiles by filter and pager
	 * 
	 * @action list
	 * @param KontorolFilterPager $filter
	 * @param KontorolAccessControlFilter $pager
	 * @return KontorolAccessControlListResponse
	 */
	function listAction(KontorolAccessControlFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
			$filter = new KontorolAccessControlFilter();
			
		if(!$pager)
			$pager = new KontorolFilterPager();
			
		return $filter->getListResponse($pager, $this->getResponseProfile());  
	}
}

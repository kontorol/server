<?php

/**
 * Manage access control profiles
 *
 * @service accessControlProfile
 */
class AccessControlProfileService extends KontorolBaseService
{
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		$this->applyPartnerFilterForClass('accessControl'); 	
	}
	
	/**
	 * Add new access control profile
	 * 
	 * @action add
	 * @param KontorolAccessControlProfile $accessControlProfile
	 * @return KontorolAccessControlProfile
	 */
	function addAction(KontorolAccessControlProfile $accessControlProfile)
	{
		$dbAccessControl = $accessControlProfile->toInsertableObject();
		$dbAccessControl->setPartnerId($this->getPartnerId());
		$dbAccessControl->save();
		
		$accessControlProfile = new KontorolAccessControlProfile();
		$accessControlProfile->fromObject($dbAccessControl, $this->getResponseProfile());
		return $accessControlProfile;
	}
	
	/**
	 * Get access control profile by id
	 * 
	 * @action get
	 * @param int $id
	 * @return KontorolAccessControlProfile
	 * 
	 * @throws KontorolErrors::ACCESS_CONTROL_ID_NOT_FOUND
	 */
	function getAction($id)
	{
		$dbAccessControl = accessControlPeer::retrieveByPK($id);
		if (!$dbAccessControl)
			throw new KontorolAPIException(KontorolErrors::ACCESS_CONTROL_ID_NOT_FOUND, $id);
			
		$accessControlProfile = new KontorolAccessControlProfile();
		$accessControlProfile->fromObject($dbAccessControl, $this->getResponseProfile());
		return $accessControlProfile;
	}
	
	/**
	 * Update access control profile by id
	 * 
	 * @action update
	 * @param int $id
	 * @param KontorolAccessControlProfile $accessControlProfile
	 * @return KontorolAccessControlProfile
	 * 
	 * @throws KontorolErrors::ACCESS_CONTROL_ID_NOT_FOUND
	 */
	function updateAction($id, KontorolAccessControlProfile $accessControlProfile)
	{
		$dbAccessControl = accessControlPeer::retrieveByPK($id);
		if (!$dbAccessControl)
			throw new KontorolAPIException(KontorolErrors::ACCESS_CONTROL_ID_NOT_FOUND, $id);
		
		$accessControlProfile->toUpdatableObject($dbAccessControl);
		$dbAccessControl->save();
		
		$accessControlProfile = new KontorolAccessControlProfile();
		$accessControlProfile->fromObject($dbAccessControl, $this->getResponseProfile());
		return $accessControlProfile;
	}
	
	/**
	 * Delete access control profile by id
	 * 
	 * @action delete
	 * @param int $id
	 * 
	 * @throws KontorolErrors::ACCESS_CONTROL_ID_NOT_FOUND
	 * @throws KontorolErrors::CANNOT_DELETE_DEFAULT_ACCESS_CONTROL
	 */
	function deleteAction($id)
	{
		$dbAccessControl = accessControlPeer::retrieveByPK($id);
		if (!$dbAccessControl)
			throw new KontorolAPIException(KontorolErrors::ACCESS_CONTROL_ID_NOT_FOUND, $id);

		if ($dbAccessControl->getIsDefault())
			throw new KontorolAPIException(KontorolErrors::CANNOT_DELETE_DEFAULT_ACCESS_CONTROL);
			
		$dbAccessControl->setDeletedAt(time());
		try
		{
			$dbAccessControl->save();
		}
		catch(kCoreException $e)
		{
			$code = $e->getCode();
			switch($code)
			{
				case kCoreException::EXCEEDED_MAX_ENTRIES_PER_ACCESS_CONTROL_UPDATE_LIMIT :
					throw new KontorolAPIException(KontorolErrors::EXCEEDED_ENTRIES_PER_ACCESS_CONTROL_FOR_UPDATE, $id);
				case kCoreException::NO_DEFAULT_ACCESS_CONTROL :
					throw new KontorolAPIException(KontorolErrors::CANNOT_TRANSFER_ENTRIES_TO_ANOTHER_ACCESS_CONTROL_OBJECT);
				default:
					throw $e;
			}
		}
	}
	
	/**
	 * List access control profiles by filter and pager
	 * 
	 * @action list
	 * @param KontorolFilterPager $filter
	 * @param KontorolAccessControlProfileFilter $pager
	 * @return KontorolAccessControlProfileListResponse
	 */
	function listAction(KontorolAccessControlProfileFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
			$filter = new KontorolAccessControlProfileFilter();
			
		if(!$pager)
			$pager = new KontorolFilterPager();
			
		return $filter->getListResponse($pager, $this->getResponseProfile());
	}
}

<?php
/**
 * Server Node service
 *
 * @service serverNode
 * @package api
 * @subpackage services
 */
class ServerNodeService extends KontorolBaseService
{
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		
		$partnerId = $this->getPartnerId();
		if(!$this->getPartner()->getEnabledService(PermissionName::FEATURE_SERVER_NODE) && $partnerId != Partner::BATCH_PARTNER_ID)
			throw new KontorolAPIException(KontorolErrors::SERVICE_FORBIDDEN, $this->serviceName.'->'.$this->actionName);
			
		$this->applyPartnerFilterForClass('serverNode');
	}
	
	/**
	 * Adds a server node to the Kontorol DB.
	 *
	 * @action add
	 * @param KontorolServerNode $serverNode
	 * @return KontorolServerNode
	 */
	function addAction(KontorolServerNode $serverNode)
	{	
		$dbServerNode = $this->addNewServerNode($serverNode);
		
		$serverNode = KontorolServerNode::getInstance($dbServerNode, $this->getResponseProfile());
		return $serverNode;
	}
	
	/**
	 * Get server node by id
	 * 
	 * @action get
	 * @param int $serverNodeId
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 * @return KontorolServerNode
	 */
	function getAction($serverNodeId)
	{
		$dbServerNode = ServerNodePeer::retrieveByPK($serverNodeId);
		if (!$dbServerNode)
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $serverNodeId);
		
		$serverNode = KontorolServerNode::getInstance($dbServerNode, $this->getResponseProfile());
		return $serverNode;
	}
	
	/**
	 * Update server node by id 
	 * 
	 * @action update
	 * @param int $serverNodeId
	 * @param KontorolServerNode $serverNode
	 * @return KontorolServerNode
	 */
	function updateAction($serverNodeId, KontorolServerNode $serverNode)
	{
		$dbServerNode = ServerNodePeer::retrieveByPK($serverNodeId);
		if (!$dbServerNode)
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $serverNodeId);
			
		$dbServerNode = $serverNode->toUpdatableObject($dbServerNode);
		$dbServerNode->save();
		
		$serverNode = KontorolServerNode::getInstance($dbServerNode, $this->getResponseProfile());
		return $serverNode;
	}
	
	/**
	 * delete server node by id
	 *
	 * @action delete
	 * @param string $serverNodeId
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 */
	function deleteAction($serverNodeId)
	{
		$dbServerNode = ServerNodePeer::retrieveByPK($serverNodeId);
		if(!$dbServerNode)
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $serverNodeId);
	
		$dbServerNode->setStatus(ServerNodeStatus::DELETED);
		$dbServerNode->save();
	}
	
	/**
	 * Disable server node by id
	 *
	 * @action disable
	 * @param string $serverNodeId
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 * @return KontorolServerNode
	 */
	function disableAction($serverNodeId)
	{
		$dbServerNode = ServerNodePeer::retrieveByPK($serverNodeId);
		if(!$dbServerNode)
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $serverNodeId);
	
		$dbServerNode->setStatus(ServerNodeStatus::DISABLED);
		$dbServerNode->save();
		
		$serverNode = KontorolServerNode::getInstance($dbServerNode, $this->getResponseProfile());
		return $serverNode;
	}
	
	/**
	 * Enable server node by id
	 *
	 * @action enable
	 * @param string $serverNodeId
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 * @return KontorolServerNode
	 */
	function enableAction($serverNodeId)
	{
		$dbServerNode = ServerNodePeer::retrieveByPK($serverNodeId);
		if(!$dbServerNode)
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $serverNodeId);
	
		$dbServerNode->setStatus(ServerNodeStatus::ACTIVE);
		$dbServerNode->save();
		
		$serverNode = KontorolServerNode::getInstance($dbServerNode, $this->getResponseProfile());
		return $serverNode;
	}
	
	/**	
	 * @action list
	 * @param KontorolServerNodeFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return KontorolServerNodeListResponse
	 */
	public function listAction(KontorolServerNodeFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if(!$filter)
			$filter = new KontorolServerNodeFilter();
			
		if(!$pager)
			$pager = new KontorolFilterPager();
		
		return $filter->getTypeListResponse($pager, $this->getResponseProfile(), null);
	}
	
	/**
	 * Update server node status
	 *
	 * @action reportStatus
	 * @param string $hostName
	 * @param KontorolServerNode $serverNode
	 * @param KontorolServerNodeStatus $serverNodeStatus
	 * @return KontorolServerNode
	 */
	function reportStatusAction($hostName, KontorolServerNode $serverNode = null, $serverNodeStatus = ServerNodeStatus::ACTIVE)
	{
		$dbType = null;
		if ($serverNode)
		{
			$dbServerNode1 = $serverNode->toObject();
			if ($dbServerNode1)
			{
				$dbType = $dbServerNode1->getType();
			}
		}
		$dbServerNode = ServerNodePeer::retrieveActiveServerNode($hostName, $this->getPartnerId(), $dbType);

		//Allow serverNodes auto registration without calling add
		if (!$dbServerNode)
		{
			if($serverNode)
			{
				$dbServerNode = $this->addNewServerNode($serverNode);
			}
			else 
				throw new KontorolAPIException(KontorolErrors::SERVER_NODE_NOT_FOUND, $hostName);
		}


		$dbServerNode->setHeartbeatTime(time());
		$serverNodeStatus = ($serverNodeStatus == ServerNodeStatus::NOT_OPERATIONAL) ? ServerNodeStatus::ACTIVE : $serverNodeStatus;
		$dbServerNode->setStatus($serverNodeStatus);
		$dbServerNode->save();
	
		$serverNode = KontorolServerNode::getInstance($dbServerNode, $this->getResponseProfile());
		return $serverNode;
	}
	
	private function addNewServerNode(KontorolServerNode $serverNode)
	{
		$dbServerNode = $serverNode->toInsertableObject();
		/* @var $dbServerNode ServerNode */
		$dbServerNode->setPartnerId($this->getPartnerId());
		$dbServerNode->setStatus(ServerNodeStatus::DISABLED);
		$dbServerNode->save();
		
		return $dbServerNode;
	}

	/**
	 * Mark server node offline
	 *
	 * @action markOffline
	 * @param string $serverNodeId
	 * @throws KontorolErrors::INVALID_OBJECT_ID
	 * @return KontorolServerNode
	 * @throws KontorolAPIException
	 */
	function markOfflineAction($serverNodeId)
	{
		$criteria = new Criteria();
		$criteria->add(ServerNodePeer::ID, $serverNodeId);
		$criteria->add(ServerNodePeer::STATUS, ServerNodeStatus::ACTIVE);
		$dbServerNode = ServerNodePeer::doSelectOne($criteria);

		if(!$dbServerNode)
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $serverNodeId);

		$dbServerNode->setStatus(ServerNodeStatus::NOT_REGISTERED);
		$dbServerNode->save();

		$serverNode = KontorolServerNode::getInstance($dbServerNode, $this->getResponseProfile());
		return $serverNode;
	}

	/**
	 * Get the edge server node full path
	 *
	 * @action getFullPath
	 * @param string $hostName
	 * @param string $protocol
	 * @param string $deliveryFormat
	 * @param string $deliveryType
	 * @return string
	 */
	function getFullPathAction($hostName, $protocol = 'http', $deliveryFormat = null, $deliveryType = null)
	{
		$dbServerNode = ServerNodePeer::retrieveActiveServerNode($hostName, $this->getPartnerId(), KontorolServerNodeType::EDGE);
		if (!$dbServerNode)
		{
			throw new KontorolAPIException(KontorolErrors::SERVER_NODE_NOT_FOUND, $hostName);
		}
		/** @var EdgeServerNode $dbServerNode */
		return $dbServerNode->buildEdgeFullPath($protocol, $deliveryFormat, $deliveryType);
	}


}

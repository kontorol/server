<?php

/**
 * Live Channel service lets you manage live channels
 *
 * @service liveChannel
 * @package api
 * @subpackage services
 */
class LiveChannelService extends KontorolLiveEntryService
{
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);

		if($this->getPartnerId() > 0 && !PermissionPeer::isValidForPartner(PermissionName::FEATURE_LIVE_CHANNEL, $this->getPartnerId()))
			throw new KontorolAPIException(KontorolErrors::SERVICE_FORBIDDEN, $this->serviceName.'->'.$this->actionName);
	}
	
	/**
	 * Adds new live channel.
	 * 
	 * @action add
	 * @param KontorolLiveChannel $liveChannel Live channel metadata
	 * @return KontorolLiveChannel The new live channel
	 */
	function addAction(KontorolLiveChannel $liveChannel)
	{
		$dbEntry = $this->prepareEntryForInsert($liveChannel);
		$dbEntry->save();
		
		$te = new TrackEntry();
		$te->setEntryId($dbEntry->getId());
		$te->setTrackEventTypeId(TrackEntry::TRACK_ENTRY_EVENT_TYPE_ADD_ENTRY);
		$te->setDescription(__METHOD__ . ":" . __LINE__ . "::LIVE_CHANNEL");
		TrackEntry::addTrackEntry($te);
		
		$liveChannel = new KontorolLiveChannel();
		$liveChannel->fromObject($dbEntry, $this->getResponseProfile());
		return $liveChannel;
	}

	
	/**
	 * Get live channel by ID.
	 * 
	 * @action get
	 * @param string $id Live channel id
	 * @return KontorolLiveChannel The requested live channel
	 * 
	 * @throws KontorolErrors::ENTRY_ID_NOT_FOUND
	 */
	function getAction($id)
	{
		return $this->getEntry($id, -1, KontorolEntryType::LIVE_CHANNEL);
	}

	
	/**
	 * Update live channel. Only the properties that were set will be updated.
	 * 
	 * @action update
	 * @param string $id Live channel id to update
	 * @param KontorolLiveChannel $liveChannel Live channel metadata to update
	 * @return KontorolLiveChannel The updated live channel
	 * 
	 * @throws KontorolErrors::ENTRY_ID_NOT_FOUND
	 * @validateUser entry id edit
	 */
	function updateAction($id, KontorolLiveChannel $liveChannel)
	{
		return $this->updateEntry($id, $liveChannel, KontorolEntryType::LIVE_CHANNEL);
	}

	/**
	 * Delete a live channel.
	 *
	 * @action delete
	 * @param string $id Live channel id to delete
	 * 
 	 * @throws KontorolErrors::ENTRY_ID_NOT_FOUND
 	 * @validateUser entry id edit
	 */
	function deleteAction($id)
	{
		$this->deleteEntry($id, KontorolEntryType::LIVE_CHANNEL);
	}
	
	/**
	 * List live channels by filter with paging support.
	 * 
	 * @action list
     * @param KontorolLiveChannelFilter $filter live channel filter
	 * @param KontorolFilterPager $pager Pager
	 * @return KontorolLiveChannelListResponse Wrapper for array of live channels and total count
	 */
	function listAction(KontorolLiveChannelFilter $filter = null, KontorolFilterPager $pager = null)
	{
	    if (!$filter)
			$filter = new KontorolLiveChannelFilter();
			
	    $filter->typeEqual = KontorolEntryType::LIVE_CHANNEL;
	    list($list, $totalCount) = parent::listEntriesByFilter($filter, $pager);
	    
	    $newList = KontorolLiveChannelArray::fromDbArray($list, $this->getResponseProfile());
		$response = new KontorolLiveChannelListResponse();
		$response->objects = $newList;
		$response->totalCount = $totalCount;
		return $response;
	}
	
	/**
	 * Delivering the status of a live channel (on-air/offline)
	 * 
	 * @action isLive
	 * @param string $id ID of the live channel
	 * @return bool
	 * @ksOptional
	 * 
	 * @throws KontorolErrors::ENTRY_ID_NOT_FOUND
	 */
	public function isLiveAction ($id)
	{
		$dbEntry = entryPeer::retrieveByPK($id);

		if (!$dbEntry || $dbEntry->getType() != KontorolEntryType::LIVE_CHANNEL)
			throw new KontorolAPIException(KontorolErrors::ENTRY_ID_NOT_FOUND, $id);

		return $dbEntry->isCurrentlyLive();
	}
}

<?php
/**
 * Flavor Params Output service
 *
 * @service flavorParamsOutput
 * @package api
 * @subpackage services
 */
class FlavorParamsOutputService extends KontorolBaseService
{
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		
		if($this->getPartnerId() != Partner::BATCH_PARTNER_ID && $this->getPartnerId() != Partner::ADMIN_CONSOLE_PARTNER_ID)
			throw new KontorolAPIException(KontorolErrors::SERVICE_FORBIDDEN, $this->serviceName.'->'.$this->actionName);
	}
	
	/**
	 * Get flavor params output object by ID
	 * 
	 * @action get
	 * @param int $id
	 * @return KontorolFlavorParamsOutput
	 * @throws KontorolErrors::FLAVOR_PARAMS_OUTPUT_ID_NOT_FOUND
	 */
	public function getAction($id)
	{
		$flavorParamsOutputDb = assetParamsOutputPeer::retrieveByPK($id);
		
		if (!$flavorParamsOutputDb)
			throw new KontorolAPIException(KontorolErrors::FLAVOR_PARAMS_OUTPUT_ID_NOT_FOUND, $id);
			
		$flavorParamsOutput = KontorolFlavorParamsFactory::getFlavorParamsOutputInstance($flavorParamsOutputDb->getType());
		$flavorParamsOutput->fromObject($flavorParamsOutputDb, $this->getResponseProfile());
		
		return $flavorParamsOutput;
	}
	
	/**
	 * List flavor params output objects by filter and pager
	 * 
	 * @action list
	 * @param KontorolFlavorParamsOutputFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return KontorolFlavorParamsOutputListResponse
	 */
	function listAction(KontorolFlavorParamsOutputFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
			$filter = new KontorolFlavorParamsOutputFilter();
			
		if(!$pager)
		{
			$pager = new KontorolFilterPager();
		}
			
		$types = KontorolPluginManager::getExtendedTypes(assetParamsOutputPeer::OM_CLASS, assetType::FLAVOR);
		return $filter->getTypeListResponse($pager, $this->getResponseProfile(), $types);
	}
}

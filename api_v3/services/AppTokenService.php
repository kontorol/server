<?php

/**
 * Manage application authentication tokens
 *
 * @service appToken
 */
class AppTokenService extends KontorolBaseService
{
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		$this->applyPartnerFilterForClass('AppToken');
	}
	
	/**
	 * Add new application authentication token
	 * 
	 * @action add
	 * @param KontorolAppToken $appToken
	 * @return KontorolAppToken
	 */
	function addAction(KontorolAppToken $appToken)
	{
		$dbAppToken = $appToken->toInsertableObject();
		$dbAppToken->save();
		
		$appToken = new KontorolAppToken();
		$appToken->fromObject($dbAppToken, $this->getResponseProfile());
		return $appToken;
	}
	
	/**
	 * Get application authentication token by ID
	 * 
	 * @action get
	 * @param string $id
	 * @return KontorolAppToken
	 * 
	 * @throws KontorolErrors::APP_TOKEN_ID_NOT_FOUND
	 */
	function getAction($id)
	{
		$dbAppToken = AppTokenPeer::retrieveByPK($id);
		if(!$dbAppToken)
			throw new KontorolAPIException(KontorolErrors::APP_TOKEN_ID_NOT_FOUND, $id);
		
		$appToken = new KontorolAppToken();
		$appToken->fromObject($dbAppToken, $this->getResponseProfile());
		return $appToken;
	}
	
	/**
	 * Update application authentication token by ID
	 * 
	 * @action update
	 * @param string $id
	 * @param KontorolAppToken $appToken
	 * @return KontorolAppToken
	 * 
	 * @throws KontorolErrors::APP_TOKEN_ID_NOT_FOUND
	 */
	function updateAction($id, KontorolAppToken $appToken)
	{
		$dbAppToken = AppTokenPeer::retrieveByPK($id);
		if(!$dbAppToken)
			throw new KontorolAPIException(KontorolErrors::APP_TOKEN_ID_NOT_FOUND, $id);
		
		$appToken->toUpdatableObject($dbAppToken);
		$dbAppToken->save();
		
		$appToken = new KontorolAppToken();
		$appToken->fromObject($dbAppToken, $this->getResponseProfile());
		return $appToken;
	}
	
	/**
	 * Delete application authentication token by ID
	 * 
	 * @action delete
	 * @param string $id
	 * 
	 * @throws KontorolErrors::APP_TOKEN_ID_NOT_FOUND
	 */
	function deleteAction($id)
	{
		$dbAppToken = AppTokenPeer::retrieveByPK($id);
		if(!$dbAppToken)
			throw new KontorolAPIException(KontorolErrors::APP_TOKEN_ID_NOT_FOUND, $id);
		
		$invalidSessionKey = ks::buildSessionIdHash($this->getPartnerId(), $id); 
		invalidSessionPeer::invalidateByKey($invalidSessionKey, invalidSession::INVALID_SESSION_TYPE_SESSION_ID, $dbAppToken->getExpiry());
		$dbAppToken->setStatus(AppTokenStatus::DELETED);
		$dbAppToken->save();
	}
	
	/**
	 * List application authentication tokens by filter and pager
	 * 
	 * @action list
	 * @param KontorolFilterPager $filter
	 * @param KontorolAppTokenFilter $pager
	 * @return KontorolAppTokenListResponse
	 */
	function listAction(KontorolAppTokenFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if(!$filter)
			$filter = new KontorolAppTokenFilter();
		
		if(!$pager)
			$pager = new KontorolFilterPager();


		if ($filter->sessionUserIdEqual)
		{
			$kuser = kuserPeer::getKuserByPartnerAndUid ($this->getPartnerId() , $filter->sessionUserIdEqual );
			if($kuser)
				$filter->sessionUserIdEqual = $kuser->getId();
			else
			{
				$response = new KontorolAppTokenListResponse();
				$response->totalCount = 0;
				return $response;
			}
		}

		$c = new Criteria();
		$appTokenFilter = $filter->toObject();
		$appTokenFilter->attachToCriteria($c);
		$pager->attachToCriteria($c);
		
		$list = AppTokenPeer::doSelect($c);
		
		$totalCount = null;
		$resultCount = count($list);
		if($resultCount && ($resultCount < $pager->pageSize))
		{
			$totalCount = ($pager->pageIndex - 1) * $pager->pageSize + $resultCount;
		}
		else
		{
			KontorolFilterPager::detachFromCriteria($c);
			$totalCount = AppTokenPeer::doCount($c);
		}
		
		$response = new KontorolAppTokenListResponse();
		$response->totalCount = $totalCount;
		$response->objects = KontorolAppTokenArray::fromDbArray($list, $this->getResponseProfile());
		return $response;
	}
	
	/**
	 * Starts a new KS (kontorol Session) based on an application authentication token ID
	 * 
	 * @action startSession
	 * @param string $id application token ID
	 * @param string $tokenHash a hash [MD5, SHA1, SHA256 and SHA512 are supported] of the current KS concatenated with the application token 
	 * @param string $userId session user ID, will be ignored if a different user ID already defined on the application token
	 * @param KontorolSessionType $type session type, will be ignored if a different session type is already defined on the application token
	 * @param int $expiry session expiry (in seconds), could be overridden by shorter expiry of the application token
	 * @param string $sessionPrivileges session privileges, will be ignored if a similar privilege is already defined on the application token or the privilege is server reserved
	 * @throws KontorolErrors::APP_TOKEN_ID_NOT_FOUND
	 * @return KontorolSessionInfo
	 */
	function startSessionAction($id, $tokenHash, $userId = null, $type = null, $expiry = null, $sessionPrivileges = null)
	{
		$dbAppToken = AppTokenPeer::retrieveByPK($id);
		if(!$dbAppToken)
			throw new KontorolAPIException(KontorolErrors::APP_TOKEN_ID_NOT_FOUND, $id);
		
		if($dbAppToken->getStatus() != AppTokenStatus::ACTIVE)
			throw new KontorolAPIException(KontorolErrors::APP_TOKEN_NOT_ACTIVE, $id);
		
		$appTokenHash = $dbAppToken->calcHash();
		if($appTokenHash !== $tokenHash)
			throw new KontorolAPIException(KontorolErrors::INVALID_APP_TOKEN_HASH);
		
		KontorolResponseCacher::disableCache();
		
		$tokenExpiry = $dbAppToken->getSessionDuration();
		if(!is_null($dbAppToken->getExpiry()))
		{
			$tokenExpiry = min($tokenExpiry, $dbAppToken->getExpiry() - time());
			if($tokenExpiry < 0)
				throw new KontorolAPIException(KontorolErrors::APP_TOKEN_EXPIRED, $id);
		}
		if(!$expiry)
		{
			$expiry = $tokenExpiry;
		}
		$expiry = min($expiry, $tokenExpiry);
		
		if(!is_null($dbAppToken->getSessionType()))
			$type = $dbAppToken->getSessionType();
		if(is_null($type))
			$type = SessionType::USER;
			
		if(!is_null($dbAppToken->getSessionUserId()))
			$userId = $dbAppToken->getSessionUserId();
			
		$partnerId = kCurrentContext::getCurrentPartnerId();
		$partner = PartnerPeer::retrieveByPK($partnerId);
		$secret = $type == SessionType::ADMIN ? $partner->getAdminSecret() : $partner->getSecret();
		
		$privilegesArray = array(
			ks::PRIVILEGE_SESSION_ID => array($id),
			ks::PRIVILEGE_APP_TOKEN => array($id)
		);
		if($dbAppToken->getSessionPrivileges())
		{
			$privilegesArray = array_merge_recursive($privilegesArray, ks::parsePrivileges($dbAppToken->getSessionPrivileges()));
		}

		if($sessionPrivileges)
		{
			$parsedAppSessionPrivilegesArray = ks::parsePrivileges($sessionPrivileges);
			$additionalAllowedSessionPrivliges = ks::retrieveAllowedAppSessionPrivileges($privilegesArray, $parsedAppSessionPrivilegesArray);
			$privilegesArray = array_merge_recursive($privilegesArray, $additionalAllowedSessionPrivliges);
		}

		$privileges = ks::buildPrivileges($privilegesArray);
		
		$ks = kSessionUtils::createKSession($partnerId, $secret, $userId, $expiry, $type, $privileges);
		if(!$ks)
			throw new KontorolAPIException(APIErrors::START_SESSION_ERROR, $partnerId);
			
		$sessionInfo = new KontorolSessionInfo();
		$sessionInfo->ks = $ks->toSecureString();
		$sessionInfo->partnerId = $partnerId;
		$sessionInfo->userId = $userId;
		$sessionInfo->expiry = $ks->valid_until;
		$sessionInfo->sessionType = $type;
		$sessionInfo->privileges = $privileges;
		
		return $sessionInfo;
	}
	
};

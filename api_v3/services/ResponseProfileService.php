<?php

/**
 * Manage response profiles
 *
 * @service responseProfile
 */
class ResponseProfileService extends KontorolBaseService
{
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		
		//Don;t apply partner filter if action is list to avoid returning default partner 0 response profiles on every call
		if($actionName !== "list")
			$this->applyPartnerFilterForClass('ResponseProfile'); 	
	}
	
	/* (non-PHPdoc)
	 * @see KontorolBaseService::partnerGroup()
	 */
	protected function partnerGroup($peer = null)
	{
		
		switch ($this->actionName)
		{
			case 'get':
				return $this->partnerGroup . ',0';
			//When requesting response profiles allow default once in case requesting partner is internal
			case 'list':
				if(kCurrentContext::$ks_partner_id <= 0)
					return $this->partnerGroup . ',0';
		}
			
		return $this->partnerGroup;
	}
	
	/**
	 * Add new response profile
	 * 
	 * @action add
	 * @param KontorolResponseProfile $addResponseProfile
	 * @return KontorolResponseProfile
	 */
	function addAction(KontorolResponseProfile $addResponseProfile)
	{
		$dbResponseProfile = $addResponseProfile->toInsertableObject();
		/* @var $dbResponseProfile ResponseProfile */
		$dbResponseProfile->setPartnerId($this->getPartnerId());
		$dbResponseProfile->setStatus(ResponseProfileStatus::ENABLED);
		$dbResponseProfile->save();
		
		$addResponseProfile = new KontorolResponseProfile();
		$addResponseProfile->fromObject($dbResponseProfile, $this->getResponseProfile());
		return $addResponseProfile;
	}
	
	/**
	 * Get response profile by id
	 * 
	 * @action get
	 * @param bigint $id
	 * @return KontorolResponseProfile
	 * 
	 * @throws KontorolErrors::RESPONSE_PROFILE_ID_NOT_FOUND
	 */
	function getAction($id)
	{
		$dbResponseProfile = ResponseProfilePeer::retrieveByPK($id);
		if (!$dbResponseProfile)
			throw new KontorolAPIException(KontorolErrors::RESPONSE_PROFILE_ID_NOT_FOUND, $id);
			
		$responseProfile = new KontorolResponseProfile();
		$responseProfile->fromObject($dbResponseProfile, $this->getResponseProfile());
		return $responseProfile;
	}
	
	/**
	 * Update response profile by id
	 * 
	 * @action update
	 * @param bigint $id
	 * @param KontorolResponseProfile $updateResponseProfile
	 * @return KontorolResponseProfile
	 * 
	 * @throws KontorolErrors::RESPONSE_PROFILE_ID_NOT_FOUND
	 */
	function updateAction($id, KontorolResponseProfile $updateResponseProfile)
	{
		$dbResponseProfile = ResponseProfilePeer::retrieveByPK($id);
		if (!$dbResponseProfile)
			throw new KontorolAPIException(KontorolErrors::RESPONSE_PROFILE_ID_NOT_FOUND, $id);
		
		$updateResponseProfile->toUpdatableObject($dbResponseProfile);
		$dbResponseProfile->save();
		
		$updateResponseProfile = new KontorolResponseProfile();
		$updateResponseProfile->fromObject($dbResponseProfile, $this->getResponseProfile());
		return $updateResponseProfile;
	}

	/**
	 * Update response profile status by id
	 * 
	 * @action updateStatus
	 * @param bigint $id
	 * @param KontorolResponseProfileStatus $status
	 * @return KontorolResponseProfile
	 * 
	 * @throws KontorolErrors::RESPONSE_PROFILE_ID_NOT_FOUND
	 */
	function updateStatusAction($id, $status)
	{
		$dbResponseProfile = ResponseProfilePeer::retrieveByPK($id);
		if (!$dbResponseProfile)
			throw new KontorolAPIException(KontorolErrors::RESPONSE_PROFILE_ID_NOT_FOUND, $id);

		if($status == KontorolResponseProfileStatus::ENABLED)
		{
			//Check uniqueness of new object's system name
			$systemNameProfile = ResponseProfilePeer::retrieveBySystemName($dbResponseProfile->getSystemName(), $id);
			if ($systemNameProfile)
				throw new KontorolAPIException(KontorolErrors::RESPONSE_PROFILE_DUPLICATE_SYSTEM_NAME, $dbResponseProfile->getSystemName());
		}	
		
		$dbResponseProfile->setStatus($status);
		$dbResponseProfile->save();
	
		$responseProfile = new KontorolResponseProfile();
		$responseProfile->fromObject($dbResponseProfile, $this->getResponseProfile());
		return $responseProfile;
	}
	
	/**
	 * Delete response profile by id
	 * 
	 * @action delete
	 * @param bigint $id
	 * 
	 * @throws KontorolErrors::RESPONSE_PROFILE_ID_NOT_FOUND
	 */
	function deleteAction($id)
	{
		$dbResponseProfile = ResponseProfilePeer::retrieveByPK($id);
		if (!$dbResponseProfile)
			throw new KontorolAPIException(KontorolErrors::RESPONSE_PROFILE_ID_NOT_FOUND, $id);

		$dbResponseProfile->setStatus(ResponseProfileStatus::DELETED);
		$dbResponseProfile->save();
	}
	
	/**
	 * List response profiles by filter and pager
	 * 
	 * @action list
	 * @param KontorolFilterPager $filter
	 * @param KontorolResponseProfileFilter $pager
	 * @return KontorolResponseProfileListResponse
	 */
	function listAction(KontorolResponseProfileFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
			$filter = new KontorolResponseProfileFilter();
		
		//Add partner 0 to filter only in case systemNmae or Id are provided in the filter to avoid returning it by default
		if(isset($filter->systemNameEqual) || isset($filter->idEqual)) {
			$this->partnerGroup .= ",0";
		}
		$this->applyPartnerFilterForClass('ResponseProfile');

		if (!$pager)
			$pager = new KontorolFilterPager();
			
		$responseProfileFilter = new ResponseProfileFilter();
		$filter->toObject($responseProfileFilter);

		$c = new Criteria();
		$responseProfileFilter->attachToCriteria($c);
		
		$totalCount = ResponseProfilePeer::doCount($c);
		
		$pager->attachToCriteria($c);
		$dbList = ResponseProfilePeer::doSelect($c);
		
		$list = KontorolResponseProfileArray::fromDbArray($dbList, $this->getResponseProfile());
		$response = new KontorolResponseProfileListResponse();
		$response->objects = $list;
		$response->totalCount = $totalCount;
		return $response;
	}
	
	/**
	 * Recalculate response profile cached objects
	 * 
	 * @action recalculate
	 * @param KontorolResponseProfileCacheRecalculateOptions $options
	 * @return KontorolResponseProfileCacheRecalculateResults
	 */
	function recalculateAction(KontorolResponseProfileCacheRecalculateOptions $options)
	{
		return KontorolResponseProfileCacher::recalculateCacheBySessionType($options);
	}
	
	/**
	 * Clone an existing response profile
	 * 
	 * @action clone
	 * @param bigint $id
	 * @param KontorolResponseProfile $profile
	 * @throws KontorolErrors::RESPONSE_PROFILE_ID_NOT_FOUND
	 * @throws KontorolErrors::RESPONSE_PROFILE_DUPLICATE_SYSTEM_NAME
	 * @return KontorolResponseProfile
	 */
	function cloneAction ($id, KontorolResponseProfile $profile)
	{
		$origResponseProfileDbObject = ResponseProfilePeer::retrieveByPK($id);
		if (!$origResponseProfileDbObject)
			throw new KontorolAPIException(KontorolErrors::RESPONSE_PROFILE_ID_NOT_FOUND, $id);
			
		$newResponseProfileDbObject = $origResponseProfileDbObject->copy();
		
		if ($profile)
			$newResponseProfileDbObject = $profile->toInsertableObject($newResponseProfileDbObject);
				
		$newResponseProfileDbObject->save();
		
		$newResponseProfile = new KontorolResponseProfile();
		$newResponseProfile->fromObject($newResponseProfileDbObject, $this->getResponseProfile());
		return $newResponseProfile;
	}
}

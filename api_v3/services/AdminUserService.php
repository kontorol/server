<?php
/**
 * Manage details for the administrative user
 *
 * @service adminUser
 * @package api
 * @subpackage services
 * @deprecated use user service instead
 */
class AdminUserService extends KontorolBaseUserService
{
	
	protected function partnerRequired($actionName)
	{
		if ($actionName === 'updatePassword') {
			return false;
		}
		if ($actionName === 'resetPassword') {
			return false;
		}
		if ($actionName === 'login') {
			return false;
		}
		if ($actionName === 'setInitialPassword') {
			return false;
		}
		return parent::partnerRequired($actionName);
	}	
	

	/**
	 * keep backward compatibility with changed error codes
	 * @param KontorolAPIException $e
	 * @throws KontorolAPIException
	 */
	private function throwTranslatedException(KontorolAPIException $e)
	{
		$code = $e->getCode();
		if ($code == KontorolErrors::USER_NOT_FOUND) {
			throw new KontorolAPIException(KontorolErrors::ADMIN_KUSER_NOT_FOUND);
		}
		else if ($code == KontorolErrors::WRONG_OLD_PASSWORD) {
			throw new KontorolAPIException(KontorolErrors::ADMIN_KUSER_WRONG_OLD_PASSWORD, "wrong password" );
		}
		else if ($code == KontorolErrors::USER_WRONG_PASSWORD) {
			throw new KontorolAPIException(KontorolErrors::ADMIN_KUSER_NOT_FOUND);
		}
		else if ($code == KontorolErrors::LOGIN_DATA_NOT_FOUND) {
			throw new KontorolAPIException(KontorolErrors::ADMIN_KUSER_NOT_FOUND);
		}
		throw $e;
	}
	
	
	/**
	 * Update admin user password and email
	 * 
	 * @action updatePassword
	 * @param string $email
	 * @param string $password
	 * @param string $newEmail Optional, provide only when you want to update the email
	 * @param string $newPassword
	 * @param string $otp the user's one-time password
	 * @return KontorolAdminUser
	 * @ksIgnored
	 *
	 * @throws KontorolErrors::INVALID_FIELD_VALUE
	 * @throws KontorolErrors::ADMIN_KUSER_WRONG_OLD_PASSWORD
	 * @throws KontorolErrors::ADMIN_KUSER_NOT_FOUND
	 * @throws KontorolErrors::PASSWORD_STRUCTURE_INVALID
	 * @throws KontorolErrors::PASSWORD_ALREADY_USED
	 * @throws KontorolErrors::INVALID_FIELD_VALUE
	 * @throws KontorolErrors::LOGIN_ID_ALREADY_USED
	 * @throws KontorolErrors::INVALID_OTP
	 * @throws KontorolErrors::MISSING_OTP
	 * 
	 * @deprecated
	 */
	public function updatePasswordAction( $email , $password , $newEmail = "" , $newPassword = "", $otp = null)
	{
		try
		{
			parent::updateLoginDataImpl($email, $password, $newEmail, $newPassword, null, null, $otp);
			
			// copy required parameters to a KontorolAdminUser object for backward compatibility
			$adminUser = new KontorolAdminUser();
			$adminUser->email = $newEmail ? $newEmail : $email;
			$adminUser->password = $newPassword ? $newPassword : $password;
			
			return $adminUser;
		}
		catch (KontorolAPIException $e) // keep backward compatibility with changed error codes
		{
			$this->throwTranslatedException($e);
		}
	}
	
	
	/**
	 * Reset admin user password and send it to the users email address
	 * 
	 * @action resetPassword
	 * @param string $email
	 * @ksIgnored
	 *
	 * @throws KontorolErrors::ADMIN_KUSER_NOT_FOUND
	 * @throws KontorolErrors::PASSWORD_STRUCTURE_INVALID
	 * @throws KontorolErrors::PASSWORD_ALREADY_USED
	 * @throws KontorolErrors::INVALID_FIELD_VALUE
	 * @throws KontorolErrors::LOGIN_ID_ALREADY_USED
	 */	
	public function resetPasswordAction($email)
	{
		try
		{
			return parent::resetPasswordImpl($email);
		}
		catch (KontorolAPIException $e) // keep backward compatibility with changed error codes
		{
			$this->throwTranslatedException($e);
		}
	}
	
	/**
	 * Get an admin session using admin email and password (Used for login to the KMC application)
	 * 
	 * @action login
	 * @param string $email
	 * @param string $password
	 * @param int $partnerId
	 * @return string
	 * @ksIgnored
	 *
	 * @throws KontorolErrors::ADMIN_KUSER_NOT_FOUND
	 * @thrown KontorolErrors::INVALID_PARTNER_ID
	 * @thrown KontorolErrors::LOGIN_RETRIES_EXCEEDED
	 * @thrown KontorolErrors::LOGIN_BLOCKED
	 * @thrown KontorolErrors::PASSWORD_EXPIRED
	 * @thrown KontorolErrors::INVALID_PARTNER_ID
	 * @thrown KontorolErrors::INTERNAL_SERVERL_ERROR
	 */		
	public function loginAction($email, $password, $partnerId = null)
	{
		try
		{
			$ks = parent::loginImpl(null, $email, $password, $partnerId);
			$tempKs = kSessionUtils::crackKs($ks);
			if (!$tempKs->isAdmin()) {
				throw new KontorolAPIException(KontorolErrors::ADMIN_KUSER_NOT_FOUND);
			}
			return $ks;
		}
		catch (KontorolAPIException $e) // keep backward compatibility with changed error codes
		{
			$this->throwTranslatedException($e);
		}
	}
	
	
	
	/**
	 * Set initial users password
	 * 
	 * @action setInitialPassword
	 * @param string $hashKey
	 * @param string $newPassword new password to set
	 * @return KontorolAuthentication The authentication response
	 * @ksIgnored
	 *
	 * @throws KontorolErrors::ADMIN_KUSER_NOT_FOUND
	 * @throws KontorolErrors::PASSWORD_STRUCTURE_INVALID
	 * @throws KontorolErrors::NEW_PASSWORD_HASH_KEY_EXPIRED
	 * @throws KontorolErrors::NEW_PASSWORD_HASH_KEY_INVALID
	 * @throws KontorolErrors::PASSWORD_ALREADY_USED
	 * @throws KontorolErrors::INTERNAL_SERVERL_ERROR
	 */	
	public function setInitialPasswordAction($hashKey, $newPassword)
	{
		try
		{
			return parent::setInitialPasswordImpl($hashKey, $newPassword);
		}
		catch (KontorolAPIException $e) // keep backward compatibility with changed error codes
		{
			$this->throwTranslatedException($e);
		}
	}
	
	
}

<?php

/**
 * Manage live channel segments
 *
 * @service liveChannelSegment
 */
class LiveChannelSegmentService extends KontorolBaseService
{
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		$this->applyPartnerFilterForClass('LiveChannelSegment'); 	
		
		if(!PermissionPeer::isValidForPartner(PermissionName::FEATURE_LIVE_CHANNEL, $this->getPartnerId()))
			throw new KontorolAPIException(KontorolErrors::SERVICE_FORBIDDEN, $this->serviceName.'->'.$this->actionName);
	}
	
	/**
	 * Add new live channel segment
	 * 
	 * @action add
	 * @param KontorolLiveChannelSegment $liveChannelSegment
	 * @return KontorolLiveChannelSegment
	 */
	function addAction(KontorolLiveChannelSegment $liveChannelSegment)
	{
		$dbLiveChannelSegment = $liveChannelSegment->toInsertableObject();
		$dbLiveChannelSegment->setPartnerId($this->getPartnerId());
		$dbLiveChannelSegment->setStatus(LiveChannelSegmentStatus::ACTIVE);
		$dbLiveChannelSegment->save();
		
		$liveChannelSegment = new KontorolLiveChannelSegment();
		$liveChannelSegment->fromObject($dbLiveChannelSegment, $this->getResponseProfile());
		return $liveChannelSegment;
	}
	
	/**
	 * Get live channel segment by id
	 * 
	 * @action get
	 * @param bigint $id
	 * @return KontorolLiveChannelSegment
	 * 
	 * @throws KontorolErrors::LIVE_CHANNEL_SEGMENT_ID_NOT_FOUND
	 */
	function getAction($id)
	{
		$dbLiveChannelSegment = LiveChannelSegmentPeer::retrieveByPK($id);
		if (!$dbLiveChannelSegment)
			throw new KontorolAPIException(KontorolErrors::LIVE_CHANNEL_SEGMENT_ID_NOT_FOUND, $id);
			
		$liveChannelSegment = new KontorolLiveChannelSegment();
		$liveChannelSegment->fromObject($dbLiveChannelSegment, $this->getResponseProfile());
		return $liveChannelSegment;
	}
	
	/**
	 * Update live channel segment by id
	 * 
	 * @action update
	 * @param bigint $id
	 * @param KontorolLiveChannelSegment $liveChannelSegment
	 * @return KontorolLiveChannelSegment
	 * 
	 * @throws KontorolErrors::LIVE_CHANNEL_SEGMENT_ID_NOT_FOUND
	 */
	function updateAction($id, KontorolLiveChannelSegment $liveChannelSegment)
	{
		$dbLiveChannelSegment = LiveChannelSegmentPeer::retrieveByPK($id);
		if (!$dbLiveChannelSegment)
			throw new KontorolAPIException(KontorolErrors::LIVE_CHANNEL_SEGMENT_ID_NOT_FOUND, $id);
		
		$liveChannelSegment->toUpdatableObject($dbLiveChannelSegment);
		$dbLiveChannelSegment->save();
		
		$liveChannelSegment = new KontorolLiveChannelSegment();
		$liveChannelSegment->fromObject($dbLiveChannelSegment, $this->getResponseProfile());
		return $liveChannelSegment;
	}
	
	/**
	 * Delete live channel segment by id
	 * 
	 * @action delete
	 * @param bigint $id
	 * 
	 * @throws KontorolErrors::LIVE_CHANNEL_SEGMENT_ID_NOT_FOUND
	 */
	function deleteAction($id)
	{
		$dbLiveChannelSegment = LiveChannelSegmentPeer::retrieveByPK($id);
		if (!$dbLiveChannelSegment)
			throw new KontorolAPIException(KontorolErrors::LIVE_CHANNEL_SEGMENT_ID_NOT_FOUND, $id);

		$dbLiveChannelSegment->setStatus(LiveChannelSegmentStatus::DELETED);
		$dbLiveChannelSegment->save();
	}
	
	/**
	 * List live channel segments by filter and pager
	 * 
	 * @action list
	 * @param KontorolFilterPager $filter
	 * @param KontorolLiveChannelSegmentFilter $pager
	 * @return KontorolLiveChannelSegmentListResponse
	 */
	function listAction(KontorolLiveChannelSegmentFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
			$filter = new KontorolLiveChannelSegmentFilter();
			
		if (!$pager)
			$pager = new KontorolFilterPager();
			
		return $filter->getListResponse($pager, $this->getResponseProfile());
	}
}

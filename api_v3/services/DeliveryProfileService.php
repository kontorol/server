<?php

/**
 * delivery service is used to control delivery objects
 *
 * @service deliveryProfile
 * @package api
 * @subpackage services
 */
class DeliveryProfileService extends KontorolBaseService
{
	
	/**
	 * Add new delivery.
	 *
	 * @action add
	 * @param KontorolDeliveryProfile $delivery
	 * @return KontorolDeliveryProfile
	 */
	function addAction(KontorolDeliveryProfile $delivery)
	{
		$dbKontorolDelivery = $delivery->toInsertableObject();
		$dbKontorolDelivery->setPartnerId($this->getPartnerId());
		$dbKontorolDelivery->setParentId(0);
		$dbKontorolDelivery->save();
		
		$delivery = KontorolDeliveryProfileFactory::getDeliveryProfileInstanceByType($dbKontorolDelivery->getType());
		$delivery->fromObject($dbKontorolDelivery, $this->getResponseProfile());
		return $delivery;
	}
	
	/**
	 * Update existing delivery profile
	 *
	 * @action update
	 * @param string $id
	 * @param KontorolDeliveryProfile $delivery
	 * @return KontorolDeliveryProfile
	 */
	function updateAction( $id , KontorolDeliveryProfile $delivery )
	{
		DeliveryProfilePeer::setUseCriteriaFilter(false);
		$dbDelivery = DeliveryProfilePeer::retrieveByPK($id);
		DeliveryProfilePeer::setUseCriteriaFilter(true);
		if (!$dbDelivery)
			throw new KontorolAPIException(KontorolErrors::DELIVERY_ID_NOT_FOUND, $id);
		
		// Don't allow to update default delivery profiles from the outside
		if($dbDelivery->getIsDefault())
			throw new KontorolAPIException(KontorolErrors::DELIVERY_UPDATE_ISNT_ALLOWED, $id);
		
		$delivery->toUpdatableObject($dbDelivery);
		$dbDelivery->save();
		
		$delivery = KontorolDeliveryProfileFactory::getDeliveryProfileInstanceByType($dbDelivery->getType());
		$delivery->fromObject($dbDelivery, $this->getResponseProfile());
		return $delivery;
	}
	
	/**
	* Get delivery by id
	*
	* @action get
	* @param string $id
	* @return KontorolDeliveryProfile
	*/
	function getAction( $id )
	{
		DeliveryProfilePeer::setUseCriteriaFilter(false);
		$dbDelivery = DeliveryProfilePeer::retrieveByPK($id);
		DeliveryProfilePeer::setUseCriteriaFilter(true);
		
		if (!$dbDelivery)
			throw new KontorolAPIException(KontorolErrors::DELIVERY_ID_NOT_FOUND, $id);
			
		$delivery = KontorolDeliveryProfileFactory::getDeliveryProfileInstanceByType($dbDelivery->getType());
		$delivery->fromObject($dbDelivery, $this->getResponseProfile());
		return $delivery;
	}
	
	/**
	* Add delivery based on existing delivery.
	* Must provide valid sourceDeliveryId
	*
	* @action clone
	* @param int $deliveryId
	* @return KontorolDeliveryProfile
	*/
	function cloneAction( $deliveryId )
	{
		$dbDelivery = DeliveryProfilePeer::retrieveByPK( $deliveryId );
		
		if ( ! $dbDelivery )
			throw new KontorolAPIException ( APIErrors::DELIVERY_ID_NOT_FOUND , $deliveryId );
		
		$className = get_class($dbDelivery);
		$class = new ReflectionClass($className);
		$dbKontorolDelivery = $class->newInstanceArgs(array());
		$dbKontorolDelivery = $dbDelivery->cloneToNew ( $dbKontorolDelivery );
		
		$delivery = KontorolDeliveryProfileFactory::getDeliveryProfileInstanceByType($dbKontorolDelivery->getType());
		$delivery->fromObject($dbKontorolDelivery, $this->getResponseProfile());
		return $delivery;
	}
	
	/**
	* Retrieve a list of available delivery depends on the filter given
	*
	* @action list
	* @param KontorolDeliveryProfileFilter $filter
	* @param KontorolFilterPager $pager
	* @return KontorolDeliveryProfileListResponse
	*/
	function listAction( KontorolDeliveryProfileFilter $filter=null , KontorolFilterPager $pager=null)
	{
		if (!$filter)
			$filter = new KontorolDeliveryProfileFilter();

		if (!$pager)
			$pager = new KontorolFilterPager();
			
		$delivery = new DeliveryProfileFilter();
		$filter->toObject($delivery);

		DeliveryProfilePeer::setUseCriteriaFilter(false);
		
		$c = new Criteria();
		$c->add(DeliveryProfilePeer::PARTNER_ID, array(0, kCurrentContext::getCurrentPartnerId()), Criteria::IN);
		$delivery->attachToCriteria($c);
		
		$totalCount = DeliveryProfilePeer::doCount($c);
		
		$pager->attachToCriteria($c);
		$dbList = DeliveryProfilePeer::doSelect($c);
		
		DeliveryProfilePeer::setUseCriteriaFilter(true);
		
		$objects = KontorolDeliveryProfileArray::fromDbArray($dbList, $this->getResponseProfile());
		$response = new KontorolDeliveryProfileListResponse();
		$response->objects = $objects;
		$response->totalCount = $totalCount;
		return $response;    
	}
}


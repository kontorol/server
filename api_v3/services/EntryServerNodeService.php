<?php
/**
 * Base class for entry server node
 *
 * @service entryServerNode
 * @package api
 * @subpackage services
 */
class EntryServerNodeService extends KontorolBaseService
{

	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		$this->applyPartnerFilterForClass("entry");
		$this->applyPartnerFilterForClass("entryServerNode");
	}

	/**
	 * Adds a entry_user_node to the Kontorol DB.
	 *
	 * @action add
	 * @param KontorolEntryServerNode $entryServerNode
	 * @return KontorolEntryServerNode
	 */
	private function addAction(KontorolEntryServerNode $entryServerNode)
	{
		$dbEntryServerNode = $this->addNewEntryServerNode($entryServerNode);

		$te = new TrackEntry();
		$te->setEntryId($dbEntryServerNode->getEntryId());
		$te->setTrackEventTypeId(TrackEntry::TRACK_ENTRY_EVENT_TYPE_ADD_ENTRY);
		$te->setDescription(__METHOD__ . ":" . __LINE__ . "::" . $dbEntryServerNode->getServerType().":".$dbEntryServerNode->getServerNodeId());
		TrackEntry::addTrackEntry($te);

		$entryServerNode = KontorolEntryServerNode::getInstance($dbEntryServerNode, $this->getResponseProfile());
		return $entryServerNode;

	}

	private function addNewEntryServerNode(KontorolEntryServerNode $entryServerNode)
	{
		$dbEntryServerNode = $entryServerNode->toInsertableObject();
		/* @var $dbEntryServerNode EntryServerNode */
		$dbEntryServerNode->setPartnerId($this->getPartnerId());
		$dbEntryServerNode->setStatus(EntryServerNodeStatus::STOPPED);
		$dbEntryServerNode->save();

		return $dbEntryServerNode;
	}

	/**
	 *
	 * @action update
	 * @param int $id
	 * @param KontorolEntryServerNode $entryServerNode
	 * @return KontorolEntryServerNode|null|object
	 * @throws KontorolAPIException
	 */
	public function updateAction($id, KontorolEntryServerNode $entryServerNode)
	{
		$dbEntryServerNode = EntryServerNodePeer::retrieveByPK($id);
		if (!$dbEntryServerNode)
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $id);

		$dbEntryServerNode = $entryServerNode->toUpdatableObject($dbEntryServerNode);
		$dbEntryServerNode->save();

		$entryServerNode = KontorolEntryServerNode::getInstance($dbEntryServerNode, $this->getResponseProfile());
		return $entryServerNode;
	}

	/**
	 * Deletes the row in the database
	 * @action delete
	 * @param int $id
	 * @throws KontorolAPIException
	 */
	private function deleteAction($id)
	{
		$dbEntryServerNode = EntryServerNodePeer::retrieveByPK($id);
		if (!$dbEntryServerNode)
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $id);
		$dbEntryServerNode->deleteOrMarkForDeletion();

	}

	/**
	 * @action list
	 * @param KontorolEntryServerNodeFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return KontorolEntryServerNodeListResponse
	 */
	public function listAction(KontorolEntryServerNodeFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
			$filter = new KontorolEntryServerNodeFilter();
		if (!$pager)
			$pager = new KontorolFilterPager();

		return $filter->getListResponse($pager, $this->getResponseProfile());
	}

	/**
	 * @action get
	 * @param string $id
	 * @return KontorolEntryServerNode
	 * @throws KontorolAPIException
	 */
	public function getAction($id)
	{
		$dbEntryServerNode = EntryServerNodePeer::retrieveByPK( $id );
		if(!$dbEntryServerNode)
			throw new KontorolAPIException(KontorolErrors::ENTRY_SERVER_NODE_NOT_FOUND, $id);

		$entryServerNode = KontorolEntryServerNode::getInstance($dbEntryServerNode);
		if (!$entryServerNode)
			return null;
		$entryServerNode->fromObject($dbEntryServerNode);
		return $entryServerNode;
	}
	
	/**
	 * Validates server node still registered on entry
	 *
	 * @action validateRegisteredEntryServerNode
	 * @param int $id entry server node id
	 *
	 * @throws KontorolAPIException
	 */
	public function validateRegisteredEntryServerNodeAction($id)
	{
		KontorolResponseCacher::disableCache();
		
		$dbEntryServerNode = EntryServerNodePeer::retrieveByPK( $id );
		if(!$dbEntryServerNode)
			throw new KontorolAPIException(KontorolErrors::ENTRY_SERVER_NODE_NOT_FOUND, $id);
		
		/* @var EntryServerNode $dbEntryServerNode */
		$dbEntryServerNode->validateEntryServerNode();
	}

	/**
	 * @action updateStatus
	 * @param string $id
	 * @param KontorolEntryServerNodeStatus $status
	 * @return KontorolEntryServerNode
	 * @throws KontorolAPIException
	 */
	public function updateStatusAction($id, $status)
	{
		$dbEntryServerNode = EntryServerNodePeer::retrieveByPK($id);
		if(!$dbEntryServerNode)
			throw new KontorolAPIException(KontorolErrors::ENTRY_SERVER_NODE_NOT_FOUND, $id);

		$dbEntryServerNode->setStatus($status);
		$dbEntryServerNode->save();

		$entryServerNode = KontorolEntryServerNode::getInstance($dbEntryServerNode);
		return $entryServerNode;
	}
}

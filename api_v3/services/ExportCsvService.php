<?php
/**
 * Export CSV service is used to manage CSV exports of objects
 *
 * @service exportcsv
 * @package api
 * @subpackage services
 */
class ExportCsvService extends KontorolBaseService
{
	const SERVICE_NAME = "exportCsv";
	
	
	
	/**
	 * Creates a batch job that sends an email with a link to download a CSV containing a list of users
	 *
	 * @action userExportToCsv
	 * @actionAlias user.exportToCsv
	 * @param KontorolUserFilter $filter A filter used to exclude specific types of users
	 * @param int $metadataProfileId
	 * @param KontorolCsvAdditionalFieldInfoArray $additionalFields
	 * @return string
	 *
	 * @throws APIErrors::USER_EMAIL_NOT_FOUND
	 * @throws MetadataErrors::INVALID_METADATA_PROFILE
	 * @throws MetadataErrors::METADATA_PROFILE_NOT_SPECIFIED
	 */
	public function userExportToCsvAction (KontorolUserFilter $filter = null, $metadataProfileId = null, $additionalFields = null)
	{
		if($metadataProfileId)
		{
			$metadataProfile = MetadataProfilePeer::retrieveByPK($metadataProfileId);
			if (!$metadataProfile || ($metadataProfile->getPartnerId() != $this->getPartnerId()))
				throw new KontorolAPIException(MetadataErrors::INVALID_METADATA_PROFILE, $metadataProfileId);
		}
		else
		{
			if($additionalFields->count)
				throw new KontorolAPIException(MetadataErrors::METADATA_PROFILE_NOT_SPECIFIED, $metadataProfileId);
		}
		
		if (!$filter)
			$filter = new KontorolUserFilter();
		$dbFilter = new kuserFilter();
		$filter->toObject($dbFilter);
		
		$kuser = $this->getKuser();
		if(!$kuser || !$kuser->getEmail())
			throw new KontorolAPIException(APIErrors::USER_EMAIL_NOT_FOUND, $kuser);
		
		$jobData = new kUsersCsvJobData();
		$jobData->setFilter($dbFilter);
		$jobData->setMetadataProfileId($metadataProfileId);
		$jobData->setAdditionalFields($additionalFields);
		$jobData->setUserMail($kuser->getEmail());
		$jobData->setUserName($kuser->getPuserId());
		
		kJobsManager::addExportCsvJob($jobData, $this->getPartnerId(), ExportObjectType::USER);
		
		return $kuser->getEmail();
	}
	
	
	/**
	 *
	 * Will serve a requested CSV
	 * @action serveCsv
	 *
	 *
	 * @param string $id - the requested file id
	 * @return string
	 */
	public function serveCsvAction($id)
	{
		$file_path = self::generateCsvPath($id, $this->getKs());
		
		return $this->dumpFile($file_path, 'text/csv');
	}
	
	/**
	 * Generic CSV file path generator - used from any action which calls the generateCsvPath
	 *
	 * @param string $id
	 * @param string $ks
	 * @return string
	 * @throws KontorolAPIException
	 */
	public static function generateCsvPath($id, $ks)
	{
		if(!preg_match('/^\w+\.csv$/', $id))
			throw new KontorolAPIException(KontorolErrors::INVALID_ID, $id);

		// KS verification - we accept either admin session or download privilege of the file
		if(!$ks->verifyPrivileges(ks::PRIVILEGE_DOWNLOAD, $id))
			KExternalErrors::dieError(KExternalErrors::ACCESS_CONTROL_RESTRICTED);

		$partnerId = kCurrentContext::getCurrentPartnerId();
		$fullPath = kPathManager::getExportCsvSharedPath($partnerId, $id, true);
		if ($fullPath && kFile::checkFileExists($fullPath))
		{
				return $fullPath;
		}
		return kPathManager::getExportCsvSharedPath($partnerId , $id);
	}
	
}

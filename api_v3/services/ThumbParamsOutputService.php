<?php
/**
 * Thumbnail Params Output service
 *
 * @service thumbParamsOutput
 * @package api
 * @subpackage services
 */
class ThumbParamsOutputService extends KontorolBaseService
{
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		
		if($this->getPartnerId() != Partner::BATCH_PARTNER_ID && $this->getPartnerId() != Partner::ADMIN_CONSOLE_PARTNER_ID)
			throw new KontorolAPIException(KontorolErrors::SERVICE_FORBIDDEN, $this->serviceName.'->'.$this->actionName);
	}
	
	/**
	 * Get thumb params output object by ID
	 * 
	 * @action get
	 * @param int $id
	 * @return KontorolThumbParamsOutput
	 * @throws KontorolErrors::THUMB_PARAMS_OUTPUT_ID_NOT_FOUND
	 */
	public function getAction($id)
	{
		$thumbParamsOutputDb = assetParamsOutputPeer::retrieveByPK($id);
		
		if (!$thumbParamsOutputDb)
			throw new KontorolAPIException(KontorolErrors::THUMB_PARAMS_OUTPUT_ID_NOT_FOUND, $id);
			
		$thumbParamsOutput = new KontorolThumbParamsOutput();
		$thumbParamsOutput->fromObject($thumbParamsOutputDb, $this->getResponseProfile());
		
		return $thumbParamsOutput;
	}
	
	/**
	 * List thumb params output objects by filter and pager
	 * 
	 * @action list
	 * @param KontorolThumbParamsOutputFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return KontorolThumbParamsOutputListResponse
	 */
	function listAction(KontorolThumbParamsOutputFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
			$filter = new KontorolThumbParamsOutputFilter();
			
		if(!$pager)
		{
			$pager = new KontorolFilterPager();
		}
			
		$types = KontorolPluginManager::getExtendedTypes(assetParamsOutputPeer::OM_CLASS, assetType::THUMBNAIL);
		return $filter->getTypeListResponse($pager, $this->getResponseProfile(), $types);
	}
}

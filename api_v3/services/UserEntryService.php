<?php

/**
 * @service userEntry
 * @package api
 * @subpackage services
 */
class UserEntryService extends KontorolBaseService {

	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		$this->applyPartnerFilterForClass('userEntry');
	}

	/**
	 * Adds a user_entry to the Kontorol DB.
	 *
	 * @action add
	 * @param KontorolUserEntry $userEntry
	 * @return KontorolUserEntry
	 */
	public function addAction(KontorolUserEntry $userEntry)
	{
		$dbUserEntry = $userEntry->toInsertableObject(null, array('type'));
		$lockUser = $userEntry->userId ? $userEntry->userId : kCurrentContext::getCurrentKsKuserId();
		$lockKey = "userEntry_add_" . $this->getPartnerId() . $userEntry->entryId . $lockUser;
		$dbUserEntry = kLock::runLocked($lockKey, array($this, 'addUserEntryImpl'), array($dbUserEntry));
		$userEntry->fromObject($dbUserEntry, $this->getResponseProfile());

		return $userEntry;
	}
	
	public function addUserEntryImpl($dbUserEntry)
	{
		if($dbUserEntry->checkAlreadyExists())
		{
			throw new KontorolAPIException(KontorolErrors::USER_ENTRY_ALREADY_EXISTS);
		}
		$dbUserEntry->save();
		
		return $dbUserEntry;
	}

	/**
	 *
	 * @action update
	 * @param int $id
	 * @param KontorolUserEntry $userEntry
	 * @return KontorolUserEntry
	 * @throws KontorolAPIException
	 */
	public function updateAction($id, KontorolUserEntry $userEntry)
	{
		$dbUserEntry = UserEntryPeer::retrieveByPK($id);
		if (!$dbUserEntry)
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $id);

		$dbUserEntry = $userEntry->toUpdatableObject($dbUserEntry);
		$dbUserEntry->save();
		
		$userEntry->fromObject($dbUserEntry);
		
		return $userEntry;
	}

	/**
	 * @action delete
	 * @param int $id
	 * @return KontorolUserEntry The deleted UserEntry object
 	 * @throws KontorolAPIException
	 */
	public function deleteAction($id)
	{
		$dbUserEntry = UserEntryPeer::retrieveByPK($id);
		if (!$dbUserEntry)
			throw new KontorolAPIException(KontorolErrors::INVALID_OBJECT_ID, $id);
		$dbUserEntry->setStatus(KontorolUserEntryStatus::DELETED);
		$dbUserEntry->save();

		$userEntry = KontorolUserEntry::getInstanceByType($dbUserEntry->getType());
		$userEntry->fromObject($dbUserEntry, $this->getResponseProfile());

		return $userEntry;

	}

	/**
	 * @action list
	 * @param KontorolUserEntryFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return KontorolUserEntryListResponse
	 */
	public function listAction(KontorolUserEntryFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if(!$filter)
		{
			$filter = new KontorolUserEntryFilter();
		}
		
		if (!$pager)
		{
			$pager = new KontorolFilterPager();
		}
		// return empty list when userId was not given
		if ( $this->getKs() && !$this->getKs()->isAdmin() && !kCurrentContext::$ks_uid )
		{
			return $filter->getEmptyListResponse();
		}
		return $filter->getListResponse($pager, $this->getResponseProfile());
	}

	/**
	 * @action get
	 * @param string $id
	 * @return KontorolUserEntry
	 * @throws KontorolAPIException
	 */
	public function getAction($id)
	{
		$dbUserEntry = UserEntryPeer::retrieveByPK( $id );
		if(!$dbUserEntry)
			throw new KontorolAPIException(KontorolErrors::USER_ENTRY_NOT_FOUND, $id);

		$userEntry = KontorolUserEntry::getInstanceByType($dbUserEntry->getType());
		if (!$userEntry)
			return null;
		$userEntry->fromObject($dbUserEntry);
		return $userEntry;
	}
}

<?php
/**
 * UiConf service lets you create and manage your UIConfs for the various flash components
 * This service is used by the KMC-ApplicationStudio
 *
 * @service uiConf
 * @package api
 * @subpackage services
 */
class UiConfService extends KontorolBaseService
{
	// use initService to add a peer to the partner filter
	/**
	 * @ignore
	 */
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		if(strtolower($actionName) != 'listtemplates')
			$this->applyPartnerFilterForClass('uiConf'); 	
	}
	
	protected function partnerGroup($peer = null)
	{
		if ($this->actionName === 'get' || $this->actionName === 'clone')
			return '0';
		
		return parent::partnerGroup();
	}
	
	protected function kontorolNetworkAllowed($actionName)
	{
		if ($actionName === 'get') {
			return true;
		}
		if ($actionName === 'clone') {
			return true;
		}
		return parent::kontorolNetworkAllowed($actionName);
	}
	
	
	/**
	 * UIConf Add action allows you to add a UIConf to Kontorol DB
	 * 
	 * @action add
	 * @param KontorolUiConf $uiConf Mandatory input parameter of type KontorolUiConf
	 * @return KontorolUiConf
	 */
	function addAction( KontorolUiConf $uiConf )
	{
		$uiConf->validatePropertyNotNull('creationMode');
		if($uiConf->creationMode != KontorolUiConfCreationMode::ADVANCED && $uiConf->creationMode != KontorolUiConfCreationMode::WIZARD)
		{
			throw new KontorolAPIException ( "Should not create MANUAL ui_confs via the API!! MANUAL is deprecated" );
		}
		
		// if not specified set to true (default)
		if(is_null($uiConf->useCdn))
			$uiConf->useCdn = true;
		
		$dbUiConf = $uiConf->toUiConf();
		$dbUiConf->setPartnerId ( $this->getPartnerId() );
		$dbUiConf->save();
		
		$uiConf = new KontorolUiConf(); // start from blank
		$uiConf->fromObject($dbUiConf, $this->getResponseProfile());
		
		return $uiConf;
	}
	
	/**
	 * Update an existing UIConf
	 * 
	 * @action update
	 * @param int $id 
	 * @param KontorolUiConf $uiConf
	 * @return KontorolUiConf
	 *
	 * @throws APIErrors::INVALID_UI_CONF_ID
	 */	
	function updateAction( $id , KontorolUiConf $uiConf )
	{
		$dbUiConf = uiConfPeer::retrieveByPK( $id );
		
		if ( ! $dbUiConf )
			throw new KontorolAPIException ( APIErrors::INVALID_UI_CONF_ID , $id );
		
		$dbUiConf = $uiConf->toUpdatableObject($dbUiConf);
		
		$dbUiConf->save();
		$uiConf->fromObject($dbUiConf, $this->getResponseProfile());
		
		return $uiConf;
	}	

	/**
	 * Retrieve a UIConf by id
	 * 
	 * @action get
	 * @param int $id 
	 * @return KontorolUiConf
	 *
	 * @throws APIErrors::INVALID_UI_CONF_ID
	 */		
	function getAction(  $id )
	{
		$dbUiConf = uiConfPeer::retrieveByPK( $id );
		
		if ( ! $dbUiConf )
			throw new KontorolAPIException ( APIErrors::INVALID_UI_CONF_ID , $id );
		$uiConf = new KontorolUiConf();
		$uiConf->fromObject($dbUiConf, $this->getResponseProfile());
		
		return $uiConf;
	}

	/**
	 * Delete an existing UIConf
	 * 
	 * @action delete
	 * @param int $id
	 *
	 * @throws APIErrors::INVALID_UI_CONF_ID
	 */		
	function deleteAction(  $id )
	{
		$dbUiConf = uiConfPeer::retrieveByPK( $id );
		
		if ( ! $dbUiConf )
			throw new KontorolAPIException ( APIErrors::INVALID_UI_CONF_ID , $id );
		
		$dbUiConf->setStatus ( uiConf::UI_CONF_STATUS_DELETED );

		$dbUiConf->save();
	}

	/**
	 * Clone an existing UIConf
	 * 
	 * @action clone
	 * @param int $id 
	 * @return KontorolUiConf
	 *
	 * @throws APIErrors::INVALID_UI_CONF_ID
	 */	
	// TODO - get the new data of uiConf - will help override the parameters without needing to call update 
	function cloneAction( $id ) // , KontorolUiConf $_uiConf )
	{
		$dbUiConf = uiConfPeer::retrieveByPK( $id );
		
		if ( ! $dbUiConf )
			throw new KontorolAPIException ( APIErrors::INVALID_UI_CONF_ID , $id );
		$ui_conf_verride_params = new uiConf();
		$ui_conf_verride_params->setPartnerId( $this->getPartnerId() );
		$ui_conf_verride_params->setDisplayInSearch(1);  // the cloned ui_conf should NOT be a template
			
		$uiConfClone = $dbUiConf->cloneToNew ( $ui_conf_verride_params );

		$uiConf = new KontorolUiConf();
		$uiConf->fromObject($uiConfClone, $this->getResponseProfile());
		
		return $uiConf;
	}
	
	/**
	 * retrieve a list of available template UIConfs
	 *
	 * @action listTemplates
	 * @param KontorolUiConfFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return KontorolUiConfListResponse
	 */
	function listTemplatesAction(KontorolUiConfFilter $filter = null , KontorolFilterPager $pager = null)
	{
		$templatePartnerId = 0;
		if ($this->getPartnerId() !== NULL)
		{
		        $partner = PartnerPeer::retrieveByPK($this->getPartnerId());
		        $templatePartnerId = $partner ? $partner->getTemplatePartnerId() : 0;
		}
		
		$templateCriteria = new Criteria();
		$templateCriteria->add(uiConfPeer::DISPLAY_IN_SEARCH , mySearchUtils::DISPLAY_IN_SEARCH_KONTOROL_NETWORK , Criteria::GREATER_EQUAL);
		$templateCriteria->addAnd(uiConfPeer::PARTNER_ID, $templatePartnerId);
		
		if (!$filter)
		        $filter = new KontorolUiConfFilter;
		$uiConfFilter = new uiConfFilter ();
		$filter->toObject( $uiConfFilter );
		$uiConfFilter->attachToCriteria( $templateCriteria);
		
		$count = uiConfPeer::doCount( $templateCriteria );
		if (!$pager)
		        $pager = new KontorolFilterPager ();
		$pager->attachToCriteria( $templateCriteria );
		$list = uiConfPeer::doSelect( $templateCriteria );
		$newList = KontorolUiConfArray::fromDbArray($list, $this->getResponseProfile());
		$response = new KontorolUiConfListResponse();
		$response->objects = $newList;
		$response->totalCount = $count;
		return $response;	
	}
	
	/**
	 * Retrieve a list of available UIConfs
	 * 
	 * @action list
	 * @param KontorolUiConfFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return KontorolUiConfListResponse
	 */		
	function listAction( KontorolUiConfFilter $filter = null , KontorolFilterPager $pager = null)
	{
	    myDbHelper::$use_alternative_con = myDbHelper::DB_HELPER_CONN_PROPEL2;
	    
		if (!$filter)
			$filter = new KontorolUiConfFilter;
		$uiConfFilter = new uiConfFilter ();
		$filter->toObject( $uiConfFilter );
		
		$c = new Criteria();
		$uiConfFilter->attachToCriteria( $c );
		$count = uiConfPeer::doCount( $c );
		if (! $pager)
			$pager = new KontorolFilterPager ();
		$pager->attachToCriteria( $c );
		$list = uiConfPeer::doSelect( $c );
		
		$newList = KontorolUiConfArray::fromDbArray($list, $this->getResponseProfile());
		
		$response = new KontorolUiConfListResponse();
		$response->objects = $newList;
		$response->totalCount = $count;
		
		return $response;
	}
	
	/**
	 * Retrieve a list of all available versions by object type
	 * 
	 * @action getAvailableTypes
	 * @return KontorolUiConfTypeInfoArray
	 */
	function getAvailableTypesAction()
	{
		$flashPath = myContentStorage::getFSContentRootPath() . myContentStorage::getFSFlashRootPath();
		$flashPath = realpath($flashPath);
		$uiConf = new uiConf();
		$dirs = $uiConf->getDirectoryMap();
		$swfNames = $uiConf->getSwfNames();
		
		$typesInfoArray = new KontorolUiConfTypeInfoArray();
		foreach($dirs as $objType => $dir)
		{
			$typesInfo = new KontorolUiConfTypeInfo();
			$typesInfo->type = $objType;
			$typesInfo->directory = $dir;
			$typesInfo->filename = isset($swfNames[$objType]) ? $swfNames[$objType] : '';
			$versions = array();
			$path = $flashPath . '/' . $dir . '/';
			if(!file_exists($path) || !is_dir($path))
			{
				KontorolLog::err("Path [$path] does not exist");
				continue;
			}
				
			$path = realpath($path);
			$files = scandir($path);
			if(!$files)
			{
				KontorolLog::err("Could not scan directory [$path]");
				continue;
			}
				
			foreach($files as $file)
			{
				if (is_dir(realpath($path . '/' . $file)) && strpos($file, 'v') === 0)
					$versions[] = $file;
			}
			rsort($versions);
			
			$versionsObjectArray = new KontorolStringArray();
			foreach($versions as $version)
			{
				$versionString = new KontorolString();
				$versionString->value = $version;
				$versionsObjectArray[] = $versionString;
			}
		
			$typesInfo->versions = $versionsObjectArray;
			$typesInfoArray[] = $typesInfo;
		}
		return $typesInfoArray;
	}
}

<?php
/**
 * Manage partner users on Kontorol's side
 * The userId in kontorol is the unique ID in the partner's system, and the [partnerId,Id] couple are unique key in kontorol's DB
 *
 * @service user
 * @package api
 * @subpackage services
 */
class UserService extends KontorolBaseUserService
{

	/**
	 * Adds a new user to an existing account in the Kontorol database.
	 * Input param $id is the unique identifier in the partner's system.
	 *
	 * @action add
	 * @param KontorolUser $user The new user
	 * @return KontorolUser The new user
	 *
	 * @throws KontorolErrors::DUPLICATE_USER_BY_ID
	 * @throws KontorolErrors::PROPERTY_VALIDATION_CANNOT_BE_NULL
	 * @throws KontorolErrors::INVALID_FIELD_VALUE
	 * @throws KontorolErrors::UNKNOWN_PARTNER_ID
	 * @throws KontorolErrors::ADMIN_LOGIN_USERS_QUOTA_EXCEEDED
	 * @throws KontorolErrors::PASSWORD_STRUCTURE_INVALID
	 * @throws KontorolErrors::DUPLICATE_USER_BY_LOGIN_ID
	 * @throws KontorolErrors::USER_ROLE_NOT_FOUND
	 */
	function addAction(KontorolUser $user)
	{
		if (!preg_match(kuser::PUSER_ID_REGEXP, $user->id))
		{
			throw new KontorolAPIException(KontorolErrors::INVALID_FIELD_VALUE, 'id');
		}

		if ($user instanceof KontorolAdminUser)
		{
			$user->isAdmin = true;
		}

		$lockKey = "user_add_" . $this->getPartnerId() . $user->id;
		return kLock::runLocked($lockKey, array($this, 'adduserImpl'), array($user));
	}

	/**
	 * Updates an existing user object.
	 * You can also use this action to update the userId.
	 * 
	 * @action update
	 * @param string $userId The user's unique identifier in the partner's system
	 * @param KontorolUser $user The user parameters to update
	 * @return KontorolUser The updated user object
	 *
	 * @throws KontorolErrors::INVALID_USER_ID
	 * @throws KontorolErrors::CANNOT_DELETE_OR_BLOCK_ROOT_ADMIN_USER
	 * @throws KontorolErrors::USER_ROLE_NOT_FOUND
	 * @throws KontorolErrors::ACCOUNT_OWNER_NEEDS_PARTNER_ADMIN_ROLE
	 */
	public function updateAction($userId, KontorolUser $user)
	{		
		$dbUser = kuserPeer::getKuserByPartnerAndUid($this->getPartnerId(), $userId);
		
		if (!$dbUser)
			throw new KontorolAPIException(KontorolErrors::INVALID_USER_ID, $userId);

		if ($dbUser->getIsAdmin() && !is_null($user->isAdmin) && !$user->isAdmin) {
			throw new KontorolAPIException(KontorolErrors::CANNOT_SET_ROOT_ADMIN_AS_NO_ADMIN);
		}

		// update user
		try
		{
			if (!is_null($user->roleIds)) {
				if ($this->getPartnerId() == Partner::ADMIN_CONSOLE_PARTNER_ID && !kPermissionManager::isPermitted(PermissionName::SYSTEM_ADMIN_PERMISSIONS_UPDATE))
				{
					throw new KontorolAPIException(KontorolErrors::NOT_ALLOWED_TO_CHANGE_ROLE);
				}
				UserRolePeer::testValidRolesForUser($user->roleIds, $this->getPartnerId());
				if ($user->roleIds != $dbUser->getRoleIds() &&
					$dbUser->getId() == $this->getKuser()->getId()) {
					throw new KontorolAPIException(KontorolErrors::CANNOT_CHANGE_OWN_ROLE);
				}
			}
			if (!is_null($user->id) && $user->id != $userId) {
				if(!preg_match(kuser::PUSER_ID_REGEXP, $user->id)) {
					throw new KontorolAPIException(KontorolErrors::INVALID_FIELD_VALUE, 'id');
				} 
				
				$existingUser = kuserPeer::getKuserByPartnerAndUid($this->getPartnerId(), $user->id);
				if ($existingUser) {
					throw new KontorolAPIException(KontorolErrors::DUPLICATE_USER_BY_ID, $user->id);
				}
			}			
			$dbUser = $user->toUpdatableObject($dbUser);
			$dbUser->save();
		}
		catch (kPermissionException $e)
		{
			$code = $e->getCode();
			if ($code == kPermissionException::ROLE_ID_MISSING) {
				throw new KontorolAPIException(KontorolErrors::ROLE_ID_MISSING);
			}
			if ($code == kPermissionException::ONLY_ONE_ROLE_PER_USER_ALLOWED) {
				throw new KontorolAPIException(KontorolErrors::ONLY_ONE_ROLE_PER_USER_ALLOWED);
			}
			if ($code == kPermissionException::USER_ROLE_NOT_FOUND) {
				throw new KontorolAPIException(KontorolErrors::USER_ROLE_NOT_FOUND);
			}
			if ($code == kPermissionException::ACCOUNT_OWNER_NEEDS_PARTNER_ADMIN_ROLE) {
				throw new KontorolAPIException(KontorolErrors::ACCOUNT_OWNER_NEEDS_PARTNER_ADMIN_ROLE);
			}
			throw $e;
		}
		catch (kUserException $e) {
			$code = $e->getCode();
			if ($code == kUserException::CANNOT_DELETE_OR_BLOCK_ROOT_ADMIN_USER) {
				throw new KontorolAPIException(KontorolErrors::CANNOT_DELETE_OR_BLOCK_ROOT_ADMIN_USER);
			}
			throw $e;			
		}
				
		$user = new KontorolUser();
		$user->fromObject($dbUser, $this->getResponseProfile());
		
		return $user;
	}

	
	/**
	 * Retrieves a user object for a specified user ID.
	 * 
	 * @action get
	 * @param string $userId The user's unique identifier in the partner's system
	 * @return KontorolUser The specified user object
	 *
	 * @throws KontorolErrors::INVALID_USER_ID
	 */		
	public function getAction($userId = null)
	{
	    if (is_null($userId) || $userId == '')
	    {
            $userId = kCurrentContext::$ks_uid;	        
	    }

		if (!kCurrentContext::$is_admin_session && kCurrentContext::$ks_uid != $userId)
			throw new KontorolAPIException(KontorolErrors::CANNOT_RETRIEVE_ANOTHER_USER_USING_NON_ADMIN_SESSION, $userId);

		$dbUser = kuserPeer::getKuserByPartnerAndUid($this->getPartnerId(), $userId);
	
		if (!$dbUser)
			throw new KontorolAPIException(KontorolErrors::INVALID_USER_ID, $userId);

		$user = new KontorolUser();
		$user->fromObject($dbUser, $this->getResponseProfile());
		
		return $user;
	}
	
	/**
	 * Retrieves a user object for a user's login ID and partner ID.
	 * A login ID is the email address used by a user to log into the system.
	 * 
	 * @action getByLoginId
	 * @param string $loginId The user's email address that identifies the user for login
	 * @return KontorolUser The user object represented by the login and partner IDs
	 * 
	 * @throws KontorolErrors::LOGIN_DATA_NOT_FOUND
	 * @throws KontorolErrors::USER_NOT_FOUND
	 */
	public function getByLoginIdAction($loginId)
	{
		$loginData = UserLoginDataPeer::getByEmail($loginId);
		if (!$loginData) {
			throw new KontorolAPIException(KontorolErrors::LOGIN_DATA_NOT_FOUND);
		}
		
		$kuser = kuserPeer::getByLoginDataAndPartner($loginData->getId(), $this->getPartnerId());
		if (!$kuser) {
			throw new KontorolAPIException(KontorolErrors::USER_NOT_FOUND);
		}

		// users that are not publisher administrator are only allowed to get their own object   
		if ($kuser->getId() != kCurrentContext::getCurrentKsKuserId() && !in_array(PermissionName::MANAGE_ADMIN_USERS, kPermissionManager::getCurrentPermissions()))
			throw new KontorolAPIException(KontorolErrors::INVALID_USER_ID, $loginId);
		
		$user = new KontorolUser();
		$user->fromObject($kuser, $this->getResponseProfile());
		
		return $user;
	}

	/**
	 * Deletes a user from a partner account.
	 * 
	 * @action delete
	 * @param string $userId The user's unique identifier in the partner's system
	 * @return KontorolUser The deleted user object
	 *
	 * @throws KontorolErrors::INVALID_USER_ID
	 */		
	public function deleteAction($userId)
	{
		$dbUser = kuserPeer::getKuserByPartnerAndUid($this->getPartnerId(), $userId);
	
		if (!$dbUser) {
			throw new KontorolAPIException(KontorolErrors::INVALID_USER_ID, $userId);
		}
					
		try {
			$dbUser->setStatus(KontorolUserStatus::DELETED);
		}
		catch (kUserException $e) {
			$code = $e->getCode();
			if ($code == kUserException::CANNOT_DELETE_OR_BLOCK_ROOT_ADMIN_USER) {
				throw new KontorolAPIException(KontorolErrors::CANNOT_DELETE_OR_BLOCK_ROOT_ADMIN_USER);
			}
			throw $e;			
		}
		$dbUser->save();
		
		$user = new KontorolUser();
		$user->fromObject($dbUser, $this->getResponseProfile());
		
		return $user;
	}
	
	/**
	 * Lists user objects that are associated with an account.
	 * Blocked users are listed unless you use a filter to exclude them.
	 * Deleted users are not listed unless you use a filter to include them.
	 * 
	 * @action list
	 * @param KontorolUserFilter $filter A filter used to exclude specific types of users
	 * @param KontorolFilterPager $pager A limit for the number of records to display on a page
	 * @return KontorolUserListResponse The list of user objects
	 */
	public function listAction(KontorolUserFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
			$filter = new KontorolUserFilter();
			
		if(!$pager)
			$pager = new KontorolFilterPager();
			
		return $filter->getListResponse($pager, $this->getResponseProfile());
	}
	
	/**
	 * Notifies that a user is banned from an account.
	 * 
	 * @action notifyBan
	 * @param string $userId The user's unique identifier in the partner's system
	 *
	 * @throws KontorolErrors::INVALID_USER_ID
	 */		
	public function notifyBan($userId)
	{
		$dbUser = kuserPeer::getKuserByPartnerAndUid($this->getPartnerId(), $userId);
		if (!$dbUser)
			throw new KontorolAPIException(KontorolErrors::INVALID_USER_ID, $userId);
		
		myNotificationMgr::createNotification(kNotificationJobData::NOTIFICATION_TYPE_USER_BANNED, $dbUser);
	}

	/**
	 * Logs a user into a partner account with a partner ID, a partner user ID (puser), and a user password.
	 * 
	 * @action login
	 * @param int $partnerId The identifier of the partner account
	 * @param string $userId The user's unique identifier in the partner's system
	 * @param string $password The user's password
	 * @param int $expiry The requested time (in seconds) before the generated KS expires (By default, a KS expires after 24 hours).
	 * @param string $privileges Special privileges
	 * @return string A session KS for the user
	 * @ksIgnored
	 *
	 * @throws KontorolErrors::USER_NOT_FOUND
	 * @throws KontorolErrors::USER_WRONG_PASSWORD
	 * @throws KontorolErrors::INVALID_PARTNER_ID
	 * @throws KontorolErrors::LOGIN_RETRIES_EXCEEDED
	 * @throws KontorolErrors::LOGIN_BLOCKED
	 * @throws KontorolErrors::PASSWORD_EXPIRED
	 * @throws KontorolErrors::USER_IS_BLOCKED
	 */		
	public function loginAction($partnerId, $userId, $password, $expiry = 86400, $privileges = '*')
	{
		// exceptions might be thrown
		return parent::loginImpl($userId, null, $password, $partnerId, $expiry, $privileges);
	}
	
	/**
	 * Logs a user into a partner account with a user login ID and a user password.
	 * 
	 * @action loginByLoginId
	 * 
	 * @param string $loginId The user's email address that identifies the user for login
	 * @param string $password The user's password
	 * @param int $partnerId The identifier of the partner account
	 * @param int $expiry The requested time (in seconds) before the generated KS expires (By default, a KS expires after 24 hours).
	 * @param string $privileges Special privileges
	 * @param string $otp the user's one-time password
	 * @return string A session KS for the user
	 * @ksIgnored
	 *
	 * @throws KontorolErrors::USER_NOT_FOUND
	 * @throws KontorolErrors::USER_WRONG_PASSWORD
	 * @throws KontorolErrors::INVALID_PARTNER_ID
	 * @throws KontorolErrors::LOGIN_RETRIES_EXCEEDED
	 * @throws KontorolErrors::LOGIN_BLOCKED
	 * @throws KontorolErrors::PASSWORD_EXPIRED
	 * @throws KontorolErrors::USER_IS_BLOCKED
	 * @throws KontorolErrors::DIRECT_LOGIN_BLOCKED
	 */		
	public function loginByLoginIdAction($loginId, $password, $partnerId = null, $expiry = 86400, $privileges = '*', $otp = null)
	{
		// exceptions might be thrown
		return parent::loginImpl(null, $loginId, $password, $partnerId, $expiry, $privileges, $otp);
	}

	protected static function validateLoginDataParams($paramsArray)
	{
		kCurrentContext::$HTMLPurifierBehaviour = HTMLPurifierBehaviourType::BLOCK;
		foreach ($paramsArray as $paramName => $paramValue)
		{
			try
			{
				kHtmlPurifier::purify('kuser', $paramName, $paramValue);
			}
			catch (Exception $e)
			{
				throw new KontorolAPIException(KontorolErrors::UNSAFE_HTML_TAGS, 'UserLoginData', $paramName);
			}
		}
	}


	/**
	 * Updates a user's login data: email, password, name.
	 * 
	 * @action updateLoginData
	 * 
	 * @param string $oldLoginId The user's current email address that identified the user for login
	 * @param string $password The user's current email address that identified the user for login
	 * @param string $newLoginId Optional, The user's email address that will identify the user for login
	 * @param string $newPassword Optional, The user's new password
	 * @param string $newFirstName Optional, The user's new first name
	 * @param string $newLastName Optional, The user's new last name
	 * @param string $otp the user's one-time password
	 * @ksIgnored
	 *
	 * @throws KontorolErrors::INVALID_FIELD_VALUE
	 * @throws KontorolErrors::PASSWORD_STRUCTURE_INVALID
	 * @throws KontorolErrors::PASSWORD_ALREADY_USED
	 * @throws KontorolErrors::LOGIN_ID_ALREADY_USED
	 * @throws KontorolErrors::ADMIN_KUSER_NOT_FOUND
	 * @throws APIErrors::LOGIN_RETRIES_EXCEEDED
	 * @throws APIErrors::LOGIN_BLOCKED
	 */
	public function updateLoginDataAction( $oldLoginId , $password , $newLoginId = "" , $newPassword = "", $newFirstName = null, $newLastName = null, $otp = null)
	{
		self::validateLoginDataParams(array('id' => $newLoginId,
										'firstName' => $newFirstName,
										'lastName' => $newLastName));

		try
		{
			$updateLoginData = parent::updateLoginDataImpl($oldLoginId , $password , $newLoginId, $newPassword, $newFirstName, $newLastName, $otp);
		}
		catch(KontorolAPIException $e)
		{
			$error = $e->getCode().';;'.$e->getMessage();
			if ($error == KontorolErrors::LOGIN_DATA_NOT_FOUND ||
				$error == KontorolErrors::USER_WRONG_PASSWORD ||
				$error == KontorolErrors::WRONG_OLD_PASSWORD ||
				$error == KontorolErrors::INVALID_OTP ||
				$error == KontorolErrors::MISSING_OTP)
			{
				throw new KontorolAPIException(KontorolErrors::ADMIN_KUSER_NOT_FOUND);
			}
			throw $e;
		}
		return $updateLoginData;
	}
	
	/**
	 * Reset user's password and send the user an email to generate a new one.
	 * 
	 * @action resetPassword
	 * 
	 * @param string $email The user's email address (login email)
	 * @param KontorolResetPassLinkType $linkType kmc or kms
	 * @ksIgnored
	 *
	 * @throws KontorolErrors::LOGIN_DATA_NOT_FOUND
	 * @throws KontorolErrors::PASSWORD_STRUCTURE_INVALID
	 * @throws KontorolErrors::PASSWORD_ALREADY_USED
	 * @throws KontorolErrors::INVALID_FIELD_VALUE
	 * @throws KontorolErrors::LOGIN_ID_ALREADY_USED
	 */	
	public function resetPasswordAction($email, $linkType = KontorolResetPassLinkType::KMC)
	{
		return parent::resetPasswordImpl($email, $linkType);
	}
	
	/**
	 * Set initial user password
	 * 
	 * @action setInitialPassword
	 * 
	 * @param string $hashKey The hash key used to identify the user (retrieved by email)
	 * @param string $newPassword The new password to set for the user
	 * @return KontorolAuthentication The authentication response
	 * @ksIgnored
	 *
	 * @throws KontorolErrors::LOGIN_DATA_NOT_FOUND
	 * @throws KontorolErrors::PASSWORD_STRUCTURE_INVALID
	 * @throws KontorolErrors::NEW_PASSWORD_HASH_KEY_EXPIRED
	 * @throws KontorolErrors::NEW_PASSWORD_HASH_KEY_INVALID
	 * @throws KontorolErrors::PASSWORD_ALREADY_USED
	 * @throws KontorolErrors::INTERNAL_SERVERL_ERROR
	 */	
	public function setInitialPasswordAction($hashKey, $newPassword)
	{
		return parent::setInitialPasswordImpl($hashKey, $newPassword);
	}

	/**
	 * Validate hash key
	 *
	 * @action validateHashKey
	 *
	 * @param string $hashKey The hash key used to identify the user (retrieved by email)
	 * @return KontorolAuthentication The authentication response
	 *
	 * @throws KontorolErrors::LOGIN_DATA_NOT_FOUND
	 * @throws KontorolErrors::NEW_PASSWORD_HASH_KEY_INVALID
	 * @throws KontorolErrors::NEW_PASSWORD_HASH_KEY_EXPIRED
	 * @throws KontorolErrors::INVALID_ACCESS_TO_PARTNER_SPECIFIC_SEARCH
	 * @throws KontorolErrors::INTERNAL_SERVERL_ERROR
	 */
	public function validateHashKeyAction($hashKey)
	{
		KontorolResponseCacher::disableCache();

		try
		{
			$loginData = UserLoginDataPeer::isHashKeyValid($hashKey);
		}
		catch (kUserException $e)
		{
			switch($e->getCode())
			{
				case kUserException::LOGIN_DATA_NOT_FOUND:
					throw new KontorolAPIException(KontorolErrors::LOGIN_DATA_NOT_FOUND);

				case kUserException::NEW_PASSWORD_HASH_KEY_INVALID:
					throw new KontorolAPIException(KontorolErrors::NEW_PASSWORD_HASH_KEY_INVALID);

				case kUserException::NEW_PASSWORD_HASH_KEY_EXPIRED:
					throw new KontorolAPIException(KontorolErrors::NEW_PASSWORD_HASH_KEY_EXPIRED);

				default:
					throw $e;
			}
		}
		
		if ($this->getKs() && $this->getKs()->partner_id != $loginData->getConfigPartnerId())
		{
			throw new KontorolAPIException(KontorolErrors::INVALID_ACCESS_TO_PARTNER_SPECIFIC_SEARCH, $this->getKs()->partner_id);
		}

		return new KontorolAuthentication();
	}

	/**
	 * Enables a user to log into a partner account using an email address and a password
	 * 
	 * @action enableLogin
	 * 
	 * @param string $userId The user's unique identifier in the partner's system
	 * @param string $loginId The user's email address that identifies the user for login
	 * @param string $password The user's password
	 * @return KontorolUser The user object represented by the user and login IDs
	 * 
	 * @throws KontorolErrors::USER_LOGIN_ALREADY_ENABLED
	 * @throws KontorolErrors::USER_NOT_FOUND
	 * @throws KontorolErrors::ADMIN_LOGIN_USERS_QUOTA_EXCEEDED
	 * @throws KontorolErrors::PASSWORD_STRUCTURE_INVALID
	 * @throws KontorolErrors::LOGIN_ID_ALREADY_USED
	 *
	 */	
	public function enableLoginAction($userId, $loginId, $password = null)
	{		
		try
		{
			$user = kuserPeer::getKuserByPartnerAndUid($this->getPartnerId(), $userId);
			
			if (!$user)
			{
				throw new KontorolAPIException(KontorolErrors::USER_NOT_FOUND);
			}
			
			if (!$user->getIsAdmin() && !$password) {
				throw new KontorolAPIException(KontorolErrors::PROPERTY_VALIDATION_CANNOT_BE_NULL, 'password');
			}
			
			// Gonen 2011-05-29 : NOTE - 3rd party uses this action and expect that email notification will not be sent by default
			// if this call ever changes make sure you do not change default so mails are sent.
			$user->enableLogin($loginId, $password, true);	
			$user->save();
		}
		catch (Exception $e)
		{
			$code = $e->getCode();
			if ($code == kUserException::USER_LOGIN_ALREADY_ENABLED) {
				throw new KontorolAPIException(KontorolErrors::USER_LOGIN_ALREADY_ENABLED);
			}
			if ($code == kUserException::INVALID_EMAIL) {
				throw new KontorolAPIException(KontorolErrors::USER_NOT_FOUND);
			}
			else if ($code == kUserException::INVALID_PARTNER) {
				throw new KontorolAPIException(KontorolErrors::USER_NOT_FOUND);
			}
			else if ($code == kUserException::ADMIN_LOGIN_USERS_QUOTA_EXCEEDED) {
				throw new KontorolAPIException(KontorolErrors::ADMIN_LOGIN_USERS_QUOTA_EXCEEDED);
			}
			else if ($code == kUserException::PASSWORD_STRUCTURE_INVALID) {
				throw new KontorolAPIException(KontorolErrors::PASSWORD_STRUCTURE_INVALID);
			}
			else if ($code == kUserException::LOGIN_ID_ALREADY_USED) {
				throw new KontorolAPIException(KontorolErrors::LOGIN_ID_ALREADY_USED);
			}
			else if ($code == kUserException::ADMIN_LOGIN_USERS_QUOTA_EXCEEDED) {
				throw new KontorolAPIException(KontorolErrors::ADMIN_LOGIN_USERS_QUOTA_EXCEEDED);
			}
			throw $e;
		}
		
		$apiUser = new KontorolUser();
		$apiUser->fromObject($user, $this->getResponseProfile());
		return $apiUser;
	}
	
	
	
	/**
	 * Disables a user's ability to log into a partner account using an email address and a password.
	 * You may use either a userId or a loginId parameter for this action.
	 * 
	 * @action disableLogin
	 * 
	 * @param string $userId The user's unique identifier in the partner's system
	 * @param string $loginId The user's email address that identifies the user for login
	 * 
	 * @return KontorolUser The user object represented by the user and login IDs
	 * 
	 * @throws KontorolErrors::USER_LOGIN_ALREADY_DISABLED
	 * @throws KontorolErrors::PROPERTY_VALIDATION_CANNOT_BE_NULL
	 * @throws KontorolErrors::USER_NOT_FOUND
	 * @throws KontorolErrors::CANNOT_DISABLE_LOGIN_FOR_ADMIN_USER
	 *
	 */	
	public function disableLoginAction($userId = null, $loginId = null)
	{
		if (!$loginId && !$userId)
		{
			throw new KontorolAPIException(KontorolErrors::PROPERTY_VALIDATION_CANNOT_BE_NULL, 'userId');
		}
		
		$user = null;
		try
		{
			if ($loginId)
			{
				$loginData = UserLoginDataPeer::getByEmail($loginId);
				if (!$loginData) {
					throw new KontorolAPIException(KontorolErrors::USER_NOT_FOUND);
				}
				$user = kuserPeer::getByLoginDataAndPartner($loginData->getId(), $this->getPartnerId());
			}
			else
			{
				$user = kuserPeer::getKuserByPartnerAndUid($this->getPArtnerId(), $userId);
			}
			
			if (!$user)
			{
				throw new KontorolAPIException(KontorolErrors::USER_NOT_FOUND);
			}
			
			$user->disableLogin();
		}
		catch (Exception $e)
		{
			$code = $e->getCode();
			if ($code == kUserException::USER_LOGIN_ALREADY_DISABLED) {
				throw new KontorolAPIException(KontorolErrors::USER_LOGIN_ALREADY_DISABLED);
			}
			if ($code == kUserException::CANNOT_DISABLE_LOGIN_FOR_ADMIN_USER) {
				throw new KontorolAPIException(KontorolErrors::CANNOT_DISABLE_LOGIN_FOR_ADMIN_USER);
			}
			throw $e;
		}
		
		$apiUser = new KontorolUser();
		$apiUser->fromObject($user, $this->getResponseProfile());
		return $apiUser;
	}
	
	/**
	 * Index an entry by id.
	 * 
	 * @action index
	 * @param string $id
	 * @param bool $shouldUpdate
	 * @return string 
	 * @throws KontorolErrors::USER_NOT_FOUND
	 */
	function indexAction($id, $shouldUpdate = true)
	{
		$kuser = kuserPeer::getActiveKuserByPartnerAndUid(kCurrentContext::getCurrentPartnerId(), $id);
		
		if (!$kuser)
			throw new KontorolAPIException(KontorolErrors::USER_NOT_FOUND);
		
		$kuser->indexToSearchIndex();
			
		return $kuser->getPuserId();
	}
	
	/**
	 * Logs a user to the destination account provided the KS' user ID is associated with the destination account and the loginData ID matches
	 *
	 * @action loginByKs
	 * @param int $requestedPartnerId
	 * @throws APIErrors::PARTNER_CHANGE_ACCOUNT_DISABLED
	 *
	 * @return KontorolSessionResponse The generated session information
	 * 
	 * @throws KontorolErrors::INVALID_USER_ID
	 * @throws KontorolErrors::PARTNER_CHANGE_ACCOUNT_DISABLED
	 * @throws KontorolErrors::ADMIN_KUSER_NOT_FOUND
	 * @throws KontorolErrors::LOGIN_DATA_NOT_FOUND
	 * @throws KontorolErrors::LOGIN_BLOCKED
	 * @throws KontorolErrors::USER_IS_BLOCKED
	 * @throws KontorolErrors::INTERNAL_SERVERL_ERROR
	 * @throws KontorolErrors::UNKNOWN_PARTNER_ID
	 * @throws KontorolErrors::SERVICE_ACCESS_CONTROL_RESTRICTED
	 * @throws KontorolErrors::DIRECT_LOGIN_BLOCKED
	 * 
	 */
	public function loginByKsAction($requestedPartnerId)
	{
		$this->partnerGroup .= ",$requestedPartnerId";
		$this->applyPartnerFilterForClass('kuser');
		
		$ks = parent::loginByKsImpl($this->getKs()->getOriginalString(), $requestedPartnerId);
		
		$res = new KontorolSessionResponse();
		$res->ks = $ks;
		$res->userId = $this->getKuser()->getPuserId();
		$res->partnerId = $requestedPartnerId;
		
		return $res;
	}
	/**
	 *
	 * Will serve a requested CSV
	 * @action serveCsv
	 * @deprecated use exportCsv.serveCsv
	 *
	 * @param string $id - the requested file id
	 * @return string
	 */
	public function serveCsvAction($id)
	{
		$file_path = ExportCsvService::generateCsvPath($id, $this->getKs());
		return $this->dumpFile($file_path, 'text/csv');
	}

	/**
	 * get QR image content
	 *
	 * @action generateQrCode
	 * @param string $hashKey
	 * @return string
	 * @throws KontorolErrors::INVALID_HASH
	 * @throws KontorolErrors::INVALID_USER_ID
	 * @throws KontorolErrors::ERROR_IN_QR_GENERATION
	 *
	 */
	public function generateQrCodeAction($hashKey)
	{
		try
		{
			$loginData = UserLoginDataPeer::isHashKeyValid($hashKey);
			if ($loginData)
			{
				$this->validateApiAccessControl($loginData->getLastLoginPartnerId());
			}
			$dbUser = kuserPeer::getAdminUser($loginData->getConfigPartnerId(), $loginData);
		}
		catch (kUserException $e)
		{
			throw new KontorolAPIException(KontorolErrors::INVALID_HASH);
		}

		if (!$dbUser)
		{
			throw new KontorolAPIException(KontorolErrors::INVALID_USER_ID, $loginData->getLoginEmail());
		}
		$imgContent = authenticationUtils::getQRImage($dbUser, $loginData);
		if(!$imgContent)
		{
			throw new KontorolAPIException(KontorolErrors::ERROR_IN_QR_GENERATION);
		}

		$loginData->setPasswordHashKey(null);
		$loginData->save();

		return $imgContent;
	}

}

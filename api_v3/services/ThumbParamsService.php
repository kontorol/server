<?php

/**
 * Add & Manage Thumb Params
 *
 * @service thumbParams
 * @package api
 * @subpackage services
 */
class ThumbParamsService extends KontorolBaseService
{
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		
		$this->applyPartnerFilterForClass('conversionProfile2');
		$this->applyPartnerFilterForClass('asset');
		$this->applyPartnerFilterForClass('assetParamsOutput');
		$this->applyPartnerFilterForClass('assetParams');
	}
	
	/* (non-PHPdoc)
	 * @see KontorolBaseService::partnerGroup()
	 */
	protected function partnerGroup($peer = null)
	{
		if(
			$this->actionName == 'get' ||
			$this->actionName == 'list'
			)
			return $this->partnerGroup . ',0';
			
		return $this->partnerGroup;
	}
	
	protected function globalPartnerAllowed($actionName)
	{
		if ($actionName === 'get') {
			return true;
		}
		if ($actionName === 'list') {
			return true;
		}
		return parent::globalPartnerAllowed($actionName);
	}
	
	/**
	 * Add new Thumb Params
	 * 
	 * @action add
	 * @param KontorolThumbParams $thumbParams
	 * @return KontorolThumbParams
	 */
	public function addAction(KontorolThumbParams $thumbParams)
	{	
		$thumbParamsDb = new thumbParams();
		$thumbParams->toInsertableObject($thumbParamsDb);
		
		$thumbParamsDb->setPartnerId($this->getPartnerId());
		$thumbParamsDb->save();
		
		$thumbParams->fromObject($thumbParamsDb, $this->getResponseProfile());
		return $thumbParams;
	}
	
	/**
	 * Get Thumb Params by ID
	 * 
	 * @action get
	 * @param int $id
	 * @return KontorolThumbParams
	 */
	public function getAction($id)
	{
		$thumbParamsDb = assetParamsPeer::retrieveByPK($id);
		
		if (!$thumbParamsDb)
			throw new KontorolAPIException(KontorolErrors::FLAVOR_PARAMS_ID_NOT_FOUND, $id);
			
		$thumbParams = KontorolFlavorParamsFactory::getFlavorParamsInstance($thumbParamsDb->getType());
		$thumbParams->fromObject($thumbParamsDb, $this->getResponseProfile());
		
		return $thumbParams;
	}
	
	/**
	 * Update Thumb Params by ID
	 * 
	 * @action update
	 * @param int $id
	 * @param KontorolThumbParams $thumbParams
	 * @return KontorolThumbParams
	 */
	public function updateAction($id, KontorolThumbParams $thumbParams)
	{
		$thumbParamsDb = assetParamsPeer::retrieveByPK($id);
		if (!$thumbParamsDb)
			throw new KontorolAPIException(KontorolErrors::FLAVOR_PARAMS_ID_NOT_FOUND, $id);
			
		$thumbParams->toUpdatableObject($thumbParamsDb);
		$thumbParamsDb->save();
			
		$thumbParams->fromObject($thumbParamsDb, $this->getResponseProfile());
		return $thumbParams;
	}
	
	/**
	 * Delete Thumb Params by ID
	 * 
	 * @action delete
	 * @param int $id
	 */
	public function deleteAction($id)
	{
		$thumbParamsDb = assetParamsPeer::retrieveByPK($id);
		if (!$thumbParamsDb)
			throw new KontorolAPIException(KontorolErrors::FLAVOR_PARAMS_ID_NOT_FOUND, $id);
			
		$thumbParamsDb->setDeletedAt(time());
		$thumbParamsDb->save();
	}
	
	/**
	 * List Thumb Params by filter with paging support (By default - all system default params will be listed too)
	 * 
	 * @action list
	 * @param KontorolThumbParamsFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return KontorolThumbParamsListResponse
	 */
	public function listAction(KontorolThumbParamsFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
			$filter = new KontorolThumbParamsFilter();
			
		if(!$pager)
		{
			$pager = new KontorolFilterPager();
		}

		$types = KontorolPluginManager::getExtendedTypes(assetParamsPeer::OM_CLASS, assetType::THUMBNAIL);
		return $filter->getTypeListResponse($pager, $this->getResponseProfile(), $types);
	}
	
	/**
	 * Get Thumb Params by Conversion Profile ID
	 * 
	 * @action getByConversionProfileId
	 * @param int $conversionProfileId
	 * @return KontorolThumbParamsArray
	 */
	public function getByConversionProfileIdAction($conversionProfileId)
	{
		$conversionProfileDb = conversionProfile2Peer::retrieveByPK($conversionProfileId);
		if (!$conversionProfileDb)
			throw new KontorolAPIException(KontorolErrors::CONVERSION_PROFILE_ID_NOT_FOUND, $conversionProfileId);
			
		$thumbParamsConversionProfilesDb = $conversionProfileDb->getflavorParamsConversionProfilesJoinflavorParams();
		$thumbParamsDb = array();
		foreach($thumbParamsConversionProfilesDb as $item)
		{
			/* @var $item flavorParamsConversionProfile */
			$thumbParamsDb[] = $item->getassetParams();
		}
		
		$thumbParams = KontorolThumbParamsArray::fromDbArray($thumbParamsDb, $this->getResponseProfile());
		
		return $thumbParams; 
	}
}

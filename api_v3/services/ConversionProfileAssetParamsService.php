<?php

/**
 * Manage the connection between Conversion Profiles and Asset Params
 *
 * @service conversionProfileAssetParams
 * @package api
 * @subpackage services
 */
class ConversionProfileAssetParamsService extends KontorolBaseService
{
	public function initService($serviceId, $serviceName, $actionName)
	{
		parent::initService($serviceId, $serviceName, $actionName);
		
		$this->applyPartnerFilterForClass('conversionProfile2');
		$this->applyPartnerFilterForClass('assetParams');
	}
	
	/* (non-PHPdoc)
	 * @see KontorolBaseService::partnerGroup()
	 */
	protected function partnerGroup($peer = null) 	
	{
		if($this->actionName == 'list' && $peer == 'assetParams')
			return $this->partnerGroup . ',0';
		if($this->actionName == 'update' && $peer == 'assetParams')	
			return $this->partnerGroup . ',0';
			
		return $this->partnerGroup;
	}
	
	/**
	 * Lists asset parmas of conversion profile by ID
	 * 
	 * @action list
	 * @param KontorolConversionProfileAssetParamsFilter $filter
	 * @param KontorolFilterPager $pager
	 * @return KontorolConversionProfileAssetParamsListResponse
	 */
	public function listAction(KontorolConversionProfileAssetParamsFilter $filter = null, KontorolFilterPager $pager = null)
	{
		if (!$filter)
			$filter = new KontorolConversionProfileAssetParamsFilter();
			
		if(!$pager)
			$pager = new KontorolFilterPager();
			
		return $filter->getListResponse($pager, $this->getResponseProfile());
	}
	
	/**
	 * Update asset parmas of conversion profile by ID
	 * 
	 * @action update
	 * @param int $conversionProfileId
	 * @param int $assetParamsId
	 * @param KontorolConversionProfileAssetParams $conversionProfileAssetParams
	 * @return KontorolConversionProfileAssetParams
	 */
	public function updateAction($conversionProfileId, $assetParamsId, KontorolConversionProfileAssetParams $conversionProfileAssetParams)
	{
		$conversionProfile = conversionProfile2Peer::retrieveByPK($conversionProfileId);
		if(!$conversionProfile)
			throw new KontorolAPIException(KontorolErrors::CONVERSION_PROFILE_ID_NOT_FOUND, $conversionProfileId);
			
		$flavorParamsConversionProfile = flavorParamsConversionProfilePeer::retrieveByFlavorParamsAndConversionProfile($assetParamsId, $conversionProfileId);
		if(!$flavorParamsConversionProfile)
			throw new KontorolAPIException(KontorolErrors::CONVERSION_PROFILE_ASSET_PARAMS_NOT_FOUND, $conversionProfileId, $assetParamsId);
			
		$conversionProfileAssetParams->toUpdatableObject($flavorParamsConversionProfile);
		$flavorParamsConversionProfile->save();
			
		$conversionProfileAssetParams->fromObject($flavorParamsConversionProfile, $this->getResponseProfile());
		return $conversionProfileAssetParams;
	}
}

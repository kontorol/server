<?php
/**
 * @package api
 * @subpackage objects.factory
 */
class KontorolFlavorParamsFactory
{
	static function getFlavorParamsOutputInstance($type)
	{
		switch ($type) 
		{
			case KontorolAssetType::FLAVOR:
				return new KontorolFlavorParamsOutput();
				
			case KontorolAssetType::THUMBNAIL:
				return new KontorolThumbParamsOutput();
				
			default:
				$obj = KontorolPluginManager::loadObject('KontorolFlavorParamsOutput', $type);
				if($obj)
					return $obj;
					
				return new KontorolFlavorParamsOutput();
		}
	}
	
	static function getFlavorParamsInstance($type)
	{
		switch ($type) 
		{
			case KontorolAssetType::FLAVOR:
				return new KontorolFlavorParams();
				
			case KontorolAssetType::THUMBNAIL:
				return new KontorolThumbParams();
				
			case KontorolAssetType::LIVE:
				return new KontorolLiveParams();
				
			default:
				$obj = KontorolPluginManager::loadObject('KontorolFlavorParams', $type);
				if($obj)
					return $obj;
					
				return new KontorolFlavorParams();
		}
	}
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolUserEntryListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolUserEntryArray
	 * @readonly
	 */
	public $objects;
}

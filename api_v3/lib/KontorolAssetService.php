<?php
/**
 * Retrieve information and invoke actions on all asset types
 * Used as base class for all asset services
 *
 * @package api
 * @subpackage services
 */
abstract class KontorolAssetService extends KontorolBaseService
{
	/**
	 * @var array of all entry types that extend from media
	 */
	
	protected function getEnabledMediaTypes()
	{
		$mediaTypes = KontorolPluginManager::getExtendedTypes(entryPeer::OM_CLASS, KontorolEntryType::MEDIA_CLIP);
		return $mediaTypes;
	}
		
	/* (non-PHPdoc)
	 * @see KontorolBaseService::setPartnerFilters()
	 */
	protected function setPartnerFilters($partnerId)
	{
		parent::setPartnerFilters($partnerId);
		
		$this->applyPartnerFilterForClass('conversionProfile2');
		$this->applyPartnerFilterForClass('assetParamsOutput');
		$fullActionName = "{$this->serviceName}.$this->actionName";
		if (!in_array($fullActionName, array('flavorAsset.getByEntryId', 'flavorAsset.getWebPlayableByEntryId', 'flavorAsset.list')))
			$this->applyPartnerFilterForClass('asset');
		$this->applyPartnerFilterForClass('assetParams');
	}
	
	/**
	 * Get asset by ID
	 * 
	 * @action get
	 * @param string $id
	 * @return KontorolAsset
	 */
	abstract public function getAction($id);

	/**
	 * @param asset $asset
	 * @param $fileName
	 * @param bool $forceProxy
	 * @param null $version
	 * @return kRendererDumpFile
	 * @throws KontorolAPIException
	 * @throws kCoreException
	 */
	protected function serveAsset(asset $asset, $fileName, $forceProxy = false, $version = null)
	{
		$syncKey = $asset->getSyncKey(asset::FILE_SYNC_ASSET_SUB_TYPE_ASSET, $version);
		try
		{
			list($fileSync, $serveRemote) = kFileSyncUtils::getFileSyncByStoragePriority($this->getPartnerId(), $syncKey);
		}
		catch (kCoreException $ex)
		{
			switch ($ex->getCode())
			{
				case kCoreException::FILE_NOT_FOUND:
					throw new KontorolAPIException(KontorolErrors::FILE_DOESNT_EXIST);
					break;
				default:
					throw $ex;
			}
		}
		
		if($serveRemote && $fileSync && !in_array($fileSync->getDc(), kDataCenterMgr::getSharedStorageProfileIds()))
		{
			header("Location: " . $fileSync->getExternalUrl($asset->getEntryId()));
			die;
		}
		
		return $this->serveFile($asset, asset::FILE_SYNC_ASSET_SUB_TYPE_ASSET, $fileName, $asset->getEntryId(), $forceProxy);
	}
	
	/**
	 * Action for manually exporting an asset
	 * @param string $assetId
	 * @param int $storageProfileId
	 * @throws KontorolErrors::INVALID_FLAVOR_ASSET_ID
	 * @throws KontorolErrors::STORAGE_PROFILE_ID_NOT_FOUND
	 * @throws KontorolErrors::INTERNAL_SERVERL_ERROR
	 * @return KontorolFlavorAsset The exported asset
	 */
	protected function exportAction ( $assetId , $storageProfileId )
	{	    
	    $dbAsset = assetPeer::retrieveById($assetId);
	    if (!$dbAsset)
	    {
	        throw new KontorolAPIException(KontorolErrors::INVALID_FLAVOR_ASSET_ID, $assetId);
	    }
		
	    $dbStorageProfile = StorageProfilePeer::retrieveByIdAndPartnerId($storageProfileId, $dbAsset->getPartnerId());
	    if (!$dbStorageProfile)
	    {
	        throw new KontorolAPIException(KontorolErrors::STORAGE_PROFILE_ID_NOT_FOUND, $storageProfileId);
	    }
	    
	   	$scope = $dbStorageProfile->getScope();
	    $scope->setEntryId($dbAsset->getEntryId());
	    if(!$dbStorageProfile->fulfillsRules($scope))
	    {
	    	throw new KontorolAPIException(KontorolErrors::STORAGE_PROFILE_RULES_NOT_FULFILLED, $storageProfileId);
	    }
	    
	    $exported = kStorageExporter::exportFlavorAsset($dbAsset, $dbStorageProfile);
	    
	    if ($exported !== true)
	    {
	        //TODO: implement export errors
	        throw new KontorolAPIException(KontorolErrors::INTERNAL_SERVERL_ERROR);
	    }
	    
	    return $this->getAction($assetId);
	}
	
	protected function validateEntryEntitlement($entryId, $assetId)
	{
		if(kEntitlementUtils::getEntitlementEnforcement())
		{
			$entry = entryPeer::retrieveByPK($entryId);
			if(!$entry)
			{
				//we will throw asset not found, as the user is not entitled, and should not know that the entry exists.
				throw new KontorolAPIException(KontorolErrors::ASSET_ID_NOT_FOUND, $assetId);
			}	
		}		
	}
}

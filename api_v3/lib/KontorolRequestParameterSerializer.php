<?php
class KontorolRequestParameterSerializer
{
	/**
	 * Flattens KontorolObject into an array of parameters that can sent over a GET request
	 * @param KontorolObject $object
	 * @param string $prefix
	 * @return array
	 */
	public static function serialize (KontorolObject $object, $prefix)
	{
		$params = array();
		if (!($object instanceof KontorolTypedArray))
			$params[] = "$prefix:objectType=".get_class($object);
		
		foreach ($object as $prop => $val)
		{
			if (is_null($val))
				continue;
			
			if (is_numeric($prop))
			{
				$prop = "item$prop";	
			}
			
			if (is_scalar($val))
			{
				$params[] = "$prefix:$prop=$val";
			}
			elseif ($val instanceof KontorolTypedArray)
			{
				$params = array_merge($params, self::serialize($val->toArray(),"$prefix:$prop"));				
			}
			else 
			{
				$params = array_merge($params, self::serialize($val,"$prefix:$prop"));
			}
		}
		
		return $params;
	}
}

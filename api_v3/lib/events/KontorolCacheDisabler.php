<?php
/**
 * Consumer to disable caching after an object is saved.
 *
 * @package api
 * @subpackage cache
 */
class KontorolCacheDisabler implements kObjectSavedEventConsumer
{
	public function objectSaved(BaseObject $object)
	{
		KontorolResponseCacher::disableCache();
	}
	
	public function shouldConsumeSavedEvent(BaseObject $object)
	{
		return KontorolResponseCacher::isCacheEnabled();
	}
}

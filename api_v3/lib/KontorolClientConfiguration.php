<?php

/**
 * Define client optional configurations
 */
class KontorolClientConfiguration extends KontorolObject
{
	/**
	 * @var string
	 */
	public $clientTag;
	
	/**
	 * @var string
	 */
	public $apiVersion;
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolLiveEntryServerNodeRecordingInfo extends KontorolObject
{
	
	/**
	 * @var string
	 */
	public $recordedEntryId;
	
	/**
	 * @var int
	 */
	public $duration;

	/**
	 * @var KontorolEntryServerNodeRecordingStatus
	 */
	public $recordingStatus;
	
	private static $mapBetweenObjects = array
	(
			"recordedEntryId",
			"duration",
			"recordingStatus",
	);
	
	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	*/
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$mapBetweenObjects);
	}
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject($object_to_fill, $props_to_skip)
	 */
	public function toObject($dbObject = null, $propsToSkip = array())
	{
		if (!$dbObject)
		{
			$dbObject = new LiveEntryServerNodeRecordingInfo();
		}
		if (is_null($this->recordingStatus))
			$this->recordingStatus = KontorolEntryServerNodeRecordingStatus::STOPPED;
	
		return parent::toObject($dbObject, $propsToSkip);
	}
}

<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolLiveEntryServerNodeFilter extends KontorolLiveEntryServerNodeBaseFilter
{
	public function __construct()
	{
		$this->serverTypeIn = array(KontorolEntryServerNodeType::LIVE_PRIMARY, KontorolEntryServerNodeType::LIVE_BACKUP);
	}
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolLiveStreamParamsArray extends KontorolTypedArray {
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolLiveStreamParamsArray();
		if ($arr == null)
			return $newArr;
	
		foreach ($arr as $obj)
		{
			$nObj = new KontorolLiveStreamParams();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
	
		return $newArr;
	}
	
	public function __construct()
	{
		return parent::__construct("KontorolLiveStreamParams");
	}
}

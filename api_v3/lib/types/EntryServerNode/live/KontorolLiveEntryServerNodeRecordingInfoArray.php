<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolLiveEntryServerNodeRecordingInfoArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolLiveEntryServerNodeRecordingInfoArray();
		if ($arr == null)
			return $newArr;
	
		foreach ($arr as $obj)
		{
			$nObj = new KontorolLiveEntryServerNodeRecordingInfo();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
	
		return $newArr;
	}
	
	public function __construct()
	{
		return parent::__construct("KontorolLiveEntryServerNodeRecordingInfo");
	}
}

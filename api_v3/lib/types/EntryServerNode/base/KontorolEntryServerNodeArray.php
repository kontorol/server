<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolEntryServerNodeArray extends KontorolTypedArray
{
	public static function fromDbArray($arr)
	{
		$newArr = new KontorolEntryServerNodeArray();
		foreach($arr as $obj)
		{
			/* @var $obj KontorolEntryServerNode */
			$nObj = KontorolEntryServerNode::getInstance($obj);
			if (!$nObj)
			{
				throw new KontorolAPIException(KontorolErrors::ENTRY_SERVER_NODE_OBJECT_TYPE_ERROR, $obj->getServerType(), $obj->getId());
			}
			$nObj->fromObject($obj);
			$newArr[] = $nObj;
		}

		return $newArr;
	}

	public function __construct()
	{
		return parent::__construct("KontorolEntryServerNode");
	}

}

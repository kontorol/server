<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolEntryServerNodeFilter extends KontorolEntryServerNodeBaseFilter
{
	/**
	 * @return baseObjectFilter
	 */
	protected function getCoreFilter()
	{
		return new EntryServerNodeFilter();
	}

	/**
	 * @param KontorolFilterPager $pager
	 * @param KontorolDetachedResponseProfile $responseProfile
	 * @return KontorolListResponse
	 * @throws KontorolAPIException
	 */
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		if($this->entryIdEqual)
		{
			$entry = entryPeer::retrieveByPK($this->entryIdEqual);
			if(!$entry)
				throw new KontorolAPIException(KontorolErrors::ENTRY_ID_NOT_FOUND, $this->entryIdEqual);
		} 
		else if ($this->entryIdIn)
		{
			$entryIds = explode(',', $this->entryIdIn);
			$entries = entryPeer::retrieveByPKs($entryIds);
			
			$validEntryIds = array();
			foreach ($entries as $entry)
				$validEntryIds[] = $entry->getId();
			
			if (!count($validEntryIds))
			{
				return array(array(), 0);
			}
			
			$entryIds = implode($validEntryIds, ',');
			$this->entryIdIn = $entryIds;
		}

		$c = new Criteria();
		$entryServerNodeFilter = $this->toObject();
		$entryServerNodeFilter->attachToCriteria($c);
		$pager->attachToCriteria($c);

		$dbEntryServerNodes = EntryServerNodePeer::doSelect($c);

		$entryServerNodeList = KontorolEntryServerNodeArray::fromDbArray($dbEntryServerNodes, $responseProfile);
		$response = new KontorolEntryServerNodeListResponse();
		$response->objects = $entryServerNodeList;
		$response->totalCount = count($dbEntryServerNodes);
		return $response;
	}
}

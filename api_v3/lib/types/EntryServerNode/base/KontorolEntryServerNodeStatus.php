<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolEntryServerNodeStatus extends KontorolEnum implements EntryServerNodeStatus{

	public static function getEnumClass()
	{
		return 'EntryServerNodeStatus';
	}
}

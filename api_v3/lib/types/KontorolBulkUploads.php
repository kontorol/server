<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolBulkUploads extends KontorolTypedArray
{
	public static function fromBatchJobArray ($arr)
	{
		$newArr = new KontorolBulkUploads();
		if ($arr == null)
			return $newArr;
					
		foreach ($arr as $obj)
		{
			$nObj = new KontorolBulkUpload();
			$nObj->fromObject($obj);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolBulkUpload");
	}
}

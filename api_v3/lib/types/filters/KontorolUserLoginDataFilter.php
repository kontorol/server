<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolUserLoginDataFilter extends KontorolUserLoginDataBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new UserLoginDataFilter();
	}

	/* (non-PHPdoc)
	 * @see KontorolRelatedFilter::getListResponse()
	 */
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{	
		$userLoginDataFilter = $this->toObject();
		
		$c = new Criteria();
		$userLoginDataFilter->attachToCriteria($c);
		
		$totalCount = UserLoginDataPeer::doCount($c);
		$pager->attachToCriteria($c);
		$list = UserLoginDataPeer::doSelect($c);
		$newList = KontorolUserLoginDataArray::fromDbArray($list, $responseProfile);
		
		$response = new KontorolUserLoginDataListResponse();
		$response->totalCount = $totalCount;
		$response->objects = $newList;
		return $response;
	}
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolMediaEntryFilterForPlaylistArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr = null, KontorolDetachedResponseProfile $responseProfile = null)
	{
		foreach ( $arr as $obj )
		{
			$nObj = new KontorolMediaEntryFilterForPlaylist();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct( )
	{
		return parent::__construct ( "KontorolMediaEntryFilterForPlaylist" );
	}
}

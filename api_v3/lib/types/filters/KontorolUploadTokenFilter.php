<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolUploadTokenFilter extends KontorolUploadTokenBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new UploadTokenFilter();
	}
}

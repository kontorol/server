<?php
/**
 * @package api
 * @subpackage filters
 */
abstract class KontorolRelatedFilter extends KontorolFilter
{
	/**
	 * @param KontorolFilterPager $pager
	 * @param KontorolDetachedResponseProfile $responseProfile
	 * @return KontorolListResponse
	 */
	abstract public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null);
	
	public function validateForResponseProfile()
	{
		
	}

	/**
	 * @param KontorolFilterPager $pager
	 * @param KontorolDetachedResponseProfile|null $responseProfile
	 * @return KontorolListResponse
	 * @throws Exception
	 */
	public function validateAndGetListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{

		if (ValidateAccessResponseProfile::validateAccess($this))
			return $this->getListResponse($pager,$responseProfile);
		return new KontorolListResponse();

	}

}

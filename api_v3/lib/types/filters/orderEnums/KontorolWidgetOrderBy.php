<?php
/**
 * @package api
 * @subpackage filters.enum
 */
class KontorolWidgetOrderBy extends KontorolStringEnum
{
	const CREATED_AT_ASC = "+createdAt";
	const CREATED_AT_DESC = "-createdAt";
}

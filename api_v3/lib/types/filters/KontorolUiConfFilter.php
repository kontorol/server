<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolUiConfFilter extends KontorolUiConfBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new uiConfFilter();
	}
}

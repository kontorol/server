<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolWidgetFilter extends KontorolWidgetBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new widgetFilter();
	}
}

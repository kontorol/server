<?php
/**
 * @package api
 * @subpackage filters.enum
 */
class KontorolSearchConditionComparison extends KontorolDynamicEnum implements searchConditionComparison
{
	public static function getEnumClass()
	{
		return 'searchConditionComparison';
	}
}

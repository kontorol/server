<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolSearchItemArray extends KontorolTypedArray
{
	/**
	 * @param array $arr
	 * @return KontorolSearchItemArray
	 */
	public static function fromDbArray(array $arr = null, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolSearchItemArray();
		if(!$arr || !count($arr))
			return $newArr;
			
		foreach ( $arr as $obj )
		{
			$kontorolClass = $obj->getKontorolClass();
			if(!class_exists($kontorolClass))
			{
				KontorolLog::err("Class [$kontorolClass] not found");
				continue;
			}
				
			$nObj = new $kontorolClass();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	/**
	 * @return array
	 */
	public function toObjectsArray()
	{
		$ret = array();
		foreach($this as $item)
		{
			$ret[] = $item->toObject();
		}
			
		return $ret;
	}
	
	public function __construct( )
	{
		return parent::__construct ( "KontorolSearchItem" );
	}
}
?>

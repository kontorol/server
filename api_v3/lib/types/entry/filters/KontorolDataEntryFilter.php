<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolDataEntryFilter extends KontorolDataEntryBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolBaseEntryFilter::getListResponse()
	 */
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		list($list, $totalCount) = $this->doGetListResponse($pager);
		
	    $newList = KontorolDataEntryArray::fromDbArray($list, $responseProfile);
		$response = new KontorolBaseEntryListResponse();
		$response->objects = $newList;
		$response->totalCount = $totalCount;
		
		return $response;
	}
}

<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolLiveChannelFilter extends KontorolLiveChannelBaseFilter
{
	public function __construct()
	{
		$this->typeIn = KontorolEntryType::LIVE_CHANNEL;
	}

	/* (non-PHPdoc)
	 * @see KontorolBaseEntryFilter::getListResponse()
	 */
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		list($list, $totalCount) = $this->doGetListResponse($pager);
		
	    $newList = KontorolLiveChannelArray::fromDbArray($list, $responseProfile);
		$response = new KontorolLiveChannelListResponse();
		$response->objects = $newList;
		$response->totalCount = $totalCount;
		
		return $response;
	}
}

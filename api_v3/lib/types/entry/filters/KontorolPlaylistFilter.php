<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolPlaylistFilter extends KontorolPlaylistBaseFilter
{
	/**
	 * @var KontorolPlaylistType
	 */
	public $playListTypeEqual;

	/**
	 * @var string
	 */
	public $playListTypeIn;

	static private $map_between_objects = array
	(
		"playListTypeEqual" => "_eq_media_type",
		"playListTypeIn" => "_in_media_type",
	);

	/* (non-PHPdoc)
	 * @see KontorolBaseEntryFilter::getListResponse()
	 */
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		list($list, $totalCount) = $this->doGetListResponse($pager);
		
	    $newList = KontorolPlaylistArray::fromDbArray($list, $responseProfile);
		$response = new KontorolPlaylistListResponse();
		$response->objects = $newList;
		$response->totalCount = $totalCount;
		
		return $response;
	}

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}
}

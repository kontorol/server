<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolLiveChannelSegmentFilter extends KontorolLiveChannelSegmentBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new LiveChannelSegmentFilter();
	}

	/* (non-PHPdoc)
	 * @see KontorolRelatedFilter::getListResponse()
	 */
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$liveChannelSegmentFilter = $this->toObject();

		$c = new Criteria();
		$liveChannelSegmentFilter->attachToCriteria($c);
		
		$totalCount = LiveChannelSegmentPeer::doCount($c);
		
		$pager->attachToCriteria($c);
		$dbList = LiveChannelSegmentPeer::doSelect($c);
		
		$list = KontorolLiveChannelSegmentArray::fromDbArray($dbList, $responseProfile);
		$response = new KontorolLiveChannelSegmentListResponse();
		$response->objects = $list;
		$response->totalCount = $totalCount;
		return $response;    
	}
}

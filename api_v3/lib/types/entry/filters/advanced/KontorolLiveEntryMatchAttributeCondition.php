<?php

/**
 * Auto-generated class.
 * 
 * Used to search KontorolLiveEntry attributes. Use KontorolLiveEntryMatchAttribute enum to provide attribute name.
*/
class KontorolLiveEntryMatchAttributeCondition extends KontorolSearchMatchAttributeCondition
{
	/**
	 * @var KontorolLiveEntryMatchAttribute
	 */
	public $attribute;

	private static $mapBetweenObjects = array
	(
		"attribute" => "attribute",
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects() , self::$mapBetweenObjects);
	}

	protected function getIndexClass()
	{
		return 'entryIndex';
	}
}


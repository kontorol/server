<?php

/**
 * Auto-generated class.
 * 
 * Used to search KontorolBaseEntry attributes. Use KontorolBaseEntryMatchAttribute enum to provide attribute name.
*/
class KontorolBaseEntryMatchAttributeCondition extends KontorolSearchMatchAttributeCondition
{
	/**
	 * @var KontorolBaseEntryMatchAttribute
	 */
	public $attribute;

	private static $mapBetweenObjects = array
	(
		"attribute" => "attribute",
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects() , self::$mapBetweenObjects);
	}

	protected function getIndexClass()
	{
		return 'entryIndex';
	}
}


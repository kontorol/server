<?php

/**
 * Auto-generated class.
 * 
 * Used to search KontorolPlayableEntry attributes. Use KontorolPlayableEntryMatchAttribute enum to provide attribute name.
*/
class KontorolPlayableEntryMatchAttributeCondition extends KontorolSearchMatchAttributeCondition
{
	/**
	 * @var KontorolPlayableEntryMatchAttribute
	 */
	public $attribute;

	private static $mapBetweenObjects = array
	(
		"attribute" => "attribute",
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects() , self::$mapBetweenObjects);
	}

	protected function getIndexClass()
	{
		return 'entryIndex';
	}
}


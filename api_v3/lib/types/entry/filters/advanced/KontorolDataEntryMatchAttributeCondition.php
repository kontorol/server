<?php

/**
 * Auto-generated class.
 * 
 * Used to search KontorolDataEntry attributes. Use KontorolDataEntryMatchAttribute enum to provide attribute name.
*/
class KontorolDataEntryMatchAttributeCondition extends KontorolSearchMatchAttributeCondition
{
	/**
	 * @var KontorolDataEntryMatchAttribute
	 */
	public $attribute;

	private static $mapBetweenObjects = array
	(
		"attribute" => "attribute",
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects() , self::$mapBetweenObjects);
	}

	protected function getIndexClass()
	{
		return 'entryIndex';
	}
}


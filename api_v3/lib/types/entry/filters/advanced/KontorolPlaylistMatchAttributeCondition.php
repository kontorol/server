<?php

/**
 * Auto-generated class.
 * 
 * Used to search KontorolPlaylist attributes. Use KontorolPlaylistMatchAttribute enum to provide attribute name.
*/
class KontorolPlaylistMatchAttributeCondition extends KontorolSearchMatchAttributeCondition
{
	/**
	 * @var KontorolPlaylistMatchAttribute
	 */
	public $attribute;

	private static $mapBetweenObjects = array
	(
		"attribute" => "attribute",
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects() , self::$mapBetweenObjects);
	}

	protected function getIndexClass()
	{
		return 'entryIndex';
	}
}


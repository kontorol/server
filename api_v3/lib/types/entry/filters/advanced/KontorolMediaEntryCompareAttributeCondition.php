<?php

/**
 * Auto-generated class.
 * 
 * Used to search KontorolMediaEntry attributes. Use KontorolMediaEntryCompareAttribute enum to provide attribute name.
*/
class KontorolMediaEntryCompareAttributeCondition extends KontorolSearchComparableAttributeCondition
{
	/**
	 * @var KontorolMediaEntryCompareAttribute
	 */
	public $attribute;

	private static $mapBetweenObjects = array
	(
		"attribute" => "attribute",
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects() , self::$mapBetweenObjects);
	}

	protected function getIndexClass()
	{
		return 'entryIndex';
	}
}


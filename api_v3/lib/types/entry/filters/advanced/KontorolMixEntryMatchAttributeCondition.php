<?php

/**
 * Auto-generated class.
 * 
 * Used to search KontorolMixEntry attributes. Use KontorolMixEntryMatchAttribute enum to provide attribute name.
*/
class KontorolMixEntryMatchAttributeCondition extends KontorolSearchMatchAttributeCondition
{
	/**
	 * @var KontorolMixEntryMatchAttribute
	 */
	public $attribute;

	private static $mapBetweenObjects = array
	(
		"attribute" => "attribute",
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects() , self::$mapBetweenObjects);
	}

	protected function getIndexClass()
	{
		return 'entryIndex';
	}
}


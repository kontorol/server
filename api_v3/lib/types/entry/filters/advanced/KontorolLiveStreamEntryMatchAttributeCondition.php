<?php

/**
 * Auto-generated class.
 * 
 * Used to search KontorolLiveStreamEntry attributes. Use KontorolLiveStreamEntryMatchAttribute enum to provide attribute name.
*/
class KontorolLiveStreamEntryMatchAttributeCondition extends KontorolSearchMatchAttributeCondition
{
	/**
	 * @var KontorolLiveStreamEntryMatchAttribute
	 */
	public $attribute;

	private static $mapBetweenObjects = array
	(
		"attribute" => "attribute",
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects() , self::$mapBetweenObjects);
	}

	protected function getIndexClass()
	{
		return 'entryIndex';
	}
}


<?php

/**
 * Auto-generated class.
 * 
 * Used to search KontorolLiveChannel attributes. Use KontorolLiveChannelMatchAttribute enum to provide attribute name.
*/
class KontorolLiveChannelMatchAttributeCondition extends KontorolSearchMatchAttributeCondition
{
	/**
	 * @var KontorolLiveChannelMatchAttribute
	 */
	public $attribute;

	private static $mapBetweenObjects = array
	(
		"attribute" => "attribute",
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects() , self::$mapBetweenObjects);
	}

	protected function getIndexClass()
	{
		return 'entryIndex';
	}
}


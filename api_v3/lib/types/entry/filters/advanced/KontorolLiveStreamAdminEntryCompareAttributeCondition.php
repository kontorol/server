<?php

/**
 * Auto-generated class.
 * 
 * Used to search KontorolLiveStreamAdminEntry attributes. Use KontorolLiveStreamAdminEntryCompareAttribute enum to provide attribute name.
*/
class KontorolLiveStreamAdminEntryCompareAttributeCondition extends KontorolSearchComparableAttributeCondition
{
	/**
	 * @var KontorolLiveStreamAdminEntryCompareAttribute
	 */
	public $attribute;

	private static $mapBetweenObjects = array
	(
		"attribute" => "attribute",
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects() , self::$mapBetweenObjects);
	}

	protected function getIndexClass()
	{
		return 'entryIndex';
	}
}


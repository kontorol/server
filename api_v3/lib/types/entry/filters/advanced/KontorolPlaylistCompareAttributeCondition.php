<?php

/**
 * Auto-generated class.
 * 
 * Used to search KontorolPlaylist attributes. Use KontorolPlaylistCompareAttribute enum to provide attribute name.
*/
class KontorolPlaylistCompareAttributeCondition extends KontorolSearchComparableAttributeCondition
{
	/**
	 * @var KontorolPlaylistCompareAttribute
	 */
	public $attribute;

	private static $mapBetweenObjects = array
	(
		"attribute" => "attribute",
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects() , self::$mapBetweenObjects);
	}

	protected function getIndexClass()
	{
		return 'entryIndex';
	}
}


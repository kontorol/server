<?php

/**
 * Auto-generated class.
 * 
 * Used to search KontorolPlayableEntry attributes. Use KontorolPlayableEntryCompareAttribute enum to provide attribute name.
*/
class KontorolPlayableEntryCompareAttributeCondition extends KontorolSearchComparableAttributeCondition
{
	/**
	 * @var KontorolPlayableEntryCompareAttribute
	 */
	public $attribute;

	private static $mapBetweenObjects = array
	(
		"attribute" => "attribute",
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects() , self::$mapBetweenObjects);
	}

	protected function getIndexClass()
	{
		return 'entryIndex';
	}
}


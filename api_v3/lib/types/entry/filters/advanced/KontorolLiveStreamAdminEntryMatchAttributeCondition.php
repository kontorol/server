<?php

/**
 * Auto-generated class.
 * 
 * Used to search KontorolLiveStreamAdminEntry attributes. Use KontorolLiveStreamAdminEntryMatchAttribute enum to provide attribute name.
*/
class KontorolLiveStreamAdminEntryMatchAttributeCondition extends KontorolSearchMatchAttributeCondition
{
	/**
	 * @var KontorolLiveStreamAdminEntryMatchAttribute
	 */
	public $attribute;

	private static $mapBetweenObjects = array
	(
		"attribute" => "attribute",
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects() , self::$mapBetweenObjects);
	}

	protected function getIndexClass()
	{
		return 'entryIndex';
	}
}


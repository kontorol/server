<?php

/**
 * Auto-generated class.
 * 
 * Used to search KontorolLiveStreamEntry attributes. Use KontorolLiveStreamEntryCompareAttribute enum to provide attribute name.
*/
class KontorolLiveStreamEntryCompareAttributeCondition extends KontorolSearchComparableAttributeCondition
{
	/**
	 * @var KontorolLiveStreamEntryCompareAttribute
	 */
	public $attribute;

	private static $mapBetweenObjects = array
	(
		"attribute" => "attribute",
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects() , self::$mapBetweenObjects);
	}

	protected function getIndexClass()
	{
		return 'entryIndex';
	}
}


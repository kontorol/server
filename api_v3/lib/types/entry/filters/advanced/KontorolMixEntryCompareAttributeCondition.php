<?php

/**
 * Auto-generated class.
 * 
 * Used to search KontorolMixEntry attributes. Use KontorolMixEntryCompareAttribute enum to provide attribute name.
*/
class KontorolMixEntryCompareAttributeCondition extends KontorolSearchComparableAttributeCondition
{
	/**
	 * @var KontorolMixEntryCompareAttribute
	 */
	public $attribute;

	private static $mapBetweenObjects = array
	(
		"attribute" => "attribute",
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects() , self::$mapBetweenObjects);
	}

	protected function getIndexClass()
	{
		return 'entryIndex';
	}
}


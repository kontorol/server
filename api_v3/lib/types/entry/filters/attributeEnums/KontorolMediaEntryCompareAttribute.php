<?php

/**
 * Auto-generated enum class
*/
class KontorolMediaEntryCompareAttribute extends KontorolPlayableEntryCompareAttribute
{
	const MEDIA_TYPE = "mediaType";
	const MEDIA_DATE = "mediaDate";
}


<?php

/**
 * Auto-generated enum class
*/
class KontorolPlayableEntryCompareAttribute extends KontorolBaseEntryCompareAttribute
{
	const VIEWS = "views";
	const MS_DURATION = "msDuration";
	const PLAYS = "plays";
	const LAST_PLAYED_AT = "lastPlayedAt";
}


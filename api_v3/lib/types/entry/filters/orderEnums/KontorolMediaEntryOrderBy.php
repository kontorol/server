<?php
/**
 * @package api
 * @subpackage filters.enum
 */
class KontorolMediaEntryOrderBy extends KontorolPlayableEntryOrderBy
{
	const MEDIA_TYPE_ASC = "+mediaType";
	const MEDIA_TYPE_DESC = "-mediaType";
}

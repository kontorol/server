<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolLiveStreamEntryFilter extends KontorolLiveStreamEntryBaseFilter
{
	public function __construct()
	{
		$this->typeIn = KontorolEntryType::LIVE_STREAM;
	}

	/* (non-PHPdoc)
	 * @see KontorolBaseEntryFilter::getListResponse()
	 */
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		list($list, $totalCount) = $this->doGetListResponse($pager);
		
	    $newList = KontorolLiveStreamEntryArray::fromDbArray($list, $responseProfile);
		$response = new KontorolBaseEntryListResponse();
		$response->objects = $newList;
		$response->totalCount = $totalCount;
		
		return $response;
	}
}

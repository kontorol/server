<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolMixEntryFilter extends KontorolMixEntryBaseFilter
{
	public function __construct()
	{
		$this->typeIn = KontorolEntryType::MIX;
	}
	
	/* (non-PHPdoc)
	 * @see KontorolBaseEntryFilter::getListResponse()
	 */
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		list($list, $totalCount) = $this->doGetListResponse($pager);
		
	    $newList = KontorolMixEntryArray::fromDbArray($list, $responseProfile);
		$response = new KontorolBaseEntryListResponse();
		$response->objects = $newList;
		$response->totalCount = $totalCount;
		
		return $response;
	}
}

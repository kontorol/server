<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolLiveChannelArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolLiveChannelArray();
		if ($arr == null)
			return $newArr;
			
		foreach ($arr as $obj)
		{
    		$nObj = KontorolEntryFactory::getInstanceByType($obj->getType());
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolLiveChannel");
	}
}

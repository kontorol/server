<?php
/**
 * A representation of a live stream recording entry configuration
 * 
 * @package api
 * @subpackage objects
 */
class KontorolLiveEntryRecordingOptions extends KontorolObject
{
	
	/**
	 * @var KontorolNullableBoolean
	 */
	public $shouldCopyEntitlement;

	/**
	 * @var KontorolNullableBoolean
	 */
	public $shouldCopyScheduling;
	
	/**
	 * @var KontorolNullableBoolean
	 */
	public $shouldCopyThumbnail;

	/**
	 * @var KontorolNullableBoolean
	 */
	public $shouldMakeHidden;

    /**
     * @var KontorolNullableBoolean
     */
	public $shouldAutoArchive;

    /**
     * @var string
     */
	public $nonDeletedCuePointsTags;

	/**
	 * @var string
	 */
	public $archiveVodSuffixTimezone;

	private static $mapBetweenObjects = array
	(
		"shouldCopyEntitlement",
		"shouldCopyScheduling",
		"shouldCopyThumbnail",
		"shouldMakeHidden",
		"shouldAutoArchive",
		"nonDeletedCuePointsTags",
		"archiveVodSuffixTimezone",
	);
	
	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$mapBetweenObjects);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject($object_to_fill, $props_to_skip)
	 */
	public function toObject($dbObject = null, $propsToSkip = array())
	{
		if (!$dbObject)
		{
			$dbObject = new kLiveEntryRecordingOptions();
		}
		
		return parent::toObject($dbObject, $propsToSkip);
	}
}

<?php
/**
 * @package api
 * @subpackage objects
 *
 */
class KontorolLiveStreamConfigurationArray extends KontorolTypedArray
{
	/**
	 * Returns API array object from regular array of database objects.
	 * @param array $dbArray
	 * @return KontorolLiveStreamConfiguration
	 */
	public static function fromDbArray(array $dbArray = null, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$array = new KontorolLiveStreamConfigurationArray();
		if($dbArray && is_array($dbArray))
		{
			foreach($dbArray as $object)
			{
				/* @var $object kLiveStreamConfiguration */
				$configObject = new KontorolLiveStreamConfiguration();
				$configObject->fromObject($object, $responseProfile);;
				$array[] = $configObject;
			}
		}
		return $array;
	}
	
	public function __construct()
	{
		return parent::__construct("KontorolLiveStreamConfiguration");
	}
	
	/* (non-PHPdoc)
	 * @see KontorolTypedArray::toObjectsArray()
	 */
	public function toObjectsArray()
	{
		$objects = $this->toArray();
		for ($i = 0; $i < count($objects); $i++)
		{
			for ($j = $i+1; $j <count($objects); $j++ )
			{
				if ($objects[$i]->protocol == $objects[$j]->protocol)
				{
					unset($objects[$i]);
				}
			}
		}
		
		$ret = array();
		foreach ($objects as $object)
		{
			$ret[] = $object->toObject();
		}
		
		return $ret;
	}
	
}

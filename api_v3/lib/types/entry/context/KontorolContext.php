<?php
/**
 * @package api
 * @subpackage objects
 */
abstract class KontorolContext extends KontorolObject
{
    /**
     * Function to validate the context.
     */
    abstract protected function validate ();
}

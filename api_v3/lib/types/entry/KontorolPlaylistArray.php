<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolPlaylistArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr = null, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolPlaylistArray();
		if ( $arr == null ) return $newArr;
		foreach ( $arr as $obj )
		{
    		$nObj = KontorolEntryFactory::getInstanceByType($obj->getType());
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolPlaylist");
	}
}

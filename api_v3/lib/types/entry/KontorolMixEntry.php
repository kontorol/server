<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolMixEntry extends KontorolPlayableEntry
{
	/**
	 * Indicates whether the user has submited a real thumbnail to the mix (Not the one that was generated automaticaly)
	 * 
	 * @var bool
	 * @readonly
	 */
	public $hasRealThumbnail;
	
	/**
	 * The editor type used to edit the metadata
	 * 
	 * @var KontorolEditorType
	 */
	public $editorType;

	/**
	 * The xml data of the mix
	 *
	 * @var string
	 */
	public $dataContent;
	
	public function __construct()
	{
		$this->type = KontorolEntryType::MIX;
	}
	
	private static $map_between_objects = array
	(
		"hasRealThumbnail" => "hasRealThumb",
		"editorType",
		"dataContent"
	);
	
	public function getMapBetweenObjects ( )
	{
		return array_merge ( parent::getMapBetweenObjects() , self::$map_between_objects );
	}
	
    public function doFromObject($entry, KontorolDetachedResponseProfile $responseProfile = null)
	{
		parent::doFromObject($entry, $responseProfile);

		if($this->shouldGet('editorType', $responseProfile))
		{
			if ($entry->getEditorType() == "kontorolAdvancedEditor" || $entry->getEditorType() == "Keditor")
			    $this->editorType = KontorolEditorType::ADVANCED;
			else
			    $this->editorType = KontorolEditorType::SIMPLE;
		}
	}
	
	public function toObject($entry = null, $skip = array())
	{
		$entry = parent::toObject($entry, $skip);
		
		if ($this->editorType === KontorolEditorType::ADVANCED)
			$entry->setEditorType("kontorolAdvancedEditor");
		else
			$entry->setEditorType("kontorolSimpleEditor");
			
		return $entry;
	}
}
?>

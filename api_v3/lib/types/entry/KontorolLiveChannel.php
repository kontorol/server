<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolLiveChannel extends KontorolLiveEntry
{
	/**
	 * Playlist id to be played
	 * 
	 * @var string
	 */
	public $playlistId;
	
	/**
	 * Indicates that the segments should be repeated for ever
	 * @var KontorolNullableBoolean
	 */
	public $repeat;
	
	private static $map_between_objects = array
	(
		'playlistId',
		'repeat',
	);

	/* (non-PHPdoc)
	 * @see KontorolLiveEntry::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}
	
	public function __construct()
	{
		parent::__construct();
		
		$this->type = KontorolEntryType::LIVE_CHANNEL;
	}
	
	/* (non-PHPdoc)
	 * @see KontorolMediaEntry::fromSourceType()
	 */
	protected function fromSourceType(entry $entry) 
	{
		$this->sourceType = KontorolSourceType::LIVE_CHANNEL;
	}
	
	/* (non-PHPdoc)
	 * @see KontorolMediaEntry::toSourceType()
	 */
	protected function toSourceType(entry $entry) 
	{
		$entry->setSource(KontorolSourceType::LIVE_CHANNEL);
	}
}

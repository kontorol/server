<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolLiveStreamBitrateArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolLiveStreamBitrateArray();
		if ($arr == null)
			return $newArr;
			
		foreach ($arr as $obj)
		{
			$nObj = new KontorolLiveStreamBitrate();
			$nObj->fromArray($obj);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolLiveStreamBitrate");
	}
}

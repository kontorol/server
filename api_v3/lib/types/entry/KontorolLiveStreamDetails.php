<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolLiveStreamDetails extends KontorolObject
{
	/**
	 * The status of the primary stream
	 *
	 * @var KontorolEntryServerNodeStatus
	 */
	public $primaryStreamStatus = KontorolEntryServerNodeStatus::STOPPED;

	/**
	 * The status of the secondary stream
	 *
	 * @var KontorolEntryServerNodeStatus
	 */
	public $secondaryStreamStatus = KontorolEntryServerNodeStatus::STOPPED;

	/**
	 * @var KontorolViewMode
	 */
	public $viewMode = KontorolViewMode::PREVIEW;

	/**
	 * @var bool
	 */
	public $wasBroadcast = false;

	/**
	 * @var KontorolLiveStreamBroadcastStatus
	 */
	public $broadcastStatus  = KontorolLiveStreamBroadcastStatus::OFFLINE;

	private static $map_between_objects = array
	(
	);
	
	/* (non-PHPdoc)
	 * @see KontorolMediaEntry::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}

}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolConversionAttributeArray extends KontorolTypedArray
{
	public function __construct()
	{
		parent::__construct("KontorolConversionAttribute");
	}
}

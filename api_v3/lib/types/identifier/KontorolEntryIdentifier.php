<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolEntryIdentifier extends KontorolObjectIdentifier
{
	/**
	 * Identifier of the object
	 * @var KontorolEntryIdentifierField
	 */
	public $identifier;
	
	/* (non-PHPdoc)
	 * @see KontorolObjectIdentifier::toObject()
	 */
	public function toObject ($dbObject = null, $propsToSkip = array())
	{
		if (!$dbObject)
			$dbObject = new kEntryIdentifier();

		return parent::toObject($dbObject, $propsToSkip);
	}
	
	private static $map_between_objects = array(
			"identifier",
		);
	
	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}
	
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolExtendingItemMrssParameterArray extends KontorolTypedArray
{
	
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolExtendingItemMrssParameterArray();
		foreach($arr as $obj)
		{
			$nObj = new KontorolExtendingItemMrssParameter();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct()
	{
		return parent::__construct("KontorolExtendingItemMrssParameter");
	}
}

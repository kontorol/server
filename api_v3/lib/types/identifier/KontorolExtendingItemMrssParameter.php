<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolExtendingItemMrssParameter extends KontorolObject
{
	/**
	 * XPath for the extending item
	 * @var string
	 */
	public $xpath;
	
	/**
	 * Object identifier
	 * @var KontorolObjectIdentifier
	 */
	public $identifier;
	
	/**
	 * Mode of extension - append to MRSS or replace the xpath content.
	 * @var KontorolMrssExtensionMode
	 */
	public $extensionMode;
	
	
	private static $map_between_objects = array(
			"xpath",
			"identifier",
			"extensionMode"
		);
		
	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject($object_to_fill, $props_to_skip)
	 */
	public function toObject($dbObject = null, $propsToSkip = array())
	{
		$this->validate();
		if (!$dbObject)
			$dbObject = new kExtendingItemMrssParameter();

		return parent::toObject($dbObject, $propsToSkip);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::fromObject($source_object)
	 */
	public function doFromObject($dbObject, KontorolDetachedResponseProfile $responseProfile = null)
	{
		parent::doFromObject($dbObject, $responseProfile);
		
		/* @var $dbObject kExtendingItemMrssParameter */
		if($this->shouldGet('identifier', $responseProfile))
		{
			$identifierType = get_class($dbObject->getIdentifier());
			KontorolLog::info("Creating identifier for DB identifier type $identifierType");
			switch ($identifierType)
			{
				case 'kEntryIdentifier':
					$this->identifier = new KontorolEntryIdentifier();
					break;
				case 'kCategoryIdentifier':
					$this->identifier = new KontorolCategoryIdentifier();
			}
			
			if ($this->identifier)
				$this->identifier->fromObject($dbObject->getIdentifier());
		}
	}
	
	protected function validate ()
	{
		//Should not allow any extending object but entries to be added in APPEND mode
		if ($this->extensionMode == KontorolMrssExtensionMode::APPEND && get_class($this->identifier) !== 'KontorolEntryIdentifier')
		{
			throw new KontorolAPIException(KontorolErrors::EXTENDING_ITEM_INCOMPATIBLE_COMBINATION);
		}
		
		if (!$this->xpath)
		{
			throw new KontorolAPIException(KontorolErrors::EXTENDING_ITEM_MISSING_XPATH);
		}
	}
}

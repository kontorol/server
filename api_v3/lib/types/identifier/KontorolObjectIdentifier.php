<?php
/**
 * Configuration for extended item in the Kontorol MRSS feeds
 *
 * @package api
 * @subpackage objects
 */
abstract class KontorolObjectIdentifier extends KontorolObject
{
	/**
	 * Comma separated string of enum values denoting which features of the item need to be included in the MRSS 
	 * @dynamicType KontorolObjectFeatureType
	 * @var string
	 */
	public $extendedFeatures;
	
	
	private static $map_between_objects = array(
		"extendedFeatures",
	);
	
	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}

}

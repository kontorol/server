<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolBaseEntryCloneOptionsArray extends KontorolTypedArray
{
    public function __construct()
    {
        parent::__construct("KontorolBaseEntryCloneOptionItem");
    }
}

<?php
/**
 * An int representation to return evaluated dynamic value
 * 
 * @package api
 * @subpackage objects
 * @abstract
 */
abstract class KontorolIntegerField extends KontorolIntegerValue
{
	/* (non-PHPdoc)
	 * @see KontorolIntegerValue::toObject()
	 */
	public function toObject($dbObject = null, $skip = array())
	{
		if(!is_null($this->value) && !($this->value instanceof KontorolNullField))
			throw new KontorolAPIException(KontorolErrors::PROPERTY_VALIDATION_NOT_UPDATABLE, $this->getFormattedPropertyNameWithClassName('value'));

		return parent::toObject($dbObject, $skip);
	}
}

<?php
/**
 * A string representation to return an array of strings
 * 
 * @see KontorolStringValueArray
 * @package api
 * @subpackage objects
 */
class KontorolStringValue extends KontorolValue
{
	/**
	 * @var string
	 */
    public $value;

	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbObject = null, $skip = array())
	{
		if(!$dbObject)
			$dbObject = new kStringValue();
			
		return parent::toObject($dbObject, $skip);
	}
}

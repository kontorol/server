<?php
/**
 * A boolean representation to return an array of booleans
 * 
 * @see KontorolBooleanValueArray
 * @package api
 * @subpackage objects
 */
class KontorolBooleanValue extends KontorolValue
{
	/**
	 * @var bool
	 */
    public $value;

	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbObject = null, $skip = array())
	{
		if(!$dbObject)
			$dbObject = new kBooleanValue();
			
		return parent::toObject($dbObject, $skip);
	}
}

<?php
/**
 * An array of KontorolIntegerValue
 * 
 * @package api
 * @subpackage objects
 */
class KontorolIntegerValueArray extends KontorolTypedArray
{
	/**
	 * @param array<string|kIntegerValue> $strings
	 * @return KontorolIntegerValueArray
	 */
	public static function fromDbArray(array $ints = null, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$intArray = new KontorolIntegerValueArray();
		if($ints && is_array($ints))
		{
			foreach($ints as $int)
			{
				$intObject = new KontorolIntegerValue();
				
				if($int instanceof kValue)
				{
					$intObject->fromObject($int, $responseProfile);;
				}
				else
				{					
					$intObject->value = $int;
				}
				
				$intArray[] = $intObject;
			}
		}
		return $intArray;
	}
	
	public function __construct()
	{
		return parent::__construct("KontorolIntegerValue");
	}
}

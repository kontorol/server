<?php
/**
 * An array of KontorolBooleanValue
 * 
 * @package api
 * @subpackage objects
 */
class KontorolBooleanValueArray extends KontorolTypedArray
{
	/**
	 * @param array<string|kBooleanValue> $strings
	 * @return KontorolBooleanValueArray
	 */
	public static function fromDbArray(array $bools = null, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$boolArray = new KontorolBooleanValueArray();
		if($bools && is_array($bools))
		{
			foreach($bools as $bool)
			{
				$boolObject = new KontorolBooleanValue();
				
				if($bool instanceof kValue)
				{
					$boolObject->fromObject($bool, $responseProfile);;
				}
				else
				{					
					$boolObject->value = $bool;
				}
				
				$boolArray[] = $boolObject;
			}
		}
		return $boolArray;
	}
	
	public function __construct()
	{
		return parent::__construct("KontorolBooleanValue");
	}
}

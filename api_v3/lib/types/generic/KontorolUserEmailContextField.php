<?php
/**
 * Represents the current session user e-mail address context
 * 
 * @package api
 * @subpackage objects
 */
class KontorolUserEmailContextField extends KontorolStringField
{
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbObject = null, $skip = array())
	{
		if(!$dbObject)
			$dbObject = new kUserEmailContextField();
			
		return parent::toObject($dbObject, $skip);
	}
}

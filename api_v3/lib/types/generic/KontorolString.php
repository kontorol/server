<?php
/**
 * A string representation to return an array of strings
 * 
 * @see KontorolStringArray
 * @package api
 * @subpackage objects
 */
class KontorolString extends KontorolObject
{
	/**
	 * @var string
	 */
    public $value;
    
	private static $mapBetweenObjects = array
	(
		"value",
	);
	
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$mapBetweenObjects);
	}
	
	public function toObject($object_to_fill = null, $props_to_skip = array())
	{
	    return $this->value;
	}
}

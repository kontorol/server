<?php
/**
 * An array of KontorolString
 * 
 * @package api
 * @subpackage objects
 */
class KontorolStringArray extends KontorolTypedArray
{
	public static function fromDbArray(array $strings = null)
	{
		return self::fromStringArray($strings);
	}
	
	public static function fromStringArray(array $strings = null)
	{
		$stringArray = new KontorolStringArray();
		if($strings && is_array($strings))
		{
			foreach($strings as $string)
			{
				$stringObject = new KontorolString();
				$stringObject->value = $string;
				$stringArray[] = $stringObject;
			}
		}
		return $stringArray;
	}
	
	public function __construct()
	{
		return parent::__construct("KontorolString");
	}

}

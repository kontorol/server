<?php
/**
 * An int representation to return an array of ints
 * 
 * @see KontorolIntegerValueArray
 * @package api
 * @subpackage objects
 */
class KontorolIntegerValue extends KontorolValue
{
	/**
	 * @var int
	 */
    public $value;

	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbObject = null, $skip = array())
	{
		if(!$dbObject)
			$dbObject = new kIntegerValue();
			
		return parent::toObject($dbObject, $skip);
	}
}

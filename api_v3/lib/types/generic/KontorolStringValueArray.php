<?php
/**
 * An array of KontorolStringValue
 * 
 * @package api
 * @subpackage objects
 */
class KontorolStringValueArray extends KontorolTypedArray
{
	/**
	 * @param array<string|kStringValue> $strings
	 * @return KontorolStringValueArray
	 */
	public static function fromDbArray(array $strings = null, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$stringArray = new KontorolStringValueArray();
		if($strings && is_array($strings))
		{
			foreach($strings as $string)
			{
				$stringObject = new KontorolStringValue();
				
				if($string instanceof kValue)
				{
					$stringObject->fromObject($string, $responseProfile);;
				}
				else
				{					
					$stringObject->value = $string;
				}
				
				$stringArray[] = $stringObject;
			}
		}
		return $stringArray;
	}
	
	public function __construct()
	{
		return parent::__construct("KontorolStringValue");
	}
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolBulkUploadPluginData extends KontorolObject
{
    /**
     * @var string
     */
    public $field;
    
    /**
     * @var string
     */
    public $value;
}

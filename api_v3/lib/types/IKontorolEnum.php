<?php
/**
 * @package api
 * @subpackage enum
 */
interface IKontorolEnum
{
	/**
	 * @return array
	 */
	public static function getDescriptions();
}

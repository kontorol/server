<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolUserEntryArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolUserEntryArray();
		foreach($arr as $obj)
		{
			/* @var $obj UserEntry */
			$nObj = KontorolUserEntry::getInstanceByType($obj->getType());
			if (!$nObj)
			{
				throw new KontorolAPIException(KontorolErrors::USER_ENTRY_OBJECT_TYPE_ERROR, $obj->getType(), $obj->getId());
			}
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}

		return $newArr;
	}

	public function __construct( )
	{
		return parent::__construct ( "KontorolUserEntry" );
	}
}

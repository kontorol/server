<?php
/**
 * @package api
 * @subpackage objects
 */
abstract class KontorolAssociativeArray extends KontorolTypedArray
{
	/* (non-PHPdoc)
	 * @see KontorolTypedArray::offsetSet()
	 */
	public function offsetSet($offset, $value) 
	{
		$this->validateType($value);
		
		if ($offset === null)
		{
			$this->array[] = $value;
		}
		else
		{
			$this->array[$offset] = $value;
		}
			
		$this->count = count ( $this->array );
	}
}

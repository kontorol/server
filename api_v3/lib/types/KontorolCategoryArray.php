<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolCategoryArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolCategoryArray();
		foreach($arr as $obj)
		{
			$nObj = new KontorolCategory();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct()
	{
		return parent::__construct("KontorolCategory");
	}
}
?>

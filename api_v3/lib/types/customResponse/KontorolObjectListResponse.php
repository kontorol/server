<?php

/**
 * @package api
 * @subpackage objects
 */
class KontorolObjectListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolObjectArray
	 * @readonly
	 */
	public $objects;
}

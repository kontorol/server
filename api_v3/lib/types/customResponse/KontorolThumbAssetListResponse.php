<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolThumbAssetListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolThumbAssetArray
	 * @readonly
	 */
	public $objects;
}

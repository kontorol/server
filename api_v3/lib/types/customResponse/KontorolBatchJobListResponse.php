<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolBatchJobListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolBatchJobArray
	 * @readonly
	 */
	public $objects;
	
}

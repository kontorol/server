<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolMediaInfoListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolMediaInfoArray
	 * @readonly
	 */
	public $objects;
}

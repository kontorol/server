<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolCategoryUserListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolCategoryUserArray
	 * @readonly
	 */
	public $objects;
}

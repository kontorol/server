<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolUploadTokenListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolUploadTokenArray
	 * @readonly
	 */
	public $objects;
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolStartWidgetSessionResponse extends KontorolObject
{
	/**
	 * @var int
	 * @readonly
	 */
	public $partnerId;
	
	/**
	 * @var string
	 * @readonly
	 */
	public $ks;
	
	/**
	 * @var string
	 * @readonly
	 */
	public $userId;
}

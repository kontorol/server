<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolUiConfListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolUiConfArray
	 * @readonly
	 */
	public $objects;
}

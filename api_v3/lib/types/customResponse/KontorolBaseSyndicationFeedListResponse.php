<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolBaseSyndicationFeedListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolBaseSyndicationFeedArray
	 * @readonly
	 */
	public $objects;
}

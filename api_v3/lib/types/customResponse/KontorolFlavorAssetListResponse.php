<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolFlavorAssetListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolFlavorAssetArray
	 * @readonly
	 */
	public $objects;
}

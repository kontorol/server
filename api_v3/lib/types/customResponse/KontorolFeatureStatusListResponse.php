<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolFeatureStatusListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolFeatureStatusArray
	 * @readonly
	 */
	public $objects;
}

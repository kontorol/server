<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolBatchGetExclusiveNotificationJobsResponse extends KontorolObject
{
	/**
	 * @var KontorolBatchJobArray
	 * @readonly
	 */
	public $notifications;

	/**
	 * @var KontorolPartnerArray
	 * @readonly
	 */
	public $partners;
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolCategoryEntryListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolCategoryEntryArray
	 * @readonly
	 */
	public $objects;
}

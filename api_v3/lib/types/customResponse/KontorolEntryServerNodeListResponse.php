<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolEntryServerNodeListResponse extends KontorolListResponse {

	/**
	 * @var KontorolEntryServerNodeArray
	 * @readonly
	 */
	public $objects;
}

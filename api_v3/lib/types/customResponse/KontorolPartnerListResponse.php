<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolPartnerListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolPartnerArray
	 * @readonly
	 */
	public $objects;
}

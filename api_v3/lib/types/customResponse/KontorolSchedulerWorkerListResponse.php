<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolSchedulerWorkerListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolSchedulerWorkerArray
	 * @readonly
	 */
	public $objects;
}

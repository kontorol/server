<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolUserRoleListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolUserRoleArray
	 * @readonly
	 */
	public $objects;
}

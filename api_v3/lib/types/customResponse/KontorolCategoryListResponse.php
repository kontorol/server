<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolCategoryListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolCategoryArray
	 * @readonly
	 */
	public $objects;
}

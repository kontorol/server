<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolPermissionListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolPermissionArray
	 * @readonly
	 */
	public $objects;
}

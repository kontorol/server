<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolWidgetListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolWidgetArray
	 * @readonly
	 */
	public $objects;
}

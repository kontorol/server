<?php
/**
 * @package api
 * @subpackage objects
 * @deprecated
 */
class KontorolSearchResultResponse extends KontorolObject
{
	/**
	 * @var KontorolSearchResultArray
	 * @readonly
	 */
	public $objects;

	/**
	 * @var bool
	 * @readonly
	 */
	public $needMediaInfo;
}

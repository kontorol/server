<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolModerationFlagListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolModerationFlagArray
	 * @readonly
	 */
	public $objects;
}

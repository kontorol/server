<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolMixListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolMixEntryArray
	 * @readonly
	 */
	public $objects;
}

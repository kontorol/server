<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolSchedulerListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolSchedulerArray
	 * @readonly
	 */
	public $objects;
}

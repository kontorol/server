<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolAccessControlProfileListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolAccessControlProfileArray
	 * @readonly
	 */
	public $objects;
}

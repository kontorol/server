<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolBulkUploadListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolBulkUploads
	 * @readonly
	 */
	public $objects;
}

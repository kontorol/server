<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolUploadResponse extends KontorolObject
{
	/**
	 * @var string
	 */
	public $uploadTokenId;

	/**
	 * @var int
	 */
	public $fileSize;
	
	/**
	 * 
	 * @var KontorolUploadErrorCode
	 */
	public $errorCode;
	
	/**
	 * 
	 * @var string
	 */
	public $errorDescription;
	
}

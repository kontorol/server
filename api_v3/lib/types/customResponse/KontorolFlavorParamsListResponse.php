<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolFlavorParamsListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolFlavorParamsArray
	 * @readonly
	 */
	public $objects;
}

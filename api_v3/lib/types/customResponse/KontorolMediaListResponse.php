<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolMediaListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolMediaEntryArray
	 * @readonly
	 */
	public $objects;
}

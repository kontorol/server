<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolUserLoginDataListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolUserLoginDataArray
	 * @readonly
	 */
	public $objects;
}

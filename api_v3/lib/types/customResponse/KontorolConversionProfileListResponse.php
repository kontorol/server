<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolConversionProfileListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolConversionProfileArray
	 * @readonly
	 */
	public $objects;
}

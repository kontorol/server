<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolConversionProfileAssetParamsListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolConversionProfileAssetParamsArray
	 * @readonly
	 */
	public $objects;
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolLiveStreamListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolLiveStreamEntryArray
	 * @readonly
	 */
	public $objects;
}

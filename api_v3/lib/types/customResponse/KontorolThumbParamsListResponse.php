<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolThumbParamsListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolThumbParamsArray
	 * @readonly
	 */
	public $objects;
}

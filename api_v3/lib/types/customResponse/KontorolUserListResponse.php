<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolUserListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolUserArray
	 * @readonly
	 */
	public $objects;
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolControlPanelCommandListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolControlPanelCommandArray
	 * @readonly
	 */
	public $objects;
}

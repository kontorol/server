<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolDeliveryProfileListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolDeliveryProfileArray
	 * @readonly
	 */
	public $objects;
}

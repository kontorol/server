<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolPermissionItemListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolPermissionItemArray
	 * @readonly
	 */
	public $objects;
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolPlaylistListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolPlaylistArray
	 * @readonly
	 */
	public $objects;
}

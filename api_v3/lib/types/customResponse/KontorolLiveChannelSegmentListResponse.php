<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolLiveChannelSegmentListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolLiveChannelSegmentArray
	 * @readonly
	 */
	public $objects;
}

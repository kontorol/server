<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolReportListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolReportArray
	 * @readonly
	 */
	public $objects;
}

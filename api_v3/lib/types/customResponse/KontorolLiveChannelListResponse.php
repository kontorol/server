<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolLiveChannelListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolLiveChannelArray
	 * @readonly
	 */
	public $objects;
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolAppTokenListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolAppTokenArray
	 * @readonly
	 */
	public $objects;
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolDataListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolDataEntryArray
	 * @readonly
	 */
	public $objects;
}

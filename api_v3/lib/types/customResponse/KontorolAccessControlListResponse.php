<?php
/**
 * @package api
 * @subpackage objects
 * @deprecated use KontorolAccessControlProfileListResponse instead
 */
class KontorolAccessControlListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolAccessControlArray
	 * @readonly
	 */
	public $objects;
}

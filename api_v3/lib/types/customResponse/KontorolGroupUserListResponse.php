<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolGroupUserListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolGroupUserArray
	 * @readonly
	 */
	public $objects;
}

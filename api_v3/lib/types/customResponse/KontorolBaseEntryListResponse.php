<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolBaseEntryListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolBaseEntryArray
	 * @readonly
	 */
	public $objects;
}

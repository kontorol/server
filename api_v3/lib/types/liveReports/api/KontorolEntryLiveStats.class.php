<?php

/**
 * @package api
 * @subpackage objects
 */
class KontorolEntryLiveStats extends KontorolLiveStats
{				
	/**
	 * @var string
	 **/
	public $entryId;
	
	/**
	 * @var int
	 */
	public $peakAudience;

	/**
	 * @var int
	 */
	public $peakDvrAudience;
	
	public function getWSObject() {
		$obj = new WSEntryLiveStats();
		$obj->fromKontorolObject($this);
		return $obj;
	}
	
}



<?php

/**
 * @package api
 * @subpackage objects
 */
class KontorolLiveReportExportResponse extends KontorolObject
{				
	/**
	 * @var bigint
	 **/
	public $referenceJobId;
	
	/**
	 * @var string
	 **/
	public $reportEmail;
	
}



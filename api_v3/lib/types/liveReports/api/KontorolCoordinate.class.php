<?php

/**
 * @package api
 * @subpackage objects
 */
class KontorolCoordinate extends KontorolObject
{	
	/**
	 * @var float
	 **/
	public $latitude;
	
	/**
	 * @var float
	 **/
	public $longitude;
	
	/**
	 * @var string
	 **/
	public $name;
	
	public function getWSObject() {
		$obj = new WSCoordinate();
		$obj->fromKontorolObject($this);
		return $obj;
	}
}



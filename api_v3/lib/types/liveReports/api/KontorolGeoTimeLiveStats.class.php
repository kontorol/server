<?php

/**
 * @package api
 * @subpackage objects
 */
class KontorolGeoTimeLiveStats extends KontorolEntryLiveStats
{	
	/**
	 * @var KontorolCoordinate
	 **/
	public $city;
	
	/**
	 * @var KontorolCoordinate
	 **/
	public $country;
	
	public function getWSObject() {
		$obj = new WSGeoTimeLiveStats();
		$obj->fromKontorolObject($this);
		return $obj;
	}
}



<?php

/**
 * @package api
 * @subpackage objects
 */
class KontorolLiveReportInputFilter extends KontorolObject
{	
	/**
	 * @var string
	 **/
	public $entryIds;
	
	/**
	 * @var time
	 **/
	public $fromTime;
	
	/**
	 * @var time
	 **/
	public $toTime;
	
	/**
	 * @var KontorolNullableBoolean
	 **/
	public $live;
	
	/**
	 * @var KontorolLiveReportOrderBy
	 */
	public $orderBy;
	
	public function getWSObject() {
		$obj = new WSLiveReportInputFilter();
		$obj->fromKontorolObject($this);
		return $obj;
	}
}



<?php

/**
 * @package api
 * @subpackage objects
 */
class KontorolEntryReferrerLiveStats extends KontorolEntryLiveStats
{			
	/**
	 * @var string
	 **/
	public $referrer;
	
	public function getWSObject() {
		$obj = new WSEntryReferrerLiveStats();
		$obj->fromKontorolObject($this);
		return $obj;
	}
}



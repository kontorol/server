<?php

/**
 * @package api
 * @subpackage objects
 */
class KontorolLiveStatsListResponse extends KontorolListResponse
{				
	/**
	 *
	 * @var KontorolLiveStats
	 **/
	public $objects;
	
	public function getWSObject() {
		$obj = new WSLiveEntriesListResponse();
		$obj->fromKontorolObject($this);
		return $obj;
	}
	
}



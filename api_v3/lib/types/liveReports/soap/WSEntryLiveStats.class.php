<?php


class WSEntryLiveStats extends WSLiveStats
{				
	function getKontorolObject() {
		return new KontorolEntryLiveStats();
	}
	
	/**
	 * @var string
	 **/
	public $entryId;
	
	/**
	 * @var long
	 */
	public $peakAudience;

	/**
	 * @var long
	 */
	public $peakDvrAudience;
}



<?php


class WSLiveEventsListResponse extends WSBaseObject
{				
	function getKontorolObject() {
		return new KontorolLiveEventsListResponse();
	}
	
	/**
	 * @var array
	 **/
	public $objects;
	
	/**
	 * @var int
	 **/
	public $totalCount;
	
}



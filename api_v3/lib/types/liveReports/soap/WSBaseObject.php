<?php

abstract class WSBaseObject extends SoapObject {
	
	abstract function getKontorolObject();
	
	public function toKontorolObject() {
		$kontorolObj = $this->getKontorolObject();
		self::cloneObject($this, $kontorolObj);
		return $kontorolObj;
	}
	
	public function fromKontorolObject($kontorolObj) {
		self::cloneObject($kontorolObj, $this);
	}
	
	protected static function cloneObject($objA, $objB) {
		$reflect = new ReflectionClass($objA);
		foreach($reflect->getProperties(ReflectionProperty::IS_PUBLIC) as $prop)
		{
			$name = $prop->getName();
			$value = $prop->getValue($objA);
			
			if ($value instanceof WSBaseObject) {
				$value = $value->toKontorolObject();
			} else if($value instanceof SoapArray) {
				/**
				 * @var SoapArray $value
				 */
				$arr = $value->toArray();
				$newObj = array();
				foreach($arr as $val) {
					if ($val instanceof WSBaseObject) {
						$newObj[] = $val->toKontorolObject();
					} else {
						$newObj[] = $val;
					}
				} 
				$value = $newObj;
			}
			
			$objB->$name = $value; 
		}
	}
}


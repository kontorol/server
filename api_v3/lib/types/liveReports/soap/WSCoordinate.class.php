<?php

class WSCoordinate extends WSBaseObject
{	
	function getKontorolObject() {
		return new KontorolCoordinate();
	}
				
	/**
	 * @var float
	 **/
	public $latitude;
	
	/**
	 * @var float
	 **/
	public $longitude;
	
	/**
	 * @var string
	 **/
	public $name;
	
}



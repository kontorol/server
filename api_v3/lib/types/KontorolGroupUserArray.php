<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolGroupUserArray extends KontorolTypedArray
{
	public static function fromDbArray($arr)
	{
		$newArr = new KontorolGroupUserArray();
		foreach($arr as $obj)
		{
			$nObj = new KontorolGroupUser();
			$nObj->fromObject($obj);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct()
	{
		return parent::__construct("KontorolGroupUser");
	}
}

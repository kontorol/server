<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolMailType extends KontorolDynamicEnum implements MailType
{
	public static function getEnumClass()
	{
		return 'MailType';
	}
}

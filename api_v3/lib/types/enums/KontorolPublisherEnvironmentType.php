<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolPublisherEnvironmentType extends KontorolDynamicEnum implements PublisherEnvironmentType
{
	/**
	 * @return string
	 */
	public static function getEnumClass()
	{
		return 'PublisherEnvironmentType';
	}
}

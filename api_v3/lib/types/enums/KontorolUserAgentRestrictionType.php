<?php
/**
 * @package api
 * @subpackage enum
 * @deprecated use KontorolRule instead
 */
class KontorolUserAgentRestrictionType extends KontorolEnum implements accessControlListRestrictionType
{
	// see interface "accessControlListRestrictionType" for values
}

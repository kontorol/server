<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolConversionEngineType extends KontorolDynamicEnum implements conversionEngineType
{
	public static function getEnumClass()
	{
		return 'conversionEngineType';
	}
}

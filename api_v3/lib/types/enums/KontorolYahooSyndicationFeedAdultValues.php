<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolYahooSyndicationFeedAdultValues extends KontorolStringEnum
{
	// applied on <media:rating scheme="urn:simple">
	const ADULT = "adult";
	const NON_ADULT = "nonadult";
}

<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolLiveChannelSegmentType extends KontorolDynamicEnum implements LiveChannelSegmentType
{
	public static function getEnumClass()
	{
		return 'LiveChannelSegmentType';
	}
}

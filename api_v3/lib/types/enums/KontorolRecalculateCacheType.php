<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolRecalculateCacheType extends KontorolDynamicEnum implements RecalculateCacheType
{
	/**
	 * @return string
	 */
	public static function getEnumClass()
	{
		return 'RecalculateCacheType';
	}
}

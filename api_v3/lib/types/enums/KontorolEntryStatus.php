<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolEntryStatus extends KontorolDynamicEnum implements entryStatus
{
	public static function getEnumClass()
	{
		return 'entryStatus';
	}
}

<?php 
/**
 * @package api
 * @subpackage enum
 */
class KontorolNotificationObjectType extends KontorolEnum
{
	const ENTRY = 1;
	const KSHOW = 2;
	const USER = 3;	
	const BATCH_JOB = 4;	
}

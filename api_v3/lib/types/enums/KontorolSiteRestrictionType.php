<?php
/**
 * @package api
 * @subpackage enum
 * @deprecated use KontorolRule instead
 */
class KontorolSiteRestrictionType extends KontorolEnum
{
	const RESTRICT_SITE_LIST = 0;
	const ALLOW_SITE_LIST = 1;
}

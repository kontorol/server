<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolContextType extends KontorolDynamicEnum implements ContextType
{
	public static function getEnumClass()
	{
		return 'ContextType';
	}
}

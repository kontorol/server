<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolLiveChannelSegmentStatus extends KontorolDynamicEnum implements LiveChannelSegmentStatus
{
	public static function getEnumClass()
	{
		return 'LiveChannelSegmentStatus';
	}
}

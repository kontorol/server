<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolExtractMediaType extends KontorolEnum
{
	const ENTRY_INPUT = 0;
	const FLAVOR_INPUT = 1;
}

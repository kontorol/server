<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolResetPassLinkType extends KontorolDynamicEnum implements resetPassLinkType
{
	public static function getEnumClass()
	{
		return 'resetPassLinkType';
	}
}

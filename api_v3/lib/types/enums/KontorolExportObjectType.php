<?php

/**
 * @package api
 * @subpackage enum
 */
class KontorolExportObjectType extends KontorolDynamicEnum implements ExportObjectType
{
	public static function getEnumClass()
	{
		return 'ExportObjectType';
	}
}

<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolProvisionJobType extends KontorolEnum
{
	const PROVIDE = 1;
	const DELETE = 2;
}
?>

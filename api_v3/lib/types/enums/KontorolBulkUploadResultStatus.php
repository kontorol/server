<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolBulkUploadResultStatus extends KontorolDynamicEnum implements BulkUploadResultStatus
{
	public static function getEnumClass()
	{
		return 'BulkUploadResultStatus';
	}
}

<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolLiveChannelSegmentTriggerType extends KontorolDynamicEnum implements LiveChannelSegmentTriggerType
{
	public static function getEnumClass()
	{
		return 'LiveChannelSegmentTriggerType';
	}
}

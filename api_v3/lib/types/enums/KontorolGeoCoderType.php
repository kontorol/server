<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolGeoCoderType extends KontorolDynamicEnum implements geoCoderType
{
	public static function getEnumClass()
	{
		return 'geoCoderType';
	}
}

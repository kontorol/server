<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolWidgetSecurityType extends KontorolEnum
{
	const NONE = 1;
	const TIMEHASH = 2;
}

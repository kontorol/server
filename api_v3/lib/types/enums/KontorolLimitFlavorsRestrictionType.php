<?php
/**
 * @package api
 * @subpackage enum
 * @deprecated use KontorolRule instead
 */
class KontorolLimitFlavorsRestrictionType extends KontorolEnum implements accessControlListRestrictionType
{
	// see interface "accessControlListRestrictionType" for values
}

<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolEntryType extends KontorolDynamicEnum implements entryType
{
	public static function getEnumClass()
	{
		return 'entryType';
	}
}

<?php
/**
 * @package api
 * @subpackage enum
 * @deprecated use KontorolRule instead
 */
class KontorolCountryRestrictionType extends KontorolEnum
{
	const RESTRICT_COUNTRY_LIST = 0;
	const ALLOW_COUNTRY_LIST = 1;
}

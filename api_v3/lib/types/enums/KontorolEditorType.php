<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolEditorType extends KontorolEnum
{
	const SIMPLE = 1;
	const ADVANCED = 2;
}

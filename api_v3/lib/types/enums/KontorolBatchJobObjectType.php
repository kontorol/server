<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolBatchJobObjectType extends KontorolDynamicEnum implements BatchJobObjectType
{
	/**
	 * @return string
	 */
	public static function getEnumClass()
	{
		return 'BatchJobObjectType';
	}
}

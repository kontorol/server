<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolReportType extends KontorolDynamicEnum implements ReportType
{
	/**
	 * @return string
	 */
	public static function getEnumClass()
	{
		return 'ReportType';
	}

}

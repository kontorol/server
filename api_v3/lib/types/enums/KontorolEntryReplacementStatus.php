<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolEntryReplacementStatus extends KontorolDynamicEnum implements entryReplacementStatus
{
	public static function getEnumClass()
	{
		return 'entryReplacementStatus';
	}
}

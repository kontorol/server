<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolModerationFlagStatus extends KontorolDynamicEnum implements moderationFlagStatus
{
	public static function getEnumClass()
	{
		return 'moderationFlagStatus';
	}
}

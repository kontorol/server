<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolSearchOperatorType extends KontorolEnum
{
	const SEARCH_AND = 1;
	const SEARCH_OR = 2;
}

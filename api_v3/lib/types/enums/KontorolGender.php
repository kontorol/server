<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolGender extends KontorolEnum
{
	const UNKNOWN = 0;
	const MALE = 1;
	const FEMALE = 2;
}

<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolEntryApplication extends KontorolDynamicEnum implements EntryApplication
{
	public static function getEnumClass()
	{
		return 'EntryApplication';
	}
}

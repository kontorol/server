<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolUiConfCreationMode extends KontorolEnum
{
	const WIZARD = 2;
	const ADVANCED = 3;
}

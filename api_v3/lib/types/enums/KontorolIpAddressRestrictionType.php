<?php
/**
 * @package api
 * @subpackage enum
 * @deprecated use KontorolRule instead
 */
class KontorolIpAddressRestrictionType extends KontorolEnum implements accessControlListRestrictionType
{
	// see interface "accessControlListRestrictionType" for values
}

<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolSyndicationFeedStatus extends KontorolEnum
{
	const DELETED = -1;
	const ACTIVE = 1;
}

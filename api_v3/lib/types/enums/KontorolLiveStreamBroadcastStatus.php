<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolLiveStreamBroadcastStatus extends KontorolEnum
{
	const OFFLINE = 1;
	const PREVIEW = 2;
	const LIVE = 3;
}

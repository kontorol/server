<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolSourceType extends KontorolDynamicEnum implements EntrySourceType
{
	public static function getEnumClass()
	{
		return 'EntrySourceType';
	}
}

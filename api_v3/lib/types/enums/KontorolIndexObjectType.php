<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolIndexObjectType extends KontorolDynamicEnum implements IndexObjectType
{
	public static function getEnumClass()
	{
		return 'IndexObjectType';
	}
}

<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolBitRateMode extends KontorolEnum
{
	const CBR = 1;
	const VBR = 2;
}

<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolPartnerStatus extends KontorolEnum
{
	const DELETED = 0;
	const ACTIVE = 1;
	const BLOCKED = 2;
	const FULL_BLOCK = 3;
}

<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolMediaParserType extends KontorolDynamicEnum implements mediaParserType
{
	public static function getEnumClass()
	{
		return 'mediaParserType';
	}
}

<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolUploadErrorCode extends KontorolEnum
{
	const NO_ERROR = 0;
	const GENERAL_ERROR = 1;
	const PARTIAL_UPLOAD = 2;
}

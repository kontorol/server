<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolBulkUploadAction extends KontorolDynamicEnum implements BulkUploadAction
{
	/**
	 * @return string
	 */
	public static function getEnumClass()
	{
		return 'BulkUploadAction';
	}
}

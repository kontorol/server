<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolStorageProfileReadyBehavior extends KontorolEnum implements StorageProfileReadyBehavior
{
    // see StorageProfileReadyBehavior for values
}

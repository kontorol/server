<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolFileSyncObjectType extends KontorolDynamicEnum implements FileSyncObjectType
{
	/**
	 * @return string
	 */
	public static function getEnumClass()
	{
		return 'FileSyncObjectType';
	}
}

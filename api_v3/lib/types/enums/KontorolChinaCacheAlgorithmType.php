<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolChinaCacheAlgorithmType extends KontorolEnum
{
	const SHA1 = 1;
	const SHA256 = 2;
}

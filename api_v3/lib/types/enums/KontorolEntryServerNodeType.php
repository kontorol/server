<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolEntryServerNodeType extends KontorolDynamicEnum implements EntryServerNodeType
{
	public static function getEnumClass()
	{
		return 'EntryServerNodeType';
	}
}

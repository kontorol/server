<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolUserEntryType extends KontorolDynamicEnum implements UserEntryType
{
	public static function getEnumClass()
	{
		return 'UserEntryType';
	}
}

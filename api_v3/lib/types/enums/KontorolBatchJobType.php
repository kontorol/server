<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolBatchJobType extends KontorolDynamicEnum implements BatchJobType
{
	/**
	 * @return string
	 */
	public static function getEnumClass()
	{
		return 'BatchJobType';
	}
}

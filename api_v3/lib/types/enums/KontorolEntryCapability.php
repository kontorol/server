<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolEntryCapability extends KontorolDynamicEnum implements EntryCapability
{
	/**
	 * @return string
	 */
	public static function getEnumClass()
	{
		return 'EntryCapability';
	}
}

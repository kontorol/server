<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolHTMLPurifierBehaviourType extends KontorolDynamicEnum implements HTMLPurifierBehaviourType
{
	/**
	 * @return string
	 */
	public static function getEnumClass()
	{
		return 'HTMLPurifierBehaviourType';
	}
}

<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolLiveStatsEventType extends KontorolEnum
{
	const LIVE = 1;
	const DVR = 2;
}

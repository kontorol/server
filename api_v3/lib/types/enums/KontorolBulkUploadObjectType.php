<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolBulkUploadObjectType extends KontorolDynamicEnum implements BulkUploadObjectType
{
	/**
	 * @return string
	 */
	public static function getEnumClass()
	{
		return 'BulkUploadObjectType';
	}
}

<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolExportProtocol extends KontorolEnum
{
	const KONTOROL_DC = 0;
	const FTP = 1;
	const SCP = 2;
	const SFTP = 3;
	const S3 = 6;
}

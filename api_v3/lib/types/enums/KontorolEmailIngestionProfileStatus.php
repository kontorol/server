<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolEmailIngestionProfileStatus extends KontorolEnum
{
	const INACTIVE = 0;
	const ACTIVE = 1;
}

<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolRuleActionType extends KontorolDynamicEnum implements RuleActionType
{
	public static function getEnumClass()
	{
		return 'RuleActionType';
	}
}

<?php
/**
 * @package api
 * @subpackage enum
 * @deprecated
 */
class KontorolDirectoryRestrictionType extends KontorolEnum
{
	const DONT_DISPLAY = 0;
	const DISPLAY_WITH_LINK = 1;
}

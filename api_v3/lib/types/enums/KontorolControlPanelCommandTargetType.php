<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolControlPanelCommandTargetType extends KontorolEnum
{
	const DATA_CENTER = 1;
	const SCHEDULER = 2;
	const JOB_TYPE = 3;
	const JOB = 4;
	const BATCH = 5;
}

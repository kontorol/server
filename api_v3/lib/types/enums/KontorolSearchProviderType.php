<?php
/**
 * @package api
 * @subpackage enum
 * @deprecated
 */
class KontorolSearchProviderType extends KontorolEnum
{
	const FLICKR = 3;
	const YOUTUBE = 4;
	const MYSPACE = 7;
	const PHOTOBUCKET = 8;
	const JAMENDO = 9;
	const CCMIXTER = 10;
	const NYPL = 11;
	const CURRENT = 12;
	const MEDIA_COMMONS = 13;
	const KONTOROL = 20;
	const KONTOROL_USER_CLIPS = 21;
	const ARCHIVE_ORG = 22;
	const KONTOROL_PARTNER = 23;
	const METACAFE = 24;
	const SEARCH_PROXY = 28;
	const PARTNER_SPECIFIC = 100;
}

<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolAssetType extends KontorolDynamicEnum implements assetType
{
	public static function getEnumClass()
	{
		return 'assetType';
	}
}

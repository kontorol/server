<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolGoogleSyndicationFeedAdultValues extends KontorolStringEnum
{
	const YES = "Yes";
	const NO = "No";
}

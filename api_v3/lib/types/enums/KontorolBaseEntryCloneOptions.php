<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolBaseEntryCloneOptions extends KontorolDynamicEnum implements BaseEntryCloneOptions
{
    public static function getEnumClass()
    {
        return 'BaseEntryCloneOptions';
    }

}

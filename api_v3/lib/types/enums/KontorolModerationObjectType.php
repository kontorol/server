<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolModerationObjectType extends KontorolDynamicEnum implements moderationObjectType
{
	public static function getEnumClass()
	{
		return 'moderationObjectType';
	}
}

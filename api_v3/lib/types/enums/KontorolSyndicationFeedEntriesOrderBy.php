<?php
/**
 * @package api
 * @subpackage filters.enum
 */
class KontorolSyndicationFeedEntriesOrderBy extends KontorolStringEnum
{
	const CREATED_AT_DESC = "-createdAt";
	const RECENT = "recent";
}

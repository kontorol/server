<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolServerNodeType extends KontorolDynamicEnum implements serverNodeType
{
	public static function getEnumClass()
	{
		return 'serverNodeType';
	}
}

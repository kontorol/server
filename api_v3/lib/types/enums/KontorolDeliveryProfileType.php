<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolDeliveryProfileType extends KontorolDynamicEnum implements DeliveryProfileType
{
	/**
	 * @return string
	 */
	public static function getEnumClass()
	{
		return 'DeliveryProfileType';
	}
}

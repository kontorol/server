<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolControlPanelCommandStatus extends KontorolEnum
{
	const PENDING = 1;
	const HANDLED = 2;
	const DONE = 3;
	const FAILED = 4;
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolCategoryEntryArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolCategoryEntryArray();
		foreach($arr as $obj)
		{
			$nObj = new KontorolCategoryEntry();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct()
	{
		return parent::__construct("KontorolCategoryEntry");
	}
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolUiConfArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolUiConfArray();
		foreach ( $arr as $obj )
		{
			$nObj = new KontorolUiConf();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct( )
	{
		return parent::__construct ( "KontorolUiConf" );
	}
}

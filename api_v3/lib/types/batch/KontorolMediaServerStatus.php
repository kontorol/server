<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolMediaServerStatus extends KontorolObject
{
	private static $mapBetweenObjects = array
	(
	);
	
	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$mapBetweenObjects);
	}
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolBatchJob extends KontorolObject implements IFilterable
{
	
	/**
	 * @var bigint
	 * @readonly
	 * @filter eq,gte
	 */
	public $id;
	
	/**
	 * @var int
	 * @readonly
	 * @filter eq,in,notin
	 */
	public $partnerId;
	
	
	/**
	 * @var time
	 * @readonly
	 * @filter gte,lte,order
	 */
	public $createdAt;
	
	/**
	 * @var time
	 * @readonly
	 * @filter gte,lte,order
	 */
	public $updatedAt;
	
	/**
	 * @var time
	 * @readonly
	 */
	public $deletedAt;
	
	/**
	 * @var int
	 * @readonly
	 */
	public $lockExpiration;
	
	/**
	 * @var int
	 * @readonly
	 * @filter gte,lte,order
	 */
	public $executionAttempts;
	
	/**
	 * @var int
	 * @readonly
	 * @filter gte,lte,order
	 */
	public $lockVersion;
	
	/**
	 * @var string
	 * @filter eq
	 */
	public $entryId;
	
	/**
	 * @var string
	 */
	public $entryName;
	
	/**
	 * @var KontorolBatchJobType
	 * @readonly 
	 * @filter eq,in,notin
	 */
    public $jobType;
    
	/**
	 * @var int
	 * @filter eq,in,notin
	 */
    public $jobSubType;
    
	/**
	 * @var KontorolJobData
	 */
    public $data;

    /**
	 * @var KontorolBatchJobStatus
	 * @filter eq,in,notin,order
	 */
    public $status;
    
    /**
	 * @var int
	 */
    public $abort;
    
    /**
	 * @var int
	 */
    public $checkAgainTimeout;

    /**
	 * @var string
	 */
    public $message ;
    
    /**
	 * @var string
	 */
    public $description ;
    
    /**
	 * @var int
	 * @filter gte,lte,eq,in,notin,order
	 */
    public $priority ;
    
    /**
     * @var KontorolBatchHistoryDataArray
     */
    public $history ;
    
    /**
     * The id of the bulk upload job that initiated this job
	 * @var int
	 */    
    public $bulkJobId;
    
    /**
     * @var int
     * @filter gte,lte,eq
     */
    public $batchVersion;
    
    
    /**
     * When one job creates another - the parent should set this parentJobId to be its own id.
	 * @var int
	 */    
    public $parentJobId;
    
    
    /**
     * The id of the root parent job
	 * @var int
	 */    
    public $rootJobId;
    
    
    /**
     * The time that the job was pulled from the queue
	 * @var int
	 * @filter gte,lte,order
	 */    
    public $queueTime;
    
    
    /**
     * The time that the job was finished or closed as failed
	 * @var int
	 * @filter gte,lte,order
	 */    
    public $finishTime;
    
    
    /**
	 * @var KontorolBatchJobErrorTypes
	 * @filter eq,in,notin
	 */    
    public $errType;
    
    
    /**
	 * @var int
	 * @filter eq,in,notin
	 */    
    public $errNumber;
    
    
    /**
	 * @var int
	 * @filter lt,gt,order
	 */    
    public $estimatedEffort;
    
    /**
     * @var int
     * @filter lte,gte
     */
    public $urgency;
    
    /**
	 * @var int
	 */    
    public $schedulerId;
	
    
    /**
	 * @var int
	 */    
    public $workerId;
	
    
    /**
	 * @var int
	 */    
    public $batchIndex;
	
    
    /**
	 * @var int
	 */    
    public $lastSchedulerId;
	
    
    /**
	 * @var int
	 */    
    public $lastWorkerId;
    
    /**
	 * @var int
	 */    
    public $dc;
    
    /**
     * @var string
     */
    public $jobObjectId;

    /**
     * @var int
     */
	public $jobObjectType;
	
	private static $map_between_objects = array
	(
		"id" ,
		"partnerId" ,
		"createdAt" , "updatedAt" , 
		"entryId" ,
		"jobType" , 
	 	"status" ,  
		"message", "description" , "parentJobId" ,
		"rootJobId", "bulkJobId" , "priority" ,
		"queueTime" , "finishTime" ,  "errType", "errNumber", 
		"dc",
		"lastSchedulerId", "lastWorkerId" , 
		"history",
		"jobObjectId" => "objectId", "jobObjectType" => "objectType"
	);
	
	public function getMapBetweenObjects ( )
	{
		return array_merge ( parent::getMapBetweenObjects() , self::$map_between_objects );
	}
	    
	public function fromStatisticsObject($dbBatchJob, $dbLockObj = null)
	{
		$dbBatchJobLock = BatchJobLockPeer::retrieveByPK($dbBatchJob->getId());
		$this->fromBatchJob($dbBatchJob, $dbBatchJobLock);
		
		if(!($dbBatchJob instanceof BatchJob))
			return $this;
			
		$entry = $dbBatchJob->getEntry(true);
		if($entry)
			$this->entryName = $entry->getName();
		
		return $this;
	}
	    
	public function fromData(BatchJob $dbBatchJob, $dbData)
	{
		if(!$dbData)
			return;
				
		switch(get_class($dbData))
		{
			case 'kConvartableJobData':
				$this->data = new KontorolConvartableJobData();
				break;
				
			case 'kConvertJobData':
				$this->data = new KontorolConvertJobData();
				break;
				
			case 'kConvertProfileJobData':
				$this->data = new KontorolConvertProfileJobData();
				break;
				
			case 'kExtractMediaJobData':
				$this->data = new KontorolExtractMediaJobData();
				break;
				
			case 'kImportJobData':
				$this->data = new KontorolImportJobData();
				break;
				
			case 'kSshImportJobData':
				$this->data = new KontorolSshImportJobData();
				break;
				
			case 'kPostConvertJobData':
				$this->data = new KontorolPostConvertJobData();
				break;
				
			case 'kMailJobData':
				$this->data = new KontorolMailJobData();
				break;
				
			case 'kNotificationJobData':
				$this->data = new KontorolNotificationJobData();
				break;
				
			case 'kBulkDownloadJobData':
				$this->data = new KontorolBulkDownloadJobData();
				break;
				
			case 'kFlattenJobData':
				$this->data = new KontorolFlattenJobData();
				break;
			
			case 'kProvisionJobData':
				$this->data = new KontorolProvisionJobData();
				break;
				
			case 'kAkamaiProvisionJobData':
				$this->data = new KontorolAkamaiProvisionJobData();
				break;	

			case 'kAkamaiUniversalProvisionJobData':
				$this->data = new KontorolAkamaiUniversalProvisionJobData();
				break;
				
			case 'kConvertCollectionJobData':
				$this->data = new KontorolConvertCollectionJobData();
				break;
				
			case 'kStorageExportJobData':
				$this->data = new KontorolStorageExportJobData();
				break;
				
			case 'kAmazonS3StorageExportJobData':
				$this->data = new KontorolAmazonS3StorageExportJobData();
				break;
				
			case 'kMoveCategoryEntriesJobData':
				$this->data = new KontorolMoveCategoryEntriesJobData();
				break;
				
			case 'kStorageDeleteJobData':
				$this->data = new KontorolStorageDeleteJobData();
				break;
				
			case 'kCaptureThumbJobData':
				$this->data = new KontorolCaptureThumbJobData();
				break;
				
			case 'kMoveCategoryEntriesJobData':
			    $this->data = new KontorolMoveCategoryEntriesJobData();
			    break;

			case 'kIndexJobData':
				$this->data = new KontorolIndexJobData();
				break;
				
			case 'kCopyJobData':
				$this->data = new KontorolCopyJobData();
				break;
				
			case 'kDeleteJobData':
				$this->data = new KontorolDeleteJobData();
				break;

			case 'kDeleteFileJobData':
				$this->data = new KontorolDeleteFileJobData();
				break;
				
			case 'kConvertLiveSegmentJobData':
				$this->data = new KontorolConvertLiveSegmentJobData();
				break;
				
			case 'kConcatJobData':
				$this->data = new KontorolConcatJobData();
				break;
				
			case 'kCopyPartnerJobData':
				$this->data = new KontorolCopyPartnerJobData();
				break;
				
			case 'kSyncCategoryPrivacyContextJobData':
				$this->data = new KontorolSyncCategoryPrivacyContextJobData();
				break;
			
			case 'kLiveReportExportJobData':
				$this->data = new KontorolLiveReportExportJobData();
				break;
			
			case 'kRecalculateResponseProfileCacheJobData':
				$this->data = new KontorolRecalculateResponseProfileCacheJobData();
				break;

			case 'kLiveToVodJobData':
				$this->data = new KontorolLiveToVodJobData();
				break;

			case 'kCopyCaptionsJobData':
				$this->data = new KontorolCopyCaptionsJobData();
				break;

			case 'kUsersCsvJobData':
				$this->data = new KontorolUsersCsvJobData();
				break;
			
			case 'kClipConcatJobData':
				$this->data = new KontorolClipConcatJobData();
				break;

			case 'kCopyCuePointsJobData':
				$this->data = new KontorolCopyCuePointsJobData();
				break;

			case 'kMultiClipCopyCuePointsJobData':
				$this->data = new KontorolMultiClipCopyCuePointsJobData();
				break;

			case 'kReportExportJobData':
				$this->data = new KontorolReportExportJobData();
				break;

			case 'kLiveEntryArchiveJobData':
				$this->data = new KontorolLiveEntryArchiveJobData();
				break;

			default:
				if($dbData instanceof kBulkUploadJobData)
				{
					$this->data = KontorolPluginManager::loadObject('KontorolBulkUploadJobData', $dbBatchJob->getJobSubType());
					if(is_null($this->data))
						KontorolLog::err("Unable to init KontorolBulkUploadJobData for sub-type [" . $dbBatchJob->getJobSubType() . "]");
				}
				else if($dbData instanceof kImportJobData)
				{
					$this->data = KontorolPluginManager::loadObject('KontorolImportJobData', get_class($dbData));
					if(is_null($this->data))
						KontorolLog::err("Unable to init KontorolImportJobData for class [" . get_class($dbData) . "]");
				}
				else
				{
					$this->data = KontorolPluginManager::loadObject('KontorolJobData', $this->jobType, array('coreJobSubType' => $dbBatchJob->getJobSubType()));
				}
		}
		
		if(is_null($this->data))
			KontorolLog::err("Unable to init KontorolJobData for job type [{$this->jobType}] sub-type [" . $dbBatchJob->getJobSubType() . "]");
			
		if($this->data)
			$this->data->fromObject($dbData);
	}
	
	public function fromLockObject(BatchJob $dbBatchJob, BatchJobLock $dbBatchJobLock) 
	{
		$this->lockExpiration = $dbBatchJobLock->getExpiration();
		$this->executionAttempts = $dbBatchJobLock->getExecutionAttempts();
		$this->lockVersion = $dbBatchJobLock->getVersion();
		$this->checkAgainTimeout = $dbBatchJobLock->getStartAt(null);
		$this->estimatedEffort = $dbBatchJobLock->getEstimatedEffort();
		
		$this->schedulerId = $dbBatchJobLock->getSchedulerId();
		$this->workerId = $dbBatchJobLock->getWorkerId();
	}

	protected function doFromObject($srcObj, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$lockInfo = $srcObj->getLockInfo();
		if($lockInfo)
		{
			$this->urgency = $lockInfo->getUrgency();
		}
	}

	public function fromBatchJob($dbBatchJob, BatchJobLock $dbBatchJobLock = null) 
	{
		parent::fromObject($dbBatchJob);
		
		$this->queueTime = $dbBatchJob->getQueueTime(null); // to return the timestamp and not string
		$this->finishTime = $dbBatchJob->getFinishTime(null); // to return the timestamp and not string
		
		if(!($dbBatchJob instanceof BatchJob))
			return $this;
			
		$dbData = $dbBatchJob->getData();
		$this->fromData($dbBatchJob, $dbData);
		if($this->data)
			$this->jobSubType = $this->data->fromSubType($dbBatchJob->getJobSubType());
		
		if($dbBatchJobLock) {
			$this->fromLockObject($dbBatchJob, $dbBatchJobLock);
		} else {
			$this->lockVersion = $dbBatchJob->getLockInfo()->getLockVersion();
			$this->estimatedEffort = $dbBatchJob->getLockInfo()->getEstimatedEffort();
		}
		
		return $this;
	}
	
	public function toData(BatchJob $dbBatchJob)
	{
		$dbData = null;
		
		if(is_null($this->jobType))
			$this->jobType = kPluginableEnumsManager::coreToApi('BatchJobType', $dbBatchJob->getJobType());
		
		switch($dbBatchJob->getJobType())
		{
			case KontorolBatchJobType::BULKUPLOAD:
				$dbData = new kBulkUploadJobData();
				if(is_null($this->data))
					$this->data = new KontorolBulkUploadJobData();
				break;
				
			case KontorolBatchJobType::CONVERT:
				$dbData = new kConvertJobData();
				if(is_null($this->data))
					$this->data = new KontorolConvertJobData();
				break;
				
			case KontorolBatchJobType::CONVERT_PROFILE:
				$dbData = new kConvertProfileJobData();
				if(is_null($this->data))
					$this->data = new KontorolConvertProfileJobData();
				break;
				
			case KontorolBatchJobType::EXTRACT_MEDIA:
				$dbData = new kExtractMediaJobData();
				if(is_null($this->data))
					$this->data = new KontorolExtractMediaJobData();
				break;
				
			case KontorolBatchJobType::IMPORT:
				$dbData = new kImportJobData();
				if(is_null($this->data))
					$this->data = new KontorolImportJobData();
				break;
				
			case KontorolBatchJobType::POSTCONVERT:
				$dbData = new kPostConvertJobData();
				if(is_null($this->data))
					$this->data = new KontorolPostConvertJobData();
				break;
				
			case KontorolBatchJobType::MAIL:
				$dbData = new kMailJobData();
				if(is_null($this->data))
					$this->data = new KontorolMailJobData();
				break;
				
			case KontorolBatchJobType::NOTIFICATION:
				$dbData = new kNotificationJobData();
				if(is_null($this->data))
					$this->data = new KontorolNotificationJobData();
				break;
				
			case KontorolBatchJobType::BULKDOWNLOAD:
				$dbData = new kBulkDownloadJobData();
				if(is_null($this->data))
					$this->data = new KontorolBulkDownloadJobData();
				break;
				
			case KontorolBatchJobType::FLATTEN:
				$dbData = new kFlattenJobData();
				if(is_null($this->data))
					$this->data = new KontorolFlattenJobData();
				break;
				
			case KontorolBatchJobType::PROVISION_PROVIDE:
			case KontorolBatchJobType::PROVISION_DELETE:
				$jobSubType = $dbBatchJob->getJobSubType();
				$dbData = kAkamaiProvisionJobData::getInstance($jobSubType);
				if(is_null($this->data))
					$this->data = KontorolProvisionJobData::getJobDataInstance($jobSubType);

				break;
				
			case KontorolBatchJobType::CONVERT_COLLECTION:
				$dbData = new kConvertCollectionJobData();
				if(is_null($this->data))
					$this->data = new KontorolConvertCollectionJobData();
				break;
				
			case KontorolBatchJobType::STORAGE_EXPORT:
				$dbData = new kStorageExportJobData();
				if(is_null($this->data))
					$this->data = new KontorolStorageExportJobData();
				break;
				
			case KontorolBatchJobType::MOVE_CATEGORY_ENTRIES:
				$dbData = new kMoveCategoryEntriesJobData();
				if(is_null($this->data))
					$this->data = new KontorolMoveCategoryEntriesJobData();
				break;
				
			case KontorolBatchJobType::STORAGE_DELETE:
				$dbData = new kStorageDeleteJobData();
				if(is_null($this->data))
					$this->data = new KontorolStorageDeleteJobData();
				break;
				
			case KontorolBatchJobType::CAPTURE_THUMB:
				$dbData = new kCaptureThumbJobData();
				if(is_null($this->data))
					$this->data = new KontorolCaptureThumbJobData();
				break;
				
			case KontorolBatchJobType::INDEX:
				$dbData = new kIndexJobData();
				if(is_null($this->data))
					$this->data = new KontorolIndexJobData();
				break;
				
			case KontorolBatchJobType::COPY:
				$dbData = new kCopyJobData();
				if(is_null($this->data))
					$this->data = new KontorolCopyJobData();
				break;
				
			case KontorolBatchJobType::DELETE:
				$dbData = new kDeleteJobData();
				if(is_null($this->data))
					$this->data = new KontorolDeleteJobData();
				break;

			case KontorolBatchJobType::DELETE_FILE:
				$dbData = new kDeleteFileJobData();
				if(is_null($this->data))
					$this->data = new KontorolDeleteFileJobData();
				break;
				
			case KontorolBatchJobType::CONVERT_LIVE_SEGMENT:
				$dbData = new kConvertLiveSegmentJobData();
				if(is_null($this->data))
					$this->data = new KontorolConvertLiveSegmentJobData();
				break;
				
			case KontorolBatchJobType::CONCAT:
				$dbData = new kConcatJobData();
				if(is_null($this->data))
					$this->data = new KontorolConcatJobData();
				break;
					
			case KontorolBatchJobType::COPY_PARTNER:
				$dbData = new kCopyPartnerJobData();
				if(is_null($this->data))
					$this->data = new KontorolCopyPartnerJobData();
				break;
					
			case KontorolBatchJobType::RECALCULATE_CACHE:
				switch($dbBatchJob->getJobSubType())
				{
					case RecalculateCacheType::RESPONSE_PROFILE:
						$dbData = new kRecalculateResponseProfileCacheJobData();
						if(is_null($this->data))
							$this->data = new KontorolRecalculateResponseProfileCacheJobData();
						break;
				}
				break;
			
			case KontorolBatchJobType::LIVE_TO_VOD:
				$dbData = new kLiveToVodJobData();
				if(is_null($this->data))
					$this->data = new KontorolLiveToVodJobData();
 				break;


			case KontorolBatchJobType::CLIP_CONCAT:
				$dbData = new kClipConcatJobData();
				if(is_null($this->data))
					$this->data = new KontorolClipConcatJobData();
				break;

			case KontorolBatchJobType::COPY_CUE_POINTS:
				switch ($dbBatchJob->getJobSubType()) {
					case CopyCuePointJobType::MULTI_CLIP:
						$dbData = new kMultiClipCopyCuePointsJobData();
						if(is_null($this->data))
							$this->data = new KontorolMultiClipCopyCuePointsJobData();
						break;
					case CopyCuePointJobType::LIVE_CLIPPING:
						$dbData = new kLiveToVodJobData();
						if(is_null($this->data))
							$this->data = new KontorolLiveToVodJobData();
						break;
					default:
						$dbData = new kCopyCuePointsJobData();
						if(is_null($this->data))
							$this->data = new KontorolCopyCuePointsJobData();
						break;
				}
				break;

			case KontorolBatchJobType::COPY_CAPTIONS:
				$dbData = new kCopyCaptionsJobData();
				if(is_null($this->data))
					$this->data = new KontorolCopyCaptionsJobData();
				break;

			case KontorolBatchJobType::USERS_CSV:
				$dbData = new kUsersCsvJobData();
				if(is_null($this->data))
					$this->data = new KontorolUsersCsvJobData();
				break;

			case KontorolBatchJobType::LIVE_ENTRY_ARCHIVE:
				$dbData = new kLiveEntryArchiveJobData();
				if(is_null($this->data))
					$this->data = new KontorolLiveEntryArchiveJobData();
				break;

			default:
				$dbData = KontorolPluginManager::loadObject('kJobData', $dbBatchJob->getJobType());
				if(is_null($this->data)) {
					$this->data = KontorolPluginManager::loadObject('KontorolJobData', $this->jobType);
				}
		}
		
		if(is_null($dbBatchJob->getData()))
			$dbBatchJob->setData($dbData);
	
		if($this->data instanceof KontorolJobData)
		{
			$dbData = $this->data->toObject($dbBatchJob->getData());
			$dbBatchJob->setData($dbData);
		}
		
		return $dbData;
	}
	
	public function toObject($dbBatchJob = null, $props_to_skip = array())
	{
		if(is_null($dbBatchJob))
			$dbBatchJob = new BatchJob();

		$dbBatchJob = parent::toObject($dbBatchJob);
		if($this->abort)
			$dbBatchJob->setExecutionStatus(BatchJobExecutionStatus::ABORTED);
		
		if (!is_null($this->data))
		    $this->toData($dbBatchJob);
		if(!is_null($this->jobSubType) && $this->data instanceof KontorolJobData)
			$dbBatchJob->setJobSubType($this->data->toSubType($this->jobSubType));
		
		return $dbBatchJob;
	}   
	
	public function getExtraFilters()
	{
		return array();
	}
	
	public function getFilterDocs()
	{
		return array();
	} 
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolDeleteFileJobData extends KontorolJobData
{
	/**
	 * @var string
	 */
	public $localFileSyncPath;
	
	/* (non-PHPdoc)
	 * @see KontorolObject::fromObject($source_object)
	 */
	public function doFromObject($sourceObject, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$this->localFileSyncPath = $sourceObject->getLocalFileSyncPath();
		parent::doFromObject($sourceObject, $responseProfile);
	}
	
}

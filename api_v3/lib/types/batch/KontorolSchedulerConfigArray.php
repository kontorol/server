<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolSchedulerConfigArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolSchedulerConfigArray();
		foreach ( $arr as $obj )
		{
			$nObj = new KontorolSchedulerConfig();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct( )
	{
		return parent::__construct ( "KontorolSchedulerConfig" );
	}
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolBatchJobArray extends KontorolTypedArray
{
	public static function fromStatisticsBatchJobArray ( $arr )
	{
		$newArr = new KontorolBatchJobArray();
		if ( is_array ( $arr ) )
		{
			foreach ( $arr as $obj )
			{
				$nObj = new KontorolBatchJob();
				$nObj->fromStatisticsObject($obj);
				$newArr[] = $nObj;
			}
		}
		
		return $newArr;
	}
	
	public static function fromBatchJobArray ( $arr )
	{
		$newArr = new KontorolBatchJobArray();
		if ( is_array ( $arr ) )
		{
			foreach ( $arr as $obj )
			{
				$nObj = new KontorolBatchJob();
				$nObj->fromBatchJob($obj);
				$newArr[] = $nObj;
			}
		}
		
		return $newArr;
	}
	
	public function __construct( )
	{
		return parent::__construct ( "KontorolBatchJob" );
	}
}
?>

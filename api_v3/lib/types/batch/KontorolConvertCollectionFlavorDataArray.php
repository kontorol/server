<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolConvertCollectionFlavorDataArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolConvertCollectionFlavorDataArray();
		foreach ( $arr as $obj )
		{
			$nObj = new KontorolConvertCollectionFlavorData();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct( )
	{
		return parent::__construct ( "KontorolConvertCollectionFlavorData" );
	}
}

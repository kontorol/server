<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolFullStatusResponse extends KontorolObject
{
	/**
	 * The status of all queues on the server
	 * 
	 * @var KontorolBatchQueuesStatusArray
	 */
	public $queuesStatus;
	
	
	/**
	 * Array of all schedulers
	 * 
	 * @var KontorolSchedulerArray
	 */
	public $schedulers;
}

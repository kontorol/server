<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolFreeJobResponse extends KontorolObject
{
	/**
	 * @var KontorolBatchJob
	 * @readonly 
	 */
	public $job;

	/**
	 * @var KontorolBatchJobType
	 * @readonly 
	 */
    public $jobType;
    
	/**
	 * @var int
	 * @readonly 
	 */
    public $queueSize;
}

?>

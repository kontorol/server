<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolDestFileSyncDescriptor extends KontorolFileSyncDescriptor
{
	public function toObject($dbObject = null, $skip = array())
	{
		if(!$dbObject)
			$dbObject = new kDestFileSyncDescriptor();
			
		return parent::toObject($dbObject, $skip);
	}
}

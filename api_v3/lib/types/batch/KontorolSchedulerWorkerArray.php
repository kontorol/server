<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolSchedulerWorkerArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolSchedulerWorkerArray();
		foreach ( $arr as $obj )
		{
			$nObj = new KontorolSchedulerWorker();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public static function statusFromSchedulerWorkerArray( $arr )
	{
		$newArr = new KontorolSchedulerWorkerArray();
		foreach ( $arr as $obj )
		{
			$nObj = new KontorolSchedulerWorker();
			$nObj->statusFromObject($obj);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct( )
	{
		return parent::__construct ( "KontorolSchedulerWorker" );
	}
}

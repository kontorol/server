<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolControlPanelCommandArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolControlPanelCommandArray();
		foreach ( $arr as $obj )
		{
			$nObj = new KontorolControlPanelCommand();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct( )
	{
		return parent::__construct ( "KontorolControlPanelCommand" );
	}
}

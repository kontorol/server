<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolWorkerQueueFilter extends KontorolObject
{
	/**
	 * @var int
	 */
	public $schedulerId;
	
    
	/**
	 * @var int
	 */
	public $workerId;
	
    
	/**
	 * @var KontorolBatchJobType
	 */
	public $jobType;
	
    
	/**
	 * @var KontorolBatchJobFilter
	 */
	public $filter;
	
    
}


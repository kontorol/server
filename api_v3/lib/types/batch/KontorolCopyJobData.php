<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolCopyJobData extends KontorolJobData
{
	/**
	 * The filter should return the list of objects that need to be copied.
	 * @var KontorolFilter
	 */
	public $filter;
	
	/**
	 * Indicates the last id that copied, used when the batch crached, to re-run from the last crash point.
	 * @var int
	 */
	public $lastCopyId;
	
	/**
	 * Template object to overwrite attributes on the copied object
	 * @var KontorolObject
	 */
	public $templateObject;
	
	private static $map_between_objects = array
	(
		"lastCopyId" ,
	);

	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects ( )
	{
		return array_merge ( parent::getMapBetweenObjects() , self::$map_between_objects );
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbData = null, $props_to_skip = array()) 
	{
		if(is_null($dbData))
			$dbData = new kCopyJobData();
			
		$dbData->setTemplateObject($this->templateObject->toObject());
		
		return parent::toObject($dbData, $props_to_skip);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::fromObject()
	 */
	public function doFromObject($dbData, KontorolDetachedResponseProfile $responseProfile = null)
	{
		/* @var $dbData kCopyJobData */
		$filter = $dbData->getFilter();
		$filterType = get_class($filter);
		switch($filterType)
		{
			case 'entryFilter':
				$this->filter = new KontorolBaseEntryFilter();
				$this->templateObject = new KontorolBaseEntry();
				break;
				
			case 'categoryFilter':
				$this->filter = new KontorolCategoryFilter();
				$this->templateObject = new KontorolCategory();
				break;
				
			case 'categoryEntryFilter':
				$this->filter = new KontorolCategoryEntryFilter();
				$this->templateObject = new KontorolCategoryEntry();
				break;
				
			case 'categoryKuserFilter':
				$this->filter = new KontorolCategoryUserFilter();
				$this->templateObject = new KontorolCategoryUser();
				break;
				
			default:
				$this->filter = KontorolPluginManager::loadObject('KontorolFilter', $filterType);
		}
		if($this->filter)
			$this->filter->fromObject($filter);
		
		if($this->templateObject)
			$this->templateObject->fromObject($dbData->getTemplateObject());
		
		parent::doFromObject($dbData, $responseProfile);
	}
}

<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolControlPanelCommandFilter extends KontorolControlPanelCommandBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new ControlPanelCommandFilter();
	}
}

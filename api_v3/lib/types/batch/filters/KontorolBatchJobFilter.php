<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolBatchJobFilter extends KontorolBatchJobBaseFilter
{
	protected function toDynamicJobSubTypeValues($jobType, $jobSubTypeIn)
	{
		$data = new KontorolJobData();
		switch($jobType)
		{
			case KontorolBatchJobType::BULKUPLOAD:
				$data = new KontorolBulkUploadJobData();
				break;
				
			case KontorolBatchJobType::CONVERT:
				$data = new KontorolConvertJobData();
				break;
				
			case KontorolBatchJobType::CONVERT_PROFILE:
				$data = new KontorolConvertProfileJobData();
				break;
				
			case KontorolBatchJobType::EXTRACT_MEDIA:
				$data = new KontorolExtractMediaJobData();
				break;
				
			case KontorolBatchJobType::IMPORT:
				$data = new KontorolImportJobData();
				break;
				
			case KontorolBatchJobType::POSTCONVERT:
				$data = new KontorolPostConvertJobData();
				break;
				
			case KontorolBatchJobType::MAIL:
				$data = new KontorolMailJobData();
				break;
				
			case KontorolBatchJobType::NOTIFICATION:
				$data = new KontorolNotificationJobData();
				break;
				
			case KontorolBatchJobType::BULKDOWNLOAD:
				$data = new KontorolBulkDownloadJobData();
				break;
				
			case KontorolBatchJobType::FLATTEN:
				$data = new KontorolFlattenJobData();
				break;
				
			case KontorolBatchJobType::PROVISION_PROVIDE:
			case KontorolBatchJobType::PROVISION_DELETE:
				$data = new KontorolProvisionJobData();
				break;
				
			case KontorolBatchJobType::CONVERT_COLLECTION:
				$data = new KontorolConvertCollectionJobData();
				break;
				
			case KontorolBatchJobType::STORAGE_EXPORT:
				$data = new KontorolStorageExportJobData();
				break;
				
			case KontorolBatchJobType::STORAGE_DELETE:
				$data = new KontorolStorageDeleteJobData();
				break;
				
			case KontorolBatchJobType::INDEX:
				$data = new KontorolIndexJobData();
				break;
				
			case KontorolBatchJobType::COPY:
				$data = new KontorolCopyJobData();
				break;
				
			case KontorolBatchJobType::DELETE:
				$data = new KontorolDeleteJobData();
				break;

			case KontorolBatchJobType::DELETE_FILE:
				$data = new KontorolDeleteFileJobData();
				break;
				
			case KontorolBatchJobType::MOVE_CATEGORY_ENTRIES:
				$data = new KontorolMoveCategoryEntriesJobData();
				break;
				
			default:
				$data = KontorolPluginManager::loadObject('KontorolJobData', $jobType);
		}
		
		if(!$data)
		{
			KontorolLog::err("Data type not found for job type [$jobType]");
			return null;
		}
			
		$jobSubTypeArray = explode(baseObjectFilter::IN_SEPARATOR, $jobSubTypeIn);
		$dbJobSubTypeArray = array();
		foreach($jobSubTypeArray as $jobSubType)
			$dbJobSubTypeArray[] = $data->toSubType($jobSubType);
			
		$dbJobSubType = implode(baseObjectFilter::IN_SEPARATOR, $dbJobSubTypeArray);
		return $dbJobSubType;
	}

	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new BatchJobFilter();
	}
	
	/**
	 * @param int $jobType
	 * @return BatchJobFilter
	 */
	public function toFilter($jobType = null)
	{
		$batchJobFilter = $this->toObject(new BatchJobFilter(false));
		
		if(!is_null($jobType) && !is_null($this->jobSubTypeIn))
		{
			$jobSubTypeIn = $this->toDynamicJobSubTypeValues($jobType, $this->jobSubTypeIn);
			$batchJobFilter->set('_in_job_sub_type', $jobSubTypeIn);
		}
	
		if(!is_null($jobType) && !is_null($this->jobSubTypeNotIn))
		{
			$jobSubTypeNotIn = $this->toDynamicJobSubTypeValues($jobType, $this->jobSubTypeNotIn);
			$batchJobFilter->set('_notin_job_sub_type', $jobSubTypeNotIn);
		}
		
		return $batchJobFilter;
	}
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolDeleteJobData extends KontorolJobData
{
	/**
	 * The filter should return the list of objects that need to be deleted.
	 * @var KontorolFilter
	 */
	public $filter;
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbData = null, $props_to_skip = array()) 
	{
		if(is_null($dbData))
			$dbData = new kDeleteJobData();
			
		return parent::toObject($dbData, $props_to_skip);
	}
	
	public function doFromObject($dbData, KontorolDetachedResponseProfile $responseProfile = null)
	{
		/* @var $dbData kDeleteJobData */
		$filter = $dbData->getFilter();
		$filterType = get_class($filter);
		switch($filterType)
		{
			case 'categoryEntryFilter':
				$this->filter = new KontorolCategoryEntryFilter();
				break;
				
			case 'categoryKuserFilter':
				$this->filter = new KontorolCategoryUserFilter();
				break;

			case 'KuserKgroupFilter':
				$this->filter = new KontorolGroupUserFilter();
				break;
				
			case 'categoryFilter':
				$this->filter = new KontorolCategoryFilter();
 				break;
				
			case 'UserEntryFilter':
				$this->filter = new KontorolUserEntryFilter();
 				break;
			
			default:
				$this->filter = KontorolPluginManager::loadObject('KontorolFilter', $filterType);
		}
		if($this->filter)
			$this->filter->fromObject($filter);
		
		parent::doFromObject($dbData, $responseProfile);
	}
}

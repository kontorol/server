<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolStorageExportJobData extends KontorolStorageJobData
{
    
	/**
	 * @var bool
	 */   	
    public $force;
    
    /**
	 * @var bool
	 */   	
    public $createLink;

	/**
	 * @var string
	 */
	public $assetId;

	/**
	 * @var string
	 */
	public $externalUrl;

	private static $map_between_objects = array
	(
	    "force",
		"createLink",
		"assetId",
		"externalUrl",
	);

	public function getMapBetweenObjects ( )
	{
		return array_merge ( parent::getMapBetweenObjects() , self::$map_between_objects );
	}

	
	public function toObject($dbData = null, $props_to_skip = array()) 
	{
		if(is_null($dbData))
			$dbData = new kStorageExportJobData();
			
		return parent::toObject($dbData);
	}
	
	/**
	 * @param string $subType
	 * @return int
	 */
	public function toSubType($subType)
	{
		switch ($subType) {
			case KontorolStorageProfileProtocol::FTP:
            case KontorolStorageProfileProtocol::SFTP:
            case KontorolStorageProfileProtocol::SCP:
            case KontorolStorageProfileProtocol::S3:
            case KontorolStorageProfileProtocol::KONTOROL_DC:
            case KontorolStorageProfileProtocol::LOCAL:
                return $subType;                  	
			default:
				return kPluginableEnumsManager::apiToCore('KontorolStorageProfileProtocol', $subType);
		}
	}
	
	/**
	 * @param int $subType
	 * @return string
	 */
	public function fromSubType($subType)
	{
		switch ($subType) {
            case StorageProfileProtocol::FTP:
            case StorageProfileProtocol::SFTP:
            case StorageProfileProtocol::SCP:
            case StorageProfileProtocol::S3:
            case StorageProfileProtocol::KONTOROL_DC:
          	case StorageProfileProtocol::LOCAL:
                return $subType;                    
            default:
                return kPluginableEnumsManager::coreToApi('StorageProfileProtocol', $subType);
        }
	}
}

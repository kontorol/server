<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolBatchJobResponse extends KontorolObject
{
	/**
	 * The main batch job
	 * 
	 * @var KontorolBatchJob
	 */
	public $batchJob;
	
	
	/**
	 * All batch jobs that reference the main job as root
	 * 
	 * @var KontorolBatchJobArray
	 */
	public $childBatchJobs;
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolSchedulerStatusResponse extends KontorolObject
{
	/**
	 * The status of all queues on the server
	 * 
	 * @var KontorolBatchQueuesStatusArray
	 */
	public $queuesStatus;
	
	
	/**
	 * The commands that sent from the control panel
	 * 
	 * @var KontorolControlPanelCommandArray
	 */
	public $controlPanelCommands;
	
	
	/**
	 * The configuration that sent from the control panel
	 * 
	 * @var KontorolSchedulerConfigArray
	 */
	public $schedulerConfigs;
}

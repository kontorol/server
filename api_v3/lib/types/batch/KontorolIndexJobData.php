<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolIndexJobData extends KontorolJobData
{
	/**
	 * The filter should return the list of objects that need to be reindexed.
	 * @var KontorolFilter
	 */
	public $filter;
	
	/**
	 * Indicates the last id that reindexed, used when the batch crached, to re-run from the last crash point.
	 * @var int
	 */
	public $lastIndexId;

	/**
	 * Indicates the last depth that reindexed, used when the batch crached, to re-run from the last crash point.
	 * @var int
	 */
	public $lastIndexDepth;
	
	/**
	 * Indicates that the object columns and attributes values should be recalculated before reindexed.
	 * @var bool
	 */
	public $shouldUpdate;
	
	private static $map_between_objects = array
	(
		"lastIndexId" ,
		"shouldUpdate" ,
		"lastIndexDepth",
	);

	public function getMapBetweenObjects ( )
	{
		return array_merge ( parent::getMapBetweenObjects() , self::$map_between_objects );
	}

	/**
	 * @param string $subType is the provider type
	 * @return int
	 */
	public function toSubType($subType)
	{
		return kPluginableEnumsManager::apiToCore('IndexObjectType', $subType);
	}

	/**
	 * @param int $subType
	 * @return string
	 */
	public function fromSubType($subType)
	{
		return kPluginableEnumsManager::coreToApi('IndexObjectType', $subType);
	}
	
	public function toObject($dbData = null, $props_to_skip = array()) 
	{
		if(is_null($dbData))
			$dbData = new kIndexJobData();
			
		return parent::toObject($dbData, $props_to_skip);
	}
	
	public function doFromObject($dbData, KontorolDetachedResponseProfile $responseProfile = null)
	{
		/* @var $dbData kIndexJobData */
		$filter = $dbData->getFilter();
		$filterType = get_class($filter);
		switch($filterType)
		{
			case 'entryFilter':
				$this->filter = new KontorolBaseEntryFilter();
				break;
				
			case 'categoryFilter':
				$this->filter = new KontorolCategoryFilter();
				break;
			
			case 'categoryEntryFilter':
				$this->filter = new KontorolCategoryEntryFilter();
				break;
				
			case 'categoryKuserFilter':
				$this->filter = new KontorolCategoryUserFilter();
				break;
			
			case 'kuserFilter':
				$this->filter = new KontorolUserFilter();
				break;
				
			default:
				$this->filter = KontorolPluginManager::loadObject('KontorolFilter', $filterType);
		}
		if($this->filter)
			$this->filter->fromObject($filter);
			
		parent::doFromObject($dbData, $responseProfile);
	}
}

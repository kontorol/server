<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolConvartableJobData extends KontorolJobData
{
	/**
	 * @var string
	 * @deprecated
	 */
	public $srcFileSyncLocalPath;
	
	/**
	 * The translated path as used by the scheduler
	 * @var string
	 * @deprecated
	 */
	public $actualSrcFileSyncLocalPath;
	
	/**
	 * @var string
	 * @deprecated
	 */
	public $srcFileSyncRemoteUrl;

	/**
	 * 
	 * @var KontorolSourceFileSyncDescriptorArray
	 */
	public $srcFileSyncs;
	
	/**
	 * @var int
	 */
	public $engineVersion;
	
	/**
	 * @var int
	 */
	public $flavorParamsOutputId;
	
	/**
	 * @var KontorolFlavorParamsOutput
	 */
	public $flavorParamsOutput;
	
	/**
	 * @var int
	 */
	public $mediaInfoId;
	
	/**
	 * @var int
	 */
	public $currentOperationSet;
	
	/**
	 * @var int
	 */
	public $currentOperationIndex;
	
	/**
	 * @var KontorolKeyValueArray
	 */
	public $pluginData;
	
	private static $map_between_objects = array
	(
		"srcFileSyncs",
		"engineVersion" ,
		"mediaInfoId" ,
		"flavorParamsOutputId" ,
		"currentOperationSet" ,
		"currentOperationIndex" ,
		"pluginData",
	);


	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects ( )
	{
		return array_merge ( parent::getMapBetweenObjects() , self::$map_between_objects );
	}
	    
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject($object_to_fill, $props_to_skip)
	 */
	public function toObject(  $dbConvartableJobData = null, $props_to_skip = array()) 
	{
		if(is_null($dbConvartableJobData))
			$dbConvartableJobData = new kConvartableJobData();
			
		return parent::toObject($dbConvartableJobData, $props_to_skip);
	}
	    
	/* (non-PHPdoc)
	 * @see KontorolObject::fromObject($srcObj)
	 */
	public function doFromObject($srcObj, KontorolDetachedResponseProfile $responseProfile = null)
	{
		/* @var $srcObj kConvartableJobData */
		$srcObj->migrateOldSerializedData();
		parent::doFromObject($srcObj, $responseProfile);
	}
}

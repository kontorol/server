<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolDestFileSyncDescriptorArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolDestFileSyncDescriptorArray();
		if ($arr == null)
			return $newArr;
		foreach ($arr as $obj)
		{
    		$nObj = new KontorolDestFileSyncDescriptor();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolDestFileSyncDescriptor");
	}
}

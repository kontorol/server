<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolNotificationJobData extends KontorolJobData
{
	/**
	 * @var string
	 */
	public $userId;
	
	/**
	 * @var KontorolNotificationType
	 */
	public $type;
	
	/**
	 * @var string
	 */
	public $typeAsString;
	
	/**
	 * @var string
	 */
	public $objectId;
	
	/**
	 * @var KontorolNotificationStatus
	 */
	public $status;
	
	/**
	 * @var string
	 */
	public $data;
	
	/**
	 * @var int
	 */
	public $numberOfAttempts;
	
	/**
	 * @var string
	 */
	public $notificationResult;
	
	/**
	 * @var KontorolNotificationObjectType
	 */
	public $objType;
	
	private static $map_between_objects = array("userId", "type", "typeAsString", "objectId", "data", "numberOfAttempts", "notificationResult", "objType");
	
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}
	
	public function toObject($dbData = null, $props_to_skip = array())
	{
		if(is_null($dbData))
			$dbData = new kNotificationJobData();
		
		return parent::toObject($dbData, $props_to_skip);
	}
}

?>

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolSchedulerArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolSchedulerArray();
		foreach ( $arr as $obj )
		{
			$nObj = new KontorolScheduler();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public static function statusFromSchedulerArray( $arr )
	{
		$newArr = new KontorolSchedulerArray();
		foreach ( $arr as $obj )
		{
			$nObj = new KontorolScheduler();
			$nObj->statusFromObject($obj);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct( )
	{
		return parent::__construct ( "KontorolScheduler" );
	}
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolBatchQueuesStatusArray extends KontorolTypedArray
{
	public static function fromBatchQueuesStatusArray($arr)
	{
		$newArr = new KontorolBatchQueuesStatusArray();
		foreach ( $arr as $obj )
		{
			$nObj = new KontorolBatchQueuesStatus();
			
			$nObj->jobType = $obj['JOB_TYPE'];
			$nObj->typeName = BatchJob::getTypeName($nObj->jobType);
			$nObj->size = $obj['JOB_TYPE_COUNT'];
			
			if(isset($obj['CREATED_AT_AVG']))
				$nObj->waitTime = $obj['CREATED_AT_AVG'];
			
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct( )
	{
		return parent::__construct ( "KontorolBatchQueuesStatus" );
	}
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolWidgetArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolWidgetArray();
		foreach ( $arr as $obj )
		{
			$nObj = new KontorolWidget();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct( )
	{
		return parent::__construct ( "KontorolWidget" );
	}
}

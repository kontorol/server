<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolUploadTokenArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolUploadTokenArray();
		foreach($arr as $obj)
		{
			$nObj = new KontorolUploadToken();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct()
	{
		return parent::__construct("KontorolUploadToken");
	}
}

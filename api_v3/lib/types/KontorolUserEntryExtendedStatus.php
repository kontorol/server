<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolUserEntryExtendedStatus extends KontorolDynamicEnum implements UserEntryExtendedStatus
{
	public static function getEnumClass()
	{
		return 'UserEntryExtendedStatus';
	}
}

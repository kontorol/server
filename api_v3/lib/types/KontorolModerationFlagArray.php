<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolModerationFlagArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr = null, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolModerationFlagArray();
		foreach($arr as $obj)
		{
			$nObj = new KontorolModerationFlag();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct()
	{
		return parent::__construct("KontorolModerationFlag");
	}
}

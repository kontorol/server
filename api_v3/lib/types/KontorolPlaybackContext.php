<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolPlaybackContext extends KontorolObject{

	/**
	 * @var KontorolPlaybackSourceArray
	 */
	public $sources;

	/**
	 * @var KontorolCaptionPlaybackPluginDataArray
	 */
	public $playbackCaptions;

	/**
	 * @var KontorolFlavorAssetArray
	 */
	public $flavorAssets;

	/**
	 * Array of actions as received from the rules that invalidated
	 * @var KontorolRuleActionArray
	 */
	public $actions;

	/**
	 * Array of actions as received from the rules that invalidated
	 * @var KontorolAccessControlMessageArray
	 */
	public $messages;

	/**
	* @var KontorolTypedArray
	*/
	public $bumperData;

	private static $mapBetweenObjects = array
	(
		'playbackCaptions',
		'flavorAssets',
		'sources',
		'messages',
		'bumperData',
	);

	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$mapBetweenObjects);
	}
}

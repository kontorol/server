<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolBulkUploadResultVendorCatalogItem extends KontorolBulkUploadResult
{
	/**
	 * @var int
	 */
	public $vendorCatalogItemId;

	/**
	 * @var int
	 */
	public $vendorPartnerId;

	/**
	 * @var string
	 */
	public $name;

	/**
	 * @var string
	 */
	public $systemName;

	/**
	 * @var KontorolVendorServiceFeature
	 */
	public $serviceFeature;

	/**
	 * @var KontorolVendorServiceType
	 */
	public $serviceType;

	/**
	 * @var KontorolVendorServiceTurnAroundTime
	 */
	public $turnAroundTime;

	/**
	 * @var KontorolCatalogItemLanguage
	 */
	public $sourceLanguage;

	/**
	 * @var KontorolCatalogItemLanguage
	 */
	public $targetLanguage;

	/**
	 * @var KontorolVendorCatalogItemOutputFormat
	 */
	public $outputFormat;

	/**
	 * @var KontorolNullableBoolean
	 */
	public $enableSpeakerId;

	/**
	 * @var int
	 */
	public $fixedPriceAddons;

	/**
	 * @var KontorolVendorCatalogItemPricing
	 */
	public $pricing;

	/**
	 * @var int
	 */
	public $flavorParamsId;

	/**
	 * @var int
	 */
	public $clearAudioFlavorParamsId;

	private static $mapBetweenObjects = array
	(
		'vendorCatalogItemId',
		'vendorPartnerId',
		'name',
		'systemName',
		'serviceFeature',
		'serviceType',
		'turnAroundTime',
		'sourceLanguage',
		'targetLanguage',
		'outputFormat',
		'enableSpeakerId',
		'fixedPriceAddons',
		'pricing',
		'flavorParamsId',
		'clearAudioFlavorParamsId'
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$mapBetweenObjects);
	}

	public function toInsertableObject ( $object_to_fill = null , $props_to_skip = array() )
	{
		return parent::toInsertableObject(new BulkUploadResultVendorCatalogItem(), $props_to_skip);
	}

	/* (non-PHPdoc)
     * @see KontorolObject::toObject()
     */
	public function toObject($object_to_fill = null, $props_to_skip = array())
	{
		if ($this->vendorCatalogItemId)
		{
			$this->objectId = $this->vendorCatalogItemId;
		}
		return parent::toObject($object_to_fill, $props_to_skip);
	}

}

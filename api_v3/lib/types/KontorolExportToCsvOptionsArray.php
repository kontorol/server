<?php

/**
 * @package api
 * @subpackage objects
 */
class KontorolExportToCsvOptionsArray extends KontorolTypedArray
{
	/**
	 * @param array                          $arr
	 * @param KontorolDetachedResponseProfile $responseProfile
	 * @return KontorolExportToCsvOptionsArray
	 * @throws KontorolClientException
	 */
	public static function fromDbArray(array $arr = null, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolExportToCsvOptionsArray();
		foreach ($arr as $obj)
		{
			$nObj = new KontorolExportToCsvOptions();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		return $newArr;
	}

	public function __construct()
	{
		parent::__construct("KontorolExportToCsvOptions");
	}
}

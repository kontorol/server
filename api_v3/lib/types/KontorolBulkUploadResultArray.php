<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolBulkUploadResultArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolBulkUploadResultArray();
		foreach ( $arr as $obj )
		{
			$nObj = new KontorolBulkUploadResult();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct( )
	{
		return parent::__construct ( "KontorolBulkUploadResult" );
	}
}

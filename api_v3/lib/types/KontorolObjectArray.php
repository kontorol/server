<?php

/**
 * @package api
 * @subpackage objects
 */
class KontorolObjectArray extends KontorolTypedArray
{
	public function __construct()
	{
		parent::__construct('KontorolObject');
	}
}

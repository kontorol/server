<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolApiExceptionArg extends KontorolObject
{
	/**
	 * @var string
	 */
	public $name;
	
	/**
	 * @var string
	 */
	public $value;
}

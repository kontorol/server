<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolFileAssetStatus extends KontorolDynamicEnum implements FileAssetStatus
{
	public static function getEnumClass()
	{
		return 'FileAssetStatus';
	}
}

<?php
/**
 * @package api
 * @subpackage filters.enum
 */
class KontorolFileAssetObjectType extends KontorolDynamicEnum implements FileAssetObjectType
{
	public static function getEnumClass()
	{
		return 'FileAssetObjectType';
	}
}

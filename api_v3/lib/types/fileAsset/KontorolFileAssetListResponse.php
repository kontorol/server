<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolFileAssetListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolFileAssetArray
	 * @readonly
	 */
	public $objects;
}

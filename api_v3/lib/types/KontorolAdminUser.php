<?php
/**
 * @package api
 * @subpackage objects
 * @deprecated
 */
class KontorolAdminUser extends KontorolUser
{
	// class exists for backward compatibility only
	// should function the same as a KontorolUser with isAdmin=true
}

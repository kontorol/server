<?php

/**
 * @package api
 * @subpackage enum
 */
class KontorolCloneComponentSelectorType extends KontorolDynamicEnum implements CloneComponentSelectorType
{
    public static function getEnumClass()
    {
        return 'CloneComponentSelectorType';
    }
}

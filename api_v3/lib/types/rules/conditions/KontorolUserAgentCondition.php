<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolUserAgentCondition extends KontorolRegexCondition
{
	/**
	 * Init object type
	 */
	public function __construct() 
	{
		$this->type = ConditionType::USER_AGENT;
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbObject = null, $skip = array())
	{
		if(!$dbObject)
			$dbObject = new kUserAgentCondition();
			
		return parent::toObject($dbObject, $skip);
	}
}

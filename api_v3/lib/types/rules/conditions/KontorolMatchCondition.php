<?php
/**
 * @package api
 * @subpackage objects
 * @abstract
 */
abstract class KontorolMatchCondition extends KontorolCondition
{
	/**
	 * @var KontorolStringValueArray
	 */
	public $values;
	
	/**
	 * @var KontorolMatchConditionType
	 */
	public $matchType;
	
	private static $mapBetweenObjects = array
	(
		'values',
		'matchType',
	);
	
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$mapBetweenObjects);
	}
}

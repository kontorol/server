<?php
/**
 * @package api
 * @subpackage objects
 * @abstract
 */
abstract class KontorolCompareCondition extends KontorolCondition
{
	/**
	 * Value to evaluate against the field and operator
	 * @var KontorolIntegerValue
	 */
	public $value;
	
	/**
	 * Comparing operator
	 * @var KontorolSearchConditionComparison
	 */
	public $comparison;
	
	private static $mapBetweenObjects = array
	(
		'comparison',
	);
	
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$mapBetweenObjects);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbObject = null, $skip = array())
	{
		/* @var $dbObject kCompareCondition */
		$dbObject->setValue($this->value->toObject());
			
		return parent::toObject($dbObject, $skip);
	}
	 
	/* (non-PHPdoc)
	 * @see KontorolObject::fromObject()
	 */
	public function doFromObject($dbObject, KontorolDetachedResponseProfile $responseProfile = null)
	{
		/* @var $dbObject kFieldMatchCondition */
		parent::doFromObject($dbObject, $responseProfile);
		
		$valueType = get_class($dbObject->getValue());
		KontorolLog::debug("Loading KontorolIntegerValue from type [$valueType]");
		switch ($valueType)
		{
			case 'kIntegerValue':
				$this->value = new KontorolIntegerValue();
				break;
				
			case 'kTimeContextField':
				$this->value = new KontorolTimeContextField();
				break;
				
			default:
				$this->value = KontorolPluginManager::loadObject('KontorolIntegerValue', $valueType);
				break;
		}
		
		if($this->value)
			$this->value->fromObject($dbObject->getValue());
	}
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolAssetPropertiesCompareCondition extends KontorolCondition
{
	/**
	 * Array of key/value objects that holds the property and the value to find and compare on an asset object
	 *
	 * @var KontorolKeyValueArray
	 */
	public $properties;

	private static $mapBetweenObjects = array
	(
		'properties',
	);

	/**
	 * Init object type
	 */
	public function __construct() 
	{
		$this->type = ConditionType::ASSET_PROPERTIES_COMPARE;
	}

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$mapBetweenObjects);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbObject = null, $skip = array())
	{
		if(!$dbObject)
			$dbObject = new kAssetPropertiesCompareCondition();

		$dbObject = parent::toObject($dbObject, $skip);

		if (!is_null($this->properties))
		{
			$properties = array();
			foreach($this->properties as $keyValue)
				$properties[$keyValue->key] = $keyValue->value;
			$dbObject->setProperties($properties);
		}

		return $dbObject;
	}

	/* (non-PHPdoc)
	 * @see KontorolObject::fromObject()
	 */
	public function doFromObject($dbObject, KontorolDetachedResponseProfile $responseProfile = null)
	{
		/** @var $dbObject kAssetPropertiesCompareCondition */
		parent::doFromObject($dbObject, $responseProfile);
		
		if($this->shouldGet('properties', $responseProfile))
			$this->properties = KontorolKeyValueArray::fromKeyValueArray($dbObject->getProperties());
	}
}

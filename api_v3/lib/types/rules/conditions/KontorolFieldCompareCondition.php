<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolFieldCompareCondition extends KontorolCompareCondition
{
	/**
	 * Field to evaluate
	 * @var KontorolIntegerField
	 */
	public $field;
	 
	/**
	 * Init object type
	 */
	public function __construct() 
	{
		$this->type = ConditionType::FIELD_COMPARE;
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbObject = null, $skip = array())
	{
		if(!$dbObject)
			$dbObject = new kFieldCompareCondition();
	
		/* @var $dbObject kFieldCompareCondition */
		$dbObject->setField($this->field->toObject());
			
		return parent::toObject($dbObject, $skip);
	}
	 
	/* (non-PHPdoc)
	 * @see KontorolObject::fromObject()
	 */
	public function doFromObject($dbObject, KontorolDetachedResponseProfile $responseProfile = null)
	{
		/* @var $dbObject kFieldMatchCondition */
		parent::doFromObject($dbObject, $responseProfile);
		
		$fieldType = get_class($dbObject->getField());
		KontorolLog::debug("Loading KontorolIntegerField from type [$fieldType]");
		switch ($fieldType)
		{
			case 'kTimeContextField':
				$this->field = new KontorolTimeContextField();
				break;
				
			default:
				$this->field = KontorolPluginManager::loadObject('KontorolIntegerField', $fieldType);
				break;
		}
		
		if($this->field)
			$this->field->fromObject($dbObject->getField());
	}
}

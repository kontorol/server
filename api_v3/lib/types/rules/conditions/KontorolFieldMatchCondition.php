<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolFieldMatchCondition extends KontorolMatchCondition
{
	/**
	 * Field to evaluate
	 * @var KontorolStringField
	 */
	public $field;
	
	/**
	 * Init object type
	 */
	public function __construct() 
	{
		$this->type = ConditionType::FIELD_MATCH;
	}
	 
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbObject = null, $skip = array())
	{
		if(!$dbObject)
			$dbObject = new kFieldMatchCondition();
	
		/* @var $dbObject kFieldMatchCondition */
		$dbObject->setField($this->field->toObject());
			
		return parent::toObject($dbObject, $skip);
	}
	 
	/* (non-PHPdoc)
	 * @see KontorolObject::fromObject()
	 */
	public function doFromObject($dbObject, KontorolDetachedResponseProfile $responseProfile = null)
	{
		/* @var $dbObject kFieldMatchCondition */
		parent::doFromObject($dbObject, $responseProfile);
		
		$fieldType = get_class($dbObject->getField());
		KontorolLog::debug("Loading KontorolStringField from type [$fieldType]");
		switch ($fieldType)
		{
			case 'kCountryContextField':
				$this->field = new KontorolCountryContextField();
				break;
				
			case 'kIpAddressContextField':
				$this->field = new KontorolIpAddressContextField();
				break;
				
			case 'kUserAgentContextField':
				$this->field = new KontorolUserAgentContextField();
				break;
				
			case 'kUserEmailContextField':
				$this->field = new KontorolUserEmailContextField();
				break;
				
			case 'kCoordinatesContextField':
				$this->field = new KontorolCoordinatesContextField();
				break;

			case 'kAnonymousIPContextField':
				$this->field = new KontorolAnonymousIPContextField();
				break;

			case 'kEvalStringField':
			    $this->field = new KontorolEvalStringField();
			    break;
			
			case 'kObjectIdField':
			    $this->field = new KontorolObjectIdField();
			    break;				
				
			case 'kEvalStringField':
				$this->field = new KontorolEvalStringField();
				break;
				
			case 'kObjectIdField':
				$this->field = new KontorolObjectIdField();
				break;
				
			default:
				$this->field = KontorolPluginManager::loadObject('KontorolStringField', $fieldType);
				break;
		}
		
		if($this->field)
			$this->field->fromObject($dbObject->getField());
	}
}

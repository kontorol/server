<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolCountryCondition extends KontorolMatchCondition
{
	/**
	 * The ip geo coder engine to be used
	 * 
	 * @var KontorolGeoCoderType
	 */
	public $geoCoderType;

	private static $mapBetweenObjects = array
	(
		'geoCoderType',
	);

	/**
	 * Init object type
	 */
	public function __construct() 
	{
		$this->type = ConditionType::COUNTRY;
	}
		
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$mapBetweenObjects);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbObject = null, $skip = array())
	{
		if(!$dbObject)
			$dbObject = new kCountryCondition();
			
		return parent::toObject($dbObject, $skip);
	}
}

<?php

/**
 * @package api
 * @subpackage objects
 */
class KontorolSiteCondition extends KontorolMatchCondition
{
	/**
	 * Init object type
	 */
	public function __construct() 
	{
		$this->type = ConditionType::SITE;
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbObject = null, $skip = array())
	{
		if(!$dbObject)
			$dbObject = new kSiteCondition();
			
		return parent::toObject($dbObject, $skip);
	}
}

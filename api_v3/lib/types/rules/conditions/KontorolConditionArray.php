<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolConditionArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolConditionArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
			$nObj = self::getInstanceByDbObject($obj);
			if(!$nObj)
			{
				KontorolLog::alert("Object [" . get_class($obj) . "] type [" . $obj->getType() . "] could not be translated to API object");
				continue;
			}
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}

	static function getInstanceByDbObject(kCondition $dbObject)
	{
		switch($dbObject->getType())
		{
			case ConditionType::AUTHENTICATED:
				return new KontorolAuthenticatedCondition();
			case ConditionType::COUNTRY:
				return new KontorolCountryCondition();
			case ConditionType::IP_ADDRESS:
				return new KontorolIpAddressCondition();
			case ConditionType::SITE:
				return new KontorolSiteCondition();
			case ConditionType::USER_AGENT:
				return new KontorolUserAgentCondition();
			case ConditionType::FIELD_COMPARE:
				return new KontorolFieldCompareCondition();
			case ConditionType::FIELD_MATCH:
				return new KontorolFieldMatchCondition();
			case ConditionType::ASSET_PROPERTIES_COMPARE:
				return new KontorolAssetPropertiesCompareCondition();
			case ConditionType::USER_ROLE:
				return new KontorolUserRoleCondition();
			case ConditionType::GEO_DISTANCE:
				return new KontorolGeoDistanceCondition();
			case ConditionType::OR_OPERATOR:
			    return new KontorolOrCondition();
			case ConditionType::HASH:
			    return new KontorolHashCondition();
			case ConditionType::DELIVERY_PROFILE:
				return new KontorolDeliveryProfileCondition();
			case ConditionType::ACTIVE_EDGE_VALIDATE:
				return new KontorolValidateActiveEdgeCondition();
			case ConditionType::ANONYMOUS_IP:
				return new KontorolAnonymousIPCondition();
			case ConditionType::ASSET_TYPE:
				return new KontorolAssetTypeCondition();
			case ConditionType::BOOLEAN:
				return new KontorolBooleanEventNotificationCondition();
			case ConditionType::HTTP_HEADER:
				return new KontorolHttpHeaderCondition();
			default:
			     return KontorolPluginManager::loadObject('KontorolCondition', $dbObject->getType());
		}
	}
		
	public function __construct()
	{
		parent::__construct("KontorolCondition");
	}
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolAuthenticatedCondition extends KontorolCondition
{
	/**
	 * The privelege needed to remove the restriction
	 * 
	 * @var KontorolStringValueArray
	 */
	public $privileges;
	
	private static $mapBetweenObjects = array
	(
		'privileges',
	);
	
	/**
	 * Init object type
	 */
	public function __construct() 
	{
		$this->type = ConditionType::AUTHENTICATED;
	}
	
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$mapBetweenObjects);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbObject = null, $skip = array())
	{
		if(!$dbObject)
			$dbObject = new kAuthenticatedCondition();
			
		return parent::toObject($dbObject, $skip);
	}
}

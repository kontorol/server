<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolContextTypeHolder extends KontorolObject
{
	/**
	 * The type of the condition context
	 * 
	 * @var KontorolContextType
	 */
	public $type;
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject()
	 */
	public function toObject($dbObject = null, $skip = array())
	{
		return $this->type;
	}
	
	private static $mapBetweenObjects = array
	(
		'type',
	);
	
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$mapBetweenObjects);
	}
}

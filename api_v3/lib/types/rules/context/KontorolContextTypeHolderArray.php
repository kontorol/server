<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolContextTypeHolderArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolContextTypeHolderArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $type)
		{
			$nObj = self::getInstanceByType($type);				
			$nObj->type = $type;
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}

	static function getInstanceByType($type)
	{
		switch($type)
		{
			case ContextType::DOWNLOAD:
			case ContextType::PLAY:
			case ContextType::THUMBNAIL:
			case ContextType::METADATA:
				return new KontorolAccessControlContextTypeHolder();
			default:
				return new KontorolContextTypeHolder();
		}		
	}
	
	public function __construct()
	{
		parent::__construct("KontorolContextTypeHolder");
	}
}

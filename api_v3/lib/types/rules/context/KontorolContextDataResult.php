<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolContextDataResult extends KontorolObject
{	
	/**
	 * Array of messages as received from the rules that invalidated
	 * @var KontorolStringArray
	 */
	public $messages;
	
	/**
	 * Array of actions as received from the rules that invalidated
	 * @var KontorolRuleActionArray
	 */
	public $actions;

	private static $mapBetweenObjects = array
	(
		'messages',
		'actions',
	);
	
	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$mapBetweenObjects);
	}
}

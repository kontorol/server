<?php
/**
 * @package api
 * @subpackage objects
 * @abstract
 */
abstract class KontorolRuleAction extends KontorolObject
{
	/**
	 * The type of the action
	 * 
	 * @readonly
	 * @var KontorolRuleActionType
	 */
	public $type;
}

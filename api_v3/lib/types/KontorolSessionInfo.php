<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolSessionInfo extends KontorolObject
{
	/**
	 * @var string
	 * @readonly
	 */
	public $ks;

	/**
	 * @var KontorolSessionType
	 * @readonly
	 */
	public $sessionType;

	/**
	 * @var int
	 * @readonly
	 */
	public $partnerId;

	/**
	 * @var string
	 * @readonly
	 */
	public $userId;

	/**
	 * @var int expiry time in seconds (unix timestamp)
	 * @readonly
	 */
	public $expiry;

	/**
	 * @var string
	 * @readonly
	 */
	public $privileges;
}

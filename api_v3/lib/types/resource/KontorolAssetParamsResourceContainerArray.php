<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolAssetParamsResourceContainerArray extends KontorolTypedArray
{
	public function __construct()
	{
		parent::__construct("KontorolAssetParamsResourceContainer");
	}
}

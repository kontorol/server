<?php
/**
 * @package api
 * @subpackage objects
 * @abstract
 */
abstract class KontorolDataCenterContentResource extends KontorolContentResource
{
	public function getDc()
	{
		return kDataCenterMgr::getCurrentDcId();
	}

	/* (non-PHPdoc)
	 * @see KontorolObject::validateForUsage($sourceObject, $propertiesToSkip)
	 */
	public function validateForUsage($sourceObject, $propertiesToSkip = array())
	{
		parent::validateForUsage($sourceObject, $propertiesToSkip);
		
		$dc = $this->getDc();
		if($dc == kDataCenterMgr::getCurrentDcId())
			return;
			
		$remoteDCHost = kDataCenterMgr::getRemoteDcExternalUrlByDcId($dc);
		if($remoteDCHost)
			kFileUtils::dumpApiRequest($remoteDCHost);
			
		throw new KontorolAPIException(KontorolErrors::REMOTE_DC_NOT_FOUND, $dc);
	}
}

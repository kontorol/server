<?php
/**
 * Concat operation attributes
 * 
 * @package api
 * @subpackage objects
 */
class KontorolConcatAttributes extends KontorolOperationAttributes
{
	/**
	 * The resource to be concatenated
	 * @var KontorolDataCenterContentResource
	 */
	public $resource;

	public function toObject ( $object_to_fill = null , $props_to_skip = array() )
	{
		throw new KontorolAPIException(KontorolErrors::RESOURCE_TYPE_NOT_SUPPORTED, get_class($this));
	}
}

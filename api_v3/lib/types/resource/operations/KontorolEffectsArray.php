<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolEffectsArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr = null, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolEffectsArray();
		if(is_null($arr))
			return $newArr;

		foreach($arr as $obj)
		{
			$nObj = new KontorolEffect();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}

		return $newArr;
	}

	public function __construct()
	{
		parent::__construct("KontorolEffect");
	}
}

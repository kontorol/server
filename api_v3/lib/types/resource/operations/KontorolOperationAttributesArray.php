<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolOperationAttributesArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr = null, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolOperationAttributesArray();
		if(is_null($arr))
			return $newArr;
			
		foreach($arr as $obj)
		{
			$class = $obj->getApiType();
			$nObj = new $class();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct()
	{
		parent::__construct("KontorolOperationAttributes");
	}
}

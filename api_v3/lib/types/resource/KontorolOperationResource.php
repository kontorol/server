<?php
/**
 * A resource that perform operation (transcoding, clipping, cropping) before the flavor is ready.
 * 
 * @package api
 * @subpackage objects
 */
class KontorolOperationResource extends KontorolContentResource
{
	/**
	 * Only KontorolEntryResource and KontorolAssetResource are supported
	 * @var KontorolContentResource
	 */
	public $resource;
	
	/**
	 * @var KontorolOperationAttributesArray
	 */
	public $operationAttributes;
	
	/**
	 * ID of alternative asset params to be used instead of the system default flavor params 
	 * @var int
	 */
	public $assetParamsId;
	
	/* (non-PHPdoc)
	 * @see KontorolObject::validateForUsage($sourceObject, $propertiesToSkip)
	 */
	public function validateForUsage($sourceObject, $propertiesToSkip = array())
	{
		parent::validateForUsage($sourceObject, $propertiesToSkip);
		
		$this->validatePropertyNotNull('resource');
		
		if(!($this->resource instanceof KontorolEntryResource) && !($this->resource instanceof KontorolAssetResource))
			throw new KontorolAPIException(KontorolErrors::RESOURCE_TYPE_NOT_SUPPORTED, get_class($this->resource));
	}
	
	/* (non-PHPdoc)
	 * @see KontorolResource::validateEntry()
	 */
	public function validateEntry(entry $dbEntry, $validateLocalExist = false)
	{
		parent::validateEntry($dbEntry, $validateLocalExist);
		
		$this->resource->validateEntry($dbEntry, $validateLocalExist);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolResource::entryHandled()
	 */
	public function entryHandled(entry $dbEntry)
	{
		parent::entryHandled($dbEntry);
		$this->resource->entryHandled($dbEntry);
	}
	
	private static $map_between_objects = array('assetParamsId');
	
	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolObject::toObject($object_to_fill, $props_to_skip)
	 */
	public function toObject($object_to_fill = null, $props_to_skip = array())
	{
		$this->validateForUsage($object_to_fill, $props_to_skip);
		
		if(is_null($this->operationAttributes) || !count($this->operationAttributes))
			return $this->resource->toObject();
		
		if(!$object_to_fill)
			$object_to_fill = new kOperationResource();
		
		$operationAttributes = array();
		foreach($this->operationAttributes as $operationAttributesObject)
			$operationAttributes[] = $operationAttributesObject->toObject();
		
		$object_to_fill->setOperationAttributes($operationAttributes);
		$object_to_fill->setResource($this->resource->toObject());
		
		return parent::toObject($object_to_fill, $props_to_skip);
	}

}

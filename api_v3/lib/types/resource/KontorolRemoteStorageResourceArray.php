<?php
/**
 * Used to ingest media that is available on remote server and accessible using the supplied URL, the media file won't be downloaded but a file sync object of URL type will point to the media URL.
 *
 * @package api
 * @subpackage objects
 */
class KontorolRemoteStorageResourceArray extends KontorolTypedArray
{
	/**
	 * @param array<kRemoteStorageResource> $arr
	 * @return KontorolRemoteStorageResourceArray
	 */
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolRemoteStorageResourceArray();
		foreach($arr as $obj)
		{
			$nObj = new KontorolRemoteStorageResource();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}

		return $newArr;
	}
	
	public function __construct()
	{
		parent::__construct("KontorolRemoteStorageResource");
	}
}

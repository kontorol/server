<?php
/**
 * @package api
 * @subpackage objects
 */
abstract class KontorolMediaServerNode extends KontorolDeliveryServerNode
{
	/**
	 * Media server application name
	 *
	 * @var string
	 */
	public $applicationName;
			
	/**
	 * Media server playback port configuration by protocol and format
	 *
	 * @var KontorolKeyValueArray
	 */
	public $mediaServerPortConfig;
	
	/**
	 * Media server playback Domain configuration by protocol and format
	 *
	 * @var KontorolKeyValueArray
	 * @deprecated Use Delivery Profile Ids instead
	 * 
	 */
	public $mediaServerPlaybackDomainConfig;
	
	private static $mapBetweenObjects = array
	(
		'applicationName',
		'mediaServerPortConfig',
		'mediaServerPlaybackDomainConfig',
	);
	
	/* (non-PHPdoc)
	 * @see KontorolObject::getMapBetweenObjects()
	 */
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$mapBetweenObjects);
	}
}

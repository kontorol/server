<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolEdgeServerNodeFilter extends KontorolEdgeServerNodeBaseFilter
{
	public function getTypeListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null, $type = null)
	{
		if(!$type)
			$type = serverNodeType::EDGE;
	
		return parent::getTypeListResponse($pager, $responseProfile, $type);
	}
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolServerNodeListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolServerNodeArray
	 * @readonly
	 */
	public $objects;
}

<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolServerNodeStatus extends KontorolEnum implements ServerNodeStatus
{
	public static function getEnumClass()
	{
		return 'ServerNodeStatus';
	}
}

<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolServerNodeFilter extends KontorolServerNodeBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new ServerNodeFilter();
	}
	
	public function getTypeListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null, $type = null)
	{
		list($list, $totalCount) = $this->doGetListResponse($pager, $type);
		$response = new KontorolServerNodeListResponse();
		$response->objects = KontorolServerNodeArray::fromDbArray($list, $responseProfile);
		$response->totalCount = $totalCount;
	
		return $response;
	}
	
	protected function doGetListResponse(KontorolFilterPager $pager, $type = null)
	{
		$c = new Criteria();
			
		if($type)
			$c->add(ServerNodePeer::TYPE, $type);
			
		$serverNodeFilter = $this->toObject();
		$serverNodeFilter->attachToCriteria($c);
		$pager->attachToCriteria($c);
			
		$list = ServerNodePeer::doSelect($c);
		$totalCount = count($list);
	
		return array($list, $totalCount);
	}

	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		return $this->getTypeListResponse($pager, $responseProfile);
	}
}

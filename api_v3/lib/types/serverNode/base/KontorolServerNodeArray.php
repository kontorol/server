<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolServerNodeArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolServerNodeArray();
		foreach($arr as $obj)
		{
			$nObj = KontorolServerNode::getInstance($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct( )
	{
		return parent::__construct ( "KontorolServerNode" );
	}
}

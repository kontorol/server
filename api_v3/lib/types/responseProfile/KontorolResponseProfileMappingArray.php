<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolResponseProfileMappingArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolResponseProfileMappingArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
			$dbClass = get_class($obj);
			if ($dbClass == 'kResponseProfileMapping')
				$nObj = new KontorolResponseProfileMapping();
			else
				$nObj = KontorolPluginManager::loadObject('KontorolResponseProfileMapping', $dbClass);

			if (is_null($nObj))
				KontorolLog::err('Failed to load api object for '.$dbClass);

			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolResponseProfileMapping");
	}
}

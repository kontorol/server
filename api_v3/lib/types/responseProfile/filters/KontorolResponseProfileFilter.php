<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolResponseProfileFilter extends KontorolResponseProfileBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new ResponseProfileFilter();
	}
}

<?php
/**
 * @package api
 * @subpackage objects
 */
abstract class KontorolBaseResponseProfile extends KontorolObject implements IApiObjectFactory
{
	public static function getInstance($sourceObject, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$object = null;
		
		if($sourceObject instanceof ResponseProfile)
		{
			$object = new KontorolResponseProfile();
		}
		elseif($sourceObject instanceof kResponseProfile)
		{
			$object = new KontorolDetachedResponseProfile();
		}
		
		if($object)
		{
			$object->fromObject($sourceObject, $responseProfile);
		}
		
		return $object;
	}
}

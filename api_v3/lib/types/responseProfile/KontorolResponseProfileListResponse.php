<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolResponseProfileListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolResponseProfileArray
	 * @readonly
	 */
	public $objects;
}

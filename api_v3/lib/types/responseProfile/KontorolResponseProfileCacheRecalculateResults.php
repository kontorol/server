<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolResponseProfileCacheRecalculateResults extends KontorolObject
{
	/**
	 * Last recalculated id
	 * 
	 * @var string
	 */
	public $lastObjectKey;
	
	/**
	 * Number of recalculated keys
	 * 
	 * @var int
	 */
	public $recalculated;
}

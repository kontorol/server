<?php
/**
 * Associative array of KontorolListResponse
 * 
 * @package api
 * @subpackage objects
 */
class KontorolListResponseArray extends KontorolAssociativeArray
{
	public function __construct()
	{
		return parent::__construct("KontorolListResponse");
	}
}

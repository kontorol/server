<?php
/**
 * @package api
 * @subpackage objects
 * @deprecated use KontorolRule instead
 */
class KontorolSiteRestriction extends KontorolBaseRestriction
{
	/**
	 * The site restriction type (allow or deny)
	 * 
	 * @var KontorolSiteRestrictionType
	 */
	public $siteRestrictionType;
	
	/**
	 * Comma separated list of sites (domains) to allow or deny
	 * 
	 * @var string
	 */
	public $siteList;
	
	private static $mapBetweenObjects = array
	(
		"siteRestrictionType",
		"siteList",
	);
	
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$mapBetweenObjects);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolBaseRestriction::toRule()
	 */
	public function toRule(KontorolRestrictionArray $restrictions)
	{
		return $this->toObject(new kAccessControlSiteRestriction());
	}
}

<?php
/**
 * @package api
 * @subpackage objects
 * @abstract
 * @deprecated use KontorolRuleAction
 */
abstract class KontorolAccessControlAction extends KontorolObject
{
	/**
	 * The type of the access control action
	 * 
	 * @readonly
	 * @var KontorolAccessControlActionType
	 */
	public $type;
}

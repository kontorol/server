<?php
/**
 * @package api
 * @subpackage objects
 * @deprecated use KontorolRuleActionArray
 */
class KontorolAccessControlActionArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolAccessControlActionArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
			$nObj = self::getInstanceByDbObject($obj);
			if(!$nObj)
				throw new kCoreException("No API object found for core object [" . get_class($obj) . "] with type [" . $obj->getType() . "]", kCoreException::OBJECT_API_TYPE_NOT_FOUND);
				
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}

	static function getInstanceByDbObject(kRuleAction $dbObject)
	{
		switch($dbObject->getType())
		{
			case RuleActionType::BLOCK:
				return new KontorolAccessControlBlockAction();
			case RuleActionType::PREVIEW:
				return new KontorolAccessControlPreviewAction();
			case RuleActionType::LIMIT_FLAVORS:
				return new KontorolAccessControlLimitFlavorsAction();
			case RuleActionType::LIMIT_THUMBNAIL_CAPTURE:
				return new KontorolAccessControlLimitThumbnailCaptureAction();
			default:
				return KontorolPluginManager::loadObject('KontorolAccessControlAction', $dbObject->getType());
		}
	}
		
	public function __construct()
	{
		parent::__construct("KontorolAccessControlAction");
	}
}

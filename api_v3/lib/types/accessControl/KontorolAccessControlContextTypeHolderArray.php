<?php
/**
 * @package api
 * @subpackage objects
 * @deprecated use KontorolContextTypeHolderArray
 */
class KontorolAccessControlContextTypeHolderArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolAccessControlContextTypeHolderArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $type)
		{
    		$nObj = new KontorolAccessControlContextTypeHolder();
			$nObj->type = $type;
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct()
	{
		parent::__construct("KontorolAccessControlContextTypeHolder");
	}
}

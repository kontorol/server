<?php
/**
 * @package api
 * @subpackage objects
 * @deprecated use KontorolRule instead
 */
class KontorolIpAddressRestriction extends KontorolBaseRestriction
{
	/**
	 * Ip address restriction type (Allow or deny)
	 * 
	 * @var KontorolIpAddressRestrictionType
	 */
	public $ipAddressRestrictionType; 
	
	/**
	 * Comma separated list of ip address to allow to deny 
	 * 
	 * @var string
	 */
	public $ipAddressList;
	
	private static $mapBetweenObjects = array
	(
		"ipAddressRestrictionType",
		"ipAddressList",
	);
	
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$mapBetweenObjects);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolBaseRestriction::toRule()
	 */
	public function toRule(KontorolRestrictionArray $restrictions)
	{
		return $this->toObject(new kAccessControlIpAddressRestriction());
	}
}

<?php
/**
 * @package api
 * @subpackage objects
 * @deprecated use KontorolRuleArray instead
 */
class KontorolRestrictionArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolRestrictionArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
			$nObj = self::getInstanceByDbObject($obj);
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}

	static function getInstanceByDbObject(kAccessControlRestriction $dbObject)
	{
		$objectClass = get_class($dbObject);
		switch($objectClass)
		{
			case "kAccessControlSiteRestriction":
				return new KontorolSiteRestriction();
			case "kAccessControlCountryRestriction":
				return new KontorolCountryRestriction();
			case "kAccessControlSessionRestriction":
				return new KontorolSessionRestriction();
			case "kAccessControlPreviewRestriction":
				return new KontorolPreviewRestriction();
			case "kAccessControlIpAddressRestriction":
				return new KontorolIpAddressRestriction();
			case "kAccessControlUserAgentRestriction":
				return new KontorolUserAgentRestriction();
			case "kAccessControlLimitFlavorsRestriction":
				return new KontorolLimitFlavorsRestriction();
			default:
				KontorolLog::err("Access control rule type [$objectClass] could not be loaded");
				return null;
		}
	}
	
	public function __construct()
	{
		parent::__construct("KontorolBaseRestriction");
	}
}

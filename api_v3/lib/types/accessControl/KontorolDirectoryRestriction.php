<?php
/**
 * @package api
 * @subpackage objects
 * @deprecated
 */
class KontorolDirectoryRestriction extends KontorolBaseRestriction
{
	/**
	 * Kontorol directory restriction type
	 * 
	 * @var KontorolDirectoryRestrictionType
	 */
	public $directoryRestrictionType;
	
	/* (non-PHPdoc)
	 * @see KontorolBaseRestriction::toRule()
	 */
	public function toRule(KontorolRestrictionArray $restrictions)
	{
		return null;
	}
}

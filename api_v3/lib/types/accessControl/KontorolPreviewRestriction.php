<?php
/**
 * @package api
 * @subpackage objects
 * @deprecated use KontorolRule instead
 */
class KontorolPreviewRestriction extends KontorolSessionRestriction
{
	/**
	 * The preview restriction length 
	 * 
	 * @var int
	 */
	public $previewLength;
	
	private static $mapBetweenObjects = array
	(
		"previewLength",
	);
	
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$mapBetweenObjects);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolBaseRestriction::toRule()
	 */
	public function toRule(KontorolRestrictionArray $restrictions)
	{
		// Preview restriction became a rule action, it's not a rule.
		return null;
	}
}

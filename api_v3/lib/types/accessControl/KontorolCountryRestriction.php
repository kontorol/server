<?php
/**
 * @package api
 * @subpackage objects
 * @deprecated use KontorolRule instead
 */
class KontorolCountryRestriction extends KontorolBaseRestriction
{
	/**
	 * Country restriction type (Allow or deny)
	 * 
	 * @var KontorolCountryRestrictionType
	 */
	public $countryRestrictionType; 
	
	/**
	 * Comma separated list of country codes to allow to deny 
	 * 
	 * @var string
	 */
	public $countryList;
	
	private static $mapBetweenObjects = array
	(
		"countryRestrictionType",
		"countryList",
	);
	
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$mapBetweenObjects);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolBaseRestriction::toRule()
	 */
	public function toRule(KontorolRestrictionArray $restrictions)
	{
		return $this->toObject(new kAccessControlCountryRestriction());
	}
}

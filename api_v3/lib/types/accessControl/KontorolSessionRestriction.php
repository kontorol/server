<?php
/**
 * @package api
 * @subpackage objects
 * @deprecated use KontorolRule instead
 */
class KontorolSessionRestriction extends KontorolBaseRestriction
{
	/* (non-PHPdoc)
	 * @see KontorolBaseRestriction::toRule()
	 */
	public function toRule(KontorolRestrictionArray $restrictions)
	{	
		$rule = null;
		
		foreach($restrictions as $restriction)
		{
			if($restriction instanceof KontorolPreviewRestriction)
			{
				$rule = $restriction->toObject(new kAccessControlPreviewRestriction());
			}
		}
	
		if(!$rule)
			$rule = $this->toObject(new kAccessControlSessionRestriction());
		
		return $rule;
	}
}

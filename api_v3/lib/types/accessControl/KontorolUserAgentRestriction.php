<?php
/**
 * @package api
 * @subpackage objects
 * @deprecated use KontorolRule instead
 */
class KontorolUserAgentRestriction extends KontorolBaseRestriction
{
	/**
	 * User agent restriction type (Allow or deny)
	 * 
	 * @var KontorolUserAgentRestrictionType
	 */
	public $userAgentRestrictionType; 
	
	/**
	 * A comma seperated list of user agent regular expressions
	 * 
	 * @var string
	 */
	public $userAgentRegexList;
	
	private static $mapBetweenObjects = array
	(
		"userAgentRestrictionType",
		"userAgentRegexList",
	);
	
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$mapBetweenObjects);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolBaseRestriction::toRule()
	 */
	public function toRule(KontorolRestrictionArray $restrictions)
	{
		return $this->toObject(new kAccessControlUserAgentRestriction());
	}
}

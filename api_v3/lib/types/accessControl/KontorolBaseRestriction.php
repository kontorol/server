<?php
/**
 * @package api
 * @subpackage objects
 * @deprecated use KontorolRule instead
 * @abstract
 */
abstract class KontorolBaseRestriction extends KontorolObject
{
	/**
	 * @param KontorolRestrictionArray $restrictions enable one restriction to be affected by other restrictions
	 * @return kAccessControlRestriction
	 * @abstract must be implemented
	 */
	abstract public function toRule(KontorolRestrictionArray $restrictions);
}

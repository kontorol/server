<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolBulkUploadPluginDataArray extends KontorolTypedArray
{
	public function __construct()
	{
		return parent::__construct("KontorolBulkUploadPluginData");
	}
	
	public function toValuesArray()
	{
		$ret = array();
		foreach($this as $pluginData)
			$ret[$pluginData->field] = $pluginData->value;
			
		return $ret;
	}
}
?>

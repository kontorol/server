<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolUserEntryStatus extends KontorolDynamicEnum implements UserEntryStatus
{
	public static function getEnumClass()
	{
		return 'UserEntryStatus';
	}
}


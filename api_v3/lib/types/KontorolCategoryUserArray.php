<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolCategoryUserArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolCategoryUserArray();
		foreach($arr as $obj)
		{
			$nObj = new KontorolCategoryUser();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct()
	{
		return parent::__construct("KontorolCategoryUser");
	}
}

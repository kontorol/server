<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolReportArray extends KontorolTypedArray
{
	public function __construct()
	{
		return parent::__construct("KontorolReport");
	}
	
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolReportArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
    		$nObj = new KontorolReport();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
}
?>

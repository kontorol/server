<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolReportResponse extends KontorolObject
{
	/**
	 * @var string
	 */
	public $columns;
	
	/**
	 * @var KontorolStringArray
	 */
	public $results;
	
	public static function fromColumnsAndRows($columns, $rows)
	{
		$reportResponse = new KontorolReportResponse();
		$reportResponse->columns = implode(',', $columns);
		$reportResponse->results = new KontorolStringArray();
		foreach($rows as $row)
		{
			// we are using comma as a seperator, so don't allow it in results
			foreach($row as &$tempColumnData)
				$tempColumnData = str_replace(',', '', $tempColumnData);
				
			$string = new KontorolString();
			$string->value = implode(',', $row);
			$reportResponse->results[] = $string;
		}
		return $reportResponse;
	}
}

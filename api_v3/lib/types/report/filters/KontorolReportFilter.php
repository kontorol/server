<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolReportFilter extends KontorolReportBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new ReportFilter();
	}
}

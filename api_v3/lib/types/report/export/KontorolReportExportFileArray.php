<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolReportExportFileArray extends KontorolTypedArray
{

	public function __construct()
	{
		return parent::__construct("KontorolReportExportFile");
	}

	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolReportExportFileArray();
		foreach ( $arr as $obj )
		{
			$nObj = new KontorolReportExportFile();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}

		return $newArr;
	}

}

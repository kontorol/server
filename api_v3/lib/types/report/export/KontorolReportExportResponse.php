<?php

/**
 * @package api
 * @subpackage objects
 */
class KontorolReportExportResponse extends KontorolObject
{
	/**
	 * @var bigint
	 **/
	public $referenceJobId;

	/**
	 * @var string
	 **/
	public $reportEmail;

}

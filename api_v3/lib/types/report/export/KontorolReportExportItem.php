<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolReportExportItem extends KontorolObject
{

	/**
	 * @var string
	 */
	public $reportTitle;

	/**
	 * @var KontorolReportExportItemType
	 */
	public $action;

	/**
	 * @var KontorolReportType
	 */
	public $reportType;

	/**
	 * @var KontorolReportInputFilter
	 */
	public $filter;

	/**
	 * @var string
	 */
	public $order;

	/**
	 * @var string
	 */
	public $objectIds;

	/**
	 * @var KontorolReportResponseOptions
	 */
	public $responseOptions;

	private static $map_between_objects = array
	(
		"reportTitle",
		"action",
		"reportType",
		"order",
		"objectIds",
		"responseOptions",
	);

	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}

	public function toObject($object_to_fill = null, $props_to_skip = array())
	{
		if (!$object_to_fill)
		{
			$object_to_fill = new kReportExportItem();
		}
		$object_to_fill->setFilter($this->filter);

		return parent::toObject($object_to_fill, array('filter'));
	}

	protected function doFromObject($srcObj, KontorolDetachedResponseProfile $responseProfile = null)
	{
		parent::doFromObject($srcObj, $responseProfile);
		$this->filter = $srcObj->getFilter();
	}

}

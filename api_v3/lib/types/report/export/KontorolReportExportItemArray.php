<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolReportExportItemArray extends KontorolTypedArray
{

	public function __construct()
	{
		return parent::__construct("KontorolReportExportItem");
	}

	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolReportExportItemArray();
		foreach ( $arr as $obj )
		{
			$nObj = new KontorolReportExportItem();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}

		return $newArr;
	}

}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolReportGraphArray extends KontorolTypedArray
{
	public static function fromReportDataArray ( $arr, $delimiter = ',' )
	{
		$newArr = new KontorolReportGraphArray();
		foreach ( $arr as $id => $data )
		{
			$nObj = new KontorolReportGraph();
			$nObj->fromReportData ( $id, $data, $delimiter );
			$newArr[] = $nObj;
		}
			
		return $newArr;
	}
	
	public function __construct( )
	{
		return parent::__construct ( "KontorolReportGraph" );
	}
}
?>

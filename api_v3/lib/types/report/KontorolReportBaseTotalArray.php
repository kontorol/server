<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolReportBaseTotalArray extends KontorolTypedArray
{
	public static function fromReportDataArray ( $arr )
	{
		$newArr = new KontorolReportBaseTotalArray();
		foreach ( $arr as $id => $data )
		{
			$nObj = new KontorolReportBaseTotal();
			$nObj->fromReportData ( $id, $data );
			$newArr[] = $nObj;
		}
			
		return $newArr;
	}
	
	public function __construct( )
	{
		return parent::__construct ( "KontorolReportBaseTotal" );
	}
}
?>

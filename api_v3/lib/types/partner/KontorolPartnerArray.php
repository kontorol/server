<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolPartnerArray extends KontorolTypedArray
{
	public static function fromPartnerArray(array $arr)
	{
		$newArr = new KontorolPartnerArray();
		foreach($arr as $obj)
		{
			$nObj = new KontorolPartner();
			$nObj->fromPartner($obj);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct( )
	{
		return parent::__construct ( "KontorolPartner" );
	}
}
?>

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolFeatureStatusArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolFeatureStatusArray();
		foreach($arr as $obj)
		{
			if ($obj){
				$nObj = new KontorolFeatureStatus();
				$nObj->fromObject($obj, $responseProfile);
				$newArr[] = $nObj;
			}
		}
		
		return $newArr;
	}
	
	public function __construct( )
	{
		return parent::__construct ( "KontorolFeatureStatus" );
	}
}

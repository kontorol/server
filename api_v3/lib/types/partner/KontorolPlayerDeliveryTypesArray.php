<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolPlayerDeliveryTypesArray extends KontorolTypedArray
{
	public function __construct()
	{
		return parent::__construct("KontorolPlayerDeliveryType");
	}

	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$ret = new KontorolPlayerDeliveryTypesArray();
		foreach($arr as $id => $item)
		{
			$obj = new KontorolPlayerDeliveryType();
			$obj->id = $id;
			$obj->fromArray($item);
			$obj->enabledByDefault = (bool)$obj->enabledByDefault;
				
			if(isset($item['flashvars']))
				$obj->flashvars = KontorolKeyValueArray::fromDbArray($item['flashvars']);
				
			$ret[] = $obj;
		}
		return $ret;
	}
}

<?php
/**
 * @package api
 * @subpackage objects
 */

class KontorolESearchLanguageArray extends KontorolTypedArray
{
	public function __construct()
	{
		return parent::__construct("KontorolESearchLanguageItem");
	}

	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolESearchLanguageArray();
		if($arr && is_array($arr))
		{
			foreach($arr as $item)
			{
				$arrayObject = new KontorolESearchLanguageItem();
				$arrayObject->eSerachLanguage = $item;
				$newArr[] = $arrayObject;
			}
		}
		return $newArr;
	}

	public function toObjectsArray()
	{
		$ret = array();
		foreach ($this->toArray() as $item)
		{
			/* @var $item KontorolESearchLanguageItem */
			$ret[] = $item->eSerachLanguage;
		}

		return array_unique($ret);
	}
}



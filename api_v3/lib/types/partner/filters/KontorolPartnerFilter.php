<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolPartnerFilter extends KontorolPartnerBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new partnerFilter();
	}
}

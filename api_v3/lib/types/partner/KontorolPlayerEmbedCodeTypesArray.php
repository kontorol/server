<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolPlayerEmbedCodeTypesArray extends KontorolTypedArray
{
	public function __construct()
	{
		return parent::__construct("KontorolPlayerEmbedCodeType");
	}
	
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$ret = new KontorolPlayerEmbedCodeTypesArray();
		foreach($arr as $id => $item)
		{
			$obj = new KontorolPlayerEmbedCodeType();
			$obj->id = $id;
			$obj->fromArray($item);
			$ret[] = $obj;
		}
		return $ret;
	}
}

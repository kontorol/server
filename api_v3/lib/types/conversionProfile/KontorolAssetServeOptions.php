<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolAssetServeOptions extends KontorolObject
{
	/**
	 * @var bool
	 */
	public $download;

	/**
	 * @var string
	 */
	public $referrer;
}

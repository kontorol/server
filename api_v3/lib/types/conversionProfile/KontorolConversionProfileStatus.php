<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolConversionProfileStatus extends KontorolDynamicEnum implements ConversionProfileStatus
{
	public static function getEnumClass()
	{
		return 'ConversionProfileStatus';
	}
}

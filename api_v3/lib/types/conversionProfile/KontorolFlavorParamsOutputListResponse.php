<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolFlavorParamsOutputListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolFlavorParamsOutputArray
	 * @readonly
	 */
	public $objects;
}

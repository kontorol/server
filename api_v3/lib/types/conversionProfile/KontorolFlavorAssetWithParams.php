<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolFlavorAssetWithParams extends KontorolObject
{
	/**
	 * The Flavor Asset (Can be null when there are params without asset)
	 * 
	 * @var KontorolFlavorAsset
	 */
	public $flavorAsset;
	
	/**
	 * The Flavor Params
	 * 
	 * @var KontorolFlavorParams
	 */
	public $flavorParams;
	
	/**
	 * The entry id
	 * 
	 * @var string
	 */
	public $entryId;
}

<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolLiveAssetFilter extends KontorolLiveAssetBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolAssetFilter::getListResponse()
	 */
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$types = KontorolPluginManager::getExtendedTypes(assetPeer::OM_CLASS, assetType::LIVE);
		return $this->getTypeListResponse($pager, $responseProfile, $types);
	}
}

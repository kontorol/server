<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolAssetParamsFilter extends KontorolAssetParamsBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new assetParamsFilter();
	}

	protected function doGetListResponse(KontorolFilterPager $pager, array $types = null)
	{
		$flavorParamsFilter = $this->toObject();
		
		$c = new Criteria();
		$flavorParamsFilter->attachToCriteria($c);
		
		$pager->attachToCriteria($c);
		
		if($types)
		{
			$c->add(assetParamsPeer::TYPE, $types, Criteria::IN);
		}
		
		$list = assetParamsPeer::doSelect($c);
		
		$c->setLimit(null);
		$totalCount = assetParamsPeer::doCount($c);

		return array($list, $totalCount);
	}

	public function getTypeListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null, array $types = null)
	{
		list($list, $totalCount) = $this->doGetListResponse($pager, $types);
		
		$response = new KontorolFlavorParamsListResponse();
		$response->objects = KontorolFlavorParamsArray::fromDbArray($list, $responseProfile);
		$response->totalCount = $totalCount;
		return $response;  
	}

	/* (non-PHPdoc)
	 * @see KontorolRelatedFilter::getListResponse()
	 */
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		return $this->getTypeListResponse($pager, $responseProfile);  
	}
}

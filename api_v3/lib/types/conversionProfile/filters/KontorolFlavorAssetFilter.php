<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolFlavorAssetFilter extends KontorolFlavorAssetBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolAssetFilter::getListResponse()
	 */
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$types = assetPeer::retrieveAllFlavorsTypes();
		return $this->getTypeListResponse($pager, $responseProfile, $types);
	}
}

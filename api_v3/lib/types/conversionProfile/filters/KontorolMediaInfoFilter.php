<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolMediaInfoFilter extends KontorolMediaInfoBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new MediaInfoFilter();
	}
	
	/* (non-PHPdoc)
	 * @see KontorolFilter::toObject()
	 */
	public function toObject ( $object_to_fill = null, $props_to_skip = array() )
	{
		if(!$this->flavorAssetIdEqual)
			throw new KontorolAPIException(KontorolErrors::PROPERTY_VALIDATION_CANNOT_BE_NULL, $this->getFormattedPropertyNameWithClassName('flavorAssetIdEqual'));
		return parent::toObject($object_to_fill, $props_to_skip);
	}
}

<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolConversionProfileFilter extends KontorolConversionProfileBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new conversionProfile2Filter();
	}
	
	/* (non-PHPdoc)
	 * @see KontorolRelatedFilter::getListResponse()
	 */
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$conversionProfile2Filter = $this->toObject();

		$c = new Criteria();
		$conversionProfile2Filter->attachToCriteria($c);
		
		$totalCount = conversionProfile2Peer::doCount($c);
		
		$pager->attachToCriteria($c);
		$dbList = conversionProfile2Peer::doSelect($c);
		
		$list = KontorolConversionProfileArray::fromDbArray($dbList, $responseProfile);
		$list->loadFlavorParamsIds();
		$response = new KontorolConversionProfileListResponse();
		$response->objects = $list;
		$response->totalCount = $totalCount;
		return $response;  
	}
}

<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolAssetFilter extends KontorolAssetBaseFilter
{
	/**
	 * @dynamicType KontorolAssetType
	 * @var string
	 */
	public $typeIn;
	
	static private $map_between_objects = array
	(
		"typeIn" => "_in_type",
	);
	
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$map_between_objects);
	}

	/**
	 * @return array
	 * @throws KontorolAPIException
	 */
	public function validateEntryIdsFiltered()
	{
		if (!$this->entryIdEqual && !$this->entryIdIn)
		{
			throw new KontorolAPIException(KontorolErrors::PROPERTY_VALIDATION_CANNOT_BE_NULL, $this->getFormattedPropertyNameWithClassName('entryIdEqual') . '/' . $this->getFormattedPropertyNameWithClassName('entryIdIn'));
		}
	}

	/**
	 * @return array
	 * @throws KontorolAPIException
	 */
	public function retrieveEntryIdsFiltered()
	{
		if ($this->entryIdEqual)
		{
			return array($this->entryIdEqual);
		}
		else if ($this->entryIdIn)
		{
			return explode(',', $this->entryIdIn);
		}
		return array();
	}

	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new AssetFilter();
	}
	
	public function doGetListResponse(KontorolFilterPager $pager, array $types = null)
	{
		myDbHelper::$use_alternative_con = myDbHelper::DB_HELPER_CONN_PROPEL2;

		// verify access to the relevant entries - either same partner as the KS or kontorol network
		$this->validateEntryIdsFiltered();
		$entryIds = $this->retrieveEntryIdsFiltered();
		$entryIds = entryPeer::filterEntriesByPartnerOrKontorolNetwork($entryIds, kCurrentContext::getCurrentPartnerId());
		if (!$entryIds)
		{
			return array(array(), 0);
		}
		
		$this->entryIdEqual = null;
		$this->entryIdIn = implode(',', $entryIds);

		// get the flavors
		$flavorAssetFilter = new AssetFilter();
		
		$this->toObject($flavorAssetFilter);

		$c = new Criteria();
		$flavorAssetFilter->attachToCriteria($c);
		
		if ($flavorAssetFilter->get('_in_type'))
        {
        	//If the $types array is empty we should not return results on the query.
        	$types = array_intersect($types, explode (',', $flavorAssetFilter->get('_in_type')));
        	if(!count($types))
        	{
        		myDbHelper::$use_alternative_con = null;
                return array(array(), 0);
        	}
        }
        
		if($types)
		{
			$c->add(assetPeer::TYPE, $types, Criteria::IN);
		}

		$pager->attachToCriteria($c);
		$list = assetPeer::doSelect($c);

		$resultCount = count($list);
		if ($resultCount && $resultCount < $pager->pageSize)
			$totalCount = ($pager->pageIndex - 1) * $pager->pageSize + $resultCount;
		else
		{
			KontorolFilterPager::detachFromCriteria($c);
			$totalCount = assetPeer::doCount($c);
		}
		
		myDbHelper::$use_alternative_con = null;
		
		return array($list, $totalCount);
	}

	public function getTypeListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null, array $types = null)
	{
		list($list, $totalCount) = $this->doGetListResponse($pager, $types);
		
		$response = new KontorolFlavorAssetListResponse();
		$response->objects = KontorolFlavorAssetArray::fromDbArray($list, $responseProfile);
		$response->totalCount = $totalCount;
		return $response;  
	}

	/* (non-PHPdoc)
	 * @see KontorolRelatedFilter::getListResponse()
	 */
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		return $this->getTypeListResponse($pager, $responseProfile);  
	}
}

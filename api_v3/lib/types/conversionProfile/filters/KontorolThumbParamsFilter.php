<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolThumbParamsFilter extends KontorolThumbParamsBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolAssetParamsFilter::getTypeListResponse()
	 */
	public function getTypeListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null, array $types = null)
	{
		list($list, $totalCount) = $this->doGetListResponse($pager, $types);
		
		$response = new KontorolThumbParamsListResponse();
		$response->objects = KontorolThumbParamsArray::fromDbArray($list, $responseProfile);
		$response->totalCount = $totalCount;
		return $response;  
	}
}

<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolFlavorParamsOutputFilter extends KontorolFlavorParamsOutputBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new assetParamsOutputFilter();
	}
	
	protected function doGetListResponse(KontorolFilterPager $pager, array $types = null)
	{
		$flavorParamsOutputFilter = $this->toObject();
	
		$c = new Criteria();
		$flavorParamsOutputFilter->attachToCriteria($c);
	
		$pager->attachToCriteria($c);
	
		if($types)
		{
			$c->add(assetParamsOutputPeer::TYPE, $types, Criteria::IN);
		}
	
		$list = assetParamsOutputPeer::doSelect($c);
	
		$c->setLimit(null);
		$totalCount = assetParamsOutputPeer::doCount($c);
	
		return array($list, $totalCount);
	}
	
	/* (non-PHPdoc)
	 * @see KontorolAssetParamsFilter::getTypeListResponse()
	 */
	public function getTypeListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null, array $types = null)
	{
		list($list, $totalCount) = $this->doGetListResponse($pager, $types);
		
		$response = new KontorolFlavorParamsOutputListResponse();
		$response->objects = KontorolFlavorParamsOutputArray::fromDbArray($list, $responseProfile);
		$response->totalCount = $totalCount;
		return $response;  
	}
}

<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolConversionProfileType extends KontorolDynamicEnum implements ConversionProfileType
{
	public static function getEnumClass()
	{
		return 'ConversionProfileType';
	}
}

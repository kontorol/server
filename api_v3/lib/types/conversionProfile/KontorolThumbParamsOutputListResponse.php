<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolThumbParamsOutputListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolThumbParamsOutputArray
	 * @readonly
	 */
	public $objects;
}

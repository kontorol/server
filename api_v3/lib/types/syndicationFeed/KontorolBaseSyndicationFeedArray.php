<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolBaseSyndicationFeedArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolBaseSyndicationFeedArray();
		if ( $arr == null ) return $newArr;
		foreach ( $arr as $obj )
		{
			$nObj = KontorolSyndicationFeedFactory::getInstanceByType($obj->getType());
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct("KontorolBaseSyndicationFeed");
	}
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolTubeMogulSyndicationFeed extends KontorolBaseSyndicationFeed
{
        /**
         *
         * @var KontorolTubeMogulSyndicationFeedCategories
         * @readonly
         */
        public $category;
        
	function __construct()
	{
		$this->type = KontorolSyndicationFeedType::TUBE_MOGUL;
	}
        
	private static $mapBetweenObjects = array
	(
	);
	
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$mapBetweenObjects);
	}
        
        public function toObject($object_to_fill = null , $props_to_skip = array())
        {
            $categories = explode(',', $this->categories);
            $numCategories = array();
            foreach($categories as $category)
            {
                $numCategories[] = $this->getCategoryId($category);
            }
            $this->categories = implode(',', $numCategories);
            parent::toObject($object_to_fill);
            $this->categories = implode(',', $categories);
        }
        
        public function doFromObject($source_object, KontorolDetachedResponseProfile $responseProfile = null)
        {
            parent::doFromObject($source_object, $responseProfile);
            $categories = explode(',', $this->categories);
            $strCategories = array();
            foreach($categories as $category)
            {
                $strCategories[] = $this->getCategoryName($category);
            }
            $this->categories = implode(',', $strCategories);
        }
        
        private static $mapCategories = array(
            KontorolTubeMogulSyndicationFeedCategories::ARTS_AND_ANIMATION => 1,
            KontorolTubeMogulSyndicationFeedCategories::COMEDY => 3,
            KontorolTubeMogulSyndicationFeedCategories::ENTERTAINMENT => 4,
            KontorolTubeMogulSyndicationFeedCategories::MUSIC => 5,
            KontorolTubeMogulSyndicationFeedCategories::NEWS_AND_BLOGS => 6,
            KontorolTubeMogulSyndicationFeedCategories::SCIENCE_AND_TECHNOLOGY => 7,
            KontorolTubeMogulSyndicationFeedCategories::SPORTS => 8,
            KontorolTubeMogulSyndicationFeedCategories::TRAVEL_AND_PLACES => 9,
            KontorolTubeMogulSyndicationFeedCategories::VIDEO_GAMES => 10,
            KontorolTubeMogulSyndicationFeedCategories::ANIMALS_AND_PETS => 11,
            KontorolTubeMogulSyndicationFeedCategories::AUTOS => 12,
            KontorolTubeMogulSyndicationFeedCategories::VLOGS_PEOPLE => 13,
            KontorolTubeMogulSyndicationFeedCategories::HOW_TO_INSTRUCTIONAL_DIY => 14,
            KontorolTubeMogulSyndicationFeedCategories::COMMERCIALS_PROMOTIONAL => 15,
            KontorolTubeMogulSyndicationFeedCategories::FAMILY_AND_KIDS => 16,
        );
	public static function getCategoryId( $category )
	{
            return self::$mapCategories[$category];
	}
        
        public static function getCategoryName( $id )
        {
            $arrCategories = array_flip(self::$mapCategories);
            return $arrCategories[$id];
        }
}

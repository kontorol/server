<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolOperaSyndicationFeed extends KontorolConstantXsltSyndicationFeed
{

    function __construct()
	{
		$this->type = KontorolSyndicationFeedType::OPERA_TV_SNAP;
		$this->xsltPath =  __DIR__."/xslt/opera_syndication.xslt";
	}
}

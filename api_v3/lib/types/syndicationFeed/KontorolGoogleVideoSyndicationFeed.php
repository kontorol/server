<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolGoogleVideoSyndicationFeed extends KontorolBaseSyndicationFeed
{
        /**
         *
         * @var KontorolGoogleSyndicationFeedAdultValues
         */
        public $adultContent;
	
	private static $mapBetweenObjects = array
	(
    	"adultContent",
	);
	
	public function getMapBetweenObjects()
	{
		return array_merge(parent::getMapBetweenObjects(), self::$mapBetweenObjects);
	}
        
        function __construct()
	{
		$this->type = KontorolSyndicationFeedType::GOOGLE_VIDEO;
	}
}

<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolBaseSyndicationFeedFilter extends KontorolBaseSyndicationFeedBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new syndicationFeedFilter();
	}
}

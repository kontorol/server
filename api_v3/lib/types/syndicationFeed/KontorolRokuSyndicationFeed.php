<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolRokuSyndicationFeed extends KontorolConstantXsltSyndicationFeed
{

    function __construct()
	{
		$this->type = KontorolSyndicationFeedType::ROKU_DIRECT_PUBLISHER;
		$this->xsltPath =  __DIR__."/xslt/roku_syndication.xslt";
	}
}

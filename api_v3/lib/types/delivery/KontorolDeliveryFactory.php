<?php

/**
 * @package api
 * @subpackage objects.factory
 */
class KontorolDeliveryProfileFactory {
	
	public static function getCoreDeliveryProfileInstanceByType($type) {
		$coreType = kPluginableEnumsManager::apiToCore('DeliveryProfileType', $type); 
		$class = DeliveryProfilePeer::getClassByDeliveryProfileType($coreType);
		return new $class();
	}
	
	public static function getDeliveryProfileInstanceByType($type) {
		switch ($type) {
			case KontorolDeliveryProfileType::GENERIC_HLS:
			case KontorolDeliveryProfileType::GENERIC_HLS_MANIFEST:
				return new KontorolDeliveryProfileGenericAppleHttp();
			case KontorolDeliveryProfileType::GENERIC_HDS:
			case KontorolDeliveryProfileType::GENERIC_HDS_MANIFEST:
				return new KontorolDeliveryProfileGenericHds();
			case KontorolDeliveryProfileType::GENERIC_HTTP:
					return new KontorolDeliveryProfileGenericHttp();
			case KontorolDeliveryProfileType::RTMP:
			case KontorolDeliveryProfileType::LIVE_RTMP:
				return new KontorolDeliveryProfileRtmp();
			case KontorolDeliveryProfileType::AKAMAI_HTTP:
				return new KontorolDeliveryProfileAkamaiHttp();
			case KontorolDeliveryProfileType::AKAMAI_HLS_MANIFEST:
				return new KontorolDeliveryProfileAkamaiAppleHttpManifest();
			case KontorolDeliveryProfileType::AKAMAI_HDS:
				return new KontorolDeliveryProfileAkamaiHds();
			case KontorolDeliveryProfileType::LIVE_HLS:
				return new KontorolDeliveryProfileLiveAppleHttp();
			case KontorolDeliveryProfileType::GENERIC_SS:
				return new KontorolDeliveryProfileGenericSilverLight();
			case KontorolDeliveryProfileType::GENERIC_RTMP:
				return new KontorolDeliveryProfileGenericRtmp();
			case KontorolDeliveryProfileType::VOD_PACKAGER_HLS_MANIFEST:
			case KontorolDeliveryProfileType::VOD_PACKAGER_HLS:
				return new KontorolDeliveryProfileVodPackagerHls();
			case KontorolDeliveryProfileType::VOD_PACKAGER_DASH:
				return new KontorolDeliveryProfileVodPackagerPlayServer();
			case KontorolDeliveryProfileType::VOD_PACKAGER_MSS:
				return new KontorolDeliveryProfileVodPackagerPlayServer();
			case KontorolDeliveryProfileType::LIVE_PACKAGER_HLS:
				return new KontorolDeliveryProfileLivePackagerHls();
			case KontorolDeliveryProfileType::LIVE_PACKAGER_DASH:
			case KontorolDeliveryProfileType::LIVE_PACKAGER_MSS:
			case KontorolDeliveryProfileType::LIVE_PACKAGER_HDS:
				return new KontorolDeliveryProfileLivePackager();
			default:
				$obj = KontorolPluginManager::loadObject('KontorolDeliveryProfile', $type);
				if(!$obj)
					$obj = new KontorolDeliveryProfile();
				return $obj;
		}
	}
	
	public static function getTokenizerInstanceByType($type) {
		switch ($type) {
			case 'kLevel3UrlTokenizer':
				return new KontorolUrlTokenizerLevel3();
			case 'kL3UrlTokenizer':
				return new KontorolUrlTokenizerL3();
			case 'kLimeLightUrlTokenizer':
				return new KontorolUrlTokenizerLimeLight();
			case 'kAkamaiHttpUrlTokenizer':
				return new KontorolUrlTokenizerAkamaiHttp();
			case 'kAkamaiRtmpUrlTokenizer':
				return new KontorolUrlTokenizerAkamaiRtmp();
			case 'kAkamaiRtspUrlTokenizer':
				return new KontorolUrlTokenizerAkamaiRtsp();
			case 'kAkamaiSecureHDUrlTokenizer':
				return new KontorolUrlTokenizerAkamaiSecureHd();
			case 'kCloudFrontUrlTokenizer':
				return new KontorolUrlTokenizerCloudFront();
			case 'kBitGravityUrlTokenizer':
				return new KontorolUrlTokenizerBitGravity();
			case 'kVnptUrlTokenizer':
				return new KontorolUrlTokenizerVnpt();
			case 'kChtHttpUrlTokenizer':
				return new KontorolUrlTokenizerCht();
			case 'kChinaCacheUrlTokenizer':
				return new KontorolUrlTokenizerChinaCache();	
			case 'kKsUrlTokenizer':
				return new KontorolUrlTokenizerKs();
			case 'kWowzaSecureTokenUrlTokenizer':
				return new KontorolUrlTokenizerWowzaSecureToken();
			case 'kKontorolUrlTokenizer':
				return new KontorolUrlTokenizerKontorol();

			// Add other tokenizers here
			default:
				$apiObject = KontorolPluginManager::loadObject('KontorolTokenizer', $type);
				if($apiObject)
					return $apiObject;
				KontorolLog::err("Cannot load API object for core Tokenizer [" . $type . "]");
				return null;
		}
	}
	
	public static function getRecognizerByType($type) {
		switch ($type) {
			case 'kUrlRecognizerAkamaiG2O':
				return new KontorolUrlRecognizerAkamaiG2O();
			case 'kKontorolUrlRecognizer':
				return new KontorolUrlRecognizerKontorol();
			case 'kUrlRecognizer':
				return new KontorolUrlRecognizer();
			default:
				$apiObject = KontorolPluginManager::loadObject('KontorolRecognizer', $type);
				if($apiObject)
					return $apiObject;
				KontorolLog::err("Cannot load API object for core Recognizer [" . $type . "]");
				return null;
		}
	}

}

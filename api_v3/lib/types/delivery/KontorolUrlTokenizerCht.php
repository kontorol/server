<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolUrlTokenizerCht extends KontorolUrlTokenizer {

	public function toObject($dbObject = null, $skip = array())
	{
		if (is_null($dbObject))
			$dbObject = new kChtHttpUrlTokenizer();
			
		parent::toObject($dbObject, $skip);
	
		return $dbObject;
	}
}

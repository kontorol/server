<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolPermissionName extends KontorolDynamicEnum implements PermissionName
{
	// see permissionName interface
	
	public static function getEnumClass()
	{
		return 'PermissionName';
	}
}

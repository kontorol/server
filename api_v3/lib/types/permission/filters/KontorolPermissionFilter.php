<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolPermissionFilter extends KontorolPermissionBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new PermissionFilter();
	}

	/* (non-PHPdoc)
	 * @see KontorolRelatedFilter::getListResponse()
	 */
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$permissionFilter = $this->toObject();
		
		$c = new Criteria();
		$permissionFilter->attachToCriteria($c);
		$count = PermissionPeer::doCount($c);
		
		$pager->attachToCriteria ( $c );
		
		$list = PermissionPeer::doSelect($c);
		
		$response = new KontorolPermissionListResponse();
		$response->objects = KontorolPermissionArray::fromDbArray($list, $responseProfile);
		$response->totalCount = $count;
		
		return $response;
	}
}

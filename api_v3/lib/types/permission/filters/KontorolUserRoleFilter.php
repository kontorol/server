<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolUserRoleFilter extends KontorolUserRoleBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new UserRoleFilter();
	}

	/* (non-PHPdoc)
	 * @see KontorolRelatedFilter::getListResponse()
	 */
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$userRoleFilter = $this->toObject();

		$c = new Criteria();
		$userRoleFilter->attachToCriteria($c);
		$count = UserRolePeer::doCount($c);
		
		$pager->attachToCriteria ( $c );
		$list = UserRolePeer::doSelect($c);
		
		$response = new KontorolUserRoleListResponse();
		$response->objects = KontorolUserRoleArray::fromDbArray($list, $responseProfile);
		$response->totalCount = $count;
		
		return $response;
	}
}

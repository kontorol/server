<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolPermissionItemFilter extends KontorolPermissionItemBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new PermissionItemFilter();
	}
	
	/* (non-PHPdoc)
	 * @see KontorolRelatedFilter::getListResponse()
	 */
	public function getListResponse(KontorolFilterPager $pager, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$permissionItemFilter = $this->toObject();
		
		$c = new Criteria();
		$permissionItemFilter->attachToCriteria($c);
		$count = PermissionItemPeer::doCount($c);
		
		$pager->attachToCriteria ( $c );
		$list = PermissionItemPeer::doSelect($c);
		
		$response = new KontorolPermissionItemListResponse();
		$response->objects = KontorolPermissionItemArray::fromDbArray($list, $responseProfile);
		$response->totalCount = $count;
		
		return $response;
	}
}

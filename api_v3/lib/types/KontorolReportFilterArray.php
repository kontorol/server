<?php
/**
 * @package api
 * @subpackage objects
 *
 */
class KontorolReportFilterArray extends KontorolTypedArray
{
	public function __construct()
	{
		return parent::__construct("KontorolReportFilter");
	}
}

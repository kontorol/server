<?php
/**
 * An array of KontorolUiConfTypeInfo
 * 
 * @package api
 * @subpackage objects
 */
class KontorolUiConfTypeInfoArray extends KontorolTypedArray
{
	public function __construct()
	{
		return parent::__construct("KontorolUiConfTypeInfo");
	}
}
?>

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolScope extends KontorolObject
{
	public function toObject($objectToFill = null, $propsToSkip = array())
	{
		if (is_null($objectToFill))
			$objectToFill = new kScope();

		return parent::toObject($objectToFill, $propsToSkip);
	}
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolPluginDataArray extends KontorolAssociativeArray
{

    public function __construct()
    {
        parent::__construct("KontorolPluginData");
    }
}

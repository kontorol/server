<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolStreamContainerArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolStreamContainerArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
			$stream = new KontorolStreamContainer();
			$stream->fromObject( $obj, $responseProfile );
			$newArr[] = $stream;
		}

		return $newArr;
	}

	public function __construct()
	{
		parent::__construct("KontorolStreamContainer");
	}
}

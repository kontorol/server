<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolStorageProfileArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolStorageProfileArray();
		foreach($arr as $obj)
		{
		    /* @var $obj StorageProfile */
			$nObj = KontorolStorageProfile::getInstanceByType($obj->getProtocol());
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct( )
	{
		return parent::__construct ( "KontorolStorageProfile" );
	}
}

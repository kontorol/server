<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolStorageServePriority extends KontorolEnum
{	  				
	const KONTOROL_ONLY = 1;
	const KONTOROL_FIRST = 2;
	const EXTERNAL_FIRST = 3;
	const EXTERNAL_ONLY = 4;
}

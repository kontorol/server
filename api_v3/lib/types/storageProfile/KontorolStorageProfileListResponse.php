<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolStorageProfileListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolStorageProfileArray
	 * @readonly
	 */
	public $objects;
}

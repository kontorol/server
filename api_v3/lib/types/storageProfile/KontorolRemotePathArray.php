<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolRemotePathArray extends KontorolTypedArray
{
	public static function fromDbArray(array $arr = null, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolRemotePathArray();
		foreach($arr as $obj)
		{
			$nObj = new KontorolRemotePath();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct( )
	{
		return parent::__construct ( "KontorolRemotePath" );
	}
}

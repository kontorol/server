<?php
/**
 * @package api
 * @subpackage enum
 */
class KontorolStorageProfileProtocol extends KontorolDynamicEnum implements StorageProfileProtocol
{
	public static function getEnumClass()
	{
		return 'StorageProfileProtocol';
	}
}

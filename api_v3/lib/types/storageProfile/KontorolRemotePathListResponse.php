<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolRemotePathListResponse extends KontorolListResponse
{
	/**
	 * @var KontorolRemotePathArray
	 * @readonly
	 */
	public $objects;
}

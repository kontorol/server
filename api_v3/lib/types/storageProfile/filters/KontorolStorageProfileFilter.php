<?php
/**
 * @package api
 * @subpackage filters
 */
class KontorolStorageProfileFilter extends KontorolStorageProfileBaseFilter
{
	/* (non-PHPdoc)
	 * @see KontorolFilter::getCoreFilter()
	 */
	protected function getCoreFilter()
	{
		return new StorageProfileFilter();
	}
}

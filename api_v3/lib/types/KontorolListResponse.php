<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolListResponse extends KontorolObject
{
	/**
	 * @var int
	 * @readonly
	 */
	public $totalCount;

	/* (non-PHPdoc)
	 * @see KontorolObject::loadRelatedObjects($responseProfile)
	 */
	public function loadRelatedObjects(KontorolDetachedResponseProfile $responseProfile)
	{
		if($this->objects)
		{
			$this->objects->loadRelatedObjects($responseProfile);
		}
	}
}

<?php
/**
 * @package api
 * @subpackage enum
 */
interface IKontorolDynamicEnum extends IKontorolEnum
{
	/**
	 * @return array
	 */
	public static function getEnumClass();
}

<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolCsvAdditionalFieldInfoArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolCsvAdditionalFieldInfoArray();
		foreach($arr as $obj)
		{
			$nObj = new KontorolCsvAdditionalFieldInfo();
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}

		return $newArr;
	}

	public function __construct()
	{
		return parent::__construct("KontorolCsvAdditionalFieldInfo");
	}
}

<?php
/**
 * @package api
 * @subpackage objects
 * @deprecated
 */
class KontorolSearchResultArray extends KontorolTypedArray
{
	public static function fromSearchResultArray ( $arr , KontorolSearch $search )
	{
		$newArr = new KontorolSearchResultArray();
		foreach ( $arr as $obj )
		{
			$nObj = new KontorolSearchResult();
			$nObj->fromSearchResult( $obj , $search );
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
	
	public function __construct( )
	{
		return parent::__construct ( "KontorolSearchResult" );
	}
}
?>

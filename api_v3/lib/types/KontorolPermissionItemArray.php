<?php
/**
 * @package api
 * @subpackage objects
 */
class KontorolPermissionItemArray extends KontorolTypedArray
{
	public static function fromDbArray($arr, KontorolDetachedResponseProfile $responseProfile = null)
	{
		$newArr = new KontorolPermissionItemArray();
		if ($arr == null)
			return $newArr;

		foreach ($arr as $obj)
		{
			if ($obj->getType() == PermissionItemType::API_ACTION_ITEM) {
				$nObj = new KontorolApiActionPermissionItem();
			}
			else if ($obj->getType() == PermissionItemType::API_PARAMETER_ITEM) {
				$nObj = new KontorolApiParameterPermissionItem();
			}
			else {
				KontorolLog::crit('Unknown permission item type ['.$obj->getType().'] defined with id ['.$obj->getId().'] - skipping!');
				continue;
			}
			$nObj->fromObject($obj, $responseProfile);
			$newArr[] = $nObj;
		}
		
		return $newArr;
	}
		
	public function __construct()
	{
		parent::__construct('KontorolPermissionItem');
	}
}

<?php
/**
 * @package api
 * @subpackage objects.factory
 */
class KontorolAssetParamsFactory
{
	static function getAssetParamsOutputInstance($type)
	{
		switch ($type) 
		{
			case KontorolAssetType::FLAVOR:
				return new KontorolFlavorParamsOutput();
				
			case KontorolAssetType::THUMBNAIL:
				return new KontorolThumbParamsOutput();
				
			default:
				$obj = KontorolPluginManager::loadObject('KontorolAssetParamsOutput', $type);
				if($obj)
					return $obj;
					
				return new KontorolFlavorParamsOutput();
		}
	}
	
	static function getAssetParamsInstance($type)
	{
		switch ($type) 
		{
			case KontorolAssetType::FLAVOR:
				return new KontorolFlavorParams();
				
			case KontorolAssetType::THUMBNAIL:
				return new KontorolThumbParams();
				
			default:
				$obj = KontorolPluginManager::loadObject('KontorolAssetParams', $type);
				if($obj)
					return $obj;
					
				return new KontorolFlavorParams();
		}
	}
}

<?php

/**
 * Define client request optional configurations
 */
class KontorolRequestConfiguration extends KontorolObject
{
	/**
	 * Impersonated partner id
	 * @var int
	 */
	public $partnerId;
	
	/**
	 * Kontorol API session
	 * @alias sessionId
	 * @var string
	 */
	public $ks;
	
	/**
	 * Response profile - this attribute will be automatically unset after every API call.
	 * @var KontorolBaseResponseProfile
	 * @volatile
	 */
	public $responseProfile;
}

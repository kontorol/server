<?php
/**
 * @package api
 * @subpackage v3
 */
class KontorolServiceActionItem
{
    /**
     * @var string
     */
    public $serviceId;
    
    /**
     * @var string
     */
    public $serviceClass;
    
    /**
     * @var KontorolDocCommentParser
     */
    public $serviceInfo;
    
    /**
     * @var array
     */
    public $actionMap;
    
    public static function cloneItem (KontorolServiceActionItem $item)
    {
        $serviceActionItem = new KontorolServiceActionItem();
        $serviceActionItem->serviceId = $item->serviceId;
        $serviceActionItem->serviceClass = $item->serviceClass;
        $serviceActionItem->serviceInfo = $item->serviceInfo;
        $serviceActionItem->actionMap = $item->actionMap;
        return $serviceActionItem;
    }

}

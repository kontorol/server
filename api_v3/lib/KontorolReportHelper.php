<?php
/**
 * @package api
 * @subpackage v3
 */
class KontorolReportHelper
{
	public static function getValidateExecutionParameters(Report $report, KontorolKeyValueArray $params = null)
	{
		if (is_null($params))
			$params = new KontorolKeyValueArray();
			
		$execParams = array();
		$currentParams = $report->getParameters();
		foreach($currentParams as $currentParam)
		{
			$found = false;
			foreach($params as $param)
			{
				/* @var $param KontorolKeyValue */
				if ((strtolower($param->key) == strtolower($currentParam)))
				{
					$execParams[':'.$currentParam] = $param->value;
					$found = true;
				}
			}
			
			if (!$found)
				throw new KontorolAPIException(KontorolErrors::REPORT_PARAMETER_MISSING, $currentParam);
		}
		return $execParams;
	}
}

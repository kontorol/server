<?php
/**
 * @package api
 * @subpackage objects.factory
 */
class KontorolEntryFactory
{
	/**
	 * @param int $type
	 * @param bool $isAdmin
	 * @return KontorolBaseEntry
	 */
	static function getInstanceByType ($type, $isAdmin = false)
	{
		switch ($type) 
		{
			case KontorolEntryType::MEDIA_CLIP:
				$obj = new KontorolMediaEntry();
				break;
				
			case KontorolEntryType::MIX:
				$obj = new KontorolMixEntry();
				break;
				
			case KontorolEntryType::PLAYLIST:
				$obj = new KontorolPlaylist();
				break;
				
			case KontorolEntryType::DATA:
				$obj = new KontorolDataEntry();
				break;
				
			case KontorolEntryType::LIVE_STREAM:
				if($isAdmin)
				{
					$obj = new KontorolLiveStreamAdminEntry();
				}
				else
				{
					$obj = new KontorolLiveStreamEntry();
				}
				break;
				
			case KontorolEntryType::LIVE_CHANNEL:
				$obj = new KontorolLiveChannel();
				break;
				
			default:
				$obj = KontorolPluginManager::loadObject('KontorolBaseEntry', $type);
				
				if(!$obj)
					$obj = new KontorolBaseEntry();
					
				break;
		}
		
		return $obj;
	}
}

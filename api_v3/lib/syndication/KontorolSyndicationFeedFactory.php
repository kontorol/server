<?php
/**
 * @package api
 * @subpackage objects.factory
 */
class KontorolSyndicationFeedFactory
{
	static function getInstanceByType ($type)
	{
		switch ($type) 
		{
			case KontorolSyndicationFeedType::GOOGLE_VIDEO:
				$obj = new KontorolGoogleVideoSyndicationFeed();
				break;
			case KontorolSyndicationFeedType::YAHOO:
				$obj = new KontorolYahooSyndicationFeed();
				break;
			case KontorolSyndicationFeedType::ITUNES:
				$obj = new KontorolITunesSyndicationFeed();
				break;
			case KontorolSyndicationFeedType::TUBE_MOGUL:
				$obj = new KontorolTubeMogulSyndicationFeed();
				break;
			case KontorolSyndicationFeedType::KONTOROL:
				$obj = new KontorolGenericSyndicationFeed();
				break;
			case KontorolSyndicationFeedType::KONTOROL_XSLT:
				$obj = new KontorolGenericXsltSyndicationFeed();
				break;
			case KontorolSyndicationFeedType::OPERA_TV_SNAP:
				$obj = new KontorolOperaSyndicationFeed();
				break;
			case KontorolSyndicationFeedType::ROKU_DIRECT_PUBLISHER:
				$obj = new KontorolRokuSyndicationFeed();
				break;
			default:
				$obj = new KontorolBaseSyndicationFeed();
				break;
		}
		
		return $obj;
	}
	
	static function getRendererByType($type)
	{
		switch ($type)
		{
			case KontorolSyndicationFeedType::GOOGLE_VIDEO:
				$obj = new GoogleVideoFeedRenderer();
				break;
			case KontorolSyndicationFeedType::YAHOO:
				$obj = new YahooFeedRenderer();
				break;
			case KontorolSyndicationFeedType::ITUNES:
				$obj = new ITunesFeedRenderer();
				break;
			case KontorolSyndicationFeedType::TUBE_MOGUL:
				$obj = new TubeMogulFeedRenderer();
				break;
			case KontorolSyndicationFeedType::KONTOROL:
			case KontorolSyndicationFeedType::KONTOROL_XSLT:
			case KontorolSyndicationFeedType::OPERA_TV_SNAP:
			case KontorolSyndicationFeedType::ROKU_DIRECT_PUBLISHER:
			default:
				return new KontorolFeedRenderer();
				break;
		}
		return $obj;
	}
}

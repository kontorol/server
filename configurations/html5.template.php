<?php
/**
 * This file store all of mwEmbed local configuration ( in a default svn check out this file is empty )
 *
 * See includes/DefaultSettings.php for a configuration options
 */

// Old kConf path
$kConfPath = '@APP_DIR@/alpha/config/kConf.php';
if( ! file_exists( $kConfPath ) ) {
        // New kConf path
        $kConfPath = '@APP_DIR@/infra/kConf.php';
        if( ! file_exists( $kConfPath ) ) {
                die('Error: Unable to find kConf.php at ' . $kConfPath);
        }
}
// Load kontorol configuration file
require_once( $kConfPath );

$kConf = new kConf();

// Kontorol HTML5lib Version
$wgKontorolVersion = basename(getcwd()); // Gets the version by the folder name

// The default Kontorol service url:
$wgKontorolServiceUrl = wgGetUrl('cdn_api_host');
// Default Kontorol CDN url:
$wgKontorolCDNUrl = wgGetUrl('cdn_host');
// Default Stats URL
$wgKontorolStatsServiceUrl = wgGetUrl('stats_host');
// Default Live Stats URL
$wgKontorolLiveStatsServiceUrl = wgGetUrl('live_stats_host');
// Default Kontorol Analytics URL
$wgKontorolAnalyticsServiceUrl = wgGetUrl('analytics_host');

// SSL host names
if( $wgHTTPProtocol == 'https' ){
        $wgKontorolServiceUrl = wgGetUrl('cdn_api_host_https');
        $wgKontorolCDNUrl = wgGetUrl('cdn_host_https');
        $wgKontorolStatsServiceUrl = wgGetUrl('stats_host_https');
	$wgKontorolLiveStatsServiceUrl = wgGetUrl('live_stats_host_https');
	$wgKontorolAnalyticsServiceUrl = wgGetUrl('analytics_host_https');
}

// Default Asset CDN Path (used in ResouceLoader.php):
$wgCDNAssetPath = $wgKontorolCDNUrl;

// Default Kontorol Cache Path
$wgScriptCacheDirectory = $kConf->get('cache_root_path') . '/html5/' . $wgKontorolVersion;

$wgLoadScript = $wgKontorolServiceUrl . '/html5/html5lib/' . $wgKontorolVersion . '/load.php';
$wgResourceLoaderUrl = $wgLoadScript;

// Salt for proxy the user IP address to Kontorol API
if( $kConf->hasParam('remote_addr_header_salt') ) {
        $wgKontorolRemoteAddressSalt = $kConf->get('remote_addr_header_salt');
}

// Disable Apple HLS if defined in kConf
if( $kConf->hasParam('use_apple_adaptive') ) {
        $wgKontorolUseAppleAdaptive = $kConf->get('use_apple_adaptive');
}

// Get Kontorol Supported API Features
if( $kConf->hasParam('features') ) {
        $wgKontorolApiFeatures = $kConf->get('features');
}

// Allow Iframe to connect remote service
$wgKontorolAllowIframeRemoteService = true;

// Set debug for true (testing only)
$wgEnableScriptDebug = false;

// Get PlayReady License URL
if( $kConf->hasMap('playReady') ) {
        $playReadyMap = $kConf->getMap('playReady');
        if($playReadyMap)
                $wgKontorolLicenseServerUrl = $playReadyMap['license_server_url'];
}

// A helper function to get full URL of host
function wgGetUrl( $hostKey = null ) {
        global $wgHTTPProtocol, $wgServerPort, $kConf;
        if( $hostKey && $kConf->hasParam($hostKey) ) {
                return $wgHTTPProtocol . '://' . $kConf->get($hostKey);
        }
        return null;
}
